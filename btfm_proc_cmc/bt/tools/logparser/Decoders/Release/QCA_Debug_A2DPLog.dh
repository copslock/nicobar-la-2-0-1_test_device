/** @file
 *
 * QCA Bluetooth Firmware Logging Parser
 *
 */

/*-------------------------------------------------------------------------
 * Copyright (c) 2018 Qualcomm Technologies, Inc.
 * All Rights Reserved
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 * Notifications and licenses are retained for attribution purposes only
 *-----------------------------------------------------------------------*/

/************************************************
 * A2DP log main table
************************************************/
TABLE tQca_A2DPLogID APPENDS_ONLY tQca_EnhLogID
    {0x0900 "LOG_ID_A2DP_PKT_TIMER_EXPIRED"         "LOG_ID_A2DP_PKT_TIMER_EXPIRED"         0 gQca_A2DP_PktTimerExpired}
    {0x0901 "LOG_ID_A2DP_SAMPLE_BUF_OVERFLOW"       "LOG_ID_A2DP_SAMPLE_BUF_OVERFLOW"       0 gQca_A2DP_SampleBufOverflow}
    {0x0902 "LOG_ID_AOLE_COMMON"                    "LOG_ID_AOLE_COMMON"                    0 gQca_AoLE_Common}
    {0x0903 "LOG_ID_AOLE_SYNC"                      "LOG_ID_AOLE_SYNC"                      0 gQca_AoLE_Common}
    {0x0904 "LOG_ID_AOLE_VOICE_TH"                  "LOG_ID_AOLE_VOICE_TH"                  0 gQca_AoLE_Common}
    {0x0905 "LOG_ID_AOLE_VOICE_BH"                  "LOG_ID_AOLE_VOICE_BH"                  0 gQca_AoLE_Common}
    {0x0906 "LOG_ID_AOLE_LINK_ALIGN"                "LOG_ID_AOLE_LINK_ALIGN"                0 gQca_AoLE_Link_Align}
    {0x0907 "LOG_ID_A2DP_LINK_THROUGHPUT"           "LOG_ID_A2DP_LINK_THROUGHPUT"           0 gQca_A2DPThroughput}
    {0x0908 "LOG_ID_VOICE_BUFFER_MIC_DATA"          "LOG_ID_VOICE_BUFFER_MIC_DATA"          0 gQca_VOICE_BUFFER_MIC_DATA}
    {0x0909 "LOG_ID_VOICE_BUFFER_SPKR_DATA"         "LOG_ID_VOICE_BUFFER_SPKR_DATA"         0 gQca_VOICE_BUFFER_SPKR_DATA}
    {0x090A "LOG_ID_AUDIO_BUFFER_DATA"              "LOG_ID_AUDIO_BUFFER_DATA"              0 gQca_AUDIO_BUFFER_DATA}
    {0x090B "LOG_ID_AUDIO_DATA_RINGBUFFER_FULL"     "LOG_ID_AUDIO_DATA_RINGBUFFER_FULL"     0 gQca_AUDIO_DATA_RINGBUFFER_FULL}
    {0x090C "LOG_ID_A2DP_LINK_THRESHOLD_ARRAY"      "LOG_ID_A2DP_LINK_THRESHOLD_ARRAY"      0 gQca_thresholdArray}
    {0x0913 "LOG_ID_A2DP_LINK_QUALITY"              "LOG_ID_A2DP_LINK_QUALITY"              0 gQca_A2DP_LINK_QUALITY}
    {0x0915 "LOG_ID_A2DP_LINK_QUALITY_TXDONE"       "LOG_ID_A2DP_LINK_QUALITY_TXDONE"       0 gQca_DefaultPrint}
    {0x0916 "LOG_ID_COP_MEDIA_PKT_INFO"             "LOG_ID_COP_MEDIA_PKT_INFO"             0 gQca_COPMediaPktInfo}
    {0x0917 "LOG_ID_A2DP_MEDIA_BUFFER_SIZE"         "LOG_ID_A2DP_MEDIA_BUFFER_SIZE"         0 gQca_BufferSize}
    {0x0918 "LOG_ID_A2DP_LQ_UPSCALE_RESET"          "LOG_ID_A2DP_LQ_UPSCALE_RESET"          0 gQca_LQReset}
    
    {0x09FF "A2DP End"}
ENDTABLE

GROUP gQca_A2DP_PktTimerExpired "Sample Count"
{
    FIELD fQca_A2DP_SampleCount                 (Fixed 2 Bytes)     (Decimal)                           "Sample Count"
}

GROUP gQca_A2DP_SampleBufOverflow "Overflow Count"
{
    FIELD fQca_A2DP_OverflowCount               (Fixed 2 Bytes)     (Decimal)                           "Overflow Count"
}

TABLE tQca_AoLE_Links
    { 0x00 "Left"}
    { 0x01 "Right"}
    {Default "Invalid" }
ENDTABLE

TABLE tQca_AoLE_Common_State
    { 0x00 "Init"}
    { 0x01 "Attach"}
    { 0x02 "Detach"}
    { 0x03 "Voice Start Succeed"}
    { 0x04 "Voice Start Failed"}
    { 0x05 "Voice Stop"}
    { 0x06 "Voice Get"}
    { 0x07 "Voice Put"}
    { 0x08 "Voice Underflow"}
    { 0x09 "Voice Overflow"}
    { 0x0A "Link Aligned"}
    {Default "Invalid" }
ENDTABLE

GROUP gQca_AoLE_Common "AoLE Common State"
{
    FIELD fQca_AoLE_Common	(Fixed 1 Byte)     (TABLE tQca_AoLE_Common_State) 	"AoLE Common State"
    FIELD fQca_AoLE_Links	(Fixed 1 Byte)     (TABLE tQca_AoLE_Links) 		"AoLE Links"
}

GROUP gQca_AoLE_Link_Align "AoLE Link Align"
{
    GROUP gQca_AoLE_Common; 
    FIELD fQca_AoLE_AnchorDelta	    (Fixed 4 Bytes)     (Decimal) 	"AoLE Anchor Delta"
}

GROUP gQca_A2DPThroughput "A2DP Link Throughput"
{
    FIELD fQca_A2DPTpt    (Fixed 4 Bytes)    (Decimal)    "Throughput_kbps"
}

GROUP gQca_thresholdArray "A2DP NAKrate thresholds"
{
    FIELD fQcaCntr (Fixed 0) RETRIEVE (StoreInteger 1) (hex) SUPPRESS_DETAIL STORE cntr
    GROUP gQca_thresholdPrinting REPEAT COUNT (Fixed 32 Times)
    {
        GROUP gQCA_thresholdPrintingPkts LABEL(LabelCount "threshold for #" cntr)
        {
           FIELD fQca_PktThresholdq0   (Fixed 1 Bytes)      (Decimal)          "NAKThreshold_Q0"
           FIELD fQca_PktThresholdq1   (Fixed 1 Bytes)      (Decimal)          "NAKThreshold_Q1"
           FIELD fQca_PktThresholdq2   (Fixed 1 Bytes)      (Decimal)          "NAKThreshold_Q2"
           FIELD fQca_PktThresholdq3   (Fixed 1 Bytes)      (Decimal)          "NAKThreshold_Q3"
        }
        /* Increment the Counter */
        FIELD fQca_ThresholdCntrIncr (Fixed 0) RETRIEVE (StoreField Cntr) ALSO (AddInteger 1) (Decimal) SUPPRESS_DETAIL STORE Cntr
    }
}

GROUP gQca_AUDIO_BUFFER_DATA "Audio Data"
{
    FIELD fQca_Audio_Pkt_Sqn_No                 (Fixed 2 Bytes)                         (Decimal)                           "Sequence No"
    FIELD fQca_Audio_Pkt_Len_                   (Fixed 2 Byte)                          (Decimal)                           "Pkt Length"
    FIELD fQca_Audio_IOP_Data                   (FromField Bytes fQca_Audio_Pkt_Len_)   (HEX)                               "Audio Data"
}

GROUP gQca_AUDIO_DATA_RINGBUFFER_FULL "Line No"
{
    FIELD fQca_Error_Line_No                    (Fixed 1 Byte)                          (Decimal)                           "Line No"
}

GROUP gQca_VOICE_BUFFER_MIC_DATA "TO AIR"
{
    FIELD fQca_Pkt_Sqn                          (Fixed 2 Bytes)                         (Decimal)                           "Sequence No"
    FIELD fQca_IOP_Pkt_Len                      (Fixed 1 Byte)                          (Decimal)                           "IOP Pkt Length"
    FIELD fQca_IOP_Pkt_Data_Mic                 (FromField Bytes fQca_IOP_Pkt_Len)      (HEX)                               "IOP DATA"
    FIELD fQca_Interpoller_Pkt_Len              (Fixed 1 Byte)                          (Decimal)                           "Interpoller pkt Length"
    FIELD fQca_Pkt_Interpoller_Data_Mic         (FromField Bytes fQca_Interpoller_Pkt_Len)      (HEX)                       "Interpoller DATA"
    FIELD fQca_Air_Pkt_Len                      (Fixed 1 Byte)                          (Decimal)                           "Air Pkt Length"
    FIELD fQca_Pkt_Air_Data_Mic                 (FromField Bytes fQca_Air_Pkt_Len)      (HEX)                               "Air DATA"
}

GROUP gQca_VOICE_BUFFER_SPKR_DATA "FROM AIR"
{
    FIELD fQca_Pkt_Sqn_spkr                     (Fixed 2 Bytes)                         (Decimal)                           "Sequence No"
    FIELD fQca_Air_Pkt_Len_spkr                 (Fixed 1 Byte)                          (Decimal)                           "Air Pkt Length"
    FIELD fQca_Pkt_AIR_Data_spkr                (FromField Bytes fQca_Air_Pkt_Len_spkr) (HEX)                               "AIR DATA"
    FIELD fQca_IOP_Pkt_Len_spkr                 (Fixed 1 Byte)                          (Decimal)                           "IOP Pkt Length"
    FIELD fQca_IOP_Pkt_Data_spkr                (FromField Bytes fQca_IOP_Pkt_Len_spkr) (HEX)                               "IOP DATA"
}

GROUP gQca_A2DP_LINK_QUALITY "A2DP Link Quality paramters"
{
    FIELD fQca_A2DP_Packet_Latency      (Fixed 2 Bytes)     (Decimal)   "Avg Packet Latency(ms)"
    FIELD fQca_A2DP_Active_Conn_Count   (Fixed 2 Bytes)     (Decimal)   "Active Conn Count"
    FIELD fQca_A2DP_Dur_Cxm_Denials     (Fixed 4 Bytes)     (Decimal)   "Avg Cxm Denial Duration(ms)"
    FIELD fQca_A2DP_FlowOnOffGap        (Fixed 4 Bytes)     (Decimal)   "Avg Flow Off-On Gap(ms)"
    FIELD fQca_A2DP_Link_Quality        (Fixed 4 Bytes)     (Decimal)   "A2DP Link Quality Level"
}

GROUP gQca_COPMediaPktInfo "COP Media Packet Info"
{
    FIELD fQca_COPMediaPktTTPAdj                (Fixed 4 Bytes)         (Decimal)       "TTP Adjust in usec"
    FIELD fQca_COPMediaPktInterval              (Fixed 2 Bytes)         (Decimal)       "Interval in usec"
    FIELD fQca_rsvd                             (Fixed 2 Bytes)         (Decimal)       "Reserved"
}

GROUP gQca_LQReset "Link Quality reset"
{
    FIELD fQca_LQReset                          (Fixed 1 Byte)          (Decimal)                "LQ counter"
}

TABLE tA2dpCodecType
    {0, "SBC"}
    {2, "AAC"}
    {4, "LDAC"}
    {8, "APT-X"}
    {9, "APT-X HD"}
    {10, "APT-X Adaptive"}
    {11, "APT-X TWS+"}
ENDTABLE

GROUP gQca_BufferSize "A2DP Buffer info"
{
    FIELD fQca_TimeToBuffer                     (Fixed 2 Bytes)         (Decimal)                       "Time to buffer in msec"
    FIELD fQca_BufferSize                       (Fixed 2 Bytes)         (Decimal)                       "Buffer Size in bytes"
    FIELD fQca_CodecInfo                        (Fixed 1 Byte)          (TABLE tA2dpCodecType)          "Codec type"
}
