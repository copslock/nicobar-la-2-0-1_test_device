#!/usr/bin/python
#
# Copyright (c) 2018 Qualcomm Technologies, Inc.
# All Rights Reserved
# Confidential and Proprietary - Qualcomm Technologies, Inc.
# Notifications and licenses are retained for attribution purposes only
#
import sys
import re
import os
import binascii
import struct
from optparse import OptionParser

if os.path.exists(os.path.dirname(sys.argv[0])+'/CPAS_Parser.py') == True :
    import CPAS_Parser


Description = """
Description : 
    1. This Script can be used for Splitting a Single Frame containing Multiple QCADebug logs into Multiple frames with Single QCADebug log in it.
       This can be very useful to analyze already collected logs.
       Following are list of files currently this is supported.
        - btsnoop file which is collected from Android HOST stack.
        - btsnoop file which is generated from QXDM
        - btsnoop file which is collected using Bluez BTMON
        - CPAS file thru which Snoop logs are collected using Virtual Sniffer.
        - CPAS file thru which Snoop logs are collected using HCI H4 Sniffer.
    
    2. This Script can also be used for converting raw hex data in hci h4 format to a btsnoop file format.
        Following are the list of log files for which this can be used. 
         - Ring buffer logs collected at Controller
         - Ring buffer logs collected at HOST
         - Ring buffer logs collected by HOST driver with Timestamps

    3. This Script can also be used to extract the Crashdump files from the Vendor events.
        The dump files are generated as bt_fw_crashdump-x.bin in the directory where the script is executed.
        Any missing Crash packet sequence id, full packet will be filled with sequence of "ABABABAB" pattern.
        
Usage: 
    %prog -i <inputfilename.xxx> -o <outputfilename.cfa> [-f <btsnoop/hcitxdump/hcirxdump/hcitxdump_ts/hcirxdump_ts/hcitxdump_ts1/hcirxdump_ts1>  -v <1/2/3>]

    1. If the input file is already a btsnoop or btmon or cpas file, run the following command for splitting QCADebug Logs.
        - %prog -i <.log/.cfa file> -o <.cfa file>
    2. If the input file is raw hex hci h4 logs without any timestamp
        - %prog -i <.bin/.txt file> -o <.log file> -f hcitxdump
        - %prog -i <.bin/.txt file> -o <.log file> -f hcirxdump
    3. If the input file is raw hex hci h4 logs with timestamp
        - %prog -i <.bin/.txt file> -o <.log file> -f hcitxdump_ts
        - %prog -i <.bin/.txt file> -o <.log file> -f hcirxdump_ts
        - %prog -i <.bin/.txt file> -o <.log file> -f hcitxdump_ts1
        - %prog -i <.bin/.txt file> -o <.log file> -f hcirxdump_ts1        
"""


DEBUG_INFO_FLAG = 0

#----------------------------------------------------
#File Types Supported by Script
#----------------------------------------------------
FILE_TYPE_NOT_SUPPORTED         = 0x00
FILE_TYPE_RAWDUMP_BINARY        = 0x01
FILE_TYPE_RAWDUMP_BINARY_TS     = 0x02
FILE_TYPE_BTSNOOP_HCI_H1        = 0x10
FILE_TYPE_BTSNOOP_HCI_H4        = 0x11
FILE_TYPE_BTSNOOP_HCI_BTMON     = 0x1f
FILE_TYPE_CPAS_VIRTUAL_SNIFFER  = 0x20
FILE_TYPE_CPAS_HSU_H4_SNIFFER   = 0x21

filetype                        = FILE_TYPE_NOT_SUPPORTED

#----------------------------------------------------
#BTSNOOP Format Specific Variables
#----------------------------------------------------
BTSNOOP_FILE_HDR_LEN                    = 16
BTSNOOP_FILE_HDR_START_PATTERN          = "6274736e6f6f70"                   #"btsnoop"
BTSNOOP_FILE_HDR_HCI_H1_HEX             = "6274736e6f6f700000000001000003e9"
BTSNOOP_FILE_HDR_HCI_H4_HEX             = "6274736e6f6f700000000001000003ea"
BTSNOOP_FILE_HDR_HCI_H4_BLUEZ_BTMON     = "6274736e6f6f700000000001000007d1"
BTSNOOP_FILE_HDR_DL_TYPE_HCI_H1         = 1001
BTSNOOP_FILE_HDR_DL_TYPE_HCI_UART_H4    = 1002
BTSNOOP_FILE_HDR_DL_TYPE_BLUEZ_BTMON    = 2001      #"Linux Monitor"
BTSNOOP_PKT_RECORD_FMT_LEN              = 24
BTSNOOP_FILE_TIMESTAMP_EPOCH_REF        = 0x00E03AB44A676000

BTSNOOP_PKT_FLAGS_DIR_MASK              = 0x01
BTSNOOP_PKT_FLAGS_DIR_HOST_TO_SOC       = 0
BTSNOOP_PKT_FLAGS_DIR_SOC_TO_HOST       = 0x01

BTSNOOP_PKT_FLAGS_CMD_FLAG_MASK         = 0x02
BTSNOOP_PKT_FLAGS_CMD_FLAG_ACL_DATA     = 0
BTSNOOP_PKT_FLAGS_CMD_FLAG_CMD_EVT      = 0x02

#----------------------------------------------------
#QCADebug Log Specific Variables
#----------------------------------------------------
QCADEBUG_PACKET_HDR_HEX         = "dc2e"
QCADEBUG_ENHLOG_HDR_LEN         = 10
QCADEBUG_PKTLOG_HDR_LEN         = 10

QCADEBUG_PKTID_SLC              = 0x0000

OVERHEAD_BYTES_FOR_ENHLOGLEN_PKTLOGLEN = 0x10
#OVERHEAD_BYTES_FOR_SNOOPLEN_HCIPACKET  = 0x05

# Variables related to Crashdump
EVENT_ID_VENDOR                 = "ff"
QCA_CRASHDUMP_PACKET_HDR_HEX    = "0108"
gCrashDumpDetected  = 0
gCrashDumpFileIdx   = 0
gCrashDumpSeq       = 0
gCrashDumpFilename  = "bt_fw_crashdump-"
gCrashDumpFilePath  = "."
gCrashContinueWithMissingPackets = 0
gCrashPadByte = int("AB", 16)
gCrashDumpPattern = ""
gCrashDumpEvtCount = 0
#----------------------------------------------------
#HCI H4 Packet ID's
#----------------------------------------------------
PKTID_HCI_COMMAND   = 0x01
PKTID_ACL_DATA      = 0x02
PKTID_SCO_DATA      = 0x03
PKTID_HCI_EVENT     = 0x04
PKTID_IBS_WAKEACK   = 0xFC
PKTID_IBS_WAKEIND   = 0xFD
PKTID_IBS_SLEEP     = 0xFE

#----------------------------------------------------
#BLUEZ BTMON Packet ID's
#----------------------------------------------------
PKTID_BTMON_OPCODE_MASK = 0xFFFF
PKTID_BTMON_HCI_COMMAND = 0x0002
PKTID_BTMON_HCI_EVENT   = 0x0003
PKTID_BTMON_ACL_TX_DATA = 0x0004
PKTID_BTMON_ACL_RX_DATA = 0x0005
PKTID_BTMON_SCO_TX_DATA = 0x0006
PKTID_BTMON_SCO_RX_DATA = 0x0007


#------------------------------------------------------------------------------
# Parse command line arguments
#------------------------------------------------------------------------------
def parse_args():    
    version = "GenFWLog_Snoop.py 2.0"
    global DEBUG_INFO_FLAG

    parser = OptionParser(usage=Description, version=version)
    parser.add_option("-i", "--input", dest="inputfile",
                  help="Input file to parse based on format specifier. Default is btsnoop file format.", metavar="FILE")
    parser.add_option("-o", "--output", dest="outputfile",
                  help="btsnoop file with QcaDebug Logs split into multiple Log Packets", metavar="FILE")
    parser.add_option("-f", "--format", dest="format", default="btsnoop",
                  help="Optional: Provide the info regarding the input file format like hcitxdump, hcirxdump, btsnoop, hcitxdump_ts, hcirxdump_ts ", metavar="str")
    parser.add_option("-v", "--verbose", dest="loglevel",
                  help="Pass LogLevel for more logs ", metavar="int")

    (options, args) = parser.parse_args()

    if options.loglevel is not None:
        DEBUG_INFO_FLAG = int(options.loglevel)

    if DEBUG_INFO_FLAG >= 2:
        print "Inputfile: " , options.inputfile
        print "Outputfile: ", options.outputfile

    if options.inputfile is None:
        #parser.error("-i option must be defined")
        parser.print_help()
        parser.exit(1)

    if options.outputfile is None:
        #parser.error("-O option must be defined")
        parser.print_help()
        parser.exit(1)

    return (options, args)

#------------------------------------------------------------------------------
# Routine to parse btsnoop format and Split the FW logs.
# File Format can be detected by the Header of the file and it should be as per
# BTSnoop format.
# If any QCADebug Logs are found while parsing those logs are splitted into 
# individual Frame with same timestamps. 
# This will help in faster decoding of logs.
# Output of file is again a btsnoop format but contains more frames.
#------------------------------------------------------------------------------
def Parse_BtSnoop(options, args):
    frameCount = 0
    global QcaDebugLogsFound
    global QcaDebugLogsAdded
    global QcaDebugSLCLogsAdded
    global filetype
    
    QcaDebugLogsFound = 0
    QcaDebugLogsAdded = 0
    QcaDebugSLCLogsAdded = 0
    input_obj = open(options.inputfile, "rb")
    output_obj = open(options.outputfile, "wb")

    print '\n'

    SnoopFileLen = os.fstat(input_obj.fileno()).st_size
    if DEBUG_INFO_FLAG >= 1: print SnoopFileLen

    SnoopFileHdr = input_obj.read(BTSNOOP_FILE_HDR_LEN)
    output_obj.write(SnoopFileHdr)
    bytesLeft = SnoopFileLen - BTSNOOP_FILE_HDR_LEN

    SnoopFileHdrStruct = struct.unpack('>8s2I', SnoopFileHdr[0:BTSNOOP_FILE_HDR_LEN])
    if DEBUG_INFO_FLAG >= 1: print "BTSnoop Format Version: ", SnoopFileHdrStruct[1]

    SnoopFileHdrDatalinkType = SnoopFileHdrStruct[2]
    if DEBUG_INFO_FLAG >= 1: print "BTSnoop Format Datalink Type: ", SnoopFileHdrDatalinkType

    if SnoopFileHdrDatalinkType != BTSNOOP_FILE_HDR_DL_TYPE_HCI_H1 and SnoopFileHdrDatalinkType != BTSNOOP_FILE_HDR_DL_TYPE_HCI_UART_H4 :
        print "BTSnoop Format not supported for DL Type: ", SnoopFileHdrDatalinkType
        exit(1)

    while bytesLeft > BTSNOOP_PKT_RECORD_FMT_LEN:
        # Read the Packet header
        SnoopPktHdr = input_obj.read(BTSNOOP_PKT_RECORD_FMT_LEN)
        SnoopPktHdrStruct = struct.unpack('>3I12s', SnoopPktHdr[0:BTSNOOP_PKT_RECORD_FMT_LEN])
        bytesLeft = bytesLeft - BTSNOOP_PKT_RECORD_FMT_LEN
        if DEBUG_INFO_FLAG >= 3: print "SnoopPktHdr: ", binascii.hexlify(SnoopPktHdr)

        # Get Included length param from the Packet header
        SnoopPktIncludeLen = SnoopPktHdrStruct[1]
        if DEBUG_INFO_FLAG >= 1: print "SnoopPktIncludeLen:", SnoopPktIncludeLen

        # Get the Packet Flags Info
        SnoopPktFlags = SnoopPktHdrStruct[2]
        if DEBUG_INFO_FLAG >= 3: print "SnoopPktFlags:", SnoopPktFlags


        # Read the Packet Data based on the Included Length
        SnoopPktData = input_obj.read(SnoopPktIncludeLen)
        bytesLeft = bytesLeft - SnoopPktIncludeLen
        frameCount += 1        

        PktDataHdl = '0000' 
        index = 0
        if SnoopFileHdrDatalinkType == BTSNOOP_FILE_HDR_DL_TYPE_HCI_H1:
            index = 0
        elif SnoopFileHdrDatalinkType == BTSNOOP_FILE_HDR_DL_TYPE_HCI_UART_H4:
            HCIPktID = SnoopPktData[0]
            index += 1
                
        if (SnoopPktFlags & BTSNOOP_PKT_FLAGS_CMD_FLAG_MASK) == BTSNOOP_PKT_FLAGS_CMD_FLAG_ACL_DATA:     #ACL Data
            if DEBUG_INFO_FLAG >= 2: print "ACL Packet Found: ", binascii.hexlify(SnoopPktData)
            PktDataHdl = SnoopPktData[index:index+2]
            if binascii.hexlify(PktDataHdl) == QCADEBUG_PACKET_HDR_HEX:
                Parse_QcaDebugLogPkt(SnoopPktData, index, SnoopPktHdrStruct, SnoopPktIncludeLen, SnoopFileHdrDatalinkType, output_obj)
                continue
                
        elif (SnoopPktFlags & BTSNOOP_PKT_FLAGS_CMD_FLAG_MASK) == BTSNOOP_PKT_FLAGS_CMD_FLAG_CMD_EVT and (SnoopPktFlags & BTSNOOP_PKT_FLAGS_DIR_MASK) == BTSNOOP_PKT_FLAGS_DIR_SOC_TO_HOST:   #Event
            if DEBUG_INFO_FLAG >= 2: print "Event Found: ", binascii.hexlify(SnoopPktData)
            
            PktEventID = SnoopPktData[index]
            if (binascii.hexlify(PktEventID) == EVENT_ID_VENDOR):
                gCrashDumpPattern = SnoopPktData[index+2:index+4]
                if (binascii.hexlify(gCrashDumpPattern) == QCA_CRASHDUMP_PACKET_HDR_HEX) :
                    if DEBUG_INFO_FLAG >= 3: print "Crashdump Event Detected"
                    Parse_CrashDumpPkt(SnoopPktData, index)
        else :
            if DEBUG_INFO_FLAG >= 2: print "SnoopPktData: ", binascii.hexlify(SnoopPktData)

        
        output_obj.write(SnoopPktHdr)
        output_obj.write(SnoopPktData)

    print "BTSnoop Frames Processed: ", frameCount
    print "QcaDebug Logs Found: ", QcaDebugLogsFound
    print "QcaDebug Logs Added: ", QcaDebugLogsAdded
    print "QcaDebug SLCLogs Added: ", QcaDebugSLCLogsAdded
    print options.outputfile , "is generated successfully."

    output_obj.close()
    input_obj.close()
    return None

#------------------------------------------------------------------------------
# Routine to parse btmon snoop format and Split the FW logs.
# File Format can be detected by the Header of the file and it should be as per
# BTSnoop format.
# If any QCADebug Logs are found while parsing those logs are splitted into 
# individual Frame with same timestamps. 
# This will help in faster decoding of logs.
# Output of file is again a btsnoop format but contains more frames.
#------------------------------------------------------------------------------
def Parse_BluezBTMonData(options, args):
    BTMON_frameCount = 0
    BTSNOOP_frameCount = 0
    global QcaDebugLogsFound
    global QcaDebugLogsAdded
    global QcaDebugSLCLogsAdded
    global filetype
    
    QcaDebugLogsFound = 0
    QcaDebugLogsAdded = 0
    QcaDebugSLCLogsAdded = 0    
    input_obj = open(options.inputfile, "rb")
    output_obj = open(options.outputfile, "wb")

    print '\n'

    SnoopFileLen = os.fstat(input_obj.fileno()).st_size
    if DEBUG_INFO_FLAG >= 1: print SnoopFileLen
    
    SnoopFileHdr = input_obj.read(BTSNOOP_FILE_HDR_LEN)
    bytesLeft = SnoopFileLen - BTSNOOP_FILE_HDR_LEN    
    
    #Write BTSnoop Header
    output_obj.write(binascii.unhexlify(BTSNOOP_FILE_HDR_HCI_H4_HEX))
    out_SnoopFileHdrDatalinkType = BTSNOOP_FILE_HDR_DL_TYPE_HCI_UART_H4
    
    SnoopFileHdrStruct = struct.unpack('>8s2I', SnoopFileHdr[0:BTSNOOP_FILE_HDR_LEN])
    if DEBUG_INFO_FLAG >= 1: print "BTSnoop Format Version: ", SnoopFileHdrStruct[1]

    SnoopFileHdrDatalinkType = SnoopFileHdrStruct[2]
    if DEBUG_INFO_FLAG >= 1: print "BTSnoop Format Datalink Type: ", SnoopFileHdrDatalinkType

    if SnoopFileHdrDatalinkType != BTSNOOP_FILE_HDR_DL_TYPE_BLUEZ_BTMON :
        print "BTSnoop Format not supported BTMON for DL Type: ", SnoopFileHdrDatalinkType
        exit(1)


    while bytesLeft > BTSNOOP_PKT_RECORD_FMT_LEN:
        # Read the Packet header
        # 4 Bytes : Original Length
        # 4 Bytes : Included Length
        # 2 Bytes : Adapter ID
        # 2 Bytes : Pkt Opcode 
        # 4 Bytes : 
        # 8 Bytes : Timestamp
        SnoopPktHdr = input_obj.read(BTSNOOP_PKT_RECORD_FMT_LEN)
        SnoopPktHdrStruct = struct.unpack('>3I12s', SnoopPktHdr[0:BTSNOOP_PKT_RECORD_FMT_LEN])
        bytesLeft = bytesLeft - BTSNOOP_PKT_RECORD_FMT_LEN
        if DEBUG_INFO_FLAG >= 3: print "SnoopPktHdr: ", binascii.hexlify(SnoopPktHdr)

        # Get Included length param from the Packet header
        SnoopPktIncludeLen = SnoopPktHdrStruct[1]
        if DEBUG_INFO_FLAG >= 1: print "SnoopPktIncludeLen:", SnoopPktIncludeLen

        # Get the Packet Flags Info
        # SnoopPktHdrStruct[2] is Adapter ID
        SnoopPktFlags = (SnoopPktHdrStruct[2] & PKTID_BTMON_OPCODE_MASK)
        if DEBUG_INFO_FLAG >= 3: print "SnoopPktFlags:", SnoopPktFlags

        # Read the Packet Data based on the Included Length
        SnoopPktData = input_obj.read(SnoopPktIncludeLen)
        bytesLeft = bytesLeft - SnoopPktIncludeLen
        BTMON_frameCount += 1


        PktDataHdl = '0000' 
        index = 0
        
        if (SnoopPktFlags == PKTID_BTMON_HCI_COMMAND) : 
            out_SnoopPktFlags = BTSNOOP_PKT_FLAGS_DIR_HOST_TO_SOC | BTSNOOP_PKT_FLAGS_CMD_FLAG_CMD_EVT
            out_SnoopPktID = PKTID_HCI_COMMAND
        
        elif (SnoopPktFlags == PKTID_BTMON_HCI_EVENT) :
            out_SnoopPktFlags = BTSNOOP_PKT_FLAGS_DIR_SOC_TO_HOST | BTSNOOP_PKT_FLAGS_CMD_FLAG_CMD_EVT
            out_SnoopPktID = PKTID_HCI_EVENT
            PktEventID = SnoopPktData[index]
            if (binascii.hexlify(PktEventID) == EVENT_ID_VENDOR):
                gCrashDumpPattern = SnoopPktData[index+2:index+4]
                if (binascii.hexlify(gCrashDumpPattern) == QCA_CRASHDUMP_PACKET_HDR_HEX) :
                    if DEBUG_INFO_FLAG >= 3: print "Crashdump Event Detected"
                    Parse_CrashDumpPkt(SnoopPktData, index)
        
        elif (SnoopPktFlags == PKTID_BTMON_ACL_TX_DATA) :
            out_SnoopPktFlags = BTSNOOP_PKT_FLAGS_DIR_HOST_TO_SOC | BTSNOOP_PKT_FLAGS_CMD_FLAG_ACL_DATA
            out_SnoopPktID = PKTID_ACL_DATA
        
        elif (SnoopPktFlags == PKTID_BTMON_ACL_RX_DATA) :
            out_SnoopPktFlags = BTSNOOP_PKT_FLAGS_DIR_SOC_TO_HOST | BTSNOOP_PKT_FLAGS_CMD_FLAG_ACL_DATA
            out_SnoopPktID = PKTID_ACL_DATA
            
        elif (SnoopPktFlags == PKTID_BTMON_SCO_TX_DATA) :
            out_SnoopPktFlags = BTSNOOP_PKT_FLAGS_DIR_HOST_TO_SOC
            out_SnoopPktID = PKTID_SCO_DATA
            
        elif (SnoopPktFlags == PKTID_BTMON_SCO_RX_DATA) :
            out_SnoopPktFlags = BTSNOOP_PKT_FLAGS_DIR_SOC_TO_HOST
            out_SnoopPktID = PKTID_SCO_DATA
            
        else :
            if DEBUG_INFO_FLAG >= 3: print "Invalid SnoopPktFlags:", SnoopPktFlags
            continue

        BTSNOOP_frameCount += 1
        out_SnoopPktIncludeLen = SnoopPktIncludeLen + 1
        out_SnoopPktHdrUpdate = struct.pack('>3I12s', out_SnoopPktIncludeLen,  out_SnoopPktIncludeLen, out_SnoopPktFlags, SnoopPktHdrStruct[3])
        out_SnoopPktHdrStruct = struct.unpack('>3I12s', out_SnoopPktHdrUpdate)
        out_SnoopPktData = chr(out_SnoopPktID) + SnoopPktData
        index += 1  #Adjust index as we are adding one byte into the SnoopPktData
        
        if (SnoopPktFlags == PKTID_BTMON_ACL_RX_DATA):
            PktDataHdl = out_SnoopPktData[index:index+2]
            if binascii.hexlify(PktDataHdl) == QCADEBUG_PACKET_HDR_HEX:
                if DEBUG_INFO_FLAG >= 2: print "QCADebug Packet Found: ", binascii.hexlify(out_SnoopPktData)
                Parse_QcaDebugLogPkt(out_SnoopPktData, index, out_SnoopPktHdrStruct, out_SnoopPktIncludeLen, out_SnoopFileHdrDatalinkType, output_obj)
                continue        

        output_obj.write(out_SnoopPktHdrUpdate)
        #output_obj.write(out_SnoopPktID)
        output_obj.write(out_SnoopPktData)

    print "BTMON Frames Processed: ", BTMON_frameCount
    print "BTSnoop Frames Processed: ", BTSNOOP_frameCount
    print "QcaDebug Logs Found: ", QcaDebugLogsFound
    print "QcaDebug Logs Added: ", QcaDebugLogsAdded
    print "QcaDebug SLCLogs Added: ", QcaDebugSLCLogsAdded
    print options.outputfile , "is generated successfully."

    
    output_obj.close()
    input_obj.close()
    return None

#------------------------------------------------------------------------------
# Routine to parse QcaDebugPkt 
# It splits the Multiple Logs in a Single Pkt to Multiple Packets with Single Log.
#------------------------------------------------------------------------------
def Parse_QcaDebugLogPkt(SnoopPktData, index, SnoopPktHdrStruct, SnoopPktIncludeLen, SnoopFileHdrDatalinkType, output_obj):
    global QcaDebugLogsFound
    global QcaDebugLogsAdded
    global QcaDebugSLCLogsAdded

    QcaDebugLogsFound += 1
    OVERHEAD_BYTES_FOR_SNOOPLEN_HCIPACKET = index+4
    if SnoopFileHdrDatalinkType == BTSNOOP_FILE_HDR_DL_TYPE_HCI_UART_H4:
        HCIPktID = SnoopPktData[0]
            
    # Read the QCA Enhlog Header including the packet format id
    QcaEnhHdr = SnoopPktData[index:index+QCADEBUG_ENHLOG_HDR_LEN]
    QcaEnhHdrStruct = struct.unpack('<2sHH4s', QcaEnhHdr[0:QCADEBUG_ENHLOG_HDR_LEN])
    index += QCADEBUG_ENHLOG_HDR_LEN

    while index < SnoopPktIncludeLen:
        # Read the QCA Debug Packet Header
        QcaPktHdr = SnoopPktData[index:index+QCADEBUG_PKTLOG_HDR_LEN]
        index += QCADEBUG_PKTLOG_HDR_LEN
        # Now get the Packet Id and Packet Len from the Packet Header. In the below struct format only the interested parameters are picked. Others skipped by mentioning 6s
        QcaPktHdrStruct = struct.unpack('<H6sH', QcaPktHdr[0:QCADEBUG_PKTLOG_HDR_LEN])
        QcaPktID = QcaPktHdrStruct[0]
        if DEBUG_INFO_FLAG >= 1: print "QcaPktID :", QcaPktID
        QcaPktLen = QcaPktHdrStruct[2]
        if DEBUG_INFO_FLAG >= 1: print "QcaPktLen :", QcaPktLen

        QcaPktData = SnoopPktData[index:index+QcaPktLen]
        index += QcaPktLen
        if DEBUG_INFO_FLAG >= 2: print "QcaPktData :", binascii.hexlify(QcaPktData)

        #Check whether it is SLC Log or other Debug Logs
        if QcaPktID == QCADEBUG_PKTID_SLC:
            indexSLC = 0

            if DEBUG_INFO_FLAG >= 2: print "SLC Log Detected"

            while indexSLC < QcaPktLen:
                QcaSLCLenFmt = ord(QcaPktData[indexSLC]) >> 6
                if QcaSLCLenFmt == 0:
                    QcaSLCLen = 2
                elif QcaSLCLenFmt == 1:
                    QcaSLCLen = 3
                elif QcaSLCLenFmt == 2:
                    QcaSLCLen = 5
                elif QcaSLCLenFmt == 3:
                    if ord(QcaPktData[indexSLC+1]) < 128:
                        QcaSLCLen = ord(QcaPktData[indexSLC+1]) + 2
                    else:
                        QcaSLCLen = (ord(QcaPktData[indexSLC+2]) << 8) + (ord(QcaPktData[indexSLC+1]) & 0x7f)
                else:
                    print "Invalid SLC packet detected"
                    sys.exit(2)

                QcaSLCData = QcaPktData[indexSLC:indexSLC+QcaSLCLen]
                indexSLC += QcaSLCLen
                if DEBUG_INFO_FLAG >= 3: print binascii.hexlify(QcaSLCData)

                QcaPktLenUpdate = QcaSLCLen
                QcaPktHdrUpdate = struct.pack('<H6sH', QcaPktHdrStruct[0], QcaPktHdrStruct[1], QcaPktLenUpdate)
                QcaEnhLenUpdate = QcaPktLenUpdate + OVERHEAD_BYTES_FOR_ENHLOGLEN_PKTLOGLEN
                QcaEnhHdrUpdate = struct.pack('<2sHH4s', QcaEnhHdrStruct[0], QcaEnhLenUpdate,  QcaEnhLenUpdate - 0x04, QcaEnhHdrStruct[3])
                SnoopPktLenUpdate = QcaEnhLenUpdate + OVERHEAD_BYTES_FOR_SNOOPLEN_HCIPACKET
                SnoopPktHdrUpdate = struct.pack('>3I12s', SnoopPktLenUpdate,  SnoopPktLenUpdate, SnoopPktHdrStruct[2], SnoopPktHdrStruct[3])

                QcaDebugSLCLogsAdded += 1
                output_obj.write(SnoopPktHdrUpdate)
                if SnoopFileHdrDatalinkType == BTSNOOP_FILE_HDR_DL_TYPE_HCI_UART_H4:
                    output_obj.write(HCIPktID)
                output_obj.write(QcaEnhHdrUpdate)
                output_obj.write(QcaPktHdrUpdate)
                output_obj.write(QcaSLCData)
        else:
            # Update the Headers of Snoop and Enh Log based on the packet length.
            QcaEnhLenUpdate = QcaPktLen + OVERHEAD_BYTES_FOR_ENHLOGLEN_PKTLOGLEN
            QcaEnhHdrUpdate = struct.pack('<2sHH4s', QcaEnhHdrStruct[0], QcaEnhLenUpdate,  QcaEnhLenUpdate - 0x04, QcaEnhHdrStruct[3])
            SnoopPktLenUpdate = QcaEnhLenUpdate + OVERHEAD_BYTES_FOR_SNOOPLEN_HCIPACKET
            SnoopPktHdrUpdate = struct.pack('>3I12s', SnoopPktLenUpdate,  SnoopPktLenUpdate, SnoopPktHdrStruct[2], SnoopPktHdrStruct[3])

            QcaDebugLogsAdded += 1
            output_obj.write(SnoopPktHdrUpdate)
            if SnoopFileHdrDatalinkType == BTSNOOP_FILE_HDR_DL_TYPE_HCI_UART_H4:
                output_obj.write(HCIPktID)
            output_obj.write(QcaEnhHdrUpdate)
            output_obj.write(QcaPktHdr)
            output_obj.write(QcaPktData)
    
    return None
#------------------------------------------------------------------------------
# Routine to parse CrashDumpPkt 
# It creates a file and dump the crashdump contents into it. 
#------------------------------------------------------------------------------
def Parse_CrashDumpPkt(SnoopPktData, index):
    global crash_obj
    global gCrashDumpFileIdx
    global gCrashDumpSeqNext
    
    PktLen = int(binascii.hexlify(SnoopPktData[index+1]), 16)
    SnoopPktLen = len(SnoopPktData)
    gCrashDumpSeq = int(binascii.hexlify(SnoopPktData[index+5] + SnoopPktData[index+4]), 16)
    if DEBUG_INFO_FLAG >= 1: print "Crashdump Sequence No: ", (gCrashDumpSeq)

    if(gCrashDumpSeq == 0) :
        gCrashDumpFile = gCrashDumpFilePath + "\\" + gCrashDumpFilename + str(gCrashDumpFileIdx) + ".bin"
        crash_obj = open(gCrashDumpFile, "wb")
        print "CrashDump File: ", gCrashDumpFile
        print "CrashDump Detected with size: 0x", (binascii.hexlify(SnoopPktData[index+10] + SnoopPktData[index+9] + SnoopPktData[index+8] + SnoopPktData[index+7]))
        gCrashDumpSeqNext = 1
        gCrashDumpPkt = SnoopPktData[index+11:SnoopPktLen]
        crash_obj.write(gCrashDumpPkt)
        
    elif(gCrashDumpSeq == 65535) :
        gCrashDumpPkt = SnoopPktData[index+7:SnoopPktLen]
        crash_obj.write(gCrashDumpPkt)
        crash_obj.close()
        print '\n'
        gCrashDumpFileIdx+=1
    else :
        if (gCrashDumpSeq != gCrashDumpSeqNext):
            print "Expected Crash Seq# ", gCrashDumpSeqNext, "Actual Crash Seq# ", gCrashDumpSeq
            missingbytes = (gCrashDumpSeq - gCrashDumpSeqNext) * 248
            print "No of Bytes Padding: ", missingbytes, "Padding Byte: ", hex(gCrashPadByte)
            i = 0
            while i < missingbytes :
                crash_obj.write(chr(gCrashPadByte))
                i+=1
            gCrashDumpSeqNext = gCrashDumpSeq
        
        gCrashDumpPkt = SnoopPktData[index+7:SnoopPktLen]
        crash_obj.write(gCrashDumpPkt)
        gCrashDumpSeqNext+=1
     
    return None

#------------------------------------------------------------------------------
# Routine to parse RAW Hex data file. 
# There is no way to automatically detect the file type from contents.
# This file contains either Tx data or Rx data. Both data can't be part of same
# file. 
# Packet Format is
# 1. PKT ID followed by data which is as per the HCI protocol
#------------------------------------------------------------------------------
def Parse_BTRawData(options, args):
    FrameCount = 0
    input_obj = open(options.inputfile, "rb")
    output_obj = open(options.outputfile, "wb")

    RawFileSize = os.fstat(input_obj.fileno()).st_size
    bytesLeft = RawFileSize
    if DEBUG_INFO_FLAG >= 1: print "Input file size: ", RawFileSize

    #Write BTSnoop Header
    output_obj.write(binascii.unhexlify(BTSNOOP_FILE_HDR_HCI_H4_HEX))

    while bytesLeft > 0:
        if options.format == "hcitxdump_ts" or options.format == "hcirxdump_ts":
            tsFields = 3
            tStamp = ""
            while tsFields > 0:
                tsByte = input_obj.read(1)
                bytesLeft -= 1
                if tsByte == ":":
                    tsFields-=1
                tStamp = tStamp + tsByte
            if DEBUG_INFO_FLAG >= 1: print tStamp
        
        if options.format == "hcitxdump_ts1" or options.format == "hcirxdump_ts1":
            tStamp = ""
            tsByte = input_obj.read(1)
            bytesLeft -= 1
            while tsByte != "-":
                tStamp = tStamp + tsByte
                tsByte = input_obj.read(1)
                bytesLeft -= 1
                
            if DEBUG_INFO_FLAG >= 1: print tStamp
            
        if options.format == "hcitxdump" or options.format == "hcitxdump_ts" or options.format == "hcitxdump_ts1":
            SnoopHdrFlags = BTSNOOP_PKT_FLAGS_DIR_HOST_TO_SOC
        elif options.format == "hcirxdump" or options.format == "hcirxdump_ts" or options.format == "hcirxdump_ts1":
            SnoopHdrFlags = BTSNOOP_PKT_FLAGS_DIR_SOC_TO_HOST
        
        SnoopHdrLen = 0
        SnoopHdrDrops = 0
        SnoopHdrTimestamp = 0
    
        isIBSPkt = 0
        PktID = input_obj.read(1)
        bytesLeft -= 1
        SnoopHdrLen += 1
        FrameCount += 1
        if ord(PktID) == PKTID_HCI_COMMAND:
            PktHdr = input_obj.read(3)
            bytesLeft -= 3
            PktLen = ord(PktHdr[2])
            PktData = input_obj.read(PktLen)
            bytesLeft -= PktLen
            SnoopHdrLen += (3 + PktLen)
            SnoopHdrFlags |= BTSNOOP_PKT_FLAGS_CMD_FLAG_CMD_EVT

        elif ord(PktID) == PKTID_ACL_DATA:
            PktHdr = input_obj.read(4)
            bytesLeft -= 4
            PktHdrStruct = struct.unpack('<2sH', PktHdr[0:4])
            PktLen = PktHdrStruct[1]
            PktData = input_obj.read(PktLen)
            bytesLeft -= PktLen
            SnoopHdrLen += (4 + PktLen)
            SnoopHdrFlags |= BTSNOOP_PKT_FLAGS_CMD_FLAG_ACL_DATA

        elif ord(PktID) == PKTID_HCI_EVENT:
            PktHdr = input_obj.read(2)
            bytesLeft -= 2
            PktLen = ord(PktHdr[1])
            PktData = input_obj.read(PktLen)
            bytesLeft -= PktLen
            SnoopHdrLen += (2 + PktLen)
            SnoopHdrFlags |= BTSNOOP_PKT_FLAGS_CMD_FLAG_CMD_EVT
        else:
            isIBSPkt = 1

        SnoopPktHdrStruct = struct.pack('>4IQ', SnoopHdrLen,SnoopHdrLen,SnoopHdrFlags,SnoopHdrDrops,SnoopHdrTimestamp)
        output_obj.write(SnoopPktHdrStruct)
        if DEBUG_INFO_FLAG >= 1: print "PktID: ", binascii.hexlify(PktID)
        output_obj.write(PktID)

        if isIBSPkt == 0:
            if DEBUG_INFO_FLAG >= 2: print "PktHdr: ", binascii.hexlify(PktHdr)
            output_obj.write(PktHdr)
            if DEBUG_INFO_FLAG >= 3: print "PktData: ", binascii.hexlify(PktData)
            output_obj.write(PktData)

    print "Raw Frames Found: ", FrameCount
    print options.outputfile , "is generated successfully."

    output_obj.close()
    input_obj.close()
    return None


#------------------------------------------------------------------------------
#   Main function which parses the input file to decide on the parsing method.
#------------------------------------------------------------------------------
def GenFWLog_Snoop():
    global filetype

    print ""
    
    (options, args) = parse_args()

    # Detect whether any raw binary data is requested to parse.
    if options.format == "hcitxdump" or options.format == "hcirxdump":
        filetype = FILE_TYPE_RAWDUMP_BINARY
        print "Input File Format Considered as : RAW Binary"
        Parse_BTRawData(options, args)
        return

    # Detect whether any raw binary data is requested to parse.
    if options.format == "hcitxdump_ts" or options.format == "hcirxdump_ts" or options.format == "hcirxdump_ts1" or options.format == "hcitxdump_ts1":
        filetype = FILE_TYPE_RAWDUMP_BINARY_TS
        print "Input File Format Considered as : RAW Binary with Timestamp"
        Parse_BTRawData(options, args)
        return

    # Detect BTSnoop file format
    # Read the BTSnoop File Header Contents and Compare to see whether it is supported file.
    input_obj = open(options.inputfile, "rb")
    SnoopFileHdr = input_obj.read(BTSNOOP_FILE_HDR_LEN)    
    input_obj.close()

    if binascii.hexlify(SnoopFileHdr) == BTSNOOP_FILE_HDR_HCI_H1_HEX :
        filetype = FILE_TYPE_BTSNOOP_HCI_H1
        print "Input File Format Detected is : BTSnoop H1"
        Parse_BtSnoop(options, args)
        return None

    if binascii.hexlify(SnoopFileHdr) == BTSNOOP_FILE_HDR_HCI_H4_HEX :
        filetype = FILE_TYPE_BTSNOOP_HCI_H4
        print "Input File Format Detected is : BTSnoop H4"
        Parse_BtSnoop(options, args)
        return None

    if binascii.hexlify(SnoopFileHdr) == BTSNOOP_FILE_HDR_HCI_H4_BLUEZ_BTMON :
        filetype = FILE_TYPE_BTSNOOP_HCI_BTMON
        print "Input File Format Detected is : Bluez btmon Snoop"
        Parse_BluezBTMonData(options, args)
        return None

        
    # Detect CPAS file format
    if  CPAS_Parser.IsCPASFormatSupported(options, args) == True :
        #Now pass the generated file to split the btsnoop files.
        print "\nStarted Spliting of QCADebug Logs ......"
        options.inputfile = (options.outputfile).split('.')[0] + '.tmp'
        Parse_BtSnoop(options, args) 
        return None
    
    if DEBUG_INFO_FLAG >= 1: print "Input File Format Type: ", filetype

    if filetype == FILE_TYPE_NOT_SUPPORTED :
        print "Input File Format is not Supported for Parsing...."
        exit(1)

    return None

GenFWLog_Snoop()