#===========================================================================
#  Copyright (c) 2017 QUALCOMM Incorporated.
#  All Rights Reserved.
#  Qualcomm Confidential and Proprietary
#===========================================================================
#
# App Core
#
# GENERAL DESCRIPTION
#    build script
#                      EDIT HISTORY FOR FILE
#
#  This section contains schedulerents describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------

#===============================================================================
import os

Import('env')

aliases = ['kmtest']

if not env.CheckAlias(aliases):
  Return()

env = env.Clone()


libs = [
  env.SConscript('${BUILD_ROOT}/apps/securemsm/trustzone/qsapps/keymaster/lib/SConscript', exports='env'),
]
#-------------------------------------------------------------------------------
# Compiler, object, and linker definitions
#-------------------------------------------------------------------------------
includes = [
  "$BUILD_ROOT/apps/securemsm/trustzone/qsapps/keymaster/kmtest/src",
  "$BUILD_ROOT/apps/securemsm/trustzone/qsapps/keymaster/lib",
  "$BUILD_ROOT/apps/securemsm/trustzone/qsapps/keymaster/km4/inc",
  '${BUILD_ROOT}/apps/securemsm/trustzone/qsapps/misc_headers',
  ]

#------------------------------------------------------------------------------
# We need to specify "neon" to generate SIMD instructions in 32-bit mode
#------------------------------------------------------------------------------
if env['PROC'] == 'scorpion':
  env.Append(CCFLAGS = " -mfpu=neon ")

#----------------------------------------------------------------------------
# App core Objects
#----------------------------------------------------------------------------

sources = [
  'app_main.c',
]

deploy_sources = []
deploy_sources.extend(sources)

#-------------------------------------------------------------------------------
# Add metadata to image
#-------------------------------------------------------------------------------
md = {
  'appName':    'kmtest',
  'privileges': ['default',
                 'KMHal',
                 ],
  'heapSize':   0x10000,
}

deploy_header_files = env.Glob('../inc/*') + env.Glob('../idl/*.idl')

Kmtestta_units = env.SecureAppBuilder(
  sources = sources,
  includes = includes,
  metadata = md,
  image = 'kmtest',
  user_libs = libs,
  deploy_sources = deploy_sources + ['SConscript'] + deploy_header_files,
  deploy_variants = env.GetDefaultPublicVariants()
)

env.Alias(aliases, Kmtestta_units)
