#ifndef __CHECK_H
#define __CHECK_H

/**
 * @file check.h
 *
 * This file declares macros for dealing with errors.  Goals include:
 *
 *   - Making it easier to write code that checks errors correctly.
 *
 *   - Improving the readability of code.
 *
 *   - Encouraging a more systematic approach to error handling.
 *
 * Be advised that these macros embed control flow, so users should
 * take care to read and understand each of these.
 *
 * A convention encouraged here is that functions return integer error
 * codes, where 0 indicates success and non-zero values indicate failure.
 * The CHECK() and GUARD() encapsulate the following patterns, and add some
 * additional functionality that is discussed below.
 *
 *    CHECK(is_okay);    -->    if (! is_okay) {
 *                                 return 1;
 *                              }
 *
 *
 *    GUARD(status);     -->    if (status != 0) {
 *                                 return status;
 *                              }
 *
 * Example:
 *
 *    ...
 *    CHECK(a < max);
 *    CHECK(b < max);
 *    GUARD(computeValue(array[a], array[b]));
 *    ...
 *
 * Meaningful Errors and Diagnostics
 *
 * There is no need to define a distinct error code for every place in the
 * code where validation occurs.  While this can help the author with "in
 * the field" diagnostics, it pollutes the interface specification with
 * implementation details that are meaningless to the caller of the API.
 * Instead, to provide even better diagnostics and to avoid such pollution,
 * we log each CHECK failure.
 *
 * CHECK() should be used for exceptional conditions: those that are not
 * encountered when all software in the system is functioning properly. For
 * example, most bounds-checking falls into this category.
 *
 * GUARD() does not log additional information.  It re-reports a previously-
 * detected error condition.  An analogy can be made to exception handling
 * in languages like C++ and Java: a CHECK failure "throws", and GUARD will
 * "re-throw".
 *
 * CHECK_LOG() logs the file and line at which CHECK was called.  In unit
 * test builds, these are printed to stdout.
 *
 * Cleanup
 *
 * Some functions allocate and free resources during a call.  In these
 * cases, a "return" in between the allocation and the free could lead to a
 * leak.  One of the following approaches are recommended:
 *
 *  1. Store the pointer to the resource in an object (as a "member
 *     variable").  This allows the resource to be freed later when the
 *     destructor ("deinit" or "free" function) is called.
 *
 *  2. Ensure that no CHECK, GUARD, or return statements occur between the
 *     allocation and the free.  For example, the code could copy out values
 *     that can then be validated after the free.
 *
 *  3. Use the "goto bail" convention.
 *
 *     Free all resources in only one place, at the bottom of the function,
 *     following a label named "bail".  At the top of the function,
 *     intialize variables that hold resource pointers to NULL.  Within the
 *     body of the function, use "goto bail" whenever an error is
 *     encountered.  Since both success and failure cases will fall through
 *     to "bail", initialize the return code to "failure".
 *
 *     Instead of CHECK() use CHECK_ELSE(..., goto bail).
 *
 *     Instead of GUARD() use `if (status) goto bail;`.
 *
 */
#include <keymaster_defs.h>
#include <km_platform_utils.h>

#define CHECK_BAIL(is_valid, code)      \
    do {\
      if (!(is_valid)) {                  \
         ret = (code);                     \
         KM_LOG_ERROR("%s:%u:%d", __func__, __LINE__, ret);               \
         goto ret_handle;                  \
      };\
    } while(0)

#define CHECK(is_valid, code)           \
       if (!(is_valid)) {               \
         ret = (code);                  \
         goto ret_handle;               \
       } else

#define CHECK_RET(is_valid, code)      \
    do {\
      if (!(is_valid)) {                  \
         return -1;                 \
      };\
    } while(0)

#define CHECK_MSG_BAIL(is_valid, code, msg)  \
    do {\
       if (!(is_valid)) {                  \
         KM_LOG_ERROR(msg);                \
         ret = (code);                     \
         goto ret_handle;                  \
       };                                  \
    } while(0)

#define GUARD_BAIL(code)                \
    do {\
       ret = (code);     \
       if (ret != 0) {                     \
          goto ret_handle;                 \
       };                                  \
    }while(0)

#define GUARD(code)                     \
    do {\
       if (code != 0) {\
          return code;           \
       };\
    } while(0)

#define GUARD_MSG_BAIL(code)            \
   do {\
      ret = (code);                   \
      if (ret) {                     \
         KM_LOG_ERROR("%s:%u:%d", __func__, __LINE__, ret);               \
         goto ret_handle;                 \
      };                                 \
   }while(0)

#define GUARD_MSG_BAIL_EXT_CODE(code, code1)            \
   do {\
      ret = (code);                   \
      if ((ret) && (ret != code1)) {                     \
         KM_LOG_ERROR("%s:%u:%d", __func__, __LINE__, ret);               \
         goto ret_handle;                 \
      };                                 \
   }while(0)

#define GUARD_MSG_BAIL_EXP_VAL(expectedval,\
      val, code)            \
   do {\
      ret = val;\
      if ((ret) != expectedval) {                     \
         KM_LOG_ERROR("%s:%u:%d", __func__, __LINE__, ret);               \
         ret = code;\
         goto ret_handle;                 \
      };                                 \
   }while(0)

#define VERIFY_SERIALIZED_HEADER(cmdlen, size, cmdlenRem) \
   do {\
      if((cmdlen) < (size)) { \
         ret = KM_ERROR_INSUFFICIENT_BUFFER_SPACE; \
         goto ret_handle; \
      } \
      (cmdlenRem) -= (size); \
   }while(0)

#define ALLOCATE(bufTo, size) \
   do {\
      void *allocTemp = km_malloc((size)); \
      CHECK_BAIL((allocTemp), KM_ERROR_MEMORY_ALLOCATION_FAILED); \
      (bufTo.data_length) = (size); \
      bufTo.data = allocTemp; \
   }while(0)

#define ALLOCATE_AND_COPY(bufTo, bufFrom) \
   do {\
      void *allocTemp = km_malloc((bufFrom.data_length)); \
      CHECK_BAIL((allocTemp), KM_ERROR_MEMORY_ALLOCATION_FAILED); \
      ((bufTo).data_length) = (bufFrom.data_length); \
      memscpy((allocTemp), ((bufTo).data_length), (bufFrom.data), (bufFrom.data_length)); \
      (bufTo).data = allocTemp; \
   }while(0)

#define ALLOCATE_AND_COPY_KEYBLOB(bufTo, bufFrom) \
   do {\
      void *allocTemp = km_malloc((bufFrom.key_material_size)); \
      CHECK_BAIL((allocTemp), KM_ERROR_MEMORY_ALLOCATION_FAILED); \
      (bufTo.key_material_size) = (bufFrom.key_material_size); \
      memscpy((allocTemp), (bufTo.key_material_size), (bufFrom.key_material), (bufFrom.key_material_size)); \
      bufTo.key_material = allocTemp; \
   }while(0)

#define ALLOW_KEY_MAX_OPS_EXCEEDED(code, msg)\
   do {\
      ret = (code);                   \
      if (ret &&                          \
         ret != KM_ERROR_KEY_MAX_OPS_EXCEEDED) { \
         KM_LOG_ERROR(msg);               \
         KM_LOG_ERROR("ret: %d", code);   \
         goto ret_handle;                 \
      };                                 \
   }while(0)

#define INITIALIZE_BBUF(ptr, minOffset, maxOffset) \
   BBuf serializedBlob = {0}; \
   serializedBlob.baseAddr = ptr; \
   serializedBlob.minIndirectDataOffset = minOffset; \
   serializedBlob.maxIndirectDataOffset = maxOffset; \

#define VALIDATE_OUTPUT_OUTPUT_LEN(output, output_length, keySize, code) \
   do { \
      if (((output_length) < (keySize)) || (!output)) {\
         ret = (code);                   \
         if (ret) {                     \
            KM_LOG_ERROR("%s:%u:%d", __func__, __LINE__, ret);               \
            goto ret_handle;                 \
         }; \
      }\
   }while(0)

#define BUFFER_SPACE_AVAILABLE(a, b)             \
   do {\
     if(KEYMASTER_SIMPLE_COMPARE(a,b) <= 0)         \
        return KM_ERROR_INSUFFICIENT_BUFFER_SPACE;   \
   } while(0)

#define CHECK_INTEGER_OVERFLOW(a,b) \
        if((uint32_t)a > (UINT_MAX - (uint32_t)b)) {\
                KM_LOG_ERROR("Integer Overflow: a=%u, b=%u", (uint32_t)a, (uint32_t)b); \
                ret = KM_ERROR_INSUFFICIENT_BUFFER_SPACE; \
                goto ret_handle; \
        }

#define CHECK_INTEGER_OVERFLOW64(a,b) \
        if((uint64_t)a > (KM_UINT64_MAX - (uint64_t)b)) {\
                KM_LOG_ERROR("Integer Overflow: a=%llu, b=%llu", (uint64_t)a, (uint64_t)b); \
                ret = KM_ERROR_INSUFFICIENT_BUFFER_SPACE; \
                goto ret_handle; \
        }

#define CHECK_INTEGER_UNDERFLOW(a,b) \
        if((uint32_t)a < (uint32_t)b) {\
                KM_LOG_ERROR("Integer Underflow: a=%u, b=%u", (uint32_t)a, (uint32_t)b); \
                ret = KM_ERROR_INSUFFICIENT_BUFFER_SPACE; \
                goto ret_handle; \
        }

#define CHECK_INTEGER_UNDERFLOW64(a,b) \
        if((uint64_t)a < (uint64_t)b) {\
                KM_LOG_ERROR("Integer Underflow: a=%u, b=%u", (uint64_t)a, (uint64_t)b); \
                ret = KM_ERROR_INSUFFICIENT_BUFFER_SPACE; \
                goto ret_handle; \
        }

#define CHECK_INTEGER_OVERFLOW_MULT(a,b) \
                if ((uint32_t)b && ((uint32_t)a > UINT_MAX / (uint32_t)b)) {\
                        KM_LOG_ERROR("Integer Overflow: a=%u, b=%u", (uint32_t)a, (uint32_t)b); \
                        ret = KM_ERROR_INSUFFICIENT_BUFFER_SPACE; \
                        goto ret_handle; \
                }

#define CHECK_OP(val1, val2, code, op)                          \
    do {                                                                \
        if ((val1) op (val2)) {                                         \
        } else {                                                        \
            KM_LOG_ERROR("%s:%u: 0x%x %s 0x%x", __func__, __LINE__, (val1), #op, (val2));\
            ret = (code);                                            \
            goto ret_handle;                                               \
        }                                                               \
    } while (0)

#define CHECK_OP_EXT_VAL(val1, val2, val3, code, op)                          \
    do {                                                                \
        if (((val1) op (val2)) && ((val1) op (val3))){                                         \
        } else {                                                        \
            KM_LOG_ERROR("%s:%u: 0x%x %s 0x%x 0x%x", __func__, __LINE__, (val1), #op, (val2), (val3));\
            ret = (code);                                            \
            goto ret_handle;                                               \
        }                                                               \
    } while (0)

#define CHECK_EQ(val1, val2, code) CHECK_OP(val1, val2, code, ==)

#define CHECK_NE(val1, val2, code)                                      \
    CHECK_OP(val1, val2, code, !=)

#define CHECK_NE_EXT_VAL(val1, val2, val3, code)                                      \
    CHECK_OP(val1, val2, code, !=)

#define CHECK_LT(val1, val2, code)                                      \
    CHECK_OP(val1, val2, code, <)

#define CHECK_GT(val1, val2, code)                                      \
    CHECK_OP(val1, val2, code, >)

#define CHECK_LE(val1, val2, code)                                      \
    CHECK_OP(val1, val2, code, <=)

#define CHECK_GE(val1, val2, code)                                      \
    CHECK_OP(val1, val2, code, >=)

#endif // __CHECK_H
