/* =================================================================================
 *  Copyright (c) 2018 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
 * =================================================================================
 */

#ifndef KEYMASTER_KM4_INC_KM_HEAP_H_
#define KEYMASTER_KM4_INC_KM_HEAP_H_

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <qsee_log.h>

void* km_malloc(size_t size);
void km_free(void *ptr);

#define HEAP_ZALLOC_TYPE(ty)      ((ty *) km_malloc(sizeof(ty)))
#define HEAP_FREE_PTR(var)        ((void) (km_free(var), (var) = 0))
#define HEAP_FREE_PTR_MEMSET(var, ty)   \
   do {\
      if(var) { \
         secure_memset(var, 0, sizeof(ty)); \
         ((void) (km_free(var), (var) = 0)); \
      } \
   }while(0)

#define HEAP_FREE_PTR_MEMSET_SIZE(var, size)   \
   do {\
      if(var) { \
         secure_memset(var, 0, size); \
         ((void) (km_free(var), (var) = 0)); \
      } \
   }while(0)

#define HEAP_UTIL_FREE_PTR_MEMSET(var, ty)   \
   do {\
      if(var) { \
         secure_memset(var, 0, sizeof(ty)); \
         ((void) (qsee_util_free_s_bigint((ty *)var), (var) = 0)); \
      } \
   }while(0)


#endif /* KEYMASTER_KM4_INC_KM_HEAP_H_ */
