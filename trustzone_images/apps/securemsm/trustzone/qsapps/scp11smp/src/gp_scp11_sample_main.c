/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
                           <gp_scp11_sample_main.c>
  DESCRIPTION
    Global Platform TA to act as a test and sample OCE for SCP11.

  Copyright (c) 2016-2018 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cdefs.h"
#include "tee_internal_api.h"
#include "tee_internal_se_api.h"
#include "tee_internal_se_api_ext.h"
#include "gpTypes.h"
#include "APDU.h"
#include "BER.h"
#include "CryptoUtils.h"
#include "qsee_log.h"

/**
 *  @file
 *  GP TA to demonstrate use of SCP11 in SEAPI.
 *
 *  @anchor SCP11STACommands
 *  <table>
 *    <tr> <th colspan=2> TA commands and parameters </th> </tr>
 *    <tr> <th> Command ID </th> <th> Description, with parameter types and values </th> </tr>
 *    <tr>
 *      <td align="center"> 1 </td>
 *      <td>
 *        <pre><i>Start SCP11 Session</i>
 *        <b>Param 1:</b> <i>Memref Input  </i> - Target AID, SD AID TLV Array
 *        <b>Param 2:</b> <i>Value Input   </i> - ECC Curve + KVN
 *        <b>Param 3:</b> <i>Value Input   </i> - SCP11 Type + Security Level
 *        <b>Param 4:</b> <i>Memref Input  </i> - PK.OCE.ECKA (if SCP11a) </pre>
 *      </td>
 *    </tr>
 *    <tr>
 *      <td align="center"> 2 </td>
 *      <td>
 *        <pre><i>Attach to Shareable SCP11 Session</i>
 *        <b>Param 1:</b> <i>Memref Input  </i> - eSE applet AID (NOT Global Service)
 *        <b>Param 2:</b> <i>Value Input   </i> - SCP11 Type + Security Level
 *        <b>Param 3:</b> <i>Not Used      </i>
 *        <b>Param 4:</b> <i>Not Used      </i> </pre>
 *      </td>
 *    </tr>
 *    <tr>
 *      <td align="center"> 3 </td>
 *      <td>
 *        <pre><i>End SCP11 Session</i>
 *        <b>Param 1:</b> <i>Not Used      </i>
 *        <b>Param 2:</b> <i>Not Used      </i>
 *        <b>Param 3:</b> <i>Not Used      </i>
 *        <b>Param 4:</b> <i>Not Used      </i> </pre>
 *      </td>
 *    </tr>
 *    <tr>
 *      <td align="center"> 4 </td>
 *      <td>
 *        <pre><i>Exchange APDUs</i>
 *        <b>Param 1:</b> <i>Memref Input  </i> - C-APDU
 *        <b>Param 2:</b> <i>Memref Output </i> - R-APDU buffer
 *        <b>Param 3:</b> <i>Not Used      </i>
 *        <b>Param 4:</b> <i>Not Used      </i> </pre>
 *      </td>
 *    </tr>
 *  </table>
 *
 *  TAs associated with SCP11 use the following UUIDs:
 *  @anchor SCP11STAUUIDs
 *  <table>
 *    <tr> <th colspan=2> UUIDs for GP SCP11 TAs </th>                                        </tr>
 *    <tr> <th> UUID </th>                                <th> TA Description </th>           </tr>
 *    <tr> <td> 7EDC14D9-0E89-45FB-AEBE26A94F8B5362 </td> <td> Crypto Services TA       </td> </tr>
 *    <tr> <td> 80D38030-92B6-4DA1-AB3D042F183F7276 </td> <td> Key Provisioning TA      </td> </tr>
 *    <tr> <td> A6891849-E466-49AB-83A3E74CCA7D931E </td> <td> Sample Host TA (this TA) </td> </tr>
 *  </table>
 *
 *  This code references the following standards:
 *  @anchor SCP11SRefs
 *  <table>
 *    <tr> <th colspan=2> References </th> </tr>
 *    <tr> <td> 1 </td> <td> GlobalPlatform TEE Internal Core API Specification, v1.1.2        </td> </tr>
 *    <tr> <td> 2 </td> <td> GlobalPlatform TEE Secure Element API Specification, v1.1.1       </td> </tr>
 *    <tr> <td> 3 </td> <td> GlobalPlatform Card Specification, v2.3                           </td> </tr>
 *    <tr> <td> 4 </td> <td> GlobalPlatform Card Specification, Amendment D - SCP03, v1.1.1    </td> </tr>
 *    <tr> <td> 5 </td> <td> GlobalPlatform Card Specification, Amendment E - v1.1             </td> </tr>
 *    <tr> <td> 6 </td> <td> GlobalPlatform Card Specification, Amendment F - SCP11, v1.1      </td> </tr>
 *    <tr> <td> 7 </td> <td> TR-03111 - Elliptic Curve Cryptography, v2.0                      </td> </tr>
 *  </table>
 */

/*==========================================================================*
 * Debug macros                                                             *
 *==========================================================================*/
/* What do we want to log... */
#ifdef REX_WINDOWS_SIMULATION
  /* Running on Windows/PicoC harness */
  #define TA_LOG_MASK                           ( QSEE_LOG_MSG_LOW   | \
                                                  QSEE_LOG_MSG_MED   | \
                                                  QSEE_LOG_MSG_HIGH  | \
                                                  QSEE_LOG_MSG_DEBUG | \
                                                  QSEE_LOG_MSG_ERROR | \
                                                  QSEE_LOG_MSG_FATAL )
  #define SCP11S_DEBUGDUMP(p, l)                DebugDump((p), (l))
  #define SCP11S_TA_REF_COUNT
#else
  /* Running on ARM/TZ target */
  //#define TA_LOG_MASK                           0
  #define TA_LOG_MASK                           ( QSEE_LOG_MSG_LOW   | \
                                                  QSEE_LOG_MSG_MED   | \
                                                  QSEE_LOG_MSG_HIGH  | \
                                                  QSEE_LOG_MSG_DEBUG | \
                                                  QSEE_LOG_MSG_ERROR | \
                                                  QSEE_LOG_MSG_FATAL )
  #define SCP11S_DEBUGDUMP(p, l)                /* DebugDump((p), (l)) */
  #undef  SCP11S_TA_REF_COUNT
#endif

#define SCP11S_ENTER(xx_fmt, ...)               qsee_log(QSEE_LOG_MSG_DEBUG, "+%s%s" xx_fmt, __FUNCTION__, " ", ##__VA_ARGS__)
#define SCP11S_EXIT(xx_fmt, ...)                qsee_log(QSEE_LOG_MSG_DEBUG, "-%s%s" xx_fmt, __FUNCTION__, " ", ##__VA_ARGS__)
#define SCP11S_DEBUGMSG(xx_fmt, ...)            qsee_log(QSEE_LOG_MSG_DEBUG, " %s%s" xx_fmt, __FUNCTION__, " ", ##__VA_ARGS__)
#define SCP11S_ERRORMSG(xx_fmt, ...)            qsee_log(QSEE_LOG_MSG_ERROR, " %s%s" xx_fmt, __FUNCTION__, " ", ##__VA_ARGS__)

#ifdef SCP11S_TA_REF_COUNT
  static uint32_t taReferenceCount = 0;
  #define SCP11S_INC_TA_REF_COUNT()             (++taReferenceCount)
  #define SCP11S_DEC_TA_REF_COUNT()             (--taReferenceCount)
  #define SCP11S_NO_MORE_REFERENCES()           (taReferenceCount == 0)
#else
  #define SCP11S_INC_TA_REF_COUNT()             /* */
  #define SCP11S_DEC_TA_REF_COUNT()             /* */
  #define SCP11S_NO_MORE_REFERENCES()           (1)
#endif /* SCP11S_TA_REF_COUNT */

/*==========================================================================*
 * Type definitions                                                         *
 *==========================================================================*/
/* Command ID values for TA_InvokeCommandEntryPoint() */
#define TA_CMD_START_SESSION                    1
#define TA_CMD_ATTACH_SHARED_SESSION            2
#define TA_CMD_END_SESSION                      3
#define TA_CMD_EXCHANGE_APDUS                   4

/* TA Session context */
#define TA_CONTEXT_MAGIC                        0x41544F48

/** @brief Maximum supported ECC key sizes */
#define MAX_ECC_KEY_BITS                        521
#define MAX_ECC_KEY_BYTES                       TEE_BITS2BYTES(MAX_ECC_KEY_BITS)
#define MAX_ECC_PRIVATE_KEY_BYTES               MAX_ECC_KEY_BYTES
#define MAX_ECC_PUBLIC_KEY_BYTES                ECCSK2PK(MAX_ECC_PRIVATE_KEY_BYTES)

/** @brief Limits on AID length */
#define MIN_AID_BYTES                           5
#define MAX_AID_BYTES                           16

/** @brief Limits on CAPDU size */
#define MIN_CAPDU_BYTES                         4
#define MAX_CAPDU_BYTES                         65535

/* SCP information */
typedef struct
{
  uint32_t            aidLength1;
  uint8_t             aid1[MAX_AID_BYTES];
  uint32_t            aidLength2;
  uint8_t             aid2[MAX_AID_BYTES];
  uint32_t            aidLengthSecDomain;
  uint8_t             aidSecDomain[MAX_AID_BYTES];

  uint32_t            pkOceEckaLength;
  uint8_t             pkOceEcka[MAX_ECC_PUBLIC_KEY_BYTES];

  bool                useLogicalChannel;
  uint8_t             cardKeyID;
  uint8_t             cardKeyVersion;
  uint32_t            eccCurve;
  uint32_t            scp11Type;
  uint32_t            securityLevel;

  TEE_SEServiceHandle seService;
  TEE_SEReaderHandle  seReader;
  TEE_SESessionHandle seSession;
  TEE_SEChannelHandle seChannel1;
  TEE_SEChannelHandle seChannel2;

  TEE_ObjectHandle    keyObject;

} SCPINFO;


typedef struct
{
  uint32_t magic;
  SCPINFO  scpInfo;
} TASESSIONCONTEXT;

/* Tag values for TLVs from HLOS */
typedef enum
{
  /* Application ID values */
  TAG_OCE_AIDSECDOMAIN        = 0x01,
  TAG_OCE_AIDTARGET           = 0x02

} TAG_OCE_ITEM;

/*==========================================================================*
 * SCPInit                                                                  *
 *==========================================================================*/
static TEE_Result SCPInit(SCPINFO *pSCPInfo)
{
  TEE_Result     teeResult   = TEE_ERROR_BAD_PARAMETERS;

  const uint8_t  oidSCP11a[] = TEE_OID_SCP11a;
  const uint8_t  oidSCP11b[] = TEE_OID_SCP11b;

  const uint8_t *pOID        = NULL;
  size_t         oidSize     = 0;

  teeResult = TEE_SEServiceOpen(&pSCPInfo->seService);

  if (teeResult == TEE_SUCCESS)
  {
    TEE_SEReaderHandle seReaders[] = { 0, 0, 0, 0, 0 };
    uint32_t           maxReaders  = C_LENGTHOF(seReaders);

    teeResult = TEE_SEServiceGetReaders(pSCPInfo->seService, &seReaders[0], &maxReaders);
    if (teeResult == TEE_SUCCESS)
      pSCPInfo->seReader = seReaders[0];
  }

  if (teeResult == TEE_SUCCESS)
    teeResult = TEE_SEReaderOpenSession(pSCPInfo->seReader, &pSCPInfo->seSession);

  if ((teeResult == TEE_SUCCESS) && (pSCPInfo->aidLengthSecDomain != 0))
  {
    TEE_SEAID teeAIDSD;
    teeAIDSD.buffer    = &pSCPInfo->aidSecDomain[0];
    teeAIDSD.bufferLen = pSCPInfo->aidLengthSecDomain;

    if (pSCPInfo->useLogicalChannel)
      teeResult = TEE_SESessionOpenLogicalChannel(pSCPInfo->seSession, &teeAIDSD, &pSCPInfo->seChannel1);
    else
      teeResult = TEE_SESessionOpenBasicChannel(pSCPInfo->seSession, &teeAIDSD, &pSCPInfo->seChannel1);

    SCP11S_DEBUGMSG("SE Service Handle   (SD) : 0x%08X", pSCPInfo->seService);
    SCP11S_DEBUGMSG("SE Reader Handle    (SD) : 0x%08X", pSCPInfo->seReader);
    SCP11S_DEBUGMSG("SE Session Handle   (SD) : 0x%08X", pSCPInfo->seSession);
    SCP11S_DEBUGMSG("SE Channel Handle 1 (SD) : 0x%08X", pSCPInfo->seChannel1);
    SCP11S_DEBUGMSG("SE Channel Handle 2 (SD) : 0x%08X", pSCPInfo->seChannel2);

    if (teeResult == TEE_SUCCESS)
    {
      TEE_SEAID aidTarget;
      aidTarget.buffer    = &pSCPInfo->aid1[0];
      aidTarget.bufferLen = pSCPInfo->aidLength1;

      teeResult = TEE_SESecureChannelGetSdCertificate_ext(pSCPInfo->seChannel1,
                                                          &aidTarget,
                                                          pSCPInfo->scp11Type,
                                                          pSCPInfo->cardKeyVersion,
                                                          &pSCPInfo->pkOceEcka[0],
                                                          pSCPInfo->pkOceEckaLength);
    }

    TEE_SEChannelClose(pSCPInfo->seChannel1);
    pSCPInfo->seChannel1 = TEE_HANDLE_NULL;
  }

  if (teeResult == TEE_SUCCESS)
  {
    TEE_SEAID teeAID;
    teeAID.buffer    = &pSCPInfo->aid1[0];
    teeAID.bufferLen = pSCPInfo->aidLength1;

    if (pSCPInfo->useLogicalChannel)
      teeResult = TEE_SESessionOpenLogicalChannel(pSCPInfo->seSession, &teeAID, &pSCPInfo->seChannel1);
    else
      teeResult = TEE_SESessionOpenBasicChannel(pSCPInfo->seSession, &teeAID, &pSCPInfo->seChannel1);
  }

  SCP11S_DEBUGMSG("SE Service Handle   (Tgt) : 0x%08X", pSCPInfo->seService);
  SCP11S_DEBUGMSG("SE Reader Handle    (Tgt) : 0x%08X", pSCPInfo->seReader);
  SCP11S_DEBUGMSG("SE Session Handle   (Tgt) : 0x%08X", pSCPInfo->seSession);
  SCP11S_DEBUGMSG("SE Channel Handle 1 (Tgt) : 0x%08X", pSCPInfo->seChannel1);
  SCP11S_DEBUGMSG("SE Channel Handle 2 (Tgt) : 0x%08X", pSCPInfo->seChannel2);

  if (teeResult == TEE_SUCCESS)
  {
    TEE_SC_Params scParams = { 0 };

    pSCPInfo->keyObject = TEE_HANDLE_NULL;

    switch (pSCPInfo->scp11Type)
    {
      case TEE_SC_TYPE_SCP11a:  SCP11S_ERRORMSG("Standard SCP11a");
                                pSCPInfo->keyObject = CreateECCKeyObject(pSCPInfo->eccCurve,
                                                                         TEE_TYPE_ECDH_PUBLIC_KEY,
                                                                         pSCPInfo->pkOceEcka,
                                                                         pSCPInfo->pkOceEckaLength,
                                                                         NULL, 0, false);
                                if (pSCPInfo->keyObject == TEE_HANDLE_NULL)
                                {
                                  teeResult = TEE_ERROR_BAD_PARAMETERS;
                                  goto GetOut;
                                }
                                pOID = &oidSCP11a[0];
                                oidSize = sizeof(oidSCP11a);
                                break;

      case TEE_SC_TYPE_SCP11b:  SCP11S_DEBUGMSG("Standard SCP11b");
                                pOID = &oidSCP11b[0];
                                oidSize = sizeof(oidSCP11b);
                                break;

      default:                  SCP11S_ERRORMSG("Unrecognised SCP type (0x%08X)", pSCPInfo->scp11Type);
                                teeResult = TEE_ERROR_BAD_PARAMETERS;
                                goto GetOut;
    }

    scParams.scType                                   = pSCPInfo->scp11Type;
    scParams.scOID.buffer                             = (uint8_t *) pOID;
    scParams.scOID.bufferLen                          = oidSize;
    scParams.scSecurityLevel                          = pSCPInfo->securityLevel;
    scParams.scCardKeyRef.scKeyID                     = pSCPInfo->cardKeyID;
    scParams.scCardKeyRef.scKeyVersion                = pSCPInfo->cardKeyVersion;
    scParams.scDeviceKeyRef.scKeyType                 = TEE_SC_BASE_KEY;
    scParams.scDeviceKeyRef.__TEE_key.scBaseKeyHandle = pSCPInfo->keyObject;

    teeResult = TEE_SESecureChannelOpen(pSCPInfo->seChannel1, &scParams);
  }

  if (teeResult == TEE_SUCCESS)
    pSCPInfo->seChannel2 = pSCPInfo->seChannel1;

GetOut:
  SCP11S_DEBUGMSG("SCPInit() result: 0x%08X", teeResult);
  return teeResult;
}

/*==========================================================================*
 * SCPAttach                                                                *
 *==========================================================================*/
static TEE_Result SCPAttach(SCPINFO *pSCPInfo)
{
  TEE_Result teeResult = TEE_ERROR_BAD_PARAMETERS;
  TEE_SEAID  teeAID;

  teeAID.buffer    = &pSCPInfo->aid2[0];
  teeAID.bufferLen = pSCPInfo->aidLength2;

  if (pSCPInfo->useLogicalChannel)
    teeResult = TEE_SESessionOpenLogicalChannel(pSCPInfo->seSession, &teeAID, &pSCPInfo->seChannel2);
  else
    teeResult = TEE_SESessionOpenBasicChannel(pSCPInfo->seSession, &teeAID, &pSCPInfo->seChannel2);

  SCP11S_DEBUGMSG("SE Service Handle   : 0x%08X", pSCPInfo->seService);
  SCP11S_DEBUGMSG("SE Reader Handle    : 0x%08X", pSCPInfo->seReader);
  SCP11S_DEBUGMSG("SE Session Handle   : 0x%08X", pSCPInfo->seSession);
  SCP11S_DEBUGMSG("SE Channel Handle 1 : 0x%08X", pSCPInfo->seChannel1);
  SCP11S_DEBUGMSG("SE Channel Handle 2 : 0x%08X", pSCPInfo->seChannel2);

  if (teeResult == TEE_SUCCESS)
  {
    teeResult = TEE_SESecureChannelAttachShared_ext(pSCPInfo->seChannel2,
                                                    (uint8_t) pSCPInfo->scp11Type,
                                                    (TEE_SC_SecurityLevel) pSCPInfo->securityLevel);
  }

//GetOut:
  SCP11S_DEBUGMSG("SCPAttach() result: 0x%08X", teeResult);
  return teeResult;
}

/*==========================================================================*
 * SCPClose                                                                 *
 *==========================================================================*/
static void SCPClose(SCPINFO *pSCPInfo)
{
  if ((pSCPInfo->seChannel2 != NULL) && (pSCPInfo->seChannel2 != pSCPInfo->seChannel1))
  {
    TEE_SESecureChannelClose(pSCPInfo->seChannel2);
    TEE_SEChannelClose(pSCPInfo->seChannel2);
  }

  if (pSCPInfo->seChannel1 != NULL)
  {
    TEE_SESecureChannelClose(pSCPInfo->seChannel1);
    TEE_SEChannelClose(pSCPInfo->seChannel1);
  }

  if (pSCPInfo->seSession != NULL)
    TEE_SESessionClose(pSCPInfo->seSession);

  if (pSCPInfo->seService != NULL)
    TEE_SEServiceClose(pSCPInfo->seService);

  if (pSCPInfo->keyObject != TEE_HANDLE_NULL)
    TEE_FreeTransientObject(pSCPInfo->keyObject);

  pSCPInfo->seService  = NULL;
  pSCPInfo->seReader   = NULL;
  pSCPInfo->seSession  = NULL;
  pSCPInfo->seChannel1 = NULL;
  pSCPInfo->seChannel2 = NULL;
  pSCPInfo->keyObject  = TEE_HANDLE_NULL;
}

/*==========================================================================*
 * Send TEST C-APDU                                                         *
 *==========================================================================*/
static TEE_Result SCP11Send(SCPINFO       *pSCPInfo,
                            const uint8_t *pCAPDU,
                            uint32_t       capduLength,
                            uint8_t       *pRAPDU,
                            uint32_t      *pRAPDULength)
{
  TEE_Result  teeResult       = TEE_ERROR_BAD_PARAMETERS;
  uint8_t    *pCAPDUBuffer    = NULL;
  uint32_t    capduBufferSize = 0;
  uint8_t    *pRAPDUBuffer    = NULL;
  uint32_t    rapduBufferSize = *pRAPDULength;

  SCP11S_ENTER("");

  if ((capduLength < MIN_CAPDU_BYTES) || (capduLength > MAX_CAPDU_BYTES))
  {
    SCP11S_ERRORMSG("C-APDU size invalid");
    goto GetOut;
  }

  capduBufferSize = capduLength + 128;
  pCAPDUBuffer    = TEE_Malloc(capduBufferSize, TEE_MALLOC_FILL_ZERO);
  pRAPDUBuffer    = TEE_Malloc(rapduBufferSize, TEE_MALLOC_FILL_ZERO);
  if ((pCAPDUBuffer == NULL) || (pRAPDUBuffer == NULL))
  {
    SCP11S_ERRORMSG("Unable to allocate send and/or receive buffer");
    teeResult = TEE_ERROR_OUT_OF_MEMORY;
    goto GetOut;
  }

  TEE_MemMove(pCAPDUBuffer, c_const_cast(void *, pCAPDU), capduLength);
  SCP11S_DEBUGMSG("C-APDU (%d bytes):", capduLength);
  SCP11S_DEBUGDUMP(pCAPDUBuffer, capduLength);

  teeResult = TEE_SEChannelTransmit(pSCPInfo->seChannel2,
                                    pCAPDUBuffer, capduLength,
                                    pRAPDUBuffer, pRAPDULength);
  if (teeResult != TEE_SUCCESS)
  {
    SCP11S_ERRORMSG("TEE_SEChannelTransmit() returned 0x%08X", teeResult);
    *pRAPDULength = 0;
    goto GetOut;
  }

  TEE_MemMove(pRAPDU, pRAPDUBuffer, *pRAPDULength);
  SCP11S_DEBUGMSG("R-APDU (%d bytes):", *pRAPDULength);
  SCP11S_DEBUGDUMP(pRAPDU, *pRAPDULength);

GetOut:
  if (pCAPDUBuffer != NULL) TEE_Free(pCAPDUBuffer);
  if (pRAPDUBuffer != NULL) TEE_Free(pRAPDUBuffer);
  SCP11S_EXIT("0x%08X", teeResult);
  return teeResult;
}

/*==========================================================================*
 * Process TA_CMD_START_SESSION                                             *
 *==========================================================================*/
static TEE_Result TestOCEStartSession(TASESSIONCONTEXT *pContext,
                                      uint32_t          paramTypes,
                                      TEE_Param        *params)
{
  TEE_Result      teeResult          = TEE_ERROR_BAD_PARAMETERS;
  const uint32_t  expectedParamTypes = TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_INPUT,
                                                       TEE_PARAM_TYPE_VALUE_INPUT,
                                                       TEE_PARAM_TYPE_VALUE_INPUT,
                                                       TEE_PARAM_TYPE_MEMREF_INPUT);
  const uint8_t  *pParam            = NULL;
  size_t          paramSize         = 0;
  const uint8_t  *pAIDTarget        = NULL;
  size_t          aidTargetSize     = 0;
  const uint8_t  *pAIDSecDomain     = NULL;
  size_t          aidSecDomainSize  = 0;

  SCP11S_ENTER("");

  /* Check we have the parameters we expect */
  if ((paramTypes != expectedParamTypes) || (params == NULL) || (params[0].memref.buffer == NULL))
  {
    SCP11S_ERRORMSG("Invalid parameters, type 0x%08X", paramTypes);
    goto GetOut;
  }

  /* Extract the parameters into something more readable, and validate */
  pParam    = params[0].memref.buffer;
  paramSize = params[0].memref.size;
  while ((pParam != NULL) && (paramSize != 0))
  {
    BERTag          itemTag;
    BERLength       itemLength;
    const BEROctet *pItemValue;

    pParam = BERGetTLV(pParam, &paramSize, &itemTag, &itemLength, &pItemValue);
    if (pParam != NULL)
    {
      switch (itemTag)
      {
        case TAG_OCE_AIDSECDOMAIN:  pAIDSecDomain    = pItemValue;
                                    aidSecDomainSize = itemLength;
                                    break;
        case TAG_OCE_AIDTARGET:     pAIDTarget       = pItemValue;
                                    aidTargetSize    = itemLength;
                                    break;
        default:                    SCP11S_ERRORMSG("Unknown tag in parameter 1 0x%04lX", itemTag);
                                    goto GetOut;
      }
    }
  }

  if (aidTargetSize > sizeof(pContext->scpInfo.aid1))
  {
    SCP11S_ERRORMSG("SCP Target AID too long (%d/%d)", aidTargetSize, sizeof(pContext->scpInfo.aid1));
    goto GetOut;
  }
  if (aidSecDomainSize > sizeof(pContext->scpInfo.aidSecDomain))
  {
    SCP11S_ERRORMSG("SCP Security Domain AID too long (%d/%d)", aidSecDomainSize, sizeof(pContext->scpInfo.aidSecDomain));
    goto GetOut;
  }

  /* Extract the test parameters to a more meaningful form */
  TEE_MemFill(&pContext->scpInfo, 0, sizeof(pContext->scpInfo));
  pContext->scpInfo.useLogicalChannel = true;
  pContext->scpInfo.scp11Type         = params[2].value.a;
  pContext->scpInfo.securityLevel     = params[2].value.b;
  pContext->scpInfo.eccCurve          = params[1].value.a;
  pContext->scpInfo.cardKeyVersion    = params[1].value.b;
  pContext->scpInfo.cardKeyID         = (pContext->scpInfo.scp11Type == TEE_SC_TYPE_SCP11a) ? TEE_KID_SCP11a_SKSDECKA
                                                                                            : TEE_KID_SCP11b_SKSDECKA;

  if ((pAIDTarget != NULL) && (aidTargetSize != 0))
  {
    pContext->scpInfo.aidLength1 = (uint32_t) TEE_MemSMove_ext(&pContext->scpInfo.aid1[0],
                                                               sizeof(pContext->scpInfo.aid1),
                                                               pAIDTarget,
                                                               aidTargetSize);
  }

  if ((pAIDSecDomain != NULL) && (aidSecDomainSize != 0))
  {
    pContext->scpInfo.aidLengthSecDomain = (uint32_t) TEE_MemSMove_ext(&pContext->scpInfo.aidSecDomain[0],
                                                                       sizeof(pContext->scpInfo.aidSecDomain),
                                                                       pAIDSecDomain,
                                                                       aidSecDomainSize);
  }

  /* PK.OCE.ECKA is applicable only to SCP11a */
  if (pContext->scpInfo.scp11Type == TEE_SC_TYPE_SCP11a)
  {
    if ((params[3].memref.buffer == NULL) ||
        (params[3].memref.size == 0)      ||
        (params[3].memref.size > sizeof(pContext->scpInfo.pkOceEcka)))
    {
      SCP11S_ERRORMSG("PK.OCE.ECKA missing or too long");
      goto GetOut;
    }
    else
    {
      pContext->scpInfo.pkOceEckaLength = (uint32_t) TEE_MemSMove_ext(&pContext->scpInfo.pkOceEcka[0],
                                                                      sizeof(pContext->scpInfo.pkOceEcka),
                                                                      params[3].memref.buffer,
                                                                      params[3].memref.size);
    }
  }

  SCP11S_DEBUGMSG("ECC Curve       : 0x%08X", pContext->scpInfo.eccCurve);
  SCP11S_DEBUGMSG("SCP11 type      : 0x%08X", pContext->scpInfo.scp11Type);
  SCP11S_DEBUGMSG("Security level  : 0x%08X", pContext->scpInfo.securityLevel);
  SCP11S_DEBUGMSG("Logical Channel : %s",     (pContext->scpInfo.useLogicalChannel ? "Yes" : "No"));

  SCP11S_DEBUGMSG("SCP11 Target AID 1 (%d bytes):", pContext->scpInfo.aidLength1);
  SCP11S_DEBUGDUMP(&pContext->scpInfo.aid1[0], pContext->scpInfo.aidLength1);

  SCP11S_DEBUGMSG("SCP11 Security Domain AID (%d bytes):", pContext->scpInfo.aidLengthSecDomain);
  SCP11S_DEBUGDUMP(&pContext->scpInfo.aidSecDomain[0], pContext->scpInfo.aidLengthSecDomain);

  if (pContext->scpInfo.pkOceEckaLength == 0)
    SCP11S_DEBUGMSG("PK.OCE.ECKA not given");
  else
  {
    SCP11S_DEBUGMSG("PK.OCE.ECKA (%d bytes):", pContext->scpInfo.pkOceEckaLength);
    SCP11S_DEBUGDUMP(&pContext->scpInfo.pkOceEcka[0], pContext->scpInfo.pkOceEckaLength);
  }

  /* Start SCP */
  teeResult = SCPInit(&pContext->scpInfo);
  if (teeResult != TEE_SUCCESS)
  {
    SCP11S_ERRORMSG("Failed to start SCP session");
    goto GetOut;
  }

GetOut:
  if (teeResult != TEE_SUCCESS)
  {
    SCPClose(&pContext->scpInfo);
    TEE_MemFill(&pContext->scpInfo, 0, sizeof(pContext->scpInfo));
  }

  SCP11S_EXIT("0x%08X", teeResult);
  return teeResult;
}

/*==========================================================================*
 * Process TA_CMD_ATTACH_SHARED_SESSION                                     *
 *==========================================================================*/
static TEE_Result TestOCEAttachSharedSession(TASESSIONCONTEXT *pContext,
                                             uint32_t          paramTypes,
                                             TEE_Param        *params)
{
  TEE_Result     teeResult          = TEE_ERROR_BAD_PARAMETERS;
  const uint32_t expectedParamTypes = TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_INPUT,
                                                      TEE_PARAM_TYPE_VALUE_INPUT,
                                                      TEE_PARAM_TYPE_NONE,
                                                      TEE_PARAM_TYPE_NONE);

  SCP11S_ENTER("");

  /* Check we have the parameters we expect */
  if ((paramTypes != expectedParamTypes) || (params == NULL) || (params[0].memref.buffer == NULL))
  {
    SCP11S_ERRORMSG("Invalid parameters, type 0x%08X", paramTypes);
    goto GetOut;
  }
  if (params[0].memref.size > sizeof(pContext->scpInfo.aid2))
  {
    SCP11S_ERRORMSG("SCP Applet AID too long (%d/%d)", params[0].memref.size, sizeof(pContext->scpInfo.aid2));
    goto GetOut;
  }

  /* Ensure a shared session is already open */
  if ((pContext->scpInfo.seService  == NULL) ||
      (pContext->scpInfo.seReader   == NULL) ||
      (pContext->scpInfo.seSession  == NULL) ||
      (pContext->scpInfo.seChannel1 == NULL) ||
      (pContext->scpInfo.seChannel2 != pContext->scpInfo.seChannel1))
  {
    SCP11S_ERRORMSG("Shared SCP session must be open first");
    goto GetOut;
  }

  /* Ensure the connection parameters match the shared session */
  if ((params[1].value.a != pContext->scpInfo.scp11Type) ||
      (params[1].value.b != pContext->scpInfo.securityLevel))
  {
    SCP11S_ERRORMSG("SCP Type and Security Level must match shared session");
    goto GetOut;
  }

  pContext->scpInfo.aidLength2 = (uint32_t) TEE_MemSMove_ext(&pContext->scpInfo.aid2[0],
                                                             sizeof(pContext->scpInfo.aid2),
                                                             params[0].memref.buffer,
                                                             params[0].memref.size);

  SCP11S_DEBUGMSG("SCP11 Applet AID 2:");
  SCP11S_DEBUGDUMP(&pContext->scpInfo.aid2[0], pContext->scpInfo.aidLength2);

  /* Attach to shared SCP session */
  teeResult = SCPAttach(&pContext->scpInfo);
  if (teeResult != TEE_SUCCESS)
  {
    SCP11S_ERRORMSG("Failed to attach to shared SCP session");
    goto GetOut;
  }

GetOut:
  if (teeResult != TEE_SUCCESS)
  {
    SCPClose(&pContext->scpInfo);
    TEE_MemFill(&pContext->scpInfo, 0, sizeof(pContext->scpInfo));
  }

  SCP11S_EXIT("0x%08X", teeResult);
  return teeResult;
}

/*==========================================================================*
 * Process TA_CMD_END_SESSION                                               *
 *==========================================================================*/
static TEE_Result TestOCEEndSession(TASESSIONCONTEXT *pContext,
                                    uint32_t          paramTypes,
                                    TEE_Param        *params)
{
  TEE_Result     teeResult          = TEE_ERROR_BAD_PARAMETERS;
  const uint32_t expectedParamTypes = TEE_PARAM_TYPES(TEE_PARAM_TYPE_NONE,
                                                      TEE_PARAM_TYPE_NONE,
                                                      TEE_PARAM_TYPE_NONE,
                                                      TEE_PARAM_TYPE_NONE);

  SCP11S_ENTER("");

  /* Check we have the parameters we expect */
  if (paramTypes != expectedParamTypes)
  {
    SCP11S_ERRORMSG("Invalid parameters, type 0x%08X", paramTypes);
    goto GetOut;
  }

  SCPClose(&pContext->scpInfo);
  TEE_MemFill(&pContext->scpInfo, 0, sizeof(pContext->scpInfo));

  teeResult = TEE_SUCCESS;

GetOut:
  SCP11S_EXIT("0x%08X", teeResult);
  return teeResult;
}

/*==========================================================================*
 * Process TA_CMD_EXCHANGE_APDUS                                            *
 *==========================================================================*/
static TEE_Result TestOCEExchangeAPDUs(TASESSIONCONTEXT *pContext,
                                       uint32_t          paramTypes,
                                       TEE_Param        *params)
{
  TEE_Result     teeResult          = TEE_ERROR_BAD_PARAMETERS;
  const uint32_t expectedParamTypes = TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_INPUT,
                                                      TEE_PARAM_TYPE_MEMREF_OUTPUT,
                                                      TEE_PARAM_TYPE_NONE,
                                                      TEE_PARAM_TYPE_NONE);

  SCP11S_ENTER("");

  /* Check we have the parameters we expect */
  if ( (paramTypes != expectedParamTypes) ||
       (params == NULL) ||
       (params[0].memref.buffer == NULL) || (params[0].memref.size < 4) ||
       (params[1].memref.buffer == NULL) || (params[1].memref.size < 2) )
  {
    SCP11S_ERRORMSG("Invalid parameters, type 0x%08X", paramTypes);
    goto GetOut;
  }

  teeResult = SCP11Send(&pContext->scpInfo,
                        params[0].memref.buffer,
                        params[0].memref.size,
                        params[1].memref.buffer,
                        &params[1].memref.size);

GetOut:
  SCP11S_EXIT("0x%08X", teeResult);
  return teeResult;
}

/*==========================================================================*
 * Entry point called once when TA loaded                                   *
 *==========================================================================*/
TEE_Result SCP11Sample_TA_CreateEntryPoint(void)
{
  SCP11S_ENTER("");
  SCP11S_INC_TA_REF_COUNT();
  SCP11S_EXIT("");
  return TEE_SUCCESS;
}

/*==========================================================================*
 * Entry point called once when TA unloaded                                 *
 *==========================================================================*/
void SCP11Sample_TA_DestroyEntryPoint(void)
{
  SCP11S_ENTER("");

  SCP11S_DEC_TA_REF_COUNT();
  if (SCP11S_NO_MORE_REFERENCES())
  {
    /* Stuff to do when unloading the last instance */
  }

  SCP11S_EXIT("");
}

/*==========================================================================*
 * Entry point called at start of new session                               *
 *==========================================================================*/
TEE_Result SCP11Sample_TA_OpenSessionEntryPoint(uint32_t    paramTypes,
                                                TEE_Param  *params,
                                                void      **ppContext)
{
  TEE_Result teeResult = TEE_SUCCESS;

  SCP11S_ENTER("0x%08X, 0x%08X, 0x%08X", paramTypes, params, ppContext);

  qsee_log_set_mask(qsee_log_get_mask() | TA_LOG_MASK);

  if (ppContext == NULL)
  {
    SCP11S_ERRORMSG("Session Context pointer is NULL");
    teeResult = TEE_ERROR_BAD_PARAMETERS;
    goto GetOut;
  }
  else
  {
    TASESSIONCONTEXT *pContext = (TASESSIONCONTEXT *) TEE_Malloc(sizeof(TASESSIONCONTEXT), TEE_MALLOC_FILL_ZERO);
    if (pContext == NULL)
    {
      SCP11S_ERRORMSG("Failed to allocate TASESSIONCONTEXT");
      teeResult = TEE_ERROR_OUT_OF_MEMORY;
      goto GetOut;
    }

    pContext->magic = TA_CONTEXT_MAGIC;

    *ppContext = (void *) pContext;
  }

GetOut:
  SCP11S_EXIT("0x%08X", teeResult);
  return teeResult;
}

/*==========================================================================*
 * Entry point called at end of session                                     *
 *==========================================================================*/
void SCP11Sample_TA_CloseSessionEntryPoint(void *pvContext)
{
  TASESSIONCONTEXT *pContext = (TASESSIONCONTEXT *) pvContext;

  SCP11S_ENTER("0x%08X", pContext);

  if ((pContext != NULL) && (pContext->magic == TA_CONTEXT_MAGIC))
  {
    SCP11S_DEBUGMSG("Free session context");

    TEE_MemFill(pContext, 0, sizeof(*pContext));
    TEE_Free(pContext);
  }

  SCP11S_EXIT("");
}

/*==========================================================================*
 * Entry point called when command invoked                                  *
 *==========================================================================*/
TEE_Result SCP11Sample_TA_InvokeCommandEntryPoint(void      *pvContext,
                                                  uint32_t   commandID,
                                                  uint32_t   paramTypes,
                                                  TEE_Param *params)
{
  TEE_Result        teeResult = TEE_ERROR_GENERIC;
  TASESSIONCONTEXT *pContext  = (TASESSIONCONTEXT *) pvContext;

  SCP11S_ENTER("0x%08X 0x%08X 0x%08X 0x%08X", pContext, commandID, paramTypes, params);

  if ((pContext == NULL) || (pContext->magic != TA_CONTEXT_MAGIC))
  {
    SCP11S_ERRORMSG("Invalid session context");
    goto GetOut;
  }

  switch (commandID)
  {
    case TA_CMD_START_SESSION:
      teeResult = TestOCEStartSession(pContext, paramTypes, params);
      break;

    case TA_CMD_ATTACH_SHARED_SESSION:
      teeResult = TestOCEAttachSharedSession(pContext, paramTypes, params);
      break;

    case TA_CMD_END_SESSION:
      teeResult = TestOCEEndSession(pContext, paramTypes, params);
      break;

    case TA_CMD_EXCHANGE_APDUS:
      teeResult = TestOCEExchangeAPDUs(pContext, paramTypes, params);
      break;

    default:
      SCP11S_ERRORMSG("Invalid command ID 0x%08X", commandID);
      break;
  }

GetOut:
  SCP11S_EXIT("0x%08X", teeResult);
  return teeResult;
}

