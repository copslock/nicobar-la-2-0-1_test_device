/* ----------------------------------------------------------------------------
 *   Includes
 * ---------------------------------------------------------------------------- */
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "gpTypes.h"
#include "gpAppLibMain.h"

/* ----------------------------------------------------------------------------
 *   Worker function declarations
 * ---------------------------------------------------------------------------- */
extern TEE_Result SCP11Sample_TA_CreateEntryPoint(void);
extern void       SCP11Sample_TA_DestroyEntryPoint(void);
extern TEE_Result SCP11Sample_TA_OpenSessionEntryPoint(uint32_t    paramTypes,
                                                       TEE_Param  *params,
                                                       void      **sessionContext);
extern void       SCP11Sample_TA_CloseSessionEntryPoint(void *sessionContext);
extern TEE_Result SCP11Sample_TA_InvokeCommandEntryPoint(void      *sessionContext,
                                                         uint32_t   commandID,
                                                         uint32_t   paramTypes,
                                                         TEE_Param *params);

/* ----------------------------------------------------------------------------
 *   Trusted Application Entry Points
 * ---------------------------------------------------------------------------- */

TEE_Result TA_CreateEntryPoint(void)
{
  SLogTrace("TA_CreateEntryPoint()");
  return SCP11Sample_TA_CreateEntryPoint();
}//TA_CreateEntryPoint()


void TA_DestroyEntryPoint(void)
{
  SLogTrace("TA_DestroyEntryPoint()");
  SCP11Sample_TA_DestroyEntryPoint();
}//TA_DestroyEntryPoint()


TEE_Result TA_OpenSessionEntryPoint(uint32_t    nParamTypes,
                                    TEE_Param   pParams[4],
                                    void      **ppSessionContext)
{
  SLogTrace("TA_OpenSessionEntryPoint()");
  return SCP11Sample_TA_OpenSessionEntryPoint(nParamTypes,
                                              &pParams[0],
                                              ppSessionContext);
}//TA_OpenSessionEntryPoint()


void TA_CloseSessionEntryPoint(void *pSessionContext)
{
  SLogTrace("TA_CloseSessionEntryPoint()");
  SCP11Sample_TA_CloseSessionEntryPoint(pSessionContext);
}//TA_CloseSessionEntryPoint()

TEE_Result TA_InvokeCommandEntryPoint(void      *pSessionContext,
                                      uint32_t   nCommandID,
                                      uint32_t   nParamTypes,
                                      TEE_Param  pParams[4])
{
  SLogTrace("TA_InvokeCommandEntryPoint()");
  return SCP11Sample_TA_InvokeCommandEntryPoint(pSessionContext,
                                                nCommandID,
                                                nParamTypes,
                                                &pParams[0]);
}//TA_InvokeCommandEntryPoint()

