#===============================================================================
#
# App Core
#
# GENERAL DESCRIPTION
#    build script
#
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/apps.tz/2.1.1/securemsm/trustzone/qsapps/scp11smp/src/SConscript#1 $
#  $DateTime: 2019/03/27 01:51:42 $
#  $Author: pwbldsvc $
#  $Change: 18705850 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains schedulerents describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#===============================================================================
Import('env')
env = env.Clone()

#------------------------------------------------------------------------------
# We need to specify "neon" to generate SIMD instructions in 32-bit mode
#------------------------------------------------------------------------------
if env['PROC'] == 'scorpion':
  env.Append(CCFLAGS = " -mfpu=neon ")

includes = [
  "${BUILD_ROOT}/core/api/services",
  "${BUILD_ROOT}/core/api/kernel/libstd/stringl",
  "${BUILD_ROOT}/ssg/api/securemsm/trustzone/gp",
  "${BUILD_ROOT}/ssg/api/securemsm/trustzone/qsee",
  "${BUILD_ROOT}/ssg/securemsm/secrsa/shared/inc",
  "${BUILD_ROOT}/ssg/securemsm/secmath/shared/inc",
  "${BUILD_ROOT}/ssg/securemsm/trustzone/qsapps/libs/tee_se_utils/inc",
  "${BUILD_ROOT}/ssg/securemsm/trustzone/qsapps/libs/tee_se_api/inc",
  "${SDK_ROOT}/inc/tee_se_utils",
]

if env.StandaloneSdk():
  libs = [
    '$SDK_ROOT/libs/$APP_EXEC_MODE/tee_se_api.lib',
    '$SDK_ROOT/libs/$APP_EXEC_MODE/tee_se_utils.lib'
  ]
else:
  libs = [
    env.SConscript('${BUILD_ROOT}/ssg/securemsm/trustzone/qsapps/libs/tee_se_api/build/SConscript',exports='env'),
    env.SConscript('${BUILD_ROOT}/ssg/securemsm/trustzone/qsapps/libs/tee_se_utils/build/SConscript',exports='env'),
  ]


#----------------------------------------------------------------------------
# App core Objects
#----------------------------------------------------------------------------
sources = [
  'gp_scp11_sample.c',
  'gp_scp11_sample_main.c',
]

app_name = 'scp11smp'

#-------------------------------------------------------------------------------
# Add metadata to image
#-------------------------------------------------------------------------------
md = {
    'appName':       app_name,
    'UUID':          'A6891849-E466-49AB-83A3-E74CCA7D931E',
    'privileges':    [ 'default', 'ESEService', 'SCP11Crypto' ],
    'acceptBufSize': 139264,    # CAPDU(64K) and RAPDU(64K) plus some overhead
    'heapSize':      0x21000,   # At least For accepted CAPDU and RAPDU
}

app_units = env.SecureAppBuilder(
  sources = sources,
  includes = includes,
  metadata = md,
  image = app_name,
  deploy_sources=[sources, 'SConscript'],
  deploy_variants = env.GetDefaultPublicVariants(),
  user_libs = libs
)

#-------------------------------------------------------------------------------
# Must ship the binaries as well as sources above
#-------------------------------------------------------------------------------
env.Deploy(app_units)

op = env.Alias(app_name, app_units)
Return('app_units')

