/*! \file
*  \n
*  \brief  pm_reg_dump_config.c
*  \n
*  \n This file contains pmic configuration data specific for PMIC debug APIs.
*  \n
*  \n &copy; Copyright 2014-2018 Qualcomm Technologies Incorporated, All Rights Reserved
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.tz/2.1.1/pmic/pm/config/nicobar_pm6125/src/pm_reg_dump_config.c#2 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
07/06/17   akt     Updated for Warlock
02/02/17   akm     Moved PMIC,SPMI out to Systemdrivers
06/16/16   aab     Added support to enable desired PMIC Register dump
01/26/16   vtw     Updated for Nazgul.
========================================================================== */

/*=========================================================================
      Include Files
==========================================================================*/

#include "pm_internal.h"

/*===========================================================================
Data Declarations
===========================================================================*/

/*
 * Array PMIC peripherals supply DDR rails.
 */
const pm_reg_dump_list_type pm_reg_dump_list[] =
{
  /* VDD_CX/VDD_MX/EBI*/
   {0x1, 0x1A00, PM_PERIPH_TYPE_SMPS},    /* S3A_CTRL */
   {0x1, 0x1C00, PM_PERIPH_TYPE_SMPS},    /* S3A_FREQ */
   {0x1, 0x1D00, PM_PERIPH_TYPE_SMPS},    /* S4A_CTRL */
   {0x1, 0x1F00, PM_PERIPH_TYPE_SMPS},    /* S4A_FREQ */
   {0x1, 0x2000, PM_PERIPH_TYPE_SMPS},    /* S5A_CTRL */
   {0x1, 0x2200, PM_PERIPH_TYPE_SMPS},    /* S5A_FREQ */

   {0x1, 0x4900, PM_PERIPH_TYPE_SMPS},    /* LDO10A */

  /* RF rails L13/21/3/2/1a*/
   {0x1, 0x4000, PM_PERIPH_TYPE_SMPS},    /* LDO1A */
   {0x1, 0x4100, PM_PERIPH_TYPE_SMPS},    /* LDO2A */
   {0x1, 0x4200, PM_PERIPH_TYPE_SMPS},    /* LDO3A */
   {0x1, 0x4C00, PM_PERIPH_TYPE_SMPS},    /* LDO13A */
   {0x1, 0x5400, PM_PERIPH_TYPE_SMPS},    /* LDO21A */
   
   
   /* WLAN Rails L8A/L9A/L16A/L17A/L23A/Lnbbclk2 */
   {0x1, 0x4700, PM_PERIPH_TYPE_SMPS},    /* LDO8A WCSS_CX*/
   {0x1, 0x4800, PM_PERIPH_TYPE_SMPS},    /* LDO9A VDD_PX3*/
   {0x1, 0x4F00, PM_PERIPH_TYPE_SMPS},    /* LDO16A */
   {0x1, 0x5000, PM_PERIPH_TYPE_SMPS},    /* LDO17A */
   {0x1, 0x5600, PM_PERIPH_TYPE_SMPS},    /* LDO23A */

   /* PON: All Peripheral dump for PON */
   {0x0, 0x0100, PM_PERIPH_TYPE_FULL_PERI_REG},  /* REVID */
   {0x0, 0x0500, PM_PERIPH_TYPE_FULL_PERI_REG},  /* INT */
   {0x0, 0x0800, PM_PERIPH_TYPE_FULL_PERI_REG},  /* PON */
   {0x0, 0x0900, PM_PERIPH_TYPE_FULL_PERI_REG},  /* MISC */
   {0x0, 0x7400, PM_PERIPH_TYPE_FULL_PERI_REG},  /* MISC_PBS2 */
   {0x0, 0xB200, PM_PERIPH_TYPE_FULL_PERI_REG},  /* SDAM3 */
   {0x0, 0x7000, PM_PERIPH_TYPE_FULL_PERI_REG},  /* PBS_CORE */
   {0x0, 0x7200, PM_PERIPH_TYPE_FULL_PERI_REG},  /* PBS_CLIENT1 */
   {0x0, 0x5100, PM_PERIPH_TYPE_FULL_PERI_REG},  /* LNBBCLK1 */
   {0x0, 0x5200, PM_PERIPH_TYPE_FULL_PERI_REG},  /* LNBBCLK2 */
   {0x0, 0x5400, PM_PERIPH_TYPE_FULL_PERI_REG},  /* RFCLK1 */
   {0x0, 0x5500, PM_PERIPH_TYPE_FULL_PERI_REG},  /* RFCLK2 */
   {0x0, 0x5600, PM_PERIPH_TYPE_FULL_PERI_REG},  /* RFCLK3 */

   {0x2, 0x0100, PM_PERIPH_TYPE_FULL_PERI_REG},  /* REVID */
   {0x2, 0x0500, PM_PERIPH_TYPE_FULL_PERI_REG},  /* INT */
   {0x2, 0x0800, PM_PERIPH_TYPE_FULL_PERI_REG},  /* PON */


   {0xf, 0xffff, PM_PERIPH_TYPE_INVALID}      /* End of list */

};

