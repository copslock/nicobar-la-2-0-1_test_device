#ifndef __MSMHWIOBASE_H__
#define __MSMHWIOBASE_H__
/*
===========================================================================
*/
/**
  @file msmhwiobase.h
  @brief Auto-generated HWIO base include file.
*/
/*
  ===========================================================================

  Copyright (c) 2019 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/core.tz/2.1.1/api/systemdrivers/hwio/nicobar/phys/msmhwiobase.h#1 $
  $DateTime: 2019/03/27 02:08:14 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * BASE: DCC_RAM_SIZE
 *--------------------------------------------------------------------------*/

#define DCC_RAM_SIZE_BASE                                           0x00002000
#define DCC_RAM_SIZE_BASE_SIZE                                      0x00000000
#define DCC_RAM_SIZE_BASE_PHYS                                      0x00002000

/*----------------------------------------------------------------------------
 * BASE: RPM_SS_MSG_RAM_SIZE
 *--------------------------------------------------------------------------*/

#define RPM_SS_MSG_RAM_SIZE_BASE                                    0x00007000
#define RPM_SS_MSG_RAM_SIZE_BASE_SIZE                               0x00000000
#define RPM_SS_MSG_RAM_SIZE_BASE_PHYS                               0x00007000

/*----------------------------------------------------------------------------
 * BASE: LPASS_LPM_SIZE
 *--------------------------------------------------------------------------*/

#define LPASS_LPM_SIZE_BASE                                         0x00010000
#define LPASS_LPM_SIZE_BASE_SIZE                                    0x00000000
#define LPASS_LPM_SIZE_BASE_PHYS                                    0x00010000

/*----------------------------------------------------------------------------
 * BASE: RPM_DATA_RAM_SIZE
 *--------------------------------------------------------------------------*/

#define RPM_DATA_RAM_SIZE_BASE                                      0x00014000
#define RPM_DATA_RAM_SIZE_BASE_SIZE                                 0x00000000
#define RPM_DATA_RAM_SIZE_BASE_PHYS                                 0x00014000

/*----------------------------------------------------------------------------
 * BASE: RPM_CODE_RAM_SIZE
 *--------------------------------------------------------------------------*/

#define RPM_CODE_RAM_SIZE_BASE                                      0x00028000
#define RPM_CODE_RAM_SIZE_BASE_SIZE                                 0x00000000
#define RPM_CODE_RAM_SIZE_BASE_PHYS                                 0x00028000

/*----------------------------------------------------------------------------
 * BASE: SYSTEM_IMEM_SIZE
 *--------------------------------------------------------------------------*/

#define SYSTEM_IMEM_SIZE_BASE                                       0x0002a000
#define SYSTEM_IMEM_SIZE_BASE_SIZE                                  0x00000000
#define SYSTEM_IMEM_SIZE_BASE_PHYS                                  0x0002a000

/*----------------------------------------------------------------------------
 * BASE: BOOT_ROM_START_ADDRESS
 *--------------------------------------------------------------------------*/

#define BOOT_ROM_START_ADDRESS_BASE                                 0x00100000
#define BOOT_ROM_START_ADDRESS_BASE_SIZE                            0x00000000
#define BOOT_ROM_START_ADDRESS_BASE_PHYS                            0x00100000

/*----------------------------------------------------------------------------
 * BASE: BOOT_ROM_SIZE
 *--------------------------------------------------------------------------*/

#define BOOT_ROM_SIZE_BASE                                          0x00100000
#define BOOT_ROM_SIZE_BASE_SIZE                                     0x00000000
#define BOOT_ROM_SIZE_BASE_PHYS                                     0x00100000

/*----------------------------------------------------------------------------
 * BASE: BOOT_ROM
 *--------------------------------------------------------------------------*/

#define BOOT_ROM_BASE                                               0x00100000
#define BOOT_ROM_BASE_SIZE                                          0x00100000
#define BOOT_ROM_BASE_PHYS                                          0x00100000

/*----------------------------------------------------------------------------
 * BASE: BOOT_IMEM_SIZE
 *--------------------------------------------------------------------------*/

#define BOOT_IMEM_SIZE_BASE                                         0x0017ffff
#define BOOT_IMEM_SIZE_BASE_SIZE                                    0x00000000
#define BOOT_IMEM_SIZE_BASE_PHYS                                    0x0017ffff

/*----------------------------------------------------------------------------
 * BASE: MSS_QDSP6_TCM_SIZE
 *--------------------------------------------------------------------------*/

#define MSS_QDSP6_TCM_SIZE_BASE                                     0x0017ffff
#define MSS_QDSP6_TCM_SIZE_BASE_SIZE                                0x00000000
#define MSS_QDSP6_TCM_SIZE_BASE_PHYS                                0x0017ffff

/*----------------------------------------------------------------------------
 * BASE: BOOT_ROM_END_ADDRESS
 *--------------------------------------------------------------------------*/

#define BOOT_ROM_END_ADDRESS_BASE                                   0x001fffff
#define BOOT_ROM_END_ADDRESS_BASE_SIZE                              0x00000000
#define BOOT_ROM_END_ADDRESS_BASE_PHYS                              0x001fffff

/*----------------------------------------------------------------------------
 * BASE: CORE_TOP_CSR
 *--------------------------------------------------------------------------*/

#define CORE_TOP_CSR_BASE                                           0x00300000
#define CORE_TOP_CSR_BASE_SIZE                                      0x00100000
#define CORE_TOP_CSR_BASE_PHYS                                      0x00300000

/*----------------------------------------------------------------------------
 * BASE: LPASS_QDSP6_TCM_SIZE
 *--------------------------------------------------------------------------*/

#define LPASS_QDSP6_TCM_SIZE_BASE                                   0x00400000
#define LPASS_QDSP6_TCM_SIZE_BASE_SIZE                              0x00000000
#define LPASS_QDSP6_TCM_SIZE_BASE_PHYS                              0x00400000

/*----------------------------------------------------------------------------
 * BASE: TLMM
 *--------------------------------------------------------------------------*/

#define TLMM_BASE                                                   0x00400000
#define TLMM_BASE_SIZE                                              0x00c00000
#define TLMM_BASE_PHYS                                              0x00400000

/*----------------------------------------------------------------------------
 * BASE: QDSS_STM_SIZE
 *--------------------------------------------------------------------------*/

#define QDSS_STM_SIZE_BASE                                          0x01000000
#define QDSS_STM_SIZE_BASE_SIZE                                     0x00000000
#define QDSS_STM_SIZE_BASE_PHYS                                     0x01000000

/*----------------------------------------------------------------------------
 * BASE: CLK_CTL
 *--------------------------------------------------------------------------*/

#define CLK_CTL_BASE                                                0x01400000
#define CLK_CTL_BASE_SIZE                                           0x00200000
#define CLK_CTL_BASE_PHYS                                           0x01400000

/*----------------------------------------------------------------------------
 * BASE: AHB2PHY_0
 *--------------------------------------------------------------------------*/

#define AHB2PHY_0_BASE                                              0x01600000
#define AHB2PHY_0_BASE_SIZE                                         0x00004000
#define AHB2PHY_0_BASE_PHYS                                         0x01600000

/*----------------------------------------------------------------------------
 * BASE: AHB2PHY_1
 *--------------------------------------------------------------------------*/

#define AHB2PHY_1_BASE                                              0x01610000
#define AHB2PHY_1_BASE_SIZE                                         0x00010000
#define AHB2PHY_1_BASE_PHYS                                         0x01610000

/*----------------------------------------------------------------------------
 * BASE: AHB2PHY_2
 *--------------------------------------------------------------------------*/

#define AHB2PHY_2_BASE                                              0x01628000
#define AHB2PHY_2_BASE_SIZE                                         0x00008000
#define AHB2PHY_2_BASE_PHYS                                         0x01628000

/*----------------------------------------------------------------------------
 * BASE: VSENSE_CONTROLLER
 *--------------------------------------------------------------------------*/

#define VSENSE_CONTROLLER_BASE                                      0x01640000
#define VSENSE_CONTROLLER_BASE_SIZE                                 0x00001000
#define VSENSE_CONTROLLER_BASE_PHYS                                 0x01640000

/*----------------------------------------------------------------------------
 * BASE: CX_CPR3
 *--------------------------------------------------------------------------*/

#define CX_CPR3_BASE                                                0x01648000
#define CX_CPR3_BASE_SIZE                                           0x00004000
#define CX_CPR3_BASE_PHYS                                           0x01648000

/*----------------------------------------------------------------------------
 * BASE: MX_CPR3
 *--------------------------------------------------------------------------*/

#define MX_CPR3_BASE                                                0x0164c000
#define MX_CPR3_BASE_SIZE                                           0x00004000
#define MX_CPR3_BASE_PHYS                                           0x0164c000

/*----------------------------------------------------------------------------
 * BASE: QM_MPU_CFG_QM_MPU_WRAPPER
 *--------------------------------------------------------------------------*/

#define QM_MPU_CFG_QM_MPU_WRAPPER_BASE                              0x01874000
#define QM_MPU_CFG_QM_MPU_WRAPPER_BASE_SIZE                         0x00001200
#define QM_MPU_CFG_QM_MPU_WRAPPER_BASE_PHYS                         0x01874000

/*----------------------------------------------------------------------------
 * BASE: CNOC_SNOC_QDSS_MPU_MPU32Q2N7S1V0_28_CL36M27L12_AHB
 *--------------------------------------------------------------------------*/

#define CNOC_SNOC_QDSS_MPU_MPU32Q2N7S1V0_28_CL36M27L12_AHB_BASE     0x0187a000
#define CNOC_SNOC_QDSS_MPU_MPU32Q2N7S1V0_28_CL36M27L12_AHB_BASE_SIZE 0x00001e00
#define CNOC_SNOC_QDSS_MPU_MPU32Q2N7S1V0_28_CL36M27L12_AHB_BASE_PHYS 0x0187a000

/*----------------------------------------------------------------------------
 * BASE: CNOC_SNOC_MPU_MPU32Q2N7S1V0_24_CL36M27L12_AHB
 *--------------------------------------------------------------------------*/

#define CNOC_SNOC_MPU_MPU32Q2N7S1V0_24_CL36M27L12_AHB_BASE          0x0187c000
#define CNOC_SNOC_MPU_MPU32Q2N7S1V0_24_CL36M27L12_AHB_BASE_SIZE     0x00001c00
#define CNOC_SNOC_MPU_MPU32Q2N7S1V0_24_CL36M27L12_AHB_BASE_PHYS     0x0187c000

/*----------------------------------------------------------------------------
 * BASE: MDSP_MPU_CFG_MPU32Q2N7S1V0_16_CL36M35L12_AHB
 *--------------------------------------------------------------------------*/

#define MDSP_MPU_CFG_MPU32Q2N7S1V0_16_CL36M35L12_AHB_BASE           0x0187e000
#define MDSP_MPU_CFG_MPU32Q2N7S1V0_16_CL36M35L12_AHB_BASE_SIZE      0x00001800
#define MDSP_MPU_CFG_MPU32Q2N7S1V0_16_CL36M35L12_AHB_BASE_PHYS      0x0187e000

/*----------------------------------------------------------------------------
 * BASE: SYSTEM_NOC
 *--------------------------------------------------------------------------*/

#define SYSTEM_NOC_BASE                                             0x01880000
#define SYSTEM_NOC_BASE_SIZE                                        0x00060200
#define SYSTEM_NOC_BASE_PHYS                                        0x01880000

/*----------------------------------------------------------------------------
 * BASE: CONFIG_NOC
 *--------------------------------------------------------------------------*/

#define CONFIG_NOC_BASE                                             0x01900000
#define CONFIG_NOC_BASE_SIZE                                        0x00008200
#define CONFIG_NOC_BASE_PHYS                                        0x01900000

/*----------------------------------------------------------------------------
 * BASE: CRYPTO0_CRYPTO_TOP
 *--------------------------------------------------------------------------*/

#define CRYPTO0_CRYPTO_TOP_BASE                                     0x01b00000
#define CRYPTO0_CRYPTO_TOP_BASE_SIZE                                0x00040000
#define CRYPTO0_CRYPTO_TOP_BASE_PHYS                                0x01b00000

/*----------------------------------------------------------------------------
 * BASE: SECURITY_CONTROL
 *--------------------------------------------------------------------------*/

#define SECURITY_CONTROL_BASE                                       0x01b40000
#define SECURITY_CONTROL_BASE_SIZE                                  0x00010000
#define SECURITY_CONTROL_BASE_PHYS                                  0x01b40000

/*----------------------------------------------------------------------------
 * BASE: PRNG_CFG_PRNG_TOP
 *--------------------------------------------------------------------------*/

#define PRNG_CFG_PRNG_TOP_BASE                                      0x01b50000
#define PRNG_CFG_PRNG_TOP_BASE_SIZE                                 0x00010000
#define PRNG_CFG_PRNG_TOP_BASE_PHYS                                 0x01b50000

/*----------------------------------------------------------------------------
 * BASE: RAMBLUR_PIMEM
 *--------------------------------------------------------------------------*/

#define RAMBLUR_PIMEM_BASE                                          0x01b60000
#define RAMBLUR_PIMEM_BASE_SIZE                                     0x00008000
#define RAMBLUR_PIMEM_BASE_PHYS                                     0x01b60000

/*----------------------------------------------------------------------------
 * BASE: QM
 *--------------------------------------------------------------------------*/

#define QM_BASE                                                     0x01b80000
#define QM_BASE_SIZE                                                0x00004000
#define QM_BASE_PHYS                                                0x01b80000

/*----------------------------------------------------------------------------
 * BASE: CAMERA_RT_THROTTLE_THROTTLE_WRAPPER_1
 *--------------------------------------------------------------------------*/

#define CAMERA_RT_THROTTLE_THROTTLE_WRAPPER_1_BASE                  0x01b88000
#define CAMERA_RT_THROTTLE_THROTTLE_WRAPPER_1_BASE_SIZE             0x00001000
#define CAMERA_RT_THROTTLE_THROTTLE_WRAPPER_1_BASE_PHYS             0x01b88000

/*----------------------------------------------------------------------------
 * BASE: CAMERA_NRT_THROTTLE_THROTTLE_WRAPPER_2
 *--------------------------------------------------------------------------*/

#define CAMERA_NRT_THROTTLE_THROTTLE_WRAPPER_2_BASE                 0x01b89000
#define CAMERA_NRT_THROTTLE_THROTTLE_WRAPPER_2_BASE_SIZE            0x00001000
#define CAMERA_NRT_THROTTLE_THROTTLE_WRAPPER_2_BASE_PHYS            0x01b89000

/*----------------------------------------------------------------------------
 * BASE: MDSS_THROTTLE_THROTTLE_WRAPPER_1
 *--------------------------------------------------------------------------*/

#define MDSS_THROTTLE_THROTTLE_WRAPPER_1_BASE                       0x01b8a000
#define MDSS_THROTTLE_THROTTLE_WRAPPER_1_BASE_SIZE                  0x00001000
#define MDSS_THROTTLE_THROTTLE_WRAPPER_1_BASE_PHYS                  0x01b8a000

/*----------------------------------------------------------------------------
 * BASE: VIDEO_THROTTLE_THROTTLE_BWMON_WRAPPER_1
 *--------------------------------------------------------------------------*/

#define VIDEO_THROTTLE_THROTTLE_BWMON_WRAPPER_1_BASE                0x01b8b000
#define VIDEO_THROTTLE_THROTTLE_BWMON_WRAPPER_1_BASE_SIZE           0x00001000
#define VIDEO_THROTTLE_THROTTLE_BWMON_WRAPPER_1_BASE_PHYS           0x01b8b000

/*----------------------------------------------------------------------------
 * BASE: GPU_THROTTLE_THROTTLE_BWMON_WRAPPER_1
 *--------------------------------------------------------------------------*/

#define GPU_THROTTLE_THROTTLE_BWMON_WRAPPER_1_BASE                  0x01b8c000
#define GPU_THROTTLE_THROTTLE_BWMON_WRAPPER_1_BASE_SIZE             0x00001000
#define GPU_THROTTLE_THROTTLE_BWMON_WRAPPER_1_BASE_PHYS             0x01b8c000

/*----------------------------------------------------------------------------
 * BASE: CDSP_THROTTLE_THROTTLE_BWMON_WRAPPER_1
 *--------------------------------------------------------------------------*/

#define CDSP_THROTTLE_THROTTLE_BWMON_WRAPPER_1_BASE                 0x01b8d000
#define CDSP_THROTTLE_THROTTLE_BWMON_WRAPPER_1_BASE_SIZE            0x00001000
#define CDSP_THROTTLE_THROTTLE_BWMON_WRAPPER_1_BASE_PHYS            0x01b8d000

/*----------------------------------------------------------------------------
 * BASE: APSS_THROTTLE_THROTTLE_BWMON_WRAPPER_1
 *--------------------------------------------------------------------------*/

#define APSS_THROTTLE_THROTTLE_BWMON_WRAPPER_1_BASE                 0x01b8e000
#define APSS_THROTTLE_THROTTLE_BWMON_WRAPPER_1_BASE_SIZE            0x00001000
#define APSS_THROTTLE_THROTTLE_BWMON_WRAPPER_1_BASE_PHYS            0x01b8e000

/*----------------------------------------------------------------------------
 * BASE: OCIMEM_WRAPPER_CSR
 *--------------------------------------------------------------------------*/

#define OCIMEM_WRAPPER_CSR_BASE                                     0x01bd0000
#define OCIMEM_WRAPPER_CSR_BASE_SIZE                                0x00006000
#define OCIMEM_WRAPPER_CSR_BASE_PHYS                                0x01bd0000

/*----------------------------------------------------------------------------
 * BASE: QC_DCC_NICOBAR_8KB4LL
 *--------------------------------------------------------------------------*/

#define QC_DCC_NICOBAR_8KB4LL_BASE                                  0x01be0000
#define QC_DCC_NICOBAR_8KB4LL_BASE_SIZE                             0x00010000
#define QC_DCC_NICOBAR_8KB4LL_BASE_PHYS                             0x01be0000

/*----------------------------------------------------------------------------
 * BASE: DCC_RAM_START_ADDRESS
 *--------------------------------------------------------------------------*/

#define DCC_RAM_START_ADDRESS_BASE                                  0x01bee000
#define DCC_RAM_START_ADDRESS_BASE_SIZE                             0x00000000
#define DCC_RAM_START_ADDRESS_BASE_PHYS                             0x01bee000

/*----------------------------------------------------------------------------
 * BASE: DCC_RAM_END_ADDRESS
 *--------------------------------------------------------------------------*/

#define DCC_RAM_END_ADDRESS_BASE                                    0x01beffff
#define DCC_RAM_END_ADDRESS_BASE_SIZE                               0x00000000
#define DCC_RAM_END_ADDRESS_BASE_PHYS                               0x01beffff

/*----------------------------------------------------------------------------
 * BASE: PMIC_ARB
 *--------------------------------------------------------------------------*/

#define PMIC_ARB_BASE                                               0x01c00000
#define PMIC_ARB_BASE_SIZE                                          0x02800000
#define PMIC_ARB_BASE_PHYS                                          0x01c00000

/*----------------------------------------------------------------------------
 * BASE: PIMEM_SIZE
 *--------------------------------------------------------------------------*/

#define PIMEM_SIZE_BASE                                             0x04000000
#define PIMEM_SIZE_BASE_SIZE                                        0x00000000
#define PIMEM_SIZE_BASE_PHYS                                        0x04000000

/*----------------------------------------------------------------------------
 * BASE: MAPSS
 *--------------------------------------------------------------------------*/

#define MAPSS_BASE                                                  0x04400000
#define MAPSS_BASE_SIZE                                             0x00040000
#define MAPSS_BASE_PHYS                                             0x04400000

/*----------------------------------------------------------------------------
 * BASE: DDR_SS
 *--------------------------------------------------------------------------*/

#define DDR_SS_BASE                                                 0x0447d000
#define DDR_SS_BASE_SIZE                                            0x000a3000
#define DDR_SS_BASE_PHYS                                            0x0447d000

/*----------------------------------------------------------------------------
 * BASE: RPM_SS_MSG_RAM_START_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_SS_MSG_RAM_START_ADDRESS_BASE                           0x045f0000
#define RPM_SS_MSG_RAM_START_ADDRESS_BASE_SIZE                      0x00000000
#define RPM_SS_MSG_RAM_START_ADDRESS_BASE_PHYS                      0x045f0000

/*----------------------------------------------------------------------------
 * BASE: RPM_MSG_RAM
 *--------------------------------------------------------------------------*/

#define RPM_MSG_RAM_BASE                                            0x045f0000
#define RPM_MSG_RAM_BASE_SIZE                                       0x00007000
#define RPM_MSG_RAM_BASE_PHYS                                       0x045f0000

/*----------------------------------------------------------------------------
 * BASE: RPM_SS_MSG_RAM_END_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_SS_MSG_RAM_END_ADDRESS_BASE                             0x045f6fff
#define RPM_SS_MSG_RAM_END_ADDRESS_BASE_SIZE                        0x00000000
#define RPM_SS_MSG_RAM_END_ADDRESS_BASE_PHYS                        0x045f6fff

/*----------------------------------------------------------------------------
 * BASE: RPM_CODE_RAM_START_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_CODE_RAM_START_ADDRESS_BASE                             0x04600000
#define RPM_CODE_RAM_START_ADDRESS_BASE_SIZE                        0x00000000
#define RPM_CODE_RAM_START_ADDRESS_BASE_PHYS                        0x04600000

/*----------------------------------------------------------------------------
 * BASE: RPM
 *--------------------------------------------------------------------------*/

#define RPM_BASE                                                    0x04600000
#define RPM_BASE_SIZE                                               0x00100000
#define RPM_BASE_PHYS                                               0x04600000

/*----------------------------------------------------------------------------
 * BASE: RPM_CODE_RAM_END_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_CODE_RAM_END_ADDRESS_BASE                               0x04627fff
#define RPM_CODE_RAM_END_ADDRESS_BASE_SIZE                          0x00000000
#define RPM_CODE_RAM_END_ADDRESS_BASE_PHYS                          0x04627fff

/*----------------------------------------------------------------------------
 * BASE: RPM_DATA_RAM_START_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_DATA_RAM_START_ADDRESS_BASE                             0x04690000
#define RPM_DATA_RAM_START_ADDRESS_BASE_SIZE                        0x00000000
#define RPM_DATA_RAM_START_ADDRESS_BASE_PHYS                        0x04690000

/*----------------------------------------------------------------------------
 * BASE: RPM_DATA_RAM_END_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_DATA_RAM_END_ADDRESS_BASE                               0x046a3fff
#define RPM_DATA_RAM_END_ADDRESS_BASE_SIZE                          0x00000000
#define RPM_DATA_RAM_END_ADDRESS_BASE_PHYS                          0x046a3fff

/*----------------------------------------------------------------------------
 * BASE: PERIPH_SS_PDM_PERPH_WEB
 *--------------------------------------------------------------------------*/

#define PERIPH_SS_PDM_PERPH_WEB_BASE                                0x04700000
#define PERIPH_SS_PDM_PERPH_WEB_BASE_SIZE                           0x00004000
#define PERIPH_SS_PDM_PERPH_WEB_BASE_PHYS                           0x04700000

/*----------------------------------------------------------------------------
 * BASE: PERIPH_SS_SDC1_SDCC5_TOP
 *--------------------------------------------------------------------------*/

#define PERIPH_SS_SDC1_SDCC5_TOP_BASE                               0x04740000
#define PERIPH_SS_SDC1_SDCC5_TOP_BASE_SIZE                          0x00020000
#define PERIPH_SS_SDC1_SDCC5_TOP_BASE_PHYS                          0x04740000

/*----------------------------------------------------------------------------
 * BASE: PERIPH_SS_SDC2_SDCC5_TOP
 *--------------------------------------------------------------------------*/

#define PERIPH_SS_SDC2_SDCC5_TOP_BASE                               0x04780000
#define PERIPH_SS_SDC2_SDCC5_TOP_BASE_SIZE                          0x00020000
#define PERIPH_SS_SDC2_SDCC5_TOP_BASE_PHYS                          0x04780000

/*----------------------------------------------------------------------------
 * BASE: UFS_MEM_UFS_REGS
 *--------------------------------------------------------------------------*/

#define UFS_MEM_UFS_REGS_BASE                                       0x04800000
#define UFS_MEM_UFS_REGS_BASE_SIZE                                  0x00020000
#define UFS_MEM_UFS_REGS_BASE_PHYS                                  0x04800000

/*----------------------------------------------------------------------------
 * BASE: QUPV3_0_QUPV3_ID_1
 *--------------------------------------------------------------------------*/

#define QUPV3_0_QUPV3_ID_1_BASE                                     0x04a00000
#define QUPV3_0_QUPV3_ID_1_BASE_SIZE                                0x00100000
#define QUPV3_0_QUPV3_ID_1_BASE_PHYS                                0x04a00000

/*----------------------------------------------------------------------------
 * BASE: QUPV3_1_QUPV3_ID_1
 *--------------------------------------------------------------------------*/

#define QUPV3_1_QUPV3_ID_1_BASE                                     0x04c00000
#define QUPV3_1_QUPV3_ID_1_BASE_SIZE                                0x00100000
#define QUPV3_1_QUPV3_ID_1_BASE_PHYS                                0x04c00000

/*----------------------------------------------------------------------------
 * BASE: USB3_PRI_USB30_PRIM
 *--------------------------------------------------------------------------*/

#define USB3_PRI_USB30_PRIM_BASE                                    0x04e00000
#define USB3_PRI_USB30_PRIM_BASE_SIZE                               0x00200000
#define USB3_PRI_USB30_PRIM_BASE_PHYS                               0x04e00000

/*----------------------------------------------------------------------------
 * BASE: IPA_0_IPA_WRAPPER
 *--------------------------------------------------------------------------*/

#define IPA_0_IPA_WRAPPER_BASE                                      0x05800000
#define IPA_0_IPA_WRAPPER_BASE_SIZE                                 0x00100000
#define IPA_0_IPA_WRAPPER_BASE_PHYS                                 0x05800000

/*----------------------------------------------------------------------------
 * BASE: GFX
 *--------------------------------------------------------------------------*/

#define GFX_BASE                                                    0x05900000
#define GFX_BASE_SIZE                                               0x000e0000
#define GFX_BASE_PHYS                                               0x05900000

/*----------------------------------------------------------------------------
 * BASE: VIDEO_SS_WRAPPER
 *--------------------------------------------------------------------------*/

#define VIDEO_SS_WRAPPER_BASE                                       0x05a00000
#define VIDEO_SS_WRAPPER_BASE_SIZE                                  0x00200000
#define VIDEO_SS_WRAPPER_BASE_PHYS                                  0x05a00000

/*----------------------------------------------------------------------------
 * BASE: CAMSS_CAMERA_SS
 *--------------------------------------------------------------------------*/

#define CAMSS_CAMERA_SS_BASE                                        0x05c00000
#define CAMSS_CAMERA_SS_BASE_SIZE                                   0x00100000
#define CAMSS_CAMERA_SS_BASE_PHYS                                   0x05c00000

/*----------------------------------------------------------------------------
 * BASE: MDSS
 *--------------------------------------------------------------------------*/

#define MDSS_BASE                                                   0x05e00000
#define MDSS_BASE_SIZE                                              0x00200000
#define MDSS_BASE_PHYS                                              0x05e00000

/*----------------------------------------------------------------------------
 * BASE: MODEM_TOP
 *--------------------------------------------------------------------------*/

#define MODEM_TOP_BASE                                              0x06000000
#define MODEM_TOP_BASE_SIZE                                         0x01000000
#define MODEM_TOP_BASE_PHYS                                         0x06000000

/*----------------------------------------------------------------------------
 * BASE: MSS_QDSP6_TCM
 *--------------------------------------------------------------------------*/

#define MSS_QDSP6_TCM_BASE                                          0x06400000
#define MSS_QDSP6_TCM_BASE_SIZE                                     0x00000000
#define MSS_QDSP6_TCM_BASE_PHYS                                     0x06400000

/*----------------------------------------------------------------------------
 * BASE: MSS_QDSP6_TCM_END
 *--------------------------------------------------------------------------*/

#define MSS_QDSP6_TCM_END_BASE                                      0x0657fffe
#define MSS_QDSP6_TCM_END_BASE_SIZE                                 0x00000000
#define MSS_QDSP6_TCM_END_BASE_PHYS                                 0x0657fffe

/*----------------------------------------------------------------------------
 * BASE: QDSS_SOC_DBG
 *--------------------------------------------------------------------------*/

#define QDSS_SOC_DBG_BASE                                           0x08000000
#define QDSS_SOC_DBG_BASE_SIZE                                      0x02000000
#define QDSS_SOC_DBG_BASE_PHYS                                      0x08000000

/*----------------------------------------------------------------------------
 * BASE: LPASS
 *--------------------------------------------------------------------------*/

#define LPASS_BASE                                                  0x0a000000
#define LPASS_BASE_SIZE                                             0x01000000
#define LPASS_BASE_PHYS                                             0x0a000000

/*----------------------------------------------------------------------------
 * BASE: LPASS_LPM
 *--------------------------------------------------------------------------*/

#define LPASS_LPM_BASE                                              0x0a250000
#define LPASS_LPM_BASE_SIZE                                         0x00000000
#define LPASS_LPM_BASE_PHYS                                         0x0a250000

/*----------------------------------------------------------------------------
 * BASE: LPASS_LPM_END
 *--------------------------------------------------------------------------*/

#define LPASS_LPM_END_BASE                                          0x0a25ffff
#define LPASS_LPM_END_BASE_SIZE                                     0x00000000
#define LPASS_LPM_END_BASE_PHYS                                     0x0a25ffff

/*----------------------------------------------------------------------------
 * BASE: LPASS_QDSP6_TCM
 *--------------------------------------------------------------------------*/

#define LPASS_QDSP6_TCM_BASE                                        0x0ac00000
#define LPASS_QDSP6_TCM_BASE_SIZE                                   0x00000000
#define LPASS_QDSP6_TCM_BASE_PHYS                                   0x0ac00000

/*----------------------------------------------------------------------------
 * BASE: LPASS_QDSP6_TCM_END
 *--------------------------------------------------------------------------*/

#define LPASS_QDSP6_TCM_END_BASE                                    0x0affffff
#define LPASS_QDSP6_TCM_END_BASE_SIZE                               0x00000000
#define LPASS_QDSP6_TCM_END_BASE_PHYS                               0x0affffff

/*----------------------------------------------------------------------------
 * BASE: TURING
 *--------------------------------------------------------------------------*/

#define TURING_BASE                                                 0x0b000000
#define TURING_BASE_SIZE                                            0x00800000
#define TURING_BASE_PHYS                                            0x0b000000

/*----------------------------------------------------------------------------
 * BASE: SNOC_MSS_NAV_CE_MPU_CFG_MPU32Q2N7S1V0_8_CL36M35L12_AHB
 *--------------------------------------------------------------------------*/

#define SNOC_MSS_NAV_CE_MPU_CFG_MPU32Q2N7S1V0_8_CL36M35L12_AHB_BASE 0x0c000000
#define SNOC_MSS_NAV_CE_MPU_CFG_MPU32Q2N7S1V0_8_CL36M35L12_AHB_BASE_SIZE 0x00001400
#define SNOC_MSS_NAV_CE_MPU_CFG_MPU32Q2N7S1V0_8_CL36M35L12_AHB_BASE_PHYS 0x0c000000

/*----------------------------------------------------------------------------
 * BASE: SNOC_LPBK_MPU_CFG_MPU32Q2N7S1V0_16_CL36M35L12_AHB
 *--------------------------------------------------------------------------*/

#define SNOC_LPBK_MPU_CFG_MPU32Q2N7S1V0_16_CL36M35L12_AHB_BASE      0x0c002000
#define SNOC_LPBK_MPU_CFG_MPU32Q2N7S1V0_16_CL36M35L12_AHB_BASE_SIZE 0x00001800
#define SNOC_LPBK_MPU_CFG_MPU32Q2N7S1V0_16_CL36M35L12_AHB_BASE_PHYS 0x0c002000

/*----------------------------------------------------------------------------
 * BASE: SNOC_BOOT_IMEM_SS_MPU_CFG_MPU32Q2N7S1V0_4_CL36M26L12_AHB
 *--------------------------------------------------------------------------*/

#define SNOC_BOOT_IMEM_SS_MPU_CFG_MPU32Q2N7S1V0_4_CL36M26L12_AHB_BASE 0x0c006000
#define SNOC_BOOT_IMEM_SS_MPU_CFG_MPU32Q2N7S1V0_4_CL36M26L12_AHB_BASE_SIZE 0x00001200
#define SNOC_BOOT_IMEM_SS_MPU_CFG_MPU32Q2N7S1V0_4_CL36M26L12_AHB_BASE_PHYS 0x0c006000

/*----------------------------------------------------------------------------
 * BASE: SYSTEM_IMEM
 *--------------------------------------------------------------------------*/

#define SYSTEM_IMEM_BASE                                            0x0c100000
#define SYSTEM_IMEM_BASE_SIZE                                       0x00000000
#define SYSTEM_IMEM_BASE_PHYS                                       0x0c100000

/*----------------------------------------------------------------------------
 * BASE: SYSTEM_IMEM_END
 *--------------------------------------------------------------------------*/

#define SYSTEM_IMEM_END_BASE                                        0x0c129fff
#define SYSTEM_IMEM_END_BASE_SIZE                                   0x00000000
#define SYSTEM_IMEM_END_BASE_PHYS                                   0x0c129fff

/*----------------------------------------------------------------------------
 * BASE: BOOT_IMEM
 *--------------------------------------------------------------------------*/

#define BOOT_IMEM_BASE                                              0x0c200000
#define BOOT_IMEM_BASE_SIZE                                         0x00000000
#define BOOT_IMEM_BASE_PHYS                                         0x0c200000

/*----------------------------------------------------------------------------
 * BASE: BOOT_IMEM_END
 *--------------------------------------------------------------------------*/

#define BOOT_IMEM_END_BASE                                          0x0c37fffe
#define BOOT_IMEM_END_BASE_SIZE                                     0x00000000
#define BOOT_IMEM_END_BASE_PHYS                                     0x0c37fffe

/*----------------------------------------------------------------------------
 * BASE: SMMU_500_APPS_REG_WRAPPER
 *--------------------------------------------------------------------------*/

#define SMMU_500_APPS_REG_WRAPPER_BASE                              0x0c600000
#define SMMU_500_APPS_REG_WRAPPER_BASE_SIZE                         0x00200000
#define SMMU_500_APPS_REG_WRAPPER_BASE_PHYS                         0x0c600000

/*----------------------------------------------------------------------------
 * BASE: WCSS_WRAPPER
 *--------------------------------------------------------------------------*/

#define WCSS_WRAPPER_BASE                                           0x0c800000
#define WCSS_WRAPPER_BASE_SIZE                                      0x00800000
#define WCSS_WRAPPER_BASE_PHYS                                      0x0c800000

/*----------------------------------------------------------------------------
 * BASE: QDSS_STM
 *--------------------------------------------------------------------------*/

#define QDSS_STM_BASE                                               0x0e000000
#define QDSS_STM_BASE_SIZE                                          0x00000000
#define QDSS_STM_BASE_PHYS                                          0x0e000000

/*----------------------------------------------------------------------------
 * BASE: QDSS_STM_END
 *--------------------------------------------------------------------------*/

#define QDSS_STM_END_BASE                                           0x0effffff
#define QDSS_STM_END_BASE_SIZE                                      0x00000000
#define QDSS_STM_END_BASE_PHYS                                      0x0effffff

/*----------------------------------------------------------------------------
 * BASE: A53SS
 *--------------------------------------------------------------------------*/

#define A53SS_BASE                                                  0x0f000000
#define A53SS_BASE_SIZE                                             0x00800000
#define A53SS_BASE_PHYS                                             0x0f000000

/*----------------------------------------------------------------------------
 * BASE: PIMEM
 *--------------------------------------------------------------------------*/

#define PIMEM_BASE                                                  0x10000000
#define PIMEM_BASE_SIZE                                             0x00000000
#define PIMEM_BASE_PHYS                                             0x10000000

/*----------------------------------------------------------------------------
 * BASE: PIMEM_END
 *--------------------------------------------------------------------------*/

#define PIMEM_END_BASE                                              0x13ffffff
#define PIMEM_END_BASE_SIZE                                         0x00000000
#define PIMEM_END_BASE_PHYS                                         0x13ffffff

/*----------------------------------------------------------------------------
 * BASE: MODEM
 *--------------------------------------------------------------------------*/

#define MODEM_BASE                                                  0x14000000
#define MODEM_BASE_SIZE                                             0x02000000
#define MODEM_BASE_PHYS                                             0x14000000

/*----------------------------------------------------------------------------
 * BASE: DDR_SPACE
 *--------------------------------------------------------------------------*/

#define DDR_SPACE_BASE                                              0x18000000
#define DDR_SPACE_BASE_SIZE                                         0x00000000
#define DDR_SPACE_BASE_PHYS                                         0x18000000

/*----------------------------------------------------------------------------
 * BASE: DDR_SPACE_SIZE
 *--------------------------------------------------------------------------*/

#define DDR_SPACE_SIZE_BASE                                         0x200000000uLL
#define DDR_SPACE_SIZE_BASE_SIZE                                    0x00000000
#define DDR_SPACE_SIZE_BASE_PHYS                                    0x200000000uLL

/*----------------------------------------------------------------------------
 * BASE: DDR_SPACE_END
 *--------------------------------------------------------------------------*/

#define DDR_SPACE_END_BASE                                          0x217ffffffuLL
#define DDR_SPACE_END_BASE_SIZE                                     0x00000000
#define DDR_SPACE_END_BASE_PHYS                                     0x217ffffffuLL


#endif /* __MSMHWIOBASE_H__ */
