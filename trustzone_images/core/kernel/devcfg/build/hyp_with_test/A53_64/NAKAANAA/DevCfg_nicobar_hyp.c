#include "DALPropDef.h"
#include "DALDeviceId.h"
#include "SMMUMasters.h"
#include "SMMUConfig.h"
#include "SMMUTypes.h"
#include "com_dtypes.h"
#include "AccessControlHyp.h"
#include "comdef.h"
#include "target.h"
#include "customer.h"
#include "custnakaanaaa.h"
#include "targnakaanaaa.h"
#include "custremoteapis.h"
#include "custtarget.h"
#include "custsdcc.h"
#include "custsurf.h"
#include "custdiag.h"
#include "custefs.h"
#include "custpmic.h"
#include "custsio_8660.h"
#include "custsec.h"
#include "custsfs.h"
#include "armasm.h"
#include "ACCommon.h"
#include "IAccessControl.h"
#include "object.h"
#include "tz_syscall_pub.h"
#include "SMMUDynamicSID.h"
#include "IxErrno.h"
#include "SMMUImplDef.h"
#include "HALSMMU.h"
#include "DALPropDef.h"
#ifndef DEVCFG_STANDALONE 
#include "DALStdDef.h" 
#include "DALSysTypes.h" 

#else 
#include "stubs.h" 

#include "devcfgTypes.h" 

#endif 
extern void * device_qup_0i0_nicobar_hyp;
extern void * device_qup_0i0i_nicobar_hyp;
extern void * device_qup_0i1_nicobar_hyp;
extern void * device_qup_0i1i_nicobar_hyp;
extern void * device_qup_0i2_nicobar_hyp;
extern void * device_qup_0i2i_nicobar_hyp;
extern void * device_qup_0i3_nicobar_hyp;
extern void * device_qup_0i3i_nicobar_hyp;
extern void * device_qup_0i4_nicobar_hyp;
extern void * device_qup_0i4i_nicobar_hyp;
extern void * device_qup_0i5_nicobar_hyp;
extern void * device_qup_0i5i_nicobar_hyp;
extern void * device_qup_0i6_nicobar_hyp;
extern void * device_qup_0i6i_nicobar_hyp;
extern void * device_qup_0i7_nicobar_hyp;
extern void * device_qup_0i7i_nicobar_hyp;
extern void * device_qup_0s0_nicobar_hyp;
extern void * device_qup_0s0s_nicobar_hyp;
extern void * device_qup_0s0g0_nicobar_hyp;
extern void * device_qup_0s0g2_nicobar_hyp;
extern void * device_qup_0s1_nicobar_hyp;
extern void * device_qup_0s1s_nicobar_hyp;
extern void * device_qup_0s1g0_nicobar_hyp;
extern void * device_qup_0s2_nicobar_hyp;
extern void * device_qup_0s2s_nicobar_hyp;
extern void * device_qup_0s2g0_nicobar_hyp;
extern void * device_qup_0s2g2_nicobar_hyp;
extern void * device_qup_0s3_nicobar_hyp;
extern void * device_qup_0s3s_nicobar_hyp;
extern void * device_qup_0s3g0_nicobar_hyp;
extern void * device_qup_0s3g2_nicobar_hyp;
extern void * device_qup_0s4_nicobar_hyp;
extern void * device_qup_0s4s_nicobar_hyp;
extern void * device_qup_0s4g0_nicobar_hyp;
extern void * device_qup_0s4g2_nicobar_hyp;
extern void * device_qup_0c_nicobar_hyp;
extern void * device_qup_0cs_nicobar_hyp;
extern void * device_qup_1i0_nicobar_hyp;
extern void * device_qup_1i0i_nicobar_hyp;
extern void * device_qup_1i1_nicobar_hyp;
extern void * device_qup_1i1i_nicobar_hyp;
extern void * device_qup_1i2_nicobar_hyp;
extern void * device_qup_1i2i_nicobar_hyp;
extern void * device_qup_1i3_nicobar_hyp;
extern void * device_qup_1i3i_nicobar_hyp;
extern void * device_qup_1i4_nicobar_hyp;
extern void * device_qup_1i4i_nicobar_hyp;
extern void * device_qup_1i5_nicobar_hyp;
extern void * device_qup_1i5i_nicobar_hyp;
extern void * device_qup_1i6_nicobar_hyp;
extern void * device_qup_1i6i_nicobar_hyp;
extern void * device_qup_1i7_nicobar_hyp;
extern void * device_qup_1i7i_nicobar_hyp;
extern void * device_qup_1s0_nicobar_hyp;
extern void * device_qup_1s0s_nicobar_hyp;
extern void * device_qup_1s0g0_nicobar_hyp;
extern void * device_qup_1s0g2_nicobar_hyp;
extern void * device_qup_1s1_nicobar_hyp;
extern void * device_qup_1s1s_nicobar_hyp;
extern void * device_qup_1s1g0_nicobar_hyp;
extern void * device_qup_1s1g2_nicobar_hyp;
extern void * device_qup_1s2_nicobar_hyp;
extern void * device_qup_1s2s_nicobar_hyp;
extern void * device_qup_1s2g0_nicobar_hyp;
extern void * device_qup_1s3_nicobar_hyp;
extern void * device_qup_1s3s_nicobar_hyp;
extern void * device_qup_1s3g0_nicobar_hyp;
extern void * device_qup_1s3g2_nicobar_hyp;
extern void * device_qup_1s4_nicobar_hyp;
extern void * device_qup_1s4s_nicobar_hyp;
extern void * device_qup_1s4g0_nicobar_hyp;
extern void * device_qup_1s4g2_nicobar_hyp;
extern void * device_qup_1c_nicobar_hyp;
extern void * device_qup_1cs_nicobar_hyp;
extern void * device_sensor_pram_nicobar_hyp;
extern void * qupv3_se_hw_nicobar_hyp;
extern void * qupv3_se_hw_size_nicobar_hyp;
extern void * qupv3_common_hw_nicobar_hyp;
extern void * qupv3_common_hw_size_nicobar_hyp;
extern void * qupv3_0_clocks_arr_nicobar_hyp;
extern void * qupv3_0_clocks_arr_size_nicobar_hyp;
extern void * qupv3_1_clocks_arr_nicobar_hyp;
extern void * qupv3_1_clocks_arr_size_nicobar_hyp;
extern void * qupv3_private_arr_nicobar_hyp;
extern void * qupv3_private_arr_size_nicobar_hyp;


static SMMUImplDef1ConfigEntry_t	devcfg_0={0x0,0,0,0,0,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0,{0,0,0,0,0,0,0,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF},0,{0,0,0,0,0,0,0,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF},0,{0,0,0,0,0,0,0,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF},0xFFFFFFFF,{0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF},0x0,{0,0,0,0,0,0,0,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF},0,{0,0,0,0,0,0,0,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF},0,{0,0,0,0,0,0,0,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF},0,0,0,0,0,0,0,0,0,0xFFFFFFFF,{0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF},0,{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},{0,0,0,0},0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0,0,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,{0xFFFFFFFF,0xFFFFFFFF},0xFFFFFFFF,0xFFFFFFFF,{0xFFFFFFFF,0xFFFFFFFF}};


static SMMUImplDef1ConfigEntry_t	devcfg_1={0x05040000,2,0,0,0,0x70,0x1055,0x6,0x3FF,0xFFFFFFFF,0xFFFFFFFF,{0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF},0xFFFFFFFF,{0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF},0xFFFFFFFF,{0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF},0xFFFFFFFF,{0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF},0xFFFFFFFF,{0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF},0xFFFFFFFF,{0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF},0xFFFFFFFF,{0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF},0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,{0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF},0,{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},{0,0,0,0},0x204,0x11000,0x800,0xFFFFFFFF,0xFFFFFFFF,0x1A5551,0x2AAA2F82,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,{0xFFFFFFFF,0xFFFFFFFF},0xFFFFFFFF,0xFFFFFFFF,{0xFFFFFFFF,0xFFFFFFFF}};

const DALSYSPropStructTblType DALPROP_StructPtrs_nicobar_hyp[88] =  {
	 {sizeof(devcfg_0), &devcfg_0},
	 {sizeof(devcfg_1), &devcfg_1},
	 {sizeof(void *), &device_qup_0i0_nicobar_hyp},
	 {sizeof(void *), &device_qup_0i0i_nicobar_hyp},
	 {sizeof(void *), &device_qup_0i1_nicobar_hyp},
	 {sizeof(void *), &device_qup_0i1i_nicobar_hyp},
	 {sizeof(void *), &device_qup_0i2_nicobar_hyp},
	 {sizeof(void *), &device_qup_0i2i_nicobar_hyp},
	 {sizeof(void *), &device_qup_0i3_nicobar_hyp},
	 {sizeof(void *), &device_qup_0i3i_nicobar_hyp},
	 {sizeof(void *), &device_qup_0i4_nicobar_hyp},
	 {sizeof(void *), &device_qup_0i4i_nicobar_hyp},
	 {sizeof(void *), &device_qup_0i5_nicobar_hyp},
	 {sizeof(void *), &device_qup_0i5i_nicobar_hyp},
	 {sizeof(void *), &device_qup_0i6_nicobar_hyp},
	 {sizeof(void *), &device_qup_0i6i_nicobar_hyp},
	 {sizeof(void *), &device_qup_0i7_nicobar_hyp},
	 {sizeof(void *), &device_qup_0i7i_nicobar_hyp},
	 {sizeof(void *), &device_qup_0s0_nicobar_hyp},
	 {sizeof(void *), &device_qup_0s0s_nicobar_hyp},
	 {sizeof(void *), &device_qup_0s0g0_nicobar_hyp},
	 {sizeof(void *), &device_qup_0s0g2_nicobar_hyp},
	 {sizeof(void *), &device_qup_0s1_nicobar_hyp},
	 {sizeof(void *), &device_qup_0s1s_nicobar_hyp},
	 {sizeof(void *), &device_qup_0s1g0_nicobar_hyp},
	 {sizeof(void *), &device_qup_0s2_nicobar_hyp},
	 {sizeof(void *), &device_qup_0s2s_nicobar_hyp},
	 {sizeof(void *), &device_qup_0s2g0_nicobar_hyp},
	 {sizeof(void *), &device_qup_0s2g2_nicobar_hyp},
	 {sizeof(void *), &device_qup_0s3_nicobar_hyp},
	 {sizeof(void *), &device_qup_0s3s_nicobar_hyp},
	 {sizeof(void *), &device_qup_0s3g0_nicobar_hyp},
	 {sizeof(void *), &device_qup_0s3g2_nicobar_hyp},
	 {sizeof(void *), &device_qup_0s4_nicobar_hyp},
	 {sizeof(void *), &device_qup_0s4s_nicobar_hyp},
	 {sizeof(void *), &device_qup_0s4g0_nicobar_hyp},
	 {sizeof(void *), &device_qup_0s4g2_nicobar_hyp},
	 {sizeof(void *), &device_qup_0c_nicobar_hyp},
	 {sizeof(void *), &device_qup_0cs_nicobar_hyp},
	 {sizeof(void *), &device_qup_1i0_nicobar_hyp},
	 {sizeof(void *), &device_qup_1i0i_nicobar_hyp},
	 {sizeof(void *), &device_qup_1i1_nicobar_hyp},
	 {sizeof(void *), &device_qup_1i1i_nicobar_hyp},
	 {sizeof(void *), &device_qup_1i2_nicobar_hyp},
	 {sizeof(void *), &device_qup_1i2i_nicobar_hyp},
	 {sizeof(void *), &device_qup_1i3_nicobar_hyp},
	 {sizeof(void *), &device_qup_1i3i_nicobar_hyp},
	 {sizeof(void *), &device_qup_1i4_nicobar_hyp},
	 {sizeof(void *), &device_qup_1i4i_nicobar_hyp},
	 {sizeof(void *), &device_qup_1i5_nicobar_hyp},
	 {sizeof(void *), &device_qup_1i5i_nicobar_hyp},
	 {sizeof(void *), &device_qup_1i6_nicobar_hyp},
	 {sizeof(void *), &device_qup_1i6i_nicobar_hyp},
	 {sizeof(void *), &device_qup_1i7_nicobar_hyp},
	 {sizeof(void *), &device_qup_1i7i_nicobar_hyp},
	 {sizeof(void *), &device_qup_1s0_nicobar_hyp},
	 {sizeof(void *), &device_qup_1s0s_nicobar_hyp},
	 {sizeof(void *), &device_qup_1s0g0_nicobar_hyp},
	 {sizeof(void *), &device_qup_1s0g2_nicobar_hyp},
	 {sizeof(void *), &device_qup_1s1_nicobar_hyp},
	 {sizeof(void *), &device_qup_1s1s_nicobar_hyp},
	 {sizeof(void *), &device_qup_1s1g0_nicobar_hyp},
	 {sizeof(void *), &device_qup_1s1g2_nicobar_hyp},
	 {sizeof(void *), &device_qup_1s2_nicobar_hyp},
	 {sizeof(void *), &device_qup_1s2s_nicobar_hyp},
	 {sizeof(void *), &device_qup_1s2g0_nicobar_hyp},
	 {sizeof(void *), &device_qup_1s3_nicobar_hyp},
	 {sizeof(void *), &device_qup_1s3s_nicobar_hyp},
	 {sizeof(void *), &device_qup_1s3g0_nicobar_hyp},
	 {sizeof(void *), &device_qup_1s3g2_nicobar_hyp},
	 {sizeof(void *), &device_qup_1s4_nicobar_hyp},
	 {sizeof(void *), &device_qup_1s4s_nicobar_hyp},
	 {sizeof(void *), &device_qup_1s4g0_nicobar_hyp},
	 {sizeof(void *), &device_qup_1s4g2_nicobar_hyp},
	 {sizeof(void *), &device_qup_1c_nicobar_hyp},
	 {sizeof(void *), &device_qup_1cs_nicobar_hyp},
	 {sizeof(void *), &device_sensor_pram_nicobar_hyp},
	 {sizeof(void *), &qupv3_se_hw_nicobar_hyp},
	 {sizeof(void *), &qupv3_se_hw_size_nicobar_hyp},
	 {sizeof(void *), &qupv3_common_hw_nicobar_hyp},
	 {sizeof(void *), &qupv3_common_hw_size_nicobar_hyp},
	 {sizeof(void *), &qupv3_0_clocks_arr_nicobar_hyp},
	 {sizeof(void *), &qupv3_0_clocks_arr_size_nicobar_hyp},
	 {sizeof(void *), &qupv3_1_clocks_arr_nicobar_hyp},
	 {sizeof(void *), &qupv3_1_clocks_arr_size_nicobar_hyp},
	 {sizeof(void *), &qupv3_private_arr_nicobar_hyp},
	 {sizeof(void *), &qupv3_private_arr_size_nicobar_hyp},
	{0, 0 } 
 };
const uint32 DALPROP_PropBin_nicobar_hyp[] = {

			0x000006b0, 0x00000020, 0x00000220, 0x00000220, 0x00000220, 
			0x00000001, 0x00000000, 0x0000064c, 0x71006964, 0x33767075, 
			0x5f65735f, 0x71007768, 0x33767075, 0x5f65735f, 0x735f7768, 
			0x00657a69, 0x76707571, 0x6f635f33, 0x6e6f6d6d, 0x0077685f, 
			0x76707571, 0x6f635f33, 0x6e6f6d6d, 0x5f77685f, 0x657a6973, 
			0x70757100, 0x305f3376, 0x6f6c635f, 0x5f736b63, 0x00727261, 
			0x76707571, 0x5f305f33, 0x636f6c63, 0x615f736b, 0x735f7272, 
			0x00657a69, 0x76707571, 0x5f315f33, 0x636f6c63, 0x615f736b, 
			0x71007272, 0x33767075, 0x635f315f, 0x6b636f6c, 0x72615f73, 
			0x69735f72, 0x6900657a, 0x73735f73, 0x75715f63, 0x63615f70, 
			0x6e6f635f, 0x5f676966, 0x70707573, 0x6574726f, 0x75710064, 
			0x5f337670, 0x6f697067, 0x6573755f, 0x6d736d5f, 0x7265765f, 
			0x70757100, 0x745f3376, 0x5f727363, 0x5f636f73, 0x765f7768, 
			0x615f7265, 0x00726464, 0x76707571, 0x6f645f33, 0x656c6275, 
			0x6769735f, 0x0064656e, 0x5f626369, 0x69746f76, 0x765f676e, 
			0x69737265, 0x71006e6f, 0x33767075, 0x6972705f, 0x65746176, 
			0x7272615f, 0x70757100, 0x705f3376, 0x61766972, 0x615f6574, 
			0x735f7272, 0x00657a69, 0x456c6144, 0x5400766e, 0x65677261, 
			0x67664374, 0x626c4700, 0x74787443, 0x754d5748, 0x4e786574, 
			0x65626d75, 0x79680072, 0x6e655f70, 0x656c6261, 0x6d76645f, 
			0x70796800, 0x6170695f, 0x7361625f, 0x735f6465, 0x5f656661, 
			0x5f6f6365, 0x00617773, 0x5f707968, 0x65666173, 0x6f63655f, 
			0x6177735f, 0x70796800, 0x6365735f, 0x5f657275, 0x5f636364, 
			0x62616e65, 0x6800656c, 0x655f7079, 0x6c62616e, 0x6e755f65, 
			0x5f676f6c, 0x00636d73, 0x5f707968, 0x6f6c6e75, 0x6d735f67, 
			0x64695f63, 0x6d756e5f, 0x70796800, 0x6c6e755f, 0x735f676f, 
			0x695f636d, 0x696c5f64, 0x65007473, 0x6c62616e, 0x706b5f65, 
			0x00000000, 0x00000001, 0x00000000, 0x00002070, 0x00000001, 
			0x80008000, 0xc4000001, (301989888 | (SMMU_APPS_TCU & 0x00FFFFFF)), 0x00000000, (301989888 | (SMMU_MMSS_OXILI & 0x00FFFFFF)), 
			0x00000001, 0xff00ff00, 0x12800000, 0x00000002, 0xff00ff00, 
			0x12800000, 0x00000003, 0xff00ff00, 0x12800000, 0x00000004, 
			0xff00ff00, 0x12800000, 0x00000005, 0xff00ff00, 0x12800000, 
			0x00000006, 0xff00ff00, 0x12800000, 0x00000007, 0xff00ff00, 
			0x12800000, 0x00000008, 0xff00ff00, 0x12800000, 0x00000009, 
			0xff00ff00, 0x12800000, 0x0000000a, 0xff00ff00, 0x12800000, 
			0x0000000b, 0xff00ff00, 0x12800000, 0x0000000c, 0xff00ff00, 
			0x12800000, 0x0000000d, 0xff00ff00, 0x12800000, 0x0000000e, 
			0xff00ff00, 0x12800000, 0x0000000f, 0xff00ff00, 0x12800000, 
			0x00000010, 0xff00ff00, 0x12800000, 0x00000011, 0xff00ff00, 
			0x12800000, 0x00000012, 0xff00ff00, 0x12800000, 0x00000013, 
			0xff00ff00, 0x12800000, 0x00000014, 0xff00ff00, 0x12800000, 
			0x00000015, 0xff00ff00, 0x12800000, 0x00000016, 0xff00ff00, 
			0x12800000, 0x00000017, 0xff00ff00, 0x12800000, 0x00000018, 
			0xff00ff00, 0x12800000, 0x00000019, 0xff00ff00, 0x12800000, 
			0x0000001a, 0xff00ff00, 0x12800000, 0x0000001b, 0xff00ff00, 
			0x12800000, 0x0000001c, 0xff00ff00, 0x12800000, 0x0000001d, 
			0xff00ff00, 0x12800000, 0x0000001e, 0xff00ff00, 0x12800000, 
			0x0000001f, 0xff00ff00, 0x12800000, 0x00000020, 0xff00ff00, 
			0x12800000, 0x00000021, 0xff00ff00, 0x12800000, 0x00000022, 
			0xff00ff00, 0x12800000, 0x00000023, 0xff00ff00, 0x12800000, 
			0x00000024, 0xff00ff00, 0x12800000, 0x00000025, 0xff00ff00, 
			0x12800000, 0x00000026, 0xff00ff00, 0x12800000, 0x00000027, 
			0xff00ff00, 0x12800000, 0x00000028, 0xff00ff00, 0x12800000, 
			0x00000029, 0xff00ff00, 0x12800000, 0x0000002a, 0xff00ff00, 
			0x12800000, 0x0000002b, 0xff00ff00, 0x12800000, 0x0000002c, 
			0xff00ff00, 0x12800000, 0x0000002d, 0xff00ff00, 0x12800000, 
			0x0000002e, 0xff00ff00, 0x12800000, 0x0000002f, 0xff00ff00, 
			0x12800000, 0x00000030, 0xff00ff00, 0x12800000, 0x00000031, 
			0xff00ff00, 0x12800000, 0x00000032, 0xff00ff00, 0x12800000, 
			0x00000033, 0xff00ff00, 0x12800000, 0x00000034, 0xff00ff00, 
			0x12800000, 0x00000035, 0xff00ff00, 0x12800000, 0x00000036, 
			0xff00ff00, 0x12800000, 0x00000037, 0xff00ff00, 0x12800000, 
			0x00000038, 0xff00ff00, 0x12800000, 0x00000039, 0xff00ff00, 
			0x12800000, 0x0000003a, 0xff00ff00, 0x12800000, 0x0000003b, 
			0xff00ff00, 0x12800000, 0x0000003c, 0xff00ff00, 0x12800000, 
			0x0000003d, 0xff00ff00, 0x12800000, 0x0000003e, 0xff00ff00, 
			0x12800000, 0x0000003f, 0xff00ff00, 0x12800000, 0x00000040, 
			0xff00ff00, 0x12800000, 0x00000041, 0xff00ff00, 0x12800000, 
			0x00000042, 0xff00ff00, 0x12800000, 0x00000043, 0xff00ff00, 
			0x12800000, 0x00000044, 0xff00ff00, 0x12800000, 0x00000045, 
			0xff00ff00, 0x12800000, 0x00000046, 0xff00ff00, 0x12800000, 
			0x00000047, 0xff00ff00, 0x12800000, 0x00000048, 0xff00ff00, 
			0x12800000, 0x00000049, 0xff00ff00, 0x12800000, 0x0000004a, 
			0xff00ff00, 0x12800000, 0x0000004b, 0xff00ff00, 0x12800000, 
			0x0000004c, 0xff00ff00, 0x12800003, 0x0000004d, 0x1280000f, 
			0x0000004e, 0x12800020, 0x0000004f, 0x12800030, 0x00000050, 
			0x12800045, 0x00000051, 0x12800058, 0x00000052, 0x12800070, 
			0x00000053, 0x12800083, 0x00000054, 0x0280009b, 0x00000000, 
			0x028000ba, 0x00000000, 0x028000d1, 0x003c8000, 0x028000ec, 
			0x00000001, 0x02800100, 0x00000002, 0x12800113, 0x00000055, 
			0x12800125, 0x00000056, 0xff00ff00, 0x0280013c, 0x10000002, 
			0x14800143, 0x00000000, 0x0280014d, 0x0000000a, 0xff00ff00, 
			0x02800162, 0x00000000, 0x02800171, 0x00000000, 0x0280018c, 
			0x00000000, 0x0280019d, 0x00000001, 0x028001b3, 0x00000001, 
			0x028001c8, 0x00000002, 0x148001dd, 0x0000000c, 0xff00ff00, 
			0x028001f3, 0x00000001, 0xff00ff00 };



const StringDevice driver_list_nicobar_hyp[] = {
			{"/dev/smmu_V1",2982605418u, 568, NULL, 0, NULL },
			{"QUP_0I0",722463235u, 588, NULL, 0, NULL },
			{"QUP_0I0I",2366450348u, 600, NULL, 0, NULL },
			{"QUP_0I1",722463236u, 612, NULL, 0, NULL },
			{"QUP_0I1I",2366450381u, 624, NULL, 0, NULL },
			{"QUP_0I2",722463237u, 636, NULL, 0, NULL },
			{"QUP_0I2I",2366450414u, 648, NULL, 0, NULL },
			{"QUP_0I3",722463238u, 660, NULL, 0, NULL },
			{"QUP_0I3I",2366450447u, 672, NULL, 0, NULL },
			{"QUP_0I4",722463239u, 684, NULL, 0, NULL },
			{"QUP_0I4I",2366450480u, 696, NULL, 0, NULL },
			{"QUP_0I5",722463240u, 708, NULL, 0, NULL },
			{"QUP_0I5I",2366450513u, 720, NULL, 0, NULL },
			{"QUP_0I6",722463241u, 732, NULL, 0, NULL },
			{"QUP_0I6I",2366450546u, 744, NULL, 0, NULL },
			{"QUP_0I7",722463242u, 756, NULL, 0, NULL },
			{"QUP_0I7I",2366450579u, 768, NULL, 0, NULL },
			{"QUP_0S0",722463565u, 780, NULL, 0, NULL },
			{"QUP_0S0S",2366461248u, 792, NULL, 0, NULL },
			{"QUP_0S0G0",783809508u, 804, NULL, 0, NULL },
			{"QUP_0S0G2",783809510u, 816, NULL, 0, NULL },
			{"QUP_0S1",722463566u, 828, NULL, 0, NULL },
			{"QUP_0S1S",2366461281u, 840, NULL, 0, NULL },
			{"QUP_0S1G0",783810597u, 852, NULL, 0, NULL },
			{"QUP_0S2",722463567u, 864, NULL, 0, NULL },
			{"QUP_0S2S",2366461314u, 876, NULL, 0, NULL },
			{"QUP_0S2G0",783811686u, 888, NULL, 0, NULL },
			{"QUP_0S2G2",783811688u, 900, NULL, 0, NULL },
			{"QUP_0S3",722463568u, 912, NULL, 0, NULL },
			{"QUP_0S3S",2366461347u, 924, NULL, 0, NULL },
			{"QUP_0S3G0",783812775u, 936, NULL, 0, NULL },
			{"QUP_0S3G2",783812777u, 948, NULL, 0, NULL },
			{"QUP_0S4",722463569u, 960, NULL, 0, NULL },
			{"QUP_0S4S",2366461380u, 972, NULL, 0, NULL },
			{"QUP_0S4G0",783813864u, 984, NULL, 0, NULL },
			{"QUP_0S4G2",783813866u, 996, NULL, 0, NULL },
			{"QUP_0C",3405806445u, 1008, NULL, 0, NULL },
			{"QUP_0CS",722463072u, 1020, NULL, 0, NULL },
			{"QUP_1I0",722464324u, 1032, NULL, 0, NULL },
			{"QUP_1I0I",2366486285u, 1044, NULL, 0, NULL },
			{"QUP_1I1",722464325u, 1056, NULL, 0, NULL },
			{"QUP_1I1I",2366486318u, 1068, NULL, 0, NULL },
			{"QUP_1I2",722464326u, 1080, NULL, 0, NULL },
			{"QUP_1I2I",2366486351u, 1092, NULL, 0, NULL },
			{"QUP_1I3",722464327u, 1104, NULL, 0, NULL },
			{"QUP_1I3I",2366486384u, 1116, NULL, 0, NULL },
			{"QUP_1I4",722464328u, 1128, NULL, 0, NULL },
			{"QUP_1I4I",2366486417u, 1140, NULL, 0, NULL },
			{"QUP_1I5",722464329u, 1152, NULL, 0, NULL },
			{"QUP_1I5I",2366486450u, 1164, NULL, 0, NULL },
			{"QUP_1I6",722464330u, 1176, NULL, 0, NULL },
			{"QUP_1I6I",2366486483u, 1188, NULL, 0, NULL },
			{"QUP_1I7",722464331u, 1200, NULL, 0, NULL },
			{"QUP_1I7I",2366486516u, 1212, NULL, 0, NULL },
			{"QUP_1S0",722464654u, 1224, NULL, 0, NULL },
			{"QUP_1S0S",2366497185u, 1236, NULL, 0, NULL },
			{"QUP_1S0G0",784995429u, 1248, NULL, 0, NULL },
			{"QUP_1S0G2",784995431u, 1260, NULL, 0, NULL },
			{"QUP_1S1",722464655u, 1272, NULL, 0, NULL },
			{"QUP_1S1S",2366497218u, 1284, NULL, 0, NULL },
			{"QUP_1S1G0",784996518u, 1296, NULL, 0, NULL },
			{"QUP_1S1G2",784996520u, 1308, NULL, 0, NULL },
			{"QUP_1S2",722464656u, 1320, NULL, 0, NULL },
			{"QUP_1S2S",2366497251u, 1332, NULL, 0, NULL },
			{"QUP_1S2G0",784997607u, 1344, NULL, 0, NULL },
			{"QUP_1S3",722464657u, 1356, NULL, 0, NULL },
			{"QUP_1S3S",2366497284u, 1368, NULL, 0, NULL },
			{"QUP_1S3G0",784998696u, 1380, NULL, 0, NULL },
			{"QUP_1S3G2",784998698u, 1392, NULL, 0, NULL },
			{"QUP_1S4",722464658u, 1404, NULL, 0, NULL },
			{"QUP_1S4S",2366497317u, 1416, NULL, 0, NULL },
			{"QUP_1S4G0",784999785u, 1428, NULL, 0, NULL },
			{"QUP_1S4G2",784999787u, 1440, NULL, 0, NULL },
			{"QUP_1C",3405806478u, 1452, NULL, 0, NULL },
			{"QUP_1CS",722464161u, 1464, NULL, 0, NULL },
			{"SENSOR_PRAM",3724033006u, 1476, NULL, 0, NULL },
			{"QUPV3_HW_PROP",751796930u, 1488, NULL, 0, NULL },
			{"/dev/hyp",3784637939u, 1640, NULL, 0, NULL },
			{"/kp/general",1520996892u, 1700, NULL, 0, NULL }
};
