#include "DALPropDef.h"
#include "DALDeviceId.h"
#include "DALPropDef.h"
#ifndef DEVCFG_STANDALONE 
#include "DALStdDef.h" 
#include "DALSysTypes.h" 

#else 
#include "stubs.h" 

#include "devcfgTypes.h" 

#endif 
extern void * qupv3_perms_default_nicobar_oem_hyp;
extern void * qupv3_perms_size_default_nicobar_oem_hyp;
extern void * qupv3_gpii_perms_nicobar_oem_hyp;
extern void * qupv3_gpii_perms_size_nicobar_oem_hyp;
extern void * qupv3_perms_rumi_nicobar_oem_hyp;
extern void * qupv3_perms_size_rumi_nicobar_oem_hyp;

const DALSYSPropStructTblType DALPROP_StructPtrs_nicobar_oem_hyp[7] =  {
	 {sizeof(void *), &qupv3_perms_default_nicobar_oem_hyp},
	 {sizeof(void *), &qupv3_perms_size_default_nicobar_oem_hyp},
	 {sizeof(void *), &qupv3_gpii_perms_nicobar_oem_hyp},
	 {sizeof(void *), &qupv3_gpii_perms_size_nicobar_oem_hyp},
	 {sizeof(void *), &qupv3_perms_rumi_nicobar_oem_hyp},
	 {sizeof(void *), &qupv3_perms_size_rumi_nicobar_oem_hyp},
	{0, 0 } 
 };
const uint32 DALPROP_PropBin_nicobar_oem_hyp[] = {

			0x000002a8, 0x00000020, 0x000001a4, 0x000001a4, 0x000001a4, 
			0x00000001, 0x00000001, 0x00000214, 0x76707571, 0x65705f33, 
			0x00736d72, 0x76707571, 0x65705f33, 0x5f736d72, 0x657a6973, 
			0x69706700, 0x65705f69, 0x00736d72, 0x69697067, 0x7265705f, 
			0x735f736d, 0x00657a69, 0x456c6144, 0x5400766e, 0x65677261, 
			0x67664374, 0x70796800, 0x6365735f, 0x5f657275, 0x5f726f6d, 
			0x62616e65, 0x0064656c, 0x5f707968, 0x5f6d756e, 0x61636e75, 
			0x64656863, 0x6e61725f, 0x00736567, 0x5f707968, 0x61636e75, 
			0x64656863, 0x6e61725f, 0x00736567, 0x5f707968, 0x61656c63, 
			0x76655f6e, 0x5f746369, 0x5f415753, 0x67616c66, 0x70796800, 
			0x6365735f, 0x5f657275, 0x5f726f6d, 0x53505041, 0x6174735f, 
			0x68007472, 0x735f7079, 0x72756365, 0x6f6d5f65, 0x50415f72, 
			0x6c5f5350, 0x68006e65, 0x735f7079, 0x72756365, 0x6f6d5f65, 
			0x49505f72, 0x74735f4c, 0x00747261, 0x5f707968, 0x5f70646d, 
			0x735f6264, 0x74726174, 0x70796800, 0x70646d5f, 0x5f62645f, 
			0x006e656c, 0x61736964, 0x5f656c62, 0x756d6d73, 0x0063615f, 
			0x62616e65, 0x635f656c, 0x705f6d61, 0x5f737968, 0x746f6f62, 
			0x6f72705f, 0x74636574, 0x006e6f69, 0x746f6f62, 0x6f72705f, 
			0x74636574, 0x635f6465, 0x705f6d61, 0x00737968, 0x64636573, 
			0x5f707369, 0x666e6f63, 0x4c5f6769, 0x64695f4d, 0x65730078, 
			0x73696463, 0x6f635f70, 0x6769666e, 0x6973645f, 0x00000000, 
			0x00000001, 0x00000000, 0x00002070, 0x00000003, 0x4ab00000, 
			0x0b700000, 0x5e400000, 0x01400000, 0x00000000, 0x00000002, 
			0x12800000, 0x00000000, 0x1280000c, 0x00000001, 0x1280001d, 
			0x00000002, 0x12800028, 0x00000003, 0xff00ff00, 0x12800000, 
			0x00000004, 0x1280000c, 0x00000005, 0x1280001d, 0x00000002, 
			0x12800028, 0x00000003, 0xff00ff00, 0x02800038, 0x80000002, 
			0x1480003f, 0x00000000, 0xff00ff00, 0x02800049, 0x00000000, 
			0x02800060, 0x00000002, 0x14800078, 0x0000000c, 0x0280008c, 
			0x00000000, 0x028000a5, 0x45f00000, 0x028000bf, 0x000c0000, 
			0x028000d7, 0x4ab00000, 0x028000f0, 0x5cf00000, 0x02800101, 
			0x02400000, 0xff00ff00, 0x02800110, 0x00000000, 0xff00ff00, 
			0x02800120, 0x00000000, 0x14800140, 0x00000020, 0xff00ff00, 
			0x02800158, 0x00000000, 0x0280016e, 0x00000000, 0xff00ff00 };



const StringDevice driver_list_nicobar_oem_hyp[] = {
			{"/dev/buses/qupac",818130829u, 460, NULL, 0, NULL },
			{"/dev/buses/qupac/15",2116513506u, 496, NULL, 0, NULL },
			{"/dev/hyp_config_oem",4080669576u, 552, NULL, 0, NULL },
			{"/ac/smmu",4045178921u, 628, NULL, 0, NULL },
			{"secure_camera",1688615316u, 640, NULL, 0, NULL },
			{"secdisp_config",3657295525u, 660, NULL, 0, NULL }
};
