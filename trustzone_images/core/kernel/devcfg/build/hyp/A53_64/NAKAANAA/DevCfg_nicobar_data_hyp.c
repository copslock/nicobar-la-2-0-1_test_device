#include "devcfgTypes.h" 

extern const DALSYSPropStructTblType DALPROP_StructPtrs_nicobar_hyp[];

extern const uint32 DALPROP_PropBin_nicobar_hyp[];

extern const StringDevice driver_list_nicobar_hyp[];


const DALProps DALPROP_PropsInfo_nicobar_hyp = {(const byte*)DALPROP_PropBin_nicobar_hyp, DALPROP_StructPtrs_nicobar_hyp, 79, driver_list_nicobar_hyp};
