#include "devcfgTypes.h" 

extern const DALSYSPropStructTblType DALPROP_StructPtrs_nicobar_oem_tz[];

extern const uint32 DALPROP_PropBin_nicobar_oem_tz[];

extern const StringDevice driver_list_nicobar_oem_tz[];


const DALProps DALPROP_PropsInfo_nicobar_oem_tz = {(const byte*)DALPROP_PropBin_nicobar_oem_tz, DALPROP_StructPtrs_nicobar_oem_tz, 36, driver_list_nicobar_oem_tz};
