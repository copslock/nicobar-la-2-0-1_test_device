#include "devcfgTypes.h" 

extern const DALSYSPropStructTblType DALPROP_StructPtrs_nicobar_oem_hyp[];

extern const uint32 DALPROP_PropBin_nicobar_oem_hyp[];

extern const StringDevice driver_list_nicobar_oem_hyp[];


const DALProps DALPROP_PropsInfo_nicobar_oem_hyp = {(const byte*)DALPROP_PropBin_nicobar_oem_hyp, DALPROP_StructPtrs_nicobar_oem_hyp, 6, driver_list_nicobar_oem_hyp};
