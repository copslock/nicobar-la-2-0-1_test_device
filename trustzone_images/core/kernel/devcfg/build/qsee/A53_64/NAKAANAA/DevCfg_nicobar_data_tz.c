#include "devcfgTypes.h" 

extern const DALSYSPropStructTblType DALPROP_StructPtrs_nicobar_tz[];

extern const uint32 DALPROP_PropBin_nicobar_tz[];

extern const StringDevice driver_list_nicobar_tz[];


const DALProps DALPROP_PropsInfo_nicobar_tz = {(const byte*)DALPROP_PropBin_nicobar_tz, DALPROP_StructPtrs_nicobar_tz, 92, driver_list_nicobar_tz};
