#include "DALPropDef.h"
#include "DALDeviceId.h"
#include "DALPropDef.h"
#include "HALvmidmt.h"
#include "HALvmidmtTarget.h"
#include "HALcomdef.h"
#include "com_dtypes.h"
#include "HALvmidmtInfoImpl.h"
#include "HALhwio.h"
#include "HALvmidmtHWIOTarget.h"
#include "msmhwiobase.h"
#include "ddr_defs.h"
#include "tzbsp_target.h"
#include "customer.h"
#include "custnakaanaaa.h"
#include "targnakaanaaa.h"
#include "custremoteapis.h"
#include "custtarget.h"
#include "custsdcc.h"
#include "custsurf.h"
#include "custdiag.h"
#include "custefs.h"
#include "custpmic.h"
#include "custsio_8660.h"
#include "custsec.h"
#include "custsfs.h"
#include "HALqgic.h"
#include "pimem_defs.h"
#include "timer_defs.h"
#include "dbg_cfg.h"
#include "dbg_cfg_arch.h"
#ifndef DEVCFG_STANDALONE 
#include "DALStdDef.h" 
#include "DALSysTypes.h" 

#else 
#include "stubs.h" 

#include "devcfgTypes.h" 

#endif 
extern void * bam_tgt_config_nicobar_tz;
extern void * spmiInfo_nicobar_tz;
extern void * device_qup_0i0_nicobar_tz;
extern void * device_qup_0i0i_nicobar_tz;
extern void * device_qup_0i1_nicobar_tz;
extern void * device_qup_0i1i_nicobar_tz;
extern void * device_qup_0i2_nicobar_tz;
extern void * device_qup_0i2i_nicobar_tz;
extern void * device_qup_0i3_nicobar_tz;
extern void * device_qup_0i3i_nicobar_tz;
extern void * device_qup_0i4_nicobar_tz;
extern void * device_qup_0i4i_nicobar_tz;
extern void * device_qup_0i5_nicobar_tz;
extern void * device_qup_0i5i_nicobar_tz;
extern void * device_qup_0i6_nicobar_tz;
extern void * device_qup_0i6i_nicobar_tz;
extern void * device_qup_0i7_nicobar_tz;
extern void * device_qup_0i7i_nicobar_tz;
extern void * device_qup_0s0_nicobar_tz;
extern void * device_qup_0s0s_nicobar_tz;
extern void * device_qup_0s0g0_nicobar_tz;
extern void * device_qup_0s0g2_nicobar_tz;
extern void * device_qup_0s1_nicobar_tz;
extern void * device_qup_0s1s_nicobar_tz;
extern void * device_qup_0s1g0_nicobar_tz;
extern void * device_qup_0s2_nicobar_tz;
extern void * device_qup_0s2s_nicobar_tz;
extern void * device_qup_0s2g0_nicobar_tz;
extern void * device_qup_0s2g2_nicobar_tz;
extern void * device_qup_0s3_nicobar_tz;
extern void * device_qup_0s3s_nicobar_tz;
extern void * device_qup_0s3g0_nicobar_tz;
extern void * device_qup_0s3g2_nicobar_tz;
extern void * device_qup_0s4_nicobar_tz;
extern void * device_qup_0s4s_nicobar_tz;
extern void * device_qup_0s4g0_nicobar_tz;
extern void * device_qup_0s4g2_nicobar_tz;
extern void * device_qup_0c_nicobar_tz;
extern void * device_qup_0cs_nicobar_tz;
extern void * device_qup_1i0_nicobar_tz;
extern void * device_qup_1i0i_nicobar_tz;
extern void * device_qup_1i1_nicobar_tz;
extern void * device_qup_1i1i_nicobar_tz;
extern void * device_qup_1i2_nicobar_tz;
extern void * device_qup_1i2i_nicobar_tz;
extern void * device_qup_1i3_nicobar_tz;
extern void * device_qup_1i3i_nicobar_tz;
extern void * device_qup_1i4_nicobar_tz;
extern void * device_qup_1i4i_nicobar_tz;
extern void * device_qup_1i5_nicobar_tz;
extern void * device_qup_1i5i_nicobar_tz;
extern void * device_qup_1i6_nicobar_tz;
extern void * device_qup_1i6i_nicobar_tz;
extern void * device_qup_1i7_nicobar_tz;
extern void * device_qup_1i7i_nicobar_tz;
extern void * device_qup_1s0_nicobar_tz;
extern void * device_qup_1s0s_nicobar_tz;
extern void * device_qup_1s0g0_nicobar_tz;
extern void * device_qup_1s0g2_nicobar_tz;
extern void * device_qup_1s1_nicobar_tz;
extern void * device_qup_1s1s_nicobar_tz;
extern void * device_qup_1s1g0_nicobar_tz;
extern void * device_qup_1s1g2_nicobar_tz;
extern void * device_qup_1s2_nicobar_tz;
extern void * device_qup_1s2s_nicobar_tz;
extern void * device_qup_1s2g0_nicobar_tz;
extern void * device_qup_1s3_nicobar_tz;
extern void * device_qup_1s3s_nicobar_tz;
extern void * device_qup_1s3g0_nicobar_tz;
extern void * device_qup_1s3g2_nicobar_tz;
extern void * device_qup_1s4_nicobar_tz;
extern void * device_qup_1s4s_nicobar_tz;
extern void * device_qup_1s4g0_nicobar_tz;
extern void * device_qup_1s4g2_nicobar_tz;
extern void * device_qup_1c_nicobar_tz;
extern void * device_qup_1cs_nicobar_tz;
extern void * device_sensor_pram_nicobar_tz;
extern void * qupv3_se_hw_nicobar_tz;
extern void * qupv3_se_hw_size_nicobar_tz;
extern void * qupv3_common_hw_nicobar_tz;
extern void * qupv3_common_hw_size_nicobar_tz;
extern void * qupv3_0_clocks_arr_nicobar_tz;
extern void * qupv3_0_clocks_arr_size_nicobar_tz;
extern void * qupv3_1_clocks_arr_nicobar_tz;
extern void * qupv3_1_clocks_arr_size_nicobar_tz;
extern void * qupv3_private_arr_nicobar_tz;
extern void * qupv3_private_arr_size_nicobar_tz;
extern void * BIMC_propdata_nicobar_tz;
extern void * NOCERR_propdata_nicobar_tz;
extern void * icbcfg_boot_prop_nicobar_tz;
extern void * map_ddr_regions_nicobar_tz;
extern void * bimc_hal_info_nicobar_tz;
extern void * channel_map_nicobar_tz;
extern void * safe_reset_seg_nicobar_tz;
extern void * info_nicobar_tz;


static HAL_vmidmt_InfoType	devcfg_96={((0x04600000+0x00088000)+0x00000000),{1,0,0,0,0,0}};


static HAL_vmidmt_InfoType	devcfg_97={((0x04a00000+0x000c6000)+0x00000000),{48,0,0,0,0,0}};


static HAL_vmidmt_InfoType	devcfg_98={((0x04c00000+0x000c6000)+0x00000000),{48,0,0,0,0,0}};


static HAL_vmidmt_InfoType	devcfg_99={((0x08000000+0x00088000)+0x00000000),{2,0,0,0,0,0}};


static HAL_vmidmt_InfoType	devcfg_100={((0x08000000+0x00049000)+0x00000000),{2,0,0,0,0,0}};


static HAL_vmidmt_InfoType	devcfg_101={((0x05800000+0x00030000)+0x00000000),{32,0,0,0,0,0}};


static ddr_config_data_t	devcfg_102={4,(0x40000000+0x06B00000),0x0200000,};


static HAL_qgic_BaseAddressType	devcfg_103={0xf200000,0xf400000,0x0,0x0,0xf300000,};


static pimem_reg_data_t	devcfg_104={0x01B60000,0x10000000,419,418,67108864,};


static timer_reg_data_t	devcfg_105={2,7,17,42,0x4000,{0x0F120000,0x0F020000},};


static dbg_cfg_data_t	devcfg_106={0x04407000,0x0F01700C,0x0F011004,0x01490004,306,36,225,458,};


static HAL_vmidmt_InfoType	devcfg_95={((0x01b00000+0x00000000)+0x00000000),{17,0,0,0,0,0}};

const DALSYSPropStructTblType DALPROP_StructPtrs_nicobar_tz[108] =  {
	 {sizeof(void *), &bam_tgt_config_nicobar_tz},
	 {sizeof(void *), &spmiInfo_nicobar_tz},
	 {sizeof(void *), &device_qup_0i0_nicobar_tz},
	 {sizeof(void *), &device_qup_0i0i_nicobar_tz},
	 {sizeof(void *), &device_qup_0i1_nicobar_tz},
	 {sizeof(void *), &device_qup_0i1i_nicobar_tz},
	 {sizeof(void *), &device_qup_0i2_nicobar_tz},
	 {sizeof(void *), &device_qup_0i2i_nicobar_tz},
	 {sizeof(void *), &device_qup_0i3_nicobar_tz},
	 {sizeof(void *), &device_qup_0i3i_nicobar_tz},
	 {sizeof(void *), &device_qup_0i4_nicobar_tz},
	 {sizeof(void *), &device_qup_0i4i_nicobar_tz},
	 {sizeof(void *), &device_qup_0i5_nicobar_tz},
	 {sizeof(void *), &device_qup_0i5i_nicobar_tz},
	 {sizeof(void *), &device_qup_0i6_nicobar_tz},
	 {sizeof(void *), &device_qup_0i6i_nicobar_tz},
	 {sizeof(void *), &device_qup_0i7_nicobar_tz},
	 {sizeof(void *), &device_qup_0i7i_nicobar_tz},
	 {sizeof(void *), &device_qup_0s0_nicobar_tz},
	 {sizeof(void *), &device_qup_0s0s_nicobar_tz},
	 {sizeof(void *), &device_qup_0s0g0_nicobar_tz},
	 {sizeof(void *), &device_qup_0s0g2_nicobar_tz},
	 {sizeof(void *), &device_qup_0s1_nicobar_tz},
	 {sizeof(void *), &device_qup_0s1s_nicobar_tz},
	 {sizeof(void *), &device_qup_0s1g0_nicobar_tz},
	 {sizeof(void *), &device_qup_0s2_nicobar_tz},
	 {sizeof(void *), &device_qup_0s2s_nicobar_tz},
	 {sizeof(void *), &device_qup_0s2g0_nicobar_tz},
	 {sizeof(void *), &device_qup_0s2g2_nicobar_tz},
	 {sizeof(void *), &device_qup_0s3_nicobar_tz},
	 {sizeof(void *), &device_qup_0s3s_nicobar_tz},
	 {sizeof(void *), &device_qup_0s3g0_nicobar_tz},
	 {sizeof(void *), &device_qup_0s3g2_nicobar_tz},
	 {sizeof(void *), &device_qup_0s4_nicobar_tz},
	 {sizeof(void *), &device_qup_0s4s_nicobar_tz},
	 {sizeof(void *), &device_qup_0s4g0_nicobar_tz},
	 {sizeof(void *), &device_qup_0s4g2_nicobar_tz},
	 {sizeof(void *), &device_qup_0c_nicobar_tz},
	 {sizeof(void *), &device_qup_0cs_nicobar_tz},
	 {sizeof(void *), &device_qup_1i0_nicobar_tz},
	 {sizeof(void *), &device_qup_1i0i_nicobar_tz},
	 {sizeof(void *), &device_qup_1i1_nicobar_tz},
	 {sizeof(void *), &device_qup_1i1i_nicobar_tz},
	 {sizeof(void *), &device_qup_1i2_nicobar_tz},
	 {sizeof(void *), &device_qup_1i2i_nicobar_tz},
	 {sizeof(void *), &device_qup_1i3_nicobar_tz},
	 {sizeof(void *), &device_qup_1i3i_nicobar_tz},
	 {sizeof(void *), &device_qup_1i4_nicobar_tz},
	 {sizeof(void *), &device_qup_1i4i_nicobar_tz},
	 {sizeof(void *), &device_qup_1i5_nicobar_tz},
	 {sizeof(void *), &device_qup_1i5i_nicobar_tz},
	 {sizeof(void *), &device_qup_1i6_nicobar_tz},
	 {sizeof(void *), &device_qup_1i6i_nicobar_tz},
	 {sizeof(void *), &device_qup_1i7_nicobar_tz},
	 {sizeof(void *), &device_qup_1i7i_nicobar_tz},
	 {sizeof(void *), &device_qup_1s0_nicobar_tz},
	 {sizeof(void *), &device_qup_1s0s_nicobar_tz},
	 {sizeof(void *), &device_qup_1s0g0_nicobar_tz},
	 {sizeof(void *), &device_qup_1s0g2_nicobar_tz},
	 {sizeof(void *), &device_qup_1s1_nicobar_tz},
	 {sizeof(void *), &device_qup_1s1s_nicobar_tz},
	 {sizeof(void *), &device_qup_1s1g0_nicobar_tz},
	 {sizeof(void *), &device_qup_1s1g2_nicobar_tz},
	 {sizeof(void *), &device_qup_1s2_nicobar_tz},
	 {sizeof(void *), &device_qup_1s2s_nicobar_tz},
	 {sizeof(void *), &device_qup_1s2g0_nicobar_tz},
	 {sizeof(void *), &device_qup_1s3_nicobar_tz},
	 {sizeof(void *), &device_qup_1s3s_nicobar_tz},
	 {sizeof(void *), &device_qup_1s3g0_nicobar_tz},
	 {sizeof(void *), &device_qup_1s3g2_nicobar_tz},
	 {sizeof(void *), &device_qup_1s4_nicobar_tz},
	 {sizeof(void *), &device_qup_1s4s_nicobar_tz},
	 {sizeof(void *), &device_qup_1s4g0_nicobar_tz},
	 {sizeof(void *), &device_qup_1s4g2_nicobar_tz},
	 {sizeof(void *), &device_qup_1c_nicobar_tz},
	 {sizeof(void *), &device_qup_1cs_nicobar_tz},
	 {sizeof(void *), &device_sensor_pram_nicobar_tz},
	 {sizeof(void *), &qupv3_se_hw_nicobar_tz},
	 {sizeof(void *), &qupv3_se_hw_size_nicobar_tz},
	 {sizeof(void *), &qupv3_common_hw_nicobar_tz},
	 {sizeof(void *), &qupv3_common_hw_size_nicobar_tz},
	 {sizeof(void *), &qupv3_0_clocks_arr_nicobar_tz},
	 {sizeof(void *), &qupv3_0_clocks_arr_size_nicobar_tz},
	 {sizeof(void *), &qupv3_1_clocks_arr_nicobar_tz},
	 {sizeof(void *), &qupv3_1_clocks_arr_size_nicobar_tz},
	 {sizeof(void *), &qupv3_private_arr_nicobar_tz},
	 {sizeof(void *), &qupv3_private_arr_size_nicobar_tz},
	 {sizeof(void *), &BIMC_propdata_nicobar_tz},
	 {sizeof(void *), &NOCERR_propdata_nicobar_tz},
	 {sizeof(void *), &icbcfg_boot_prop_nicobar_tz},
	 {sizeof(void *), &map_ddr_regions_nicobar_tz},
	 {sizeof(void *), &bimc_hal_info_nicobar_tz},
	 {sizeof(void *), &channel_map_nicobar_tz},
	 {sizeof(void *), &safe_reset_seg_nicobar_tz},
	 {sizeof(void *), &info_nicobar_tz},
	 {sizeof(devcfg_95), &devcfg_95},
	 {sizeof(devcfg_96), &devcfg_96},
	 {sizeof(devcfg_97), &devcfg_97},
	 {sizeof(devcfg_98), &devcfg_98},
	 {sizeof(devcfg_99), &devcfg_99},
	 {sizeof(devcfg_100), &devcfg_100},
	 {sizeof(devcfg_101), &devcfg_101},
	 {sizeof(devcfg_102), &devcfg_102},
	 {sizeof(devcfg_103), &devcfg_103},
	 {sizeof(devcfg_104), &devcfg_104},
	 {sizeof(devcfg_105), &devcfg_105},
	 {sizeof(devcfg_106), &devcfg_106},
	{0, 0 } 
 };
const uint32 DALPROP_PropBin_nicobar_tz[] = {

			0x00000850, 0x00000020, 0x000002ac, 0x000002bc, 0x000002bc, 
			0x00000001, 0x00000000, 0x00000718, 0x5f6d6162, 0x5f746774, 
			0x666e6f63, 0x6f006769, 0x72656e77, 0x6d707300, 0x65645f69, 
			0x65636976, 0x69640073, 0x70757100, 0x735f3376, 0x77685f65, 
			0x70757100, 0x735f3376, 0x77685f65, 0x7a69735f, 0x75710065, 
			0x5f337670, 0x6d6d6f63, 0x685f6e6f, 0x75710077, 0x5f337670, 
			0x6d6d6f63, 0x685f6e6f, 0x69735f77, 0x7100657a, 0x33767075, 
			0x635f305f, 0x6b636f6c, 0x72615f73, 0x75710072, 0x5f337670, 
			0x6c635f30, 0x736b636f, 0x7272615f, 0x7a69735f, 0x75710065, 
			0x5f337670, 0x6c635f31, 0x736b636f, 0x7272615f, 0x70757100, 
			0x315f3376, 0x6f6c635f, 0x5f736b63, 0x5f727261, 0x657a6973, 
			0x5f736900, 0x5f637373, 0x5f707571, 0x635f6361, 0x69666e6f, 
			0x75735f67, 0x726f7070, 0x00646574, 0x76707571, 0x70675f33, 
			0x755f6f69, 0x6d5f6573, 0x765f6d73, 0x71007265, 0x33767075, 
			0x7363745f, 0x6f735f72, 0x77685f63, 0x7265765f, 0x6464615f, 
			0x75710072, 0x5f337670, 0x62756f64, 0x735f656c, 0x656e6769, 
			0x63690064, 0x6f765f62, 0x676e6974, 0x7265765f, 0x6e6f6973, 
			0x70757100, 0x705f3376, 0x61766972, 0x615f6574, 0x71007272, 
			0x33767075, 0x6972705f, 0x65746176, 0x7272615f, 0x7a69735f, 
			0x61440065, 0x766e456c, 0x72615400, 0x43746567, 0x47006766, 
			0x7443626c, 0x57487478, 0x6574754d, 0x6d754e78, 0x00726562, 
			0x72736374, 0x7361625f, 0x616e5f65, 0x7400656d, 0x5f727363, 
			0x65736162, 0x74756d00, 0x6f5f7865, 0x65736666, 0x77007374, 
			0x65636e6f, 0x66666f5f, 0x73746573, 0x4d494200, 0x72724543, 
			0x505f726f, 0x64706f72, 0x00617461, 0x45434f4e, 0x726f7272, 
			0x6f72505f, 0x74616470, 0x63690061, 0x6f635f62, 0x6769666e, 
			0x7461645f, 0x756e0061, 0x68635f6d, 0x656e6e61, 0x6e00736c, 
			0x735f6d75, 0x656d6765, 0x0073746e, 0x5f70616d, 0x5f726464, 
			0x69676572, 0x635f6e6f, 0x746e756f, 0x70616d00, 0x7264645f, 
			0x6765725f, 0x736e6f69, 0x6d696200, 0x61685f63, 0x6e695f6c, 
			0x63006f66, 0x6e6e6168, 0x6d5f6c65, 0x73007061, 0x5f656661, 
			0x65736572, 0x65735f74, 0x6e690067, 0x61006f66, 0x776f6c6c, 
			0x6172655f, 0x695f6573, 0x65735f6e, 0x65727563, 0x646f6d5f, 
			0x79620065, 0x73736170, 0x6f6f625f, 0x65725f74, 0x69727473, 
			0x00007463, 0x45524f43, 0x504f545f, 0x5253435f, 0x00000000, 
			0x00000001, 0x00000000, 0x00002070, 0x00000007, 0x00040000, 
			0x00041000, 0x00042000, 0x00043000, 0x00044000, 0x00045000, 
			0x00046000, 0x00047000, 0x00000001, 0x000d4000, 0x000d4004, 
			0x12800000, 0x00000000, 0xff00ff00, 0x0280000f, 0x00000001, 
			0x12800015, 0x00000001, 0xff00ff00, 0x12800022, 0x00000002, 
			0xff00ff00, 0x12800022, 0x00000003, 0xff00ff00, 0x12800022, 
			0x00000004, 0xff00ff00, 0x12800022, 0x00000005, 0xff00ff00, 
			0x12800022, 0x00000006, 0xff00ff00, 0x12800022, 0x00000007, 
			0xff00ff00, 0x12800022, 0x00000008, 0xff00ff00, 0x12800022, 
			0x00000009, 0xff00ff00, 0x12800022, 0x0000000a, 0xff00ff00, 
			0x12800022, 0x0000000b, 0xff00ff00, 0x12800022, 0x0000000c, 
			0xff00ff00, 0x12800022, 0x0000000d, 0xff00ff00, 0x12800022, 
			0x0000000e, 0xff00ff00, 0x12800022, 0x0000000f, 0xff00ff00, 
			0x12800022, 0x00000010, 0xff00ff00, 0x12800022, 0x00000011, 
			0xff00ff00, 0x12800022, 0x00000012, 0xff00ff00, 0x12800022, 
			0x00000013, 0xff00ff00, 0x12800022, 0x00000014, 0xff00ff00, 
			0x12800022, 0x00000015, 0xff00ff00, 0x12800022, 0x00000016, 
			0xff00ff00, 0x12800022, 0x00000017, 0xff00ff00, 0x12800022, 
			0x00000018, 0xff00ff00, 0x12800022, 0x00000019, 0xff00ff00, 
			0x12800022, 0x0000001a, 0xff00ff00, 0x12800022, 0x0000001b, 
			0xff00ff00, 0x12800022, 0x0000001c, 0xff00ff00, 0x12800022, 
			0x0000001d, 0xff00ff00, 0x12800022, 0x0000001e, 0xff00ff00, 
			0x12800022, 0x0000001f, 0xff00ff00, 0x12800022, 0x00000020, 
			0xff00ff00, 0x12800022, 0x00000021, 0xff00ff00, 0x12800022, 
			0x00000022, 0xff00ff00, 0x12800022, 0x00000023, 0xff00ff00, 
			0x12800022, 0x00000024, 0xff00ff00, 0x12800022, 0x00000025, 
			0xff00ff00, 0x12800022, 0x00000026, 0xff00ff00, 0x12800022, 
			0x00000027, 0xff00ff00, 0x12800022, 0x00000028, 0xff00ff00, 
			0x12800022, 0x00000029, 0xff00ff00, 0x12800022, 0x0000002a, 
			0xff00ff00, 0x12800022, 0x0000002b, 0xff00ff00, 0x12800022, 
			0x0000002c, 0xff00ff00, 0x12800022, 0x0000002d, 0xff00ff00, 
			0x12800022, 0x0000002e, 0xff00ff00, 0x12800022, 0x0000002f, 
			0xff00ff00, 0x12800022, 0x00000030, 0xff00ff00, 0x12800022, 
			0x00000031, 0xff00ff00, 0x12800022, 0x00000032, 0xff00ff00, 
			0x12800022, 0x00000033, 0xff00ff00, 0x12800022, 0x00000034, 
			0xff00ff00, 0x12800022, 0x00000035, 0xff00ff00, 0x12800022, 
			0x00000036, 0xff00ff00, 0x12800022, 0x00000037, 0xff00ff00, 
			0x12800022, 0x00000038, 0xff00ff00, 0x12800022, 0x00000039, 
			0xff00ff00, 0x12800022, 0x0000003a, 0xff00ff00, 0x12800022, 
			0x0000003b, 0xff00ff00, 0x12800022, 0x0000003c, 0xff00ff00, 
			0x12800022, 0x0000003d, 0xff00ff00, 0x12800022, 0x0000003e, 
			0xff00ff00, 0x12800022, 0x0000003f, 0xff00ff00, 0x12800022, 
			0x00000040, 0xff00ff00, 0x12800022, 0x00000041, 0xff00ff00, 
			0x12800022, 0x00000042, 0xff00ff00, 0x12800022, 0x00000043, 
			0xff00ff00, 0x12800022, 0x00000044, 0xff00ff00, 0x12800022, 
			0x00000045, 0xff00ff00, 0x12800022, 0x00000046, 0xff00ff00, 
			0x12800022, 0x00000047, 0xff00ff00, 0x12800022, 0x00000048, 
			0xff00ff00, 0x12800022, 0x00000049, 0xff00ff00, 0x12800022, 
			0x0000004a, 0xff00ff00, 0x12800022, 0x0000004b, 0xff00ff00, 
			0x12800022, 0x0000004c, 0xff00ff00, 0x12800025, 0x0000004d, 
			0x12800031, 0x0000004e, 0x12800042, 0x0000004f, 0x12800052, 
			0x00000050, 0x12800067, 0x00000051, 0x1280007a, 0x00000052, 
			0x12800092, 0x00000053, 0x128000a5, 0x00000054, 0x028000bd, 
			0x00000000, 0x028000dc, 0x00000000, 0x028000f3, 0x003c8000, 
			0x0280010e, 0x00000001, 0x02800122, 0x00000002, 0x12800135, 
			0x00000055, 0x12800147, 0x00000056, 0xff00ff00, 0x0280015e, 
			0x10000001, 0x14800165, 0x00000000, 0x0280016f, 0x00000009, 
			0xff00ff00, 0x11800184, 0x00000000, 0x02800193, 0x00300000, 
			0x1480019d, 0x0000000c, 0x148001ab, 0x00000030, 0xff00ff00, 
			0x128001b9, 0x00000057, 0xff00ff00, 0x128001cc, 0x00000058, 
			0xff00ff00, 0x128001de, 0x00000059, 0x028001ee, 0x00000001, 
			0x028001fb, 0x00000004, 0x02800208, 0x00000001, 0x1280021d, 
			0x0000005a, 0x1280022d, 0x0000005b, 0x1280023b, 0x0000005c, 
			0x12800247, 0x0000005d, 0xff00ff00, 0x12800256, 0x0000005e, 
			0xff00ff00, 0x0280025b, 0x00000000, 0xff00ff00, 0x12000002, 
			0x0000005f, 0x12000023, 0x00000060, 0x12000018, 0x00000061, 
			0x12000019, 0x00000062, 0x12000021, 0x00000063, 0x12000022, 
			0x00000064, 0x12000028, 0x00000065, 0xff00ff00, 0x12000001, 
			0x00000066, 0xff00ff00, 0x12000001, 0x00000067, 0xff00ff00, 
			0x12000001, 0x00000068, 0xff00ff00, 0x12000001, 0x00000069, 
			0xff00ff00, 0x12000001, 0x0000006a, 0xff00ff00, 0x02800276, 
			0x00000000, 0xff00ff00 };



const StringDevice driver_list_nicobar_tz[] = {
			{"/core/hwengines/bam",1285428979u, 760, NULL, 0, NULL },
			{"DALDEVICEID_SPMI_DEVICE",3290583706u, 772, NULL, 0, NULL },
			{"QUP_0I0",722463235u, 792, NULL, 0, NULL },
			{"QUP_0I0I",2366450348u, 804, NULL, 0, NULL },
			{"QUP_0I1",722463236u, 816, NULL, 0, NULL },
			{"QUP_0I1I",2366450381u, 828, NULL, 0, NULL },
			{"QUP_0I2",722463237u, 840, NULL, 0, NULL },
			{"QUP_0I2I",2366450414u, 852, NULL, 0, NULL },
			{"QUP_0I3",722463238u, 864, NULL, 0, NULL },
			{"QUP_0I3I",2366450447u, 876, NULL, 0, NULL },
			{"QUP_0I4",722463239u, 888, NULL, 0, NULL },
			{"QUP_0I4I",2366450480u, 900, NULL, 0, NULL },
			{"QUP_0I5",722463240u, 912, NULL, 0, NULL },
			{"QUP_0I5I",2366450513u, 924, NULL, 0, NULL },
			{"QUP_0I6",722463241u, 936, NULL, 0, NULL },
			{"QUP_0I6I",2366450546u, 948, NULL, 0, NULL },
			{"QUP_0I7",722463242u, 960, NULL, 0, NULL },
			{"QUP_0I7I",2366450579u, 972, NULL, 0, NULL },
			{"QUP_0S0",722463565u, 984, NULL, 0, NULL },
			{"QUP_0S0S",2366461248u, 996, NULL, 0, NULL },
			{"QUP_0S0G0",783809508u, 1008, NULL, 0, NULL },
			{"QUP_0S0G2",783809510u, 1020, NULL, 0, NULL },
			{"QUP_0S1",722463566u, 1032, NULL, 0, NULL },
			{"QUP_0S1S",2366461281u, 1044, NULL, 0, NULL },
			{"QUP_0S1G0",783810597u, 1056, NULL, 0, NULL },
			{"QUP_0S2",722463567u, 1068, NULL, 0, NULL },
			{"QUP_0S2S",2366461314u, 1080, NULL, 0, NULL },
			{"QUP_0S2G0",783811686u, 1092, NULL, 0, NULL },
			{"QUP_0S2G2",783811688u, 1104, NULL, 0, NULL },
			{"QUP_0S3",722463568u, 1116, NULL, 0, NULL },
			{"QUP_0S3S",2366461347u, 1128, NULL, 0, NULL },
			{"QUP_0S3G0",783812775u, 1140, NULL, 0, NULL },
			{"QUP_0S3G2",783812777u, 1152, NULL, 0, NULL },
			{"QUP_0S4",722463569u, 1164, NULL, 0, NULL },
			{"QUP_0S4S",2366461380u, 1176, NULL, 0, NULL },
			{"QUP_0S4G0",783813864u, 1188, NULL, 0, NULL },
			{"QUP_0S4G2",783813866u, 1200, NULL, 0, NULL },
			{"QUP_0C",3405806445u, 1212, NULL, 0, NULL },
			{"QUP_0CS",722463072u, 1224, NULL, 0, NULL },
			{"QUP_1I0",722464324u, 1236, NULL, 0, NULL },
			{"QUP_1I0I",2366486285u, 1248, NULL, 0, NULL },
			{"QUP_1I1",722464325u, 1260, NULL, 0, NULL },
			{"QUP_1I1I",2366486318u, 1272, NULL, 0, NULL },
			{"QUP_1I2",722464326u, 1284, NULL, 0, NULL },
			{"QUP_1I2I",2366486351u, 1296, NULL, 0, NULL },
			{"QUP_1I3",722464327u, 1308, NULL, 0, NULL },
			{"QUP_1I3I",2366486384u, 1320, NULL, 0, NULL },
			{"QUP_1I4",722464328u, 1332, NULL, 0, NULL },
			{"QUP_1I4I",2366486417u, 1344, NULL, 0, NULL },
			{"QUP_1I5",722464329u, 1356, NULL, 0, NULL },
			{"QUP_1I5I",2366486450u, 1368, NULL, 0, NULL },
			{"QUP_1I6",722464330u, 1380, NULL, 0, NULL },
			{"QUP_1I6I",2366486483u, 1392, NULL, 0, NULL },
			{"QUP_1I7",722464331u, 1404, NULL, 0, NULL },
			{"QUP_1I7I",2366486516u, 1416, NULL, 0, NULL },
			{"QUP_1S0",722464654u, 1428, NULL, 0, NULL },
			{"QUP_1S0S",2366497185u, 1440, NULL, 0, NULL },
			{"QUP_1S0G0",784995429u, 1452, NULL, 0, NULL },
			{"QUP_1S0G2",784995431u, 1464, NULL, 0, NULL },
			{"QUP_1S1",722464655u, 1476, NULL, 0, NULL },
			{"QUP_1S1S",2366497218u, 1488, NULL, 0, NULL },
			{"QUP_1S1G0",784996518u, 1500, NULL, 0, NULL },
			{"QUP_1S1G2",784996520u, 1512, NULL, 0, NULL },
			{"QUP_1S2",722464656u, 1524, NULL, 0, NULL },
			{"QUP_1S2S",2366497251u, 1536, NULL, 0, NULL },
			{"QUP_1S2G0",784997607u, 1548, NULL, 0, NULL },
			{"QUP_1S3",722464657u, 1560, NULL, 0, NULL },
			{"QUP_1S3S",2366497284u, 1572, NULL, 0, NULL },
			{"QUP_1S3G0",784998696u, 1584, NULL, 0, NULL },
			{"QUP_1S3G2",784998698u, 1596, NULL, 0, NULL },
			{"QUP_1S4",722464658u, 1608, NULL, 0, NULL },
			{"QUP_1S4S",2366497317u, 1620, NULL, 0, NULL },
			{"QUP_1S4G0",784999785u, 1632, NULL, 0, NULL },
			{"QUP_1S4G2",784999787u, 1644, NULL, 0, NULL },
			{"QUP_1C",3405806478u, 1656, NULL, 0, NULL },
			{"QUP_1CS",722464161u, 1668, NULL, 0, NULL },
			{"SENSOR_PRAM",3724033006u, 1680, NULL, 0, NULL },
			{"QUPV3_HW_PROP",751796930u, 1692, NULL, 0, NULL },
			{"/dev/core/mproc/smem",1012060316u, 1844, NULL, 0, NULL },
			{"/dev/ABTimeout",814297740u, 1880, NULL, 0, NULL },
			{"/dev/BIMCError",2760514887u, 1880, NULL, 0, NULL },
			{"/dev/NOCError",1518077100u, 1892, NULL, 0, NULL },
			{"/dev/icbcfg/boot",2382255043u, 1904, NULL, 0, NULL },
			{"/icb/uarb",3907582491u, 1972, NULL, 0, NULL },
			{"/storage/rpmb",2005825481u, 1984, NULL, 0, NULL },
			{"/dev/vmidmt",337776915u, 1996, NULL, 0, NULL },
			{"ddr",193489311u, 2056, NULL, 0, NULL },
			{"int_controller",2851810835u, 2068, NULL, 0, NULL },
			{"pimem",270870909u, 2080, NULL, 0, NULL },
			{"timer",275614598u, 2092, NULL, 0, NULL },
			{"dbg_cfg",26643713u, 2104, NULL, 0, NULL },
			{"boot_restrict",2479631688u, 2116, NULL, 0, NULL }
};
