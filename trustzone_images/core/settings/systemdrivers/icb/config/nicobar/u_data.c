/*===========================================================================

FILE:        u_data.c

DESCRIPTION: This file implements the target-specific icb micro driver data

   Copyright (c) 2019 QUALCOMM Technologies, Incorporated.
             All Rights Reserved
             QUALCOMM Proprietary
-------------------------------------------------------------------------------

$Header: //components/rel/core.tz/2.1.1/settings/systemdrivers/icb/config/nicobar/u_data.c#1 $ 
$DateTime: 2019/03/27 02:08:14 $
$Author: pwbldsvc $
$Change: 18706042 $

                      EDIT HISTORY FOR FILE

 Autogenerated for target: Trustzone
 This file was generated using Autogen script: 
 gen.py

 when         who     what, where, why
 ----------   ---     -----------------------------------------------------------
 2019/01/08 Autogen  Autogenerated version

=============================================================================*/
#include "icbuarb.h"
#include "icbuarbi.h"
#include "comdef.h"

/*============================================================================
                          DEFINES
============================================================================*/
/*============================================================================
                   INTERNAL DATA DECLARATIONS
============================================================================*/
/*============================================================================
                         CLOCK DECLARATIONS
============================================================================*/
/* CNOC */
static uint32_t clock_cnoc_reqs[5] = {0};
static icb_clock clock_cnoc_info =
{
  "/clk/cnoc",
  NULL,
  false,
  0,
  5,
  clock_cnoc_reqs,
};

/*============================================================================
                        MASTER DECLARATIONS
============================================================================*/
/* APPS_PROC */
static icb_master master_apps_proc =
{
  ICBID_MASTER_APPSS_PROC,
  { 
    0
  },
  true, /* remote node? */
  8, /* width in bytes */
  0,
  NULL,
};

/* SNOC_BIMC */
static icb_master master_snoc_bimc =
{
  ICBID_MASTER_SNOC_BIMC,
  { 
    0
  },
  true, /* remote node? */
  8, /* width in bytes */
  0,
  NULL,
};

/* SNOC_CNOC */
icb_clock_handle master_snoc_cnoc_clocks[1] =
{
  {
    &clock_cnoc_info,
    0,
  },
};

static icb_master master_snoc_cnoc =
{
  ICBID_MASTER_SNOC_CNOC,
  { 
    0
  },
  false, /* remote node? */
  8, /* width in bytes */
  1,
  master_snoc_cnoc_clocks,
};

/* CRYPTO_C0 */
static icb_master master_crypto_c0 =
{
  ICBID_MASTER_CRYPTO_CORE0,
  { 
    0
  },
  true, /* remote node? */
  650, /* width in bytes */
  0,
  NULL,
};

/* QUP_CORE_MASTER_1 */
static icb_master master_qup_core_master_1 =
{
  ICBID_MASTER_QUP_CORE_0,
  { 
    0
  },
  true, /* remote node? */
  4, /* width in bytes */
  0,
  NULL,
};

/* QUP_CORE_MASTER_2 */
static icb_master master_qup_core_master_2 =
{
  ICBID_MASTER_QUP_CORE_1,
  { 
    0
  },
  true, /* remote node? */
  4, /* width in bytes */
  0,
  NULL,
};

/* ANOC_SNOC */
static icb_master master_anoc_snoc =
{
  ICBID_MASTER_ANOC_SNOC,
  { 
    0
  },
  true, /* remote node? */
  16, /* width in bytes */
  0,
  NULL,
};

/* BIMC_SNOC */
static icb_master master_bimc_snoc =
{
  ICBID_MASTER_BIMC_SNOC,
  { 
    0
  },
  true, /* remote node? */
  8, /* width in bytes */
  0,
  NULL,
};

/* QHM_QUP0 */
static icb_master master_qhm_qup0 =
{
  ICBID_MASTER_QUP_0,
  { 
    0
  },
  true, /* remote node? */
  4, /* width in bytes */
  0,
  NULL,
};

/* QHM_QUP1 */
static icb_master master_qhm_qup1 =
{
  ICBID_MASTER_QUP_1,
  { 
    0
  },
  true, /* remote node? */
  4, /* width in bytes */
  0,
  NULL,
};

/* CR_VIRT_A1NOC */
static icb_master master_cr_virt_a1noc =
{
  ICBID_MASTER_CRVIRT_A1NOC,
  { 
    0
  },
  true, /* remote node? */
  8, /* width in bytes */
  0,
  NULL,
};


/*============================================================================
                        SLAVE DECLARATIONS
============================================================================*/
/* EBI */
static icb_slave slave_ebi =
{
  ICBID_SLAVE_EBI1,
  { 
    0
  },
  true, /* remote node? */
  8, /* width in bytes */
  0,
  NULL,
};

/* BIMC_SNOC */
static icb_slave slave_bimc_snoc =
{
  ICBID_SLAVE_BIMC_SNOC,
  { 
    0
  },
  true, /* remote node? */
  8, /* width in bytes */
  0,
  NULL,
};

/* QHS_CRYPTO0_CFG */
static icb_clock_handle slave_qhs_crypto0_cfg_clocks[1] =
{
  {
    &clock_cnoc_info,
    1,
  },
};

static icb_slave slave_qhs_crypto0_cfg =
{
  ICBID_SLAVE_CRYPTO_0_CFG,
  { 
    0
  },
  false, /* remote node? */
  4, /* width in bytes */
  1,
  slave_qhs_crypto0_cfg_clocks,
};

/* QHS_PRNG */
static icb_clock_handle slave_qhs_prng_clocks[1] =
{
  {
    &clock_cnoc_info,
    2,
  },
};

static icb_slave slave_qhs_prng =
{
  ICBID_SLAVE_PRNG,
  { 
    0
  },
  false, /* remote node? */
  4, /* width in bytes */
  1,
  slave_qhs_prng_clocks,
};

/* QHS_QUP0 */
static icb_clock_handle slave_qhs_qup0_clocks[1] =
{
  {
    &clock_cnoc_info,
    3,
  },
};

static icb_slave slave_qhs_qup0 =
{
  ICBID_SLAVE_QUP_0,
  { 
    0
  },
  false, /* remote node? */
  4, /* width in bytes */
  1,
  slave_qhs_qup0_clocks,
};

/* QHS_QUP1 */
static icb_clock_handle slave_qhs_qup1_clocks[1] =
{
  {
    &clock_cnoc_info,
    4,
  },
};

static icb_slave slave_qhs_qup1 =
{
  ICBID_SLAVE_QUP_1,
  { 
    0
  },
  false, /* remote node? */
  4, /* width in bytes */
  1,
  slave_qhs_qup1_clocks,
};

/* SNOC_CNOC */
static icb_slave slave_snoc_cnoc =
{
  ICBID_SLAVE_SNOC_CNOC,
  { 
    0
  },
  true, /* remote node? */
  8, /* width in bytes */
  0,
  NULL,
};

/* QXS_IMEM */
static icb_slave slave_qxs_imem =
{
  ICBID_SLAVE_IMEM,
  { 
    0
  },
  true, /* remote node? */
  8, /* width in bytes */
  0,
  NULL,
};

/* SNOC_BIMC */
static icb_slave slave_snoc_bimc =
{
  ICBID_SLAVE_SNOC_BIMC,
  { 
    0
  },
  true, /* remote node? */
  16, /* width in bytes */
  0,
  NULL,
};

/* ANOC_SNOC */
static icb_slave slave_anoc_snoc =
{
  ICBID_SLAVE_ANOC_SNOC,
  { 
    0
  },
  true, /* remote node? */
  16, /* width in bytes */
  0,
  NULL,
};

/*============================================================================
                        TOPOLOGY ROUTE DECLARATIONS
============================================================================*/
static icb_pair route_apps_proc_ebi_hops[1] =
{
  {
    &master_apps_proc,
    &slave_ebi
  },
};

static icb_route route_apps_proc_ebi =
{

  ICBID_MASTER_APPSS_PROC,
  ICBID_SLAVE_EBI1,
  1,
  route_apps_proc_ebi_hops
};
 
static icb_pair route_apps_proc_qhs_crypto0_cfg_hops[3] =
{
  {
    &master_apps_proc,
    &slave_bimc_snoc
  },
  {
    &master_bimc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_qhs_crypto0_cfg
  },
};

static icb_route route_apps_proc_qhs_crypto0_cfg =
{

  ICBID_MASTER_APPSS_PROC,
  ICBID_SLAVE_CRYPTO_0_CFG,
  3,
  route_apps_proc_qhs_crypto0_cfg_hops
};
 
static icb_pair route_apps_proc_qhs_prng_hops[3] =
{
  {
    &master_apps_proc,
    &slave_bimc_snoc
  },
  {
    &master_bimc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_qhs_prng
  },
};

static icb_route route_apps_proc_qhs_prng =
{

  ICBID_MASTER_APPSS_PROC,
  ICBID_SLAVE_PRNG,
  3,
  route_apps_proc_qhs_prng_hops
};
 
static icb_pair route_apps_proc_qhs_qup0_hops[3] =
{
  {
    &master_apps_proc,
    &slave_bimc_snoc
  },
  {
    &master_bimc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_qhs_qup0
  },
};

static icb_route route_apps_proc_qhs_qup0 =
{

  ICBID_MASTER_APPSS_PROC,
  ICBID_SLAVE_QUP_0,
  3,
  route_apps_proc_qhs_qup0_hops
};
 
static icb_pair route_apps_proc_qhs_qup1_hops[3] =
{
  {
    &master_apps_proc,
    &slave_bimc_snoc
  },
  {
    &master_bimc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_qhs_qup1
  },
};

static icb_route route_apps_proc_qhs_qup1 =
{

  ICBID_MASTER_APPSS_PROC,
  ICBID_SLAVE_QUP_1,
  3,
  route_apps_proc_qhs_qup1_hops
};
 
static icb_pair route_apps_proc_qxs_imem_hops[2] =
{
  {
    &master_apps_proc,
    &slave_bimc_snoc
  },
  {
    &master_bimc_snoc,
    &slave_qxs_imem
  },
};

static icb_route route_apps_proc_qxs_imem =
{

  ICBID_MASTER_APPSS_PROC,
  ICBID_SLAVE_IMEM,
  2,
  route_apps_proc_qxs_imem_hops
};
 
static icb_pair route_crypto_c0_ebi_hops[4] =
{
  {
    &master_crypto_c0,
    NULL
  },
  {
    &master_cr_virt_a1noc,
    &slave_anoc_snoc
  },
  {
    &master_anoc_snoc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_ebi
  },
};

static icb_route route_crypto_c0_ebi =
{

  ICBID_MASTER_CRYPTO_CORE0,
  ICBID_SLAVE_EBI1,
  4,
  route_crypto_c0_ebi_hops
};
 
static icb_pair route_crypto_c0_qxs_imem_hops[3] =
{
  {
    &master_crypto_c0,
    NULL
  },
  {
    &master_cr_virt_a1noc,
    &slave_anoc_snoc
  },
  {
    &master_anoc_snoc,
    &slave_qxs_imem
  },
};

static icb_route route_crypto_c0_qxs_imem =
{

  ICBID_MASTER_CRYPTO_CORE0,
  ICBID_SLAVE_IMEM,
  3,
  route_crypto_c0_qxs_imem_hops
};
 
static icb_pair route_crypto_c0_snoc_bimc_hops[3] =
{
  {
    &master_crypto_c0,
    NULL
  },
  {
    &master_cr_virt_a1noc,
    &slave_anoc_snoc
  },
  {
    &master_anoc_snoc,
    &slave_snoc_bimc
  },
};

static icb_route route_crypto_c0_snoc_bimc =
{

  ICBID_MASTER_CRYPTO_CORE0,
  ICBID_SLAVE_SNOC_BIMC,
  3,
  route_crypto_c0_snoc_bimc_hops
};
 
static icb_pair route_qhm_qup0_ebi_hops[3] =
{
  {
    &master_qhm_qup0,
    &slave_anoc_snoc
  },
  {
    &master_anoc_snoc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_ebi
  },
};

static icb_route route_qhm_qup0_ebi =
{

  ICBID_MASTER_QUP_0,
  ICBID_SLAVE_EBI1,
  3,
  route_qhm_qup0_ebi_hops
};
 
static icb_pair route_qhm_qup0_qhs_crypto0_cfg_hops[3] =
{
  {
    &master_qhm_qup0,
    &slave_anoc_snoc
  },
  {
    &master_anoc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_qhs_crypto0_cfg
  },
};

static icb_route route_qhm_qup0_qhs_crypto0_cfg =
{

  ICBID_MASTER_QUP_0,
  ICBID_SLAVE_CRYPTO_0_CFG,
  3,
  route_qhm_qup0_qhs_crypto0_cfg_hops
};
 
static icb_pair route_qhm_qup0_qhs_prng_hops[3] =
{
  {
    &master_qhm_qup0,
    &slave_anoc_snoc
  },
  {
    &master_anoc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_qhs_prng
  },
};

static icb_route route_qhm_qup0_qhs_prng =
{

  ICBID_MASTER_QUP_0,
  ICBID_SLAVE_PRNG,
  3,
  route_qhm_qup0_qhs_prng_hops
};
 
static icb_pair route_qhm_qup0_qhs_qup0_hops[3] =
{
  {
    &master_qhm_qup0,
    &slave_anoc_snoc
  },
  {
    &master_anoc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_qhs_qup0
  },
};

static icb_route route_qhm_qup0_qhs_qup0 =
{

  ICBID_MASTER_QUP_0,
  ICBID_SLAVE_QUP_0,
  3,
  route_qhm_qup0_qhs_qup0_hops
};
 
static icb_pair route_qhm_qup0_qhs_qup1_hops[3] =
{
  {
    &master_qhm_qup0,
    &slave_anoc_snoc
  },
  {
    &master_anoc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_qhs_qup1
  },
};

static icb_route route_qhm_qup0_qhs_qup1 =
{

  ICBID_MASTER_QUP_0,
  ICBID_SLAVE_QUP_1,
  3,
  route_qhm_qup0_qhs_qup1_hops
};
 
static icb_pair route_qhm_qup0_qxs_imem_hops[2] =
{
  {
    &master_qhm_qup0,
    &slave_anoc_snoc
  },
  {
    &master_anoc_snoc,
    &slave_qxs_imem
  },
};

static icb_route route_qhm_qup0_qxs_imem =
{

  ICBID_MASTER_QUP_0,
  ICBID_SLAVE_IMEM,
  2,
  route_qhm_qup0_qxs_imem_hops
};
 
static icb_pair route_qhm_qup0_snoc_bimc_hops[2] =
{
  {
    &master_qhm_qup0,
    &slave_anoc_snoc
  },
  {
    &master_anoc_snoc,
    &slave_snoc_bimc
  },
};

static icb_route route_qhm_qup0_snoc_bimc =
{

  ICBID_MASTER_QUP_0,
  ICBID_SLAVE_SNOC_BIMC,
  2,
  route_qhm_qup0_snoc_bimc_hops
};
 
static icb_pair route_qhm_qup1_ebi_hops[3] =
{
  {
    &master_qhm_qup1,
    &slave_anoc_snoc
  },
  {
    &master_anoc_snoc,
    &slave_snoc_bimc
  },
  {
    &master_snoc_bimc,
    &slave_ebi
  },
};

static icb_route route_qhm_qup1_ebi =
{

  ICBID_MASTER_QUP_1,
  ICBID_SLAVE_EBI1,
  3,
  route_qhm_qup1_ebi_hops
};
 
static icb_pair route_qhm_qup1_qhs_crypto0_cfg_hops[3] =
{
  {
    &master_qhm_qup1,
    &slave_anoc_snoc
  },
  {
    &master_anoc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_qhs_crypto0_cfg
  },
};

static icb_route route_qhm_qup1_qhs_crypto0_cfg =
{

  ICBID_MASTER_QUP_1,
  ICBID_SLAVE_CRYPTO_0_CFG,
  3,
  route_qhm_qup1_qhs_crypto0_cfg_hops
};
 
static icb_pair route_qhm_qup1_qhs_prng_hops[3] =
{
  {
    &master_qhm_qup1,
    &slave_anoc_snoc
  },
  {
    &master_anoc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_qhs_prng
  },
};

static icb_route route_qhm_qup1_qhs_prng =
{

  ICBID_MASTER_QUP_1,
  ICBID_SLAVE_PRNG,
  3,
  route_qhm_qup1_qhs_prng_hops
};
 
static icb_pair route_qhm_qup1_qhs_qup0_hops[3] =
{
  {
    &master_qhm_qup1,
    &slave_anoc_snoc
  },
  {
    &master_anoc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_qhs_qup0
  },
};

static icb_route route_qhm_qup1_qhs_qup0 =
{

  ICBID_MASTER_QUP_1,
  ICBID_SLAVE_QUP_0,
  3,
  route_qhm_qup1_qhs_qup0_hops
};
 
static icb_pair route_qhm_qup1_qhs_qup1_hops[3] =
{
  {
    &master_qhm_qup1,
    &slave_anoc_snoc
  },
  {
    &master_anoc_snoc,
    &slave_snoc_cnoc
  },
  {
    &master_snoc_cnoc,
    &slave_qhs_qup1
  },
};

static icb_route route_qhm_qup1_qhs_qup1 =
{

  ICBID_MASTER_QUP_1,
  ICBID_SLAVE_QUP_1,
  3,
  route_qhm_qup1_qhs_qup1_hops
};
 
static icb_pair route_qhm_qup1_qxs_imem_hops[2] =
{
  {
    &master_qhm_qup1,
    &slave_anoc_snoc
  },
  {
    &master_anoc_snoc,
    &slave_qxs_imem
  },
};

static icb_route route_qhm_qup1_qxs_imem =
{

  ICBID_MASTER_QUP_1,
  ICBID_SLAVE_IMEM,
  2,
  route_qhm_qup1_qxs_imem_hops
};
 
static icb_pair route_qhm_qup1_snoc_bimc_hops[2] =
{
  {
    &master_qhm_qup1,
    &slave_anoc_snoc
  },
  {
    &master_anoc_snoc,
    &slave_snoc_bimc
  },
};

static icb_route route_qhm_qup1_snoc_bimc =
{

  ICBID_MASTER_QUP_1,
  ICBID_SLAVE_SNOC_BIMC,
  2,
  route_qhm_qup1_snoc_bimc_hops
};
 
static icb_pair route_qup_core_master_1_qup_core_slave_1_hops[1] =
{
  {
    &master_qup_core_master_1,
    NULL
  },
};

static icb_route route_qup_core_master_1_qup_core_slave_1 =
{

  ICBID_MASTER_QUP_CORE_0,
  ICBID_SLAVE_QUP_CORE_0,
  1,
  route_qup_core_master_1_qup_core_slave_1_hops
};
 
static icb_pair route_qup_core_master_2_qup_core_slave_2_hops[1] =
{
  {
    &master_qup_core_master_2,
    NULL
  },
};

static icb_route route_qup_core_master_2_qup_core_slave_2 =
{

  ICBID_MASTER_QUP_CORE_1,
  ICBID_SLAVE_QUP_CORE_1,
  1,
  route_qup_core_master_2_qup_core_slave_2_hops
};
 
static icb_pair route_snoc_bimc_ebi_hops[1] =
{
  {
    &master_snoc_bimc,
    &slave_ebi
  },
};

static icb_route route_snoc_bimc_ebi =
{

  ICBID_MASTER_SNOC_BIMC,
  ICBID_SLAVE_EBI1,
  1,
  route_snoc_bimc_ebi_hops
};
 
/*============================================================================
                             LISTS
============================================================================*/
static icb_clock *icb_clock_list[1]=
{
  &clock_cnoc_info,
};

/*============================================================================
                   EXTERNAL DATA DECLARATIONS
============================================================================*/
static icb_route *icb_route_list[26] =
{
  &route_apps_proc_ebi,
  &route_apps_proc_qhs_crypto0_cfg,
  &route_apps_proc_qhs_prng,
  &route_apps_proc_qhs_qup0,
  &route_apps_proc_qhs_qup1,
  &route_apps_proc_qxs_imem,
  &route_crypto_c0_ebi,
  &route_crypto_c0_qxs_imem,
  &route_crypto_c0_snoc_bimc,
  &route_qhm_qup0_ebi,
  &route_qhm_qup0_qhs_crypto0_cfg,
  &route_qhm_qup0_qhs_prng,
  &route_qhm_qup0_qhs_qup0,
  &route_qhm_qup0_qhs_qup1,
  &route_qhm_qup0_qxs_imem,
  &route_qhm_qup0_snoc_bimc,
  &route_qhm_qup1_ebi,
  &route_qhm_qup1_qhs_crypto0_cfg,
  &route_qhm_qup1_qhs_prng,
  &route_qhm_qup1_qhs_qup0,
  &route_qhm_qup1_qhs_qup1,
  &route_qhm_qup1_qxs_imem,
  &route_qhm_qup1_snoc_bimc,
  &route_qup_core_master_1_qup_core_slave_1,
  &route_qup_core_master_2_qup_core_slave_2,
  &route_snoc_bimc_ebi,
};

const icb_info info =
{
  26,
  icb_route_list,
  1,
  icb_clock_list,
};
