/*==============================================================================

FILE:      NOC_error_data.c

DESCRIPTION: This file contains target/platform specific configuration data.

PUBLIC CLASSES:  Not Applicable

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A
 
Edit History

//#CHANGE - Update when put in the depot
$Header: //components/rel/core.tz/2.1.1/settings/systemdrivers/icb/config/nicobar/NOC_error_data.c#1 $ 
$DateTime: 2019/03/27 02:08:14 $
$Author: pwbldsvc $
$Change: 18706042 $ 

When        Who    What, where, why
----------  ---    -----------------------------------------------------------
2015/05/07  tb     Port to 8952
2013/10/30  pm     Port to 8916
2012/10/03  av     Created
 
        Copyright (c) 2012-2018 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary/GTDR
==============================================================================*/
#include "NOC_error.h"
#include "NOC_error_HWIO.h"
#include "HALhwio.h"

/*============================================================================
                        DEVICE CONFIG PROPERTY DATA
============================================================================*/

/* Internal NOC Configuration Data*/
static NOCERR_info_type NOCERR_cfgdata[] = 
{ 

  [0] = {
    .name            = "CONFIG_NOC",
    .hw              = &QNOC_4_0,
    .base_addr       = (void *)HWIO_ADDR(CNOC_OBS_SWID_LOW),
    .intr_vector     = 282,
    .num_sbms        = 1,
    .sb_hw           = (NOC_sideband_hw_type *[]){ &QNOC_SB_4_0 },
    .sb_base_addrs   = (void *[]){ (void *)HWIO_ADDR(CNOC_CENTER_SBM_SWID_LOW) },
    .num_tos         = 1,
    .to_addrs        = (void *[]){ (void *)HWIO_ADDR(CNOC_CENTER_SBM_FLAGOUTSET0_LOW)},
    .syndrome        = { .sbms = (NOCERR_sbm_syndrome_type []){ {0, 0} }, },
  },
  [1] = {
    .name            = "SYSTEM_NOC",
    .hw              = &QNOC_4_0,
    .base_addr       = (void *)HWIO_ADDR(SNOC_OBS_ERRORLOGGER_SWID_LOW),
    .intr_vector     = 281,
    .num_sbms        = 1,
    .sb_hw           = (NOC_sideband_hw_type *[]){ &QNOC_SB_4_0_L },
    .sb_base_addrs   = (void *[]){ (void *)HWIO_ADDR(SNOC_CENTER_SBM_SWID_LOW) },
    .num_tos         = 1,
    .to_addrs        = (void *[]){ (void *)HWIO_ADDR(SNOC_CENTER_SBM_FLAGOUTSET0_LOW) },
    .syndrome        = { .sbms = (NOCERR_sbm_syndrome_type []){ {0, 0} }, },
  },
};

static void *clock_reg_addrs[] =
{
  (void *)HWIO_ADDR(GCC_NOC_BUS_TIMEOUT_EXTREF_CBCR),
  (void *)HWIO_ADDR(GCC_NOC_BUS_TIMEOUT_EXTREF_CDIVR),
  (void *)HWIO_ADDR(GCC_NOC_BUS_TIMEOUT_EXTREF_DIV512_CDIVR)
};

NOCERR_config_info_type NOCERR_propdata =
{
  .num_configs = 1,
  .configs = (NOCERR_propdata_type [])
{
      /* Target info: NICOBAR v1.0 and later */
      [0] = 

{
          .family          = CHIPINFO_FAMILY_NICOBAR,
          .match           = false,
          .version         = CHIPINFO_VERSION(1,0),
          /* NoC info. */
          .len             = sizeof(NOCERR_cfgdata)/sizeof(NOCERR_info_type),
          .NOCInfo         = NOCERR_cfgdata,
          .num_clock_regs  = sizeof(clock_reg_addrs)/sizeof(clock_reg_addrs[0]),
          .clock_reg_addrs = clock_reg_addrs,
        },
    },
};
