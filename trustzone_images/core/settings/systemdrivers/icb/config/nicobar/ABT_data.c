/*==============================================================================

FILE:      ABT_data.c

DESCRIPTION: This file contains target/platform specific configuration data.

PUBLIC CLASSES:  Not Applicable

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A
 
Edit History

//#CHANGE - Update when put in the depot
$Header: //components/rel/core.tz/2.1.1/settings/systemdrivers/icb/config/nicobar/ABT_data.c#1 $ 
$DateTime: 2019/03/27 02:08:14 $
$Author: pwbldsvc $
$Change: 18706042 $ 

When        Who    What, where, why
----------  ---    ----------------------------------------------------------- 
2015/09/14  ddk    Port for 8937
2015/05/07  tb     Port for 8952
2014/12/09  tb     Split OEM modifiable data from internal data
2013/11/14  tb     Added support for multiple enable/status registers
2013/10/30  pm     Port to MSM8916 
2013/04/16  pm     Added slot for interrupt priority  
2012/10/04  av     Support for disabling ABT 
2012/05/31  av     Created
 
        Copyright (c) 2012-2018 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary/GTDR
==============================================================================*/
#include "ABTimeout.h"

/*============================================================================
                      TARGET AND PLATFORM SPECIFIC DATA
============================================================================*/

/* Base address for devices */





/*============================================================================
                        DEVICE CONFIG PROPERTY DATA
============================================================================*/




/* Internal ABT Configuration Property Data*/
const ABT_propdata_type ABT_propdata =  
{
0
};

