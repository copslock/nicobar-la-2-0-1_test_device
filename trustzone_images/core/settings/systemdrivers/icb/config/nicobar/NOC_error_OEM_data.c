/*==============================================================================

FILE:      NOC_error_OEM_data.c

DESCRIPTION: This file contains target/platform specific configuration data.

PUBLIC CLASSES:  Not Applicable

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A
 
Edit History

//#CHANGE - Update when put in the depot
$Header: //components/rel/core.tz/2.1.1/settings/systemdrivers/icb/config/nicobar/NOC_error_OEM_data.c#4 $ 
$DateTime: 2019/06/17 14:00:46 $
$Author: pwbldsvc $
$Change: 19559926 $ 

When        Who    What, where, why
----------  ---    -----------------------------------------------------------
2017/10/02  sds    Add support for chip versioned configs
2017/04/11  sds    Enable MMSS NOC
2017/03/01  sds    Add timeout enable register values
2017/02/07  sds    Created
 
        Copyright (c) 2017-2018 Qualcomm Technologies, Inc.
               All Rights Reserved.
            QUALCOMM Proprietary/GTDR
==============================================================================*/
#include "NOC_error.h"
#include "NOC_error_HWIO.h"
#include <stddef.h>

#define OFFSET(member) (uint32_t) offsetof(NOCERR_syndrome_type, member)
/*============================================================================
                           TARGET SPECIFIC DATA
============================================================================*/

/*============================================================================
                        DEVICE CONFIG PROPERTY DATA
============================================================================*/

/* OEM NOC Configuration Data*/
NOCERR_info_type_oem NOCERR_cfgdata_oem[] = 
{ 
  [0] = {
    .name        = "CONFIG_NOC",
    .intr_enable = true,
    .error_fatal = true,
    .sbms        = (NOCERR_sbm_info_type_oem []){ 
        [0] = { .faultin_en0_low  = 0xFFEFFFFD,
                .faultin_en0_high = 0xB0FFFFFF},
      },
    .obs_mask    = (NOCERR_sbm_info_type_oem []){ 
        [0] = { .faultin_en0_low = 0x1,
                .faultin_en0_high = 0x0}, 
      },
    .to_reg_vals = (uint32_t []){ 0x7FFC},
  },

  [1] = {
    .name        = "SYSTEM_NOC",
    .intr_enable = true,
    .error_fatal = true,
    .sbms        = (NOCERR_sbm_info_type_oem []){ 
        [0] = { .faultin_en0_low = 0x400FB}, 
      },
    .obs_mask    = (NOCERR_sbm_info_type_oem []){ 
        [0] = { .faultin_en0_low = 0x1}, 
      },
    .to_reg_vals = (uint32_t []){ 0xEE0 },
    .filter_len  = 7,
    .filter_info = (FILTER_info_type [])
    {
        [0]=
        {
            .len    = 1,
            .checks = (CHECK_info_type []){ 
              [0] = { .offs = OFFSET(ERRLOG1_HIGH), //NOCERR_syndrome_type.
                      .mask = 0xFF00,
                      .val = 0x2A00 /*cpp*/ },
                    },
        },
        [1]=
        {
            .len    = 1,
            .checks = (CHECK_info_type []){ 
              [0] = { .offs = OFFSET(ERRLOG1_HIGH), //NOCERR_syndrome_type.
                      .mask = 0xFF00,
                      .val = 0x2200 /*jpeg*/ },
                    },
        },
        [2]=
        {
            .len    = 1,
            .checks = (CHECK_info_type []){ 
              [0] = { .offs = OFFSET(ERRLOG1_HIGH), //NOCERR_syndrome_type.
                      .mask = 0xFF00,
                      .val = 0x2300 /*mdp0*/ },
                    },
        },
        [3]=
        {
            .len    = 1,
            .checks = (CHECK_info_type []){ 
              [0] = { .offs = OFFSET(ERRLOG1_HIGH), //NOCERR_syndrome_type.
                      .mask = 0xFF00,
                      .val = 0x2600 /*venus0*/ },
                    },
        },
        [4]=
        {
            .len    = 1,
            .checks = (CHECK_info_type []){ 
              [0] = { .offs = OFFSET(ERRLOG1_HIGH), //NOCERR_syndrome_type.
                      .mask = 0xFF00,
                      .val = 0x3500 /*venus_arm*/ },
                    },
        },
        [5]=
        {
            .len    = 1,
            .checks = (CHECK_info_type []){ 
              [0] = { .offs = OFFSET(ERRLOG1_HIGH), //NOCERR_syndrome_type.
                      .mask = 0xFF00,
                      .val = 0x2700 /*vfe0*/ },
                    },
        },
        [6]=
        {
            .len    = 1,
            .checks = (CHECK_info_type []){ 
              [0] = { .offs = OFFSET(ERRLOG1_HIGH), //NOCERR_syndrome_type.
                      .mask = 0xFF00,
                      .val = 0x2800 /*wlan*/ },
                    },
        }
    }
  },
};
uint32_t clock_reg_vals[] =
{
  [0] = 0x1, /* Enable timeout reference clock */
  [1] = 0x3, /* Set timeout divider to maximum, timeout = ~6.8 ms */
  [2] = 0xF,
};

NOCERR_config_info_type_oem NOCERR_propdata_oem =
{
  .num_configs = 1,
  .configs = (NOCERR_propdata_type_oem [])
    {
      /* Target info: NICOBAR v1.0 */
      [0] = 
        {
          .family          = CHIPINFO_FAMILY_NICOBAR,
          .match           = false,
          .version         = CHIPINFO_VERSION(1,0),

          .len            = sizeof(NOCERR_cfgdata_oem)/sizeof(NOCERR_info_type_oem),
          .NOCInfoOEM     = NOCERR_cfgdata_oem,
          .clock_reg_vals = clock_reg_vals,
        },
    },
};
