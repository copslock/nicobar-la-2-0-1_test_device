#ifndef __NOC_ERROR_HWIO_H__
#define __NOC_ERROR_HWIO_H__
/*
===========================================================================
*/
/**
  @file NOC_error_HWIO.h
  @brief Auto-generated HWIO interface include file.

  Reference chip release:
    (Nicobar) [nicobar_v1.0_p3q1r91_F02]
 
  This file contains HWIO register definitions for the following modules:
    SYSTEM_NOC
    CONFIG_NOC
    APCS_CCI_GLADIATOR_NOC

  'Include' filters applied: OBS_ERRORLOGGER_SWID_LOW[SYSTEM_NOC] SBM_SWID_LOW[SYSTEM_NOC] SBM_FLAGOUTSET0[SYSTEM_NOC] SBM_FLAGOUTSET1[SYSTEM_NOC] 

  Generation parameters: 
  { u'filename': u'NOC_error_HWIO.h',
    u'header': u'#include "msmhwiobase.h"',
    u'module-filter-exclude': { },
    u'module-filter-include': { u'SYSTEM_NOC': [ u'OBS_ERRORLOGGER_SWID_LOW',
                                                 u'SBM_SWID_LOW',
                                                 u'SBM_FLAGOUTSET0',
                                                 u'SBM_FLAGOUTSET1']},
    u'modules': [u'SYSTEM_NOC', u'CONFIG_NOC', u'APCS_CCI_GLADIATOR_NOC']}
*/
/*
  ===========================================================================

  Copyright (c) 2018 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies, Inc. and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies, Inc.

  ===========================================================================

  $Header: //components/rel/core.tz/2.1.1/settings/systemdrivers/icb/config/nicobar/NOC_error_HWIO.h#1 $
  $DateTime: 2019/03/27 02:08:14 $
  $Author: pwbldsvc $

  ===========================================================================
*/

#include "msmhwiobase.h"

/*----------------------------------------------------------------------------
 * MODULE: GCC_CLK_CTL_REG
 *--------------------------------------------------------------------------*/

#define GCC_CLK_CTL_REG_REG_BASE                                                                    (CLK_CTL_BASE      + 0x00000000)
#define GCC_CLK_CTL_REG_REG_BASE_SIZE                                                               0x1f0000
#define GCC_CLK_CTL_REG_REG_BASE_USED                                                               0xc7000

#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_BCR_ADDR                                                    (GCC_CLK_CTL_REG_REG_BASE      + 0x0002c000)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_BCR_RMSK                                                           0x1
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_BCR_IN          \
        in_dword(HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_BCR_ADDR)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_BCR_INM(m)      \
        in_dword_masked(HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_BCR_ADDR, m)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_BCR_OUT(v)      \
        out_dword(HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_BCR_ADDR,v)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_BCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_BCR_ADDR,m,v,HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_BCR_IN)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_BCR_BLK_ARES_BMSK                                                  0x1
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_BCR_BLK_ARES_SHFT                                                  0x0

#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CBCR_ADDR                                                   (GCC_CLK_CTL_REG_REG_BASE      + 0x0002c004)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CBCR_RMSK                                                   0x80000005
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CBCR_IN          \
        in_dword(HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CBCR_ADDR)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CBCR_INM(m)      \
        in_dword_masked(HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CBCR_ADDR, m)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CBCR_OUT(v)      \
        out_dword(HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CBCR_ADDR,v)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CBCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CBCR_ADDR,m,v,HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CBCR_IN)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CBCR_CLK_OFF_BMSK                                           0x80000000
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CBCR_CLK_OFF_SHFT                                                 0x1f
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CBCR_CLK_ARES_BMSK                                                 0x4
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CBCR_CLK_ARES_SHFT                                                 0x2
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CBCR_CLK_ENABLE_BMSK                                               0x1
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CBCR_CLK_ENABLE_SHFT                                               0x0

#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CDIVR_ADDR                                                  (GCC_CLK_CTL_REG_REG_BASE      + 0x0002c008)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CDIVR_RMSK                                                         0xf
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CDIVR_IN          \
        in_dword(HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CDIVR_ADDR)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CDIVR_INM(m)      \
        in_dword_masked(HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CDIVR_ADDR, m)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CDIVR_OUT(v)      \
        out_dword(HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CDIVR_ADDR,v)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CDIVR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CDIVR_ADDR,m,v,HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CDIVR_IN)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CDIVR_CLK_DIV_BMSK                                                 0xf
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_CDIVR_CLK_DIV_SHFT                                                 0x0

#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_DIV512_CDIVR_ADDR                                           (GCC_CLK_CTL_REG_REG_BASE      + 0x0002c00c)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_DIV512_CDIVR_RMSK                                                0x1ff
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_DIV512_CDIVR_IN          \
        in_dword(HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_DIV512_CDIVR_ADDR)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_DIV512_CDIVR_INM(m)      \
        in_dword_masked(HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_DIV512_CDIVR_ADDR, m)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_DIV512_CDIVR_OUT(v)      \
        out_dword(HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_DIV512_CDIVR_ADDR,v)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_DIV512_CDIVR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_DIV512_CDIVR_ADDR,m,v,HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_DIV512_CDIVR_IN)
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_DIV512_CDIVR_CLK_DIV_BMSK                                        0x1ff
#define HWIO_GCC_NOC_BUS_TIMEOUT_EXTREF_DIV512_CDIVR_CLK_DIV_SHFT                                          0x0

/*----------------------------------------------------------------------------
 * MODULE: SYSTEM_NOC
 *--------------------------------------------------------------------------*/

#define SYSTEM_NOC_REG_BASE                                                                            (SYSTEM_NOC_BASE      + 0x00000000)
#define SYSTEM_NOC_REG_BASE_SIZE                                                                       0x60200
#define SYSTEM_NOC_REG_BASE_USED                                                                       0x60100

#define HWIO_SNOC_OBS_ERRORLOGGER_SWID_LOW_ADDR                                                        (SYSTEM_NOC_REG_BASE      + 0x00000100)
#define HWIO_SNOC_OBS_ERRORLOGGER_SWID_LOW_RMSK                                                          0xffffff
#define HWIO_SNOC_OBS_ERRORLOGGER_SWID_LOW_IN          \
        in_dword(HWIO_SNOC_OBS_ERRORLOGGER_SWID_LOW_ADDR)
#define HWIO_SNOC_OBS_ERRORLOGGER_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_SNOC_OBS_ERRORLOGGER_SWID_LOW_ADDR, m)
#define HWIO_SNOC_OBS_ERRORLOGGER_SWID_LOW_UNITTYPEID_BMSK                                               0xff0000
#define HWIO_SNOC_OBS_ERRORLOGGER_SWID_LOW_UNITTYPEID_SHFT                                                   0x10
#define HWIO_SNOC_OBS_ERRORLOGGER_SWID_LOW_UNITCONFID_BMSK                                                 0xffff
#define HWIO_SNOC_OBS_ERRORLOGGER_SWID_LOW_UNITCONFID_SHFT                                                    0x0

#define HWIO_SNOC_CENTER_SBM_SWID_LOW_ADDR                                                             (SYSTEM_NOC_REG_BASE      + 0x00000200)
#define HWIO_SNOC_CENTER_SBM_SWID_LOW_RMSK                                                               0xffffff
#define HWIO_SNOC_CENTER_SBM_SWID_LOW_IN          \
        in_dword(HWIO_SNOC_CENTER_SBM_SWID_LOW_ADDR)
#define HWIO_SNOC_CENTER_SBM_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_SNOC_CENTER_SBM_SWID_LOW_ADDR, m)
#define HWIO_SNOC_CENTER_SBM_SWID_LOW_UNITTYPEID_BMSK                                                    0xff0000
#define HWIO_SNOC_CENTER_SBM_SWID_LOW_UNITTYPEID_SHFT                                                        0x10
#define HWIO_SNOC_CENTER_SBM_SWID_LOW_UNITCONFID_BMSK                                                      0xffff
#define HWIO_SNOC_CENTER_SBM_SWID_LOW_UNITCONFID_SHFT                                                         0x0

#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_ADDR                                                      (SYSTEM_NOC_REG_BASE      + 0x00000288)
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_RMSK                                                          0xffff
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_OUT(v)      \
        out_dword(HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_ADDR,v)
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT15_BMSK                                                   0x8000
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT15_SHFT                                                      0xf
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT14_BMSK                                                   0x4000
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT14_SHFT                                                      0xe
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT13_BMSK                                                   0x2000
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT13_SHFT                                                      0xd
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT12_BMSK                                                   0x1000
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT12_SHFT                                                      0xc
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT11_BMSK                                                    0x800
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT11_SHFT                                                      0xb
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT10_BMSK                                                    0x400
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT10_SHFT                                                      0xa
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT9_BMSK                                                     0x200
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT9_SHFT                                                       0x9
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT8_BMSK                                                     0x100
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT8_SHFT                                                       0x8
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT7_BMSK                                                      0x80
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT7_SHFT                                                       0x7
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT6_BMSK                                                      0x40
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT6_SHFT                                                       0x6
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT5_BMSK                                                      0x20
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT5_SHFT                                                       0x5
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT4_BMSK                                                      0x10
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT4_SHFT                                                       0x4
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT3_BMSK                                                       0x8
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT3_SHFT                                                       0x3
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT2_BMSK                                                       0x4
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT2_SHFT                                                       0x2
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT1_BMSK                                                       0x2
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT1_SHFT                                                       0x1
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT0_BMSK                                                       0x1
#define HWIO_SNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT0_SHFT                                                       0x0

#define HWIO_SNOC_EAST_SBM_SWID_LOW_ADDR                                                               (SYSTEM_NOC_REG_BASE      + 0x00000400)
#define HWIO_SNOC_EAST_SBM_SWID_LOW_RMSK                                                                 0xffffff
#define HWIO_SNOC_EAST_SBM_SWID_LOW_IN          \
        in_dword(HWIO_SNOC_EAST_SBM_SWID_LOW_ADDR)
#define HWIO_SNOC_EAST_SBM_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_SNOC_EAST_SBM_SWID_LOW_ADDR, m)
#define HWIO_SNOC_EAST_SBM_SWID_LOW_UNITTYPEID_BMSK                                                      0xff0000
#define HWIO_SNOC_EAST_SBM_SWID_LOW_UNITTYPEID_SHFT                                                          0x10
#define HWIO_SNOC_EAST_SBM_SWID_LOW_UNITCONFID_BMSK                                                        0xffff
#define HWIO_SNOC_EAST_SBM_SWID_LOW_UNITCONFID_SHFT                                                           0x0

#define HWIO_SNOC_MM_NRT_SBM_SWID_LOW_ADDR                                                             (SYSTEM_NOC_REG_BASE      + 0x00000600)
#define HWIO_SNOC_MM_NRT_SBM_SWID_LOW_RMSK                                                               0xffffff
#define HWIO_SNOC_MM_NRT_SBM_SWID_LOW_IN          \
        in_dword(HWIO_SNOC_MM_NRT_SBM_SWID_LOW_ADDR)
#define HWIO_SNOC_MM_NRT_SBM_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_SNOC_MM_NRT_SBM_SWID_LOW_ADDR, m)
#define HWIO_SNOC_MM_NRT_SBM_SWID_LOW_UNITTYPEID_BMSK                                                    0xff0000
#define HWIO_SNOC_MM_NRT_SBM_SWID_LOW_UNITTYPEID_SHFT                                                        0x10
#define HWIO_SNOC_MM_NRT_SBM_SWID_LOW_UNITCONFID_BMSK                                                      0xffff
#define HWIO_SNOC_MM_NRT_SBM_SWID_LOW_UNITCONFID_SHFT                                                         0x0

#define HWIO_SNOC_MM_RT_SBM_SWID_LOW_ADDR                                                              (SYSTEM_NOC_REG_BASE      + 0x00000800)
#define HWIO_SNOC_MM_RT_SBM_SWID_LOW_RMSK                                                                0xffffff
#define HWIO_SNOC_MM_RT_SBM_SWID_LOW_IN          \
        in_dword(HWIO_SNOC_MM_RT_SBM_SWID_LOW_ADDR)
#define HWIO_SNOC_MM_RT_SBM_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_SNOC_MM_RT_SBM_SWID_LOW_ADDR, m)
#define HWIO_SNOC_MM_RT_SBM_SWID_LOW_UNITTYPEID_BMSK                                                     0xff0000
#define HWIO_SNOC_MM_RT_SBM_SWID_LOW_UNITTYPEID_SHFT                                                         0x10
#define HWIO_SNOC_MM_RT_SBM_SWID_LOW_UNITCONFID_BMSK                                                       0xffff
#define HWIO_SNOC_MM_RT_SBM_SWID_LOW_UNITCONFID_SHFT                                                          0x0

#define HWIO_SNOC_MONAQ_SBM_SWID_LOW_ADDR                                                              (SYSTEM_NOC_REG_BASE      + 0x00000a00)
#define HWIO_SNOC_MONAQ_SBM_SWID_LOW_RMSK                                                                0xffffff
#define HWIO_SNOC_MONAQ_SBM_SWID_LOW_IN          \
        in_dword(HWIO_SNOC_MONAQ_SBM_SWID_LOW_ADDR)
#define HWIO_SNOC_MONAQ_SBM_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_SNOC_MONAQ_SBM_SWID_LOW_ADDR, m)
#define HWIO_SNOC_MONAQ_SBM_SWID_LOW_UNITTYPEID_BMSK                                                     0xff0000
#define HWIO_SNOC_MONAQ_SBM_SWID_LOW_UNITTYPEID_SHFT                                                         0x10
#define HWIO_SNOC_MONAQ_SBM_SWID_LOW_UNITCONFID_BMSK                                                       0xffff
#define HWIO_SNOC_MONAQ_SBM_SWID_LOW_UNITCONFID_SHFT                                                          0x0

#define HWIO_SNOC_NORTH_SBM_SWID_LOW_ADDR                                                              (SYSTEM_NOC_REG_BASE      + 0x00000c00)
#define HWIO_SNOC_NORTH_SBM_SWID_LOW_RMSK                                                                0xffffff
#define HWIO_SNOC_NORTH_SBM_SWID_LOW_IN          \
        in_dword(HWIO_SNOC_NORTH_SBM_SWID_LOW_ADDR)
#define HWIO_SNOC_NORTH_SBM_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_SNOC_NORTH_SBM_SWID_LOW_ADDR, m)
#define HWIO_SNOC_NORTH_SBM_SWID_LOW_UNITTYPEID_BMSK                                                     0xff0000
#define HWIO_SNOC_NORTH_SBM_SWID_LOW_UNITTYPEID_SHFT                                                         0x10
#define HWIO_SNOC_NORTH_SBM_SWID_LOW_UNITCONFID_BMSK                                                       0xffff
#define HWIO_SNOC_NORTH_SBM_SWID_LOW_UNITCONFID_SHFT                                                          0x0

#define HWIO_SNOC_WEST_SBM_SWID_LOW_ADDR                                                               (SYSTEM_NOC_REG_BASE      + 0x00001000)
#define HWIO_SNOC_WEST_SBM_SWID_LOW_RMSK                                                                 0xffffff
#define HWIO_SNOC_WEST_SBM_SWID_LOW_IN          \
        in_dword(HWIO_SNOC_WEST_SBM_SWID_LOW_ADDR)
#define HWIO_SNOC_WEST_SBM_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_SNOC_WEST_SBM_SWID_LOW_ADDR, m)
#define HWIO_SNOC_WEST_SBM_SWID_LOW_UNITTYPEID_BMSK                                                      0xff0000
#define HWIO_SNOC_WEST_SBM_SWID_LOW_UNITTYPEID_SHFT                                                          0x10
#define HWIO_SNOC_WEST_SBM_SWID_LOW_UNITCONFID_BMSK                                                        0xffff
#define HWIO_SNOC_WEST_SBM_SWID_LOW_UNITCONFID_SHFT                                                           0x0

#define HWIO_SNOC_WEST_SBM_FLAGOUTSET0_LOW_ADDR                                                        (SYSTEM_NOC_REG_BASE      + 0x00001088)
#define HWIO_SNOC_WEST_SBM_FLAGOUTSET0_LOW_RMSK                                                               0x3
#define HWIO_SNOC_WEST_SBM_FLAGOUTSET0_LOW_OUT(v)      \
        out_dword(HWIO_SNOC_WEST_SBM_FLAGOUTSET0_LOW_ADDR,v)
#define HWIO_SNOC_WEST_SBM_FLAGOUTSET0_LOW_PORT1_BMSK                                                         0x2
#define HWIO_SNOC_WEST_SBM_FLAGOUTSET0_LOW_PORT1_SHFT                                                         0x1
#define HWIO_SNOC_WEST_SBM_FLAGOUTSET0_LOW_PORT0_BMSK                                                         0x1
#define HWIO_SNOC_WEST_SBM_FLAGOUTSET0_LOW_PORT0_SHFT                                                         0x0

#define HWIO_SNOC_CENTER_DISABLE_SBM_SWID_LOW_ADDR                                                     (SYSTEM_NOC_REG_BASE      + 0x00060000)
#define HWIO_SNOC_CENTER_DISABLE_SBM_SWID_LOW_RMSK                                                       0xffffff
#define HWIO_SNOC_CENTER_DISABLE_SBM_SWID_LOW_IN          \
        in_dword(HWIO_SNOC_CENTER_DISABLE_SBM_SWID_LOW_ADDR)
#define HWIO_SNOC_CENTER_DISABLE_SBM_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_SNOC_CENTER_DISABLE_SBM_SWID_LOW_ADDR, m)
#define HWIO_SNOC_CENTER_DISABLE_SBM_SWID_LOW_UNITTYPEID_BMSK                                            0xff0000
#define HWIO_SNOC_CENTER_DISABLE_SBM_SWID_LOW_UNITTYPEID_SHFT                                                0x10
#define HWIO_SNOC_CENTER_DISABLE_SBM_SWID_LOW_UNITCONFID_BMSK                                              0xffff
#define HWIO_SNOC_CENTER_DISABLE_SBM_SWID_LOW_UNITCONFID_SHFT                                                 0x0

#define HWIO_SNOC_CENTER_DISABLE_SBM_FLAGOUTSET0_LOW_ADDR                                              (SYSTEM_NOC_REG_BASE      + 0x00060088)
#define HWIO_SNOC_CENTER_DISABLE_SBM_FLAGOUTSET0_LOW_RMSK                                                     0x3
#define HWIO_SNOC_CENTER_DISABLE_SBM_FLAGOUTSET0_LOW_OUT(v)      \
        out_dword(HWIO_SNOC_CENTER_DISABLE_SBM_FLAGOUTSET0_LOW_ADDR,v)
#define HWIO_SNOC_CENTER_DISABLE_SBM_FLAGOUTSET0_LOW_PORT1_BMSK                                               0x2
#define HWIO_SNOC_CENTER_DISABLE_SBM_FLAGOUTSET0_LOW_PORT1_SHFT                                               0x1
#define HWIO_SNOC_CENTER_DISABLE_SBM_FLAGOUTSET0_LOW_PORT0_BMSK                                               0x1
#define HWIO_SNOC_CENTER_DISABLE_SBM_FLAGOUTSET0_LOW_PORT0_SHFT                                               0x0

/*----------------------------------------------------------------------------
 * MODULE: CONFIG_NOC
 *--------------------------------------------------------------------------*/

#define CONFIG_NOC_REG_BASE                                                    (CONFIG_NOC_BASE      + 0x00000000)
#define CONFIG_NOC_REG_BASE_SIZE                                               0x8200
#define CONFIG_NOC_REG_BASE_USED                                               0x8104

#define HWIO_CNOC_OBS_SWID_LOW_ADDR                                            (CONFIG_NOC_REG_BASE      + 0x00000000)
#define HWIO_CNOC_OBS_SWID_LOW_RMSK                                              0xffffff
#define HWIO_CNOC_OBS_SWID_LOW_IN          \
        in_dword(HWIO_CNOC_OBS_SWID_LOW_ADDR)
#define HWIO_CNOC_OBS_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_OBS_SWID_LOW_ADDR, m)
#define HWIO_CNOC_OBS_SWID_LOW_UNITTYPEID_BMSK                                   0xff0000
#define HWIO_CNOC_OBS_SWID_LOW_UNITTYPEID_SHFT                                       0x10
#define HWIO_CNOC_OBS_SWID_LOW_UNITCONFID_BMSK                                     0xffff
#define HWIO_CNOC_OBS_SWID_LOW_UNITCONFID_SHFT                                        0x0

#define HWIO_CNOC_OBS_SWID_HIGH_ADDR                                           (CONFIG_NOC_REG_BASE      + 0x00000004)
#define HWIO_CNOC_OBS_SWID_HIGH_RMSK                                           0xffffffff
#define HWIO_CNOC_OBS_SWID_HIGH_IN          \
        in_dword(HWIO_CNOC_OBS_SWID_HIGH_ADDR)
#define HWIO_CNOC_OBS_SWID_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_OBS_SWID_HIGH_ADDR, m)
#define HWIO_CNOC_OBS_SWID_HIGH_QNOCID_BMSK                                    0xffffffff
#define HWIO_CNOC_OBS_SWID_HIGH_QNOCID_SHFT                                           0x0

#define HWIO_CNOC_OBS_MAINCTL_LOW_ADDR                                         (CONFIG_NOC_REG_BASE      + 0x00000008)
#define HWIO_CNOC_OBS_MAINCTL_LOW_RMSK                                                0x3
#define HWIO_CNOC_OBS_MAINCTL_LOW_IN          \
        in_dword(HWIO_CNOC_OBS_MAINCTL_LOW_ADDR)
#define HWIO_CNOC_OBS_MAINCTL_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_OBS_MAINCTL_LOW_ADDR, m)
#define HWIO_CNOC_OBS_MAINCTL_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_OBS_MAINCTL_LOW_ADDR,v)
#define HWIO_CNOC_OBS_MAINCTL_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_OBS_MAINCTL_LOW_ADDR,m,v,HWIO_CNOC_OBS_MAINCTL_LOW_IN)
#define HWIO_CNOC_OBS_MAINCTL_LOW_STALLEN_BMSK                                        0x2
#define HWIO_CNOC_OBS_MAINCTL_LOW_STALLEN_SHFT                                        0x1
#define HWIO_CNOC_OBS_MAINCTL_LOW_FAULTEN_BMSK                                        0x1
#define HWIO_CNOC_OBS_MAINCTL_LOW_FAULTEN_SHFT                                        0x0

#define HWIO_CNOC_OBS_ERRVLD_LOW_ADDR                                          (CONFIG_NOC_REG_BASE      + 0x00000010)
#define HWIO_CNOC_OBS_ERRVLD_LOW_RMSK                                                 0x1
#define HWIO_CNOC_OBS_ERRVLD_LOW_IN          \
        in_dword(HWIO_CNOC_OBS_ERRVLD_LOW_ADDR)
#define HWIO_CNOC_OBS_ERRVLD_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_OBS_ERRVLD_LOW_ADDR, m)
#define HWIO_CNOC_OBS_ERRVLD_LOW_ERRVLD_BMSK                                          0x1
#define HWIO_CNOC_OBS_ERRVLD_LOW_ERRVLD_SHFT                                          0x0

#define HWIO_CNOC_OBS_ERRCLR_LOW_ADDR                                          (CONFIG_NOC_REG_BASE      + 0x00000018)
#define HWIO_CNOC_OBS_ERRCLR_LOW_RMSK                                                 0x1
#define HWIO_CNOC_OBS_ERRCLR_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_OBS_ERRCLR_LOW_ADDR,v)
#define HWIO_CNOC_OBS_ERRCLR_LOW_ERRCLR_BMSK                                          0x1
#define HWIO_CNOC_OBS_ERRCLR_LOW_ERRCLR_SHFT                                          0x0

#define HWIO_CNOC_OBS_ERRLOG0_LOW_ADDR                                         (CONFIG_NOC_REG_BASE      + 0x00000020)
#define HWIO_CNOC_OBS_ERRLOG0_LOW_RMSK                                         0xffffffff
#define HWIO_CNOC_OBS_ERRLOG0_LOW_IN          \
        in_dword(HWIO_CNOC_OBS_ERRLOG0_LOW_ADDR)
#define HWIO_CNOC_OBS_ERRLOG0_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_OBS_ERRLOG0_LOW_ADDR, m)
#define HWIO_CNOC_OBS_ERRLOG0_LOW_RSV2_BMSK                                    0xffc00000
#define HWIO_CNOC_OBS_ERRLOG0_LOW_RSV2_SHFT                                          0x16
#define HWIO_CNOC_OBS_ERRLOG0_LOW_ADDRSPACE_BMSK                                 0x3f0000
#define HWIO_CNOC_OBS_ERRLOG0_LOW_ADDRSPACE_SHFT                                     0x10
#define HWIO_CNOC_OBS_ERRLOG0_LOW_RSV1_BMSK                                        0xc000
#define HWIO_CNOC_OBS_ERRLOG0_LOW_RSV1_SHFT                                           0xe
#define HWIO_CNOC_OBS_ERRLOG0_LOW_SIZEF_BMSK                                       0x3800
#define HWIO_CNOC_OBS_ERRLOG0_LOW_SIZEF_SHFT                                          0xb
#define HWIO_CNOC_OBS_ERRLOG0_LOW_ERRCODE_BMSK                                      0x700
#define HWIO_CNOC_OBS_ERRLOG0_LOW_ERRCODE_SHFT                                        0x8
#define HWIO_CNOC_OBS_ERRLOG0_LOW_RSV0_BMSK                                          0x80
#define HWIO_CNOC_OBS_ERRLOG0_LOW_RSV0_SHFT                                           0x7
#define HWIO_CNOC_OBS_ERRLOG0_LOW_OPC_BMSK                                           0x70
#define HWIO_CNOC_OBS_ERRLOG0_LOW_OPC_SHFT                                            0x4
#define HWIO_CNOC_OBS_ERRLOG0_LOW_DEVICE_BMSK                                         0x8
#define HWIO_CNOC_OBS_ERRLOG0_LOW_DEVICE_SHFT                                         0x3
#define HWIO_CNOC_OBS_ERRLOG0_LOW_NONSECURE_BMSK                                      0x4
#define HWIO_CNOC_OBS_ERRLOG0_LOW_NONSECURE_SHFT                                      0x2
#define HWIO_CNOC_OBS_ERRLOG0_LOW_WORDERROR_BMSK                                      0x2
#define HWIO_CNOC_OBS_ERRLOG0_LOW_WORDERROR_SHFT                                      0x1
#define HWIO_CNOC_OBS_ERRLOG0_LOW_LOGINFOVLD_BMSK                                     0x1
#define HWIO_CNOC_OBS_ERRLOG0_LOW_LOGINFOVLD_SHFT                                     0x0

#define HWIO_CNOC_OBS_ERRLOG0_HIGH_ADDR                                        (CONFIG_NOC_REG_BASE      + 0x00000024)
#define HWIO_CNOC_OBS_ERRLOG0_HIGH_RMSK                                             0x3ff
#define HWIO_CNOC_OBS_ERRLOG0_HIGH_IN          \
        in_dword(HWIO_CNOC_OBS_ERRLOG0_HIGH_ADDR)
#define HWIO_CNOC_OBS_ERRLOG0_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_OBS_ERRLOG0_HIGH_ADDR, m)
#define HWIO_CNOC_OBS_ERRLOG0_HIGH_LEN1_BMSK                                        0x3ff
#define HWIO_CNOC_OBS_ERRLOG0_HIGH_LEN1_SHFT                                          0x0

#define HWIO_CNOC_OBS_ERRLOG1_LOW_ADDR                                         (CONFIG_NOC_REG_BASE      + 0x00000028)
#define HWIO_CNOC_OBS_ERRLOG1_LOW_RMSK                                         0xffffffff
#define HWIO_CNOC_OBS_ERRLOG1_LOW_IN          \
        in_dword(HWIO_CNOC_OBS_ERRLOG1_LOW_ADDR)
#define HWIO_CNOC_OBS_ERRLOG1_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_OBS_ERRLOG1_LOW_ADDR, m)
#define HWIO_CNOC_OBS_ERRLOG1_LOW_RSV0_BMSK                                    0xffff0000
#define HWIO_CNOC_OBS_ERRLOG1_LOW_RSV0_SHFT                                          0x10
#define HWIO_CNOC_OBS_ERRLOG1_LOW_PATH_BMSK                                        0xffff
#define HWIO_CNOC_OBS_ERRLOG1_LOW_PATH_SHFT                                           0x0

#define HWIO_CNOC_OBS_ERRLOG1_HIGH_ADDR                                        (CONFIG_NOC_REG_BASE      + 0x0000002c)
#define HWIO_CNOC_OBS_ERRLOG1_HIGH_RMSK                                           0x3ffff
#define HWIO_CNOC_OBS_ERRLOG1_HIGH_IN          \
        in_dword(HWIO_CNOC_OBS_ERRLOG1_HIGH_ADDR)
#define HWIO_CNOC_OBS_ERRLOG1_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_OBS_ERRLOG1_HIGH_ADDR, m)
#define HWIO_CNOC_OBS_ERRLOG1_HIGH_EXTID_BMSK                                     0x3ffff
#define HWIO_CNOC_OBS_ERRLOG1_HIGH_EXTID_SHFT                                         0x0

#define HWIO_CNOC_OBS_ERRLOG2_LOW_ADDR                                         (CONFIG_NOC_REG_BASE      + 0x00000030)
#define HWIO_CNOC_OBS_ERRLOG2_LOW_RMSK                                         0xffffffff
#define HWIO_CNOC_OBS_ERRLOG2_LOW_IN          \
        in_dword(HWIO_CNOC_OBS_ERRLOG2_LOW_ADDR)
#define HWIO_CNOC_OBS_ERRLOG2_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_OBS_ERRLOG2_LOW_ADDR, m)
#define HWIO_CNOC_OBS_ERRLOG2_LOW_ERRLOG2_LSB_BMSK                             0xffffffff
#define HWIO_CNOC_OBS_ERRLOG2_LOW_ERRLOG2_LSB_SHFT                                    0x0

#define HWIO_CNOC_OBS_ERRLOG2_HIGH_ADDR                                        (CONFIG_NOC_REG_BASE      + 0x00000034)
#define HWIO_CNOC_OBS_ERRLOG2_HIGH_RMSK                                           0x1ffff
#define HWIO_CNOC_OBS_ERRLOG2_HIGH_IN          \
        in_dword(HWIO_CNOC_OBS_ERRLOG2_HIGH_ADDR)
#define HWIO_CNOC_OBS_ERRLOG2_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_OBS_ERRLOG2_HIGH_ADDR, m)
#define HWIO_CNOC_OBS_ERRLOG2_HIGH_ERRLOG2_MSB_BMSK                               0x1ffff
#define HWIO_CNOC_OBS_ERRLOG2_HIGH_ERRLOG2_MSB_SHFT                                   0x0

#define HWIO_CNOC_OBS_ERRLOG3_LOW_ADDR                                         (CONFIG_NOC_REG_BASE      + 0x00000038)
#define HWIO_CNOC_OBS_ERRLOG3_LOW_RMSK                                         0xffffffff
#define HWIO_CNOC_OBS_ERRLOG3_LOW_IN          \
        in_dword(HWIO_CNOC_OBS_ERRLOG3_LOW_ADDR)
#define HWIO_CNOC_OBS_ERRLOG3_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_OBS_ERRLOG3_LOW_ADDR, m)
#define HWIO_CNOC_OBS_ERRLOG3_LOW_ERRLOG3_LSB_BMSK                             0xffffffff
#define HWIO_CNOC_OBS_ERRLOG3_LOW_ERRLOG3_LSB_SHFT                                    0x0

#define HWIO_CNOC_OBS_ERRLOG3_HIGH_ADDR                                        (CONFIG_NOC_REG_BASE      + 0x0000003c)
#define HWIO_CNOC_OBS_ERRLOG3_HIGH_RMSK                                        0xffffffff
#define HWIO_CNOC_OBS_ERRLOG3_HIGH_IN          \
        in_dword(HWIO_CNOC_OBS_ERRLOG3_HIGH_ADDR)
#define HWIO_CNOC_OBS_ERRLOG3_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_OBS_ERRLOG3_HIGH_ADDR, m)
#define HWIO_CNOC_OBS_ERRLOG3_HIGH_ERRLOG3_MSB_BMSK                            0xffffffff
#define HWIO_CNOC_OBS_ERRLOG3_HIGH_ERRLOG3_MSB_SHFT                                   0x0

#define HWIO_CNOC_CENTER_SBM_SWID_LOW_ADDR                                     (CONFIG_NOC_REG_BASE      + 0x00000200)
#define HWIO_CNOC_CENTER_SBM_SWID_LOW_RMSK                                       0xffffff
#define HWIO_CNOC_CENTER_SBM_SWID_LOW_IN          \
        in_dword(HWIO_CNOC_CENTER_SBM_SWID_LOW_ADDR)
#define HWIO_CNOC_CENTER_SBM_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_CENTER_SBM_SWID_LOW_ADDR, m)
#define HWIO_CNOC_CENTER_SBM_SWID_LOW_UNITTYPEID_BMSK                            0xff0000
#define HWIO_CNOC_CENTER_SBM_SWID_LOW_UNITTYPEID_SHFT                                0x10
#define HWIO_CNOC_CENTER_SBM_SWID_LOW_UNITCONFID_BMSK                              0xffff
#define HWIO_CNOC_CENTER_SBM_SWID_LOW_UNITCONFID_SHFT                                 0x0

#define HWIO_CNOC_CENTER_SBM_SWID_HIGH_ADDR                                    (CONFIG_NOC_REG_BASE      + 0x00000204)
#define HWIO_CNOC_CENTER_SBM_SWID_HIGH_RMSK                                    0xffffffff
#define HWIO_CNOC_CENTER_SBM_SWID_HIGH_IN          \
        in_dword(HWIO_CNOC_CENTER_SBM_SWID_HIGH_ADDR)
#define HWIO_CNOC_CENTER_SBM_SWID_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_CENTER_SBM_SWID_HIGH_ADDR, m)
#define HWIO_CNOC_CENTER_SBM_SWID_HIGH_QNOCID_BMSK                             0xffffffff
#define HWIO_CNOC_CENTER_SBM_SWID_HIGH_QNOCID_SHFT                                    0x0

#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_ADDR                               (CONFIG_NOC_REG_BASE      + 0x00000240)
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_RMSK                               0xffffffff
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_IN          \
        in_dword(HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_ADDR)
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_ADDR, m)
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_ADDR,v)
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_ADDR,m,v,HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_IN)
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT31_BMSK                        0x80000000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT31_SHFT                              0x1f
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT30_BMSK                        0x40000000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT30_SHFT                              0x1e
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT29_BMSK                        0x20000000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT29_SHFT                              0x1d
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT28_BMSK                        0x10000000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT28_SHFT                              0x1c
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT27_BMSK                         0x8000000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT27_SHFT                              0x1b
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT26_BMSK                         0x4000000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT26_SHFT                              0x1a
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT25_BMSK                         0x2000000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT25_SHFT                              0x19
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT24_BMSK                         0x1000000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT24_SHFT                              0x18
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT23_BMSK                          0x800000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT23_SHFT                              0x17
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT22_BMSK                          0x400000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT22_SHFT                              0x16
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT21_BMSK                          0x200000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT21_SHFT                              0x15
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT20_BMSK                          0x100000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT20_SHFT                              0x14
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT19_BMSK                           0x80000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT19_SHFT                              0x13
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT18_BMSK                           0x40000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT18_SHFT                              0x12
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT17_BMSK                           0x20000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT17_SHFT                              0x11
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT16_BMSK                           0x10000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT16_SHFT                              0x10
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT15_BMSK                            0x8000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT15_SHFT                               0xf
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT14_BMSK                            0x4000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT14_SHFT                               0xe
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT13_BMSK                            0x2000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT13_SHFT                               0xd
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT12_BMSK                            0x1000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT12_SHFT                               0xc
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT11_BMSK                             0x800
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT11_SHFT                               0xb
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT10_BMSK                             0x400
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT10_SHFT                               0xa
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT9_BMSK                              0x200
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT9_SHFT                                0x9
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT8_BMSK                              0x100
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT8_SHFT                                0x8
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT7_BMSK                               0x80
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT7_SHFT                                0x7
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT6_BMSK                               0x40
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT6_SHFT                                0x6
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT5_BMSK                               0x20
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT5_SHFT                                0x5
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT4_BMSK                               0x10
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT4_SHFT                                0x4
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT3_BMSK                                0x8
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT3_SHFT                                0x3
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT2_BMSK                                0x4
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT2_SHFT                                0x2
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT1_BMSK                                0x2
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT1_SHFT                                0x1
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT0_BMSK                                0x1
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_LOW_PORT0_SHFT                                0x0

#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_ADDR                              (CONFIG_NOC_REG_BASE      + 0x00000244)
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_RMSK                              0xffffffff
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_IN          \
        in_dword(HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_ADDR)
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_ADDR, m)
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_OUT(v)      \
        out_dword(HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_ADDR,v)
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_ADDR,m,v,HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_IN)
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT63_BMSK                       0x80000000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT63_SHFT                             0x1f
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT62_BMSK                       0x40000000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT62_SHFT                             0x1e
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT61_BMSK                       0x20000000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT61_SHFT                             0x1d
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT60_BMSK                       0x10000000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT60_SHFT                             0x1c
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT59_BMSK                        0x8000000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT59_SHFT                             0x1b
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT58_BMSK                        0x4000000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT58_SHFT                             0x1a
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT57_BMSK                        0x2000000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT57_SHFT                             0x19
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT56_BMSK                        0x1000000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT56_SHFT                             0x18
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT55_BMSK                         0x800000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT55_SHFT                             0x17
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT54_BMSK                         0x400000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT54_SHFT                             0x16
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT53_BMSK                         0x200000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT53_SHFT                             0x15
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT52_BMSK                         0x100000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT52_SHFT                             0x14
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT51_BMSK                          0x80000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT51_SHFT                             0x13
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT50_BMSK                          0x40000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT50_SHFT                             0x12
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT49_BMSK                          0x20000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT49_SHFT                             0x11
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT48_BMSK                          0x10000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT48_SHFT                             0x10
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT47_BMSK                           0x8000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT47_SHFT                              0xf
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT46_BMSK                           0x4000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT46_SHFT                              0xe
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT45_BMSK                           0x2000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT45_SHFT                              0xd
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT44_BMSK                           0x1000
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT44_SHFT                              0xc
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT43_BMSK                            0x800
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT43_SHFT                              0xb
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT42_BMSK                            0x400
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT42_SHFT                              0xa
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT41_BMSK                            0x200
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT41_SHFT                              0x9
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT40_BMSK                            0x100
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT40_SHFT                              0x8
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT39_BMSK                             0x80
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT39_SHFT                              0x7
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT38_BMSK                             0x40
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT38_SHFT                              0x6
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT37_BMSK                             0x20
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT37_SHFT                              0x5
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT36_BMSK                             0x10
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT36_SHFT                              0x4
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT35_BMSK                              0x8
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT35_SHFT                              0x3
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT34_BMSK                              0x4
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT34_SHFT                              0x2
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT33_BMSK                              0x2
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT33_SHFT                              0x1
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT32_BMSK                              0x1
#define HWIO_CNOC_CENTER_SBM_FAULTINEN0_HIGH_PORT32_SHFT                              0x0

#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_ADDR                           (CONFIG_NOC_REG_BASE      + 0x00000248)
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_RMSK                           0xffffffff
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_IN          \
        in_dword(HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_ADDR)
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_ADDR, m)
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT31_BMSK                    0x80000000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT31_SHFT                          0x1f
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT30_BMSK                    0x40000000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT30_SHFT                          0x1e
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT29_BMSK                    0x20000000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT29_SHFT                          0x1d
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT28_BMSK                    0x10000000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT28_SHFT                          0x1c
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT27_BMSK                     0x8000000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT27_SHFT                          0x1b
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT26_BMSK                     0x4000000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT26_SHFT                          0x1a
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT25_BMSK                     0x2000000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT25_SHFT                          0x19
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT24_BMSK                     0x1000000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT24_SHFT                          0x18
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT23_BMSK                      0x800000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT23_SHFT                          0x17
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT22_BMSK                      0x400000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT22_SHFT                          0x16
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT21_BMSK                      0x200000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT21_SHFT                          0x15
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT20_BMSK                      0x100000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT20_SHFT                          0x14
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT19_BMSK                       0x80000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT19_SHFT                          0x13
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT18_BMSK                       0x40000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT18_SHFT                          0x12
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT17_BMSK                       0x20000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT17_SHFT                          0x11
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT16_BMSK                       0x10000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT16_SHFT                          0x10
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT15_BMSK                        0x8000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT15_SHFT                           0xf
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT14_BMSK                        0x4000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT14_SHFT                           0xe
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT13_BMSK                        0x2000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT13_SHFT                           0xd
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT12_BMSK                        0x1000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT12_SHFT                           0xc
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT11_BMSK                         0x800
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT11_SHFT                           0xb
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT10_BMSK                         0x400
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT10_SHFT                           0xa
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT9_BMSK                          0x200
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT9_SHFT                            0x9
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT8_BMSK                          0x100
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT8_SHFT                            0x8
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT7_BMSK                           0x80
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT7_SHFT                            0x7
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT6_BMSK                           0x40
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT6_SHFT                            0x6
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT5_BMSK                           0x20
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT5_SHFT                            0x5
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT4_BMSK                           0x10
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT4_SHFT                            0x4
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT3_BMSK                            0x8
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT3_SHFT                            0x3
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT2_BMSK                            0x4
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT2_SHFT                            0x2
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT1_BMSK                            0x2
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT1_SHFT                            0x1
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT0_BMSK                            0x1
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_LOW_PORT0_SHFT                            0x0

#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_ADDR                          (CONFIG_NOC_REG_BASE      + 0x0000024c)
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_RMSK                          0xffffffff
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_IN          \
        in_dword(HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_ADDR)
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_ADDR, m)
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT63_BMSK                   0x80000000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT63_SHFT                         0x1f
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT62_BMSK                   0x40000000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT62_SHFT                         0x1e
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT61_BMSK                   0x20000000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT61_SHFT                         0x1d
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT60_BMSK                   0x10000000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT60_SHFT                         0x1c
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT59_BMSK                    0x8000000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT59_SHFT                         0x1b
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT58_BMSK                    0x4000000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT58_SHFT                         0x1a
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT57_BMSK                    0x2000000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT57_SHFT                         0x19
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT56_BMSK                    0x1000000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT56_SHFT                         0x18
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT55_BMSK                     0x800000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT55_SHFT                         0x17
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT54_BMSK                     0x400000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT54_SHFT                         0x16
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT53_BMSK                     0x200000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT53_SHFT                         0x15
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT52_BMSK                     0x100000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT52_SHFT                         0x14
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT51_BMSK                      0x80000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT51_SHFT                         0x13
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT50_BMSK                      0x40000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT50_SHFT                         0x12
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT49_BMSK                      0x20000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT49_SHFT                         0x11
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT48_BMSK                      0x10000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT48_SHFT                         0x10
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT47_BMSK                       0x8000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT47_SHFT                          0xf
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT46_BMSK                       0x4000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT46_SHFT                          0xe
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT45_BMSK                       0x2000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT45_SHFT                          0xd
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT44_BMSK                       0x1000
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT44_SHFT                          0xc
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT43_BMSK                        0x800
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT43_SHFT                          0xb
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT42_BMSK                        0x400
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT42_SHFT                          0xa
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT41_BMSK                        0x200
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT41_SHFT                          0x9
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT40_BMSK                        0x100
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT40_SHFT                          0x8
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT39_BMSK                         0x80
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT39_SHFT                          0x7
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT38_BMSK                         0x40
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT38_SHFT                          0x6
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT37_BMSK                         0x20
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT37_SHFT                          0x5
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT36_BMSK                         0x10
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT36_SHFT                          0x4
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT35_BMSK                          0x8
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT35_SHFT                          0x3
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT34_BMSK                          0x4
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT34_SHFT                          0x2
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT33_BMSK                          0x2
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT33_SHFT                          0x1
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT32_BMSK                          0x1
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS0_HIGH_PORT32_SHFT                          0x0

#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_ADDR                               (CONFIG_NOC_REG_BASE      + 0x00000250)
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_RMSK                                     0xff
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_IN          \
        in_dword(HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_ADDR)
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_ADDR, m)
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_ADDR,v)
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_ADDR,m,v,HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_IN)
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_PORT71_BMSK                              0x80
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_PORT71_SHFT                               0x7
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_PORT70_BMSK                              0x40
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_PORT70_SHFT                               0x6
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_PORT69_BMSK                              0x20
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_PORT69_SHFT                               0x5
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_PORT68_BMSK                              0x10
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_PORT68_SHFT                               0x4
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_PORT67_BMSK                               0x8
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_PORT67_SHFT                               0x3
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_PORT66_BMSK                               0x4
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_PORT66_SHFT                               0x2
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_PORT65_BMSK                               0x2
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_PORT65_SHFT                               0x1
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_PORT64_BMSK                               0x1
#define HWIO_CNOC_CENTER_SBM_FAULTINEN1_LOW_PORT64_SHFT                               0x0

#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_ADDR                           (CONFIG_NOC_REG_BASE      + 0x00000258)
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_RMSK                                 0xff
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_IN          \
        in_dword(HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_ADDR)
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_ADDR, m)
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_PORT71_BMSK                          0x80
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_PORT71_SHFT                           0x7
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_PORT70_BMSK                          0x40
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_PORT70_SHFT                           0x6
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_PORT69_BMSK                          0x20
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_PORT69_SHFT                           0x5
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_PORT68_BMSK                          0x10
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_PORT68_SHFT                           0x4
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_PORT67_BMSK                           0x8
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_PORT67_SHFT                           0x3
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_PORT66_BMSK                           0x4
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_PORT66_SHFT                           0x2
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_PORT65_BMSK                           0x2
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_PORT65_SHFT                           0x1
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_PORT64_BMSK                           0x1
#define HWIO_CNOC_CENTER_SBM_FAULTINSTATUS1_LOW_PORT64_SHFT                           0x0

#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_ADDR                              (CONFIG_NOC_REG_BASE      + 0x00000280)
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_RMSK                                 0x3ffff
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_ADDR,v)
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT17_BMSK                          0x20000
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT17_SHFT                             0x11
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT16_BMSK                          0x10000
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT16_SHFT                             0x10
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT15_BMSK                           0x8000
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT15_SHFT                              0xf
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT14_BMSK                           0x4000
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT14_SHFT                              0xe
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT13_BMSK                           0x2000
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT13_SHFT                              0xd
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT12_BMSK                           0x1000
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT12_SHFT                              0xc
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT11_BMSK                            0x800
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT11_SHFT                              0xb
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT10_BMSK                            0x400
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT10_SHFT                              0xa
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT9_BMSK                             0x200
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT9_SHFT                               0x9
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT8_BMSK                             0x100
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT8_SHFT                               0x8
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT7_BMSK                              0x80
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT7_SHFT                               0x7
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT6_BMSK                              0x40
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT6_SHFT                               0x6
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT5_BMSK                              0x20
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT5_SHFT                               0x5
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT4_BMSK                              0x10
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT4_SHFT                               0x4
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT3_BMSK                               0x8
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT3_SHFT                               0x3
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT2_BMSK                               0x4
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT2_SHFT                               0x2
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT1_BMSK                               0x2
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT1_SHFT                               0x1
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT0_BMSK                               0x1
#define HWIO_CNOC_CENTER_SBM_FLAGOUTCLR0_LOW_PORT0_SHFT                               0x0

#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_ADDR                              (CONFIG_NOC_REG_BASE      + 0x00000288)
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_RMSK                                 0x3ffff
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_ADDR,v)
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT17_BMSK                          0x20000
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT17_SHFT                             0x11
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT16_BMSK                          0x10000
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT16_SHFT                             0x10
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT15_BMSK                           0x8000
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT15_SHFT                              0xf
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT14_BMSK                           0x4000
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT14_SHFT                              0xe
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT13_BMSK                           0x2000
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT13_SHFT                              0xd
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT12_BMSK                           0x1000
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT12_SHFT                              0xc
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT11_BMSK                            0x800
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT11_SHFT                              0xb
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT10_BMSK                            0x400
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT10_SHFT                              0xa
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT9_BMSK                             0x200
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT9_SHFT                               0x9
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT8_BMSK                             0x100
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT8_SHFT                               0x8
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT7_BMSK                              0x80
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT7_SHFT                               0x7
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT6_BMSK                              0x40
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT6_SHFT                               0x6
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT5_BMSK                              0x20
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT5_SHFT                               0x5
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT4_BMSK                              0x10
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT4_SHFT                               0x4
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT3_BMSK                               0x8
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT3_SHFT                               0x3
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT2_BMSK                               0x4
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT2_SHFT                               0x2
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT1_BMSK                               0x2
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT1_SHFT                               0x1
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT0_BMSK                               0x1
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSET0_LOW_PORT0_SHFT                               0x0

#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_ADDR                           (CONFIG_NOC_REG_BASE      + 0x00000290)
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_RMSK                              0x3ffff
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_IN          \
        in_dword(HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_ADDR)
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_ADDR, m)
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT17_BMSK                       0x20000
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT17_SHFT                          0x11
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT16_BMSK                       0x10000
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT16_SHFT                          0x10
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT15_BMSK                        0x8000
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT15_SHFT                           0xf
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT14_BMSK                        0x4000
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT14_SHFT                           0xe
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT13_BMSK                        0x2000
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT13_SHFT                           0xd
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT12_BMSK                        0x1000
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT12_SHFT                           0xc
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT11_BMSK                         0x800
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT11_SHFT                           0xb
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT10_BMSK                         0x400
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT10_SHFT                           0xa
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT9_BMSK                          0x200
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT9_SHFT                            0x9
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT8_BMSK                          0x100
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT8_SHFT                            0x8
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT7_BMSK                           0x80
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT7_SHFT                            0x7
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT6_BMSK                           0x40
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT6_SHFT                            0x6
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT5_BMSK                           0x20
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT5_SHFT                            0x5
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT4_BMSK                           0x10
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT4_SHFT                            0x4
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT3_BMSK                            0x8
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT3_SHFT                            0x3
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT2_BMSK                            0x4
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT2_SHFT                            0x2
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT1_BMSK                            0x2
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT1_SHFT                            0x1
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT0_BMSK                            0x1
#define HWIO_CNOC_CENTER_SBM_FLAGOUTSTATUS0_LOW_PORT0_SHFT                            0x0

#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_ADDR                                 (CONFIG_NOC_REG_BASE      + 0x00000300)
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_RMSK                                 0xffffffff
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_IN          \
        in_dword(HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_ADDR)
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_ADDR, m)
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT31_BMSK                          0x80000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT31_SHFT                                0x1f
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT30_BMSK                          0x40000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT30_SHFT                                0x1e
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT29_BMSK                          0x20000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT29_SHFT                                0x1d
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT28_BMSK                          0x10000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT28_SHFT                                0x1c
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT27_BMSK                           0x8000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT27_SHFT                                0x1b
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT26_BMSK                           0x4000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT26_SHFT                                0x1a
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT25_BMSK                           0x2000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT25_SHFT                                0x19
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT24_BMSK                           0x1000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT24_SHFT                                0x18
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT23_BMSK                            0x800000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT23_SHFT                                0x17
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT22_BMSK                            0x400000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT22_SHFT                                0x16
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT21_BMSK                            0x200000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT21_SHFT                                0x15
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT20_BMSK                            0x100000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT20_SHFT                                0x14
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT19_BMSK                             0x80000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT19_SHFT                                0x13
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT18_BMSK                             0x40000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT18_SHFT                                0x12
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT17_BMSK                             0x20000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT17_SHFT                                0x11
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT16_BMSK                             0x10000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT16_SHFT                                0x10
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT15_BMSK                              0x8000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT15_SHFT                                 0xf
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT14_BMSK                              0x4000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT14_SHFT                                 0xe
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT13_BMSK                              0x2000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT13_SHFT                                 0xd
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT12_BMSK                              0x1000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT12_SHFT                                 0xc
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT11_BMSK                               0x800
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT11_SHFT                                 0xb
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT10_BMSK                               0x400
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT10_SHFT                                 0xa
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT9_BMSK                                0x200
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT9_SHFT                                  0x9
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT8_BMSK                                0x100
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT8_SHFT                                  0x8
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT7_BMSK                                 0x80
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT7_SHFT                                  0x7
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT6_BMSK                                 0x40
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT6_SHFT                                  0x6
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT5_BMSK                                 0x20
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT5_SHFT                                  0x5
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT4_BMSK                                 0x10
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT4_SHFT                                  0x4
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT3_BMSK                                  0x8
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT3_SHFT                                  0x3
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT2_BMSK                                  0x4
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT2_SHFT                                  0x2
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT1_BMSK                                  0x2
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT1_SHFT                                  0x1
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT0_BMSK                                  0x1
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_LOW_PORT0_SHFT                                  0x0

#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_ADDR                                (CONFIG_NOC_REG_BASE      + 0x00000304)
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_RMSK                                0xffffffff
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_IN          \
        in_dword(HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_ADDR)
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_ADDR, m)
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT63_BMSK                         0x80000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT63_SHFT                               0x1f
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT62_BMSK                         0x40000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT62_SHFT                               0x1e
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT61_BMSK                         0x20000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT61_SHFT                               0x1d
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT60_BMSK                         0x10000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT60_SHFT                               0x1c
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT59_BMSK                          0x8000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT59_SHFT                               0x1b
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT58_BMSK                          0x4000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT58_SHFT                               0x1a
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT57_BMSK                          0x2000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT57_SHFT                               0x19
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT56_BMSK                          0x1000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT56_SHFT                               0x18
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT55_BMSK                           0x800000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT55_SHFT                               0x17
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT54_BMSK                           0x400000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT54_SHFT                               0x16
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT53_BMSK                           0x200000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT53_SHFT                               0x15
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT52_BMSK                           0x100000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT52_SHFT                               0x14
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT51_BMSK                            0x80000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT51_SHFT                               0x13
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT50_BMSK                            0x40000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT50_SHFT                               0x12
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT49_BMSK                            0x20000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT49_SHFT                               0x11
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT48_BMSK                            0x10000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT48_SHFT                               0x10
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT47_BMSK                             0x8000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT47_SHFT                                0xf
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT46_BMSK                             0x4000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT46_SHFT                                0xe
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT45_BMSK                             0x2000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT45_SHFT                                0xd
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT44_BMSK                             0x1000
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT44_SHFT                                0xc
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT43_BMSK                              0x800
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT43_SHFT                                0xb
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT42_BMSK                              0x400
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT42_SHFT                                0xa
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT41_BMSK                              0x200
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT41_SHFT                                0x9
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT40_BMSK                              0x100
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT40_SHFT                                0x8
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT39_BMSK                               0x80
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT39_SHFT                                0x7
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT38_BMSK                               0x40
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT38_SHFT                                0x6
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT37_BMSK                               0x20
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT37_SHFT                                0x5
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT36_BMSK                               0x10
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT36_SHFT                                0x4
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT35_BMSK                                0x8
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT35_SHFT                                0x3
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT34_BMSK                                0x4
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT34_SHFT                                0x2
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT33_BMSK                                0x2
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT33_SHFT                                0x1
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT32_BMSK                                0x1
#define HWIO_CNOC_CENTER_SBM_SENSEIN0_HIGH_PORT32_SHFT                                0x0

#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_ADDR                                 (CONFIG_NOC_REG_BASE      + 0x00000308)
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_RMSK                                 0xffffffff
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_IN          \
        in_dword(HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_ADDR)
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_ADDR, m)
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT95_BMSK                          0x80000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT95_SHFT                                0x1f
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT94_BMSK                          0x40000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT94_SHFT                                0x1e
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT93_BMSK                          0x20000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT93_SHFT                                0x1d
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT92_BMSK                          0x10000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT92_SHFT                                0x1c
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT91_BMSK                           0x8000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT91_SHFT                                0x1b
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT90_BMSK                           0x4000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT90_SHFT                                0x1a
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT89_BMSK                           0x2000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT89_SHFT                                0x19
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT88_BMSK                           0x1000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT88_SHFT                                0x18
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT87_BMSK                            0x800000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT87_SHFT                                0x17
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT86_BMSK                            0x400000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT86_SHFT                                0x16
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT85_BMSK                            0x200000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT85_SHFT                                0x15
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT84_BMSK                            0x100000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT84_SHFT                                0x14
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT83_BMSK                             0x80000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT83_SHFT                                0x13
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT82_BMSK                             0x40000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT82_SHFT                                0x12
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT81_BMSK                             0x20000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT81_SHFT                                0x11
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT80_BMSK                             0x10000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT80_SHFT                                0x10
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT79_BMSK                              0x8000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT79_SHFT                                 0xf
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT78_BMSK                              0x4000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT78_SHFT                                 0xe
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT77_BMSK                              0x2000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT77_SHFT                                 0xd
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT76_BMSK                              0x1000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT76_SHFT                                 0xc
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT75_BMSK                               0x800
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT75_SHFT                                 0xb
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT74_BMSK                               0x400
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT74_SHFT                                 0xa
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT73_BMSK                               0x200
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT73_SHFT                                 0x9
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT72_BMSK                               0x100
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT72_SHFT                                 0x8
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT71_BMSK                                0x80
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT71_SHFT                                 0x7
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT70_BMSK                                0x40
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT70_SHFT                                 0x6
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT69_BMSK                                0x20
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT69_SHFT                                 0x5
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT68_BMSK                                0x10
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT68_SHFT                                 0x4
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT67_BMSK                                 0x8
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT67_SHFT                                 0x3
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT66_BMSK                                 0x4
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT66_SHFT                                 0x2
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT65_BMSK                                 0x2
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT65_SHFT                                 0x1
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT64_BMSK                                 0x1
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_LOW_PORT64_SHFT                                 0x0

#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_ADDR                                (CONFIG_NOC_REG_BASE      + 0x0000030c)
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_RMSK                                0xffffffff
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_IN          \
        in_dword(HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_ADDR)
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_ADDR, m)
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT127_BMSK                        0x80000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT127_SHFT                              0x1f
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT126_BMSK                        0x40000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT126_SHFT                              0x1e
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT125_BMSK                        0x20000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT125_SHFT                              0x1d
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT124_BMSK                        0x10000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT124_SHFT                              0x1c
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT123_BMSK                         0x8000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT123_SHFT                              0x1b
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT122_BMSK                         0x4000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT122_SHFT                              0x1a
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT121_BMSK                         0x2000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT121_SHFT                              0x19
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT120_BMSK                         0x1000000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT120_SHFT                              0x18
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT119_BMSK                          0x800000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT119_SHFT                              0x17
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT118_BMSK                          0x400000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT118_SHFT                              0x16
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT117_BMSK                          0x200000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT117_SHFT                              0x15
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT116_BMSK                          0x100000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT116_SHFT                              0x14
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT115_BMSK                           0x80000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT115_SHFT                              0x13
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT114_BMSK                           0x40000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT114_SHFT                              0x12
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT113_BMSK                           0x20000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT113_SHFT                              0x11
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT112_BMSK                           0x10000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT112_SHFT                              0x10
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT111_BMSK                            0x8000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT111_SHFT                               0xf
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT110_BMSK                            0x4000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT110_SHFT                               0xe
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT109_BMSK                            0x2000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT109_SHFT                               0xd
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT108_BMSK                            0x1000
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT108_SHFT                               0xc
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT107_BMSK                             0x800
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT107_SHFT                               0xb
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT106_BMSK                             0x400
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT106_SHFT                               0xa
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT105_BMSK                             0x200
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT105_SHFT                               0x9
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT104_BMSK                             0x100
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT104_SHFT                               0x8
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT103_BMSK                              0x80
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT103_SHFT                               0x7
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT102_BMSK                              0x40
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT102_SHFT                               0x6
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT101_BMSK                              0x20
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT101_SHFT                               0x5
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT100_BMSK                              0x10
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT100_SHFT                               0x4
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT99_BMSK                                0x8
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT99_SHFT                                0x3
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT98_BMSK                                0x4
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT98_SHFT                                0x2
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT97_BMSK                                0x2
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT97_SHFT                                0x1
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT96_BMSK                                0x1
#define HWIO_CNOC_CENTER_SBM_SENSEIN1_HIGH_PORT96_SHFT                                0x0

#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_ADDR                                 (CONFIG_NOC_REG_BASE      + 0x00000310)
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_RMSK                                      0x1ff
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_IN          \
        in_dword(HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_ADDR)
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_ADDR, m)
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_PORT136_BMSK                              0x100
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_PORT136_SHFT                                0x8
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_PORT135_BMSK                               0x80
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_PORT135_SHFT                                0x7
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_PORT134_BMSK                               0x40
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_PORT134_SHFT                                0x6
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_PORT133_BMSK                               0x20
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_PORT133_SHFT                                0x5
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_PORT132_BMSK                               0x10
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_PORT132_SHFT                                0x4
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_PORT131_BMSK                                0x8
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_PORT131_SHFT                                0x3
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_PORT130_BMSK                                0x4
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_PORT130_SHFT                                0x2
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_PORT129_BMSK                                0x2
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_PORT129_SHFT                                0x1
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_PORT128_BMSK                                0x1
#define HWIO_CNOC_CENTER_SBM_SENSEIN2_LOW_PORT128_SHFT                                0x0

#define HWIO_CNOC_MONAQ_SBM_SWID_LOW_ADDR                                      (CONFIG_NOC_REG_BASE      + 0x00000400)
#define HWIO_CNOC_MONAQ_SBM_SWID_LOW_RMSK                                        0xffffff
#define HWIO_CNOC_MONAQ_SBM_SWID_LOW_IN          \
        in_dword(HWIO_CNOC_MONAQ_SBM_SWID_LOW_ADDR)
#define HWIO_CNOC_MONAQ_SBM_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_MONAQ_SBM_SWID_LOW_ADDR, m)
#define HWIO_CNOC_MONAQ_SBM_SWID_LOW_UNITTYPEID_BMSK                             0xff0000
#define HWIO_CNOC_MONAQ_SBM_SWID_LOW_UNITTYPEID_SHFT                                 0x10
#define HWIO_CNOC_MONAQ_SBM_SWID_LOW_UNITCONFID_BMSK                               0xffff
#define HWIO_CNOC_MONAQ_SBM_SWID_LOW_UNITCONFID_SHFT                                  0x0

#define HWIO_CNOC_MONAQ_SBM_SWID_HIGH_ADDR                                     (CONFIG_NOC_REG_BASE      + 0x00000404)
#define HWIO_CNOC_MONAQ_SBM_SWID_HIGH_RMSK                                     0xffffffff
#define HWIO_CNOC_MONAQ_SBM_SWID_HIGH_IN          \
        in_dword(HWIO_CNOC_MONAQ_SBM_SWID_HIGH_ADDR)
#define HWIO_CNOC_MONAQ_SBM_SWID_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_MONAQ_SBM_SWID_HIGH_ADDR, m)
#define HWIO_CNOC_MONAQ_SBM_SWID_HIGH_QNOCID_BMSK                              0xffffffff
#define HWIO_CNOC_MONAQ_SBM_SWID_HIGH_QNOCID_SHFT                                     0x0

#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_ADDR                                  (CONFIG_NOC_REG_BASE      + 0x00000500)
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_RMSK                                       0x1ff
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_IN          \
        in_dword(HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_ADDR)
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_ADDR, m)
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_PORT8_BMSK                                 0x100
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_PORT8_SHFT                                   0x8
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_PORT7_BMSK                                  0x80
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_PORT7_SHFT                                   0x7
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_PORT6_BMSK                                  0x40
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_PORT6_SHFT                                   0x6
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_PORT5_BMSK                                  0x20
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_PORT5_SHFT                                   0x5
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_PORT4_BMSK                                  0x10
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_PORT4_SHFT                                   0x4
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_PORT3_BMSK                                   0x8
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_PORT3_SHFT                                   0x3
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_PORT2_BMSK                                   0x4
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_PORT2_SHFT                                   0x2
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_PORT1_BMSK                                   0x2
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_PORT1_SHFT                                   0x1
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_PORT0_BMSK                                   0x1
#define HWIO_CNOC_MONAQ_SBM_SENSEIN0_LOW_PORT0_SHFT                                   0x0

#define HWIO_CNOC_WEST_SBM_SWID_LOW_ADDR                                       (CONFIG_NOC_REG_BASE      + 0x00000800)
#define HWIO_CNOC_WEST_SBM_SWID_LOW_RMSK                                         0xffffff
#define HWIO_CNOC_WEST_SBM_SWID_LOW_IN          \
        in_dword(HWIO_CNOC_WEST_SBM_SWID_LOW_ADDR)
#define HWIO_CNOC_WEST_SBM_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_WEST_SBM_SWID_LOW_ADDR, m)
#define HWIO_CNOC_WEST_SBM_SWID_LOW_UNITTYPEID_BMSK                              0xff0000
#define HWIO_CNOC_WEST_SBM_SWID_LOW_UNITTYPEID_SHFT                                  0x10
#define HWIO_CNOC_WEST_SBM_SWID_LOW_UNITCONFID_BMSK                                0xffff
#define HWIO_CNOC_WEST_SBM_SWID_LOW_UNITCONFID_SHFT                                   0x0

#define HWIO_CNOC_WEST_SBM_SWID_HIGH_ADDR                                      (CONFIG_NOC_REG_BASE      + 0x00000804)
#define HWIO_CNOC_WEST_SBM_SWID_HIGH_RMSK                                      0xffffffff
#define HWIO_CNOC_WEST_SBM_SWID_HIGH_IN          \
        in_dword(HWIO_CNOC_WEST_SBM_SWID_HIGH_ADDR)
#define HWIO_CNOC_WEST_SBM_SWID_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_WEST_SBM_SWID_HIGH_ADDR, m)
#define HWIO_CNOC_WEST_SBM_SWID_HIGH_QNOCID_BMSK                               0xffffffff
#define HWIO_CNOC_WEST_SBM_SWID_HIGH_QNOCID_SHFT                                      0x0

#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_ADDR                                   (CONFIG_NOC_REG_BASE      + 0x00000900)
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_RMSK                                    0xfffffff
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_IN          \
        in_dword(HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_ADDR)
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_ADDR, m)
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT27_BMSK                             0x8000000
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT27_SHFT                                  0x1b
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT26_BMSK                             0x4000000
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT26_SHFT                                  0x1a
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT25_BMSK                             0x2000000
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT25_SHFT                                  0x19
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT24_BMSK                             0x1000000
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT24_SHFT                                  0x18
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT23_BMSK                              0x800000
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT23_SHFT                                  0x17
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT22_BMSK                              0x400000
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT22_SHFT                                  0x16
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT21_BMSK                              0x200000
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT21_SHFT                                  0x15
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT20_BMSK                              0x100000
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT20_SHFT                                  0x14
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT19_BMSK                               0x80000
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT19_SHFT                                  0x13
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT18_BMSK                               0x40000
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT18_SHFT                                  0x12
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT17_BMSK                               0x20000
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT17_SHFT                                  0x11
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT16_BMSK                               0x10000
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT16_SHFT                                  0x10
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT15_BMSK                                0x8000
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT15_SHFT                                   0xf
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT14_BMSK                                0x4000
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT14_SHFT                                   0xe
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT13_BMSK                                0x2000
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT13_SHFT                                   0xd
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT12_BMSK                                0x1000
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT12_SHFT                                   0xc
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT11_BMSK                                 0x800
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT11_SHFT                                   0xb
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT10_BMSK                                 0x400
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT10_SHFT                                   0xa
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT9_BMSK                                  0x200
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT9_SHFT                                    0x9
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT8_BMSK                                  0x100
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT8_SHFT                                    0x8
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT7_BMSK                                   0x80
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT7_SHFT                                    0x7
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT6_BMSK                                   0x40
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT6_SHFT                                    0x6
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT5_BMSK                                   0x20
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT5_SHFT                                    0x5
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT4_BMSK                                   0x10
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT4_SHFT                                    0x4
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT3_BMSK                                    0x8
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT3_SHFT                                    0x3
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT2_BMSK                                    0x4
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT2_SHFT                                    0x2
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT1_BMSK                                    0x2
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT1_SHFT                                    0x1
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT0_BMSK                                    0x1
#define HWIO_CNOC_WEST_SBM_SENSEIN0_LOW_PORT0_SHFT                                    0x0

#define HWIO_CNOC_EAST_SBM_SWID_LOW_ADDR                                       (CONFIG_NOC_REG_BASE      + 0x00000a00)
#define HWIO_CNOC_EAST_SBM_SWID_LOW_RMSK                                         0xffffff
#define HWIO_CNOC_EAST_SBM_SWID_LOW_IN          \
        in_dword(HWIO_CNOC_EAST_SBM_SWID_LOW_ADDR)
#define HWIO_CNOC_EAST_SBM_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_EAST_SBM_SWID_LOW_ADDR, m)
#define HWIO_CNOC_EAST_SBM_SWID_LOW_UNITTYPEID_BMSK                              0xff0000
#define HWIO_CNOC_EAST_SBM_SWID_LOW_UNITTYPEID_SHFT                                  0x10
#define HWIO_CNOC_EAST_SBM_SWID_LOW_UNITCONFID_BMSK                                0xffff
#define HWIO_CNOC_EAST_SBM_SWID_LOW_UNITCONFID_SHFT                                   0x0

#define HWIO_CNOC_EAST_SBM_SWID_HIGH_ADDR                                      (CONFIG_NOC_REG_BASE      + 0x00000a04)
#define HWIO_CNOC_EAST_SBM_SWID_HIGH_RMSK                                      0xffffffff
#define HWIO_CNOC_EAST_SBM_SWID_HIGH_IN          \
        in_dword(HWIO_CNOC_EAST_SBM_SWID_HIGH_ADDR)
#define HWIO_CNOC_EAST_SBM_SWID_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_EAST_SBM_SWID_HIGH_ADDR, m)
#define HWIO_CNOC_EAST_SBM_SWID_HIGH_QNOCID_BMSK                               0xffffffff
#define HWIO_CNOC_EAST_SBM_SWID_HIGH_QNOCID_SHFT                                      0x0

#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_ADDR                                   (CONFIG_NOC_REG_BASE      + 0x00000b00)
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_RMSK                                        0x1ff
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_IN          \
        in_dword(HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_ADDR)
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_ADDR, m)
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_PORT8_BMSK                                  0x100
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_PORT8_SHFT                                    0x8
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_PORT7_BMSK                                   0x80
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_PORT7_SHFT                                    0x7
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_PORT6_BMSK                                   0x40
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_PORT6_SHFT                                    0x6
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_PORT5_BMSK                                   0x20
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_PORT5_SHFT                                    0x5
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_PORT4_BMSK                                   0x10
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_PORT4_SHFT                                    0x4
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_PORT3_BMSK                                    0x8
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_PORT3_SHFT                                    0x3
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_PORT2_BMSK                                    0x4
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_PORT2_SHFT                                    0x2
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_PORT1_BMSK                                    0x2
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_PORT1_SHFT                                    0x1
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_PORT0_BMSK                                    0x1
#define HWIO_CNOC_EAST_SBM_SENSEIN0_LOW_PORT0_SHFT                                    0x0

#define HWIO_CNOC_NORTH_SBM_SWID_LOW_ADDR                                      (CONFIG_NOC_REG_BASE      + 0x00000c00)
#define HWIO_CNOC_NORTH_SBM_SWID_LOW_RMSK                                        0xffffff
#define HWIO_CNOC_NORTH_SBM_SWID_LOW_IN          \
        in_dword(HWIO_CNOC_NORTH_SBM_SWID_LOW_ADDR)
#define HWIO_CNOC_NORTH_SBM_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_NORTH_SBM_SWID_LOW_ADDR, m)
#define HWIO_CNOC_NORTH_SBM_SWID_LOW_UNITTYPEID_BMSK                             0xff0000
#define HWIO_CNOC_NORTH_SBM_SWID_LOW_UNITTYPEID_SHFT                                 0x10
#define HWIO_CNOC_NORTH_SBM_SWID_LOW_UNITCONFID_BMSK                               0xffff
#define HWIO_CNOC_NORTH_SBM_SWID_LOW_UNITCONFID_SHFT                                  0x0

#define HWIO_CNOC_NORTH_SBM_SWID_HIGH_ADDR                                     (CONFIG_NOC_REG_BASE      + 0x00000c04)
#define HWIO_CNOC_NORTH_SBM_SWID_HIGH_RMSK                                     0xffffffff
#define HWIO_CNOC_NORTH_SBM_SWID_HIGH_IN          \
        in_dword(HWIO_CNOC_NORTH_SBM_SWID_HIGH_ADDR)
#define HWIO_CNOC_NORTH_SBM_SWID_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_NORTH_SBM_SWID_HIGH_ADDR, m)
#define HWIO_CNOC_NORTH_SBM_SWID_HIGH_QNOCID_BMSK                              0xffffffff
#define HWIO_CNOC_NORTH_SBM_SWID_HIGH_QNOCID_SHFT                                     0x0

#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_ADDR                                  (CONFIG_NOC_REG_BASE      + 0x00000d00)
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_RMSK                                       0x1ff
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_IN          \
        in_dword(HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_ADDR)
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_ADDR, m)
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_PORT8_BMSK                                 0x100
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_PORT8_SHFT                                   0x8
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_PORT7_BMSK                                  0x80
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_PORT7_SHFT                                   0x7
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_PORT6_BMSK                                  0x40
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_PORT6_SHFT                                   0x6
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_PORT5_BMSK                                  0x20
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_PORT5_SHFT                                   0x5
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_PORT4_BMSK                                  0x10
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_PORT4_SHFT                                   0x4
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_PORT3_BMSK                                   0x8
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_PORT3_SHFT                                   0x3
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_PORT2_BMSK                                   0x4
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_PORT2_SHFT                                   0x2
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_PORT1_BMSK                                   0x2
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_PORT1_SHFT                                   0x1
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_PORT0_BMSK                                   0x1
#define HWIO_CNOC_NORTH_SBM_SENSEIN0_LOW_PORT0_SHFT                                   0x0

#define HWIO_CNOC_CENTER_DCD_QHCLK_SWID_LOW_ADDR                               (CONFIG_NOC_REG_BASE      + 0x00002000)
#define HWIO_CNOC_CENTER_DCD_QHCLK_SWID_LOW_RMSK                                 0xffffff
#define HWIO_CNOC_CENTER_DCD_QHCLK_SWID_LOW_IN          \
        in_dword(HWIO_CNOC_CENTER_DCD_QHCLK_SWID_LOW_ADDR)
#define HWIO_CNOC_CENTER_DCD_QHCLK_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_CENTER_DCD_QHCLK_SWID_LOW_ADDR, m)
#define HWIO_CNOC_CENTER_DCD_QHCLK_SWID_LOW_UNITTYPEID_BMSK                      0xff0000
#define HWIO_CNOC_CENTER_DCD_QHCLK_SWID_LOW_UNITTYPEID_SHFT                          0x10
#define HWIO_CNOC_CENTER_DCD_QHCLK_SWID_LOW_UNITCONFID_BMSK                        0xffff
#define HWIO_CNOC_CENTER_DCD_QHCLK_SWID_LOW_UNITCONFID_SHFT                           0x0

#define HWIO_CNOC_CENTER_DCD_QHCLK_SWID_HIGH_ADDR                              (CONFIG_NOC_REG_BASE      + 0x00002004)
#define HWIO_CNOC_CENTER_DCD_QHCLK_SWID_HIGH_RMSK                              0xffffffff
#define HWIO_CNOC_CENTER_DCD_QHCLK_SWID_HIGH_IN          \
        in_dword(HWIO_CNOC_CENTER_DCD_QHCLK_SWID_HIGH_ADDR)
#define HWIO_CNOC_CENTER_DCD_QHCLK_SWID_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_CENTER_DCD_QHCLK_SWID_HIGH_ADDR, m)
#define HWIO_CNOC_CENTER_DCD_QHCLK_SWID_HIGH_QNOCID_BMSK                       0xffffffff
#define HWIO_CNOC_CENTER_DCD_QHCLK_SWID_HIGH_QNOCID_SHFT                              0x0

#define HWIO_CNOC_CENTER_DCD_QHCLK_MAXDIV_LOW_ADDR                             (CONFIG_NOC_REG_BASE      + 0x00002008)
#define HWIO_CNOC_CENTER_DCD_QHCLK_MAXDIV_LOW_RMSK                                    0x7
#define HWIO_CNOC_CENTER_DCD_QHCLK_MAXDIV_LOW_IN          \
        in_dword(HWIO_CNOC_CENTER_DCD_QHCLK_MAXDIV_LOW_ADDR)
#define HWIO_CNOC_CENTER_DCD_QHCLK_MAXDIV_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_CENTER_DCD_QHCLK_MAXDIV_LOW_ADDR, m)
#define HWIO_CNOC_CENTER_DCD_QHCLK_MAXDIV_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_CENTER_DCD_QHCLK_MAXDIV_LOW_ADDR,v)
#define HWIO_CNOC_CENTER_DCD_QHCLK_MAXDIV_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_CENTER_DCD_QHCLK_MAXDIV_LOW_ADDR,m,v,HWIO_CNOC_CENTER_DCD_QHCLK_MAXDIV_LOW_IN)
#define HWIO_CNOC_CENTER_DCD_QHCLK_MAXDIV_LOW_MAXDIV_BMSK                             0x7
#define HWIO_CNOC_CENTER_DCD_QHCLK_MAXDIV_LOW_MAXDIV_SHFT                             0x0

#define HWIO_CNOC_CENTER_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR                       (CONFIG_NOC_REG_BASE      + 0x00002010)
#define HWIO_CNOC_CENTER_DCD_QHCLK_FIRSTHYSTCNT_LOW_RMSK                           0xffff
#define HWIO_CNOC_CENTER_DCD_QHCLK_FIRSTHYSTCNT_LOW_IN          \
        in_dword(HWIO_CNOC_CENTER_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR)
#define HWIO_CNOC_CENTER_DCD_QHCLK_FIRSTHYSTCNT_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_CENTER_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR, m)
#define HWIO_CNOC_CENTER_DCD_QHCLK_FIRSTHYSTCNT_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_CENTER_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR,v)
#define HWIO_CNOC_CENTER_DCD_QHCLK_FIRSTHYSTCNT_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_CENTER_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR,m,v,HWIO_CNOC_CENTER_DCD_QHCLK_FIRSTHYSTCNT_LOW_IN)
#define HWIO_CNOC_CENTER_DCD_QHCLK_FIRSTHYSTCNT_LOW_FIRSTHYSTCNT_BMSK              0xffff
#define HWIO_CNOC_CENTER_DCD_QHCLK_FIRSTHYSTCNT_LOW_FIRSTHYSTCNT_SHFT                 0x0

#define HWIO_CNOC_CENTER_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR                        (CONFIG_NOC_REG_BASE      + 0x00002018)
#define HWIO_CNOC_CENTER_DCD_QHCLK_NEXTHYSTCNT_LOW_RMSK                             0xfff
#define HWIO_CNOC_CENTER_DCD_QHCLK_NEXTHYSTCNT_LOW_IN          \
        in_dword(HWIO_CNOC_CENTER_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR)
#define HWIO_CNOC_CENTER_DCD_QHCLK_NEXTHYSTCNT_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_CENTER_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR, m)
#define HWIO_CNOC_CENTER_DCD_QHCLK_NEXTHYSTCNT_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_CENTER_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR,v)
#define HWIO_CNOC_CENTER_DCD_QHCLK_NEXTHYSTCNT_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_CENTER_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR,m,v,HWIO_CNOC_CENTER_DCD_QHCLK_NEXTHYSTCNT_LOW_IN)
#define HWIO_CNOC_CENTER_DCD_QHCLK_NEXTHYSTCNT_LOW_NEXTHYSTCNT_BMSK                 0xfff
#define HWIO_CNOC_CENTER_DCD_QHCLK_NEXTHYSTCNT_LOW_NEXTHYSTCNT_SHFT                   0x0

#define HWIO_CNOC_WEST_DCD_QHCLK_SWID_LOW_ADDR                                 (CONFIG_NOC_REG_BASE      + 0x00002100)
#define HWIO_CNOC_WEST_DCD_QHCLK_SWID_LOW_RMSK                                   0xffffff
#define HWIO_CNOC_WEST_DCD_QHCLK_SWID_LOW_IN          \
        in_dword(HWIO_CNOC_WEST_DCD_QHCLK_SWID_LOW_ADDR)
#define HWIO_CNOC_WEST_DCD_QHCLK_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_WEST_DCD_QHCLK_SWID_LOW_ADDR, m)
#define HWIO_CNOC_WEST_DCD_QHCLK_SWID_LOW_UNITTYPEID_BMSK                        0xff0000
#define HWIO_CNOC_WEST_DCD_QHCLK_SWID_LOW_UNITTYPEID_SHFT                            0x10
#define HWIO_CNOC_WEST_DCD_QHCLK_SWID_LOW_UNITCONFID_BMSK                          0xffff
#define HWIO_CNOC_WEST_DCD_QHCLK_SWID_LOW_UNITCONFID_SHFT                             0x0

#define HWIO_CNOC_WEST_DCD_QHCLK_SWID_HIGH_ADDR                                (CONFIG_NOC_REG_BASE      + 0x00002104)
#define HWIO_CNOC_WEST_DCD_QHCLK_SWID_HIGH_RMSK                                0xffffffff
#define HWIO_CNOC_WEST_DCD_QHCLK_SWID_HIGH_IN          \
        in_dword(HWIO_CNOC_WEST_DCD_QHCLK_SWID_HIGH_ADDR)
#define HWIO_CNOC_WEST_DCD_QHCLK_SWID_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_WEST_DCD_QHCLK_SWID_HIGH_ADDR, m)
#define HWIO_CNOC_WEST_DCD_QHCLK_SWID_HIGH_QNOCID_BMSK                         0xffffffff
#define HWIO_CNOC_WEST_DCD_QHCLK_SWID_HIGH_QNOCID_SHFT                                0x0

#define HWIO_CNOC_WEST_DCD_QHCLK_MAXDIV_LOW_ADDR                               (CONFIG_NOC_REG_BASE      + 0x00002108)
#define HWIO_CNOC_WEST_DCD_QHCLK_MAXDIV_LOW_RMSK                                      0x7
#define HWIO_CNOC_WEST_DCD_QHCLK_MAXDIV_LOW_IN          \
        in_dword(HWIO_CNOC_WEST_DCD_QHCLK_MAXDIV_LOW_ADDR)
#define HWIO_CNOC_WEST_DCD_QHCLK_MAXDIV_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_WEST_DCD_QHCLK_MAXDIV_LOW_ADDR, m)
#define HWIO_CNOC_WEST_DCD_QHCLK_MAXDIV_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_WEST_DCD_QHCLK_MAXDIV_LOW_ADDR,v)
#define HWIO_CNOC_WEST_DCD_QHCLK_MAXDIV_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_WEST_DCD_QHCLK_MAXDIV_LOW_ADDR,m,v,HWIO_CNOC_WEST_DCD_QHCLK_MAXDIV_LOW_IN)
#define HWIO_CNOC_WEST_DCD_QHCLK_MAXDIV_LOW_MAXDIV_BMSK                               0x7
#define HWIO_CNOC_WEST_DCD_QHCLK_MAXDIV_LOW_MAXDIV_SHFT                               0x0

#define HWIO_CNOC_WEST_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR                         (CONFIG_NOC_REG_BASE      + 0x00002110)
#define HWIO_CNOC_WEST_DCD_QHCLK_FIRSTHYSTCNT_LOW_RMSK                             0xffff
#define HWIO_CNOC_WEST_DCD_QHCLK_FIRSTHYSTCNT_LOW_IN          \
        in_dword(HWIO_CNOC_WEST_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR)
#define HWIO_CNOC_WEST_DCD_QHCLK_FIRSTHYSTCNT_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_WEST_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR, m)
#define HWIO_CNOC_WEST_DCD_QHCLK_FIRSTHYSTCNT_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_WEST_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR,v)
#define HWIO_CNOC_WEST_DCD_QHCLK_FIRSTHYSTCNT_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_WEST_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR,m,v,HWIO_CNOC_WEST_DCD_QHCLK_FIRSTHYSTCNT_LOW_IN)
#define HWIO_CNOC_WEST_DCD_QHCLK_FIRSTHYSTCNT_LOW_FIRSTHYSTCNT_BMSK                0xffff
#define HWIO_CNOC_WEST_DCD_QHCLK_FIRSTHYSTCNT_LOW_FIRSTHYSTCNT_SHFT                   0x0

#define HWIO_CNOC_WEST_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR                          (CONFIG_NOC_REG_BASE      + 0x00002118)
#define HWIO_CNOC_WEST_DCD_QHCLK_NEXTHYSTCNT_LOW_RMSK                               0xfff
#define HWIO_CNOC_WEST_DCD_QHCLK_NEXTHYSTCNT_LOW_IN          \
        in_dword(HWIO_CNOC_WEST_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR)
#define HWIO_CNOC_WEST_DCD_QHCLK_NEXTHYSTCNT_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_WEST_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR, m)
#define HWIO_CNOC_WEST_DCD_QHCLK_NEXTHYSTCNT_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_WEST_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR,v)
#define HWIO_CNOC_WEST_DCD_QHCLK_NEXTHYSTCNT_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_WEST_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR,m,v,HWIO_CNOC_WEST_DCD_QHCLK_NEXTHYSTCNT_LOW_IN)
#define HWIO_CNOC_WEST_DCD_QHCLK_NEXTHYSTCNT_LOW_NEXTHYSTCNT_BMSK                   0xfff
#define HWIO_CNOC_WEST_DCD_QHCLK_NEXTHYSTCNT_LOW_NEXTHYSTCNT_SHFT                     0x0

#define HWIO_CNOC_NORTH_DCD_QHCLK_SWID_LOW_ADDR                                (CONFIG_NOC_REG_BASE      + 0x00002200)
#define HWIO_CNOC_NORTH_DCD_QHCLK_SWID_LOW_RMSK                                  0xffffff
#define HWIO_CNOC_NORTH_DCD_QHCLK_SWID_LOW_IN          \
        in_dword(HWIO_CNOC_NORTH_DCD_QHCLK_SWID_LOW_ADDR)
#define HWIO_CNOC_NORTH_DCD_QHCLK_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_NORTH_DCD_QHCLK_SWID_LOW_ADDR, m)
#define HWIO_CNOC_NORTH_DCD_QHCLK_SWID_LOW_UNITTYPEID_BMSK                       0xff0000
#define HWIO_CNOC_NORTH_DCD_QHCLK_SWID_LOW_UNITTYPEID_SHFT                           0x10
#define HWIO_CNOC_NORTH_DCD_QHCLK_SWID_LOW_UNITCONFID_BMSK                         0xffff
#define HWIO_CNOC_NORTH_DCD_QHCLK_SWID_LOW_UNITCONFID_SHFT                            0x0

#define HWIO_CNOC_NORTH_DCD_QHCLK_SWID_HIGH_ADDR                               (CONFIG_NOC_REG_BASE      + 0x00002204)
#define HWIO_CNOC_NORTH_DCD_QHCLK_SWID_HIGH_RMSK                               0xffffffff
#define HWIO_CNOC_NORTH_DCD_QHCLK_SWID_HIGH_IN          \
        in_dword(HWIO_CNOC_NORTH_DCD_QHCLK_SWID_HIGH_ADDR)
#define HWIO_CNOC_NORTH_DCD_QHCLK_SWID_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_NORTH_DCD_QHCLK_SWID_HIGH_ADDR, m)
#define HWIO_CNOC_NORTH_DCD_QHCLK_SWID_HIGH_QNOCID_BMSK                        0xffffffff
#define HWIO_CNOC_NORTH_DCD_QHCLK_SWID_HIGH_QNOCID_SHFT                               0x0

#define HWIO_CNOC_NORTH_DCD_QHCLK_MAXDIV_LOW_ADDR                              (CONFIG_NOC_REG_BASE      + 0x00002208)
#define HWIO_CNOC_NORTH_DCD_QHCLK_MAXDIV_LOW_RMSK                                     0x7
#define HWIO_CNOC_NORTH_DCD_QHCLK_MAXDIV_LOW_IN          \
        in_dword(HWIO_CNOC_NORTH_DCD_QHCLK_MAXDIV_LOW_ADDR)
#define HWIO_CNOC_NORTH_DCD_QHCLK_MAXDIV_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_NORTH_DCD_QHCLK_MAXDIV_LOW_ADDR, m)
#define HWIO_CNOC_NORTH_DCD_QHCLK_MAXDIV_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_NORTH_DCD_QHCLK_MAXDIV_LOW_ADDR,v)
#define HWIO_CNOC_NORTH_DCD_QHCLK_MAXDIV_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_NORTH_DCD_QHCLK_MAXDIV_LOW_ADDR,m,v,HWIO_CNOC_NORTH_DCD_QHCLK_MAXDIV_LOW_IN)
#define HWIO_CNOC_NORTH_DCD_QHCLK_MAXDIV_LOW_MAXDIV_BMSK                              0x7
#define HWIO_CNOC_NORTH_DCD_QHCLK_MAXDIV_LOW_MAXDIV_SHFT                              0x0

#define HWIO_CNOC_NORTH_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR                        (CONFIG_NOC_REG_BASE      + 0x00002210)
#define HWIO_CNOC_NORTH_DCD_QHCLK_FIRSTHYSTCNT_LOW_RMSK                            0xffff
#define HWIO_CNOC_NORTH_DCD_QHCLK_FIRSTHYSTCNT_LOW_IN          \
        in_dword(HWIO_CNOC_NORTH_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR)
#define HWIO_CNOC_NORTH_DCD_QHCLK_FIRSTHYSTCNT_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_NORTH_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR, m)
#define HWIO_CNOC_NORTH_DCD_QHCLK_FIRSTHYSTCNT_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_NORTH_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR,v)
#define HWIO_CNOC_NORTH_DCD_QHCLK_FIRSTHYSTCNT_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_NORTH_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR,m,v,HWIO_CNOC_NORTH_DCD_QHCLK_FIRSTHYSTCNT_LOW_IN)
#define HWIO_CNOC_NORTH_DCD_QHCLK_FIRSTHYSTCNT_LOW_FIRSTHYSTCNT_BMSK               0xffff
#define HWIO_CNOC_NORTH_DCD_QHCLK_FIRSTHYSTCNT_LOW_FIRSTHYSTCNT_SHFT                  0x0

#define HWIO_CNOC_NORTH_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR                         (CONFIG_NOC_REG_BASE      + 0x00002218)
#define HWIO_CNOC_NORTH_DCD_QHCLK_NEXTHYSTCNT_LOW_RMSK                              0xfff
#define HWIO_CNOC_NORTH_DCD_QHCLK_NEXTHYSTCNT_LOW_IN          \
        in_dword(HWIO_CNOC_NORTH_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR)
#define HWIO_CNOC_NORTH_DCD_QHCLK_NEXTHYSTCNT_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_NORTH_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR, m)
#define HWIO_CNOC_NORTH_DCD_QHCLK_NEXTHYSTCNT_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_NORTH_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR,v)
#define HWIO_CNOC_NORTH_DCD_QHCLK_NEXTHYSTCNT_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_NORTH_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR,m,v,HWIO_CNOC_NORTH_DCD_QHCLK_NEXTHYSTCNT_LOW_IN)
#define HWIO_CNOC_NORTH_DCD_QHCLK_NEXTHYSTCNT_LOW_NEXTHYSTCNT_BMSK                  0xfff
#define HWIO_CNOC_NORTH_DCD_QHCLK_NEXTHYSTCNT_LOW_NEXTHYSTCNT_SHFT                    0x0

#define HWIO_CNOC_MONAQ_DCD_QHCLK_SWID_LOW_ADDR                                (CONFIG_NOC_REG_BASE      + 0x00002300)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_SWID_LOW_RMSK                                  0xffffff
#define HWIO_CNOC_MONAQ_DCD_QHCLK_SWID_LOW_IN          \
        in_dword(HWIO_CNOC_MONAQ_DCD_QHCLK_SWID_LOW_ADDR)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_MONAQ_DCD_QHCLK_SWID_LOW_ADDR, m)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_SWID_LOW_UNITTYPEID_BMSK                       0xff0000
#define HWIO_CNOC_MONAQ_DCD_QHCLK_SWID_LOW_UNITTYPEID_SHFT                           0x10
#define HWIO_CNOC_MONAQ_DCD_QHCLK_SWID_LOW_UNITCONFID_BMSK                         0xffff
#define HWIO_CNOC_MONAQ_DCD_QHCLK_SWID_LOW_UNITCONFID_SHFT                            0x0

#define HWIO_CNOC_MONAQ_DCD_QHCLK_SWID_HIGH_ADDR                               (CONFIG_NOC_REG_BASE      + 0x00002304)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_SWID_HIGH_RMSK                               0xffffffff
#define HWIO_CNOC_MONAQ_DCD_QHCLK_SWID_HIGH_IN          \
        in_dword(HWIO_CNOC_MONAQ_DCD_QHCLK_SWID_HIGH_ADDR)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_SWID_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_MONAQ_DCD_QHCLK_SWID_HIGH_ADDR, m)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_SWID_HIGH_QNOCID_BMSK                        0xffffffff
#define HWIO_CNOC_MONAQ_DCD_QHCLK_SWID_HIGH_QNOCID_SHFT                               0x0

#define HWIO_CNOC_MONAQ_DCD_QHCLK_MAXDIV_LOW_ADDR                              (CONFIG_NOC_REG_BASE      + 0x00002308)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_MAXDIV_LOW_RMSK                                     0x7
#define HWIO_CNOC_MONAQ_DCD_QHCLK_MAXDIV_LOW_IN          \
        in_dword(HWIO_CNOC_MONAQ_DCD_QHCLK_MAXDIV_LOW_ADDR)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_MAXDIV_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_MONAQ_DCD_QHCLK_MAXDIV_LOW_ADDR, m)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_MAXDIV_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_MONAQ_DCD_QHCLK_MAXDIV_LOW_ADDR,v)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_MAXDIV_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_MONAQ_DCD_QHCLK_MAXDIV_LOW_ADDR,m,v,HWIO_CNOC_MONAQ_DCD_QHCLK_MAXDIV_LOW_IN)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_MAXDIV_LOW_MAXDIV_BMSK                              0x7
#define HWIO_CNOC_MONAQ_DCD_QHCLK_MAXDIV_LOW_MAXDIV_SHFT                              0x0

#define HWIO_CNOC_MONAQ_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR                        (CONFIG_NOC_REG_BASE      + 0x00002310)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_FIRSTHYSTCNT_LOW_RMSK                            0xffff
#define HWIO_CNOC_MONAQ_DCD_QHCLK_FIRSTHYSTCNT_LOW_IN          \
        in_dword(HWIO_CNOC_MONAQ_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_FIRSTHYSTCNT_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_MONAQ_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR, m)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_FIRSTHYSTCNT_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_MONAQ_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR,v)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_FIRSTHYSTCNT_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_MONAQ_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR,m,v,HWIO_CNOC_MONAQ_DCD_QHCLK_FIRSTHYSTCNT_LOW_IN)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_FIRSTHYSTCNT_LOW_FIRSTHYSTCNT_BMSK               0xffff
#define HWIO_CNOC_MONAQ_DCD_QHCLK_FIRSTHYSTCNT_LOW_FIRSTHYSTCNT_SHFT                  0x0

#define HWIO_CNOC_MONAQ_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR                         (CONFIG_NOC_REG_BASE      + 0x00002318)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_NEXTHYSTCNT_LOW_RMSK                              0xfff
#define HWIO_CNOC_MONAQ_DCD_QHCLK_NEXTHYSTCNT_LOW_IN          \
        in_dword(HWIO_CNOC_MONAQ_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_NEXTHYSTCNT_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_MONAQ_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR, m)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_NEXTHYSTCNT_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_MONAQ_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR,v)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_NEXTHYSTCNT_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_MONAQ_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR,m,v,HWIO_CNOC_MONAQ_DCD_QHCLK_NEXTHYSTCNT_LOW_IN)
#define HWIO_CNOC_MONAQ_DCD_QHCLK_NEXTHYSTCNT_LOW_NEXTHYSTCNT_BMSK                  0xfff
#define HWIO_CNOC_MONAQ_DCD_QHCLK_NEXTHYSTCNT_LOW_NEXTHYSTCNT_SHFT                    0x0

#define HWIO_CNOC_EAST_DCD_QHCLK_SWID_LOW_ADDR                                 (CONFIG_NOC_REG_BASE      + 0x00002400)
#define HWIO_CNOC_EAST_DCD_QHCLK_SWID_LOW_RMSK                                   0xffffff
#define HWIO_CNOC_EAST_DCD_QHCLK_SWID_LOW_IN          \
        in_dword(HWIO_CNOC_EAST_DCD_QHCLK_SWID_LOW_ADDR)
#define HWIO_CNOC_EAST_DCD_QHCLK_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_EAST_DCD_QHCLK_SWID_LOW_ADDR, m)
#define HWIO_CNOC_EAST_DCD_QHCLK_SWID_LOW_UNITTYPEID_BMSK                        0xff0000
#define HWIO_CNOC_EAST_DCD_QHCLK_SWID_LOW_UNITTYPEID_SHFT                            0x10
#define HWIO_CNOC_EAST_DCD_QHCLK_SWID_LOW_UNITCONFID_BMSK                          0xffff
#define HWIO_CNOC_EAST_DCD_QHCLK_SWID_LOW_UNITCONFID_SHFT                             0x0

#define HWIO_CNOC_EAST_DCD_QHCLK_SWID_HIGH_ADDR                                (CONFIG_NOC_REG_BASE      + 0x00002404)
#define HWIO_CNOC_EAST_DCD_QHCLK_SWID_HIGH_RMSK                                0xffffffff
#define HWIO_CNOC_EAST_DCD_QHCLK_SWID_HIGH_IN          \
        in_dword(HWIO_CNOC_EAST_DCD_QHCLK_SWID_HIGH_ADDR)
#define HWIO_CNOC_EAST_DCD_QHCLK_SWID_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_EAST_DCD_QHCLK_SWID_HIGH_ADDR, m)
#define HWIO_CNOC_EAST_DCD_QHCLK_SWID_HIGH_QNOCID_BMSK                         0xffffffff
#define HWIO_CNOC_EAST_DCD_QHCLK_SWID_HIGH_QNOCID_SHFT                                0x0

#define HWIO_CNOC_EAST_DCD_QHCLK_MAXDIV_LOW_ADDR                               (CONFIG_NOC_REG_BASE      + 0x00002408)
#define HWIO_CNOC_EAST_DCD_QHCLK_MAXDIV_LOW_RMSK                                      0x7
#define HWIO_CNOC_EAST_DCD_QHCLK_MAXDIV_LOW_IN          \
        in_dword(HWIO_CNOC_EAST_DCD_QHCLK_MAXDIV_LOW_ADDR)
#define HWIO_CNOC_EAST_DCD_QHCLK_MAXDIV_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_EAST_DCD_QHCLK_MAXDIV_LOW_ADDR, m)
#define HWIO_CNOC_EAST_DCD_QHCLK_MAXDIV_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_EAST_DCD_QHCLK_MAXDIV_LOW_ADDR,v)
#define HWIO_CNOC_EAST_DCD_QHCLK_MAXDIV_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_EAST_DCD_QHCLK_MAXDIV_LOW_ADDR,m,v,HWIO_CNOC_EAST_DCD_QHCLK_MAXDIV_LOW_IN)
#define HWIO_CNOC_EAST_DCD_QHCLK_MAXDIV_LOW_MAXDIV_BMSK                               0x7
#define HWIO_CNOC_EAST_DCD_QHCLK_MAXDIV_LOW_MAXDIV_SHFT                               0x0

#define HWIO_CNOC_EAST_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR                         (CONFIG_NOC_REG_BASE      + 0x00002410)
#define HWIO_CNOC_EAST_DCD_QHCLK_FIRSTHYSTCNT_LOW_RMSK                             0xffff
#define HWIO_CNOC_EAST_DCD_QHCLK_FIRSTHYSTCNT_LOW_IN          \
        in_dword(HWIO_CNOC_EAST_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR)
#define HWIO_CNOC_EAST_DCD_QHCLK_FIRSTHYSTCNT_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_EAST_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR, m)
#define HWIO_CNOC_EAST_DCD_QHCLK_FIRSTHYSTCNT_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_EAST_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR,v)
#define HWIO_CNOC_EAST_DCD_QHCLK_FIRSTHYSTCNT_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_EAST_DCD_QHCLK_FIRSTHYSTCNT_LOW_ADDR,m,v,HWIO_CNOC_EAST_DCD_QHCLK_FIRSTHYSTCNT_LOW_IN)
#define HWIO_CNOC_EAST_DCD_QHCLK_FIRSTHYSTCNT_LOW_FIRSTHYSTCNT_BMSK                0xffff
#define HWIO_CNOC_EAST_DCD_QHCLK_FIRSTHYSTCNT_LOW_FIRSTHYSTCNT_SHFT                   0x0

#define HWIO_CNOC_EAST_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR                          (CONFIG_NOC_REG_BASE      + 0x00002418)
#define HWIO_CNOC_EAST_DCD_QHCLK_NEXTHYSTCNT_LOW_RMSK                               0xfff
#define HWIO_CNOC_EAST_DCD_QHCLK_NEXTHYSTCNT_LOW_IN          \
        in_dword(HWIO_CNOC_EAST_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR)
#define HWIO_CNOC_EAST_DCD_QHCLK_NEXTHYSTCNT_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_EAST_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR, m)
#define HWIO_CNOC_EAST_DCD_QHCLK_NEXTHYSTCNT_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_EAST_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR,v)
#define HWIO_CNOC_EAST_DCD_QHCLK_NEXTHYSTCNT_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_EAST_DCD_QHCLK_NEXTHYSTCNT_LOW_ADDR,m,v,HWIO_CNOC_EAST_DCD_QHCLK_NEXTHYSTCNT_LOW_IN)
#define HWIO_CNOC_EAST_DCD_QHCLK_NEXTHYSTCNT_LOW_NEXTHYSTCNT_BMSK                   0xfff
#define HWIO_CNOC_EAST_DCD_QHCLK_NEXTHYSTCNT_LOW_NEXTHYSTCNT_SHFT                     0x0

#define HWIO_CNOC_RPM_M2_DYNATTR_SWID_LOW_ADDR                                 (CONFIG_NOC_REG_BASE      + 0x00006000)
#define HWIO_CNOC_RPM_M2_DYNATTR_SWID_LOW_RMSK                                   0xffffff
#define HWIO_CNOC_RPM_M2_DYNATTR_SWID_LOW_IN          \
        in_dword(HWIO_CNOC_RPM_M2_DYNATTR_SWID_LOW_ADDR)
#define HWIO_CNOC_RPM_M2_DYNATTR_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_RPM_M2_DYNATTR_SWID_LOW_ADDR, m)
#define HWIO_CNOC_RPM_M2_DYNATTR_SWID_LOW_UNITTYPEID_BMSK                        0xff0000
#define HWIO_CNOC_RPM_M2_DYNATTR_SWID_LOW_UNITTYPEID_SHFT                            0x10
#define HWIO_CNOC_RPM_M2_DYNATTR_SWID_LOW_UNITCONFID_BMSK                          0xffff
#define HWIO_CNOC_RPM_M2_DYNATTR_SWID_LOW_UNITCONFID_SHFT                             0x0

#define HWIO_CNOC_RPM_M2_DYNATTR_SWID_HIGH_ADDR                                (CONFIG_NOC_REG_BASE      + 0x00006004)
#define HWIO_CNOC_RPM_M2_DYNATTR_SWID_HIGH_RMSK                                0xffffffff
#define HWIO_CNOC_RPM_M2_DYNATTR_SWID_HIGH_IN          \
        in_dword(HWIO_CNOC_RPM_M2_DYNATTR_SWID_HIGH_ADDR)
#define HWIO_CNOC_RPM_M2_DYNATTR_SWID_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_RPM_M2_DYNATTR_SWID_HIGH_ADDR, m)
#define HWIO_CNOC_RPM_M2_DYNATTR_SWID_HIGH_QNOCID_BMSK                         0xffffffff
#define HWIO_CNOC_RPM_M2_DYNATTR_SWID_HIGH_QNOCID_SHFT                                0x0

#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_ADDR                              (CONFIG_NOC_REG_BASE      + 0x00006008)
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_RMSK                                0x1fffff
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_IN          \
        in_dword(HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_ADDR)
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_ADDR, m)
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_ADDR,v)
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_ADDR,m,v,HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_IN)
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_CACHEINDEXOV_BMSK                   0x100000
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_CACHEINDEXOV_SHFT                       0x14
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_RSV3_BMSK                            0x80000
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_RSV3_SHFT                               0x13
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_ALLOCCTLOV_BMSK                      0x40000
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_ALLOCCTLOV_SHFT                         0x12
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_ALLOCWROV_BMSK                       0x20000
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_ALLOCWROV_SHFT                          0x11
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_ALLOCRDOV_BMSK                       0x10000
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_ALLOCRDOV_SHFT                          0x10
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_CMFPWRVAL_BMSK                        0x8000
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_CMFPWRVAL_SHFT                           0xf
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_CMFPWROV_BMSK                         0x4000
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_CMFPWROV_SHFT                            0xe
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_CMFPRDVAL_BMSK                        0x2000
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_CMFPRDVAL_SHFT                           0xd
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_CMFPRDOV_BMSK                         0x1000
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_CMFPRDOV_SHFT                            0xc
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_RSV2_BMSK                              0xc00
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_RSV2_SHFT                                0xa
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_DEVICEVAL_BMSK                         0x200
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_DEVICEVAL_SHFT                           0x9
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_DEVICEOV_BMSK                          0x100
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_DEVICEOV_SHFT                            0x8
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_RSV1_BMSK                               0xe0
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_RSV1_SHFT                                0x5
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_QOSDISABLE_BMSK                         0x10
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_QOSDISABLE_SHFT                          0x4
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_RSV0_BMSK                                0xc
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_RSV0_SHFT                                0x2
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_MODE_BMSK                                0x3
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINCTL_LOW_MODE_SHFT                                0x0

#define HWIO_CNOC_RPM_M2_DYNATTR_MAINSTATUS_LOW_ADDR                           (CONFIG_NOC_REG_BASE      + 0x00006010)
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINSTATUS_LOW_RMSK                                  0x3
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINSTATUS_LOW_IN          \
        in_dword(HWIO_CNOC_RPM_M2_DYNATTR_MAINSTATUS_LOW_ADDR)
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINSTATUS_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_RPM_M2_DYNATTR_MAINSTATUS_LOW_ADDR, m)
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINSTATUS_LOW_URGPENDING_BMSK                       0x2
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINSTATUS_LOW_URGPENDING_SHFT                       0x1
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINSTATUS_LOW_TRPENDING_BMSK                        0x1
#define HWIO_CNOC_RPM_M2_DYNATTR_MAINSTATUS_LOW_TRPENDING_SHFT                        0x0

#define HWIO_CNOC_RPM_M2_DYNATTR_REQUSEROV_LOW_ADDR                            (CONFIG_NOC_REG_BASE      + 0x00006018)
#define HWIO_CNOC_RPM_M2_DYNATTR_REQUSEROV_LOW_RMSK                                   0x1
#define HWIO_CNOC_RPM_M2_DYNATTR_REQUSEROV_LOW_IN          \
        in_dword(HWIO_CNOC_RPM_M2_DYNATTR_REQUSEROV_LOW_ADDR)
#define HWIO_CNOC_RPM_M2_DYNATTR_REQUSEROV_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_RPM_M2_DYNATTR_REQUSEROV_LOW_ADDR, m)
#define HWIO_CNOC_RPM_M2_DYNATTR_REQUSEROV_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_RPM_M2_DYNATTR_REQUSEROV_LOW_ADDR,v)
#define HWIO_CNOC_RPM_M2_DYNATTR_REQUSEROV_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_RPM_M2_DYNATTR_REQUSEROV_LOW_ADDR,m,v,HWIO_CNOC_RPM_M2_DYNATTR_REQUSEROV_LOW_IN)
#define HWIO_CNOC_RPM_M2_DYNATTR_REQUSEROV_LOW_REQUSEROV_BMSK                         0x1
#define HWIO_CNOC_RPM_M2_DYNATTR_REQUSEROV_LOW_REQUSEROV_SHFT                         0x0

#define HWIO_CNOC_RPM_M2_DYNATTR_REQUSERVAL_LOW_ADDR                           (CONFIG_NOC_REG_BASE      + 0x00006020)
#define HWIO_CNOC_RPM_M2_DYNATTR_REQUSERVAL_LOW_RMSK                                  0x1
#define HWIO_CNOC_RPM_M2_DYNATTR_REQUSERVAL_LOW_IN          \
        in_dword(HWIO_CNOC_RPM_M2_DYNATTR_REQUSERVAL_LOW_ADDR)
#define HWIO_CNOC_RPM_M2_DYNATTR_REQUSERVAL_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_RPM_M2_DYNATTR_REQUSERVAL_LOW_ADDR, m)
#define HWIO_CNOC_RPM_M2_DYNATTR_REQUSERVAL_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_RPM_M2_DYNATTR_REQUSERVAL_LOW_ADDR,v)
#define HWIO_CNOC_RPM_M2_DYNATTR_REQUSERVAL_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_RPM_M2_DYNATTR_REQUSERVAL_LOW_ADDR,m,v,HWIO_CNOC_RPM_M2_DYNATTR_REQUSERVAL_LOW_IN)
#define HWIO_CNOC_RPM_M2_DYNATTR_REQUSERVAL_LOW_REQUSERVAL_BMSK                       0x1
#define HWIO_CNOC_RPM_M2_DYNATTR_REQUSERVAL_LOW_REQUSERVAL_SHFT                       0x0

#define HWIO_CNOC_RPM_M2_DYNATTR_LOGUSEROV_LOW_ADDR                            (CONFIG_NOC_REG_BASE      + 0x00006028)
#define HWIO_CNOC_RPM_M2_DYNATTR_LOGUSEROV_LOW_RMSK                               0x7ffff
#define HWIO_CNOC_RPM_M2_DYNATTR_LOGUSEROV_LOW_IN          \
        in_dword(HWIO_CNOC_RPM_M2_DYNATTR_LOGUSEROV_LOW_ADDR)
#define HWIO_CNOC_RPM_M2_DYNATTR_LOGUSEROV_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_RPM_M2_DYNATTR_LOGUSEROV_LOW_ADDR, m)
#define HWIO_CNOC_RPM_M2_DYNATTR_LOGUSEROV_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_RPM_M2_DYNATTR_LOGUSEROV_LOW_ADDR,v)
#define HWIO_CNOC_RPM_M2_DYNATTR_LOGUSEROV_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_RPM_M2_DYNATTR_LOGUSEROV_LOW_ADDR,m,v,HWIO_CNOC_RPM_M2_DYNATTR_LOGUSEROV_LOW_IN)
#define HWIO_CNOC_RPM_M2_DYNATTR_LOGUSEROV_LOW_LOGUSEROV_BMSK                     0x7ffff
#define HWIO_CNOC_RPM_M2_DYNATTR_LOGUSEROV_LOW_LOGUSEROV_SHFT                         0x0

#define HWIO_CNOC_RPM_M2_DYNATTR_LOGUSERVAL_LOW_ADDR                           (CONFIG_NOC_REG_BASE      + 0x00006030)
#define HWIO_CNOC_RPM_M2_DYNATTR_LOGUSERVAL_LOW_RMSK                              0x7ffff
#define HWIO_CNOC_RPM_M2_DYNATTR_LOGUSERVAL_LOW_IN          \
        in_dword(HWIO_CNOC_RPM_M2_DYNATTR_LOGUSERVAL_LOW_ADDR)
#define HWIO_CNOC_RPM_M2_DYNATTR_LOGUSERVAL_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_RPM_M2_DYNATTR_LOGUSERVAL_LOW_ADDR, m)
#define HWIO_CNOC_RPM_M2_DYNATTR_LOGUSERVAL_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_RPM_M2_DYNATTR_LOGUSERVAL_LOW_ADDR,v)
#define HWIO_CNOC_RPM_M2_DYNATTR_LOGUSERVAL_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_RPM_M2_DYNATTR_LOGUSERVAL_LOW_ADDR,m,v,HWIO_CNOC_RPM_M2_DYNATTR_LOGUSERVAL_LOW_IN)
#define HWIO_CNOC_RPM_M2_DYNATTR_LOGUSERVAL_LOW_LOGUSERVAL_BMSK                   0x7ffff
#define HWIO_CNOC_RPM_M2_DYNATTR_LOGUSERVAL_LOW_LOGUSERVAL_SHFT                       0x0

#define HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_ADDR                                (CONFIG_NOC_REG_BASE      + 0x00006048)
#define HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_RMSK                                   0xfffff
#define HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_IN          \
        in_dword(HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_ADDR)
#define HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_ADDR, m)
#define HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_ADDR,v)
#define HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_ADDR,m,v,HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_IN)
#define HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_CTLVAL_BMSK                            0xf0000
#define HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_CTLVAL_SHFT                               0x10
#define HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_RSV1_BMSK                               0xf000
#define HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_RSV1_SHFT                                  0xc
#define HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_WRVAL_BMSK                               0xf00
#define HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_WRVAL_SHFT                                 0x8
#define HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_RSV0_BMSK                                 0xf0
#define HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_RSV0_SHFT                                  0x4
#define HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_RDVAL_BMSK                                 0xf
#define HWIO_CNOC_RPM_M2_DYNATTR_ALLOC_LOW_RDVAL_SHFT                                 0x0

#define HWIO_CNOC_RPM_M2_DYNATTR_CACHEINDEXVAL_LOW_ADDR                        (CONFIG_NOC_REG_BASE      + 0x00006050)
#define HWIO_CNOC_RPM_M2_DYNATTR_CACHEINDEXVAL_LOW_RMSK                              0x1f
#define HWIO_CNOC_RPM_M2_DYNATTR_CACHEINDEXVAL_LOW_IN          \
        in_dword(HWIO_CNOC_RPM_M2_DYNATTR_CACHEINDEXVAL_LOW_ADDR)
#define HWIO_CNOC_RPM_M2_DYNATTR_CACHEINDEXVAL_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_RPM_M2_DYNATTR_CACHEINDEXVAL_LOW_ADDR, m)
#define HWIO_CNOC_RPM_M2_DYNATTR_CACHEINDEXVAL_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_RPM_M2_DYNATTR_CACHEINDEXVAL_LOW_ADDR,v)
#define HWIO_CNOC_RPM_M2_DYNATTR_CACHEINDEXVAL_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CNOC_RPM_M2_DYNATTR_CACHEINDEXVAL_LOW_ADDR,m,v,HWIO_CNOC_RPM_M2_DYNATTR_CACHEINDEXVAL_LOW_IN)
#define HWIO_CNOC_RPM_M2_DYNATTR_CACHEINDEXVAL_LOW_CACHEINDEXVAL_BMSK                0x1f
#define HWIO_CNOC_RPM_M2_DYNATTR_CACHEINDEXVAL_LOW_CACHEINDEXVAL_SHFT                 0x0

#define HWIO_CONC_DCC_DYNATTR_SWID_LOW_ADDR                                    (CONFIG_NOC_REG_BASE      + 0x00007000)
#define HWIO_CONC_DCC_DYNATTR_SWID_LOW_RMSK                                      0xffffff
#define HWIO_CONC_DCC_DYNATTR_SWID_LOW_IN          \
        in_dword(HWIO_CONC_DCC_DYNATTR_SWID_LOW_ADDR)
#define HWIO_CONC_DCC_DYNATTR_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_CONC_DCC_DYNATTR_SWID_LOW_ADDR, m)
#define HWIO_CONC_DCC_DYNATTR_SWID_LOW_UNITTYPEID_BMSK                           0xff0000
#define HWIO_CONC_DCC_DYNATTR_SWID_LOW_UNITTYPEID_SHFT                               0x10
#define HWIO_CONC_DCC_DYNATTR_SWID_LOW_UNITCONFID_BMSK                             0xffff
#define HWIO_CONC_DCC_DYNATTR_SWID_LOW_UNITCONFID_SHFT                                0x0

#define HWIO_CONC_DCC_DYNATTR_SWID_HIGH_ADDR                                   (CONFIG_NOC_REG_BASE      + 0x00007004)
#define HWIO_CONC_DCC_DYNATTR_SWID_HIGH_RMSK                                   0xffffffff
#define HWIO_CONC_DCC_DYNATTR_SWID_HIGH_IN          \
        in_dword(HWIO_CONC_DCC_DYNATTR_SWID_HIGH_ADDR)
#define HWIO_CONC_DCC_DYNATTR_SWID_HIGH_INM(m)      \
        in_dword_masked(HWIO_CONC_DCC_DYNATTR_SWID_HIGH_ADDR, m)
#define HWIO_CONC_DCC_DYNATTR_SWID_HIGH_QNOCID_BMSK                            0xffffffff
#define HWIO_CONC_DCC_DYNATTR_SWID_HIGH_QNOCID_SHFT                                   0x0

#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_ADDR                                 (CONFIG_NOC_REG_BASE      + 0x00007008)
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_RMSK                                   0x1fffff
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_IN          \
        in_dword(HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_ADDR)
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_INM(m)      \
        in_dword_masked(HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_ADDR, m)
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_OUT(v)      \
        out_dword(HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_ADDR,v)
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_ADDR,m,v,HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_IN)
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_CACHEINDEXOV_BMSK                      0x100000
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_CACHEINDEXOV_SHFT                          0x14
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_RSV3_BMSK                               0x80000
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_RSV3_SHFT                                  0x13
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_ALLOCCTLOV_BMSK                         0x40000
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_ALLOCCTLOV_SHFT                            0x12
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_ALLOCWROV_BMSK                          0x20000
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_ALLOCWROV_SHFT                             0x11
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_ALLOCRDOV_BMSK                          0x10000
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_ALLOCRDOV_SHFT                             0x10
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_CMFPWRVAL_BMSK                           0x8000
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_CMFPWRVAL_SHFT                              0xf
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_CMFPWROV_BMSK                            0x4000
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_CMFPWROV_SHFT                               0xe
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_CMFPRDVAL_BMSK                           0x2000
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_CMFPRDVAL_SHFT                              0xd
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_CMFPRDOV_BMSK                            0x1000
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_CMFPRDOV_SHFT                               0xc
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_RSV2_BMSK                                 0xc00
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_RSV2_SHFT                                   0xa
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_DEVICEVAL_BMSK                            0x200
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_DEVICEVAL_SHFT                              0x9
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_DEVICEOV_BMSK                             0x100
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_DEVICEOV_SHFT                               0x8
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_RSV1_BMSK                                  0xe0
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_RSV1_SHFT                                   0x5
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_QOSDISABLE_BMSK                            0x10
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_QOSDISABLE_SHFT                             0x4
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_RSV0_BMSK                                   0xc
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_RSV0_SHFT                                   0x2
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_MODE_BMSK                                   0x3
#define HWIO_CONC_DCC_DYNATTR_MAINCTL_LOW_MODE_SHFT                                   0x0

#define HWIO_CONC_DCC_DYNATTR_MAINSTATUS_LOW_ADDR                              (CONFIG_NOC_REG_BASE      + 0x00007010)
#define HWIO_CONC_DCC_DYNATTR_MAINSTATUS_LOW_RMSK                                     0x3
#define HWIO_CONC_DCC_DYNATTR_MAINSTATUS_LOW_IN          \
        in_dword(HWIO_CONC_DCC_DYNATTR_MAINSTATUS_LOW_ADDR)
#define HWIO_CONC_DCC_DYNATTR_MAINSTATUS_LOW_INM(m)      \
        in_dword_masked(HWIO_CONC_DCC_DYNATTR_MAINSTATUS_LOW_ADDR, m)
#define HWIO_CONC_DCC_DYNATTR_MAINSTATUS_LOW_URGPENDING_BMSK                          0x2
#define HWIO_CONC_DCC_DYNATTR_MAINSTATUS_LOW_URGPENDING_SHFT                          0x1
#define HWIO_CONC_DCC_DYNATTR_MAINSTATUS_LOW_TRPENDING_BMSK                           0x1
#define HWIO_CONC_DCC_DYNATTR_MAINSTATUS_LOW_TRPENDING_SHFT                           0x0

#define HWIO_CONC_DCC_DYNATTR_REQUSEROV_LOW_ADDR                               (CONFIG_NOC_REG_BASE      + 0x00007018)
#define HWIO_CONC_DCC_DYNATTR_REQUSEROV_LOW_RMSK                                      0x1
#define HWIO_CONC_DCC_DYNATTR_REQUSEROV_LOW_IN          \
        in_dword(HWIO_CONC_DCC_DYNATTR_REQUSEROV_LOW_ADDR)
#define HWIO_CONC_DCC_DYNATTR_REQUSEROV_LOW_INM(m)      \
        in_dword_masked(HWIO_CONC_DCC_DYNATTR_REQUSEROV_LOW_ADDR, m)
#define HWIO_CONC_DCC_DYNATTR_REQUSEROV_LOW_OUT(v)      \
        out_dword(HWIO_CONC_DCC_DYNATTR_REQUSEROV_LOW_ADDR,v)
#define HWIO_CONC_DCC_DYNATTR_REQUSEROV_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CONC_DCC_DYNATTR_REQUSEROV_LOW_ADDR,m,v,HWIO_CONC_DCC_DYNATTR_REQUSEROV_LOW_IN)
#define HWIO_CONC_DCC_DYNATTR_REQUSEROV_LOW_REQUSEROV_BMSK                            0x1
#define HWIO_CONC_DCC_DYNATTR_REQUSEROV_LOW_REQUSEROV_SHFT                            0x0

#define HWIO_CONC_DCC_DYNATTR_REQUSERVAL_LOW_ADDR                              (CONFIG_NOC_REG_BASE      + 0x00007020)
#define HWIO_CONC_DCC_DYNATTR_REQUSERVAL_LOW_RMSK                                     0x1
#define HWIO_CONC_DCC_DYNATTR_REQUSERVAL_LOW_IN          \
        in_dword(HWIO_CONC_DCC_DYNATTR_REQUSERVAL_LOW_ADDR)
#define HWIO_CONC_DCC_DYNATTR_REQUSERVAL_LOW_INM(m)      \
        in_dword_masked(HWIO_CONC_DCC_DYNATTR_REQUSERVAL_LOW_ADDR, m)
#define HWIO_CONC_DCC_DYNATTR_REQUSERVAL_LOW_OUT(v)      \
        out_dword(HWIO_CONC_DCC_DYNATTR_REQUSERVAL_LOW_ADDR,v)
#define HWIO_CONC_DCC_DYNATTR_REQUSERVAL_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CONC_DCC_DYNATTR_REQUSERVAL_LOW_ADDR,m,v,HWIO_CONC_DCC_DYNATTR_REQUSERVAL_LOW_IN)
#define HWIO_CONC_DCC_DYNATTR_REQUSERVAL_LOW_REQUSERVAL_BMSK                          0x1
#define HWIO_CONC_DCC_DYNATTR_REQUSERVAL_LOW_REQUSERVAL_SHFT                          0x0

#define HWIO_CONC_DCC_DYNATTR_LOGUSEROV_LOW_ADDR                               (CONFIG_NOC_REG_BASE      + 0x00007028)
#define HWIO_CONC_DCC_DYNATTR_LOGUSEROV_LOW_RMSK                                  0x7ffff
#define HWIO_CONC_DCC_DYNATTR_LOGUSEROV_LOW_IN          \
        in_dword(HWIO_CONC_DCC_DYNATTR_LOGUSEROV_LOW_ADDR)
#define HWIO_CONC_DCC_DYNATTR_LOGUSEROV_LOW_INM(m)      \
        in_dword_masked(HWIO_CONC_DCC_DYNATTR_LOGUSEROV_LOW_ADDR, m)
#define HWIO_CONC_DCC_DYNATTR_LOGUSEROV_LOW_OUT(v)      \
        out_dword(HWIO_CONC_DCC_DYNATTR_LOGUSEROV_LOW_ADDR,v)
#define HWIO_CONC_DCC_DYNATTR_LOGUSEROV_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CONC_DCC_DYNATTR_LOGUSEROV_LOW_ADDR,m,v,HWIO_CONC_DCC_DYNATTR_LOGUSEROV_LOW_IN)
#define HWIO_CONC_DCC_DYNATTR_LOGUSEROV_LOW_LOGUSEROV_BMSK                        0x7ffff
#define HWIO_CONC_DCC_DYNATTR_LOGUSEROV_LOW_LOGUSEROV_SHFT                            0x0

#define HWIO_CONC_DCC_DYNATTR_LOGUSERVAL_LOW_ADDR                              (CONFIG_NOC_REG_BASE      + 0x00007030)
#define HWIO_CONC_DCC_DYNATTR_LOGUSERVAL_LOW_RMSK                                 0x7ffff
#define HWIO_CONC_DCC_DYNATTR_LOGUSERVAL_LOW_IN          \
        in_dword(HWIO_CONC_DCC_DYNATTR_LOGUSERVAL_LOW_ADDR)
#define HWIO_CONC_DCC_DYNATTR_LOGUSERVAL_LOW_INM(m)      \
        in_dword_masked(HWIO_CONC_DCC_DYNATTR_LOGUSERVAL_LOW_ADDR, m)
#define HWIO_CONC_DCC_DYNATTR_LOGUSERVAL_LOW_OUT(v)      \
        out_dword(HWIO_CONC_DCC_DYNATTR_LOGUSERVAL_LOW_ADDR,v)
#define HWIO_CONC_DCC_DYNATTR_LOGUSERVAL_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CONC_DCC_DYNATTR_LOGUSERVAL_LOW_ADDR,m,v,HWIO_CONC_DCC_DYNATTR_LOGUSERVAL_LOW_IN)
#define HWIO_CONC_DCC_DYNATTR_LOGUSERVAL_LOW_LOGUSERVAL_BMSK                      0x7ffff
#define HWIO_CONC_DCC_DYNATTR_LOGUSERVAL_LOW_LOGUSERVAL_SHFT                          0x0

#define HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_ADDR                                   (CONFIG_NOC_REG_BASE      + 0x00007048)
#define HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_RMSK                                      0xfffff
#define HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_IN          \
        in_dword(HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_ADDR)
#define HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_INM(m)      \
        in_dword_masked(HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_ADDR, m)
#define HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_OUT(v)      \
        out_dword(HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_ADDR,v)
#define HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_ADDR,m,v,HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_IN)
#define HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_CTLVAL_BMSK                               0xf0000
#define HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_CTLVAL_SHFT                                  0x10
#define HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_RSV1_BMSK                                  0xf000
#define HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_RSV1_SHFT                                     0xc
#define HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_WRVAL_BMSK                                  0xf00
#define HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_WRVAL_SHFT                                    0x8
#define HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_RSV0_BMSK                                    0xf0
#define HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_RSV0_SHFT                                     0x4
#define HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_RDVAL_BMSK                                    0xf
#define HWIO_CONC_DCC_DYNATTR_ALLOC_LOW_RDVAL_SHFT                                    0x0

#define HWIO_CONC_DCC_DYNATTR_CACHEINDEXVAL_LOW_ADDR                           (CONFIG_NOC_REG_BASE      + 0x00007050)
#define HWIO_CONC_DCC_DYNATTR_CACHEINDEXVAL_LOW_RMSK                                 0x1f
#define HWIO_CONC_DCC_DYNATTR_CACHEINDEXVAL_LOW_IN          \
        in_dword(HWIO_CONC_DCC_DYNATTR_CACHEINDEXVAL_LOW_ADDR)
#define HWIO_CONC_DCC_DYNATTR_CACHEINDEXVAL_LOW_INM(m)      \
        in_dword_masked(HWIO_CONC_DCC_DYNATTR_CACHEINDEXVAL_LOW_ADDR, m)
#define HWIO_CONC_DCC_DYNATTR_CACHEINDEXVAL_LOW_OUT(v)      \
        out_dword(HWIO_CONC_DCC_DYNATTR_CACHEINDEXVAL_LOW_ADDR,v)
#define HWIO_CONC_DCC_DYNATTR_CACHEINDEXVAL_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CONC_DCC_DYNATTR_CACHEINDEXVAL_LOW_ADDR,m,v,HWIO_CONC_DCC_DYNATTR_CACHEINDEXVAL_LOW_IN)
#define HWIO_CONC_DCC_DYNATTR_CACHEINDEXVAL_LOW_CACHEINDEXVAL_BMSK                   0x1f
#define HWIO_CONC_DCC_DYNATTR_CACHEINDEXVAL_LOW_CACHEINDEXVAL_SHFT                    0x0

#define HWIO_CNOC_DISABLE_SBM_SWID_LOW_ADDR                                    (CONFIG_NOC_REG_BASE      + 0x00008000)
#define HWIO_CNOC_DISABLE_SBM_SWID_LOW_RMSK                                      0xffffff
#define HWIO_CNOC_DISABLE_SBM_SWID_LOW_IN          \
        in_dword(HWIO_CNOC_DISABLE_SBM_SWID_LOW_ADDR)
#define HWIO_CNOC_DISABLE_SBM_SWID_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_DISABLE_SBM_SWID_LOW_ADDR, m)
#define HWIO_CNOC_DISABLE_SBM_SWID_LOW_UNITTYPEID_BMSK                           0xff0000
#define HWIO_CNOC_DISABLE_SBM_SWID_LOW_UNITTYPEID_SHFT                               0x10
#define HWIO_CNOC_DISABLE_SBM_SWID_LOW_UNITCONFID_BMSK                             0xffff
#define HWIO_CNOC_DISABLE_SBM_SWID_LOW_UNITCONFID_SHFT                                0x0

#define HWIO_CNOC_DISABLE_SBM_SWID_HIGH_ADDR                                   (CONFIG_NOC_REG_BASE      + 0x00008004)
#define HWIO_CNOC_DISABLE_SBM_SWID_HIGH_RMSK                                   0xffffffff
#define HWIO_CNOC_DISABLE_SBM_SWID_HIGH_IN          \
        in_dword(HWIO_CNOC_DISABLE_SBM_SWID_HIGH_ADDR)
#define HWIO_CNOC_DISABLE_SBM_SWID_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_DISABLE_SBM_SWID_HIGH_ADDR, m)
#define HWIO_CNOC_DISABLE_SBM_SWID_HIGH_QNOCID_BMSK                            0xffffffff
#define HWIO_CNOC_DISABLE_SBM_SWID_HIGH_QNOCID_SHFT                                   0x0

#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_ADDR                             (CONFIG_NOC_REG_BASE      + 0x00008080)
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_RMSK                             0xffffffff
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_ADDR,v)
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT31_BMSK                      0x80000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT31_SHFT                            0x1f
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT30_BMSK                      0x40000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT30_SHFT                            0x1e
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT29_BMSK                      0x20000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT29_SHFT                            0x1d
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT28_BMSK                      0x10000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT28_SHFT                            0x1c
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT27_BMSK                       0x8000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT27_SHFT                            0x1b
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT26_BMSK                       0x4000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT26_SHFT                            0x1a
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT25_BMSK                       0x2000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT25_SHFT                            0x19
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT24_BMSK                       0x1000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT24_SHFT                            0x18
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT23_BMSK                        0x800000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT23_SHFT                            0x17
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT22_BMSK                        0x400000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT22_SHFT                            0x16
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT21_BMSK                        0x200000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT21_SHFT                            0x15
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT20_BMSK                        0x100000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT20_SHFT                            0x14
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT19_BMSK                         0x80000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT19_SHFT                            0x13
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT18_BMSK                         0x40000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT18_SHFT                            0x12
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT17_BMSK                         0x20000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT17_SHFT                            0x11
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT16_BMSK                         0x10000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT16_SHFT                            0x10
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT15_BMSK                          0x8000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT15_SHFT                             0xf
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT14_BMSK                          0x4000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT14_SHFT                             0xe
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT13_BMSK                          0x2000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT13_SHFT                             0xd
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT12_BMSK                          0x1000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT12_SHFT                             0xc
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT11_BMSK                           0x800
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT11_SHFT                             0xb
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT10_BMSK                           0x400
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT10_SHFT                             0xa
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT9_BMSK                            0x200
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT9_SHFT                              0x9
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT8_BMSK                            0x100
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT8_SHFT                              0x8
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT7_BMSK                             0x80
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT7_SHFT                              0x7
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT6_BMSK                             0x40
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT6_SHFT                              0x6
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT5_BMSK                             0x20
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT5_SHFT                              0x5
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT4_BMSK                             0x10
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT4_SHFT                              0x4
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT3_BMSK                              0x8
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT3_SHFT                              0x3
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT2_BMSK                              0x4
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT2_SHFT                              0x2
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT1_BMSK                              0x2
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT1_SHFT                              0x1
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT0_BMSK                              0x1
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_LOW_PORT0_SHFT                              0x0

#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_ADDR                            (CONFIG_NOC_REG_BASE      + 0x00008084)
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_RMSK                              0x7fffff
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_OUT(v)      \
        out_dword(HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_ADDR,v)
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT54_BMSK                       0x400000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT54_SHFT                           0x16
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT53_BMSK                       0x200000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT53_SHFT                           0x15
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT52_BMSK                       0x100000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT52_SHFT                           0x14
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT51_BMSK                        0x80000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT51_SHFT                           0x13
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT50_BMSK                        0x40000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT50_SHFT                           0x12
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT49_BMSK                        0x20000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT49_SHFT                           0x11
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT48_BMSK                        0x10000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT48_SHFT                           0x10
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT47_BMSK                         0x8000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT47_SHFT                            0xf
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT46_BMSK                         0x4000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT46_SHFT                            0xe
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT45_BMSK                         0x2000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT45_SHFT                            0xd
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT44_BMSK                         0x1000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT44_SHFT                            0xc
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT43_BMSK                          0x800
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT43_SHFT                            0xb
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT42_BMSK                          0x400
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT42_SHFT                            0xa
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT41_BMSK                          0x200
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT41_SHFT                            0x9
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT40_BMSK                          0x100
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT40_SHFT                            0x8
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT39_BMSK                           0x80
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT39_SHFT                            0x7
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT38_BMSK                           0x40
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT38_SHFT                            0x6
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT37_BMSK                           0x20
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT37_SHFT                            0x5
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT36_BMSK                           0x10
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT36_SHFT                            0x4
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT35_BMSK                            0x8
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT35_SHFT                            0x3
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT34_BMSK                            0x4
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT34_SHFT                            0x2
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT33_BMSK                            0x2
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT33_SHFT                            0x1
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT32_BMSK                            0x1
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTCLR0_HIGH_PORT32_SHFT                            0x0

#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_ADDR                             (CONFIG_NOC_REG_BASE      + 0x00008088)
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_RMSK                             0xffffffff
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_OUT(v)      \
        out_dword(HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_ADDR,v)
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT31_BMSK                      0x80000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT31_SHFT                            0x1f
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT30_BMSK                      0x40000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT30_SHFT                            0x1e
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT29_BMSK                      0x20000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT29_SHFT                            0x1d
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT28_BMSK                      0x10000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT28_SHFT                            0x1c
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT27_BMSK                       0x8000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT27_SHFT                            0x1b
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT26_BMSK                       0x4000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT26_SHFT                            0x1a
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT25_BMSK                       0x2000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT25_SHFT                            0x19
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT24_BMSK                       0x1000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT24_SHFT                            0x18
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT23_BMSK                        0x800000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT23_SHFT                            0x17
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT22_BMSK                        0x400000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT22_SHFT                            0x16
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT21_BMSK                        0x200000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT21_SHFT                            0x15
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT20_BMSK                        0x100000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT20_SHFT                            0x14
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT19_BMSK                         0x80000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT19_SHFT                            0x13
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT18_BMSK                         0x40000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT18_SHFT                            0x12
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT17_BMSK                         0x20000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT17_SHFT                            0x11
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT16_BMSK                         0x10000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT16_SHFT                            0x10
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT15_BMSK                          0x8000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT15_SHFT                             0xf
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT14_BMSK                          0x4000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT14_SHFT                             0xe
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT13_BMSK                          0x2000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT13_SHFT                             0xd
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT12_BMSK                          0x1000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT12_SHFT                             0xc
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT11_BMSK                           0x800
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT11_SHFT                             0xb
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT10_BMSK                           0x400
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT10_SHFT                             0xa
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT9_BMSK                            0x200
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT9_SHFT                              0x9
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT8_BMSK                            0x100
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT8_SHFT                              0x8
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT7_BMSK                             0x80
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT7_SHFT                              0x7
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT6_BMSK                             0x40
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT6_SHFT                              0x6
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT5_BMSK                             0x20
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT5_SHFT                              0x5
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT4_BMSK                             0x10
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT4_SHFT                              0x4
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT3_BMSK                              0x8
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT3_SHFT                              0x3
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT2_BMSK                              0x4
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT2_SHFT                              0x2
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT1_BMSK                              0x2
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT1_SHFT                              0x1
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT0_BMSK                              0x1
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_LOW_PORT0_SHFT                              0x0

#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_ADDR                            (CONFIG_NOC_REG_BASE      + 0x0000808c)
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_RMSK                              0x7fffff
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_OUT(v)      \
        out_dword(HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_ADDR,v)
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT54_BMSK                       0x400000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT54_SHFT                           0x16
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT53_BMSK                       0x200000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT53_SHFT                           0x15
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT52_BMSK                       0x100000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT52_SHFT                           0x14
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT51_BMSK                        0x80000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT51_SHFT                           0x13
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT50_BMSK                        0x40000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT50_SHFT                           0x12
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT49_BMSK                        0x20000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT49_SHFT                           0x11
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT48_BMSK                        0x10000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT48_SHFT                           0x10
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT47_BMSK                         0x8000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT47_SHFT                            0xf
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT46_BMSK                         0x4000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT46_SHFT                            0xe
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT45_BMSK                         0x2000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT45_SHFT                            0xd
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT44_BMSK                         0x1000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT44_SHFT                            0xc
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT43_BMSK                          0x800
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT43_SHFT                            0xb
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT42_BMSK                          0x400
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT42_SHFT                            0xa
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT41_BMSK                          0x200
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT41_SHFT                            0x9
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT40_BMSK                          0x100
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT40_SHFT                            0x8
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT39_BMSK                           0x80
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT39_SHFT                            0x7
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT38_BMSK                           0x40
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT38_SHFT                            0x6
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT37_BMSK                           0x20
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT37_SHFT                            0x5
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT36_BMSK                           0x10
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT36_SHFT                            0x4
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT35_BMSK                            0x8
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT35_SHFT                            0x3
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT34_BMSK                            0x4
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT34_SHFT                            0x2
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT33_BMSK                            0x2
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT33_SHFT                            0x1
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT32_BMSK                            0x1
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSET0_HIGH_PORT32_SHFT                            0x0

#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_ADDR                          (CONFIG_NOC_REG_BASE      + 0x00008090)
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_RMSK                          0xffffffff
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_IN          \
        in_dword(HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_ADDR)
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_ADDR, m)
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT31_BMSK                   0x80000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT31_SHFT                         0x1f
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT30_BMSK                   0x40000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT30_SHFT                         0x1e
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT29_BMSK                   0x20000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT29_SHFT                         0x1d
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT28_BMSK                   0x10000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT28_SHFT                         0x1c
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT27_BMSK                    0x8000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT27_SHFT                         0x1b
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT26_BMSK                    0x4000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT26_SHFT                         0x1a
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT25_BMSK                    0x2000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT25_SHFT                         0x19
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT24_BMSK                    0x1000000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT24_SHFT                         0x18
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT23_BMSK                     0x800000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT23_SHFT                         0x17
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT22_BMSK                     0x400000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT22_SHFT                         0x16
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT21_BMSK                     0x200000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT21_SHFT                         0x15
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT20_BMSK                     0x100000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT20_SHFT                         0x14
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT19_BMSK                      0x80000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT19_SHFT                         0x13
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT18_BMSK                      0x40000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT18_SHFT                         0x12
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT17_BMSK                      0x20000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT17_SHFT                         0x11
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT16_BMSK                      0x10000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT16_SHFT                         0x10
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT15_BMSK                       0x8000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT15_SHFT                          0xf
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT14_BMSK                       0x4000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT14_SHFT                          0xe
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT13_BMSK                       0x2000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT13_SHFT                          0xd
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT12_BMSK                       0x1000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT12_SHFT                          0xc
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT11_BMSK                        0x800
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT11_SHFT                          0xb
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT10_BMSK                        0x400
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT10_SHFT                          0xa
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT9_BMSK                         0x200
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT9_SHFT                           0x9
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT8_BMSK                         0x100
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT8_SHFT                           0x8
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT7_BMSK                          0x80
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT7_SHFT                           0x7
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT6_BMSK                          0x40
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT6_SHFT                           0x6
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT5_BMSK                          0x20
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT5_SHFT                           0x5
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT4_BMSK                          0x10
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT4_SHFT                           0x4
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT3_BMSK                           0x8
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT3_SHFT                           0x3
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT2_BMSK                           0x4
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT2_SHFT                           0x2
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT1_BMSK                           0x2
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT1_SHFT                           0x1
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT0_BMSK                           0x1
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_LOW_PORT0_SHFT                           0x0

#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_ADDR                         (CONFIG_NOC_REG_BASE      + 0x00008094)
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_RMSK                           0x7fffff
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_IN          \
        in_dword(HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_ADDR)
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_ADDR, m)
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT54_BMSK                    0x400000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT54_SHFT                        0x16
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT53_BMSK                    0x200000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT53_SHFT                        0x15
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT52_BMSK                    0x100000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT52_SHFT                        0x14
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT51_BMSK                     0x80000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT51_SHFT                        0x13
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT50_BMSK                     0x40000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT50_SHFT                        0x12
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT49_BMSK                     0x20000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT49_SHFT                        0x11
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT48_BMSK                     0x10000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT48_SHFT                        0x10
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT47_BMSK                      0x8000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT47_SHFT                         0xf
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT46_BMSK                      0x4000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT46_SHFT                         0xe
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT45_BMSK                      0x2000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT45_SHFT                         0xd
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT44_BMSK                      0x1000
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT44_SHFT                         0xc
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT43_BMSK                       0x800
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT43_SHFT                         0xb
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT42_BMSK                       0x400
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT42_SHFT                         0xa
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT41_BMSK                       0x200
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT41_SHFT                         0x9
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT40_BMSK                       0x100
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT40_SHFT                         0x8
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT39_BMSK                        0x80
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT39_SHFT                         0x7
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT38_BMSK                        0x40
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT38_SHFT                         0x6
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT37_BMSK                        0x20
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT37_SHFT                         0x5
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT36_BMSK                        0x10
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT36_SHFT                         0x4
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT35_BMSK                         0x8
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT35_SHFT                         0x3
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT34_BMSK                         0x4
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT34_SHFT                         0x2
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT33_BMSK                         0x2
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT33_SHFT                         0x1
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT32_BMSK                         0x1
#define HWIO_CNOC_DISABLE_SBM_FLAGOUTSTATUS0_HIGH_PORT32_SHFT                         0x0

#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_ADDR                                (CONFIG_NOC_REG_BASE      + 0x00008100)
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_RMSK                                0xffffffff
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_IN          \
        in_dword(HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_ADDR)
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_INM(m)      \
        in_dword_masked(HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_ADDR, m)
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT31_BMSK                         0x80000000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT31_SHFT                               0x1f
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT30_BMSK                         0x40000000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT30_SHFT                               0x1e
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT29_BMSK                         0x20000000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT29_SHFT                               0x1d
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT28_BMSK                         0x10000000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT28_SHFT                               0x1c
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT27_BMSK                          0x8000000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT27_SHFT                               0x1b
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT26_BMSK                          0x4000000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT26_SHFT                               0x1a
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT25_BMSK                          0x2000000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT25_SHFT                               0x19
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT24_BMSK                          0x1000000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT24_SHFT                               0x18
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT23_BMSK                           0x800000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT23_SHFT                               0x17
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT22_BMSK                           0x400000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT22_SHFT                               0x16
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT21_BMSK                           0x200000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT21_SHFT                               0x15
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT20_BMSK                           0x100000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT20_SHFT                               0x14
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT19_BMSK                            0x80000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT19_SHFT                               0x13
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT18_BMSK                            0x40000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT18_SHFT                               0x12
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT17_BMSK                            0x20000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT17_SHFT                               0x11
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT16_BMSK                            0x10000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT16_SHFT                               0x10
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT15_BMSK                             0x8000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT15_SHFT                                0xf
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT14_BMSK                             0x4000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT14_SHFT                                0xe
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT13_BMSK                             0x2000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT13_SHFT                                0xd
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT12_BMSK                             0x1000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT12_SHFT                                0xc
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT11_BMSK                              0x800
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT11_SHFT                                0xb
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT10_BMSK                              0x400
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT10_SHFT                                0xa
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT9_BMSK                               0x200
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT9_SHFT                                 0x9
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT8_BMSK                               0x100
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT8_SHFT                                 0x8
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT7_BMSK                                0x80
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT7_SHFT                                 0x7
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT6_BMSK                                0x40
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT6_SHFT                                 0x6
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT5_BMSK                                0x20
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT5_SHFT                                 0x5
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT4_BMSK                                0x10
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT4_SHFT                                 0x4
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT3_BMSK                                 0x8
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT3_SHFT                                 0x3
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT2_BMSK                                 0x4
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT2_SHFT                                 0x2
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT1_BMSK                                 0x2
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT1_SHFT                                 0x1
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT0_BMSK                                 0x1
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_LOW_PORT0_SHFT                                 0x0

#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_ADDR                               (CONFIG_NOC_REG_BASE      + 0x00008104)
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_RMSK                                 0x7fffff
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_IN          \
        in_dword(HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_ADDR)
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_INM(m)      \
        in_dword_masked(HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_ADDR, m)
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT54_BMSK                          0x400000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT54_SHFT                              0x16
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT53_BMSK                          0x200000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT53_SHFT                              0x15
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT52_BMSK                          0x100000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT52_SHFT                              0x14
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT51_BMSK                           0x80000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT51_SHFT                              0x13
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT50_BMSK                           0x40000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT50_SHFT                              0x12
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT49_BMSK                           0x20000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT49_SHFT                              0x11
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT48_BMSK                           0x10000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT48_SHFT                              0x10
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT47_BMSK                            0x8000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT47_SHFT                               0xf
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT46_BMSK                            0x4000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT46_SHFT                               0xe
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT45_BMSK                            0x2000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT45_SHFT                               0xd
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT44_BMSK                            0x1000
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT44_SHFT                               0xc
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT43_BMSK                             0x800
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT43_SHFT                               0xb
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT42_BMSK                             0x400
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT42_SHFT                               0xa
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT41_BMSK                             0x200
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT41_SHFT                               0x9
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT40_BMSK                             0x100
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT40_SHFT                               0x8
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT39_BMSK                              0x80
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT39_SHFT                               0x7
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT38_BMSK                              0x40
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT38_SHFT                               0x6
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT37_BMSK                              0x20
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT37_SHFT                               0x5
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT36_BMSK                              0x10
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT36_SHFT                               0x4
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT35_BMSK                               0x8
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT35_SHFT                               0x3
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT34_BMSK                               0x4
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT34_SHFT                               0x2
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT33_BMSK                               0x2
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT33_SHFT                               0x1
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT32_BMSK                               0x1
#define HWIO_CNOC_DISABLE_SBM_SENSEIN0_HIGH_PORT32_SHFT                               0x0

/*----------------------------------------------------------------------------
 * MODULE: APCS_CCI_GLADIATOR_NOC
 *--------------------------------------------------------------------------*/

#define APCS_CCI_GLADIATOR_NOC_REG_BASE                                                                        (A53SS_BASE      + 0x00100000)
#define APCS_CCI_GLADIATOR_NOC_REG_BASE_SIZE                                                                   0xdc00
#define APCS_CCI_GLADIATOR_NOC_REG_BASE_USED                                                                   0xd8b0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_COREID_ADDR                                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00000000)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_COREID_RMSK                                                     0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_COREID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_COREID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_COREID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_COREID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_COREID_CORECHECKSUM_BMSK                                        0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_COREID_CORECHECKSUM_SHFT                                               0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_COREID_CORETYPEID_BMSK                                                0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_COREID_CORETYPEID_SHFT                                                 0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_REVISIONID_ADDR                                                 (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00000004)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_REVISIONID_RMSK                                                 0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_REVISIONID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_REVISIONID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_REVISIONID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_REVISIONID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_REVISIONID_QNOCID_BMSK                                          0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_REVISIONID_QNOCID_SHFT                                                 0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_REVISIONID_USERID_BMSK                                                0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ID_REVISIONID_USERID_SHFT                                                 0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_ADDR                                                         (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00000008)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_RMSK                                                               0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_ENPCIRDFWD_BMSK                                                    0x80
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_ENPCIRDFWD_SHFT                                                     0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_NOTUSED_BMSK                                                       0x40
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_NOTUSED_SHFT                                                        0x6
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_FORCENOFULLWR_BMSK                                                 0x20
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_FORCENOFULLWR_SHFT                                                  0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_FORCEFULLWR_BMSK                                                   0x10
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_FORCEFULLWR_SHFT                                                    0x4
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_FORCENOWRMERGE_BMSK                                                 0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_FORCENOWRMERGE_SHFT                                                 0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_FORCENORDFWD_BMSK                                                   0x4
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_FORCENORDFWD_SHFT                                                   0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_FORCEMINSNPDATA_BMSK                                                0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_FORCEMINSNPDATA_SHFT                                                0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_ENSNOOP_BMSK                                                        0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGIO_ENSNOOP_SHFT                                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGPCI_ADDR                                                        (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000000c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGPCI_RMSK                                                        0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGPCI_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGPCI_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGPCI_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGPCI_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGPCI_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGPCI_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGPCI_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGPCI_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGPCI_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGPCI_PCISRCIDMASK_BMSK                                           0xffff0000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGPCI_PCISRCIDMASK_SHFT                                                 0x10
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGPCI_PCISRCID_BMSK                                                   0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGPCI_PCISRCID_SHFT                                                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_ADDR                                                        (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00000078)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_RMSK                                                           0x3ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_STALLSNONEVFIFOFULL_BMSK                                       0x20000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_STALLSNONEVFIFOFULL_SHFT                                          0x11
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_DISDIRONPARERR_BMSK                                            0x10000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_DISDIRONPARERR_SHFT                                               0x10
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_OPTEXTDVM_BMSK                                                  0x8000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_OPTEXTDVM_SHFT                                                     0xf
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_DISEXTDVM_BMSK                                                  0x4000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_DISEXTDVM_SHFT                                                     0xe
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_ENDICONNERRLOG_BMSK                                             0x2000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_ENDICONNERRLOG_SHFT                                                0xd
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_ENACCONNERRLOG_BMSK                                             0x1000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_ENACCONNERRLOG_SHFT                                                0xc
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_ENPARRERRLOG_BMSK                                                0x800
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_ENPARRERRLOG_SHFT                                                  0xb
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_CULOCKFAILRATE_BMSK                                              0x600
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_CULOCKFAILRATE_SHFT                                                0x9
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_EXCLMODE_BMSK                                                    0x180
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_EXCLMODE_SHFT                                                      0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_NORDFWDSTNDFWD_BMSK                                               0x40
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_NORDFWDSTNDFWD_SHFT                                                0x6
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_SPETHR_BMSK                                                       0x3f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUT_SPETHR_SHFT                                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0NSNCTOMEM_ADDR                                                 (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00000080)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0NSNCTOMEM_RMSK                                                        0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0NSNCTOMEM_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0NSNCTOMEM_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0NSNCTOMEM_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0NSNCTOMEM_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0NSNCTOMEM_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0NSNCTOMEM_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0NSNCTOMEM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0NSNCTOMEM_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0NSNCTOMEM_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0NSNCTOMEM_ACE0NSNCTOMEM_BMSK                                          0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0NSNCTOMEM_ACE0NSNCTOMEM_SHFT                                          0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_ADDR                                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00000084)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_RMSK                                                             0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_RSTNEEDFLUSH_BMSK                                                0x10
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_RSTNEEDFLUSH_SHFT                                                 0x4
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_ENEWRALLOC_BMSK                                                   0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_ENEWRALLOC_SHFT                                                   0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_ENEWRBUF_BMSK                                                     0x4
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_ENEWRBUF_SHFT                                                     0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_CHICKENBIT_BMSK                                                   0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_CHICKENBIT_SHFT                                                   0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_ENSPERD_BMSK                                                      0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0CTL_ENSPERD_SHFT                                                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0STS_ADDR                                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00000088)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0STS_RMSK                                                             0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0STS_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0STS_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0STS_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0STS_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0STS_NEEDFLUSH_BMSK                                                   0x80
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0STS_NEEDFLUSH_SHFT                                                    0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0STS_DIRSTATUS_BMSK                                                   0x70
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0STS_DIRSTATUS_SHFT                                                    0x4
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0STS_DIPWRCTL_BMSK                                                     0xc
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0STS_DIPWRCTL_SHFT                                                     0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0STS_ACPWRCTL_BMSK                                                     0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE0STS_ACPWRCTL_SHFT                                                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1NSNCTOMEM_ADDR                                                 (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000000a0)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1NSNCTOMEM_RMSK                                                        0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1NSNCTOMEM_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1NSNCTOMEM_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1NSNCTOMEM_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1NSNCTOMEM_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1NSNCTOMEM_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1NSNCTOMEM_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1NSNCTOMEM_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1NSNCTOMEM_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1NSNCTOMEM_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1NSNCTOMEM_ACE1NSNCTOMEM_BMSK                                          0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1NSNCTOMEM_ACE1NSNCTOMEM_SHFT                                          0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_ADDR                                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000000a4)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_RMSK                                                             0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_RSTNEEDFLUSH_BMSK                                                0x10
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_RSTNEEDFLUSH_SHFT                                                 0x4
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_ENEWRALLOC_BMSK                                                   0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_ENEWRALLOC_SHFT                                                   0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_ENEWRBUF_BMSK                                                     0x4
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_ENEWRBUF_SHFT                                                     0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_CHICKENBIT_BMSK                                                   0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_CHICKENBIT_SHFT                                                   0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_ENSPERD_BMSK                                                      0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1CTL_ENSPERD_SHFT                                                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1STS_ADDR                                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000000a8)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1STS_RMSK                                                             0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1STS_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1STS_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1STS_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1STS_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1STS_NEEDFLUSH_BMSK                                                   0x80
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1STS_NEEDFLUSH_SHFT                                                    0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1STS_DIRSTATUS_BMSK                                                   0x70
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1STS_DIRSTATUS_SHFT                                                    0x4
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1STS_DIPWRCTL_BMSK                                                     0xc
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1STS_DIPWRCTL_SHFT                                                     0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1STS_ACPWRCTL_BMSK                                                     0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ACE1STS_ACPWRCTL_SHFT                                                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMPINFO_ADDR                                                      (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00001000)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMPINFO_RMSK                                                      0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMPINFO_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMPINFO_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMPINFO_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMPINFO_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMPINFO_STEP_BMSK                                                 0xff000000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMPINFO_STEP_SHFT                                                       0x18
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMPINFO_INFO_BMSK                                                   0xffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMPINFO_INFO_SHFT                                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_ADDR                                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00001004)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_RMSK                                                       0x133f0003
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_SELPOS_BMSK                                                0x10000000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_SELPOS_SHFT                                                      0x1c
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_SELTABLE_BMSK                                               0x3000000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_SELTABLE_SHFT                                                    0x18
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_INDEX_BMSK                                                   0x3f0000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_INDEX_SHFT                                                       0x10
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_NOTAUTO_BMSK                                                      0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_NOTAUTO_SHFT                                                      0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_DUMP_BMSK                                                         0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMP_DUMP_SHFT                                                         0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_ADDR                                                         (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00001008)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_RMSK                                                         0xfffeffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_SELECHO_BMSK                                                 0xff000000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_SELECHO_SHFT                                                       0x18
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_VALID_BMSK                                                     0x800000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_VALID_SHFT                                                         0x17
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_ISWR_BMSK                                                      0x400000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_ISWR_SHFT                                                          0x16
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_HASSPE_BMSK                                                    0x200000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_HASSPE_SHFT                                                        0x15
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_SPEINV_BMSK                                                    0x100000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_SPEINV_SHFT                                                        0x14
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_SPERSPKP_BMSK                                                   0x80000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_SPERSPKP_SHFT                                                      0x13
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_SPERSPDP_BMSK                                                   0x40000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_SPERSPDP_SHFT                                                      0x12
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_SNPDONE_BMSK                                                    0x20000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_SNPDONE_SHFT                                                       0x11
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_SELPOS_BMSK                                                      0x8000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_SELPOS_SHFT                                                         0xf
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_SRCIDX_BMSK                                                      0x7000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_SRCIDX_SHFT                                                         0xc
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_ADDRMSB_BMSK                                                      0xfff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP0_ADDRMSB_SHFT                                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP1_ADDR                                                         (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000100c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP1_RMSK                                                         0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP1_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP1_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP1_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP1_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP1_DUMP1_BMSK                                                   0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMP1_DUMP1_SHFT                                                          0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_FAULTEN_ADDR                                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00001010)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_FAULTEN_RMSK                                                              0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_FAULTEN_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_FAULTEN_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_FAULTEN_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_FAULTEN_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_FAULTEN_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_FAULTEN_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_FAULTEN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_FAULTEN_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_FAULTEN_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_FAULTEN_FAULTEN_BMSK                                                      0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_FAULTEN_FAULTEN_SHFT                                                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRVLD_ADDR                                                        (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00001014)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRVLD_RMSK                                                               0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRVLD_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRVLD_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRVLD_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRVLD_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRVLD_ERRVLD_BMSK                                                        0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRVLD_ERRVLD_SHFT                                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRCLR_ADDR                                                        (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00001018)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRCLR_RMSK                                                               0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRCLR_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRCLR_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRCLR_ERRCLR_BMSK                                                        0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRCLR_ERRCLR_SHFT                                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG0_ADDR                                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000101c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG0_RMSK                                                       0x8fff070f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG0_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG0_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG0_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG0_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG0_FORMAT_BMSK                                                0x80000000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG0_FORMAT_SHFT                                                      0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG0_LEN1_BMSK                                                   0xfff0000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG0_LEN1_SHFT                                                        0x10
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG0_ERRTYPE_BMSK                                                    0x700
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG0_ERRTYPE_SHFT                                                      0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG0_OPC_BMSK                                                          0xe
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG0_OPC_SHFT                                                          0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG0_LOCK_BMSK                                                         0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG0_LOCK_SHFT                                                         0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG1_ADDR                                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00001020)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG1_RMSK                                                              0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG1_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG1_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG1_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG1_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG1_ERRLOG1_BMSK                                                      0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG1_ERRLOG1_SHFT                                                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG2_ADDR                                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00001024)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG2_RMSK                                                              0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG2_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG2_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG2_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG2_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG2_ERRLOG2_BMSK                                                      0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG2_ERRLOG2_SHFT                                                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG3_ADDR                                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00001028)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG3_RMSK                                                       0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG3_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG3_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG3_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG3_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG3_ERRLOG3_BMSK                                               0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG3_ERRLOG3_SHFT                                                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG4_ADDR                                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000102c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG4_RMSK                                                       0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG4_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG4_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG4_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG4_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG4_ERRLOG4_BMSK                                               0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG4_ERRLOG4_SHFT                                                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG5_ADDR                                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00001030)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG5_RMSK                                                       0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG5_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG5_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG5_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG5_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG5_ERRLOG5_BMSK                                               0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG5_ERRLOG5_SHFT                                                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG6_ADDR                                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00001034)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG6_RMSK                                                       0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG6_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG6_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG6_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG6_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG6_ERRLOG6_BMSK                                               0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG6_ERRLOG6_SHFT                                                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG8_ADDR                                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000103c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG8_RMSK                                                       0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG8_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG8_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG8_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG8_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG8_ERRLOG8_BMSK                                               0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_ERRLOG8_ERRLOG8_SHFT                                                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_ADDR                                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00001040)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_RMSK                                                       0x1ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_ENDUMPDIR_BMSK                                             0x10000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_ENDUMPDIR_SHFT                                                0x10
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_SET_BMSK                                                    0x8000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_SET_SHFT                                                       0xf
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_PORT_BMSK                                                   0x6000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_PORT_SHFT                                                      0xd
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_BANK_BMSK                                                   0x1000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_BANK_SHFT                                                      0xc
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_INDEX_BMSK                                                   0xfff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_CFGDUMPDIR_INDEX_SHFT                                                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_STSDUMPDIR_ADDR                                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00001044)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_STSDUMPDIR_RMSK                                                      0x1fffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_STSDUMPDIR_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_STSDUMPDIR_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_STSDUMPDIR_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_STSDUMPDIR_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_STSDUMPDIR_WORD_BMSK                                                 0x1f0000
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_STSDUMPDIR_WORD_SHFT                                                     0x10
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_STSDUMPDIR_INDEX_BMSK                                                  0xfff0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_STSDUMPDIR_INDEX_SHFT                                                     0x4
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_STSDUMPDIR_BANK_BMSK                                                      0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_STSDUMPDIR_BANK_SHFT                                                      0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_STSDUMPDIR_RSV_BMSK                                                       0x4
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_STSDUMPDIR_RSV_SHFT                                                       0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_STSDUMPDIR_PORT_BMSK                                                      0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_STSDUMPDIR_PORT_SHFT                                                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMPDIR_ADDR                                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00001048)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMPDIR_RMSK                                                       0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMPDIR_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMPDIR_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMPDIR_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMPDIR_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMPDIR_DUMPDIR_BMSK                                               0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_CRIXUS_DUMPDIR_DUMPDIR_SHFT                                                      0x0

#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_COREID_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00004000)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_COREID_RMSK                                          0xffffffff
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_COREID_IN          \
        in_dword(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_COREID_ADDR)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_COREID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_COREID_ADDR, m)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_COREID_CORECHECKSUM_BMSK                             0xffffff00
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_COREID_CORECHECKSUM_SHFT                                    0x8
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_COREID_CORETYPEID_BMSK                                     0xff
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_COREID_CORETYPEID_SHFT                                      0x0

#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_REVISIONID_ADDR                                      (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00004004)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_REVISIONID_RMSK                                      0xffffffff
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_REVISIONID_IN          \
        in_dword(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_REVISIONID_ADDR)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_REVISIONID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_REVISIONID_ADDR, m)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_REVISIONID_QNOCID_BMSK                               0xffffff00
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_REVISIONID_QNOCID_SHFT                                      0x8
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_REVISIONID_USERID_BMSK                                     0xff
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_ID_REVISIONID_USERID_SHFT                                      0x0

#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET0_ADDR                                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00004008)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET0_RMSK                                                   0xf
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET0_IN          \
        in_dword(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET0_ADDR)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET0_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET0_ADDR, m)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET0_OUT(v)      \
        out_dword(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET0_ADDR,v)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET0_ADDR,m,v,HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET0_IN)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET0_ALTHWIDLEREQ_BMSK                                      0x8
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET0_ALTHWIDLEREQ_SHFT                                      0x3
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET0_ALTHWEN_BMSK                                           0x4
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET0_ALTHWEN_SHFT                                           0x2
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET0_IDLEACK_BMSK                                           0x2
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET0_IDLEACK_SHFT                                           0x1
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET0_IDLEREQ_BMSK                                           0x1
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET0_IDLEREQ_SHFT                                           0x0

#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET1_ADDR                                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000400c)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET1_RMSK                                                   0xf
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET1_IN          \
        in_dword(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET1_ADDR)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET1_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET1_ADDR, m)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET1_OUT(v)      \
        out_dword(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET1_ADDR,v)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET1_ADDR,m,v,HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET1_IN)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET1_ALTHWIDLEREQ_BMSK                                      0x8
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET1_ALTHWIDLEREQ_SHFT                                      0x3
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET1_ALTHWEN_BMSK                                           0x4
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET1_ALTHWEN_SHFT                                           0x2
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET1_IDLEACK_BMSK                                           0x2
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET1_IDLEACK_SHFT                                           0x1
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET1_IDLEREQ_BMSK                                           0x1
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET1_IDLEREQ_SHFT                                           0x0

#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET2_ADDR                                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00004010)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET2_RMSK                                                   0xf
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET2_IN          \
        in_dword(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET2_ADDR)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET2_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET2_ADDR, m)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET2_OUT(v)      \
        out_dword(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET2_ADDR,v)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET2_ADDR,m,v,HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET2_IN)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET2_ALTHWIDLEREQ_BMSK                                      0x8
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET2_ALTHWIDLEREQ_SHFT                                      0x3
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET2_ALTHWEN_BMSK                                           0x4
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET2_ALTHWEN_SHFT                                           0x2
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET2_IDLEACK_BMSK                                           0x2
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET2_IDLEACK_SHFT                                           0x1
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET2_IDLEREQ_BMSK                                           0x1
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET2_IDLEREQ_SHFT                                           0x0

#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET3_ADDR                                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00004014)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET3_RMSK                                                   0xf
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET3_IN          \
        in_dword(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET3_ADDR)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET3_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET3_ADDR, m)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET3_OUT(v)      \
        out_dword(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET3_ADDR,v)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET3_ADDR,m,v,HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET3_IN)
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET3_ALTHWIDLEREQ_BMSK                                      0x8
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET3_ALTHWIDLEREQ_SHFT                                      0x3
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET3_ALTHWEN_BMSK                                           0x4
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET3_ALTHWEN_SHFT                                           0x2
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET3_IDLEACK_BMSK                                           0x2
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET3_IDLEACK_SHFT                                           0x1
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET3_IDLEREQ_BMSK                                           0x1
#define HWIO_APCS_CCI_PD_GNOC_MAIN_PROGPOWERCONTROLLER_TARGET3_IDLEREQ_SHFT                                           0x0

#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_ADDR                                         (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00004100)
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_RMSK                                         0xffffffff
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_IN          \
        in_dword(HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_ADDR)
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_ADDR, m)
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORECHECKSUM_BMSK                            0xffffff00
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORECHECKSUM_SHFT                                   0x8
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORETYPEID_BMSK                                    0xff
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORETYPEID_SHFT                                     0x0

#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_ADDR                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00004104)
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_RMSK                                     0xffffffff
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_IN          \
        in_dword(HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_ADDR)
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_ADDR, m)
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_QNOCID_BMSK                              0xffffff00
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_QNOCID_SHFT                                     0x8
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_USERID_BMSK                                    0xff
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_USERID_SHFT                                     0x0

#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSET0_ADDR                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00004150)
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSET0_RMSK                                              0x7
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSET0_OUT(v)      \
        out_dword(HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSET0_ADDR,v)
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSET0_FLAGOUTSET0_BMSK                                  0x7
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSET0_FLAGOUTSET0_SHFT                                  0x0

#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTCLR0_ADDR                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00004154)
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTCLR0_RMSK                                              0x7
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTCLR0_OUT(v)      \
        out_dword(HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTCLR0_ADDR,v)
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTCLR0_FLAGOUTCLR0_BMSK                                  0x7
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTCLR0_FLAGOUTCLR0_SHFT                                  0x0

#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSTATUS0_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00004158)
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSTATUS0_RMSK                                           0x7
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSTATUS0_IN          \
        in_dword(HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSTATUS0_ADDR)
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSTATUS0_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSTATUS0_ADDR, m)
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSTATUS0_FLAGOUTSTATUS0_BMSK                            0x7
#define HWIO_APCS_CCI_NS_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSTATUS0_FLAGOUTSTATUS0_SHFT                            0x0

#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_ADDR                                 (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00004200)
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_RMSK                                 0xffffffff
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_IN          \
        in_dword(HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_ADDR)
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_ADDR, m)
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORECHECKSUM_BMSK                    0xffffff00
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORECHECKSUM_SHFT                           0x8
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORETYPEID_BMSK                            0xff
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORETYPEID_SHFT                             0x0

#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_ADDR                             (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00004204)
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_RMSK                             0xffffffff
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_IN          \
        in_dword(HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_ADDR)
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_ADDR, m)
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_QNOCID_BMSK                      0xffffff00
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_QNOCID_SHFT                             0x8
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_USERID_BMSK                            0xff
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_USERID_SHFT                             0x0

#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSET0_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00004250)
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSET0_RMSK                                      0x1
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSET0_OUT(v)      \
        out_dword(HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSET0_ADDR,v)
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSET0_FLAGOUTSET0_BMSK                          0x1
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSET0_FLAGOUTSET0_SHFT                          0x0

#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTCLR0_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00004254)
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTCLR0_RMSK                                      0x1
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTCLR0_OUT(v)      \
        out_dword(HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTCLR0_ADDR,v)
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTCLR0_FLAGOUTCLR0_BMSK                          0x1
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTCLR0_FLAGOUTCLR0_SHFT                          0x0

#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSTATUS0_ADDR                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00004258)
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSTATUS0_RMSK                                   0x1
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSTATUS0_IN          \
        in_dword(HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSTATUS0_ADDR)
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSTATUS0_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSTATUS0_ADDR, m)
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSTATUS0_FLAGOUTSTATUS0_BMSK                    0x1
#define HWIO_APCS_CCI_MMAP_MODES_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGOUTSTATUS0_FLAGOUTSTATUS0_SHFT                    0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_COREID_ADDR                                        (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008000)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_COREID_RMSK                                        0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_COREID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_COREID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_COREID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_COREID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_COREID_CORECHECKSUM_BMSK                           0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_COREID_CORECHECKSUM_SHFT                                  0x8
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_COREID_CORETYPEID_BMSK                                   0xff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_COREID_CORETYPEID_SHFT                                    0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_REVISIONID_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008004)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_REVISIONID_RMSK                                    0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_REVISIONID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_REVISIONID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_REVISIONID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_REVISIONID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_REVISIONID_QNOCID_BMSK                             0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_REVISIONID_QNOCID_SHFT                                    0x8
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_REVISIONID_USERID_BMSK                                   0xff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ID_REVISIONID_USERID_SHFT                                    0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_FAULTEN_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008008)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_FAULTEN_RMSK                                                 0x1
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_FAULTEN_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_FAULTEN_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_FAULTEN_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_FAULTEN_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_FAULTEN_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_FAULTEN_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_FAULTEN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_FAULTEN_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_FAULTEN_IN)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_FAULTEN_FAULTEN_BMSK                                         0x1
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_FAULTEN_FAULTEN_SHFT                                         0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRVLD_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000800c)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRVLD_RMSK                                                  0x1
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRVLD_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRVLD_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRVLD_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRVLD_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRVLD_ERRVLD_BMSK                                           0x1
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRVLD_ERRVLD_SHFT                                           0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRCLR_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008010)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRCLR_RMSK                                                  0x1
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRCLR_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRCLR_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRCLR_ERRCLR_BMSK                                           0x1
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRCLR_ERRCLR_SHFT                                           0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG0_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008014)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG0_RMSK                                          0x807f071f
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG0_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG0_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG0_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG0_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG0_FORMAT_BMSK                                   0x80000000
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG0_FORMAT_SHFT                                         0x1f
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG0_LEN1_BMSK                                       0x7f0000
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG0_LEN1_SHFT                                           0x10
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG0_ERRCODE_BMSK                                       0x700
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG0_ERRCODE_SHFT                                         0x8
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG0_OPC_BMSK                                            0x1e
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG0_OPC_SHFT                                             0x1
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG0_LOCK_BMSK                                            0x1
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG0_LOCK_SHFT                                            0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG1_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008018)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG1_RMSK                                          0x7fffffff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG1_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG1_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG1_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG1_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG1_ERRLOG1_BMSK                                  0x7fffffff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG1_ERRLOG1_SHFT                                         0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG3_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008020)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG3_RMSK                                          0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG3_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG3_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG3_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG3_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG3_ERRLOG3_BMSK                                  0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG3_ERRLOG3_SHFT                                         0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG4_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008024)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG4_RMSK                                               0xfff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG4_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG4_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG4_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG4_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG4_ERRLOG4_BMSK                                       0xfff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG4_ERRLOG4_SHFT                                         0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG5_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008028)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG5_RMSK                                             0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG5_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG5_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG5_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG5_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG5_ERRLOG5_BMSK                                     0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG5_ERRLOG5_SHFT                                         0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG7_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008030)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG7_RMSK                                                 0x1
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG7_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG7_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG7_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG7_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG7_ERRLOG7_BMSK                                         0x1
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_ERRLOG7_ERRLOG7_SHFT                                         0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_STALLEN_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008038)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_STALLEN_RMSK                                                 0x1
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_STALLEN_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_STALLEN_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_STALLEN_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_STALLEN_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_STALLEN_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_STALLEN_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_STALLEN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_STALLEN_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_STALLEN_IN)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_STALLEN_STALLEN_BMSK                                         0x1
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ERRORLOGGER_0_STALLEN_STALLEN_SHFT                                         0x0

#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_ADDR                                        (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008100)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_RMSK                                        0xffffffff
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_IN          \
        in_dword(HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_ADDR)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_ADDR, m)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORECHECKSUM_BMSK                           0xffffff00
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORECHECKSUM_SHFT                                  0x8
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORETYPEID_BMSK                                   0xff
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORETYPEID_SHFT                                    0x0

#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008104)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_RMSK                                    0xffffffff
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_IN          \
        in_dword(HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_ADDR)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_ADDR, m)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_QNOCID_BMSK                             0xffffff00
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_QNOCID_SHFT                                    0x8
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_USERID_BMSK                                   0xff
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_USERID_SHFT                                    0x0

#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008108)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_RMSK                                                 0x1
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_IN          \
        in_dword(HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_ADDR)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_ADDR, m)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_OUT(v)      \
        out_dword(HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_ADDR,v)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_ADDR,m,v,HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_IN)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_FAULTEN_BMSK                                         0x1
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_FAULTEN_SHFT                                         0x0

#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTSTATUS_ADDR                                      (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000810c)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTSTATUS_RMSK                                             0x1
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTSTATUS_IN          \
        in_dword(HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTSTATUS_ADDR)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTSTATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTSTATUS_ADDR, m)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTSTATUS_FAULTSTATUS_BMSK                                 0x1
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTSTATUS_FAULTSTATUS_SHFT                                 0x0

#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_ADDR                                        (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008110)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_RMSK                                               0x3
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_IN          \
        in_dword(HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_ADDR)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_ADDR, m)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_OUT(v)      \
        out_dword(HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_ADDR,v)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_ADDR,m,v,HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_IN)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_FLAGINEN0_BMSK                                     0x3
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_FLAGINEN0_SHFT                                     0x0

#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINSTATUS0_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008114)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINSTATUS0_RMSK                                           0x3
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINSTATUS0_IN          \
        in_dword(HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINSTATUS0_ADDR)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINSTATUS0_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINSTATUS0_ADDR, m)
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINSTATUS0_FLAGINSTATUS0_BMSK                             0x3
#define HWIO_APCS_CCI_ERR_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINSTATUS0_FLAGINSTATUS0_SHFT                             0x0

#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_ADDR                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008200)
#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_RMSK                                     0xffffffff
#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_IN          \
        in_dword(HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_ADDR)
#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_ADDR, m)
#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORECHECKSUM_BMSK                        0xffffff00
#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORECHECKSUM_SHFT                               0x8
#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORETYPEID_BMSK                                0xff
#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORETYPEID_SHFT                                 0x0

#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_ADDR                                 (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008204)
#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_RMSK                                 0xffffffff
#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_IN          \
        in_dword(HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_ADDR)
#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_ADDR, m)
#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_QNOCID_BMSK                          0xffffff00
#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_QNOCID_SHFT                                 0x8
#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_USERID_BMSK                                0xff
#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_USERID_SHFT                                 0x0

#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_SENSEIN0_ADDR                                      (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000082b0)
#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_SENSEIN0_RMSK                                            0xff
#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_SENSEIN0_IN          \
        in_dword(HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_SENSEIN0_ADDR)
#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_SENSEIN0_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_SENSEIN0_ADDR, m)
#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_SENSEIN0_SENSEIN0_BMSK                                   0xff
#define HWIO_APCS_CCI_NOPNDG_SIDEBANDS_MAIN_SIDEBANDMANAGER_SENSEIN0_SENSEIN0_SHFT                                    0x0

#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_ADDR                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008300)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_RMSK                                       0xffffffff
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_IN          \
        in_dword(HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_ADDR)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_ADDR, m)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORECHECKSUM_BMSK                          0xffffff00
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORECHECKSUM_SHFT                                 0x8
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORETYPEID_BMSK                                  0xff
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORETYPEID_SHFT                                   0x0

#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_ADDR                                   (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008304)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_RMSK                                   0xffffffff
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_IN          \
        in_dword(HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_ADDR)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_ADDR, m)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_QNOCID_BMSK                            0xffffff00
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_QNOCID_SHFT                                   0x8
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_USERID_BMSK                                  0xff
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_USERID_SHFT                                   0x0

#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_ADDR                                         (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008308)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_RMSK                                                0x1
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_IN          \
        in_dword(HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_ADDR)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_ADDR, m)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_OUT(v)      \
        out_dword(HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_ADDR,v)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_ADDR,m,v,HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_IN)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_FAULTEN_BMSK                                        0x1
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTEN_FAULTEN_SHFT                                        0x0

#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTSTATUS_ADDR                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000830c)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTSTATUS_RMSK                                            0x1
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTSTATUS_IN          \
        in_dword(HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTSTATUS_ADDR)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTSTATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTSTATUS_ADDR, m)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTSTATUS_FAULTSTATUS_BMSK                                0x1
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FAULTSTATUS_FAULTSTATUS_SHFT                                0x0

#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_ADDR                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008310)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_RMSK                                              0x1
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_IN          \
        in_dword(HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_ADDR)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_ADDR, m)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_OUT(v)      \
        out_dword(HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_ADDR,v)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_ADDR,m,v,HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_IN)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_FLAGINEN0_BMSK                                    0x1
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINEN0_FLAGINEN0_SHFT                                    0x0

#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINSTATUS0_ADDR                                   (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008314)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINSTATUS0_RMSK                                          0x1
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINSTATUS0_IN          \
        in_dword(HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINSTATUS0_ADDR)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINSTATUS0_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINSTATUS0_ADDR, m)
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINSTATUS0_FLAGINSTATUS0_BMSK                            0x1
#define HWIO_APCS_CCI_PERF_SIDEBANDS_MAIN_SIDEBANDMANAGER_FLAGINSTATUS0_FLAGINSTATUS0_SHFT                            0x0

#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_ADDR                                  (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008400)
#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_RMSK                                  0xffffffff
#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_IN          \
        in_dword(HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_ADDR)
#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_ADDR, m)
#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORECHECKSUM_BMSK                     0xffffff00
#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORECHECKSUM_SHFT                            0x8
#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORETYPEID_BMSK                             0xff
#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_COREID_CORETYPEID_SHFT                              0x0

#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_ADDR                              (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00008404)
#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_RMSK                              0xffffffff
#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_IN          \
        in_dword(HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_ADDR)
#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_ADDR, m)
#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_QNOCID_BMSK                       0xffffff00
#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_QNOCID_SHFT                              0x8
#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_USERID_BMSK                             0xff
#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_ID_REVISIONID_USERID_SHFT                              0x0

#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_SENSEIN0_ADDR                                   (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000084b0)
#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_SENSEIN0_RMSK                                         0x1f
#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_SENSEIN0_IN          \
        in_dword(HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_SENSEIN0_ADDR)
#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_SENSEIN0_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_SENSEIN0_ADDR, m)
#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_SENSEIN0_SENSEIN0_BMSK                                0x1f
#define HWIO_APCS_CCI_PWRACTIVE_SIDEBANDS_MAIN_SIDEBANDMANAGER_SENSEIN0_SENSEIN0_SHFT                                 0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_COREID_ADDR                                 (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009000)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_COREID_RMSK                                 0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_COREID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_COREID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_COREID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_COREID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_COREID_CORECHECKSUM_BMSK                    0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_COREID_CORECHECKSUM_SHFT                           0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_COREID_CORETYPEID_BMSK                            0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_COREID_CORETYPEID_SHFT                             0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_REVISIONID_ADDR                             (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009004)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_REVISIONID_RMSK                             0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_REVISIONID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_REVISIONID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_REVISIONID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_REVISIONID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_REVISIONID_QNOCID_BMSK                      0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_REVISIONID_QNOCID_SHFT                             0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_REVISIONID_USERID_BMSK                            0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ID_REVISIONID_USERID_SHFT                             0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_MODE_ADDR                                      (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009008)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_MODE_RMSK                                             0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_MODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_MODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_MODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_MODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_MODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_MODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_MODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_MODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_MODE_MODE_BMSK                                        0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_MODE_MODE_SHFT                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR                              (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000900c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_LOW_RMSK                              0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_LOW_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_LOW_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_LOW_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_LOW_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_LOW_ADDRBASE_LOW_BMSK                 0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_LOW_ADDRBASE_LOW_SHFT                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR                             (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009010)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_RMSK                                  0xfff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDRBASE_HIGH_BMSK                    0xfff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDRBASE_HIGH_SHFT                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009014)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_RMSK                                  0x3f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDRWINDOWSIZE_BMSK                   0x3f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDRWINDOWSIZE_SHFT                    0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDBASE_ADDR                                 (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009018)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDBASE_RMSK                                       0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDBASE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDBASE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDBASE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDBASE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDBASE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDBASE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDBASE_SRCIDBASE_BMSK                             0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDBASE_SRCIDBASE_SHFT                              0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDMASK_ADDR                                 (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000901c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDMASK_RMSK                                       0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDMASK_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDMASK_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDMASK_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDMASK_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDMASK_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDMASK_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDMASK_SRCIDMASK_BMSK                             0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_SRCIDMASK_SRCIDMASK_SHFT                              0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_OPCODE_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009020)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_OPCODE_RMSK                                           0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_OPCODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_OPCODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_OPCODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_OPCODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_OPCODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_OPCODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_OPCODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_OPCODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_OPCODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_OPCODE_WREN_BMSK                                      0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_OPCODE_WREN_SHFT                                      0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_OPCODE_RDEN_BMSK                                      0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_OPCODE_RDEN_SHFT                                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERBASE_ADDR                                  (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009024)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERBASE_RMSK                                     0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERBASE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERBASE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERBASE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERBASE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERBASE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERBASE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERBASE_USERBASE_BMSK                            0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERBASE_USERBASE_SHFT                                0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERMASK_ADDR                                  (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009028)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERMASK_RMSK                                     0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERMASK_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERMASK_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERMASK_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERMASK_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERMASK_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERMASK_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERMASK_USERMASK_BMSK                            0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_RD_TRANSACTIONFILTER_USERMASK_USERMASK_SHFT                                0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_COREID_ADDR                                 (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009080)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_COREID_RMSK                                 0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_COREID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_COREID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_COREID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_COREID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_COREID_CORECHECKSUM_BMSK                    0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_COREID_CORECHECKSUM_SHFT                           0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_COREID_CORETYPEID_BMSK                            0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_COREID_CORETYPEID_SHFT                             0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_REVISIONID_ADDR                             (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009084)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_REVISIONID_RMSK                             0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_REVISIONID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_REVISIONID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_REVISIONID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_REVISIONID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_REVISIONID_QNOCID_BMSK                      0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_REVISIONID_QNOCID_SHFT                             0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_REVISIONID_USERID_BMSK                            0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ID_REVISIONID_USERID_SHFT                             0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_MODE_ADDR                                      (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009088)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_MODE_RMSK                                             0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_MODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_MODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_MODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_MODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_MODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_MODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_MODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_MODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_MODE_MODE_BMSK                                        0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_MODE_MODE_SHFT                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR                              (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000908c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_LOW_RMSK                              0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_LOW_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_LOW_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_LOW_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_LOW_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_LOW_ADDRBASE_LOW_BMSK                 0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_LOW_ADDRBASE_LOW_SHFT                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR                             (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009090)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_RMSK                                  0xfff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDRBASE_HIGH_BMSK                    0xfff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDRBASE_HIGH_SHFT                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009094)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_RMSK                                  0x3f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDRWINDOWSIZE_BMSK                   0x3f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDRWINDOWSIZE_SHFT                    0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDBASE_ADDR                                 (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009098)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDBASE_RMSK                                       0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDBASE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDBASE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDBASE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDBASE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDBASE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDBASE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDBASE_SRCIDBASE_BMSK                             0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDBASE_SRCIDBASE_SHFT                              0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDMASK_ADDR                                 (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000909c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDMASK_RMSK                                       0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDMASK_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDMASK_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDMASK_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDMASK_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDMASK_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDMASK_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDMASK_SRCIDMASK_BMSK                             0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_SRCIDMASK_SRCIDMASK_SHFT                              0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_OPCODE_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000090a0)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_OPCODE_RMSK                                           0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_OPCODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_OPCODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_OPCODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_OPCODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_OPCODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_OPCODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_OPCODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_OPCODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_OPCODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_OPCODE_WREN_BMSK                                      0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_OPCODE_WREN_SHFT                                      0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_OPCODE_RDEN_BMSK                                      0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_OPCODE_RDEN_SHFT                                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERBASE_ADDR                                  (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000090a4)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERBASE_RMSK                                     0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERBASE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERBASE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERBASE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERBASE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERBASE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERBASE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERBASE_USERBASE_BMSK                            0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERBASE_USERBASE_SHFT                                0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERMASK_ADDR                                  (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000090a8)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERMASK_RMSK                                     0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERMASK_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERMASK_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERMASK_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERMASK_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERMASK_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERMASK_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERMASK_USERMASK_BMSK                            0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_0_WR_TRANSACTIONFILTER_USERMASK_USERMASK_SHFT                                0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_COREID_ADDR                                 (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009100)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_COREID_RMSK                                 0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_COREID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_COREID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_COREID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_COREID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_COREID_CORECHECKSUM_BMSK                    0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_COREID_CORECHECKSUM_SHFT                           0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_COREID_CORETYPEID_BMSK                            0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_COREID_CORETYPEID_SHFT                             0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_REVISIONID_ADDR                             (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009104)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_REVISIONID_RMSK                             0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_REVISIONID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_REVISIONID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_REVISIONID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_REVISIONID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_REVISIONID_QNOCID_BMSK                      0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_REVISIONID_QNOCID_SHFT                             0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_REVISIONID_USERID_BMSK                            0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ID_REVISIONID_USERID_SHFT                             0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_MODE_ADDR                                      (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009108)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_MODE_RMSK                                             0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_MODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_MODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_MODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_MODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_MODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_MODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_MODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_MODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_MODE_MODE_BMSK                                        0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_MODE_MODE_SHFT                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR                              (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000910c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_LOW_RMSK                              0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_LOW_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_LOW_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_LOW_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_LOW_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_LOW_ADDRBASE_LOW_BMSK                 0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_LOW_ADDRBASE_LOW_SHFT                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR                             (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009110)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_RMSK                                  0xfff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDRBASE_HIGH_BMSK                    0xfff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDRBASE_HIGH_SHFT                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009114)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_RMSK                                  0x3f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDRWINDOWSIZE_BMSK                   0x3f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDRWINDOWSIZE_SHFT                    0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDBASE_ADDR                                 (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009118)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDBASE_RMSK                                       0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDBASE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDBASE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDBASE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDBASE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDBASE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDBASE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDBASE_SRCIDBASE_BMSK                             0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDBASE_SRCIDBASE_SHFT                              0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDMASK_ADDR                                 (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000911c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDMASK_RMSK                                       0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDMASK_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDMASK_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDMASK_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDMASK_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDMASK_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDMASK_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDMASK_SRCIDMASK_BMSK                             0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_SRCIDMASK_SRCIDMASK_SHFT                              0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_OPCODE_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009120)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_OPCODE_RMSK                                           0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_OPCODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_OPCODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_OPCODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_OPCODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_OPCODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_OPCODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_OPCODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_OPCODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_OPCODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_OPCODE_WREN_BMSK                                      0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_OPCODE_WREN_SHFT                                      0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_OPCODE_RDEN_BMSK                                      0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_OPCODE_RDEN_SHFT                                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERBASE_ADDR                                  (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009124)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERBASE_RMSK                                     0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERBASE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERBASE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERBASE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERBASE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERBASE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERBASE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERBASE_USERBASE_BMSK                            0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERBASE_USERBASE_SHFT                                0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERMASK_ADDR                                  (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009128)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERMASK_RMSK                                     0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERMASK_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERMASK_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERMASK_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERMASK_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERMASK_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERMASK_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERMASK_USERMASK_BMSK                            0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_RD_TRANSACTIONFILTER_USERMASK_USERMASK_SHFT                                0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_COREID_ADDR                                 (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009180)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_COREID_RMSK                                 0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_COREID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_COREID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_COREID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_COREID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_COREID_CORECHECKSUM_BMSK                    0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_COREID_CORECHECKSUM_SHFT                           0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_COREID_CORETYPEID_BMSK                            0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_COREID_CORETYPEID_SHFT                             0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_REVISIONID_ADDR                             (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009184)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_REVISIONID_RMSK                             0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_REVISIONID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_REVISIONID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_REVISIONID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_REVISIONID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_REVISIONID_QNOCID_BMSK                      0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_REVISIONID_QNOCID_SHFT                             0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_REVISIONID_USERID_BMSK                            0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ID_REVISIONID_USERID_SHFT                             0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_MODE_ADDR                                      (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009188)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_MODE_RMSK                                             0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_MODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_MODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_MODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_MODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_MODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_MODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_MODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_MODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_MODE_MODE_BMSK                                        0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_MODE_MODE_SHFT                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR                              (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000918c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_LOW_RMSK                              0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_LOW_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_LOW_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_LOW_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_LOW_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_LOW_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_LOW_ADDRBASE_LOW_BMSK                 0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_LOW_ADDRBASE_LOW_SHFT                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR                             (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009190)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_RMSK                                  0xfff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDRBASE_HIGH_BMSK                    0xfff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRBASE_HIGH_ADDRBASE_HIGH_SHFT                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009194)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_RMSK                                  0x3f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDRWINDOWSIZE_BMSK                   0x3f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_ADDRWINDOWSIZE_ADDRWINDOWSIZE_SHFT                    0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDBASE_ADDR                                 (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009198)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDBASE_RMSK                                       0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDBASE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDBASE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDBASE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDBASE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDBASE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDBASE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDBASE_SRCIDBASE_BMSK                             0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDBASE_SRCIDBASE_SHFT                              0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDMASK_ADDR                                 (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000919c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDMASK_RMSK                                       0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDMASK_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDMASK_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDMASK_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDMASK_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDMASK_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDMASK_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDMASK_SRCIDMASK_BMSK                             0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_SRCIDMASK_SRCIDMASK_SHFT                              0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_OPCODE_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000091a0)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_OPCODE_RMSK                                           0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_OPCODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_OPCODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_OPCODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_OPCODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_OPCODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_OPCODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_OPCODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_OPCODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_OPCODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_OPCODE_WREN_BMSK                                      0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_OPCODE_WREN_SHFT                                      0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_OPCODE_RDEN_BMSK                                      0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_OPCODE_RDEN_SHFT                                      0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERBASE_ADDR                                  (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000091a4)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERBASE_RMSK                                     0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERBASE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERBASE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERBASE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERBASE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERBASE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERBASE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERBASE_USERBASE_BMSK                            0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERBASE_USERBASE_SHFT                                0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERMASK_ADDR                                  (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000091a8)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERMASK_RMSK                                     0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERMASK_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERMASK_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERMASK_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERMASK_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERMASK_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERMASK_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERMASK_USERMASK_BMSK                            0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_ACE_1_WR_TRANSACTIONFILTER_USERMASK_USERMASK_SHFT                                0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_COREID_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009200)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_COREID_RMSK                                    0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_COREID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_COREID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_COREID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_COREID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_COREID_CORECHECKSUM_BMSK                       0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_COREID_CORECHECKSUM_SHFT                              0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_COREID_CORETYPEID_BMSK                               0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_COREID_CORETYPEID_SHFT                                0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_REVISIONID_ADDR                                (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009204)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_REVISIONID_RMSK                                0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_REVISIONID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_REVISIONID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_REVISIONID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_REVISIONID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_REVISIONID_QNOCID_BMSK                         0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_REVISIONID_QNOCID_SHFT                                0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_REVISIONID_USERID_BMSK                               0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_ID_REVISIONID_USERID_SHFT                                0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_EN_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009208)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_EN_RMSK                                                  0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_EN_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_EN_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_EN_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_EN_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_EN_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_EN_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_EN_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_EN_EN_BMSK                                               0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_EN_EN_SHFT                                               0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_MODE_ADDR                                         (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000920c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_MODE_RMSK                                                0xf
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_MODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_MODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_MODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_MODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_MODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_MODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_MODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_MODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_MODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_MODE_MODE_BMSK                                           0xf
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_MODE_MODE_SHFT                                           0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_0_ADDR                                (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009210)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_0_RMSK                                       0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_0_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_0_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_0_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_0_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_0_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_0_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_0_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_0_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_0_OBSERVEDSEL_0_BMSK                         0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_0_OBSERVEDSEL_0_SHFT                         0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_1_ADDR                                (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009214)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_1_RMSK                                       0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_1_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_1_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_1_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_1_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_1_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_1_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_1_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_1_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_1_OBSERVEDSEL_1_BMSK                         0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_1_OBSERVEDSEL_1_SHFT                         0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_2_ADDR                                (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009218)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_2_RMSK                                       0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_2_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_2_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_2_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_2_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_2_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_2_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_2_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_2_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_2_OBSERVEDSEL_2_BMSK                         0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_2_OBSERVEDSEL_2_SHFT                         0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_3_ADDR                                (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000921c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_3_RMSK                                       0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_3_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_3_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_3_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_3_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_3_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_3_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_3_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_3_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_3_OBSERVEDSEL_3_BMSK                         0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OBSERVEDSEL_3_OBSERVEDSEL_3_SHFT                         0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_0_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009220)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_0_RMSK                                      0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_0_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_0_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_0_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_0_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_0_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_0_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_0_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_0_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_0_NTENURELINES_0_BMSK                       0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_0_NTENURELINES_0_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_1_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009224)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_1_RMSK                                      0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_1_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_1_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_1_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_1_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_1_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_1_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_1_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_1_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_1_NTENURELINES_1_BMSK                       0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_1_NTENURELINES_1_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_2_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009228)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_2_RMSK                                      0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_2_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_2_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_2_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_2_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_2_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_2_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_2_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_2_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_2_NTENURELINES_2_BMSK                       0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_NTENURELINES_2_NTENURELINES_2_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_0_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000922c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_0_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_0_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_0_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_0_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_0_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_0_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_0_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_0_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_0_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_0_THRESHOLDS_0_0_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_0_THRESHOLDS_0_0_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_1_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009230)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_1_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_1_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_1_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_1_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_1_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_1_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_1_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_1_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_1_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_1_THRESHOLDS_0_1_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_1_THRESHOLDS_0_1_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_2_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009234)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_2_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_2_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_2_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_2_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_2_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_2_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_2_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_2_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_2_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_2_THRESHOLDS_0_2_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_2_THRESHOLDS_0_2_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_3_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009238)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_3_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_3_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_3_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_3_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_3_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_3_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_3_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_3_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_3_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_3_THRESHOLDS_0_3_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_3_THRESHOLDS_0_3_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_4_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000923c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_4_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_4_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_4_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_4_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_4_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_4_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_4_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_4_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_4_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_4_THRESHOLDS_0_4_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_4_THRESHOLDS_0_4_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_5_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009240)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_5_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_5_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_5_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_5_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_5_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_5_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_5_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_5_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_5_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_5_THRESHOLDS_0_5_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_5_THRESHOLDS_0_5_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_6_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009244)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_6_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_6_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_6_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_6_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_6_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_6_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_6_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_6_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_6_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_6_THRESHOLDS_0_6_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_0_6_THRESHOLDS_0_6_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_0_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009268)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_0_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_0_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_0_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_0_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_0_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_0_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_0_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_0_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_0_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_0_THRESHOLDS_1_0_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_0_THRESHOLDS_1_0_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_1_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000926c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_1_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_1_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_1_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_1_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_1_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_1_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_1_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_1_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_1_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_1_THRESHOLDS_1_1_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_1_THRESHOLDS_1_1_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_2_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009270)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_2_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_2_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_2_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_2_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_2_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_2_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_2_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_2_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_2_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_2_THRESHOLDS_1_2_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_2_THRESHOLDS_1_2_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_3_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009274)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_3_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_3_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_3_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_3_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_3_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_3_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_3_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_3_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_3_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_3_THRESHOLDS_1_3_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_3_THRESHOLDS_1_3_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_4_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009278)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_4_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_4_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_4_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_4_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_4_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_4_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_4_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_4_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_4_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_4_THRESHOLDS_1_4_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_4_THRESHOLDS_1_4_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_5_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000927c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_5_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_5_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_5_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_5_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_5_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_5_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_5_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_5_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_5_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_5_THRESHOLDS_1_5_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_5_THRESHOLDS_1_5_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_6_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009280)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_6_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_6_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_6_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_6_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_6_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_6_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_6_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_6_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_6_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_6_THRESHOLDS_1_6_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_1_6_THRESHOLDS_1_6_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_0_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000092a4)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_0_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_0_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_0_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_0_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_0_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_0_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_0_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_0_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_0_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_0_THRESHOLDS_2_0_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_0_THRESHOLDS_2_0_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_1_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000092a8)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_1_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_1_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_1_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_1_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_1_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_1_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_1_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_1_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_1_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_1_THRESHOLDS_2_1_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_1_THRESHOLDS_2_1_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_2_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000092ac)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_2_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_2_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_2_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_2_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_2_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_2_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_2_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_2_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_2_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_2_THRESHOLDS_2_2_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_2_THRESHOLDS_2_2_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_3_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000092b0)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_3_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_3_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_3_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_3_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_3_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_3_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_3_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_3_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_3_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_3_THRESHOLDS_2_3_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_3_THRESHOLDS_2_3_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_4_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000092b4)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_4_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_4_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_4_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_4_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_4_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_4_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_4_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_4_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_4_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_4_THRESHOLDS_2_4_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_4_THRESHOLDS_2_4_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_5_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000092b8)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_5_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_5_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_5_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_5_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_5_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_5_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_5_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_5_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_5_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_5_THRESHOLDS_2_5_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_5_THRESHOLDS_2_5_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_6_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000092bc)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_6_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_6_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_6_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_6_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_6_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_6_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_6_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_6_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_6_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_6_THRESHOLDS_2_6_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_2_6_THRESHOLDS_2_6_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_0_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000092e0)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_0_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_0_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_0_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_0_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_0_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_0_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_0_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_0_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_0_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_0_THRESHOLDS_3_0_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_0_THRESHOLDS_3_0_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_1_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000092e4)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_1_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_1_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_1_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_1_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_1_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_1_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_1_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_1_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_1_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_1_THRESHOLDS_3_1_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_1_THRESHOLDS_3_1_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_2_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000092e8)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_2_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_2_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_2_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_2_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_2_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_2_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_2_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_2_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_2_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_2_THRESHOLDS_3_2_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_2_THRESHOLDS_3_2_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_3_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000092ec)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_3_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_3_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_3_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_3_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_3_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_3_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_3_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_3_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_3_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_3_THRESHOLDS_3_3_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_3_THRESHOLDS_3_3_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_4_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000092f0)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_4_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_4_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_4_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_4_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_4_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_4_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_4_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_4_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_4_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_4_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_4_THRESHOLDS_3_4_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_4_THRESHOLDS_3_4_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_5_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000092f4)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_5_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_5_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_5_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_5_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_5_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_5_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_5_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_5_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_5_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_5_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_5_THRESHOLDS_3_5_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_5_THRESHOLDS_3_5_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_6_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000092f8)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_6_RMSK                                    0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_6_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_6_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_6_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_6_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_6_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_6_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_6_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_6_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_6_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_6_THRESHOLDS_3_6_BMSK                     0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_THRESHOLDS_3_6_THRESHOLDS_3_6_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OVERFLOWSTATUS_ADDR                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000931c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OVERFLOWSTATUS_RMSK                                      0xf
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OVERFLOWSTATUS_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OVERFLOWSTATUS_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OVERFLOWSTATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OVERFLOWSTATUS_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OVERFLOWSTATUS_OVERFLOWSTATUS_BMSK                       0xf
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OVERFLOWSTATUS_OVERFLOWSTATUS_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OVERFLOWRESET_ADDR                                (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009320)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OVERFLOWRESET_RMSK                                       0xf
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OVERFLOWRESET_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OVERFLOWRESET_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OVERFLOWRESET_OVERFLOWRESET_BMSK                         0xf
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_OVERFLOWRESET_OVERFLOWRESET_SHFT                         0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PENDINGEVENTMODE_ADDR                             (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009324)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PENDINGEVENTMODE_RMSK                                    0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PENDINGEVENTMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PENDINGEVENTMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PENDINGEVENTMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PENDINGEVENTMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PENDINGEVENTMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PENDINGEVENTMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PENDINGEVENTMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PENDINGEVENTMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PENDINGEVENTMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PENDINGEVENTMODE_PENDINGEVENTMODE_BMSK                   0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PENDINGEVENTMODE_PENDINGEVENTMODE_SHFT                   0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PRESCALER_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009328)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PRESCALER_RMSK                                          0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PRESCALER_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PRESCALER_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PRESCALER_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PRESCALER_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PRESCALER_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PRESCALER_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PRESCALER_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PRESCALER_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PRESCALER_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PRESCALER_PRESCALER_BMSK                                0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_TRANSACTIONSTATPROFILER_PRESCALER_PRESCALER_SHFT                                 0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_COREID_ADDR                                                (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009400)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_COREID_RMSK                                                0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_COREID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_COREID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_COREID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_COREID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_COREID_CORECHECKSUM_BMSK                                   0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_COREID_CORECHECKSUM_SHFT                                          0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_COREID_CORETYPEID_BMSK                                           0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_COREID_CORETYPEID_SHFT                                            0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_REVISIONID_ADDR                                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009404)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_REVISIONID_RMSK                                            0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_REVISIONID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_REVISIONID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_REVISIONID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_REVISIONID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_REVISIONID_QNOCID_BMSK                                     0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_REVISIONID_QNOCID_SHFT                                            0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_REVISIONID_USERID_BMSK                                           0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_ID_REVISIONID_USERID_SHFT                                            0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_ADDR                                                  (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009408)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_RMSK                                                        0xff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_FILTBYTEALWAYSCHAINABLEEN_BMSK                              0x80
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_FILTBYTEALWAYSCHAINABLEEN_SHFT                               0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_INTRUSIVEMODE_BMSK                                          0x40
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_INTRUSIVEMODE_SHFT                                           0x6
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_STATCONDDUMP_BMSK                                           0x20
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_STATCONDDUMP_SHFT                                            0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_ALARMEN_BMSK                                                0x10
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_ALARMEN_SHFT                                                 0x4
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_STATEN_BMSK                                                  0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_STATEN_SHFT                                                  0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_PAYLOADEN_BMSK                                               0x4
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_PAYLOADEN_SHFT                                               0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_TRACEEN_BMSK                                                 0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_TRACEEN_SHFT                                                 0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_ERREN_BMSK                                                   0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_MAINCTL_ERREN_SHFT                                                   0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_CFGCTL_ADDR                                                   (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000940c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_CFGCTL_RMSK                                                          0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_CFGCTL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_CFGCTL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_CFGCTL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_CFGCTL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_CFGCTL_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_CFGCTL_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_CFGCTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_CFGCTL_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_CFGCTL_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_CFGCTL_ACTIVE_BMSK                                                   0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_CFGCTL_ACTIVE_SHFT                                                   0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_CFGCTL_GLOBALEN_BMSK                                                 0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_CFGCTL_GLOBALEN_SHFT                                                 0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERLUT_ADDR                                                (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009414)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERLUT_RMSK                                                       0xf
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERLUT_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERLUT_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERLUT_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERLUT_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERLUT_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERLUT_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERLUT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERLUT_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERLUT_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERLUT_FILTERLUT_BMSK                                             0xf
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERLUT_FILTERLUT_SHFT                                             0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMEN_ADDR                                             (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009418)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMEN_RMSK                                                    0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMEN_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMEN_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMEN_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMEN_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMEN_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMEN_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMEN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMEN_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMEN_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMEN_TRACEALARMEN_BMSK                                       0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMEN_TRACEALARMEN_SHFT                                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMSTATUS_ADDR                                         (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000941c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMSTATUS_RMSK                                                0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMSTATUS_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMSTATUS_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMSTATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMSTATUS_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMSTATUS_TRACEALARMSTATUS_BMSK                               0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMSTATUS_TRACEALARMSTATUS_SHFT                               0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMCLR_ADDR                                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009420)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMCLR_RMSK                                                   0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMCLR_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMCLR_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMCLR_TRACEALARMCLR_BMSK                                     0x7
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_TRACEALARMCLR_TRACEALARMCLR_SHFT                                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATPERIOD_ADDR                                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009424)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATPERIOD_RMSK                                                     0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATPERIOD_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATPERIOD_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATPERIOD_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATPERIOD_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATPERIOD_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATPERIOD_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATPERIOD_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATPERIOD_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATPERIOD_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATPERIOD_STATPERIOD_BMSK                                          0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATPERIOD_STATPERIOD_SHFT                                           0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATGO_ADDR                                                   (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009428)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATGO_RMSK                                                          0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATGO_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATGO_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATGO_STATGO_BMSK                                                   0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATGO_STATGO_SHFT                                                   0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMIN_ADDR                                             (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000942c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMIN_RMSK                                             0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMIN_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMIN_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMIN_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMIN_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMIN_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMIN_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMIN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMIN_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMIN_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMIN_STATALARMMIN_BMSK                                0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMIN_STATALARMMIN_SHFT                                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMAX_ADDR                                             (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009430)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMAX_RMSK                                             0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMAX_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMAX_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMAX_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMAX_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMAX_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMAX_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMAX_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMAX_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMAX_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMAX_STATALARMMAX_BMSK                                0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMMAX_STATALARMMAX_SHFT                                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMSTATUS_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009434)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMSTATUS_RMSK                                                 0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMSTATUS_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMSTATUS_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMSTATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMSTATUS_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMSTATUS_STATALARMSTATUS_BMSK                                 0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMSTATUS_STATALARMSTATUS_SHFT                                 0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMCLR_ADDR                                             (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009438)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMCLR_RMSK                                                    0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMCLR_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMCLR_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMCLR_STATALARMCLR_BMSK                                       0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMCLR_STATALARMCLR_SHFT                                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMEN_ADDR                                              (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000943c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMEN_RMSK                                                     0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMEN_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMEN_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMEN_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMEN_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMEN_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMEN_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMEN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMEN_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMEN_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMEN_STATALARMEN_BMSK                                         0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_STATALARMEN_STATALARMEN_SHFT                                         0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDBASE_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009444)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDBASE_RMSK                                         0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDBASE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDBASE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDBASE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDBASE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDBASE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDBASE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDBASE_FILTERS_0_ROUTEIDBASE_BMSK                   0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDBASE_FILTERS_0_ROUTEIDBASE_SHFT                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDMASK_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009448)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDMASK_RMSK                                         0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDMASK_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDMASK_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDMASK_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDMASK_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDMASK_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDMASK_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDMASK_FILTERS_0_ROUTEIDMASK_BMSK                   0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ROUTEIDMASK_FILTERS_0_ROUTEIDMASK_SHFT                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_LOW_ADDR                                   (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000944c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_LOW_RMSK                                   0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_LOW_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_LOW_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_LOW_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_LOW_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_LOW_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_LOW_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_LOW_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_LOW_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_LOW_FILTERS_0_ADDRBASE_LOW_BMSK            0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_LOW_FILTERS_0_ADDRBASE_LOW_SHFT                   0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_HIGH_ADDR                                  (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009450)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_HIGH_RMSK                                       0xfff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_HIGH_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_HIGH_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_HIGH_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_HIGH_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_HIGH_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_HIGH_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_HIGH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_HIGH_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_HIGH_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_HIGH_FILTERS_0_ADDRBASE_HIGH_BMSK               0xfff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_ADDRBASE_HIGH_FILTERS_0_ADDRBASE_HIGH_SHFT                 0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_WINDOWSIZE_ADDR                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009454)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_WINDOWSIZE_RMSK                                           0x3f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_WINDOWSIZE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_WINDOWSIZE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_WINDOWSIZE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_WINDOWSIZE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_WINDOWSIZE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_WINDOWSIZE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_WINDOWSIZE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_WINDOWSIZE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_WINDOWSIZE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_WINDOWSIZE_FILTERS_0_WINDOWSIZE_BMSK                      0x3f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_WINDOWSIZE_FILTERS_0_WINDOWSIZE_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_OPCODE_ADDR                                         (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009460)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_OPCODE_RMSK                                                0xf
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_OPCODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_OPCODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_OPCODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_OPCODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_OPCODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_OPCODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_OPCODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_OPCODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_OPCODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_OPCODE_URGEN_BMSK                                          0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_OPCODE_URGEN_SHFT                                          0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_OPCODE_LOCKEN_BMSK                                         0x4
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_OPCODE_LOCKEN_SHFT                                         0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_OPCODE_WREN_BMSK                                           0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_OPCODE_WREN_SHFT                                           0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_OPCODE_RDEN_BMSK                                           0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_OPCODE_RDEN_SHFT                                           0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_STATUS_ADDR                                         (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009464)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_STATUS_RMSK                                                0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_STATUS_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_STATUS_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_STATUS_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_STATUS_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_STATUS_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_STATUS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_STATUS_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_STATUS_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_STATUS_RSPEN_BMSK                                          0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_STATUS_RSPEN_SHFT                                          0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_STATUS_REQEN_BMSK                                          0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_STATUS_REQEN_SHFT                                          0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_LENGTH_ADDR                                         (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009468)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_LENGTH_RMSK                                                0xf
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_LENGTH_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_LENGTH_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_LENGTH_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_LENGTH_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_LENGTH_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_LENGTH_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_LENGTH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_LENGTH_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_LENGTH_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_LENGTH_FILTERS_0_LENGTH_BMSK                               0xf
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_LENGTH_FILTERS_0_LENGTH_SHFT                               0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERBASE_ADDR                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009470)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERBASE_RMSK                                          0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERBASE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERBASE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERBASE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERBASE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERBASE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERBASE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERBASE_FILTERS_0_USERBASE_BMSK                       0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERBASE_FILTERS_0_USERBASE_SHFT                           0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERMASK_ADDR                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009474)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERMASK_RMSK                                          0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERMASK_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERMASK_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERMASK_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERMASK_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERMASK_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERMASK_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERMASK_FILTERS_0_USERMASK_BMSK                       0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_0_USERMASK_FILTERS_0_USERMASK_SHFT                           0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDBASE_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009480)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDBASE_RMSK                                         0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDBASE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDBASE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDBASE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDBASE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDBASE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDBASE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDBASE_FILTERS_1_ROUTEIDBASE_BMSK                   0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDBASE_FILTERS_1_ROUTEIDBASE_SHFT                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDMASK_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009484)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDMASK_RMSK                                         0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDMASK_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDMASK_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDMASK_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDMASK_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDMASK_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDMASK_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDMASK_FILTERS_1_ROUTEIDMASK_BMSK                   0x7ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ROUTEIDMASK_FILTERS_1_ROUTEIDMASK_SHFT                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_LOW_ADDR                                   (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009488)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_LOW_RMSK                                   0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_LOW_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_LOW_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_LOW_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_LOW_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_LOW_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_LOW_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_LOW_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_LOW_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_LOW_FILTERS_1_ADDRBASE_LOW_BMSK            0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_LOW_FILTERS_1_ADDRBASE_LOW_SHFT                   0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_HIGH_ADDR                                  (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000948c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_HIGH_RMSK                                       0xfff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_HIGH_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_HIGH_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_HIGH_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_HIGH_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_HIGH_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_HIGH_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_HIGH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_HIGH_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_HIGH_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_HIGH_FILTERS_1_ADDRBASE_HIGH_BMSK               0xfff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_ADDRBASE_HIGH_FILTERS_1_ADDRBASE_HIGH_SHFT                 0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_WINDOWSIZE_ADDR                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009490)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_WINDOWSIZE_RMSK                                           0x3f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_WINDOWSIZE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_WINDOWSIZE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_WINDOWSIZE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_WINDOWSIZE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_WINDOWSIZE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_WINDOWSIZE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_WINDOWSIZE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_WINDOWSIZE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_WINDOWSIZE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_WINDOWSIZE_FILTERS_1_WINDOWSIZE_BMSK                      0x3f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_WINDOWSIZE_FILTERS_1_WINDOWSIZE_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_OPCODE_ADDR                                         (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000949c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_OPCODE_RMSK                                                0xf
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_OPCODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_OPCODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_OPCODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_OPCODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_OPCODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_OPCODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_OPCODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_OPCODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_OPCODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_OPCODE_URGEN_BMSK                                          0x8
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_OPCODE_URGEN_SHFT                                          0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_OPCODE_LOCKEN_BMSK                                         0x4
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_OPCODE_LOCKEN_SHFT                                         0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_OPCODE_WREN_BMSK                                           0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_OPCODE_WREN_SHFT                                           0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_OPCODE_RDEN_BMSK                                           0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_OPCODE_RDEN_SHFT                                           0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_STATUS_ADDR                                         (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000094a0)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_STATUS_RMSK                                                0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_STATUS_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_STATUS_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_STATUS_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_STATUS_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_STATUS_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_STATUS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_STATUS_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_STATUS_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_STATUS_RSPEN_BMSK                                          0x2
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_STATUS_RSPEN_SHFT                                          0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_STATUS_REQEN_BMSK                                          0x1
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_STATUS_REQEN_SHFT                                          0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_LENGTH_ADDR                                         (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000094a4)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_LENGTH_RMSK                                                0xf
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_LENGTH_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_LENGTH_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_LENGTH_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_LENGTH_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_LENGTH_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_LENGTH_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_LENGTH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_LENGTH_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_LENGTH_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_LENGTH_FILTERS_1_LENGTH_BMSK                               0xf
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_LENGTH_FILTERS_1_LENGTH_SHFT                               0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERBASE_ADDR                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000094ac)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERBASE_RMSK                                          0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERBASE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERBASE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERBASE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERBASE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERBASE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERBASE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERBASE_FILTERS_1_USERBASE_BMSK                       0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERBASE_FILTERS_1_USERBASE_SHFT                           0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERMASK_ADDR                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000094b0)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERMASK_RMSK                                          0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERMASK_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERMASK_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERMASK_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERMASK_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERMASK_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERMASK_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERMASK_FILTERS_1_USERMASK_BMSK                       0x7ffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_FILTERS_1_USERMASK_FILTERS_1_USERMASK_SHFT                           0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_SRC_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009538)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_SRC_RMSK                                                0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_SRC_EXTEVENT_BMSK                                       0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_SRC_EXTEVENT_SHFT                                         0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_SRC_INTEVENT_BMSK                                        0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_SRC_INTEVENT_SHFT                                         0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_ALARMMODE_ADDR                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000953c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_ALARMMODE_RMSK                                            0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_ALARMMODE_COUNTERS_0_ALARMMODE_BMSK                       0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_ALARMMODE_COUNTERS_0_ALARMMODE_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_VAL_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009540)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_VAL_RMSK                                               0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_VAL_COUNTERS_0_VAL_BMSK                                0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_0_VAL_COUNTERS_0_VAL_SHFT                                   0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_SRC_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000954c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_SRC_RMSK                                                0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_SRC_EXTEVENT_BMSK                                       0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_SRC_EXTEVENT_SHFT                                         0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_SRC_INTEVENT_BMSK                                        0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_SRC_INTEVENT_SHFT                                         0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_ALARMMODE_ADDR                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009550)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_ALARMMODE_RMSK                                            0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_ALARMMODE_COUNTERS_1_ALARMMODE_BMSK                       0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_ALARMMODE_COUNTERS_1_ALARMMODE_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_VAL_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009554)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_VAL_RMSK                                               0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_VAL_COUNTERS_1_VAL_BMSK                                0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_1_VAL_COUNTERS_1_VAL_SHFT                                   0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_SRC_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009560)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_SRC_RMSK                                                0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_SRC_EXTEVENT_BMSK                                       0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_SRC_EXTEVENT_SHFT                                         0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_SRC_INTEVENT_BMSK                                        0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_SRC_INTEVENT_SHFT                                         0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_ALARMMODE_ADDR                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009564)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_ALARMMODE_RMSK                                            0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_ALARMMODE_COUNTERS_2_ALARMMODE_BMSK                       0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_ALARMMODE_COUNTERS_2_ALARMMODE_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_VAL_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009568)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_VAL_RMSK                                               0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_VAL_COUNTERS_2_VAL_BMSK                                0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_2_VAL_COUNTERS_2_VAL_SHFT                                   0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_SRC_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009574)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_SRC_RMSK                                                0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_SRC_EXTEVENT_BMSK                                       0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_SRC_EXTEVENT_SHFT                                         0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_SRC_INTEVENT_BMSK                                        0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_SRC_INTEVENT_SHFT                                         0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_ALARMMODE_ADDR                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009578)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_ALARMMODE_RMSK                                            0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_ALARMMODE_COUNTERS_3_ALARMMODE_BMSK                       0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_ALARMMODE_COUNTERS_3_ALARMMODE_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_VAL_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000957c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_VAL_RMSK                                               0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_VAL_COUNTERS_3_VAL_BMSK                                0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_3_VAL_COUNTERS_3_VAL_SHFT                                   0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_SRC_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009588)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_SRC_RMSK                                                0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_SRC_EXTEVENT_BMSK                                       0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_SRC_EXTEVENT_SHFT                                         0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_SRC_INTEVENT_BMSK                                        0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_SRC_INTEVENT_SHFT                                         0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_ALARMMODE_ADDR                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000958c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_ALARMMODE_RMSK                                            0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_ALARMMODE_COUNTERS_4_ALARMMODE_BMSK                       0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_ALARMMODE_COUNTERS_4_ALARMMODE_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_VAL_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009590)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_VAL_RMSK                                               0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_VAL_COUNTERS_4_VAL_BMSK                                0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_4_VAL_COUNTERS_4_VAL_SHFT                                   0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_SRC_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000959c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_SRC_RMSK                                                0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_SRC_EXTEVENT_BMSK                                       0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_SRC_EXTEVENT_SHFT                                         0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_SRC_INTEVENT_BMSK                                        0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_SRC_INTEVENT_SHFT                                         0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_ALARMMODE_ADDR                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000095a0)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_ALARMMODE_RMSK                                            0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_ALARMMODE_COUNTERS_5_ALARMMODE_BMSK                       0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_ALARMMODE_COUNTERS_5_ALARMMODE_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_VAL_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000095a4)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_VAL_RMSK                                               0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_VAL_COUNTERS_5_VAL_BMSK                                0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_5_VAL_COUNTERS_5_VAL_SHFT                                   0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_SRC_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000095b0)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_SRC_RMSK                                                0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_SRC_EXTEVENT_BMSK                                       0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_SRC_EXTEVENT_SHFT                                         0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_SRC_INTEVENT_BMSK                                        0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_SRC_INTEVENT_SHFT                                         0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_ALARMMODE_ADDR                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000095b4)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_ALARMMODE_RMSK                                            0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_ALARMMODE_COUNTERS_6_ALARMMODE_BMSK                       0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_ALARMMODE_COUNTERS_6_ALARMMODE_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_VAL_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000095b8)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_VAL_RMSK                                               0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_VAL_COUNTERS_6_VAL_BMSK                                0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_6_VAL_COUNTERS_6_VAL_SHFT                                   0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_SRC_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000095c4)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_SRC_RMSK                                                0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_SRC_EXTEVENT_BMSK                                       0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_SRC_EXTEVENT_SHFT                                         0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_SRC_INTEVENT_BMSK                                        0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_SRC_INTEVENT_SHFT                                         0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_ALARMMODE_ADDR                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000095c8)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_ALARMMODE_RMSK                                            0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_ALARMMODE_COUNTERS_7_ALARMMODE_BMSK                       0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_ALARMMODE_COUNTERS_7_ALARMMODE_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_VAL_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000095cc)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_VAL_RMSK                                               0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_VAL_COUNTERS_7_VAL_BMSK                                0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_7_VAL_COUNTERS_7_VAL_SHFT                                   0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_SRC_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000095d8)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_SRC_RMSK                                                0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_SRC_EXTEVENT_BMSK                                       0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_SRC_EXTEVENT_SHFT                                         0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_SRC_INTEVENT_BMSK                                        0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_SRC_INTEVENT_SHFT                                         0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_ALARMMODE_ADDR                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000095dc)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_ALARMMODE_RMSK                                            0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_ALARMMODE_COUNTERS_8_ALARMMODE_BMSK                       0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_ALARMMODE_COUNTERS_8_ALARMMODE_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_VAL_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000095e0)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_VAL_RMSK                                               0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_VAL_COUNTERS_8_VAL_BMSK                                0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_8_VAL_COUNTERS_8_VAL_SHFT                                   0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_SRC_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000095ec)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_SRC_RMSK                                                0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_SRC_EXTEVENT_BMSK                                       0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_SRC_EXTEVENT_SHFT                                         0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_SRC_INTEVENT_BMSK                                        0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_SRC_INTEVENT_SHFT                                         0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_ALARMMODE_ADDR                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000095f0)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_ALARMMODE_RMSK                                            0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_ALARMMODE_COUNTERS_9_ALARMMODE_BMSK                       0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_ALARMMODE_COUNTERS_9_ALARMMODE_SHFT                       0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_VAL_ADDR                                           (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000095f4)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_VAL_RMSK                                               0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_VAL_COUNTERS_9_VAL_BMSK                                0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_9_VAL_COUNTERS_9_VAL_SHFT                                   0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_SRC_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009600)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_SRC_RMSK                                               0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_SRC_EXTEVENT_BMSK                                      0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_SRC_EXTEVENT_SHFT                                        0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_SRC_INTEVENT_BMSK                                       0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_SRC_INTEVENT_SHFT                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_ALARMMODE_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009604)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_ALARMMODE_RMSK                                           0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_ALARMMODE_COUNTERS_10_ALARMMODE_BMSK                     0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_ALARMMODE_COUNTERS_10_ALARMMODE_SHFT                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_VAL_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009608)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_VAL_RMSK                                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_VAL_COUNTERS_10_VAL_BMSK                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_10_VAL_COUNTERS_10_VAL_SHFT                                 0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_SRC_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009614)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_SRC_RMSK                                               0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_SRC_EXTEVENT_BMSK                                      0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_SRC_EXTEVENT_SHFT                                        0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_SRC_INTEVENT_BMSK                                       0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_SRC_INTEVENT_SHFT                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_ALARMMODE_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009618)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_ALARMMODE_RMSK                                           0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_ALARMMODE_COUNTERS_11_ALARMMODE_BMSK                     0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_ALARMMODE_COUNTERS_11_ALARMMODE_SHFT                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_VAL_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000961c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_VAL_RMSK                                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_VAL_COUNTERS_11_VAL_BMSK                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_11_VAL_COUNTERS_11_VAL_SHFT                                 0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_SRC_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009628)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_SRC_RMSK                                               0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_SRC_EXTEVENT_BMSK                                      0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_SRC_EXTEVENT_SHFT                                        0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_SRC_INTEVENT_BMSK                                       0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_SRC_INTEVENT_SHFT                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_ALARMMODE_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000962c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_ALARMMODE_RMSK                                           0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_ALARMMODE_COUNTERS_12_ALARMMODE_BMSK                     0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_ALARMMODE_COUNTERS_12_ALARMMODE_SHFT                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_VAL_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009630)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_VAL_RMSK                                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_VAL_COUNTERS_12_VAL_BMSK                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_12_VAL_COUNTERS_12_VAL_SHFT                                 0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_SRC_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000963c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_SRC_RMSK                                               0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_SRC_EXTEVENT_BMSK                                      0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_SRC_EXTEVENT_SHFT                                        0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_SRC_INTEVENT_BMSK                                       0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_SRC_INTEVENT_SHFT                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_ALARMMODE_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009640)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_ALARMMODE_RMSK                                           0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_ALARMMODE_COUNTERS_13_ALARMMODE_BMSK                     0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_ALARMMODE_COUNTERS_13_ALARMMODE_SHFT                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_VAL_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009644)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_VAL_RMSK                                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_VAL_COUNTERS_13_VAL_BMSK                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_13_VAL_COUNTERS_13_VAL_SHFT                                 0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_SRC_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009650)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_SRC_RMSK                                               0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_SRC_EXTEVENT_BMSK                                      0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_SRC_EXTEVENT_SHFT                                        0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_SRC_INTEVENT_BMSK                                       0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_SRC_INTEVENT_SHFT                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_ALARMMODE_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009654)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_ALARMMODE_RMSK                                           0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_ALARMMODE_COUNTERS_14_ALARMMODE_BMSK                     0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_ALARMMODE_COUNTERS_14_ALARMMODE_SHFT                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_VAL_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009658)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_VAL_RMSK                                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_VAL_COUNTERS_14_VAL_BMSK                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_14_VAL_COUNTERS_14_VAL_SHFT                                 0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_SRC_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009664)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_SRC_RMSK                                               0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_SRC_EXTEVENT_BMSK                                      0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_SRC_EXTEVENT_SHFT                                        0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_SRC_INTEVENT_BMSK                                       0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_SRC_INTEVENT_SHFT                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_ALARMMODE_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009668)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_ALARMMODE_RMSK                                           0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_ALARMMODE_COUNTERS_15_ALARMMODE_BMSK                     0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_ALARMMODE_COUNTERS_15_ALARMMODE_SHFT                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_VAL_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000966c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_VAL_RMSK                                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_VAL_COUNTERS_15_VAL_BMSK                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_15_VAL_COUNTERS_15_VAL_SHFT                                 0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_SRC_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009678)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_SRC_RMSK                                               0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_SRC_EXTEVENT_BMSK                                      0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_SRC_EXTEVENT_SHFT                                        0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_SRC_INTEVENT_BMSK                                       0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_SRC_INTEVENT_SHFT                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_ALARMMODE_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000967c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_ALARMMODE_RMSK                                           0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_ALARMMODE_COUNTERS_16_ALARMMODE_BMSK                     0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_ALARMMODE_COUNTERS_16_ALARMMODE_SHFT                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_VAL_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009680)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_VAL_RMSK                                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_VAL_COUNTERS_16_VAL_BMSK                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_16_VAL_COUNTERS_16_VAL_SHFT                                 0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_SRC_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000968c)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_SRC_RMSK                                               0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_SRC_EXTEVENT_BMSK                                      0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_SRC_EXTEVENT_SHFT                                        0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_SRC_INTEVENT_BMSK                                       0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_SRC_INTEVENT_SHFT                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_ALARMMODE_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009690)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_ALARMMODE_RMSK                                           0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_ALARMMODE_COUNTERS_17_ALARMMODE_BMSK                     0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_ALARMMODE_COUNTERS_17_ALARMMODE_SHFT                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_VAL_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x00009694)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_VAL_RMSK                                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_VAL_COUNTERS_17_VAL_BMSK                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_17_VAL_COUNTERS_17_VAL_SHFT                                 0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_SRC_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000096a0)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_SRC_RMSK                                               0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_SRC_EXTEVENT_BMSK                                      0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_SRC_EXTEVENT_SHFT                                        0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_SRC_INTEVENT_BMSK                                       0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_SRC_INTEVENT_SHFT                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_ALARMMODE_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000096a4)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_ALARMMODE_RMSK                                           0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_ALARMMODE_COUNTERS_18_ALARMMODE_BMSK                     0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_ALARMMODE_COUNTERS_18_ALARMMODE_SHFT                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_VAL_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000096a8)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_VAL_RMSK                                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_VAL_COUNTERS_18_VAL_BMSK                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_18_VAL_COUNTERS_18_VAL_SHFT                                 0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_SRC_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000096b4)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_SRC_RMSK                                               0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_SRC_EXTEVENT_BMSK                                      0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_SRC_EXTEVENT_SHFT                                        0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_SRC_INTEVENT_BMSK                                       0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_SRC_INTEVENT_SHFT                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_ALARMMODE_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000096b8)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_ALARMMODE_RMSK                                           0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_ALARMMODE_COUNTERS_19_ALARMMODE_BMSK                     0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_ALARMMODE_COUNTERS_19_ALARMMODE_SHFT                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_VAL_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000096bc)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_VAL_RMSK                                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_VAL_COUNTERS_19_VAL_BMSK                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_19_VAL_COUNTERS_19_VAL_SHFT                                 0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_SRC_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000096c8)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_SRC_RMSK                                               0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_SRC_EXTEVENT_BMSK                                      0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_SRC_EXTEVENT_SHFT                                        0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_SRC_INTEVENT_BMSK                                       0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_SRC_INTEVENT_SHFT                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_ALARMMODE_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000096cc)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_ALARMMODE_RMSK                                           0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_ALARMMODE_COUNTERS_20_ALARMMODE_BMSK                     0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_ALARMMODE_COUNTERS_20_ALARMMODE_SHFT                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_VAL_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000096d0)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_VAL_RMSK                                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_VAL_COUNTERS_20_VAL_BMSK                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_20_VAL_COUNTERS_20_VAL_SHFT                                 0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_SRC_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000096dc)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_SRC_RMSK                                               0x1ff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_SRC_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_SRC_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_SRC_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_SRC_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_SRC_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_SRC_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_SRC_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_SRC_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_SRC_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_SRC_EXTEVENT_BMSK                                      0x1e0
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_SRC_EXTEVENT_SHFT                                        0x5
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_SRC_INTEVENT_BMSK                                       0x1f
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_SRC_INTEVENT_SHFT                                        0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_ALARMMODE_ADDR                                    (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000096e0)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_ALARMMODE_RMSK                                           0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_ALARMMODE_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_ALARMMODE_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_ALARMMODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_ALARMMODE_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_ALARMMODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_ALARMMODE_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_ALARMMODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_ALARMMODE_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_ALARMMODE_IN)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_ALARMMODE_COUNTERS_21_ALARMMODE_BMSK                     0x3
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_ALARMMODE_COUNTERS_21_ALARMMODE_SHFT                     0x0

#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_VAL_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x000096e4)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_VAL_RMSK                                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_VAL_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_VAL_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_VAL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_VAL_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_VAL_COUNTERS_21_VAL_BMSK                              0xffff
#define HWIO_APCS_CCI_GLADIATOR_MAIN_PACKETPROBE_COUNTERS_21_VAL_COUNTERS_21_VAL_SHFT                                 0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_COREID_ADDR                                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c000)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_COREID_RMSK                                                     0xffffffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_COREID_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_COREID_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_COREID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_COREID_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_COREID_CORECHECKSUM_BMSK                                        0xffffff00
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_COREID_CORECHECKSUM_SHFT                                               0x8
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_COREID_CORETYPEID_BMSK                                                0xff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_COREID_CORETYPEID_SHFT                                                 0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_REVISIONID_ADDR                                                 (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c004)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_REVISIONID_RMSK                                                 0xffffffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_REVISIONID_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_REVISIONID_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_REVISIONID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_REVISIONID_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_REVISIONID_QNOCID_BMSK                                          0xffffff00
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_REVISIONID_QNOCID_SHFT                                                 0x8
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_REVISIONID_USERID_BMSK                                                0xff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_ID_REVISIONID_USERID_SHFT                                                 0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_ADDR                                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c008)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_RMSK                                                             0xff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_FILTBYTEALWAYSCHAINABLEEN_BMSK                                   0x80
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_FILTBYTEALWAYSCHAINABLEEN_SHFT                                    0x7
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_INTRUSIVEMODE_BMSK                                               0x40
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_INTRUSIVEMODE_SHFT                                                0x6
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_STATCONDDUMP_BMSK                                                0x20
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_STATCONDDUMP_SHFT                                                 0x5
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_ALARMEN_BMSK                                                     0x10
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_ALARMEN_SHFT                                                      0x4
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_STATEN_BMSK                                                       0x8
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_STATEN_SHFT                                                       0x3
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_PAYLOADEN_BMSK                                                    0x4
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_PAYLOADEN_SHFT                                                    0x2
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_TRACEEN_BMSK                                                      0x2
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_TRACEEN_SHFT                                                      0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_ERREN_BMSK                                                        0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_MAINCTL_ERREN_SHFT                                                        0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_CFGCTL_ADDR                                                        (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c00c)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_CFGCTL_RMSK                                                               0x3
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_CFGCTL_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_CFGCTL_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_CFGCTL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_CFGCTL_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_CFGCTL_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_CFGCTL_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_CFGCTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_CFGCTL_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_CFGCTL_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_CFGCTL_ACTIVE_BMSK                                                        0x2
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_CFGCTL_ACTIVE_SHFT                                                        0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_CFGCTL_GLOBALEN_BMSK                                                      0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_CFGCTL_GLOBALEN_SHFT                                                      0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERLUT_ADDR                                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c014)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERLUT_RMSK                                                            0xf
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERLUT_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERLUT_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERLUT_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERLUT_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERLUT_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERLUT_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERLUT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERLUT_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERLUT_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERLUT_FILTERLUT_BMSK                                                  0xf
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERLUT_FILTERLUT_SHFT                                                  0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMEN_ADDR                                                  (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c018)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMEN_RMSK                                                         0x7
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMEN_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMEN_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMEN_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMEN_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMEN_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMEN_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMEN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMEN_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMEN_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMEN_TRACEALARMEN_BMSK                                            0x7
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMEN_TRACEALARMEN_SHFT                                            0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMSTATUS_ADDR                                              (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c01c)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMSTATUS_RMSK                                                     0x7
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMSTATUS_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMSTATUS_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMSTATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMSTATUS_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMSTATUS_TRACEALARMSTATUS_BMSK                                    0x7
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMSTATUS_TRACEALARMSTATUS_SHFT                                    0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMCLR_ADDR                                                 (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c020)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMCLR_RMSK                                                        0x7
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMCLR_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMCLR_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMCLR_TRACEALARMCLR_BMSK                                          0x7
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_TRACEALARMCLR_TRACEALARMCLR_SHFT                                          0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_ADDR                                         (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c044)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_RMSK                                         0x7fffffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_FILTERS_0_ROUTEIDBASE_BMSK                   0x7fffffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_FILTERS_0_ROUTEIDBASE_SHFT                          0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_ADDR                                         (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c048)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_RMSK                                         0x7fffffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_FILTERS_0_ROUTEIDMASK_BMSK                   0x7fffffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_FILTERS_0_ROUTEIDMASK_SHFT                          0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_ADDR                                        (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c04c)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_RMSK                                        0xffffffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_FILTERS_0_ADDRBASE_LOW_BMSK                 0xffffffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_FILTERS_0_ADDRBASE_LOW_SHFT                        0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_ADDR                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c050)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_RMSK                                              0xf
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_FILTERS_0_ADDRBASE_HIGH_BMSK                      0xf
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_FILTERS_0_ADDRBASE_HIGH_SHFT                      0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c054)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_RMSK                                                0x3f
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_FILTERS_0_WINDOWSIZE_BMSK                           0x3f
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_FILTERS_0_WINDOWSIZE_SHFT                            0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_ADDR                                        (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c058)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_RMSK                                               0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_FILTERS_0_SECURITYBASE_BMSK                        0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_FILTERS_0_SECURITYBASE_SHFT                        0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_ADDR                                        (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c05c)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_RMSK                                               0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_FILTERS_0_SECURITYMASK_BMSK                        0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_FILTERS_0_SECURITYMASK_SHFT                        0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_ADDR                                              (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c060)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_RMSK                                                     0xf
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_URGEN_BMSK                                               0x8
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_URGEN_SHFT                                               0x3
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_LOCKEN_BMSK                                              0x4
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_LOCKEN_SHFT                                              0x2
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_WREN_BMSK                                                0x2
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_WREN_SHFT                                                0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_RDEN_BMSK                                                0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_RDEN_SHFT                                                0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_STATUS_ADDR                                              (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c064)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_STATUS_RMSK                                                     0x3
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_STATUS_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_STATUS_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_STATUS_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_STATUS_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_STATUS_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_STATUS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_STATUS_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_STATUS_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_STATUS_RSPEN_BMSK                                               0x2
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_STATUS_RSPEN_SHFT                                               0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_STATUS_REQEN_BMSK                                               0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_STATUS_REQEN_SHFT                                               0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_ADDR                                              (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c068)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_RMSK                                                     0xf
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_FILTERS_0_LENGTH_BMSK                                    0xf
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_FILTERS_0_LENGTH_SHFT                                    0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_ADDR                                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c070)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_RMSK                                               0x7ffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_FILTERS_0_USERBASE_BMSK                            0x7ffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_FILTERS_0_USERBASE_SHFT                                0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_ADDR                                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c074)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_RMSK                                               0x7ffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_FILTERS_0_USERMASK_BMSK                            0x7ffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_FILTERS_0_USERMASK_SHFT                                0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_ADDR                                         (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c080)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_RMSK                                         0x7fffffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_FILTERS_1_ROUTEIDBASE_BMSK                   0x7fffffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_FILTERS_1_ROUTEIDBASE_SHFT                          0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_ADDR                                         (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c084)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_RMSK                                         0x7fffffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_FILTERS_1_ROUTEIDMASK_BMSK                   0x7fffffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_FILTERS_1_ROUTEIDMASK_SHFT                          0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_ADDR                                        (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c088)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_RMSK                                        0xffffffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_FILTERS_1_ADDRBASE_LOW_BMSK                 0xffffffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_FILTERS_1_ADDRBASE_LOW_SHFT                        0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_ADDR                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c08c)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_RMSK                                              0xf
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_FILTERS_1_ADDRBASE_HIGH_BMSK                      0xf
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_FILTERS_1_ADDRBASE_HIGH_SHFT                      0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c090)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_RMSK                                                0x3f
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_FILTERS_1_WINDOWSIZE_BMSK                           0x3f
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_FILTERS_1_WINDOWSIZE_SHFT                            0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_ADDR                                        (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c094)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_RMSK                                               0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_FILTERS_1_SECURITYBASE_BMSK                        0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_FILTERS_1_SECURITYBASE_SHFT                        0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_ADDR                                        (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c098)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_RMSK                                               0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_FILTERS_1_SECURITYMASK_BMSK                        0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_FILTERS_1_SECURITYMASK_SHFT                        0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_ADDR                                              (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c09c)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_RMSK                                                     0xf
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_URGEN_BMSK                                               0x8
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_URGEN_SHFT                                               0x3
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_LOCKEN_BMSK                                              0x4
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_LOCKEN_SHFT                                              0x2
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_WREN_BMSK                                                0x2
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_WREN_SHFT                                                0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_RDEN_BMSK                                                0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_RDEN_SHFT                                                0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_STATUS_ADDR                                              (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c0a0)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_STATUS_RMSK                                                     0x3
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_STATUS_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_STATUS_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_STATUS_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_STATUS_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_STATUS_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_STATUS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_STATUS_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_STATUS_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_STATUS_RSPEN_BMSK                                               0x2
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_STATUS_RSPEN_SHFT                                               0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_STATUS_REQEN_BMSK                                               0x1
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_STATUS_REQEN_SHFT                                               0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_ADDR                                              (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c0a4)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_RMSK                                                     0xf
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_FILTERS_1_LENGTH_BMSK                                    0xf
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_FILTERS_1_LENGTH_SHFT                                    0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_ADDR                                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c0ac)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_RMSK                                               0x7ffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_FILTERS_1_USERBASE_BMSK                            0x7ffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_FILTERS_1_USERBASE_SHFT                                0x0

#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_ADDR                                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c0b0)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_RMSK                                               0x7ffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_IN          \
        in_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_ADDR)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_ADDR, m)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_ADDR,v)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_ADDR,m,v,HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_IN)
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_FILTERS_1_USERMASK_BMSK                            0x7ffff
#define HWIO_APCS_CCI_M1REQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_FILTERS_1_USERMASK_SHFT                                0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_COREID_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c400)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_COREID_RMSK                                          0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_COREID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_COREID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_COREID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_COREID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_COREID_CORECHECKSUM_BMSK                             0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_COREID_CORECHECKSUM_SHFT                                    0x8
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_COREID_CORETYPEID_BMSK                                     0xff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_COREID_CORETYPEID_SHFT                                      0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_REVISIONID_ADDR                                      (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c404)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_REVISIONID_RMSK                                      0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_REVISIONID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_REVISIONID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_REVISIONID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_REVISIONID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_REVISIONID_QNOCID_BMSK                               0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_REVISIONID_QNOCID_SHFT                                      0x8
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_REVISIONID_USERID_BMSK                                     0xff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ID_REVISIONID_USERID_SHFT                                      0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBID_ADDR                                              (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c408)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBID_RMSK                                                    0x7f
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBID_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBID_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBID_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBID_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBID_IN)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBID_ATBID_BMSK                                              0x7f
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBID_ATBID_SHFT                                               0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBEN_ADDR                                              (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c40c)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBEN_RMSK                                                     0x1
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBEN_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBEN_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBEN_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBEN_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBEN_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBEN_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBEN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBEN_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBEN_IN)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBEN_ATBEN_BMSK                                               0x1
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_ATBENDPOINT_ATBEN_ATBEN_SHFT                                               0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_COREID_ADDR                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c480)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_COREID_RMSK                                       0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_COREID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_COREID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_COREID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_COREID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_COREID_CORECHECKSUM_BMSK                          0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_COREID_CORECHECKSUM_SHFT                                 0x8
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_COREID_CORETYPEID_BMSK                                  0xff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_COREID_CORETYPEID_SHFT                                   0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_REVISIONID_ADDR                                   (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c484)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_REVISIONID_RMSK                                   0xffffffff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_REVISIONID_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_REVISIONID_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_REVISIONID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_REVISIONID_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_REVISIONID_QNOCID_BMSK                            0xffffff00
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_REVISIONID_QNOCID_SHFT                                   0x8
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_REVISIONID_USERID_BMSK                                  0xff
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ID_REVISIONID_USERID_SHFT                                   0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ASYNCPERIOD_ADDR                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c488)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ASYNCPERIOD_RMSK                                           0x1f
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ASYNCPERIOD_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ASYNCPERIOD_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ASYNCPERIOD_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ASYNCPERIOD_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ASYNCPERIOD_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ASYNCPERIOD_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ASYNCPERIOD_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ASYNCPERIOD_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ASYNCPERIOD_IN)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ASYNCPERIOD_ASYNCPERIOD_BMSK                               0x1f
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_ASYNCPERIOD_ASYNCPERIOD_SHFT                                0x0

#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_STPV2EN_ADDR                                         (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000c48c)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_STPV2EN_RMSK                                                0x1
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_STPV2EN_IN          \
        in_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_STPV2EN_ADDR)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_STPV2EN_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_STPV2EN_ADDR, m)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_STPV2EN_OUT(v)      \
        out_dword(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_STPV2EN_ADDR,v)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_STPV2EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_STPV2EN_ADDR,m,v,HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_STPV2EN_IN)
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_STPV2EN_STPV2EN_BMSK                                        0x1
#define HWIO_APCS_CCI_GLADIATOR_TRACE_MAIN_STPV2CONVERTER_STPV2EN_STPV2EN_SHFT                                        0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_COREID_ADDR                                                   (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d800)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_COREID_RMSK                                                   0xffffffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_COREID_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_COREID_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_COREID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_COREID_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_COREID_CORECHECKSUM_BMSK                                      0xffffff00
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_COREID_CORECHECKSUM_SHFT                                             0x8
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_COREID_CORETYPEID_BMSK                                              0xff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_COREID_CORETYPEID_SHFT                                               0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_REVISIONID_ADDR                                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d804)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_REVISIONID_RMSK                                               0xffffffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_REVISIONID_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_REVISIONID_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_REVISIONID_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_REVISIONID_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_REVISIONID_QNOCID_BMSK                                        0xffffff00
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_REVISIONID_QNOCID_SHFT                                               0x8
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_REVISIONID_USERID_BMSK                                              0xff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_ID_REVISIONID_USERID_SHFT                                               0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_ADDR                                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d808)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_RMSK                                                           0xff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_FILTBYTEALWAYSCHAINABLEEN_BMSK                                 0x80
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_FILTBYTEALWAYSCHAINABLEEN_SHFT                                  0x7
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_INTRUSIVEMODE_BMSK                                             0x40
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_INTRUSIVEMODE_SHFT                                              0x6
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_STATCONDDUMP_BMSK                                              0x20
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_STATCONDDUMP_SHFT                                               0x5
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_ALARMEN_BMSK                                                   0x10
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_ALARMEN_SHFT                                                    0x4
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_STATEN_BMSK                                                     0x8
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_STATEN_SHFT                                                     0x3
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_PAYLOADEN_BMSK                                                  0x4
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_PAYLOADEN_SHFT                                                  0x2
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_TRACEEN_BMSK                                                    0x2
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_TRACEEN_SHFT                                                    0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_ERREN_BMSK                                                      0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_MAINCTL_ERREN_SHFT                                                      0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_CFGCTL_ADDR                                                      (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d80c)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_CFGCTL_RMSK                                                             0x3
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_CFGCTL_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_CFGCTL_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_CFGCTL_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_CFGCTL_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_CFGCTL_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_CFGCTL_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_CFGCTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_CFGCTL_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_CFGCTL_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_CFGCTL_ACTIVE_BMSK                                                      0x2
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_CFGCTL_ACTIVE_SHFT                                                      0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_CFGCTL_GLOBALEN_BMSK                                                    0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_CFGCTL_GLOBALEN_SHFT                                                    0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERLUT_ADDR                                                   (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d814)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERLUT_RMSK                                                          0xf
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERLUT_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERLUT_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERLUT_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERLUT_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERLUT_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERLUT_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERLUT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERLUT_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERLUT_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERLUT_FILTERLUT_BMSK                                                0xf
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERLUT_FILTERLUT_SHFT                                                0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMEN_ADDR                                                (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d818)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMEN_RMSK                                                       0x7
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMEN_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMEN_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMEN_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMEN_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMEN_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMEN_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMEN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMEN_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMEN_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMEN_TRACEALARMEN_BMSK                                          0x7
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMEN_TRACEALARMEN_SHFT                                          0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMSTATUS_ADDR                                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d81c)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMSTATUS_RMSK                                                   0x7
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMSTATUS_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMSTATUS_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMSTATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMSTATUS_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMSTATUS_TRACEALARMSTATUS_BMSK                                  0x7
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMSTATUS_TRACEALARMSTATUS_SHFT                                  0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMCLR_ADDR                                               (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d820)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMCLR_RMSK                                                      0x7
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMCLR_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMCLR_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMCLR_TRACEALARMCLR_BMSK                                        0x7
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_TRACEALARMCLR_TRACEALARMCLR_SHFT                                        0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_ADDR                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d844)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_RMSK                                       0x7fffffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_FILTERS_0_ROUTEIDBASE_BMSK                 0x7fffffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDBASE_FILTERS_0_ROUTEIDBASE_SHFT                        0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_ADDR                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d848)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_RMSK                                       0x7fffffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_FILTERS_0_ROUTEIDMASK_BMSK                 0x7fffffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ROUTEIDMASK_FILTERS_0_ROUTEIDMASK_SHFT                        0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_ADDR                                      (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d84c)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_RMSK                                      0xffffffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_FILTERS_0_ADDRBASE_LOW_BMSK               0xffffffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_LOW_FILTERS_0_ADDRBASE_LOW_SHFT                      0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_ADDR                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d850)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_RMSK                                            0xf
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_FILTERS_0_ADDRBASE_HIGH_BMSK                    0xf
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_ADDRBASE_HIGH_FILTERS_0_ADDRBASE_HIGH_SHFT                    0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_ADDR                                        (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d854)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_RMSK                                              0x3f
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_FILTERS_0_WINDOWSIZE_BMSK                         0x3f
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_WINDOWSIZE_FILTERS_0_WINDOWSIZE_SHFT                          0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_ADDR                                      (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d858)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_RMSK                                             0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_FILTERS_0_SECURITYBASE_BMSK                      0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYBASE_FILTERS_0_SECURITYBASE_SHFT                      0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_ADDR                                      (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d85c)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_RMSK                                             0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_FILTERS_0_SECURITYMASK_BMSK                      0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_SECURITYMASK_FILTERS_0_SECURITYMASK_SHFT                      0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_ADDR                                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d860)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_RMSK                                                   0xf
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_URGEN_BMSK                                             0x8
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_URGEN_SHFT                                             0x3
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_LOCKEN_BMSK                                            0x4
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_LOCKEN_SHFT                                            0x2
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_WREN_BMSK                                              0x2
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_WREN_SHFT                                              0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_RDEN_BMSK                                              0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_OPCODE_RDEN_SHFT                                              0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_STATUS_ADDR                                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d864)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_STATUS_RMSK                                                   0x3
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_STATUS_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_STATUS_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_STATUS_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_STATUS_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_STATUS_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_STATUS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_STATUS_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_STATUS_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_STATUS_RSPEN_BMSK                                             0x2
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_STATUS_RSPEN_SHFT                                             0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_STATUS_REQEN_BMSK                                             0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_STATUS_REQEN_SHFT                                             0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_ADDR                                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d868)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_RMSK                                                   0xf
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_FILTERS_0_LENGTH_BMSK                                  0xf
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_LENGTH_FILTERS_0_LENGTH_SHFT                                  0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d870)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_RMSK                                             0x7ffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_FILTERS_0_USERBASE_BMSK                          0x7ffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERBASE_FILTERS_0_USERBASE_SHFT                              0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d874)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_RMSK                                             0x7ffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_FILTERS_0_USERMASK_BMSK                          0x7ffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_0_USERMASK_FILTERS_0_USERMASK_SHFT                              0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_ADDR                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d880)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_RMSK                                       0x7fffffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_FILTERS_1_ROUTEIDBASE_BMSK                 0x7fffffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDBASE_FILTERS_1_ROUTEIDBASE_SHFT                        0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_ADDR                                       (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d884)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_RMSK                                       0x7fffffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_FILTERS_1_ROUTEIDMASK_BMSK                 0x7fffffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ROUTEIDMASK_FILTERS_1_ROUTEIDMASK_SHFT                        0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_ADDR                                      (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d888)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_RMSK                                      0xffffffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_FILTERS_1_ADDRBASE_LOW_BMSK               0xffffffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_LOW_FILTERS_1_ADDRBASE_LOW_SHFT                      0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_ADDR                                     (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d88c)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_RMSK                                            0xf
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_FILTERS_1_ADDRBASE_HIGH_BMSK                    0xf
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_ADDRBASE_HIGH_FILTERS_1_ADDRBASE_HIGH_SHFT                    0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_ADDR                                        (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d890)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_RMSK                                              0x3f
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_FILTERS_1_WINDOWSIZE_BMSK                         0x3f
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_WINDOWSIZE_FILTERS_1_WINDOWSIZE_SHFT                          0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_ADDR                                      (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d894)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_RMSK                                             0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_FILTERS_1_SECURITYBASE_BMSK                      0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYBASE_FILTERS_1_SECURITYBASE_SHFT                      0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_ADDR                                      (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d898)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_RMSK                                             0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_FILTERS_1_SECURITYMASK_BMSK                      0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_SECURITYMASK_FILTERS_1_SECURITYMASK_SHFT                      0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_ADDR                                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d89c)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_RMSK                                                   0xf
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_URGEN_BMSK                                             0x8
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_URGEN_SHFT                                             0x3
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_LOCKEN_BMSK                                            0x4
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_LOCKEN_SHFT                                            0x2
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_WREN_BMSK                                              0x2
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_WREN_SHFT                                              0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_RDEN_BMSK                                              0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_OPCODE_RDEN_SHFT                                              0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_STATUS_ADDR                                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d8a0)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_STATUS_RMSK                                                   0x3
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_STATUS_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_STATUS_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_STATUS_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_STATUS_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_STATUS_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_STATUS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_STATUS_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_STATUS_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_STATUS_RSPEN_BMSK                                             0x2
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_STATUS_RSPEN_SHFT                                             0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_STATUS_REQEN_BMSK                                             0x1
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_STATUS_REQEN_SHFT                                             0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_ADDR                                            (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d8a4)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_RMSK                                                   0xf
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_FILTERS_1_LENGTH_BMSK                                  0xf
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_LENGTH_FILTERS_1_LENGTH_SHFT                                  0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d8ac)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_RMSK                                             0x7ffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_FILTERS_1_USERBASE_BMSK                          0x7ffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERBASE_FILTERS_1_USERBASE_SHFT                              0x0

#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_ADDR                                          (APCS_CCI_GLADIATOR_NOC_REG_BASE      + 0x0000d8b0)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_RMSK                                             0x7ffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_IN          \
        in_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_ADDR)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_INM(m)      \
        in_dword_masked(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_ADDR, m)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_OUT(v)      \
        out_dword(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_ADDR,v)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_ADDR,m,v,HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_IN)
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_FILTERS_1_USERMASK_BMSK                          0x7ffff
#define HWIO_APCS_CCI_SRVCREQPROBE_MAIN_PROBE_FILTERS_1_USERMASK_FILTERS_1_USERMASK_SHFT                              0x0


#endif /* __NOC_ERROR_HWIO_H__ */
