/**
    @file  spi_devcfg.h
    @brief device configuration implementation
 */
/*=============================================================================
            Copyright (c) 2017-2019 Qualcomm Technologies, Incorporated.
                              All rights reserved.
              Qualcomm Technologies, Confidential and Proprietary.
=============================================================================*/

#ifndef __SPI_DEVCFG_H__
#define __SPI_DEVCFG_H__

#include "comdef.h"

#define SPI_NUM_CHIP_SELECTS   4

typedef struct spi_plat_device_config
{
    uint8      *core_base_addr;
    uint32      core_offset;
    uint8       core_index;
	uint8       se_index;
    uint16      core_irq;
    boolean     polled_mode;
    uint16      min_data_length_for_dma;

    uint8       gpi_index;
    uint8       se_clock_dfs_index;
    uint32      se_clock_frequency;
    char       *se_clock_name;
    char      **common_clocks_name;

    void       *miso_encoding;
    void       *mosi_encoding;
    void       *clk_encoding;
    void       *cs_encoding[SPI_NUM_CHIP_SELECTS];

    void       *tx_dma_ring_base;
    void       *rx_dma_ring_base;
	uint32      qup_index;
	boolean    tpm;
/*
Control where the SPI master samples the incoming data.
To compensate for line delay, SPI master samples on negedge (protocol says posedge). 
Update below byte to TRUE/FALSE(to disable compensation) for SDM845, SDM670, SDM845, SM8150, SM6125, SM6150, SM7150 targets.
Update below byte to 2/0(to disable compensation) for SM8250, SM7250, SM6250.
*/
    uint8    miso_sampling_ctrl_set; 
} spi_plat_device_config;

#endif /*__SPI_DEVCFG_H__*/
