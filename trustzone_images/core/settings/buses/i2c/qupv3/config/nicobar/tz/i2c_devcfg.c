/**
    @file  i2c_devcfg.c
    @brief device configuration data
 */
/*=============================================================================
            Copyright (c) 2018-2019 Qualcomm Technologies, Incorporated.
                              All rights reserved.
              Qualcomm Technologies, Confidential and Proprietary.
=============================================================================*/

#include "i2c_config.h"


#define ENABLE_I2C_02

#define QUPV3_0_CORE_BASE_ADDRESS 0x04A00000 
#define QUPV3_1_CORE_BASE_ADDRESS 0x04C00000

#define TLMM_GPIO_CFG(gpio, func, dir, pull, drive) \
                          (((gpio) & 0x3FF) << 4  | \
                           ((func) & 0xF  ) << 0  | \
                           ((dir)  & 0x1  ) << 14 | \
                           ((pull) & 0x3  ) << 15 | \
                           ((drive)& 0xF  ) << 17)

#define TLMM_GPIO_INPUT     0x0
#define TLMM_GPIO_PULL_UP   0x3
#define TLMM_GPIO_2MA       0x0

typedef struct
{
  char     *gpio_str_name;
  uint32    config[3];
  uint32    gpio_id;
} i2c_plat_gpio_properties;

typedef struct num_cores { uint32 num; } num_cores;

const char *common_clocks_str_0 [] =
{
    "gcc_qupv3_wrap0_core_clk",
    "gcc_qupv3_wrap0_core_2x_clk",
    "gcc_qupv3_wrap_0_m_ahb_clk",
    "gcc_qupv3_wrap_0_s_ahb_clk",
    NULL,
};

const char *common_clocks_str_1 [] =
{
    "gcc_qupv3_wrap1_core_clk",
    "gcc_qupv3_wrap1_core_2x_clk",
    "gcc_qupv3_wrap_1_m_ahb_clk",
    "gcc_qupv3_wrap_1_s_ahb_clk",
    NULL,
};


const char *se_clocks_str_0 [] =
{
    "gcc_qupv3_wrap0_s0_clk",
    "gcc_qupv3_wrap0_s1_clk",
    "gcc_qupv3_wrap0_s2_clk",
    "gcc_qupv3_wrap0_s3_clk",
    "gcc_qupv3_wrap0_s4_clk",
    "gcc_qupv3_wrap0_s5_clk",
    
};

const char *se_clocks_str_1 [] =
{
    "gcc_qupv3_wrap1_s0_clk",
    "gcc_qupv3_wrap1_s1_clk",
    "gcc_qupv3_wrap1_s2_clk",
    "gcc_qupv3_wrap1_s3_clk",
    "gcc_qupv3_wrap1_s4_clk",
    "gcc_qupv3_wrap1_s5_clk",
};


plat_clock_config clk_cfg[] =
{
    // src-freq, speed, div, cycle, high, low
    {     19200,   100,   7,    26,   10,  11 },
    {     19200,   400,   2,    24,    5,  12 },
    {     19200,  1000,   1,    18,    3,   9 },
    {    100000,  8000,   2,     0,    3,   5 },
    {    120000, 12500,   1,     0,    8,   6 },
    {         0,     0,   0,     0,    0,   0 },
};

#ifdef  ENABLE_I2C_01
static i2c_plat_gpio_properties tz_i2c_01_sda = {"qup0_l0[0]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
static i2c_plat_gpio_properties tz_i2c_01_scl = {"qup0_l1[0]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
#endif

#ifdef  ENABLE_I2C_02
static i2c_plat_gpio_properties tz_i2c_02_sda = {"qup0_l0[1]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
static i2c_plat_gpio_properties tz_i2c_02_scl = {"qup0_l1[1]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
#endif

#ifdef  ENABLE_I2C_03
static i2c_plat_gpio_properties tz_i2c_03_sda = {"qup0_l0[2]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
static i2c_plat_gpio_properties tz_i2c_03_scl = {"qup0_l1[2]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
#endif

#ifdef  ENABLE_I2C_04
static i2c_plat_gpio_properties tz_i2c_04_sda = {"qup0_l0[3]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
static i2c_plat_gpio_properties tz_i2c_04_scl = {"qup0_l1[3]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
#endif

#ifdef  ENABLE_I2C_05
static i2c_plat_gpio_properties tz_i2c_05_sda = {"qup0_l0[4]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
static i2c_plat_gpio_properties tz_i2c_05_scl = {"qup0_l1[4]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
#endif

#ifdef  ENABLE_I2C_07
static i2c_plat_gpio_properties tz_i2c_07_sda = {"qup1_l0[0]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
static i2c_plat_gpio_properties tz_i2c_07_scl = {"qup1_l1[0]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
#endif

#ifdef  ENABLE_I2C_08
static i2c_plat_gpio_properties tz_i2c_08_sda = {"qup1_l0[1]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
static i2c_plat_gpio_properties tz_i2c_08_scl = {"qup1_l1[1]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
#endif

#ifdef  ENABLE_I2C_09
static i2c_plat_gpio_properties tz_i2c_09_sda = {"qup1_l0[2]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
static i2c_plat_gpio_properties tz_i2c_09_scl = {"qup1_l1[2]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
#endif

#ifdef  ENABLE_I2C_10
static i2c_plat_gpio_properties tz_i2c_10_sda = {"qup1_l0[3]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
static i2c_plat_gpio_properties tz_i2c_10_scl = {"qup1_l1[3]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
#endif

#ifdef  ENABLE_I2C_11
static i2c_plat_gpio_properties tz_i2c_11_sda = {"qup1_l0[4]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
static i2c_plat_gpio_properties tz_i2c_11_scl = {"qup1_l1[4]", { TLMM_GPIO_INPUT, TLMM_GPIO_PULL_UP, TLMM_GPIO_2MA }, 00 };
#endif

#define NUM_I2C_CORES_IN_USE (sizeof(i2c_device_config)/sizeof(plat_device_config))

plat_device_config i2c_device_config[] =
{
#ifdef ENABLE_I2C_01
		.core_base_addr              = (uint8 *) QUPV3_0_CORE_BASE_ADDRESS,
        .core_offset                 = 0x00080000,
        .qupv3_instance              = 0,
        .core_index                  = 1,
        .se_index                    = 0,
        .core_irq                    = 634,
        .polled_mode                 = TRUE,
        .min_data_length_for_dma     = 0,

        .gpi_index                   = 0xFF,

        .sda_encoding                = (void*)&tz_i2c_01_sda,
        .scl_encoding                = (void*)&tz_i2c_01_scl,

        .tcsr_base_addr              = (uint8 *) 0x00300000,
        .tcsr_reg_offset             = 0x00000000,
        .tcsr_reg_value              = 0x00000000,

        .common_clocks               = (uint8 **) common_clocks_str_0,
        .se_clock                    = (uint8 **) (se_clocks_str_0 + 0),
        .se_src_clock                = NULL,
        .resource_votes              = 0,

        .clock_config                = clk_cfg,
#endif
#ifdef ENABLE_I2C_02
    {
        .core_base_addr              = (uint8 *) QUPV3_0_CORE_BASE_ADDRESS,
        .core_offset                 = 0x00084000,
        .qupv3_instance              = 0,
        .core_index                  = 2,
        .se_index                    = 1,
        .core_irq                    = 634,
        .polled_mode                 = TRUE,
        .min_data_length_for_dma     = 0,

        .gpi_index                   = 5,

        .sda_encoding                = (void*)&tz_i2c_02_sda,
        .scl_encoding                = (void*)&tz_i2c_02_scl,

        .tcsr_base_addr              = (uint8 *) 0x00300000,
        .tcsr_reg_offset             = 0x00000000,
        .tcsr_reg_value              = 0x00000000,

        .common_clocks               = (uint8 **) common_clocks_str_0,
        .se_clock                    = (uint8 **) (se_clocks_str_0 + 1),
        .se_src_clock                = NULL,
        .resource_votes              = 0,

        .clock_config                = clk_cfg,
    },
#endif
#ifdef ENABLE_I2C_03
    {
        .core_base_addr              = (uint8 *) QUPV3_0_CORE_BASE_ADDRESS,
        .core_offset                 = 0x00088000,
        .qupv3_instance              = 0,
        .core_index                  = 3,
        .se_index                    = 2,
        .core_irq                    = 635, 
        .polled_mode                 = TRUE,
        .min_data_length_for_dma     = 0,

        .gpi_index                   = 0xFF,

        .sda_encoding                = (void*)&tz_i2c_03_sda,
        .scl_encoding                = (void*)&tz_i2c_03_scl,

        .tcsr_base_addr              = (uint8 *) 0x00300000,
        .tcsr_reg_offset             = 0x00000000,
        .tcsr_reg_value              = 0x00000000,

        .common_clocks               = (uint8 **) common_clocks_str_0,
        .se_clock                    = (uint8 **) (se_clocks_str_0 + 2),
        .se_src_clock                = NULL,
        .resource_votes              = 0,

        .clock_config                = clk_cfg,
    },
#endif
#ifdef ENABLE_I2C_04
    {
        .core_base_addr              = (uint8 *) QUPV3_0_CORE_BASE_ADDRESS,
        .core_offset                 = 0x0008C000,
        .qupv3_instance              = 0,
        .core_index                  = 4,
        .se_index                    = 3,
        .core_irq                    = 636, 
        .polled_mode                 = TRUE,
        .min_data_length_for_dma     = 0,

        .gpi_index                   = 0xFF, 
        .sda_encoding                = (void*)&tz_i2c_04_sda,
        .scl_encoding                = (void*)&tz_i2c_04_scl,

        .tcsr_base_addr              = (uint8 *) 0x00300000,
        .tcsr_reg_offset             = 0x00000000,
        .tcsr_reg_value              = 0x00000000,

        .common_clocks               = (uint8 **) common_clocks_str_0,
        .se_clock                    = (uint8 **) (se_clocks_str_0 + 3),
        .se_src_clock                = NULL,
        .resource_votes              = 0,

        .clock_config                = clk_cfg,
    },
#endif
#ifdef ENABLE_I2C_05
    {
        .core_base_addr              = (uint8 *) QUPV3_0_CORE_BASE_ADDRESS,
        .core_offset                 = 0x00090000,
        .qupv3_instance              = 0,
        .core_index                  = 5,
        .se_index                    = 4,
        .core_irq                    = 637, 
        .polled_mode                 = TRUE,
        .min_data_length_for_dma     = 0,

        .gpi_index                   = 0xFF,

        .sda_encoding                = (void*)&tz_i2c_05_sda,
        .scl_encoding                = (void*)&tz_i2c_05_scl,

        .tcsr_base_addr              = (uint8 *) 0x01FC0000,
        .tcsr_reg_offset             = 0x00000000,
        .tcsr_reg_value              = 0x00000000,

        .common_clocks               = (uint8 **) common_clocks_str_0,
        .se_clock                    = (uint8 **) (se_clocks_str_0 + 4),
        .se_src_clock                = NULL,
        .resource_votes              = 0,

        .clock_config                = clk_cfg,
    },
#endif
#ifdef ENABLE_I2C_06
	{
		This SE is not present in nicobar according to IPCAT
	}
#endif
#ifdef ENABLE_I2C_07
    {
        .core_base_addr              = (uint8 *) QUPV3_1_CORE_BASE_ADDRESS,
        .core_offset                 = 0x00080000,
        .qupv3_instance              = 1,
        .core_index                  = 7,
        .se_index                    = 0,
        .core_irq                    = 638, 
        .polled_mode                 = TRUE,
        .min_data_length_for_dma     = 0,

        .gpi_index                   = 0xFF,

        .sda_encoding                = (void*)&tz_i2c_07_sda,
        .scl_encoding                = (void*)&tz_i2c_07_scl,

        .tcsr_base_addr              = (uint8 *) 0x00300000,
        .tcsr_reg_offset             = 0x00000000,
        .tcsr_reg_value              = 0x00000000,

        .common_clocks               = (uint8 **) common_clocks_str_1,
        .se_clock                    = (uint8 **) (se_clocks_str_1 + 0),
        .se_src_clock                = NULL,
        .resource_votes              = 0,

        .clock_config                = clk_cfg,
    },
#endif

#ifdef ENABLE_I2C_08
    {
        .core_base_addr              = (uint8 *) QUPV3_1_CORE_BASE_ADDRESS,
        .core_offset                 = 0x00084000,
        .qupv3_instance              = 1,
        .core_index                  = 8,
        .se_index                    = 1,
        .core_irq                    = 639, 
        .polled_mode                 = TRUE,
        .min_data_length_for_dma     = 0,

        .gpi_index                   = 0xFF,

        .sda_encoding                = (void*)&tz_i2c_08_sda,
        .scl_encoding                = (void*)&tz_i2c_08_scl,

        .tcsr_base_addr              = (uint8 *) 0x00300000,
        .tcsr_reg_offset             = 0x00000000,
        .tcsr_reg_value              = 0x00000000,

        .common_clocks               = (uint8 **) common_clocks_str_1,
        .se_clock                    = (uint8 **) (se_clocks_str_1 + 1),
        .se_src_clock                = NULL,
        .resource_votes              = 0,

        .clock_config                = clk_cfg,
    },
#endif

#ifdef ENABLE_I2C_09
    {
        .core_base_addr              = (uint8 *) QUPV3_1_CORE_BASE_ADDRESS,
        .core_offset                 = 0x00088000,
        .qupv3_instance              = 1,
        .core_index                  = 9,
        .se_index                    = 2,
        .core_irq                    = 640, 
        .polled_mode                 = TRUE,
        .min_data_length_for_dma     = 0,

        .gpi_index                   = 0xFF,

        .sda_encoding                = (void*)&tz_i2c_09_sda,
        .scl_encoding                = (void*)&tz_i2c_09_scl,

        .tcsr_base_addr              = (uint8 *) 0x00300000,
        .tcsr_reg_offset             = 0x00000000,
        .tcsr_reg_value              = 0x00000000,

        .common_clocks               = (uint8 **) common_clocks_str_1,
        .se_clock                    = (uint8 **) (se_clocks_str_1 + 2),
        .se_src_clock                = NULL,
        .resource_votes              = 0,

        .clock_config                = clk_cfg,
    },
#endif

#ifdef ENABLE_I2C_10
    {
        .core_base_addr              = (uint8 *) QUPV3_1_CORE_BASE_ADDRESS,
        .core_offset                 = 0x0008C000,
        .qupv3_instance              = 1,
        .core_index                  = 10,
        .se_index                    = 3,
        .core_irq                    = 640, 
        .polled_mode                 = TRUE,
        .min_data_length_for_dma     = 0,

        .gpi_index                   = 0xFF,

        .sda_encoding                = (void*)&tz_i2c_10_sda,
        .scl_encoding                = (void*)&tz_i2c_10_scl,

        .tcsr_base_addr              = (uint8 *) 0x00300000,
        .tcsr_reg_offset             = 0x00000000,
        .tcsr_reg_value              = 0x00000000,

        .common_clocks               = (uint8 **) common_clocks_str_1,
        .se_clock                    = (uint8 **) (se_clocks_str_1 + 3),
        .se_src_clock                = NULL,
        .resource_votes              = 0,

        .clock_config                = clk_cfg,
    },
#endif

#ifdef ENABLE_I2C_11
    {
        .core_base_addr              = (uint8 *) QUPV3_1_CORE_BASE_ADDRESS,
        .core_offset                 = 0x0009000,
        .qupv3_instance              = 1,
        .core_index                  = 11,
        .se_index                    = 4,
        .core_irq                    = 640, 
        .polled_mode                 = TRUE,
        .min_data_length_for_dma     = 0,

        .gpi_index                   = 0xFF,

        .sda_encoding                = (void*)&tz_i2c_11_sda,
        .scl_encoding                = (void*)&tz_i2c_11_scl,

        .tcsr_base_addr              = (uint8 *) 0x00300000,
        .tcsr_reg_offset             = 0x00000000,
        .tcsr_reg_value              = 0x00000000,

        .common_clocks               = (uint8 **) common_clocks_str_1,
        .se_clock                    = (uint8 **) (se_clocks_str_1 + 4),
        .se_src_clock                = NULL,
        .resource_votes              = 0,

        .clock_config                = clk_cfg,
    },
#endif
#ifdef ENABLE_I2C_12
	{
		This SE is not present in nicobar according to IPCAT
	}
#endif
};
num_cores num_i2c_cores = { NUM_I2C_CORES_IN_USE };
