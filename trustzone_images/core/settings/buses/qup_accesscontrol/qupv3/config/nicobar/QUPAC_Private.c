//===========================================================================
//
// FILE:         QUPAC_Private.c
//
// DESCRIPTION:  This file lists hardware configuration for all QUPS
//
//===========================================================================
//
//                             Edit History
//
// $Header: //components/rel/core.tz/2.1.1/settings/buses/qup_accesscontrol/qupv3/config/nicobar/QUPAC_Private.c#1 $
//
// when       who     what, where, why
// 01/29/19   DPK     Added master side private resource configuration.
// 12/12/18   DPK     Initial revision for Nicobar
//
//===========================================================================
//             Copyright (c) 2018-2019 QUALCOMM Technologies, Incorporated.
//                    All Rights Reserved.
//                QUALCOMM Confidential & Proprietary
//===========================================================================
#include "QupACCommonIds.h" 

const QUPv3_se_resource_type qupv3_se_hw[] =
{
                                                                   // = L0, L1, L2, L3 =
  { QUPV3_0_SE0, (uint8*)0x04a80000,  0, "gcc_qupv3_wrap0_s0_clk" }, // {  0,  1,  2,  3 } 
  { QUPV3_0_SE1, (uint8*)0x04a84000,  1, "gcc_qupv3_wrap0_s1_clk" }, // {  4,  5         }
  { QUPV3_0_SE2, (uint8*)0x04a88000,  2, "gcc_qupv3_wrap0_s2_clk" }, // {  6,  7,  8,  9 }
  { QUPV3_0_SE3, (uint8*)0x04a8c000,  3, "gcc_qupv3_wrap0_s3_clk" }, // { 14, 15, 14, 15 }
  { QUPV3_0_SE4, (uint8*)0x04a90000,  4, "gcc_qupv3_wrap0_s4_clk" }, // { 16, 17, 16, 17 }
  { QUPV3_1_SE0, (uint8*)0x04c80000,  5, "gcc_qupv3_wrap1_s0_clk" }, // { 22, 23, 24, 25 }
  { QUPV3_1_SE1, (uint8*)0x04c84000,  6, "gcc_qupv3_wrap1_s1_clk" }, // { 30, 31, 32, 33 }
  { QUPV3_1_SE2, (uint8*)0x04c88000,  7, "gcc_qupv3_wrap1_s2_clk" }, // { 28, 29         }
  { QUPV3_1_SE3, (uint8*)0x04c8c000,  8, "gcc_qupv3_wrap1_s3_clk" }, // { 18, 19, 20, 21 }
  { QUPV3_1_SE4, (uint8*)0x04c90000,  9, "gcc_qupv3_wrap1_s4_clk" }, // { 10, 11, 12, 13 }  
};

const uint32 qupv3_se_hw_size = sizeof(qupv3_se_hw)/sizeof(qupv3_se_hw[0]);

const QUPv3_common_resource_type qupv3_common_hw[] =
{
  { (uint8*)0x4AC0000, "QUP_0C", "QUP_0CS" },
  { (uint8*)0x4CC0000, "QUP_1C", "QUP_1CS" },
};

const uint32 qupv3_common_hw_size = sizeof(qupv3_common_hw)/sizeof(qupv3_common_hw[0]);

const char *qupv3_0_clocks_arr[] =
{
  "gcc_qupv3_wrap0_core_2x_clk",
  "gcc_qupv3_wrap0_core_clk",
  "gcc_qupv3_wrap_0_m_ahb_clk",
  "gcc_qupv3_wrap_0_s_ahb_clk",
};

const uint32 qupv3_0_clocks_arr_size = sizeof(qupv3_0_clocks_arr)/sizeof(qupv3_0_clocks_arr[0]);

const char *qupv3_1_clocks_arr[] =
{
  "gcc_qupv3_wrap1_core_2x_clk",
  "gcc_qupv3_wrap1_core_clk",
  "gcc_qupv3_wrap_1_m_ahb_clk",
  "gcc_qupv3_wrap_1_s_ahb_clk",
};
const uint32 qupv3_1_clocks_arr_size = sizeof(qupv3_1_clocks_arr)/sizeof(qupv3_1_clocks_arr[0]);

const QUPv3_private_resources qupv3_private_arr[] = 
{
  { "SENSOR_PRAM", AC_LPASS},   // Need this SENSOR PRAM config only when it is accessed from the QUPs outside LPASS/SSC.
};
const uint32 qupv3_private_arr_size = sizeof(qupv3_private_arr)/sizeof(qupv3_private_arr[0]);