//===========================================================================
//
// FILE:         QUPAC_Access.xml
//
// DESCRIPTION:  This file lists access permission for all QUPS
//
//===========================================================================
//
//                             Edit History
//
// $Header: //components/rel/core.tz/2.1.1/settings/buses/qup_accesscontrol/qupv3/config/nicobar/QUPAC_Access.c#1 $
//
// when       who     what, where, why
// 03/08/19   DPK     Removed QUP0 SE3 assignment due to no POR.
// 12/12/18   DPK     Initial revision for Nicobar
//
//===========================================================================
//             Copyright (c) 2018-2019 QUALCOMM Technologies, Incorporated.
//                    All Rights Reserved.
//                QUALCOMM Confidential & Proprietary
//===========================================================================
#include "QupACCommonIds.h"

/* OEMs are expected to modify this .c to suit their board design. The uAC 
   specifies the owners of the SE resource. It is initially populated
   according to System IO GPIO allocation */

//All SEs have to be listed below. Any SE not present cannot be accesssed by any subsystem. 
//It's designed to be flexible enough to list only available SEs on a particular platform.

const QUPv3_se_security_permissions_type qupv3_perms_default[] =
{
  /*PeriphID,    ProtocolID,             Mode,            NsOwner,   bAllowFifo, bLoad, bModExcl  */
  { QUPV3_0_SE0, QUPV3_PROTOCOL_SPI,     QUPV3_MODE_GSI,  AC_TZ,          FALSE, TRUE,  TRUE  }, // NFC eSE
  { QUPV3_0_SE1, QUPV3_PROTOCOL_I2C,     QUPV3_MODE_FIFO, AC_HLOS,         TRUE, TRUE,  FALSE }, // SMB/EEPROM/PM8008/Ext PD control
  { QUPV3_0_SE2, QUPV3_PROTOCOL_I2C,     QUPV3_MODE_FIFO, AC_HLOS,         TRUE, TRUE,  FALSE }, // Touch
  /*QUPV3_0_SE3*/
  { QUPV3_0_SE4, QUPV3_PROTOCOL_UART_2W, QUPV3_MODE_FIFO, AC_HLOS,         TRUE, FALSE, FALSE }, // Debug
  { QUPV3_1_SE0, QUPV3_PROTOCOL_I3C,     QUPV3_MODE_GSI,  AC_ADSP_Q6_ELF, FALSE, TRUE,  FALSE }, // SENSORS I3C
  { QUPV3_1_SE1, QUPV3_PROTOCOL_SPI,     QUPV3_MODE_GSI,  AC_TZ,          FALSE, TRUE,  TRUE  }, // Fingerprint
  { QUPV3_1_SE2, QUPV3_PROTOCOL_I2C,     QUPV3_MODE_GSI,  AC_ADSP_Q6_ELF, FALSE, TRUE,  FALSE }, // SENSORS I2C
  /*QUPV3_1_SE3*/
  { QUPV3_1_SE4, QUPV3_PROTOCOL_UART_4W, QUPV3_MODE_GSI,  AC_HLOS,        FALSE, TRUE,  FALSE }, // WCN - Bluetooth HCI UART  
};

const uint32 qupv3_perms_size_default = sizeof(qupv3_perms_default)/sizeof(qupv3_perms_default[0]);

const QUPv3_se_security_permissions_type qupv3_perms_rumi[] =
{
  /*PeriphID,    ProtocolID,             Mode,            NsOwner,   bAllowFifo, bLoad, bModExcl  */
  /*QUPV3_0_SE0*/
  { QUPV3_0_SE1, QUPV3_PROTOCOL_I2C,     QUPV3_MODE_FIFO, AC_HLOS,         TRUE, TRUE,  FALSE }, // SMB/EEPROM/PM8008/Ext PD control
  /*QUPV3_0_SE2*/
  /*QUPV3_0_SE3*/
  { QUPV3_0_SE4, QUPV3_PROTOCOL_UART_2W, QUPV3_MODE_FIFO, AC_HLOS,         TRUE, FALSE, FALSE }, // Debug
  { QUPV3_1_SE0, QUPV3_PROTOCOL_I3C,     QUPV3_MODE_GSI,  AC_ADSP_Q6_ELF, FALSE, TRUE,  FALSE }, // SENSORS I3C
  { QUPV3_1_SE1, QUPV3_PROTOCOL_SPI,     QUPV3_MODE_GSI,  AC_TZ,          FALSE, TRUE,  TRUE  }, // Fingerprint
  { QUPV3_1_SE2, QUPV3_PROTOCOL_I2C,     QUPV3_MODE_GSI,  AC_ADSP_Q6_ELF, FALSE, TRUE,  FALSE }, // SENSORS I2C
  /*QUPV3_1_SE3*/
  /*QUPV3_1_SE4*/
};

const uint32 qupv3_perms_size_rumi = sizeof(qupv3_perms_rumi)/sizeof(qupv3_perms_rumi[0]);

const QUPv3_gpii_security_permissions_type qupv3_gpii_perms[] =
{
  { QUPV3_0_GPII0,  AC_HLOS,        AC_HLOS_GSI },
  { QUPV3_0_GPII1,  AC_HLOS,        AC_HLOS_GSI },
  { QUPV3_0_GPII2,  AC_HLOS,        AC_HLOS_GSI },
  { QUPV3_0_GPII3,  AC_HLOS,        AC_HLOS_GSI },
  { QUPV3_0_GPII4,  AC_HLOS,        AC_HLOS_GSI },
  { QUPV3_0_GPII5,  AC_TZ },
  { QUPV3_0_GPII6,  AC_TZ },
  { QUPV3_0_GPII7,  AC_MSS_MSA }, 
  { QUPV3_1_GPII0,  AC_HLOS,        AC_HLOS_GSI },
  { QUPV3_1_GPII1,  AC_HLOS,        AC_HLOS_GSI },
  { QUPV3_1_GPII2,  AC_HLOS,        AC_HLOS_GSI },
  { QUPV3_1_GPII3,  AC_HLOS,        AC_HLOS_GSI },
  { QUPV3_1_GPII4,  AC_TZ },
  { QUPV3_1_GPII5,  AC_TZ },
  { QUPV3_1_GPII6,  AC_ADSP_Q6_ELF },
  { QUPV3_1_GPII7,  AC_ADSP_Q6_ELF },  
};

const uint32 qupv3_gpii_perms_size = sizeof(qupv3_gpii_perms)/sizeof(qupv3_gpii_perms[0]);
