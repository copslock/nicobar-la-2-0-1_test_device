#include "ACCommon.h"
#include "AccessControl.h"

static ACRegion AC_REGION_QUP_0I0[]=
{
	{
		.uMemoryStart = 	0x04a20000,
		.uMemorySize = 	0x4000,
	},
};

static uint32 AC_InternalSID_QUP_0I0[]=
{
	0x0,
};

ACDeviceInfo device_qup_0i0 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0I0,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0I0,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};



static uint32 AC_InternalSID_QUP_0I0I[]=
{
	0x20,
};

ACDeviceInfo device_qup_0i0i =
{
	.eSmmuInstance = 	0,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0I0I,
	.uNumMappings = 0,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_0I1[]=
{
	{
		.uMemoryStart = 	0x04a24000,
		.uMemorySize = 	0x4000,
	},
};

static uint32 AC_InternalSID_QUP_0I1[]=
{
	0x1,
};

ACDeviceInfo device_qup_0i1 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0I1,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0I1,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};



static uint32 AC_InternalSID_QUP_0I1I[]=
{
	0x21,
};

ACDeviceInfo device_qup_0i1i =
{
	.eSmmuInstance = 	0,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0I1I,
	.uNumMappings = 0,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_0I2[]=
{
	{
		.uMemoryStart = 	0x04a28000,
		.uMemorySize = 	0x4000,
	},
};

static uint32 AC_InternalSID_QUP_0I2[]=
{
	0x2,
};

ACDeviceInfo device_qup_0i2 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0I2,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0I2,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};



static uint32 AC_InternalSID_QUP_0I2I[]=
{
	0x22,
};

ACDeviceInfo device_qup_0i2i =
{
	.eSmmuInstance = 	0,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0I2I,
	.uNumMappings = 0,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_0I3[]=
{
	{
		.uMemoryStart = 	0x04a2c000,
		.uMemorySize = 	0x4000,
	},
};

static uint32 AC_InternalSID_QUP_0I3[]=
{
	0x3,
};

ACDeviceInfo device_qup_0i3 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0I3,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0I3,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};



static uint32 AC_InternalSID_QUP_0I3I[]=
{
	0x23,
};

ACDeviceInfo device_qup_0i3i =
{
	.eSmmuInstance = 	0,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0I3I,
	.uNumMappings = 0,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_0I4[]=
{
	{
		.uMemoryStart = 	0x04a30000,
		.uMemorySize = 	0x4000,
	},
};

static uint32 AC_InternalSID_QUP_0I4[]=
{
	0x4,
};

ACDeviceInfo device_qup_0i4 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0I4,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0I4,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};



static uint32 AC_InternalSID_QUP_0I4I[]=
{
	0x24,
};

ACDeviceInfo device_qup_0i4i =
{
	.eSmmuInstance = 	0,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0I4I,
	.uNumMappings = 0,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_0I5[]=
{
	{
		.uMemoryStart = 	0x04a34000,
		.uMemorySize = 	0x4000,
	},
};

static uint32 AC_InternalSID_QUP_0I5[]=
{
	0x5,
};

ACDeviceInfo device_qup_0i5 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0I5,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0I5,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};



static uint32 AC_InternalSID_QUP_0I5I[]=
{
	0x25,
};

ACDeviceInfo device_qup_0i5i =
{
	.eSmmuInstance = 	0,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0I5I,
	.uNumMappings = 0,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_0I6[]=
{
	{
		.uMemoryStart = 	0x04a38000,
		.uMemorySize = 	0x4000,
	},
};

static uint32 AC_InternalSID_QUP_0I6[]=
{
	0x6,
};

ACDeviceInfo device_qup_0i6 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0I6,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0I6,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};



static uint32 AC_InternalSID_QUP_0I6I[]=
{
	0x26,
};

ACDeviceInfo device_qup_0i6i =
{
	.eSmmuInstance = 	0,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0I6I,
	.uNumMappings = 0,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_0I7[]=
{
	{
		.uMemoryStart = 	0x04a3c000,
		.uMemorySize = 	0x4000,
	},
};

static uint32 AC_InternalSID_QUP_0I7[]=
{
	0x7,
};

ACDeviceInfo device_qup_0i7 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0I7,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0I7,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};



static uint32 AC_InternalSID_QUP_0I7I[]=
{
	0x27,
};

ACDeviceInfo device_qup_0i7i =
{
	.eSmmuInstance = 	0,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0I7I,
	.uNumMappings = 0,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_0S0[]=
{
	{
		.uMemoryStart = 	0x04a80000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x04a81000,
		.uMemorySize = 	0x1000,
	},
};

static uint32 AC_InternalSID_QUP_0S0[]=
{
	0x10,
};

ACDeviceInfo device_qup_0s0 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0S0,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0S0,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_0S0S[]=
{
	{
		.uMemoryStart = 	0x04a82000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_0s0s =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0S0S,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_0S0G0[]=
{
	{
		.uMemoryStart = 	0x500000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x501000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_0s0g0 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0S0G0,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_0S0G2[]=
{
	{
		.uMemoryStart = 	0x502000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x503000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_0s0g2 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0S0G2,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_0S1[]=
{
	{
		.uMemoryStart = 	0x04a84000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x04a85000,
		.uMemorySize = 	0x1000,
	},
};

static uint32 AC_InternalSID_QUP_0S1[]=
{
	0x11,
};

ACDeviceInfo device_qup_0s1 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0S1,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0S1,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_0S1S[]=
{
	{
		.uMemoryStart = 	0x04a86000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_0s1s =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0S1S,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_0S1G0[]=
{
	{
		.uMemoryStart = 	0x504000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x505000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_0s1g0 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0S1G0,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_0S2[]=
{
	{
		.uMemoryStart = 	0x04a88000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x04a89000,
		.uMemorySize = 	0x1000,
	},
};

static uint32 AC_InternalSID_QUP_0S2[]=
{
	0x12,
};

ACDeviceInfo device_qup_0s2 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0S2,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0S2,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_0S2S[]=
{
	{
		.uMemoryStart = 	0x04a8a000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_0s2s =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0S2S,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_0S2G0[]=
{
	{
		.uMemoryStart = 	0x506000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x507000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_0s2g0 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0S2G0,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_0S2G2[]=
{
	{
		.uMemoryStart = 	0x508000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x509000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_0s2g2 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0S2G2,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_0S3[]=
{
	{
		.uMemoryStart = 	0x04a8c000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x04a8d000,
		.uMemorySize = 	0x1000,
	},
};

static uint32 AC_InternalSID_QUP_0S3[]=
{
	0x13,
};

ACDeviceInfo device_qup_0s3 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0S3,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0S3,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_0S3S[]=
{
	{
		.uMemoryStart = 	0x04a8e000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_0s3s =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0S3S,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_0S3G0[]=
{
	{
		.uMemoryStart = 	0x50E000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x50F000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_0s3g0 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0S3G0,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_0S3G2[]=
{
	{
		.uMemoryStart = 	0x50E000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x50F000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_0s3g2 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0S3G2,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_0S4[]=
{
	{
		.uMemoryStart = 	0x04a90000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x04a91000,
		.uMemorySize = 	0x1000,
	},
};

static uint32 AC_InternalSID_QUP_0S4[]=
{
	0x14,
};

ACDeviceInfo device_qup_0s4 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0S4,
	.eVmidmtInstance = 	24,
	.pInternalSidList = AC_InternalSID_QUP_0S4,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_0S4S[]=
{
	{
		.uMemoryStart = 	0x04a92000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_0s4s =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0S4S,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_0S4G0[]=
{
	{
		.uMemoryStart = 	0x510000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x511000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_0s4g0 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0S4G0,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_0S4G2[]=
{
	{
		.uMemoryStart = 	0x510000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x511000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_0s4g2 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0S4G2,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_0C[]=
{
	{
		.uMemoryStart = 	0x04a04000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x04a05000,
		.uMemorySize = 	0x2000,
	},
	{
		.uMemoryStart = 	0x04a07000,
		.uMemorySize = 	0x8000,
	},
	{
		.uMemoryStart = 	0x04ac0000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_0c =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0C,
	.uNumMappings = 4,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_0CS[]=
{
	{
		.uMemoryStart = 	0x04a0f000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x04ac1000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_0cs =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_0CS,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_1I0[]=
{
	{
		.uMemoryStart = 	0x04c20000,
		.uMemorySize = 	0x4000,
	},
};

static uint32 AC_InternalSID_QUP_1I0[]=
{
	0x0,
};

ACDeviceInfo device_qup_1i0 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1I0,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1I0,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};



static uint32 AC_InternalSID_QUP_1I0I[]=
{
	0x20,
};

ACDeviceInfo device_qup_1i0i =
{
	.eSmmuInstance = 	0,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1I0I,
	.uNumMappings = 0,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_1I1[]=
{
	{
		.uMemoryStart = 	0x04c24000,
		.uMemorySize = 	0x4000,
	},
};

static uint32 AC_InternalSID_QUP_1I1[]=
{
	0x1,
};

ACDeviceInfo device_qup_1i1 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1I1,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1I1,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};



static uint32 AC_InternalSID_QUP_1I1I[]=
{
	0x21,
};

ACDeviceInfo device_qup_1i1i =
{
	.eSmmuInstance = 	0,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1I1I,
	.uNumMappings = 0,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_1I2[]=
{
	{
		.uMemoryStart = 	0x04c28000,
		.uMemorySize = 	0x4000,
	},
};

static uint32 AC_InternalSID_QUP_1I2[]=
{
	0x2,
};

ACDeviceInfo device_qup_1i2 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1I2,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1I2,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};



static uint32 AC_InternalSID_QUP_1I2I[]=
{
	0x22,
};

ACDeviceInfo device_qup_1i2i =
{
	.eSmmuInstance = 	0,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1I2I,
	.uNumMappings = 0,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_1I3[]=
{
	{
		.uMemoryStart = 	0x04c2c000,
		.uMemorySize = 	0x4000,
	},
};

static uint32 AC_InternalSID_QUP_1I3[]=
{
	0x3,
};

ACDeviceInfo device_qup_1i3 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1I3,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1I3,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};



static uint32 AC_InternalSID_QUP_1I3I[]=
{
	0x23,
};

ACDeviceInfo device_qup_1i3i =
{
	.eSmmuInstance = 	0,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1I3I,
	.uNumMappings = 0,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_1I4[]=
{
	{
		.uMemoryStart = 	0x04c30000,
		.uMemorySize = 	0x4000,
	},
};

static uint32 AC_InternalSID_QUP_1I4[]=
{
	0x4,
};

ACDeviceInfo device_qup_1i4 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1I4,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1I4,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};



static uint32 AC_InternalSID_QUP_1I4I[]=
{
	0x24,
};

ACDeviceInfo device_qup_1i4i =
{
	.eSmmuInstance = 	0,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1I4I,
	.uNumMappings = 0,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_1I5[]=
{
	{
		.uMemoryStart = 	0x04c34000,
		.uMemorySize = 	0x4000,
	},
};

static uint32 AC_InternalSID_QUP_1I5[]=
{
	0x5,
};

ACDeviceInfo device_qup_1i5 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1I5,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1I5,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};



static uint32 AC_InternalSID_QUP_1I5I[]=
{
	0x25,
};

ACDeviceInfo device_qup_1i5i =
{
	.eSmmuInstance = 	0,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1I5I,
	.uNumMappings = 0,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_1I6[]=
{
	{
		.uMemoryStart = 	0x04c38000,
		.uMemorySize = 	0x4000,
	},
};

static uint32 AC_InternalSID_QUP_1I6[]=
{
	0x6,
};

ACDeviceInfo device_qup_1i6 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1I6,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1I6,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};



static uint32 AC_InternalSID_QUP_1I6I[]=
{
	0x26,
};

ACDeviceInfo device_qup_1i6i =
{
	.eSmmuInstance = 	0,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1I6I,
	.uNumMappings = 0,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_1I7[]=
{
	{
		.uMemoryStart = 	0x04c3c000,
		.uMemorySize = 	0x4000,
	},
};

static uint32 AC_InternalSID_QUP_1I7[]=
{
	0x7,
};

ACDeviceInfo device_qup_1i7 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1I7,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1I7,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};



static uint32 AC_InternalSID_QUP_1I7I[]=
{
	0x27,
};

ACDeviceInfo device_qup_1i7i =
{
	.eSmmuInstance = 	0,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1I7I,
	.uNumMappings = 0,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_1S0[]=
{
	{
		.uMemoryStart = 	0x04c80000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x04c81000,
		.uMemorySize = 	0x1000,
	},
};

static uint32 AC_InternalSID_QUP_1S0[]=
{
	0x10,
};

ACDeviceInfo device_qup_1s0 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1S0,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1S0,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_1S0S[]=
{
	{
		.uMemoryStart = 	0x04c82000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_1s0s =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1S0S,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_1S0G0[]=
{
	{
		.uMemoryStart = 	0x516000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x517000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_1s0g0 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1S0G0,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_1S0G2[]=
{
	{
		.uMemoryStart = 	0x518000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x519000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_1s0g2 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1S0G2,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_1S1[]=
{
	{
		.uMemoryStart = 	0x04c84000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x04c85000,
		.uMemorySize = 	0x1000,
	},
};

static uint32 AC_InternalSID_QUP_1S1[]=
{
	0x11,
};

ACDeviceInfo device_qup_1s1 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1S1,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1S1,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_1S1S[]=
{
	{
		.uMemoryStart = 	0x04c86000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_1s1s =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1S1S,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_1S1G0[]=
{
	{
		.uMemoryStart = 	0x51E000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x51F000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_1s1g0 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1S1G0,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_1S1G2[]=
{
	{
		.uMemoryStart = 	0x520000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x521000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_1s1g2 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1S1G2,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_1S2[]=
{
	{
		.uMemoryStart = 	0x04c88000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x04c89000,
		.uMemorySize = 	0x1000,
	},
};

static uint32 AC_InternalSID_QUP_1S2[]=
{
	0x12,
};

ACDeviceInfo device_qup_1s2 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1S2,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1S2,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_1S2S[]=
{
	{
		.uMemoryStart = 	0x04c8a000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_1s2s =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1S2S,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_1S2G0[]=
{
	{
		.uMemoryStart = 	0x51C000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x51D000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_1s2g0 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1S2G0,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_1S3[]=
{
	{
		.uMemoryStart = 	0x04c8c000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x04c8d000,
		.uMemorySize = 	0x1000,
	},
};

static uint32 AC_InternalSID_QUP_1S3[]=
{
	0x13,
};

ACDeviceInfo device_qup_1s3 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1S3,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1S3,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_1S3S[]=
{
	{
		.uMemoryStart = 	0x04c8e000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_1s3s =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1S3S,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_1S3G0[]=
{
	{
		.uMemoryStart = 	0xD12000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0xD13000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_1s3g0 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1S3G0,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_1S3G2[]=
{
	{
		.uMemoryStart = 	0xD14000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0xD15000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_1s3g2 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1S3G2,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_1S4[]=
{
	{
		.uMemoryStart = 	0x04c90000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x04c91000,
		.uMemorySize = 	0x1000,
	},
};

static uint32 AC_InternalSID_QUP_1S4[]=
{
	0x14,
};

ACDeviceInfo device_qup_1s4 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1S4,
	.eVmidmtInstance = 	25,
	.pInternalSidList = AC_InternalSID_QUP_1S4,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

static ACRegion AC_REGION_QUP_1S4S[]=
{
	{
		.uMemoryStart = 	0x04c92000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_1s4s =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1S4S,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_1S4G0[]=
{
	{
		.uMemoryStart = 	0xD0A000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0xD0B000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_1s4g0 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1S4G0,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_1S4G2[]=
{
	{
		.uMemoryStart = 	0xD0C000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0xD0D000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_1s4g2 =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1S4G2,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_1C[]=
{
	{
		.uMemoryStart = 	0x04c04000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x04c05000,
		.uMemorySize = 	0x2000,
	},
	{
		.uMemoryStart = 	0x04c07000,
		.uMemorySize = 	0x8000,
	},
	{
		.uMemoryStart = 	0x04cc0000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_1c =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1C,
	.uNumMappings = 4,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_QUP_1CS[]=
{
	{
		.uMemoryStart = 	0x04c0f000,
		.uMemorySize = 	0x1000,
	},
	{
		.uMemoryStart = 	0x04cc1000,
		.uMemorySize = 	0x1000,
	},
};

ACDeviceInfo device_qup_1cs =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_QUP_1CS,
	.uNumMappings = 2,
	.uNumExternalSids = 0,
	.uNumInternalSids = 0,
};

static ACRegion AC_REGION_SENSOR_PRAM[]=
{
	{
		.uMemoryStart = 	0x0A0B8000,
		.uMemorySize = 	0x6000,
	},
};

static uint32 AC_InternalSID_SENSOR_PRAM[]=
{
	0,
};

ACDeviceInfo device_sensor_pram =
{
	.eSmmuInstance = 	0,
	.pMappings = AC_REGION_SENSOR_PRAM,
	.eVmidmtInstance = 	0,
	.pInternalSidList = AC_InternalSID_SENSOR_PRAM,
	.uNumMappings = 1,
	.uNumExternalSids = 0,
	.uNumInternalSids = 1,
};

