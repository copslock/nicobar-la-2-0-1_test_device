/*
===========================================================================

FILE:         gpi_tz_nicobar.c

DESCRIPTION:  This file implements the GPI Config Data.

===========================================================================

                             Edit History


when       who     what, where, why
--------   ---     -------------------------------------------------------- 
11/27/18   pcr     Created for Nicobar

===========================================================================
             Copyright (c) 2018 QUALCOMM Technologies, Incorporated.
                    All Rights Reserved
                   QUALCOMM Proprietary
===========================================================================
*/

#include "gpitgtcfgdata.h"
#include "msmhwiobase.h"

#define GSI_0_BASE       (QUPV3_0_QUPV3_ID_1_BASE + 0x00004000)
#define GSI_1_BASE       (QUPV3_1_QUPV3_ID_1_BASE + 0x00004000)

const tgt_gpi_config_type  tgt_gpi_config[] =
{
   { TRUE, 0, GSI_0_BASE,   0, { 367, 368, 369, 370, 371, 372, 373, 374, 0, 0, 0, 0, 0, 0, 0, 0 } },  /* QUP_0 */
   { TRUE, 1, GSI_1_BASE,   0, { 346, 347, 348, 349, 350, 351, 352, 353, 0, 0, 0, 0, 0, 0, 0, 0 } },  /* QUP_1 */
};
