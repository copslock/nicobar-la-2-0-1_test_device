////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017-2019 Qualcomm Technologies, Inc.
// All Rights Reserved.
// Confidential and Proprietary - Qualcomm Technologies, Inc.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef DUMMY_h
#define DUMMY_h
#include <vector>

enum A { v0, v1, v2, v3 };
std::vector<int> initVector(int lower_range, int upper_range);
int add(int n1, int n2);
int multiply(int n1, int n2);
void deinitVector(std::vector<int> &vec);
int enumMult(enum A);

#endif
