/*=============================================================================
Copyright (c) 2020 Qualcomm Technologies, Inc.
All Rights Reserved.
Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/

package com.qualcomm.qti.cam2test;

import android.app.Activity;
import android.content.Intent;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.media.ImageReader;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import java.util.List;
import java.util.ArrayList;
import android.widget.TextView;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;

import androidx.fragment.app.Fragment;

import java.util.Arrays;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

//Sanumala
import android.hardware.camera2.params.StreamConfigurationMap;
import android.hardware.camera2.params.SessionConfiguration;
import android.hardware.camera2.params.OutputConfiguration;
import java.util.concurrent.Executor;
//Sanumala

// Secure and Non-secure capture fragments extend this class
class Capture extends Fragment {
    static final int USAGE_PROTECTED = 0x4000;
    ConfiguredCamera cam;

    long currentFPS, numFrames, startTimestamp;
    CameraManager cameraManager;
    CameraDevice mCameraDevice;
    CameraCaptureSession mCameraCaptureSession;
    Size mCaptureDimensions;
    CaptureRequest.Builder mCaptureRequestBuilder;
    ImageReader mRawImageReader;

    TextView fps_view;
    TextView timestamp_view;
    int fps_id;
    int timestamp_id;

    private final Object mCameraStateLock = new Object();
    private final Semaphore mCameraOpenCloseLock = new Semaphore(1);

    Handler mBackgroundHandler;
    private HandlerThread mHandlerThread;

    public static final CaptureRequest
    .Key<Byte> mBypass_Resource_Check = new CaptureRequest.Key<>(
    "org.codeaurora.qcamera3.sessionParameters.overrideResourceCostValidation",
    byte.class);
    protected CameraCaptureSession.CaptureCallback captureCallback_ = null;
    // Lock & open requested camera
    @SuppressWarnings("MissingPermission")
    void openCamera() {
        try {
            // Acquire semaphore on camera before opening
            if(!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                Log.d(getLogTag(), "ERROR: Took too long to lock camera");
                throw new RuntimeException("Timeout while waiting to lock camera opening");
            } else {
                // Open camera, handle changes in state using stateCallback
                cameraManager.openCamera(cam.getCamId(), stateCallback, mBackgroundHandler);
            }

        } catch (Exception e) {
            Log.d(getLogTag(), "ERROR: Unable to open camera ID " + cam.getCamId());
            e.printStackTrace();

            // Returning with error code RESULT_FIRST_USER
            Intent returnIntent = new Intent();
            getActivity().setResult(Activity.RESULT_FIRST_USER, returnIntent);
            getActivity().finish();
        }
    }

    private CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice camera) {
            synchronized (mCameraStateLock) {
                mCameraOpenCloseLock.release();
                Log.d(getLogTag(), "Opened Camera ID " + cam.getCamId());
                mCameraDevice = camera;

                // Camera has opened. So start capture if surface texture is also available
                if (mCaptureDimensions != null) {
                    startCameraCapture();
                }
            }
        }

        @Override
        public void onDisconnected(CameraDevice camera) {
            Log.d(getLogTag(), "Disconnected Camera ID " + cam.getCamId());
            closeCamera();
        }

        @Override
        public void onError(CameraDevice camera, int error) {
            Log.d(getLogTag(), "ERROR in Camera ID " + cam.getCamId());
            closeCamera();
            getActivity().finish();
        }
    };


    protected class HandlerExecutor implements Executor {
      private final Handler ihandler;
      public HandlerExecutor(Handler handler) { ihandler = handler; }
      @Override

      public void execute(Runnable runCmd) {
        ihandler.post(runCmd);
      }
    }

    private void startCameraCapture() {
        Surface captureSurface = mRawImageReader.getSurface();

        List<Surface> outputSurfaces = new ArrayList<Surface>();
        outputSurfaces.add(captureSurface);

        try {
            // Create capture request based on chosen fps
            mCaptureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE, cam.getFps());
            mCaptureRequestBuilder.set(mBypass_Resource_Check, (byte)0x01);
            // Add target to receive frameAvailable events
          //  mCaptureRequestBuilder.addTarget(captureSurface);

            List<OutputConfiguration> outConfigurations =
                    new ArrayList<>(outputSurfaces.size());

            for (Surface sface : outputSurfaces) {
                outConfigurations.add(new OutputConfiguration(sface));
                mCaptureRequestBuilder.addTarget(sface);
              }

            //mCameraDevice.createCaptureSession(Arrays.asList(captureSurface), new CameraCaptureSession.StateCallback()
            CameraCaptureSession.StateCallback mCaptureStateCallBack = new CameraCaptureSession.StateCallback()
            {
                @Override
                public void onConfigured(CameraCaptureSession session) {
                    if(null != mCameraDevice) {
                        mCameraCaptureSession = session;

                        //SANUMALA
                        captureCallback_ = new CameraCaptureSession.CaptureCallback() {
                            @Override
                            public void onCaptureBufferLost(CameraCaptureSession session,
                                                            CaptureRequest request,
                                                            Surface target, long frameNumber) {
                              Log.d(
                                  getLogTag(),
                                  "JavaCamera2::CameraCaptureSession.CaptureCallback::onCaptureBufferLost - Camera #");
                            }

                            @Override
                            public void onCaptureCompleted(CameraCaptureSession session,
                                                           CaptureRequest request,
                                                           TotalCaptureResult result) {
                              // unlockFocus();
                              Log.d(
                                  getLogTag(),
                                  "JavaCamera2::CameraCaptureSession.CaptureCallback::onCaptureCompleted - Camera #");

                            }

                            @Override
                            public void onCaptureFailed(CameraCaptureSession session,
                                                        CaptureRequest request,
                                                        CaptureFailure failure) {
                              Log.d(
                                  getLogTag(),
                                  "JavaCamera2::CameraCaptureSession.CaptureCallback::onCaptureFailed - Camera #");
                            }

                            @Override
                            public void onCaptureProgressed(CameraCaptureSession session,
                                                            CaptureRequest request,
                                                            CaptureResult partialResult) {
                              Log.d(
                                  getLogTag(),
                                  "JavaCamera2::CameraCaptureSession.CaptureCallback::onCaptureProgressed - Camera #");
                            }

                            @Override
                            public void onCaptureSequenceAborted(CameraCaptureSession session,
                                                                 int sequenceId) {
                              Log.d(
                                  getLogTag(),
                                  "JavaCamera2::CameraCaptureSession.CaptureCallback::onCaptureSequenceAborted - Camera #");
                            }

                            @Override
                            public void onCaptureSequenceCompleted(CameraCaptureSession session,
                                                                   int sequenceId,
                                                                   long frameNumber) {
                              Log.d(
                                  getLogTag(),
                                  "JavaCamera2::CameraCaptureSession.CaptureCallback::onCaptureSequenceCompleted - Camera #");
                            }

                            @Override
                            public void onCaptureStarted(CameraCaptureSession session,
                                                         CaptureRequest request, long timestamp,
                                                         long frameNumber) {
                              Log.d(
                                  getLogTag(),
                                  "JavaCamera2::CameraCaptureSession.CaptureCallback::onCaptureStarted - Camera #");
                            }
                          };

                        //SANUMALA
                        // Session is configured, now create repeating requests
                        updateCapture();
                    }
                }

                @Override
                public void onConfigureFailed(CameraCaptureSession session) {
                    Log.d(getLogTag(), "ERROR: Failed to configure capture session");
                }
            };
            SessionConfiguration sessionCfg = new SessionConfiguration(
                    SessionConfiguration.SESSION_REGULAR, outConfigurations,
                    new HandlerExecutor(mBackgroundHandler), mCaptureStateCallBack);
                sessionCfg.setSessionParameters(mCaptureRequestBuilder.build());
                mCameraDevice.createCaptureSession(sessionCfg);

        } catch (CameraAccessException e) {
            Log.d(getLogTag(), "ERROR: Failed to access CAMERA ID " + cam.getCamId());
            e.printStackTrace();
        }
    }

    // After the first time, set repeating requests to keep getting frames
    private void updateCapture() {
        if (null != mCameraDevice) {
            mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE, cam.getFps());
            try {
                // Callback is null. There is currently no need to handle capture progression events (started, progressed, completed)
                mCameraCaptureSession.setRepeatingRequest(mCaptureRequestBuilder.build(), captureCallback_, mBackgroundHandler);//sanumal
            } catch (Exception e) {
                Log.d(getLogTag(), "ERROR: Failed to access CAMERA ID " + cam.getCamId());
                e.printStackTrace();
            }
        }
    }

    void startBackgroundThread() {
        mHandlerThread = new HandlerThread("Camera Background");
        mHandlerThread.start();

        Log.d(getLogTag(), "STARTED Background Thread");
        synchronized (mCameraStateLock) {
            mBackgroundHandler = new Handler(mHandlerThread.getLooper());
        }
    }

    void stopBackgroundThread() {
        mHandlerThread.quitSafely();
        try {
            mHandlerThread.join();
            mBackgroundHandler = null;
            mHandlerThread = null;

            Log.d(getLogTag(), "STOPPED Background Thread " + cam.getCamId());
        } catch (InterruptedException e) {
            Log.d(getLogTag(), "Interrupted while stopping background activity");
            e.printStackTrace();
        }
    }

    // Close camera, imagereader and session
    void closeCamera() {
        try {
            mCameraOpenCloseLock.acquire();

            // Checks null before closing to avoid crashing if open had failed
            synchronized (mCameraStateLock) {
                if (mCameraCaptureSession != null) {
                    mCameraCaptureSession.close();
                    mCameraCaptureSession = null;
                }

                if (mCameraDevice != null) {
                    mCameraDevice.close();
                    mCameraDevice = null;
                }

                if (mRawImageReader != null) {
                    mRawImageReader.close();
                    mRawImageReader = null;
                }

                Log.d(getLogTag(), "Closed Camera ID " + cam.getCamId());
            }
        } catch (InterruptedException e) {
            Log.d(getLogTag(), "ERROR: Interrupted while closing camera");
            e.printStackTrace();
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

    String getLogTag() {
        return getString(R.string.log_tag);
    }
}
