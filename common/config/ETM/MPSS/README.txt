ETM can be enabled from ADB shell. However if tescase involves reboots, we make the changes in script that would keep ETM enabled after reboot. 

1) Step#1: As a first step proper build is required with coresight settings on APSS build.

These are configs required in defconfig file:

CONFIG_CORESIGHT=y
CONFIG_CORESIGHT_LINK_AND_SINK_TMC=y
CONFIG_CORESIGHT_SOURCE_ETM4X=y
CONFIG_CORESIGHT_DYNAMIC_REPLICATOR=y
CONFIG_CORESIGHT_STM=y
CONFIG_CORESIGHT_CTI=y
CONFIG_CORESIGHT_TPDA=y
CONFIG_CORESIGHT_TPDM=y
CONFIG_CORESIGHT_HWEVENT=y
CONFIG_CORESIGHT_DUMMY=y
CONFIG_CORESIGHT_REMOTE_ETM=y
CONFIG_CORESIGHT_REMOTE_ETM_DEFAULT_ENABLE=0
CONFIG_CORESIGHT_TGU=y
CONFIG_CORESIGHT_EVENT=y

2) Step #2: If device is secure boot enabled, kindly enable debug policy and use JTAG override to re-enable ETM along wth above build.

3) Step#3: Manual verification to see if modem ETM works fine:

  try to do 
     echo 1 > /sys/bus/coresight/devices/coresight-tmc-etr/enable_sink 
     echo 1 > /sys/bus/coresight/devices/coresight-modem-etm0/enable_source 

  After this check 

     cat /sys/bus/coresight/devices/coresight-tmc-etr/enable_sink 
     cat /sys/bus/coresight/devices/coresight-modem-etm0/enable_source 

  These should return value "1".
  If this step fails, then look for any failure prints in APPS DMESG or ADB shell to debug further. It would be likely a case of Debug policy and JTAG override not flashed correctly or some QMI error.
  
  Steps are similar for ADSP, SLPI and CDSP. Only this changes "coresight-modem-etm0" is above command.
  
4)Then pull etm file from live device and check contents with Hex-editor to see if these are non-zero:

   adb root
   adb shell
   >>cat /dev/coresight-tmc-etr  > /data/tmc-etr.bin 
  adb pull /data/tmc-etr.bin <location to save etr bin>\tmc-etr.bin

5) Once this step is verified, it means ETM is up and can be used.

6) If test involvs reboots, use the following steps to enable ETM across reboot
	- The package consists of following: testapp_diag_senddata, etm_enable.bat and a SAMPLE init.qti.debug-SAMPLE.sh along with this "README.txt" file.
	- "init.qti.debug-SAMPLE.sh" in this package is for example purpose only. Correct "init.qti.*****.sh" file needs to be pulled from device and the name may change according to customer and target.
	- Once correct file is identified, pull the file from device using "adb pull" and edit the following accordingly to enable ETM:
          
		  
          /******************Common Settings*****************/
          echo 1 > /sys/bus/coresight/reset_source_sink
          echo 1 > /sys/bus/coresight/devices/coresight-tmc-etr/enable_sink

          /******************This is for Modem*****************/
		  echo 1 > /sys/bus/coresight/devices/coresight-modem-etm0/enable_source
          /data/qdss/testapp_diag_senddata 75 90 02 02 01 

          /******************This is for ADSP*****************/		  
		  echo 1 > /sys/bus/coresight/devices/coresight-audio-etm0/enable_source
		  /data/qdss/testapp_diag_senddata 75 90 02 05 01

          /******************This is for CDSP*****************/		  
		  echo 1 > /sys/bus/coresight/devices/coresight-turing-etm0/enable_source
		  /data/qdss/testapp_diag_senddata 75 90 02 13 01

          /******************This is for SLPI*****************/		  
		  echo 1 > /sys/bus/coresight/devices/coresight-ssc-etm0/enable_source
		  /data/qdss/testapp_diag_senddata 75 90 02 08 03
	- Now follow these steps to enable ETM for reboot:
	
	   a) Copy all 3 files in attachment to a local PC path, say c:\temp\etm_enable
       b) Edit etm_enable.bat to change path accordngly: "C:\Temp\testapp_diag_senddata"
       c) Run the batch file etm_enable.bat from windows command prompt. Device will automatically reboot at end of this. Observe for any failures on command prompt.
       d) After reboot run the test case and collect a force crash and parse dump with ramparser to generate "etr.bin" or pull it from live device as mentioned in step#4 and check for validity.
	

For any support requests, kindly file a salesforce case through https://qualcomm-cdmatech-support.my.salesforce.com with problem area "BSP/HLOS->Stability->Modem stability".




