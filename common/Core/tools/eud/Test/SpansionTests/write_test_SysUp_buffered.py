﻿import sys
import os
import time

currentdir = os.path.abspath(os.path.dirname(__file__))
os.chdir(currentdir)
##This is where EUDPythonAPIs live
sys.path.append(os.path.abspath(os.path.join(currentdir,"..")))
sys.path.append(os.path.abspath(os.path.join(currentdir,"..","AutomationSamples")))
sys.path.append(os.path.abspath(os.path.join(currentdir,"..","UnitTest")))
from EUDPythonAPIs import *
import EUDDefines
import EUDTestUtilities as utils
global EUD_USB_WARNING_SEND_SIZE_0
EUD_USB_WARNING_SEND_SIZE_0=0x2000000b

#Defines for dll 
global debug
debug=True
#debug=False
global SUCCESS, FAILURE
SUCCESS=0
FAILURE=-1


def colorprint(string,color):
    color_code_black=7
    if str(color) in "red":
        color_code=12
    elif str(color) in "yellow":
        color_code=30
    elif str(color) in "green":
        color_code=26
    elif str(color) in "blue":
        color_code=9
    #default to red
    else:
        color_code=0
        
    #print string
    #return        
    
    STD_OUTPUT_HANDLE = -11
    stdout_handle = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
    windll.kernel32.SetConsoleTextAttribute(stdout_handle,color_code)
    
    print string
    #restore to black
    #stdout_handle = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
    windll.kernel32.SetConsoleTextAttribute(stdout_handle,color_code_black)
    #print " "
    return

def checkflush(errorcode):
    if errorcode != 0:
        if errorcode != EUD_USB_WARNING_SEND_SIZE_0:
            utils.colorprint("error in SWDFlush!!","red")
            import pdb; pdb.set_trace()
            
def checkSwdStatus(pHandler, DLL):
    swdStatus32 = c_uint32(0)
    errorcode2=c_int32(0) #int32_t
    errorcode2 = DLL.SWDGetStatus(pHandler, pointer(swdStatus32))
    if errorcode2 != 0:
        utils.colorprint("error in SWDGetStatus!!","red")
    if (swdStatus32.value & 0x8):
        utils.colorprint("SWD_STATUS error: wait timeout","red")
    elif (swdStatus32.value & 0x10):
        utils.colorprint("SWD_STATUS error: parity error","red")

    if ((swdStatus32.value & 0x7) != 0x1):
        if ((swdStatus32.value & 0x7) == 0x4):
            utils.colorprint("SWD_STATUS error: acknowledge == FAULT","red")
        elif ((swdStatus32.value & 0x7) == 0x2):
            utils.colorprint("SWD_STATUS error: acknowledge == WAIT","red")
        elif ((swdStatus32.value & 0x7) == 0x7):
            utils.colorprint("SWD_STATUS error: acknowledge == ERROR","red")
        else:
            import pdb; pdb.set_trace()
            utils.colorprint("SWD_STATUS error: undefined acknowledge","red")
    print "SWD_Status: " + hex(swdStatus32.value)
    return



def SWDReadWrite(Handler, DLL, RnW, APnDP, A2_3, senddata, count, pReadBuffer):
    #raw_input("Press any key to continue with SWDReadWrite...")
    RnW_str = ""
    APnDP_str = ""
    Arrow_str = ""
    A2_3_str = hex(A2_3 * 4)
    senddata_str = hex(senddata)
    if (RnW == 0):
        RnW_str = "WRITE"
        Arrow_str = "<-"
    else:
        RnW_str = "READ "
        Arrow_str = "->"
    if (APnDP == 0):
        APnDP_str = "DPACC"
    else:
        APnDP_str = "APACC"

    errorcode = c_int32(99)
    RnW_c = c_uint8(RnW)
    APnDP_c = c_uint8(APnDP)
    A2_3_c = c_uint8(A2_3)
    senddata_c = c_uint32(senddata)
    count_c = c_uint32(count)
    #readbuffer_c = (c_uint8 * 4)(0)
    if (RnW == 1):
        errorcode = DLL.SWDRead(Handler, APnDP_c, A2_3_c,pReadBuffer)
    else:
        errorcode = DLL.SWDWrite(Handler, APnDP_c, A2_3_c,pointer(senddata))
        
    if errorcode != 0:
            utils.colorprint("EUD error occurred in SWDSetReadBuffer","red")
            print "errorcode: " + hex(errorcode)
        
    outstring = ""
    if errorcode == 99:
        utils.colorprint("errorcode untouched","red")
    elif errorcode == 0:
        if (RnW == 0):
            outstring = APnDP_str + " " + RnW_str + " " + A2_3_str + " " + Arrow_str + " " + senddata_str
        else:
            outstring = APnDP_str + " " + RnW_str + " " + A2_3_str + " " + Arrow_str + " 0x...."
        utils.colorprint(outstring,"green")
    elif errorcode == 0x10200008:
        if (RnW == 0):
            outstring = APnDP_str + " " + RnW_str + " " + A2_3_str + " " + Arrow_str + " " + senddata_str
        else:
            outstring = APnDP_str + " " + RnW_str + " " + A2_3_str + " " + Arrow_str + " 0x...."
        utils.colorprint(outstring,"green")
        utils.colorprint("flush requested","green")
    else:
        print APnDP_str + " " + RnW_str + " " + A2_3_str + " " + Arrow_str + " " + senddata_str
        utils.colorprint("EUD error occurred during SWDReadWrite","red")
        print "errorcode: " + hex(errorcode)
    #checkSwdStatus(Handler)




########################################################################################################
####### MAIN ###########################################################################################
if __name__ == '__main__':
    eudDLL = None
    eudDLL = None
    EUDDirectories=[]
    swdhandler=c_void_p()
    errorstring = ""
    errorcode=c_int32(0) #int32_t
    readbuffer_c = (c_uint8 * 4)(0)
    pReadBuf = cast(readbuffer_c, POINTER(ctypes.c_int8))

    #### Get DLL
    
    import EUDPythonAPIs
    eudDLL = EUDPythonAPIs.LoadEUDDLL(FPGAFlag=True)
    if eudDLL is None:
        print "Error! eudDLL could not be loaded. Is it in an accessible directory?"
        sys.exit(1)
        
    
    deviceID = EUDPythonAPIs.SelectEUDDevice(eudDLL)
    if deviceID is None:
        kill("Error! No EUD Devices detected")
        
    #### EUDInitSWDDevice ####
    import pdb; pdb.set_trace()
    #SWD_EUD_Device = EUDPythonAPIs.EUDInitSWDDevice(EUD_dll=eudDLL,deviceID=deviceID,FPGAFlag=True)
    #SWD_EUD_Device = EUDInitSWDDevice(EUD_dll=eudDLL)
    options = c_int(1) #uint32_t
    print "Executing EUDInitializeDeviceSWD..."
    SWD_EUD_Device = eudDLL.EUDInitializeDeviceSWD(deviceID,options,pointer(errorcode))
    
    if SWD_EUD_Device is 0:
        utils.colorprint("EUDInitializeDeviceSWD failed","red")
        #return FAILURE
    else:
        utils.colorprint("Initialized successfully via EUDInitializeDeviceSWD","green")

    #### SetBufMode ####
    print "Setting Buffer Mode..."
    errorcode = c_int32(99)
    #bufmode = c_uint32(0)  # 0 = MANAGEDBUFFERMODE, uint32_t
    bufmode = c_uint32(1)  # 1 = IMMEDIATEWRITEMODE, uint32_t
    #bufmode = c_uint32(2)  # 2 = MANUALBUFFERWRITEMODE, uint32_t
    errorcode = eudDLL.SetBufMode(SWD_EUD_Device, bufmode)
    if errorcode != 0:
        utils.colorprint("error in SetBufMode!!","red")

    #### SWDSetDelay ####
    print "Setting Delay..."
    delaytime = c_uint32(18) # 18 = suggestion John, uint32_t
    errorcode = eudDLL.SWDSetDelay(SWD_EUD_Device, delaytime)
    if errorcode != 0:
        utils.colorprint("error in SWDSetDelay!!","red")

    #### SWDSetFrequency ####
    print "Setting Frequency..."
    freq = c_uint32(0x6) # 0x6 = EUD_FREQ_7_5_MHz, uint32_t
    errorcode = eudDLL.SWDSetFrequency(SWD_EUD_Device,freq)
    if errorcode != 0:
        utils.colorprint("error in SWDSetFrequency!!","red")

    ###### Transaction of SWD commands: ######
    print "Executing SWDReadWrite..."
    raw_input("Press any key to continue with SWD transfers...")
    ###############################################################################################################
    # SWDReadWrite(Handler,       DLL, RnW,APnDP,A2_3,senddata,count)

    # DPACC READ 0x0 -> (should be JTAG ID 0x2ba01477)
    SWDReadWrite(SWD_EUD_Device, eudDLL, 1, 0, 0, 0, 1, pReadBuf)
    raw_input("Press any key to flush...")
    errorcode = eudDLL.SWDFlush(SWD_EUD_Device)
    

    checkSwdStatus(SWD_EUD_Device, eudDLL)
    print "Readbuffer: " + ''.join('{:02x}'.format(readbuffer_c[x]) for x in range(3,-1,-1))

    # DPACC WRITE 0x0 <- 0x0000001e
    SWDReadWrite(SWD_EUD_Device, eudDLL, 0, 0, 0, 0x0000001e, 1, pReadBuf)

    # DPACC WRITE 0x8 <- 0x00000000
    SWDReadWrite(SWD_EUD_Device, eudDLL, 0, 0, 2, 0x00000000, 1, pReadBuf)

    # DPACC WRITE 0x4 <- 0x00000000
    SWDReadWrite(SWD_EUD_Device, eudDLL, 0, 0, 1, 0x00000000, 1, pReadBuf)

    # DPACC WRITE 0x0 <- 0x00000004
    SWDReadWrite(SWD_EUD_Device, eudDLL, 0, 0, 0, 0x00000004, 1, pReadBuf)

    # DPACC WRITE 0x4 <- 0x50000020
    SWDReadWrite(SWD_EUD_Device, eudDLL, 0, 0, 1, 0x50000020, 1, pReadBuf)

    # DPACC READ 0x4 -> (0xf0000000)
    SWDReadWrite(SWD_EUD_Device, eudDLL, 1, 0, 1, 0, 1, pReadBuf)
    raw_input("Press any key to flush...")
    errorcode = eudDLL.SWDFlush(SWD_EUD_Device)
    checkflush(errorcode)
    
    #checkSwdStatus(SWD_EUD_Device, eudDLL)
    print "Readbuffer: 0x" + ''.join('{:02x}'.format(readbuffer_c[x]) for x in range(3,-1,-1))

    # DPACC READ 0x4 -> (0xf0000000)
    SWDReadWrite(SWD_EUD_Device, eudDLL, 1, 0, 1, 0, 1, pReadBuf)
    raw_input("Press any key to flush...")
    errorcode = eudDLL.SWDFlush(SWD_EUD_Device)
    checkflush(errorcode)
    
    #checkSwdStatus(SWD_EUD_Device, eudDLL)
    print "Readbuffer: 0x" + ''.join('{:02x}'.format(readbuffer_c[x]) for x in range(3,-1,-1))

    # DPACC WRITE 0x8 <- 0x00000000
    SWDReadWrite(SWD_EUD_Device, eudDLL, 0, 0, 2, 0x00000000, 1, pReadBuf)

    # APACC WRITE 0x0 <- 0x22000052
    SWDReadWrite(SWD_EUD_Device, eudDLL, 0, 1, 0, 0x22000052, 1, pReadBuf)

    # APACC WRITE 0x4 <- 0xe000edf0
    SWDReadWrite(SWD_EUD_Device, eudDLL, 0, 1, 1, 0xe000edf0, 1, pReadBuf)

    # APACC WRITE 0xc <- 0xa05f0003
    SWDReadWrite(SWD_EUD_Device, eudDLL, 0, 1, 3, 0xa05f0003, 1, pReadBuf)

    # DPACC READ 0xc -> (0x00000000)
    SWDReadWrite(SWD_EUD_Device, eudDLL, 1, 0, 3, 0, 1, pReadBuf)
    raw_input("Press any key to flush...")
    errorcode = eudDLL.SWDFlush(SWD_EUD_Device)
    checkflush(errorcode)
    #checkSwdStatus(SWD_EUD_Device, eudDLL)
    print "Readbuffer: 0x" + ''.join('{:02x}'.format(readbuffer_c[x]) for x in range(3,-1,-1))

    # DPACC READ 0x4 -> (0xf0000040)
    SWDReadWrite(SWD_EUD_Device, eudDLL, 1, 0, 1, 0, 1, pReadBuf)
    raw_input("Press any key to flush...")
    errorcode = eudDLL.SWDFlush(SWD_EUD_Device)
    checkflush(errorcode)
    #checkSwdStatus(SWD_EUD_Device, eudDLL)
    print "Readbuffer: 0x" + ''.join('{:02x}'.format(readbuffer_c[x]) for x in range(3,-1,-1))


################

    ###############################################################################################################
    #### SWDPeripheralReset
    raw_input("Press any key to continue with EUD reset...")
    print "Attempting to reset EUD SWD peripheral..."
    #import pdb; pdb.set_trace()
    errorcode = eudDLL.SWDPeripheralReset(SWD_EUD_Device)
    if errorcode != 0:
        utils.colorprint("error in SWDPeripheralReset!!","red")
        print "errorcode: " + hex(errorcode)
    else:
        utils.colorprint("SWDPeripheralReset successful","green")
    #### EUDClosePeripheral
    print "Closing peripheral..."
    errorcode = eudDLL.EUDClosePeripheral(SWD_EUD_Device)
    time.sleep(2)
    if errorcode != 0:
        utils.colorprint("error in EUDClosePeripheral!!","red")
        print "errorcode: " + hex(errorcode)
    else:
        utils.colorprint("EUDClosePeripheral successful","green")
    #### EUDInitializeDeviceSWD
    print "Initializing 2nd time..."
    errorcode=c_int32(0)
    options = c_uint32(0)
    SWD_EUD_Device = eudDLL.EUDInitializeDeviceSWD(options,pointer(errorcode))
    if SWD_EUD_Device is 0:
        utils.colorprint("error in EUDInitializeDeviceSWD - 2nd run!!","red")
        print "errorcode: " + hex(errorcode.value)
    else:
        utils.colorprint("EUDInitializeDeviceSWD 2nd run successful","green")

    ###############################################################################################################
    #### Transfer 1 (second run)
    raw_input("Press any key to continue with transfer (1 - 2nd run)...")
    print "DP WRITE addr=0x0 data=0x4"
    errorcode = c_int32(99)
    RnW = c_uint8(0)
    APnDP = c_uint8(0)
    A2_3 = c_uint8(0)
    senddata = c_uint32(4)
    count = c_uint32(1)
    errorcode = eudDLL.SWDReadWrite(SWD_EUD_Device,pointer(RnW),pointer(APnDP),pointer(A2_3),pointer(senddata),count)
    if errorcode == 99:
        utils.colorprint("errorcode untouched","red")
    elif errorcode == 0:
        utils.colorprint("Transfer 1 -2nd- finished without EUD error","green")
    else:
        utils.colorprint("EUD error occurred during transfer 1 -2nd-","red")
        print "errorcode: " + hex(errorcode)
    checkSwdStatus(SWD_EUD_Device, eudDLL)