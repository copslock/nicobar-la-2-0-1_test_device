

from optparse import OptionParser
import time
import sys 
import subprocess
import datetime
import re
import ast

currdir=os.path.dirname(os.path.abspath(__file__))
path = os.path.join(currdir,"..","AutomationSamples")

#path = "\\\\diwali\\NSID-HYD-01\\SDM845.LA.1.0-00386-STD.INT-1\\common\\core\\tools\\eud\\AutomationSamples"

sys.path.insert(0, path)

def log_eud(TAG, log_line):
	
	if debug_flag:
		if TAG not in 'DEBUG':
			print(str(datetime.datetime.now()) + " : " + str(TAG) + " : " + str(log_line))
	else :
		print(str(datetime.datetime.now()) + " : " + str(TAG) + " : " + str(log_line))
def execute_cmd(cmd, out=True, timeout=None, return_flag=False) :

    
    log_eud("internal", "executing command " + str(cmd))
    devicesData = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    if timeout is not None: 
		devicesData.wait(timeout)
    stdout, stderr = devicesData.communicate()
    
    if out : 
        if stdout is not None and len(stdout) is not 0:
            log_eud("internal", "stdout is " + str(stdout))
        if stderr is not None and len(stderr) is not 0:    
            log_eud("internal", "stderr is " + str(stderr))
	
	if return_flag is True:
		return str(stdout)

def get_connected_devices():
	
    devicesData = subprocess.Popen(["adb", "devices"],stdout=subprocess.PIPE)
    
    devicesList = []
    #log_eud("internal", devicesData.stdout)
    
    i= 15;
    while i>0:
        i = i-1
        line  = devicesData.stdout.readline()
        if line is "":	
            log_eud("DEBUG", "listed all devices")		
            break
        #log_eud("internal", line)
        if(re.search(r'\bdevice\b', line)) :
            devicesList.append(line.split('device')[0].strip())
    log_eud("DEBUG", "device list from ADB is " + str(devicesList))
    return devicesList
			
def enable_eud(deviceID):
    
    execute_cmd(["adb", "-s", deviceID, "root"])
    time.sleep(5)
    execute_cmd(["adb", "-s", deviceID, "remount"])
    execute_cmd(["adb", "-s", deviceID, "shell", "echo 1 > /sys/module/eud/parameters/enable"])
    execute_cmd(["adb", "-s", deviceID, "shell", "cat /sys/module/eud/parameters/enable"])

def disable_charger(deviceID):
    
	import EUDDisableCharger as file
	out = file.EUDDisableCharger(deviceID=deviceID)
	log_eud("DEBUG", "output from disable_charger for device: " + str(deviceID) + " is " + str(out))
	
	
def enable_charger(deviceID):
    
	import EUDEnableCharger as file
	out = file.EUDEnableCharger(deviceID=deviceID)
	log_eud("DEBUG", "output from enable_charger for device: " + str(deviceID) + " is " + str(out))
	
def detach_usb(deviceID):
    
	import EUDSpoofDetach as file
	out = file.EUDSpoofDetach(deviceID=deviceID)
	log_eud("DEBUG", "output from spoof_detach for device: " + str(deviceID) + " is " + str(out))
	
def attach_usb(deviceID):
    
	import EUDSpoofAttach as file
	out = file.EUDSpoofAttach(deviceID=deviceID)
	log_eud("DEBUG", "output from spoof_attach for device: " + str(deviceID) + " is " + str(out))

def get_devices():
	
	GetEUDAvailableDevices()
	get_connected_devices()
	
def GetEUDAvailableDevices():

    import EUDDisplayAvailableDevices as file 
    out = file.EUDGetListofDevices()
    log_eud("DEBUG", "output from available_devices is " + str(out))
    return out
	
def wait_for_device(deviceID) :
	
	execute_cmd(["adb", "-s", deviceID, "wait-for-device"])	

def parseoptions():
    """
        Parse out given user options
    """
    argparser = OptionParser(version='%prog 1.0')
    argparser.add_option('-d','--debug_flag', dest='debug_flag',     help="debug flag to show debug logs",default=False)
    argparser.add_option('-r','--reboot', dest='reboot_flag',     help="reboot flag to reboot all devices before starting scenario",default=False)
    argparser.add_option('-o','--redirect', dest='redirect',     help="redirect logs to timestamped file",default=False)
    argparser.add_option('-t','--test', dest='test',     help="start spoof testing ",default=False)
	
    #If -h/--help specified, print out description
    '''
    for arg in sys.argv[1:]:
        if ('-h' in arg) or ('--help' in arg):
            print EUDMSMAssertReset.__doc__
    '''        
    
    (options,args)=argparser.parse_args()
  
        
    return options

def reset_devices(): 

	devList = get_connected_devices()	
	for deviceID in devList : 
		log_eud("INFO", "rebooting device " + str(deviceID))
		#execute_cmd(["adb", "-s", deviceID, "disable-verity"])
		execute_cmd(["adb", "-s", deviceID, "reboot"])
		
	for deviceID in devList: 
		log_eud("INFO", "waiting for device " + str(deviceID))
		wait_for_device(deviceID)

				
	
	deviceList=get_connected_devices()

	num = len(deviceList)
	log_eud("DEBUG", "number of devices connected: " + str(num))

	for deviceID in deviceList: 
		log_eud("INFO", "enabling EUD on deviceID " + str(deviceID))
		enable_eud(deviceID)
		execute_cmd(["adb", "-s", deviceID, "shell", "echo 1 > /sys/module/lpm_levels/parameters/sleep_disabled"])
		time.sleep(5)

	
if __name__ == '__main__':
	
			
	options=parseoptions()
		
	debug_flag = options.debug_flag
	host_name = execute_cmd(["hostname"], return_flag=True).strip()
	
	log_eud("INFO", "hostname is " + str(host_name))
	output_file = str(int(time.time())) + "_" + str(host_name) + ".log"
	log_eud("INFO", "output is redirected to " + str(output_file))
	
	if str(options.redirect) in 'True':
		output_file = str(int(time.time())) + "_" + str(host_name) + ".log"
		log_eud("INFO", "output is redirected to " + str(output_file))
		sys.stdout = open(output_file, 'a')

	
	
	
	
	print(options)
	if str(options.reboot_flag) in 'True' :
		devList = get_connected_devices()	
		for deviceID in devList : 
			log_eud("INFO", "rebooting device " + str(deviceID))
			execute_cmd(["adb", "-s", deviceID, "disable-verity"])
			execute_cmd(["adb", "-s", deviceID, "reboot"])
			
		for deviceID in devList: 
			log_eud("INFO", "waiting for device " + str(deviceID))
			wait_for_device(deviceID)

				
	
	deviceList=get_connected_devices()

	num = len(deviceList)
	log_eud("DEBUG", "number of devices connected: " + str(num))

	for deviceID in deviceList: 
		log_eud("INFO", "enabling EUD on deviceID " + str(deviceID))
		enable_eud(deviceID)
		execute_cmd(["adb", "-s", deviceID, "shell", "echo 1 > /sys/module/lpm_levels/parameters/sleep_disabled"])
		time.sleep(5)
	
	#deviceList = ['687e0ca', '687e0a1', '687e0c6', '687e076', '687e095', '622d130']
	
	start_time = time.time()
	while(options.test) : 
		stop_time = time.time()
		
		if int(stop_time) - int(start_time) > 3600:
			log_eud("INFO", "Resetting devices @ " + str(stop_time))
			reset_devices()
			
			start_time=time.time()
			log_eud("INFO", "starting timer once again @ " + str(start_time))
		
		num = len(get_connected_devices())
		if num is 0:
			log_eud("error", "no devices detected. Exiting the suite")
			exit(1)
		log_eud("INFO", "Number of devices connected: " + str(num))
		for deviceID in deviceList :
			log_eud("INFO", "enabling EUD on deviceID " + str(deviceID))
			enable_eud(deviceID)
			execute_cmd(["adb", "-s", deviceID, "shell", "echo 1 > /sys/module/lpm_levels/parameters/sleep_disabled"])
			log_eud("INFO", "###########checking for device: " + str(deviceID))
			get_devices()
			skip = False
			num = len(get_connected_devices())	
	#		disable_charger(deviceID)
	#		time.sleep(5)
	#		
	#		enable_charger(deviceID)
	#		time.sleep(5)
	#		
			detach_usb(deviceID)
			time.sleep(15)
			get_devices()
			num = num-1
			if "0x" + str(deviceID) in GetEUDAvailableDevices() :
				if str(deviceID) in get_connected_devices():
					log_eud("ERROR", "ABHIBAS: error while detaching usb for device -_- " + str(deviceID))
					#num = len(get_connected_devices())
					skip = True
				else :
					log_eud("INFO", "ABHIBAS: going good :) ")
			else : 
				log_eud("ERROR", "ABHIBAS: device EUD connection lost while usb detaching for device: " + str(deviceID) + ". Reboot or crash might have happened ")
				
	
			#if not skip: 
			attach_usb(deviceID)
			time.sleep(15)
			get_devices()
			num = num+1
			if "0x" + str(deviceID) in GetEUDAvailableDevices() :
				if str(deviceID) in get_connected_devices():
					log_eud("INFO", "going good")
				else :
					log_eud("ERROR", "error while attaching usb for device " + str(deviceID))
					num = len(get_connected_devices())
			else: 
				log_eud("ERROR", "ABHIBAS: device EUD connection lost while usb attaching for device: " + str(deviceID) + ". Reboot or crash might have happened ")			
			#else :
			#	log_eud("INFO", "skipping usb attach; since detach is failed")
			
		time.sleep(25)		
	
	
    
	
	