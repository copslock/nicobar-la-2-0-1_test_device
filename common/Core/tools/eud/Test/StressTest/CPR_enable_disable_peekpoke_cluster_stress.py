import traceback
import csv
import subprocess
import re
import time
import datetime
import sys
import os
#import numpy as np
from optparse import OptionParser

##====================--------------Global Variables----------------====================##
global UseTestMeta_Flag
global TestMetaDir
global currdir
currdir=os.path.dirname(os.path.abspath(__file__))
TestMetaDir = os.path.join(currdir,"..","..","Trace32","TestScripts")

if os.path.isdir(TestMetaDir):
    metapath=TestMetaDir
    UseTestMeta_Flag=True
else:
    UseTestMeta_Flag=False
    
if UseTestMeta_Flag is True:
    sys.path.append(os.path.join(currdir,"..","..","AutomationSamples"))
    
global DefaultAPIPort
global DefaultIntercomPort
DefaultIntercomPort=25400
DefaultAPIPort=15400
##====================----------------------------------------------====================##

#if len(sys.argv) > 2:
#	sys.stdout = open(str(int(time.time())) + ".log", 'a')

def log_eud(TAG, log_line):
	
	print(str(datetime.datetime.now()) + " : " + str(TAG) + " : " + str(log_line))

def is_device_online(devId):
    log_eud("internal", "checking the status of device " + str(devId))
    try :
        devicesList = get_connected_devices()
        if len(devicesList) > 0 and devId in devicesList:
            return True
    except :
        log_eud("internal", "error while getting the devices connected to the station ")
    return False

def insert_device_tag(file, devID):
	
	index = file.find('.')
	return file[:index] + "_" + str(devID) + file[index:]
	
def set_L3_corner() :
 
	execute_cmd(["adb", "-s", devId, "shell", "echo userspace > /sys/devices/platform/soc/soc:qcom,l3-cpu0/devfreq/soc:qcom,l3-cpu0/governor"])
	execute_cmd(["adb", "-s", devId, "shell", "echo 300000000 > /sys/devices/platform/soc/soc:qcom,l3-cpu0/devfreq/soc:qcom,l3-cpu0/min_freq"])
	execute_cmd(["adb", "-s", devId, "shell", "echo 300000000 > /sys/devices/platform/soc/soc:qcom,l3-cpu0/devfreq/soc:qcom,l3-cpu0/max_freq"])
	execute_cmd(["adb", "-s", devId, "shell", "echo userspace > /sys/devices/platform/soc/soc:qcom,l3-cpu4/devfreq/soc:qcom,l3-cpu4/governor"])
	execute_cmd(["adb", "-s", devId, "shell", "echo 300000000 > /sys/devices/platform/soc/soc:qcom,l3-cpu4/devfreq/soc:qcom,l3-cpu4/min_freq"])
	execute_cmd(["adb", "-s", devId, "shell", "echo 300000000 > /sys/devices/platform/soc/soc:qcom,l3-cpu4/devfreq/soc:qcom,l3-cpu4/max_freq"])
	
	log_eud("main", "results from L3 corner set")
	execute_cmd(["adb", "-s", devId, "shell", "cat /sys/devices/platform/soc/soc:qcom,l3-cpu0/devfreq/soc:qcom,l3-cpu0/governor"])
	execute_cmd(["adb", "-s", devId, "shell", "cat /sys/devices/platform/soc/soc:qcom,l3-cpu0/devfreq/soc:qcom,l3-cpu0/min_freq"])
	execute_cmd(["adb", "-s", devId, "shell", "cat /sys/devices/platform/soc/soc:qcom,l3-cpu0/devfreq/soc:qcom,l3-cpu0/max_freq"])
	execute_cmd(["adb", "-s", devId, "shell", "cat /sys/devices/platform/soc/soc:qcom,l3-cpu4/devfreq/soc:qcom,l3-cpu4/governor"])
	execute_cmd(["adb", "-s", devId, "shell", "cat /sys/devices/platform/soc/soc:qcom,l3-cpu4/devfreq/soc:qcom,l3-cpu4/min_freq"])
	execute_cmd(["adb", "-s", devId, "shell", "cat /sys/devices/platform/soc/soc:qcom,l3-cpu4/devfreq/soc:qcom,l3-cpu4/max_freq"])

	
def get_connected_devices():
	
    devicesData = subprocess.Popen(["adb", "devices"],stdout=subprocess.PIPE)
    
    devicesList = []
    log_eud("internal", devicesData.stdout)
    
    i= 5;
    while i>0:
        i = i-1
        line  = devicesData.stdout.readline()
        log_eud("internal", line)
        if(re.search(r'\bdevice\b', line)) :
            devicesList.append(line.split('device')[0].strip())
    return devicesList
			
def get_number_of_devices_connected() :

    devicesList = get_connected_devices()
    log_eud("internal", "number of devices connected is " + str(len(devicesList)))
    
    return len(devicesList)
    
def get_device_status(port) :
    retries = 5
    if get_number_of_devices_connected() == 0 :
        for i in range(retries) :
            log_eud("internal", "No devices found in the adb list")
            log_eud("internal", "resetting the device via alpaca")
            execute_cmd(["taskkill", "/f", "/im", "qpstConfig.exe"])
            execute_cmd(["perl", "alpaca_cmd.pl", "POWERON", port,'Serial']) 
            if os.path.exists("serial_text.txt") :
                log_eud("internal", "device is available via com port, not need to reboot")
                return 
            else :
                log_eud("internal", "device is completely off")
                execute_cmd(["perl", "alpaca_cmd.pl", "POWEROFF", port])
                execute_cmd(["perl", "alpaca_cmd.pl", "POWERON", port])
                log_eud("internal", "reset done via alpaca, sleeping for 120 secs ")
            
            
            time.sleep(120)
            if get_number_of_devices_connected() == 0 :
                if i == (retries-1) :
                    log_eud("internal", "unable to get the device back online after " + str(retries) + " retries. EXITING")
                    sys.exit()
                else :
                    continue;
            else :
                log_eud("internal", "device is back online after reboot ")
                return 1;
                
    else : 
        log_eud("internal", "devices are detected via adb, continue")
        return 
    #return len(devicesList)

def execute_cmd(cmd, out=True, redirect=None) :

    
    
    sanitized_cmd=[]
    if type(cmd) is list:
        for subcmd in cmd:
            sanitized_cmd.append(str(subcmd))
    else:
        sanitized_cmd.append(str(cmd))
    log_eud("internal", "executing command " + str(sanitized_cmd))    
    #if 'eudpeek' in str(sanitized_cmd).lower():
    #    import pdb; pdb.set_trace()
    devicesData = subprocess.Popen(sanitized_cmd, stdout=subprocess.PIPE)
    #devicesData.wait(40)
    #devicesData.wait(40)
    stdout, stderr = devicesData.communicate()
    
    if out : 
        if stdout is not None and len(stdout) is not 0:
            log_eud("internal", "stdout is " + str(stdout))
        if stderr is not None and len(stderr) is not 0:    
            log_eud("internal", "stderr is " + str(stderr))

	if redirect is not None :
		log_eud("DEBUG", "redirecting logs to " + str(redirect))
		with open(redirect, 'w') as file: 
			file.write(stdout)
	
def get_clock_measures(): 
    
    log_eud("MAIN", "clock measures ")
    execute_cmd(["adb", "-s", devId, "shell", "cat", "/d/clk/pwrcl_clk/clk_measure"]) 
    execute_cmd(["adb", "-s", devId, "shell", "cat", "/d/clk/perfcl_clk/clk_measure"])

def set_min_max_freq():

    log_eud("MAIN", "frequency at the beginning")

    execute_cmd(["adb", "-s", devId, "shell", "cat", "/sys/devices/system/cpu/cpu*/cpufreq/scaling_cur_freq"])
    
    log_eud("MAIN", "setting the max and min frequencies")

    execute_cmd(["adb", "-s", devId, "shell", "echo 300000  > /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq" ])
    execute_cmd(["adb", "-s", devId, "shell", "echo 300000  > /sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq" ])
    execute_cmd(["adb", "-s", devId, "shell", "echo 300000  > /sys/devices/system/cpu/cpu2/cpufreq/scaling_min_freq" ])
    execute_cmd(["adb", "-s", devId, "shell", "echo 300000  > /sys/devices/system/cpu/cpu3/cpufreq/scaling_min_freq" ])
    execute_cmd(["adb", "-s", devId, "shell", "echo 1747200 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq" ])
    execute_cmd(["adb", "-s", devId, "shell", "echo 1747200 > /sys/devices/system/cpu/cpu1/cpufreq/scaling_max_freq" ])
    execute_cmd(["adb", "-s", devId, "shell", "echo 1747200 > /sys/devices/system/cpu/cpu2/cpufreq/scaling_max_freq" ])
    execute_cmd(["adb", "-s", devId, "shell", "echo 1747200 > /sys/devices/system/cpu/cpu3/cpufreq/scaling_max_freq" ])


    log_eud("MAIN", "setting the max and min to one corner")

    execute_cmd(["adb", "-s", devId, "shell", "echo 1747200 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"])
    execute_cmd(["adb", "-s", devId, "shell", "echo 1747200 > /sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq" ])
    execute_cmd(["adb", "-s", devId, "shell", "echo 1747200 > /sys/devices/system/cpu/cpu2/cpufreq/scaling_min_freq" ])
    execute_cmd(["adb", "-s", devId, "shell", "echo 1747200 > /sys/devices/system/cpu/cpu3/cpufreq/scaling_min_freq" ])
    execute_cmd(["adb", "-s", devId, "shell", "echo 1747200 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq" ])
    execute_cmd(["adb", "-s", devId, "shell", "echo 1747200 > /sys/devices/system/cpu/cpu1/cpufreq/scaling_max_freq" ])
    execute_cmd(["adb", "-s", devId, "shell", "echo 1747200 > /sys/devices/system/cpu/cpu2/cpufreq/scaling_max_freq" ])
    execute_cmd(["adb", "-s", devId, "shell", "echo 1747200 > /sys/devices/system/cpu/cpu3/cpufreq/scaling_max_freq" ])
	


    log_eud("MAIN", "frequency after setting for silver cluster")

    execute_cmd(["adb", "-s", devId, "shell", "cat /sys/devices/system/cpu/cpu[0-3]/cpufreq/scaling_cur_freq"])
    log_eud("MAIN", "setting L3 corner to 300 MHz")
	
    set_L3_corner()
	
    
def set_HLOS_Vmin():

    execute_cmd(["adb", "-s", devId, "shell", "cat /sys/devices/system/cpu/cpu0/core_ctl/min_cpus"])
    #execute_cmd(["adb", "-s", devId, "shell", "echo 0 > /sys/devices/system/cpu/cpu0/core_ctl/min_cpus"]) # not working to hotplug the core 
    #execute_cmd(["adb", "-s", devId, "shell", "echo 0 > /sys/devices/system/cpu/cpu0/core_ctl/max_cpus"]) # not working to hotplug the core 
    execute_cmd(["adb", "-s", devId, "shell", "echo 4 > /sys/devices/system/cpu/cpu0/core_ctl/min_cpus"])
    execute_cmd(["adb", "-s", devId, "shell", "echo 4 > /sys/devices/system/cpu/cpu0/core_ctl/max_cpus"])
    #execute_cmd(["adb", "-s", devId, "shell", "cat /sys/devices/system/cpu/cpu4/core_ctl/min_cpus"])
    execute_cmd(["adb", "-s", devId, "shell", "cat /sys/devices/system/cpu/cpu*/online"])

    execute_cmd(["adb", "-s", devId, "shell", "stop perfd"])
    execute_cmd(["adb", "-s", devId, "shell", "stop thermald"])
    execute_cmd(["adb", "-s", devId, "shell", "stop thermal-engine"])
    execute_cmd(["adb", "-s", devId, "shell", "stop core_ctl"])

    execute_cmd(["adb", "-s", devId, "shell", "lsmod"])

    execute_cmd(["adb", "-s", devId, "root"])
    execute_cmd(["adb", "-s", devId, "remount"])
    execute_cmd(["adb", "-s", devId, "shell", "mount -o remount,rw -t rootfs rootfs /"])
    execute_cmd(["adb", "-s", devId, "shell", "ps | grep core_ctl"])
    execute_cmd(["adb", "-s", devId, "shell", "ps | grep perfd"])


    execute_cmd(["adb", "-s", devId, "shell", "echo 1 > /sys/devices/system/cpu/cpu0/online"])
    execute_cmd(["adb", "-s", devId, "shell", "echo 1 > /sys/devices/system/cpu/cpu1/online"])
    execute_cmd(["adb", "-s", devId, "shell", "echo 1 > /sys/devices/system/cpu/cpu2/online"])
    execute_cmd(["adb", "-s", devId, "shell", "echo 1 > /sys/devices/system/cpu/cpu3/online"])
    execute_cmd(["adb", "-s", devId, "shell", "echo 0 > /sys/devices/system/cpu/cpu4/online"])
    execute_cmd(["adb", "-s", devId, "shell", "echo 0 > /sys/devices/system/cpu/cpu5/online"])
    execute_cmd(["adb", "-s", devId, "shell", "echo 0 > /sys/devices/system/cpu/cpu6/online"])
    execute_cmd(["adb", "-s", devId, "shell", "echo 0 > /sys/devices/system/cpu/cpu7/online"])
    
    execute_cmd(["adb", "-s", devId, "shell", "cat /sys/devices/system/cpu/online"])
    execute_cmd(["adb", "-s", devId, "shell", "cat /sys/devices/system/cpu/cpu*/online"])
    
    execute_cmd(["adb", "-s", devId, "shell", "echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"])
    execute_cmd(["adb", "-s", devId, "shell", "echo performance > /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor"])
    execute_cmd(["adb", "-s", devId, "shell", "echo performance > /sys/devices/system/cpu/cpu2/cpufreq/scaling_governor"])
    execute_cmd(["adb", "-s", devId, "shell", "echo performance > /sys/devices/system/cpu/cpu3/cpufreq/scaling_governor"])
    execute_cmd(["adb", "-s", devId, "shell", "echo performance > /sys/devices/system/cpu/cpu4/cpufreq/scaling_governor"])
    execute_cmd(["adb", "-s", devId, "shell", "echo performance > /sys/devices/system/cpu/cpu5/cpufreq/scaling_governor"])
    execute_cmd(["adb", "-s", devId, "shell", "echo performance > /sys/devices/system/cpu/cpu6/cpufreq/scaling_governor"])
    execute_cmd(["adb", "-s", devId, "shell", "echo performance > /sys/devices/system/cpu/cpu7/cpufreq/scaling_governor"])
    
    execute_cmd(["adb", "-s", devId, "shell", "cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor"])

def get_ADB_voltages():

    execute_cmd(["adb", "-s", devId, "shell", "echo 0x3840 > /sys/kernel/debug/regmap/spmi0-01/address"])
    execute_cmd(["adb", "-s", devId, "shell", "echo 4 > /sys/kernel/debug/regmap/spmi0-01/count"])
    execute_cmd(["adb", "-s", devId, "shell", "cat /sys/kernel/debug/regmap/spmi0-01/data"], True)
	
def del_file_out(file_name):

    try :
        log_eud("internal", "deleting file " + str(file_name)) 
        os.remove(file_name)
    except :
        log_eud("error", "NO file found @ " + str(file_name))


def get_file_out(file_name):

    try :
        #execute_cmd(["cat", file_name], True)
        with open(file_name, 'r') as file: 
            for line in file:
                log_eud("internal", line)
        os.remove(file_name)
    except :
        if os.path.isfile(file_name):
            log_eud("error", "File found but got error with command 'cat "+str(filename)+"'")
        else:
            log_eud("error", "NO file found @ " + str(file_name))
        traceback.print_exc()

def enable_eud(deviceID):
    
    execute_cmd(["adb", "-s", deviceID, "root"])
    execute_cmd(["adb", "-s", deviceID, "remount"])
    execute_cmd(["adb", "-s", deviceID, "shell", "echo 1 > /sys/module/eud/parameters/enable"])
    execute_cmd(["adb", "-s", deviceID, "shell", "cat /sys/module/eud/parameters/enable"])
        
def get_cpr_from_adb():
    
    execute_cmd(["adb", "-s", devId, "shell", "cat /d/cpr3-regulator/apc0/cpr_closed_loop_enable"], True)
        
def kill_t32():
    
#    log_eud("internal", "killing t32 process if any ")
    log_eud("internal", "skipping t32 process kill for multiple devices debug")
#    execute_cmd(["taskkill", "/f", "/im", "t32marm64.exe"])
    log_eud("internal", "executed t32 process kill command ")

def get_voltage_readings_T32(core) :

    log_eud("INFO", "reading voltage for core via T32 " + str(core))
    read_file1 = insert_device_tag(read_file, str(devID))
    del_file_out(read_file1)	
    kill_t32()
    log_eud("INFO", "Executing file read_file " + str(core))
    
    #execute_cmd(["cat", os.path.join(CMMScriptslocation,"Write_Voltage.cmm")])
    if core == "cpr6":
        execute_cmd(["python", EUDPeekPokeLocation, "-c", os.path.join(CMMScriptslocation,"Read_Voltage_silver.cmm"), "-o", "C:\Temp", "-d", devID, "-a", apiPort, "-m", intercomPort,"-t", "60", "-e", read_file1, "-s", t32close_flag], redirect=debug_file1)        
    else :
        execute_cmd(["python", EUDPeekPokeLocation, "-c", os.path.join(CMMScriptslocation,"Read_Voltage_gold.cmm"), "-o", "C:\Temp", "-d", devID, "-a", apiPort, "-m", intercomPort,"-t", "60", "-e", read_file1, "-s", t32close_flag], redirect=debug_file1)        
    get_file_out(read_file1)	
    
def get_voltages_readings():
	
    for i in range(5):

        get_ADB_voltages()
        get_voltage_readings_T32("cpr6")
        time.sleep(5)

    #execute_cmd(cmd=['adb', 'devices'])

def enable_CPR(core):

    log_eud("INFO", "enabling CPR for core " + str(core))
    kill_t32()
    enable_cpr_file1 = insert_device_tag(enable_cpr_file, str(devID))
    del_file_out(enable_cpr_file1)
    if core == "cpr6":
        execute_cmd(["python", EUDPeekPokeLocation, "-c", os.path.join(CMMScriptslocation,"Enable_CPR_silver.cmm"), "-o", "C:\Temp", "-d", devID, "-a", apiPort, "-m", intercomPort,"-t", "20", "-e", enable_cpr_file1, "-s", t32close_flag], redirect=debug_file1)
        get_file_out(enable_cpr_file1)
    else: 
        execute_cmd(["python", EUDPeekPokeLocation, "-c", os.path.join(CMMScriptslocation,"Enable_CPR_gold.cmm"), "-o", "C:\Temp", "-d", devID, "-a", apiPort, "-m", intercomPort,"-t", "20", "-e", enable_cpr_file1, "-s", t32close_flag], redirect=debug_file1)
        get_file_out(enable_cpr_file1)    

def disable_CPR(core):

    log_eud("INFO", "disabling CPR for core " + str(core))
    kill_t32()
    disable_cpr_file1 = insert_device_tag(disable_cpr_file, str(devID))
    del_file_out(disable_cpr_file1)
    if core == "cpr6":
        execute_cmd(["python", EUDPeekPokeLocation, "-c", os.path.join(CMMScriptslocation,"Disable_CPR_silver.cmm"), "-o", "C:\Temp", "-d", devID, "-a", apiPort, "-m", intercomPort,"-t", "20", "-e", disable_cpr_file1, "-s", t32close_flag], redirect=debug_file1)
        get_file_out(disable_cpr_file1)
    else: 
        execute_cmd(["python", EUDPeekPokeLocation, "-c", os.path.join(CMMScriptslocation,"Disable_CPR_gold.cmm"), "-o", "C:\Temp", "-d", devID, "-a", apiPort, "-m", intercomPort,"-t", "20", "-e", disable_cpr_file1, "-s", t32close_flag], redirect=debug_file1)
        get_file_out(disable_cpr_file1) 

		
def get_cpr_status(core):
    
    get_cpr_from_adb()
    get_cpr_status_T32(core)

def get_cpr_status_T32(core):

    log_eud("INFO", "getting CPR status for core " + str(core))
    kill_t32()
    cpr_status_file1 = insert_device_tag(cpr_status_file, devID)	
    del_file_out(cpr_status_file1)
    if core == "cpr6":
        execute_cmd(["python", EUDPeekPokeLocation, "-c", os.path.join(CMMScriptslocation,"CPR_Status_silver.cmm"), "-o", "C:\Temp", "-d", devID, "-a", apiPort, "-m", intercomPort,"-t", "60", "-e", cpr_status_file1, "-s", t32close_flag], redirect=debug_file1)
    else:
        execute_cmd(["python", EUDPeekPokeLocation, "-c", os.path.join(CMMScriptslocation,"CPR_Status_gold.cmm"), "-o", "C:\Temp", "-d", devID, "-a", apiPort, "-m", intercomPort,"-t", "60", "-e", cpr_status_file1, "-s", t32close_flag], redirect=debug_file1)
    get_file_out(cpr_status_file1)


def set_voltage_T32(core):

    log_eud("INFO", "setting voltage for core via T32 " + str(core))
    read_write_file1 = insert_device_tag(read_write_file, str(devID))
    write_voltage_file1 = insert_device_tag(write_voltage_file, str(devID))
    kill_t32()
    del_file_out(read_write_file1)
    execute_cmd(["cat", os.path.join(CMMScriptslocation, write_voltage_file1)])
    #if core == "cpr6":
    execute_cmd(["python", EUDPeekPokeLocation, "-c", os.path.join(CMMScriptslocation, write_voltage_file1), "-o", "C:\Temp", "-d", devID, "-a", apiPort, "-m", intercomPort,"-t", "100", "-e", read_write_file1, "-s", t32close_flag], redirect=debug_file1) 
    #else :
    #    execute_cmd(["python", EUDPeekPokeLocation, "-c", os.path.join(CMMScriptslocation, write_voltage_file1), "-o", "C:\Temp", "-d", devID, "-a", apiPort, "-m", intercomPort,"-t", "100", "-e", read_write_file]) 
	#time.sleep(regular_sleep_time)
    get_file_out(read_write_file1) 
	
	
def loop_write():

    for parent_iter in range(5) :
        sleep_time = 150
        regular_sleep_time = 5
        
        log_eud("MAIN", "rebooting device ")
        
        #execute_cmd(["adb", "-s", devId, "reboot"])
        
        log_eud("MAIN", "sleeping for " + str(sleep_time))
        #time.sleep(sleep_time)
        
       # get_device_status(port)
        execute_cmd(["adb", "-s", devId, "root"])
	
        execute_cmd(["adb", "-s", devId, "remount"])
		
        execute_cmd(["adb", "-s", devId, "shell", "input keyevent 82"])
        execute_cmd(["adb", "-s", devId, "shell", "input keyevent 82"])
        execute_cmd(["adb", "-s", devId, "shell", "am start com.qualcomm.adrenotest/android.app.NativeActivity"])

		
        log_eud("MAIN", "#############################################################################")
        
        log_eud("MAIN", "parent iteration is " + str(parent_iter))
        
        log_eud("MAIN", "clock measures at the beginning ")
        get_clock_measures()

        log_eud("MAIN", "cpr status at the beginning ")
        get_cpr_from_adb()

        time.sleep(20)
        log_eud("MAIN", "setting the clock freq min to min and max to max and the clock freq to Turbo for silver core ")
        set_min_max_freq()  # TODO// separate this from setting the turbo 
		
        time.sleep(20)
        log_eud("MAIN", "clock values after setting the freqs")
        get_clock_measures()
        
        log_eud("MAIN", "pre reqs for CPR disable")
        set_HLOS_Vmin()
        time.sleep(20)
        
        enable_eud(devId)    

            
        for iter in range(1) :
        
    
            log_eud("MAIN", "child iterations LOOP1 is " + str(iter))
            log_eud("MAIN", "********************************************")

            log_eud("MAIN", "values from ADB for CPR (0/1) status are ")
            
            get_cpr_status("cpr6") # silver

			# TODO// add the CPR status from T32
            
            log_eud("MAIN", "voltage values at initial stage ")
            
            get_voltages_readings()
            
            kill_t32()
            
            log_eud("MAIN", "Disabling CPR ")
            disable_CPR("cpr6") # silver
        
            time.sleep(regular_sleep_time)
            log_eud("MAIN", "CPR status after disable via EUD")
            
            get_cpr_status("cpr6")
            
            log_eud("MAIN", "voltage values when CPR is diabled via EUD")
            get_voltages_readings()
            
            set_voltage_T32("cpr6")
                
            log_eud("MAIN", "voltage values after voltage set ")

            get_voltages_readings()
 
        kill_t32()
        
        get_cpr_status("cpr6")
def getdeviceid():
    import EUDPythonAPIs
    deviceid = EUDPythonAPIs.SelectEUDDevice()
    deviceid=hex(deviceid)
    deviceid=deviceid.replace('0x','')
    if "L" in deviceid[-1]:
        deviceid=deviceid[:-1]
        
    return deviceid
def parseoptions():
    """
        Parse out given user options
    """
    argparser = OptionParser(version='%prog 1.0')
    argparser.add_option('-d','--deviceid', dest='deviceid',     help="device ID",default=None)
    argparser.add_option('-t','--metapath', dest='metapath',     help="metabuildpath",default=None)
    argparser.add_option('-j','--job', dest='action',     help="Job to perform",default=None)
    argparser.add_option('-a','--apiport', dest='apiPort',     help="apiPort for T32 device connection",default=None)	
    argparser.add_option('-m','--intercomport', dest='intercomPort',     help="intercomPort for T32 device connection",default=None)	
    argparser.add_option('-c','--cpr', dest='cpr',     help="cpr value (cpr6 for silver, cpr7 for gold)",default=None)
    argparser.add_option('-s','--t32close', dest='t32close',     help="cpr value (cpr6 for silver, cpr7 for gold)",default=True)
	
    #If -h/--help specified, print out description
    '''
    for arg in sys.argv[1:]:
        if ('-h' in arg) or ('--help' in arg):
            print EUDMSMAssertReset.__doc__
    '''        
    
    (options,args)=argparser.parse_args()
    
    if options.deviceid is None:
        if UseTestMeta_Flag is True:
            options.deviceid = getdeviceid()
    
    if options.deviceid is None:
        print("Error! Need device ID")
        sys.exit(1)
    
    if options.metapath is None:
        if UseTestMeta_Flag is True:
            options.metapath=TestMetaDir
        else:
            print("Error! Need metabuild path")
            sys.exit(1)
    #if options.action is None:
    #    print("Error! Need action to perform")
    #    sys.exit(1)
    #if options.cpr is None:
    #    print("Error! Need cpr value ")
    #    sys.exit(1)
    if options.apiPort is None:
        if UseTestMeta_Flag is True:
            options.apiPort=DefaultAPIPort
        else:
            print("Error! Need apiPort to perform")
            sys.exit(1)
    if options.intercomPort is None:
        if UseTestMeta_Flag is True:
            options.intercomPort=DefaultIntercomPort
        else:
            print("Error! Need intercomPort value ")
            sys.exit(1)		
    print("meta path given is " + str(options.metapath))
    if not os.path.isdir(os.path.join(options.metapath,"common","core","t32","shellscripts")) :
        print("Error! Could not find common/core/t32/shellscripts dictory from given meta path: " + str(options.metapath))
        sys.exit(1)
        
    return options
		

def main() :

	
		


#	with open(str(int(time.time())) + "_output_silver.csv", 'a') as csvfile:
#		writer = csv.writer(csvfile)
	#is_device_online(devId)

	while True :
    
	    #loop_read()
		loop_write()
		
		
options=parseoptions()
devID = options.deviceid
devId = options.deviceid
metapath = options.metapath
apiPort = options.apiPort
intercomPort = options.intercomPort
t32close_flag = options.t32close


    
os.chdir(os.path.join(metapath,'common','core','t32','shellscripts'))
scriptpath=os.path.join(metapath,'common','core','t32','shellscripts')

sys.path.append(os.path.join(scriptpath,"..","shellscripts"))
sys.path.append(os.path.join(scriptpath,"..","shellscripts","EUDPeekPoke"))
if UseTestMeta_Flag is True:
    EUDPeekPokeLocation=os.path.join(currdir,"..","..","AutomationSamples","EUDPeekPoke.py")
else:
    EUDPeekPokeLocation=os.path.join(scriptpath,"..","shellscripts","EUDPeekPoke","EUDPeekPoke.py")
    

if  UseTestMeta_Flag is True:
    CMMScriptslocation=os.path.join(currdir,"CPR_peekpoke_stress")
else:
    CMMScriptslocation=os.path.join("C:\\Users","asiahyd\\Desktop\\eSLT_v2_Dev_enhanced\\PythonModules")

read_write_file = r"C:\Temp\logs_read_write.log"
read_file = r"C:\Temp\Pmicout.log"
disable_cpr_file = r"C:\Temp\DISABLE_CPR.txt"
enable_cpr_file = r"C:\Temp\ENABLE_CPR.txt"
cpr_status_file = r"C:\Temp\CPR_STATUS.txt"
write_voltage_file = r"Write_Voltage.cmm"	
debug_redirect_file = r"C:\Temp\Peekpoke_debug_logs.txt"
debug_file1 = insert_device_tag(debug_redirect_file, str(devID))
args = sys.argv
log_eud("MAIN", "input args are " + str(args))
		
main()