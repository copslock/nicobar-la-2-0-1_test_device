//////////////////////////////////////////////////
///
/// @file eud_tst.cmm
//  @brief Small break-test-attach stress test for EUD
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when               who           what, where, why
// --------           ---           -----------------------------
// 01/05/2018         JBILLING      Updated for automation and tracking
// 12/01/2017         LTB           Created

LOCAL &argline
ENTRY %LINE &argline
GLOBAL &OUTPUTFILE
&OUTPUTFILE="&argline"

LOCAL &DefaultOutputFile
&DefaultOutputFile="C:\temp\eud_tst_output.txt"
IF ("&OUTPUTFILE"=="")
(
    &OUTPUTFILE="&DefaultOutputFile"
)

IF OS.FILE("&OUTPUTFILE")
(
    OS.COMMAND del &OUTPUTFILE
)

GOSUB MAIN
LOCAL &rvalue
ENTRY %LINE &rvalue

GOSUB EXIT &rvalue


//////////////////Subroutines////////////////////
MAIN:
    LOCAL &DefaultAccessMode
    &DefaultAccessMode="EZAXI"



    IF (!SYStem.Up())
    (
      SYS.M.A
    )
    
    IF (!SYSTEM.UP())
    (
      PRINT "ERROR: SYStem not up"
      GOSUB EXIT FAILURE - system not up
    )
    //Disable apps power collapse.
    do apps_power_options disable

    ; check if system is halted
    IF (STATE.RUN())
    (
      GOSUB GUARDEDBREAK
      WAIT 100.ms
    )
    ; check if system is halted
    IF (STATE.RUN())
    (
      PRINTS "ERROR: SYStem not halted!"
      GOSUB EXIT FAILURE - system not halted
    )

    ON ERROR GOSUB
    (
        ON ERROR DEFAULT
        GOSUB EXIT FAILURE - error ocurred during eud_test.cmm break/go
    )


    GOSUB TESTDATA 0x96B00000 0x0f NSP
    WAIT 200.ms

    GOSUB SETDATA 0x96B00000 0xb16babe NSP

    GOSUB GUARDEDGO

    WAIT 200.ms
    GOSUB GUARDEDBREAK
    Break
    WAIT 200.ms
    GOSUB GUARDEDGO
    WAIT 200.ms
    Break
    WAIT 200.ms
    GOSUB SETDATA 0x96B00000 0xb16babe eNSP
    //D.S eNSP:0x96B00000 %LE %Long 0xb16babe
    GOSUB TESTDATA 0x96B00000 0x0f NSP
    //Data.Test NSP:0x96B00000++0x0f 
    GOSUB PRINTDATA 0x96B00000 ENSP
    //print data.long(eNSP:0x96B00000)

    GOSUB SETDATA 0x96B00008 0x4b16babe eNSP
    //D.S eNSP:0x96B00008 %LE %Long 0x4b16babe
    GOSUB PRINTDATA 0x96B00000 ENSP
    //print data.long(eNSP:0x96B00008)

    GOSUB GUARDEDGO
    GOSUB PRINTDATA 0x96B00000 ENSP
    //print data.long(eNSP:0x96B00008)
    GOSUB TESTDATA 0x96B00000 0x0f ENSP
    //Data.Test ENSP:0x96B00000++0x0f 

    GOSUB GUARDEDBREAK
    wait 200ms
    GOSUB GUARDEDGO

    RETURN SUCCESS

GUARDEDGO:
    LOCAL &callingline 
    &callingline=PRACTICE.CALLER.LINE(1)
    ON ERROR  GOSUB
    (
        ON ERROR DEFAULT
        GOSUB  EXIT "FAILURE - GO ran into error. Calling line: &callingline"
    )
    
    GO
    
    ON  ERROR  DEFAULT
    IF STATE.RUN()!=TRUE()
    (
        GOSUB EXIT "FAILURE - GO did not succeed. Calling line: &callingline"
    )
    RETURN SUCCESS
    
GUARDEDBREAK:
    LOCAL &callingline 
    &callingline=PRACTICE.CALLER.LINE(1)
    ON ERROR  GOSUB
    (
        GOSUB  EXIT "FAILURE - BREAK ran into error. Calling line: &callingline"
    )
    
    BREAK
    
    ON  ERROR  DEFAULT
    IF STATE.RUN()==TRUE()
    (
        GOSUB EXIT "FAILURE - BREAK did not succeed. Calling line: &callingline"
    )
    RETURN SUCCESS

PRINTDATA:
    LOCAL &address &accessmode &argline &subroutine &callingline
    ENTRY &address &accessmode
    ENTRY %LINE &argline
    &subroutine="PRINTDATA"
    &callingline=PRACTICE.CALLER.LINE(1)
    
    IF "&accessmode"==""
    (
        &accessmode="&DefaultAccessMode"
    )
    ON ERROR  GOSUB
    (
        ON ERROR DEFAULT
        GOSUB  EXIT "FAILURE - &subroutine &argline. Calling Line: &callingline"
    )
    
    PRINT DATA.LONG(&accessmode:&address)
    
    ON ERROR DEFAULT
    RETURN SUCCESS
    
    
SETDATA:
    LOCAL &address &value &accessmode &argline &subroutine &callingline
    ENTRY &address &value &accessmode
    ENTRY %LINE &argline
    &subroutine="SETDATA"
    &callingline=PRACTICE.CALLER.LINE(1)
    
    IF "&accessmode"==""
    (
        &accessmode="&DefaultAccessMode"
    )
    ON ERROR  GOSUB
    (
        ON ERROR DEFAULT
        GOSUB  EXIT "FAILURE - &subroutine &argline. Calling Line: &callingline"
    )
    DATA.SET NSP:&address %LE %LONG &value
    LOCAL &readvalue
    &readvalue=DATA.LONG(&accessmode:&address)
    PRINT "SETDATA: setvalue: &value. readvalue: &readvalue"
    IF &readvalue!=&value
    (
        GOSUB EXIT "Failure - &subroutine readvalue (&readvalue) did not match  set value (&value). Calling line: &callingline"
    )
    ON  ERROR  DEFAULT
    RETURN SUCCESS
    
TESTDATA:
    LOCAL &address &value &accessmode &argline &subroutine &callingline
    ENTRY &address &value &accessmode
    ENTRY %LINE &argline
    &subroutine="TESTDATA"
    &callingline=PRACTICE.CALLER.LINE(1)
    
    IF "&accessmode"==""
    (
        &accessmode="&DefaultAccessMode"
    )
    
    ON ERROR  GOSUB
    (
        ON ERROR DEFAULT
        GOSUB  EXIT "FAILURE - &subroutine &argline. Calling Line: &callingline"
    )
    
    DATA.TEST &accessmode:&address++&value
    
    ON  ERROR  DEFAULT
    RETURN SUCCESS
    

    
EXIT:
    LOCAL &status
    ENTRY %LINE &status

    OPEN #1 &OUTPUTFILE /Create
    WRITE #1 "Program ended with status:"+"&status"
    CLOSE #1
    ENDDO &status
