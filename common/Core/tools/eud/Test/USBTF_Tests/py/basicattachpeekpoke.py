
#====================================================================
#  Created on Thu June 29 115424 2017
#  Name
#    EUDPeekPoke.py
#
#  Description
#    Opens T32 and runs provided cmm or cfg script
#
#  Usage
#      'python EUDPeekPoke.py -c configfile -o outputdir -t time 
#                             -w t32window -e endofprogramfile -d deviceid 
#                             -a apiport -m intercomport
#
# Copyright (c) 2017 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
#
#
#                      EDIT HISTORY FOR FILE
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when               who           what, where, why
# --------           ---           -----------------------------
# 11172017         JBILLING      Additional options (-s,-n) for window closure options
# 10272017         JBILLING      Added multi device support
# 06292017         KCHIMA        Created
#

import os
import sys
import xml
import xml.etree.ElementTree
import datetime
import ctypes
from ctypes import *
import argparse
import unittest
from optparse import OptionParser

DefaultEOPFile="C:\\temp\\attachresult.txt"
DefaultOutputDir="C:\\temp"
global CurrDir
CurrDir=os.path.dirname(os.path.abspath(__file__))
#BasicAttachTestCMM = os.path.join(CurrDir,"..","cmm","basicattachtest.cmm")


def kill (msg=None):
    if msg:
        print str(msg)
    sys.exit(1)
    

#def test(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
#    rvalue=SUCCESS
#    rvalue=EUDMSMAssertReset(EUD_dll=EUD_dll,deviceID=deviceID,debugflag=options.debugflag,FPGAFlag=options.FPGAFlag)
#    if rvalue == 0x0:
#        print "SUCCESS"
#    else:
#        print "FAILURE"
#    return rvalue
    
def PassandReadCMM(metabuild=None,cmmfile=None,deviceid=None,intercomport=None,apiport=None,output=DefaultOutputDir,eopfile=DefaultEOPFile):
    if not os.path.isdir(metabuild):
        kill("Error! Metabuild not accessible")
    if not deviceid:
        kill("Error! deviceID not valid!")
    if not intercomport:
        kill("Error! intercomport not given")
    if not apiport:
        kill("Error! apiport not given")
    if not cmmfile:
        kill("Error! cmmfile not given")
        
    if not os.path.isfile(cmmfile):
        if os.path.isfile(os.path.join(CurrDir,cmmfile)):
            cmmfile=os.path.join(CurrDir,cmmfile)
        else:
            kill("Error! Could not locate cmmfile:"+str(cmmfile))
            
            
    path = os.path.join(options.metabuild,"common","core","t32","shellscripts","EUDPeekPoke")
    if not os.path.isdir(path):
        kill("Error! EUDPeekPoke path not available from given metabuild")
    os.chdir(path)
    commandlist=[]
    commandlist.append("python")
    commandlist.append(os.path.join(metabuild,"common","Core","t32","shellscripts","EUDPeekPoke","EUDPeekPoke.py"))
    commandlist.append("-o "+str(options.output))
    commandlist.append("-c "+str(cmmfile))
    commandlist.append("-d "+str(options.deviceid))
    commandlist.append("-m "+str(options.intercomport))
    commandlist.append("-a "+str(options.apiport))
    commandlist.append("-e "+str(options.eopfile))
    commandlist.append("-n False") ##Don't run init scripts.
    
    command=" ".join(commandlist)
    
    try:
        os.system(command)
        print(command)
    finally:
        
        finalfile=os.path.join(options.output,options.eopfile)
        
        if not os.path.isfile(finalfile):
            print "FAILURE - output file not found"
            return "FAILURE - output file not found"
        with open(finalfile) as f:
            lines=f.readlines()
            
        rvalue=" ".join(lines)
        if "SUCCESS" in rvalue.upper():
            print "SUCCESS"
            return "SUCCESS"
        else:
            print str(rvalue)
            return rvalue

def parseoptions():
    """
        Parse out given user options
    """
    argparser = OptionParser(version='%prog 1.0')
    argparser.add_option('-b','--metabuild',    dest='metabuild',   help="Metabuild to use. Required.",default=None)
    argparser.add_option('-c','--cmmfile',    dest='cmmfile',   help="CMM File to pass. Required.",default=None)
    argparser.add_option('-d','--deviceid',     dest='deviceid',    help="32bit deviceID value. See EUDDisplayAvailableDevices.py. Required.",default=None)
    argparser.add_option('-m','--intercomport', dest='intercomport',help="intercomport number. Passed to EUDPeekPoke.py. Required.",default=None)
    argparser.add_option('-a','--apiport',      dest='apiport',     help="api port number. Passed to EUDPeekPoke.py. Required.",default=None)
    argparser.add_option('-o','--output',       dest='output',      help="Output directory. Passed to EUDPeekPoke.py. Defaults to "+str(DefaultOutputDir),default=DefaultOutputDir)
    argparser.add_option('-e','--eopfile',      dest='eopfile',     help="File to be printed by T32 when operation done. Defaults to "+str(DefaultEOPFile),default=DefaultEOPFile)
    
    #If -h/--help specified, print out description
    for arg in sys.argv[1:]:
        if ('-h' in arg) or ('--help' in arg):
            print basicattachpeekpoke.__doc__
            
    (options,args)=argparser.parse_args()
            

        
    return options
    
if __name__ == '__main__':
    options=parseoptions()
    
    PassandReadCMM(     metabuild=options.metabuild,
                        deviceid=options.deviceid,
                        intercomport=options.intercomport,
                        apiport=options.apiport,
                        output=options.output,
                        eopfile=options.eopfile
                        )
            
