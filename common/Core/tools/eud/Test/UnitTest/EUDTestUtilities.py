﻿#!/user/bin/env python
'''    
    @file EUDTestUtilties.py
    @brief Various utilites for EUD python scripts to use (e.g. color printing  etc)
    @author JBILLING    
    @bug 
    
    Prerequisites:    
            Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.    
            EUD.dll must be in same directory or in ../../EUD/bin/x64/Debug
    
    Available API's:    
        colorprint
        verboseprint
     
'''
######################################################################################
##
##        @file EUDTestUtilties.py
##        @brief Various utilites for EUD python scripts to use (e.g. color printing  etc)
##        @author JBILLING    
##        @bug 
##
##        Prerequisites:
##                Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
##                EUD.dll must be in same directory or in ../../EUD/bin/x64/Debug
##
##        Available API's:
##            colorprint
##            verboseprint
##
##                             EDIT HISTORY FOR FILE
##        This section contains comments describing changes made to the module.
##           when       who             what, where, why
##           --------   ---             ---------------------------------------------------------
##           07/03/2017 JBILLING        Added verboseprint2
##           01/11/2017 JBILLING        Changed Initialization API names
##           10/01/2016 JBILLING        Created
##
######################################################################################
import sys
import os
import pdb
from platform import system
os_platform = system()

#Defines for dll 
global debug
debug=True
#debug=False
global SUCCESS, FAILURE
SUCCESS=0
FAILURE=-1




def SetupEnvironment():
    '''
        SetupEnvironment: Internal utility to set up Python environment for EUD API's
    '''
    ##====---------------------------------------------------====##
    ##====                Global Variables                   ====##
    ##====---------------------------------------------------====##

    

    global SUCCESS, FAILURE
    SUCCESS=0
    FAILURE=-1
    
    EnvSetup=True
    import EUDPythonAPIs    
    return

currentdir = os.path.dirname(__file__)

##This is where EUDPythonAPIs live
sys.path.append(os.path.abspath(os.path.join(currentdir,"..")))
import EUDPythonAPIs


def colorprint(string,color):
    '''
    colorprint
        Utility API.
        Prints given message string in desired color to windows console
        Available colors: 'red', 'yellow', 'green', 'blue', 0 (default).

        
    Arguments:
        string - error string to print
        color - desired color in text. 
            Available colors: 'red', 'yellow', 'green', 'blue', 0 (default).
    Returns:
        None


    '''
    if os_platform == 'Windows':
        color_code_black=7
        if str(color) in "red":
            color_code=12
        elif str(color) in "yellow":
            color_code=30
        elif str(color) in "green":
            color_code=26
        elif str(color) in "blue":
            color_code=9
        #default to red
        else:
            color_code=0
            
        #print string
        #return        
        import ctypes
        STD_OUTPUT_HANDLE = -11
        stdout_handle = ctypes.windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
        ctypes.windll.kernel32.SetConsoleTextAttribute(stdout_handle,color_code)
        
        print string
        #restore to black
        #stdout_handle = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
        ctypes.windll.kernel32.SetConsoleTextAttribute(stdout_handle,color_code_black)
        #print " "
        
    elif os_platform == 'Linux':
        color_code_end = '\033[0m'
        
        if str(color) in "red":
            color_code='\33[31m'
        elif str(color) in "yellow":
            color_code='\33[33m'
        elif str(color) in "green":
            color_code='\33[32m'
        elif str(color) in "blue":
            color_code='\33[34m'
        else:
            color_code='\033[0m'
            
        #print the string in the given color and then revert back
        print(color_code + string + color_code_end)
        
    return

def verboseprint2(errcode,string,EUD_dll,expectedresult=SUCCESS,verbose=1,color=None):
    '''
        verboseprint2
        takes a result and returns it, printing output if verbose is set to 1.
        Mainly for test sequences.
        Made to be a more succinct version of verboseprint

        color can be set, but default is to print red if failure (-1), and green if success(0)

        Parameters:
            errcode - errcode returned by EUD DLL. 
            result: SUCCESS (0) or FAILURE (-1)
            string: string to print (or not print if verbose == 0)
            
                if verbose is !0 and errcode is !0, prints out err string from EUD DLL
            EUD_dll - EUD DLL
            verbose - 0 == non verbose. 1 == verbose (only failures printed), 2 == more verbose (failures and success printed)
            color= 'red' or 'green'. If not specified, will print 'red' for failure and 'green' for success

    '''
    SetupEnvironment()
    
    if color is None:
        
        if expectedresult == SUCCESS and errcode == SUCCESS:
            color = 'green'
            string = "SUCCESS: "+str(string)
            rvalue = SUCCESS
        elif expectedresult == FAILURE and errcode != SUCCESS:
            color = 'green'
            string = "SUCCESS: "+str(string)
            rvalue = SUCCESS
        else:
            color = 'red'
            string = "FAILURE: "+str(string)
            rvalue = FAILURE

    if verbose > 0:
        if not (errcode == 0):
            string=str(string+EUDPythonAPIs.EUDGetErrorString(errcode,EUD_dll=EUD_dll))
        
        if expectedresult is SUCCESS and verbose > 1:
            colorprint(string,color)
        elif expectedresult is FAILURE:
            colorprint(string,color)
        elif expectedresult is SUCCESS and not errcode is SUCCESS:
            colorprint(string,color)
        
    return rvalue
    
def verboseprint(result,string,errcode,EUD_dll,verbose,color=None):
    '''
        verboseprint
        takes a result and returns it, printing output if verbose is set to 1.
        Mainly for test sequences.

        color can be set, but default is to print red if failure (-1), and green if success(0)

        Parameters:
            result: SUCCESS (0) or FAILURE (-1)
            string: string to print (or not print if verbose == 0)
            errcode - errcode returned by EUD DLL. 
                if verbose is !0 and errcode is !0, prints out err string from EUD DLL
            EUD_dll - EUD DLL
            verbose - 0 == non verbose. 1 == verbose
            color= 'red' or 'green'. If not specified, will print 'red' for failure and 'green' for success

    '''
    SetupEnvironment()
    if color is None:
        if result is SUCCESS:
            color = 'green'
        else:
            color = 'red'

    if result is SUCCESS:
        string = "SUCCESS: "+string
    else:
        string = "FAILURE: "+string

    if verbose is 1:
        if not (errcode == 0):
            string=str(string+EUDPythonAPIs.EUDGetErrorString(errcode,EUD_dll=EUD_dll))
        
        colorprint(string,color)
        return result
    else:
        return result


if __name__ == '__main__':
    import EUDTestUtilities
    print EUDTestUtilities.__doc__
    