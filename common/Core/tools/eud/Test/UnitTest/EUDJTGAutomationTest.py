﻿#!/user/bin/env python
'''    
    @file EUDJTGAutomationTest
    @brief Test sequence for EUD JTAG peripheral    
    @author JBILLING    
    @bug JTAG peripheral not yet functional, validation has not been completed.    
    
    Prerequisites:    
            Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.    
            EUD.dll must be in same directory or in ../../EUD/bin/x64/Release
    
    Available API's:    
        TestAll    
     
'''
######################################################################################
##
##        @file EUDJTGAutomationTest
##        @brief Test sequence for EUD JTAG peripheral
##        @author JBILLING
##        @bug JTAG peripheral not yet functional, validation has not been completed.
##
##        Prerequisites:
##                Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
##                EUD.dll must be in same directory or in ../../EUD/bin/x64/Release
##
##        Available API's:
##            TestAll
##
##                             EDIT HISTORY FOR FILE
##        This section contains comments describing changes made to the module.
##           when       who             what, where, why
##           --------   ---             ---------------------------------------------------------
##           01/11/2017 JBILLING        Changed Initialization API names
##           10/01/2016 JBILLING        Created
##
######################################################################################
import os, sys,ctypes
from ctypes import *
import EUDTestUtilities as utils
from optparse import OptionParser


nonverbose=0
verbose=1
global verboselevel
verboselevel=verbose
##====---------------------------------------------------====##
##====                Global Variables                   ====##
##====---------------------------------------------------====##


currentdir = os.path.abspath(os.path.dirname(__file__))
os.chdir(currentdir)
##This is where EUDPythonAPIs live
if not os.path.isfile(os.path.abspath(os.path.join(currentdir,"..","..","AutomationSamples","EUDPythonAPIs.py"))):
    kill("Error! Could not find EUDPythonAPIs")
    
sys.path.append(os.path.abspath(os.path.join(currentdir,"..","..","AutomationSamples")))
import EUDPythonAPIs

global SUCCESS, FAILURE
SUCCESS=0
FAILURE=-1

global EUDMSMResetHelpString
##====--------------------------------------------------====##
##====                  Python API's                    ====##
##====--------------------------------------------------====##



def TestEUD_JTGCMD(EUD_dll,JTG_EUD_Device):
    result=SUCCESS
    
    return FAILURE
    ##Copied from SWD stuff but needs to be ported
    
    #err=EUD_dll.JTGCMD(0,0,0,0,0,0,0) ##Should result in 
    #if err == 0:
    #    result=utils.verboseprint(FAILURE,"FAILURE JTGCMD 1. Got error: ",err,EUD_dll,verboselevel)
    #else:
    #    result=utils.verboseprint(SUCCESS,"SUCCESS JTGCMD 1. Got error: ",err,EUD_dll,verboselevel)
    #
    #err=EUD_dll.JTGCMD(JTG_EUD_Device,0,0,0,0,0,0)
    #if err == 0:
    #    result=utils.verboseprint(FAILURE,"FAILURE JTGCMD 1. Got error: ",err,EUD_dll,verboselevel)
    #else:
    #    result=utils.verboseprint(SUCCESS,"SUCCESS JTGCMD 1. Got error: ",err,EUD_dll,verboselevel)
        


    return result

def TestJTGPeripheralReset(EUD_dll,JTGDevice):
    result = SUCCESS
    
    return FAILURE
    ##Copied from SWD stuff but needs to be ported
    
    
    #err=EUD_dll.JTGPeripheralReset(None)
    #if err == SUCCESS:
    #    result=utils.verboseprint(FAILURE,"FAILURE JTGPeripheralReset. Got error: ",err,EUD_dll,verboselevel)
    #else:
    #    result=utils.verboseprint(SUCCESS,"SUCCESS JTGPeripheralReset. Got error: ",err,EUD_dll,verboselevel)
    #
    ###How to test with USB disconnected?
    ###How to test if periph has been reset?
    #err = EUD_dll.JTGPeripheralReset(JTGDevice)
    #if err != SUCCESS:
    #    result=utils.verboseprint(FAILURE,"FAILURE JTGPeripheralReset. Got error: ",err,EUD_dll,verboselevel)
    #else:
    #    result=utils.verboseprint(SUCCESS,"SUCCESS JTGPeripheralReset. Got error: ",err,EUD_dll,verboselevel)
    
    return result

def TestJTGSetFrequency(EUD_dll,JTGDevice):
    result = SUCCESS

    err=EUD_dll.JTGSetFrequency(None,0)
    if err == SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTGSetFrequency. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTGSetFrequency. Got error: ",err,EUD_dll,verboselevel)
    
    ##How to test with USB disconnected?
    ##This should pass
    err = EUD_dll.JTGSetFrequency(JTGDevice,0x3)
    if err != SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTGSetFrequency. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTGSetFrequency. Got error: ",err,EUD_dll,verboselevel)
    
    err = EUD_dll.JTGSetFrequency(JTGDevice,0xF)
    if err == SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTGSetFrequency. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTGSetFrequency. Got error: ",err,EUD_dll,verboselevel)

    err = EUD_dll.JTGSetFrequency(JTGDevice,0xFFFFFFFF)
    if err == SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTGSetFrequency. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTGSetFrequency. Got error: ",err,EUD_dll,verboselevel)

   

    err = EUD_dll.JTGSetFrequency(JTGDevice,0x3)
    if err != SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTGSetFrequency. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTGSetFrequency. Got error: ",err,EUD_dll,verboselevel)
    
    return result

def TestJTGSetDelay(EUD_dll,JTGDevice):
    result = SUCCESS

    err=EUD_dll.JTGSetDelay(None,0)
    if err == SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTGSetDelay. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTGSetDelay. Got error: ",err,EUD_dll,verboselevel)
    
    ##How to test with USB disconnected?
    
    err = EUD_dll.JTGSetDelay(JTGDevice,0x100)
    if err == SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTGSetDelay. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTGSetDelay. Got error: ",err,EUD_dll,verboselevel)
    
    err = EUD_dll.JTGSetDelay(JTGDevice,0xFFFFFFFF)
    if err == SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTGSetDelay. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTGSetDelay. Got error: ",err,EUD_dll,verboselevel)

    return result
    ################These should result in success###########################
    #Try max delay
    err = EUD_dll.JTGSetDelay(JTGDevice,0xFF)
    if err == SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTGSetDelay. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTGSetDelay. Got error: ",err,EUD_dll,verboselevel)
    #try normal delay
    err = EUD_dll.JTGSetDelay(JTGDevice,0x3)
    if err != SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTGSetDelay. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTGSetDelay. Got error: ",err,EUD_dll,verboselevel)
    #try minimum delay
    err = EUD_dll.JTGSetDelay(JTGDevice,0x0)
    if err != SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTGSetDelay. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTGSetDelay. Got error: ",err,EUD_dll,verboselevel)
    
    return result
def TestJTGTiming(EUD_dll,JTGDevice):
    result = SUCCESS

############Expect failure##########    
    err = EUD_dll.JTGTiming(Null, 0xFFFF, 0, 0)         ##test bad handlewrap
    if err == SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTGTiming. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTGTiming. Got error: ",err,EUD_dll,verboselevel)

    err = EUD_dll.JTGTiming(JTGEUDDevice, 0x1FFFF,0,0)  ##test overflow of retrycount
    if err == SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTGTiming. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTGTiming. Got error: ",err,EUD_dll,verboselevel)

    err = EUD_dll.JTGTiming(JTGEUDDevice, 0xFFFF, 0, 5) ##test overflow of wait32 bool
    if err == SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTGTiming. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTGTiming. Got error: ",err,EUD_dll,verboselevel)

    err = EUD_dll.JTGTiming(JTGEUDDevice, 0xFFFF, 4, 1) ##test overflow of turnarounddelay
    if err == SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTGTiming. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTGTiming. Got error: ",err,EUD_dll,verboselevel)


    ###########Expect success#################
    err = EUD_dll.JTGTiming(JTGEUDDevice, 0xFFFF, 3, 1) ##test overflow of turnarounddelay
    if err != SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTGTiming. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTGTiming. Got error: ",err,EUD_dll,verboselevel)

    return result



def TestJTGGetJTAGID(EUD_dll,JTGDevice):
    result = SUCCESS

    jtagid=c_int(0)
    
    err = EUD_dll.JTAG_GetJTAGID(JTGDevice,0)
    if err == SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTAG_to_JTG. Assumed that JTAG_to_JTG already ocurred, should have gotten failure. instead got: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTAG_to_JTG. Expected error that JTAG_to_JTG already done. Error:: ",err,EUD_dll,verboselevel)

    ####Should succeed####
    err = EUD_dll.JTAG_GetJTAGID(JTGDevice,pointer(jtagid))
    if err != SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTAG_to_JTG. Assumed that JTAG_to_JTG already ocurred, should have gotten failure. instead got: ",err,EUD_dll,verboselevel)
    elif jtagid is 0x0:
        result=utils.verboseprint(FAILURE,"FAILURE JTAG_to_JTG.JTAGID passed but got 0x0. Error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTAG_to_JTG. JTAGID: "+hex(jtagid.value)+". Error: ",err,EUD_dll,verboselevel)


    return result


def TestJTG_tms(EUD_dll,JTGDevice):
    #result = SUCCESS
    result = FAILURE
    
    ################Expect failures here########################
    #err = EUD_dll.JTG_tms(None,0xFFFF, 0x32)
    #if err == SUCCESS:
    #    result=utils.verboseprint(FAILURE,"TestJTG_tms. JTAG to SWD command: ",err,EUD_dll,verboselevel)
    #else:
    #    result=utils.verboseprint(SUCCESS,"TestJTG_tms. JTAG to SWD command: ",err,EUD_dll,verboselevel)
    #
    #err = EUD_dll.JTG_tms(SWDDevice,0x1FFFF, 0x32)
    #if err == SUCCESS:
    #    result=utils.verboseprint(FAILURE,"TestJTG_tms. JTAG to SWD command: ",err,EUD_dll,verboselevel)
    #else:
    #    result=utils.verboseprint(SUCCESS,"SWD_di_tms. JTAG to SWD command: ",err,EUD_dll,verboselevel)
    #
    #err = EUD_dll.JTG_tms(SWDDevice,0xFFFF, 0x1FFFF)
    #if err == SUCCESS:
    #    result=utils.verboseprint(FAILURE,"TestJTG_tms. JTAG to SWD command: ",err,EUD_dll,verboselevel)
    #else:
    #    result=utils.verboseprint(SUCCESS,"TestJTG_tms. JTAG to SWD command: ",err,EUD_dll,verboselevel)




    return result
def TestAll(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: TestAll
        ##
        ##  Description:
        ##      Runs all EUD JTG tests
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      Returns SUCCESS (0) or Fail (-1)
        ##
        #################################################################
        '''
    
    try:
        import EUDPythonAPIs
    except:
        print "Error occurred during import of EUDPythonAPIs. Is the file present in directory?"
        return FAILURE
        
    if EUD_dll is None:
        EUD_dll = EUDPythonAPIs.LoadEUDDLL()
        if EUD_dll is None:
            print "Error! EUD_dll could not be loaded. Is it in an accessible directory?"
            return FAILURE
            
    if deviceID is None:
        deviceID = EUDPythonAPIs.SelectEUDDevice(EUD_dll)
        if deviceID is None:
            kill("Error! No EUD Devices detected")
            
    OverallResult=SUCCESS
    
    
    JTG_EUD_Device = EUDPythonAPIs.EUDInitJTGDevice(EUD_dll=local_EUD_dll)
    if JTG_EUD_Device is 0:
        utils.colorprint("EUDInitJTGDevice Test Failed at EUDInitJTGDevice","red")
        return FAILURE
    else:
        utils.colorprint("EUDInitJTGDevice - Initialized successfully via EUDInitJTGDevice","green")
    
    if SUCCESS != TestEUD_JTGCMD(local_EUD_dll,JTG_EUD_Device):
        utils.colorprint("TestEUD_JTGCMD Failed","red")
        OverallResult=FAILURE
    
    if SUCCESS != TestJTGPeripheralReset(local_EUD_dll,JTG_EUD_Device):
        utils.colorprint("TestJTGPeripheralReset Failed","red")
        OverallResult=FAILURE

    if SUCCESS != TestJTGSetFrequency(local_EUD_dll,JTG_EUD_Device):
        utils.colorprint("TestJTGSetFrequency Failed","red")
        OverallResult=FAILURE

    if SUCCESS != TestJTGSetDelay(local_EUD_dll,JTG_EUD_Device):
        utils.colorprint("TestJTGSetDelay Failed","red")
        OverallResult=FAILURE
    
    if SUCCESS != TestJTGGetJTAGID(local_EUD_dll,JTG_EUD_Device):
        utils.colorprint("TestJTGJTAG_to_JTG Failed","red")
        OverallResult=FAILURE
    
    return OverallResult

def parseoptions():
    """
        Parse out given user options
    """
    argparser = OptionParser(version='%prog 1.0')
    argparser.add_option('-e','--eud',      dest='eud',         help="EUD DLL/SO Object to use",default=None)
    argparser.add_option('-d','--deviceid', dest='deviceID',    help="32bit deviceID value. See EUDDisplayAvailableDevices.py",default=None)
    argparser.add_option('-f','--fpgaflag', dest='FPGAFlag',    help="FPGA EUD instance option.",default=None)
    argparser.add_option('-g','--debugflag', dest='debugflag',  help="Debug EUD instance option.",default=None)
    
    #If -h/--help specified, print out description
    for arg in sys.argv[1:]:
        if ('-h' in arg) or ('--help' in arg):
            print EUDJTGAutomationTest.__doc__
            
    (options,args)=argparser.parse_args()
            
    return options

if __name__ == '__main__':
    
    options=parseoptions()
    TestAll(EUD_dll=options.eud,deviceID=options.deviceID,debugflag=options.debugflag,FPGAFlag=options.FPGAFlag)
    
