﻿#!/user/bin/env python
'''
EUD Automation test sequence. Runs battery of tests on EUD.dll using available scripts in 
AutomationSamples directory. 
      Prerequisites:
      Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
      EUD.dll must be in same directory or in ../../EUD/bin/x64/Debug
    
'''
######################################################################################
##
##        File Name: EUDAutomationTestSequence.py
##        Description: EUD automated test sequence
##        Prerequisites:
##                Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
##                EUD.dll must be in same directory or in ../../EUD/build/x64/Debug
##
##
##                      EDIT HISTORY FOR FILE
##   This section contains comments describing changes made to the module.
##       when       who             what, where, why
##       --------   ---             ---------------------------------------------------------
##       10/19/2017 JBILLING        Move CTL specific tests to their own file. Add multi device tests.
##       07/20/2017 JBILLING        Added EUDGetDeviceID Test
##       03/03/2017 JBILLING        Enabled EUD Trace. Disable JTAG test.
##       01/11/2017 JBILLING        Changed Initialization API names
##       10/01/2016 JBILLING        Created
##
######################################################################################

import ctypes
import sys
import os
import pdb
from ctypes import *
from optparse import OptionParser
from platform import system
os_platform = system()

#Defines for dll 
global debug
debug=True
#debug=False

global DEBUGFLAG
DEBUGFLAG=False
global FPGAFLAG
FPGAFLAG=False

global SUCCESS, FAILURE
SUCCESS=0
FAILURE=-1





currentdir = os.path.abspath(os.path.dirname(__file__))
os.chdir(currentdir)
##This is where EUDPythonAPIs live
if not os.path.isfile(os.path.abspath(os.path.join(currentdir,"..","..","AutomationSamples","EUDPythonAPIs.py"))):
    kill("Error! Could not find EUDPythonAPIs")
    
sys.path.append(os.path.abspath(os.path.join(currentdir,"..","..","AutomationSamples")))
import EUDPythonAPIs



######################################### Subroutines ###########################################

def colorprint(string,color):
    if os_platform == 'Windows':
        color_code_black=7
        if str(color) in "red":
            color_code=12
        elif str(color) in "yellow":
            color_code=30
        elif str(color) in "green":
            color_code=26
        elif str(color) in "blue":
            color_code=9
        #default to red
        else:
            color_code=0
            
        #print string
        #return        
        
        STD_OUTPUT_HANDLE = -11
        stdout_handle = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
        windll.kernel32.SetConsoleTextAttribute(stdout_handle,color_code)
        
        print string
        #restore to black
        #stdout_handle = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
        windll.kernel32.SetConsoleTextAttribute(stdout_handle,color_code_black)
        #print " "
        
    elif os_platform == 'Linux':
        color_code_end = '\033[0m'
        
        if str(color) in "red":
            color_code='\33[31m'
        elif str(color) in "yellow":
            color_code='\33[33m'
        elif str(color) in "green":
            color_code='\33[32m'
        elif str(color) in "blue":
            color_code='\33[34m'
        else:
            color_code='\033[0m'
            
        #print the string in the given color and then revert back
        print(color_code + string + color_code_end)

    return
    
def TestEUDGetDeviceID(EUD_dll,deviceID):
    import EUDPythonAPIs
    
    deviceID=c_int(0)
    err = EUDPythonAPIs.EUDGetDeviceID(EUD_dll=EUD_dll,deviceID=deviceID)
    print "Device ID is: "+str(deviceID[0])
    
    return SUCCESS
    
def TestEUDSWDEnable(EUD_dll,deviceID):
    import EUDPythonAPIs
    return EUDPythonAPIs.EUDInitSWDDevice(EUD_dll=EUD_dll,deviceID=deviceID)

def TestEUDMultiDeviceFunctions(EUD_dll):
    import EUDPythonAPIs
    
    result = EUDPythonAPIs.DisplayAvailableDevices(EUD_dll=EUD_dll)
    
    listofdevices = EUDPythonAPIs.GetListofDevicesString(EUD_dll=EUD_dll)
    if listofdevices is None:
        return FAILURE
        
    print "Devices found:"
    for device in listofdevices:
        print "\t"+str(device)

    return SUCCESS
    
def TestEUDErrorReturns(EUD_dll):
    '''
        Runs EUD Error string functionality.
        Should get a simple string back with
        something like 'EUD Error ...'


    '''    
    expectedstring="EUD "

    #sys.path.append(os.path.join(os.getcwd(),".."))
    import EUDPythonAPIs

    errdata=9999
    
    errorstring=EUDPythonAPIs.EUDGetErrorString(errdata,EUD_dll=EUD_dll)

    if not (expectedstring in errorstring):
        print "TestEUDErrorReturns() Error. Expected string: "+expectedstring+", receivedstring: "+errorstring
        return FAILURE
    
    #print "TestEUDErrorReturns() Success. Got string: "+errorstring
    return SUCCESS

def kill(message=None):
    if not message is None:
        print message
    sys.exit(1)
    
def TestAll(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
    #################################################################
    ##
    ##  Function: TestAll
    ##
    ##  Description:
    ##      Run full test sequence for all peripherals
    ##
    ##  Arguments:
    ##      options - option structure from OptionParser
    ##          
    ##  Returns:
    ##      None.
    ##      
    #################################################################
    '''
    #Load up DLL
    EUD_dll = EUDPythonAPIs.LoadEUDDLL()
    if EUD_dll is None:
        kill("Error! Could not find/load EUD DLL")
    DLL_Loaded = True
    
    
    #Test out error handling
    if TestEUDErrorReturns(EUD_dll): 
        colorprint("TestEUDErrorReturns failed","red")
        exit(10)
    else:
        colorprint("TestEUDErrorReturns: Success","green")
        
    ######################Test Multi Device ########################
    ##          Test multi device API functionality               ##
    ##                                                            ##
    if True:
        if SUCCESS!=TestEUDMultiDeviceFunctions(EUD_dll): 
            colorprint("TestEUDMultiDeviceFunctions Failed","red")
            #exit (11)
        else:
            colorprint("TestEUDMultiDeviceFunctions Success","green")
        
    
    ######################Set Current Device########################
    ##          Get existing devices and assign EUD_dll to         ##
    ##          a particular device                               ##
    deviceID = EUDPythonAPIs.SelectEUDDevice(EUD_dll=EUD_dll)
    if deviceID is None:
        kill("Error! No devices present")
    
    if True:
        ################ Test CTL Functionality ########################
        ##          Enable CTL periph, check various functions        ##
        ##                                                            ##
        ##                                                            ##
        import EUDCTLAutomationTest
        
        result = EUDCTLAutomationTest.TestAll(EUD_dll=EUD_dll,deviceID=deviceID)
    
        if result != SUCCESS:
            colorprint("EUD CTL Automation Failed","red")
        else:
            colorprint("EUD CTL Automation Passed", "green")
            
    if False:
        ################ Test COM Functionality ########################
        ##          Enable COM periph, check CTL                      ##
        ##          Setup Frequency, delay                            ##
        ##          Ask EUD to get CHIPID via JTAG                    ##
        import EUDCOMAutomationTest
        
        result = EUDCOMAutomationTest.TestAll(EUD_dll=EUD_dll,deviceID=deviceID)
    
        if result != SUCCESS:
            colorprint("EUD COM Automation Failed","red")
        else:
            colorprint("EUD COM Automation Passed", "green")
            
            
    if False:
        ################ Test JTAG Functionality ########################
        ##          Enable JTAG periph, check CTL                      ##
        ##          Setup Frequency, delay                             ##
        ##          Ask EUD to get CHIPID via JTAG                     ##
        
        import EUDJTGAutomationTest
        
        result = EUDJTGAutomationTest.TestAll(EUD_dll=EUD_dll,deviceID=deviceID)
    
        if result != SUCCESS:
            colorprint("EUD JTG Automation Failed","red")
        else:
            colorprint("EUD JTG Automation Passed", "green")
    
    
    if True:
        ################ Test SWD Functionality ########################
        ##          Enable SWD periph, check CTL                      ##
        ##          Setup Frequency, delay                            ##
        ##          Ask EUD to get CHIPID via SWD                     ##
        import EUDSWDAutomationTest
        
        result = EUDSWDAutomationTest.TestAll(EUD_dll=EUD_dll,deviceID=deviceID)
        
        if result != SUCCESS:
            colorprint("EUD SWD Automation Failed","red")
        else:
            colorprint("EUD SWD Automation Passed", "green")
            
    
    if True:
        ################ Test Trace Functionality ######################
        ##          Enable SWD periph, check CTL                      ##
        ##          Setup Frequency, delay                            ##
        ##          Ask EUD to get CHIPID via SWD                     ##
        
        import EUDTRCAutomationTest
        import EUDStartTracing
        #EUDStartTracing.EUDStartTracing()
        if SUCCESS==EUDTRCAutomationTest.TestAll(EUD_dll=EUD_dll,deviceID=deviceID):
            colorprint("EUD TRC Automation Failed","red")
        else:
            colorprint("EUD TRC Automation Passed", "green")

    
    #ctypes.windll.FreeLibrary(EUDlib) 
    #DLL_Loaded = False
    sys.exit(0)
    
    ################ Test Standalone Scripts  ######################
    standalone_scripts=[]
    for file in os.listdir(os.path.join(currdir,"..")):
        if file.endswith('.py'):
            standalone_scripts.append(file)
    
    for script in standalone_scripts:
        try:
            scriptapi=__import__(script)
        except:
            colorprint("Error when loading "+str(script),"red")
            continue
        
        try:
            if SUCCESS == scriptapi.test(deviceID=deviceID):
                colorprint(str(script)+" standalone script Passed","green")
            else:
                colorprint(str(script)+" standalone script Failed","red")
        except:
            colorprint(str(script)+": Running standalone script ran into some exception.","red")
    
    
    
    
    #import EUDTRCAutomationTest

    
    #####Eventually#####
    #PassT32Script
    #StartTraceRead
    
    #return
    #sys.exit(0)
    #initialize JTAG device
    #errdata=c_int(0)
    #options=c_int(0)
    #errdata_p=pointer(errdata)
    #jtgHandler = EUD_dll.EUDInitializeDeviceJTG(options,errdata_p)
    
    
def parseoptions():
    """
        Parse out given user options
    """
    argparser = OptionParser(version='%prog 1.0')
    argparser.add_option('-e','--eud',      dest='eud',         help="EUD DLL/SO Object to use",default=None)
    argparser.add_option('-d','--deviceid', dest='deviceID',    help="32bit deviceID value. See EUDDisplayAvailableDevices.py",default=None)
    argparser.add_option('-f','--fpgaflag', dest='FPGAFlag',    help="FPGA EUD instance option.",default=None)
    argparser.add_option('-g','--debugflag', dest='debugflag',  help="Debug EUD instance option.",default=None)
    
    #If -h/--help specified, print out description
    for arg in sys.argv[1:]:
        if ('-h' in arg) or ('--help' in arg):
            print EUDMSMDeassertReset.__doc__
            
    (options,args)=argparser.parse_args()
            
    return options
    
    
    
if __name__ == '__main__':
    
    options = parseoptions()
    TestAll(EUD_dll=options.eud,deviceID=options.deviceID,debugflag=options.debugflag,FPGAFlag=options.FPGAFlag)
    

