﻿#!/user/bin/env python
'''
EUD Automation test sequence. Runs battery of tests on EUD.dll using available scripts in 
AutomationSamples directory. 
      Prerequisites:
      Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
      EUD.dll must be in same directory or in ../../EUD/bin/x64/Release
    
'''
######################################################################################
##
##        File Name: EUDCTLAutomationTest.py
##        Description: EUD automated test sequence for control peripheral
##        Prerequisites:
##                Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
##                EUD.dll must be in same directory or in ../../EUD/bin/x64/Release
##
##
##                      EDIT HISTORY FOR FILE
##   This section contains comments describing changes made to the module.
##       when       who             what, where, why
##       --------   ---             ---------------------------------------------------------
##       10/19/2017 JBILLING        Created
##
######################################################################################
import ctypes
import sys
import os
import pdb
from ctypes import *
from platform import system
os_platform = system()

#Defines for dll 
global debug
debug=True
#debug=False
global SUCCESS, FAILURE
SUCCESS=0
FAILURE=-1
global DEBUGFLAG
DEBUGFLAG=False
global FPGAFLAG
FPGAFLAG=False


currentdir = os.path.abspath(os.path.dirname(__file__))
os.chdir(currentdir)
##This is where EUDPythonAPIs live
if not os.path.isfile(os.path.abspath(os.path.join(currentdir,"..","..","AutomationSamples","EUDPythonAPIs.py"))):
    kill("Error! Could not find EUDPythonAPIs")
    
sys.path.append(os.path.abspath(os.path.join(currentdir,"..","..","AutomationSamples")))
import EUDPythonAPIs




######################################### Subroutines ###########################################
def ValidHandle(value):
    if value == 0:
        print "Got null handle value. "
    return value

def colorprint(string,color):
    if os_platform == 'Windows':
        color_code_black=7
        if str(color) in "red":
            color_code=12
        elif str(color) in "yellow":
            color_code=30
        elif str(color) in "green":
            color_code=26
        elif str(color) in "blue":
            color_code=9
        #default to red
        else:
            color_code=0
            
        #print string
        #return        
        
        STD_OUTPUT_HANDLE = -11
        stdout_handle = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
        windll.kernel32.SetConsoleTextAttribute(stdout_handle,color_code)
        
        print string
        #restore to black
        #stdout_handle = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
        windll.kernel32.SetConsoleTextAttribute(stdout_handle,color_code_black)
        #print " "
    
    elif os_platform == 'Linux':
        color_code_end = '\033[0m'
        
        if str(color) in "red":
            color_code='\33[31m'
        elif str(color) in "yellow":
            color_code='\33[33m'
        elif str(color) in "green":
            color_code='\33[32m'
        elif str(color) in "blue":
            color_code='\33[34m'
        else:
            color_code='\033[0m'
            
        #print the string in the given color and then revert back
        print(color_code + string + color_code_end)

    return




    

def TestEUDMSMReset(EUD_dll,deviceID):
    '''
        Tests EUDMSMReset API, which should assert and deassert 
        EUD Reset by defined amount (e.g., 2000 milliseconds)
        returns 0 for success, nonzero for failure
    '''
    import EUDPythonAPIs
    delayvalue=2000
    return EUDPythonAPIs.EUDMSMReset(EUD_dll=EUD_dll,deviceID=deviceID,delay_ms_value=delayvalue)

def TestEUDMSMAssertReset(EUD_dll,deviceID):
    import EUDPythonAPIs

    return EUDPythonAPIs.EUDMSMAssertReset(EUD_dll=EUD_dll,deviceID=deviceID)

def TestEUDMSMDeassertReset(EUD_dll,deviceID):
    import EUDPythonAPIs
    return EUDPythonAPIs.EUDMSMDeassertReset(EUD_dll=EUD_dll,deviceID=deviceID)

def TestEUDGetCTLStatusString(EUD_dll,deviceID,ExpectedValue=None):
    
    CTL_RESET_VALUE=0xE0000
    if ExpectedValue is None:
        ExpectedValue=CTL_RESET_VALUE
    import EUDPythonAPIs
    
    errdata=9999
    ctlstring=EUDPythonAPIs.EUDGetCTLStatusString(EUD_dll=EUD_dll,deviceID=deviceID)
    
    
    if not(type(ctlstring) is str):
         print "GetCTLStatus(): Failure. Expected string, got "+str(ctlstring)
         return FAILURE

    print "ctlstring data:"
    print ctlstring

    
    return SUCCESS
    
    
def TestEUDGetCTLStatus(EUD_dll,deviceID,ctlstatuslist,ExpectedValue=None):
    
    CTL_RESET_VALUE=0xE0000
    
    if ExpectedValue is None:
        ExpectedValue=CTL_RESET_VALUE
        

    import EUDPythonAPIs
    
    errdata=9999
    ctldata=EUDPythonAPIs.EUDGetCTLStatus(EUD_dll=EUD_dll,deviceID=deviceID)
    ctlstatuslist.append(ctldata)
    if ExpectedValue != ctldata:
         print "GetCTLStatus(): Warning: ctl register reset expected value: "+hex(ExpectedValue)+". Actual: "+hex(ctldata)
         return SUCCESS
    
    return SUCCESS

def TestEUDEnableCharger(EUD_dll,deviceID):

    import EUDPythonAPIs

    return EUDPythonAPIs.EUDEnableCharger(EUD_dll=EUD_dll,deviceID=deviceID)

def TestEUDDisableCharger(EUD_dll,deviceID):

    import EUDPythonAPIs

    return EUDPythonAPIs.EUDDisableCharger(EUD_dll=EUD_dll,deviceID=deviceID)

def TestEUDSpoofDetach(EUD_dll,deviceID):
    import EUDPythonAPIs
    return EUDPythonAPIs.EUDSpoofDetach(EUD_dll=EUD_dll,deviceID=deviceID)
    
def TestEUDSpoofAttach(EUD_dll,deviceID):
    import EUDPythonAPIs
    return EUDPythonAPIs.EUDSpoofAttach(EUD_dll=EUD_dll,deviceID=deviceID)

def TestEUDSWDEnable(EUD_dll,deviceID):
    import EUDPythonAPIs
    return EUDPythonAPIs.EUDInitSWDDevice(EUD_dll=EUD_dll,deviceID=deviceID)

def TestEUDMultiDeviceFunctions(EUD_dll):
    import EUDPythonAPIs
    
    result = EUDPythonAPIs.DisplayAvailableDevices(EUD_dll)
    import pdb;pdb.set_trace()
    listofdevices = EUDPythonAPIs.GetListofDevicesString(EUD_dll)
    print "Devices found:"
    for device in listofdevices:
        print "\t"+str(device)
    
    return result

    return SUCCESS
def TestEUDErrorReturns(EUD_dll):
    '''
        Runs EUD Error string functionality.
        Should get a simple string back with
        something like 'EUD Error ...'


    '''    
    expectedstring="EUD "

    #sys.path.append(os.path.join(os.getcwd(),".."))
    import EUDPythonAPIs

    errdata=9999
    
    errorstring=EUDPythonAPIs.EUDGetErrorString(errdata,EUD_dll=EUD_dll)

    if not (expectedstring in errorstring):
        print "TestEUDErrorReturns() Error. Expected string: "+expectedstring+", receivedstring: "+errorstring
        return FAILURE
    
    #print "TestEUDErrorReturns() Success. Got string: "+errorstring
    return SUCCESS

def kill(message):
    if not message is None:
        print message
    sys.exit(1)
    

def TestAll(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: TestAll
        ##
        ##  Description:
        ##      Runs all EUD CTL tests
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      deviceID=<hex device id>
        ##          Device ID to attach to. A list of available devices can 
        ##          be retrieved with EUDDisplayAvailableDevices
        ##  Returns:
        ##      Returns SUCCESS (0) or Fail (-1)
        ##
        #################################################################

        '''
    
    try:
        import EUDPythonAPIs
    except:
        print "Error occurred during import of EUDPythonAPIs. Is the file present in directory?"
        return FAILURE
        
    if EUD_dll is None:
        EUD_dll = EUDPythonAPIs.LoadEUDDLL()
        if EUD_dll is None:
            print "Error! EUD_dll could not be loaded. Is it in an accessible directory?"
            return FAILURE
            
    if deviceID is None:
        deviceID = EUDPythonAPIs.SelectEUDDevice(EUD_dll)
        if deviceID is None:
            kill("Error! No EUD Devices detected")
            
    OverallResult=SUCCESS
    
    
    if True:
        ######################Get CTL status############################
        ##          Enable charger and then check if ctl register     ##
        ##          has been updated correctly                        ##
        ctlstatuslist=[]
        if SUCCESS!=TestEUDGetCTLStatus(EUD_dll,deviceID,ctlstatuslist): 
            colorprint("TestEUDGetCTLStatus Failed","red")
            OverallResult=FAILURE
        else:
            colorprint("TestEUDGetCTLStatus Success","green")
        
        if TestEUDGetCTLStatusString(EUD_dll,deviceID): 
            colorprint("TestEUDGetCTLStatusString Failed","red")
            OverallResult=FAILURE
        else:
            colorprint("TestEUDGetCTLStatusString Success","green")
            
        
    if False:
        #########################Test EUD Resets########################
        ##          Enable charger and then check if ctl register     ##
        ##          has been updated correctly                        ##
        if TestEUDMSMReset(EUD_dll,deviceID):
            colorprint("TestEUDMSMReset failed","red")
            OverallResult=FAILURE
        else:
            colorprint("TestEUDMSMReset Success","green")
        
        ###############Test EUD Assert and Deassert sequence################
        if TestEUDMSMAssertReset(EUD_dll,deviceID):
            colorprint("EUDMSMAssertReset Failed","red")
            OverallResult=FAILURE
        else:
            if TestEUDGetCTLStatus(EUD_dll,deviceID,ctlstatuslist,ExpectedValue=ctlstatuslist[0]): 
                colorprint("TestEUDGetCTLStatus failed, after TestEUDMSMAssertReset","red")
            OverallResult=FAILURE
            if TestEUDMSMDeassertReset(EUD_dll,deviceID):
                colorprint("TestEUDMSMDeassertReset Failed","red")
                OverallResult=FAILURE
            if TestEUDGetCTLStatus(EUD_dll,ctlstatuslist,ExpectedValue=ctlstatuslist[0]): 
                colorprint("TestEUDGetCTLStatus failed, after TestEUDMSMDeassertReset","red")
                OverallResult=FAILURE
            colorprint("EUD Assert/Deassert Reset with ctl register check: Success","green")
        
        
    if False:
        #################Test Charger enable and disable################
        ##          Enable charger and then check if ctl register     ##
        ##          has been updated correctly                        ##
        CTL_reg_charge_enabled_bit=0x1<<14
        if TestEUDEnableCharger(EUD_dll,deviceID):
            colorprint("EUDTestEUDEnableCharger Failed","red")
            OverallResult=FAILURE
        else:
            if TestEUDGetCTLStatus(EUD_dll,deviceID,ctlstatuslist,ExpectedValue=(ctlstatuslist[0]+CTL_reg_charge_enabled_bit)): 
                colorprint("TestEUDGetCTLStatus failed, after EUDTestEUDEnableCharger ","red")
                OverallResult=FAILURE
            else:
                colorprint("EUD Charger Enable sequence: Success","green")

        if TestEUDDisableCharger(EUD_dll,deviceID):
            colorprint("EUDTestEUDDisableCharger Failed","red")
            OverallResult=FAILURE
        else:
            if TestEUDGetCTLStatus(EUD_dll,deviceID,ctlstatuslist,ExpectedValue=ctlstatuslist[0]): 
                colorprint("TestEUDGetCTLStatus failed, after EUDTestEUDDisableCharger","red")
                OverallResult=FAILURE
            else:
                colorprint("EUD Charger Enable sequence: Success","green")
        

    if False:
        #################Test Spoof Detach/Attach#######################
        ##          Enable charger and then check if ctl register     ##
        ##          has been updated correctly                        ##
        CTL_reg_vbus_enabled_bit=0x1<<12
        if TestEUDSpoofDetach(EUD_dll,deviceID):
            colorprint("TestEUDSpoofDetach Failed","red")
            OverallResult=FAILURE
        else:
            if TestEUDGetCTLStatus(EUD_dll,deviceID,ctlstatuslist,ExpectedValue=(ctlstatuslist[0]+CTL_reg_charge_enabled_bit)): 
                colorprint("TestEUDGetCTLStatus failed, after TestEUDSpoofDetach","red")
                OverallResult=FAILURE
            else:
                colorprint("EUD Spoof Detach sequence: Success","green")

        if TestEUDSpoofAttach(EUD_dll,deviceID):
            colorprint("TestEUDSpoofAttach Failed","red")
            OverallResult=FAILURE
        else:
            if TestEUDGetCTLStatus(EUD_dll,deviceID,ctlstatuslist,ExpectedValue=ctlstatuslist[0]): 
                colorprint("TestEUDGetCTLStatus failed, after TestEUDSpoofAttach","red")
                OverallResult=FAILURE
            else:
                colorprint("EUD Spoof Attach sequence: Success","green")
                    
    return OverallResult
    
    
def parseoptions():
    """
        Parse out given user options
    """
    argparser = OptionParser(version='%prog 1.0')
    argparser.add_option('-e','--eud',      dest='eud',         help="EUD DLL/SO Object to use",default=None)
    argparser.add_option('-d','--deviceid', dest='deviceID',    help="32bit deviceID value. See EUDDisplayAvailableDevices.py",default=None)
    argparser.add_option('-f','--fpgaflag', dest='FPGAFlag',    help="FPGA EUD instance option.",default=None)
    argparser.add_option('-g','--debugflag', dest='debugflag',  help="Debug EUD instance option.",default=None)
    
    #If -h/--help specified, print out description
    for arg in sys.argv[1:]:
        if ('-h' in arg) or ('--help' in arg):
            print EUDSWDAutomationTest.__doc__
            
    (options,args)=argparser.parse_args()
            
    return options

if __name__ == '__main__':
    
    options=parseoptions()
    TestAll(EUD_dll=options.eud,deviceID=options.deviceID,debugflag=options.debugflag,FPGAFlag=options.FPGAFlag)
    