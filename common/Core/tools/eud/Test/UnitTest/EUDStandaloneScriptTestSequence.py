﻿#!/user/bin/env python
'''    
    @file EUDStandaloneScriptTestSequence.py
    @brief Test sequence to test standalone python scripts.
    @author JBILLING
    @bug

    Prerequisites:
            Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
            EUD.dll must be in same directory or in ../../EUD/bin/x64/Release

    Available API's:
        TestAll
     
'''
######################################################################################
##
##        @file EUDStandaloneScriptTestSequence.py
##        @brief Test sequence to test standalone python scripts.
##        @author JBILLING
##        @bug
##
##        Prerequisites:
##                Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
##                EUD.dll must be in same directory or in ../../EUD/bin/x64/Release
##
##        Available API's:
##            TestAll
##
##                             EDIT HISTORY FOR FILE
##        This section contains comments describing changes made to the module.
##           when       who             what, where, why
##           --------   ---             ---------------------------------------------------------
##           10/01/2016 JBILLING        Created
##
######################################################################################
import ctypes
import sys
import os
import pdb
from ctypes import *
from platform import system
os_platform = system()


#Defines for dll 
global debug
debug=True
#debug=False
global SUCCESS, FAILURE
SUCCESS=0
FAILURE=-1





currentdir = os.path.dirname(__file__)
os.chdir(currentdir)
##This is where EUDPythonAPIs live
sys.path.append(os.path.abspath(os.path.join(currentdir,"..","..","AutomationSamples")))


######################Modify here to point to desired EUD DLL File!#######################################
global EUDDirectories
EUDDirectories=[]
EUDDirectories.append(os.path.join(os.getcwd(),"..","..","Trace32","build","x64","Debug","EUD.dll"))
EUDDirectories.append(os.path.join(os.getcwd(),"EUD.dll"))
EUDDirectories.append(os.path.join(os.getcwd(),"..","EUD","eud.dll"))
EUDDirectories.append(os.path.join(os.getcwd(),"..","..","EUD","eud.dll"))
EUDDirectories.append(os.path.join(os.getcwd(),"..","..","EUD","build","x64","debug","EUD.dll"))
EUDDirectories.append(os.path.join(os.getcwd(),"..","EUD","build","x64","debug","EUD.dll"))
EUDDirectories.append(os.path.join(os.getcwd(),"..","..","EUD","build","x64","release","EUD.dll"))
EUDDirectories.append(os.path.join(os.getcwd(),"..","EUD","build","x64","release","EUD.dll"))
EUDDirectories.append(os.path.join(os.getcwd(),"..","..","Trace32","build","x64","Debug","EUD.dll"))
EUDDirectories.append(os.path.join(os.getcwd(),"..","..","bin","x64","Debug","EUD.dll"))
global EUDlib
EUDlib=EUDDirectories[3]
##########################################################################################################
def ValidHandle(value):
    if value == 0:
        print "Got null handle value. "
    return value

def colorprint(string,color):
    if os_platform == 'Windows':
        color_code_black=7
        if str(color) in "red":
            color_code=12
        elif str(color) in "yellow":
            color_code=30
        elif str(color) in "green":
            color_code=26
        elif str(color) in "blue":
            color_code=9
        #default to red
        else:
            color_code=0
            
        #print string
        #return        
        
        STD_OUTPUT_HANDLE = -11
        stdout_handle = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
        windll.kernel32.SetConsoleTextAttribute(stdout_handle,color_code)
        
        print string
        #restore to black
        #stdout_handle = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
        windll.kernel32.SetConsoleTextAttribute(stdout_handle,color_code_black)
        #print " "
        
    elif os_platform == 'Linux':
        color_code_end = '\033[0m'
        
        if str(color) in "red":
            color_code='\33[31m'
        elif str(color) in "yellow":
            color_code='\33[33m'
        elif str(color) in "green":
            color_code='\33[32m'
        elif str(color) in "blue":
            color_code='\33[34m'
        else:
            color_code='\033[0m'
            
        #print the string in the given color and then revert back
        print(color_code + string + color_code_end)
        
    return



if __name__ == '__main__':
        
    
    
    ################ Test Standalone Scripts  ######################
    import EUDAssertReset
    if SUCCESS == EUDAssertReset.test():
        colorprint("EUDAssertReset standalone script Passed","green")
    else:
        colorprint("EUDAssertReset standalone script Failed","red")
        
        
    import EUDDeassertReset
    if SUCCESS == EUDDeassertReset.test():
        colorprint("EUDDeassertReset standalone script Passed","green")
    else:
        colorprint("EUDDeassertReset standalone script Failed","red")
    
    import EUDDisableCharger
    if SUCCESS == EUDDisableCharger.test():
        colorprint("EUDDisableCharger standalone script Passed","green")
    else:
        colorprint("EUDDisableCharger standalone script Failed","red")
    
    
    import EUDEnableCharger
    if SUCCESS == EUDEnableCharger.test():
        colorprint("EUDEnableCharger standalone script Passed","green")
    else:
        colorprint("EUDEnableCharger standalone script Failed","red")
    
    import EUDGetCTLStatus
    if SUCCESS == EUDGetCTLStatus.test():
        colorprint("EUDGetCTLStatus standalone script Passed","green")
    else:
        colorprint("EUDGetCTLStatus standalone script Failed","red")
    
    import EUDMSMReset
    if SUCCESS == EUDMSMReset.test():
        colorprint("EUDMSMReset standalone script Passed","green")
    else:
        colorprint("EUDMSMReset standalone script Failed","red")
    
    #import EUDSendT32Script
    #if SUCCESS == EUDAssertReset.test():
    #    colorprint("EUDAssertReset standalone script Passed","green")
    #else:
    #    colorprint("EUDAssertReset standalone script Failed","red")
    import EUDSpoofAttach
    if SUCCESS == EUDSpoofAttach.test():
        colorprint("EUDSpoofAttach standalone script Passed","green")
    else:
        colorprint("EUDSpoofAttach standalone script Failed","red")
    import EUDSpoofDetach
    if SUCCESS == EUDSpoofDetach.test():
        colorprint("EUDSpoofDetach standalone script Passed","green")
    else:
        colorprint("EUDSpoofDetach standalone script Failed","red")
    
