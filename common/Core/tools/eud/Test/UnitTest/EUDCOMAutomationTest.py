﻿#!/user/bin/env python
'''    
    @file  EUDCOMAutomationTest.py
    @brief Test sequence for EUD JTAG peripheral
    @author JBILLING
    @bug

    Prerequisites:
            Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
            EUD.dll must be in same directory or in ../../EUD/bin/x64/Release

    Available API's:
        TestAll

'''
######################################################################################
##
##        @file  EUDCOMAutomationTest.py
##        @brief Test sequence for EUD JTAG peripheral
##        @author JBILLING
##        @bug
##
##        Prerequisites:
##                Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
##                EUD.dll must be in same directory or in ../../EUD/bin/x64/Release
##
##        Available API's:
##            TestAll
##
##                             EDIT HISTORY FOR FILE
##        This section contains comments describing changes made to the module.
##           when       who             what, where, why
##           --------   ---             ---------------------------------------------------------
##           01/11/2017 JBILLING        Changed Initialization API names
##           10/01/2016 JBILLING        Created
##
######################################################################################
import os, sys,ctypes
from ctypes import *
import EUDTestUtilities as utils
from optparse import OptionParser


nonverbose=0
verbose=1
global verboselevel
verboselevel=verbose
##====---------------------------------------------------====##
##====                Global Variables                   ====##
##====---------------------------------------------------====##

currentdir = os.path.abspath(os.path.dirname(__file__))
os.chdir(currentdir)
##This is where EUDPythonAPIs live
if not os.path.isfile(os.path.abspath(os.path.join(currentdir,"..","..","AutomationSamples","EUDPythonAPIs.py"))):
    kill("Error! Could not find EUDPythonAPIs")
    
sys.path.append(os.path.abspath(os.path.join(currentdir,"..","..","AutomationSamples")))
import EUDPythonAPIs

global DEBUGFLAG
DEBUGFLAG=False
global FPGAFLAG
FPGAFLAG=False

global SUCCESS, FAILURE
SUCCESS=0
FAILURE=-1

global EUDMSMResetHelpString
##====--------------------------------------------------====##
##====                  Python API's                    ====##
##====--------------------------------------------------====##

    
def TestEUD_COMCMD(EUD_dll,COM_EUD_Device):
    result=SUCCESS
    
    err=EUD_dll.COMCMD(0,0,0,0,0,0,0) ##Should result in 
    if err == 0:
        result=utils.verboseprint(FAILURE,"FAILURE COMCMD 1. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS COMCMD 1. Got error: ",err,EUD_dll,verboselevel)
    
    err=EUD_dll.COMCMD(COM_EUD_Device,0,0,0,0,0,0)
    if err == 0:
        result=utils.verboseprint(FAILURE,"FAILURE COMCMD 1. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS COMCMD 1. Got error: ",err,EUD_dll,verboselevel)
        


    return result

def TestCOMPeripheralReset(EUD_dll,COMDevice):
    result = SUCCESS
    
    err=EUD_dll.COMPeripheralReset(None)
    if err == SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE COMPeripheralReset. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS COMPeripheralReset. Got error: ",err,EUD_dll,verboselevel)
    
    ##How to test with USB disconnected?
    ##How to test if periph has been reset?
    err = EUD_dll.COMPeripheralReset(COMDevice)
    if err != SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE COMPeripheralReset. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS COMPeripheralReset. Got error: ",err,EUD_dll,verboselevel)
    
    return result


def TestCOMSetTimeout(EUD_dll,COMDevice):
    result = SUCCESS

    err=EUD_dll.COMSetTimeout(None,0)
    if err == SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE COMSetTimeout. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS COMSetTimeout. Got error: ",err,EUD_dll,verboselevel)
    
    err = EUD_dll.COMSetTimeout(COMDevice,0x100)
    if err == SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE COMSetTimeout. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS COMSetTimeout. Got error: ",err,EUD_dll,verboselevel)
    
    err = EUD_dll.COMSetTimeout(COMDevice,0xFFFFFFFF)
    if err == SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE COMSetTimeout. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS COMSetTimeout. Got error: ",err,EUD_dll,verboselevel)

    return result
    ################These should result in success###########################
    #Try max delay
    err = EUD_dll.COMSetTimeout(COMDevice,0xFF)
    if err == SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE COMSetTimeout. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS COMSetTimeout. Got error: ",err,EUD_dll,verboselevel)
    #try normal delay
    err = EUD_dll.COMSetTimeout(COMDevice,0x3)
    if err != SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE COMSetTimeout. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS COMSetTimeout. Got error: ",err,EUD_dll,verboselevel)
    #try minimum delay
    err = EUD_dll.COMSetTimeout(COMDevice,0x0)
    if err != SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE COMSetTimeout. Got error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS COMSetTimeout. Got error: ",err,EUD_dll,verboselevel)
    
    return result
    
def TestAll(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: TestAll
        ##
        ##  Description:
        ##      Runs all EUD COM tests
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      Returns SUCCESS (0) or Fail (-1)
        ##
        #################################################################
    '''
    
    try:
        import EUDPythonAPIs
    except:
        print "Error occurred during import of EUDPythonAPIs. Is the file present in directory?"
        return FAILURE
        
    if EUD_dll is None:
        EUD_dll = EUDPythonAPIs.LoadEUDDLL()
        if EUD_dll is None:
            print "Error! EUD_dll could not be loaded. Is it in an accessible directory?"
            return FAILURE
            
    if deviceID is None:
        deviceID = EUDPythonAPIs.SelectEUDDevice(EUD_dll)
        if deviceID is None:
            kill("Error! No EUD Devices detected")
            
    OverallResult=SUCCESS
    
    
    EE_ID_mask=0x90
    err = EUD_dll.Test_function(EE_ID_mask+1)
    import pdb; pdb.set_trace()
    
    result="FAILURE"
    COM_EUD_Device = EUDPythonAPIs.EUDInitCOMDevice(EUD_dll=EUD_dll,deviceID=deviceID,Result=result)
    if COM_EUD_Device is 0:
        utils.colorprint("EUDInitCOMDevice Test Failed at EUDInitCOMDevice","red")
        return FAILURE
    else:
        utils.colorprint("EUDInitCOMDevice - Initialized successfully via EUDInitCOMDevice","green")
    
    if SUCCESS != TestEUD_COMCMD(EUD_dll,COM_EUD_Device):
        utils.colorprint("TestEUD_COMCMD Failed","red")
        OverallResult=FAILURE
    
    if SUCCESS != TestCOMPeripheralReset(EUD_dll,COM_EUD_Device):
        utils.colorprint("TestCOMPeripheralReset Failed","red")
        OverallResult=FAILURE

    if SUCCESS != TestCOMSetFrequency(EUD_dll,COM_EUD_Device):
        utils.colorprint("TestCOMSetFrequency Failed","red")
        OverallResult=FAILURE
    import pdb; pdb.set_trace()
    if SUCCESS != TestCOMSetDelay(EUD_dll,COM_EUD_Device):
        utils.colorprint("TestCOMSetDelay Failed","red")
        OverallResult=FAILURE
    if SUCCESS != TestCOM_di_tms(EUD_dll,COM_EUD_Device):
        utils.colorprint("TestCOM_di_tms Failed","red")
        OverallResult=FAILURE
    if SUCCESS != TestCOMJTAG_to_COM(EUD_dll,COM_EUD_Device):
        utils.colorprint("TestCOMJTAG_to_COM Failed","red")
        OverallResult=FAILURE
    if SUCCESS != TestCOMGetJTAGID(EUD_dll,COM_EUD_Device):
        utils.colorprint("TestCOMJTAG_to_COM Failed","red")
        OverallResult=FAILURE
    
    return OverallResult

def parseoptions():
    """
        Parse out given user options
    """
    argparser = OptionParser(version='%prog 1.0')
    argparser.add_option('-e','--eud',      dest='eud',         help="EUD DLL/SO Object to use",default=None)
    argparser.add_option('-d','--deviceid', dest='deviceID',    help="32bit deviceID value. See EUDDisplayAvailableDevices.py",default=None)
    argparser.add_option('-f','--fpgaflag', dest='FPGAFlag',    help="FPGA EUD instance option.",default=None)
    argparser.add_option('-g','--debugflag', dest='debugflag',  help="Debug EUD instance option.",default=None)
    

    #If -h/--help specified, print out description
    for arg in sys.argv[1:]:
        if ('-h' in arg) or ('--help' in arg):
            print EUDJTGAutomationTest.__doc__
            
    (options,args)=argparser.parse_args()
            
    return options

if __name__ == '__main__':
    
    options=parseoptions()
    TestAll(EUD_dll=options.eud,deviceID=options.deviceID,debugflag=options.debugflag,FPGAFlag=options.FPGAFlag)
    
