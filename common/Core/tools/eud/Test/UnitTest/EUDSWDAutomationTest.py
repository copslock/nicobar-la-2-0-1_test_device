﻿#!/user/bin/env python
'''    
    @file  EUDSWDAutomationTest.py
    @brief Test sequence for EUD JTAG peripheral
    @author JBILLING
    @bug

    Prerequisites:
            Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
            EUD.dll must be in same directory or in ../../EUD/bin/x64/Release

    Available API's:
        TestAll

'''
######################################################################################
##
##        @file  EUDSWDAutomationTest.py
##        @brief Test sequence for EUD JTAG peripheral
##        @author JBILLING
##        @bug
##
##        Prerequisites:
##                Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
##                EUD.dll must be in same directory or in ../../EUD/bin/x64/Release
##
##        Available API's:
##            TestAll
##
##                             EDIT HISTORY FOR FILE
##        This section contains comments describing changes made to the module.
##           when       who             what, where, why
##           --------   ---             ---------------------------------------------------------
##           10/19/2017 JBILLING        Updated error api's
##           01/11/2017 JBILLING        Changed Initialization API names
##           10/01/2016 JBILLING        Created
##
######################################################################################
import os, sys,ctypes
from ctypes import *
import EUDTestUtilities as utils
from optparse import OptionParser
import inspect


nonverbose=0
verbose=1
global verboselevel
verboselevel=verbose
##====---------------------------------------------------====##
##====                Global Variables                   ====##
##====---------------------------------------------------====##


currentdir = os.path.abspath(os.path.dirname(__file__))
os.chdir(currentdir)
##This is where EUDPythonAPIs live
if not os.path.isfile(os.path.abspath(os.path.join(currentdir,"..","..","AutomationSamples","EUDPythonAPIs.py"))):
    kill("Error! Could not find EUDPythonAPIs")
    
sys.path.append(os.path.abspath(os.path.join(currentdir,"..","..","AutomationSamples")))
import EUDPythonAPIs

global DEBUGFLAG
DEBUGFLAG=False
global FPGAFLAG
FPGAFLAG=False

global SUCCESS, FAILURE
SUCCESS=0
FAILURE=-1

global EUDMSMResetHelpString
##====--------------------------------------------------====##
##====                  Python API's                    ====##
##====--------------------------------------------------====##



def TestEUD_SWDRead(EUD_dll,SWD_EUD_Device):
    result=SUCCESS
    rptr = c_uint32(0)
    result |= utils.verboseprint2( EUD_dll.SWDRead(rptr,0,0,0), \
                                inspect.stack()[0][3]+ " Null handle ",\
                                EUD_dll,
                                expectedresult=FAILURE\
                                )
    #having issues with 0xCCCCCCCC00000000 being sent. Need to debug
    #result |= utils.verboseprint2( EUD_dll.SWDRead(SWD_EUD_Device,0,0,rptr), \
    #                            inspect.stack()[0][3]+ " Null return ptr ",\
    #                            EUD_dll,
    #                            expectedresult=FAILURE\
    #                            )
    

    return result

def TestEUD_SWDWrite(EUD_dll,SWD_EUD_Device):
    result=SUCCESS
    
    result |= utils.verboseprint2( EUD_dll.SWDWrite(0,0,0,0), \
                                    inspect.stack()[0][3]+ " Null handle ",\
                                    EUD_dll,
                                    expectedresult=FAILURE\
                                    )

    #err=EUD_dll.SWDWrite(SWD_EUD_Device,0,0,0)
    #if err == 0:
    #    result=utils.verboseprint(FAILURE,"FAILURE SWDWrite 2. Got error: ",err,EUD_dll,verboselevel)
    #else:
    #    result=utils.verboseprint(SUCCESS,"SUCCESS SWDWrite 2. Got error: ",err,EUD_dll,verboselevel)
        

    return result

def TestSWDPeripheralReset(EUD_dll,SWDDevice):
    result = SUCCESS
    
    result |= utils.verboseprint2( EUD_dll.SWDPeripheralReset(None), \
                                inspect.stack()[0][3]+ " Null handle ",\
                                EUD_dll,
                                expectedresult=FAILURE\
                                )

    ##How to test with USB disconnected?
    ##How to test if periph has been reset?
    result |= utils.verboseprint2( EUD_dll.SWDPeripheralReset(SWDDevice), \
                                inspect.stack()[0][3]+ " Null handle ",\
                                EUD_dll,
                                expectedresult=SUCCESS\
                                )
                                
    return result

def TestSWDSetFrequency(EUD_dll,SWDDevice):
    result = SUCCESS

    result |= utils.verboseprint2( EUD_dll.SWDSetFrequency(None,0), \
                                inspect.stack()[0][3]+ " Null handle ",\
                                EUD_dll,
                                expectedresult=FAILURE\
                                )
    
    ##How to test with USB disconnected?
    ##This should pass
    result |= utils.verboseprint2( EUD_dll.SWDSetFrequency(SWDDevice,0x3), \
                                inspect.stack()[0][3]+ " success ",\
                                EUD_dll,
                                expectedresult=SUCCESS\
                                )
    
    result |= utils.verboseprint2( EUD_dll.SWDSetFrequency(SWDDevice,0xF), \
                                inspect.stack()[0][3]+ " overflow frequency by 1 ",\
                                EUD_dll,
                                expectedresult=FAILURE\
                                )
    
    result |= utils.verboseprint2( EUD_dll.SWDSetFrequency(SWDDevice,0xFFFFFFFF), \
                                inspect.stack()[0][3]+ " overflow  frequency ",\
                                EUD_dll,
                                expectedresult=FAILURE\
                                )
                                
    
    result |= utils.verboseprint2( EUD_dll.SWDSetFrequency(SWDDevice,0x6), \
                                inspect.stack()[0][3]+ " normal frequency ",\
                                EUD_dll,
                                expectedresult=SUCCESS\
                                )
                                
    return result

def TestSWDSetDelay(EUD_dll,SWDDevice):
    result = SUCCESS

        
    result |= utils.verboseprint2( EUD_dll.SWDSetDelay(None,0), \
                                inspect.stack()[0][3]+ " Null handle test ",\
                                EUD_dll,
                                expectedresult=FAILURE\
                                )
    
    result |= utils.verboseprint2( EUD_dll.SWDSetDelay(SWDDevice,0x100), \
                                inspect.stack()[0][3]+ " high delay value",\
                                EUD_dll,
                                expectedresult=FAILURE\
                                )

    result |= utils.verboseprint2( EUD_dll.SWDSetDelay(SWDDevice,0xFFFFFFFF), \
                                inspect.stack()[0][3]+ " high delay value 2",\
                                EUD_dll,
                                expectedresult=FAILURE\
                                )

    return result
    ################These should result in success###########################
    #Try max delay
    result |= utils.verboseprint2( EUD_dll.SWDSetDelay(SWDDevice,0xFF), \
                                inspect.stack()[0][3]+ " high delay value 2",\
                                EUD_dll,
                                expectedresult=SUCCESS\
                                )
    
    #try normal delay
    result |= utils.verboseprint2( EUD_dll.SWDSetDelay(SWDDevice,0x3), \
                                inspect.stack()[0][3]+ " high delay value 2",\
                                EUD_dll,
                                expectedresult=SUCCESS\
                                )
        
    #try minimum delay
    result |= utils.verboseprint2( EUD_dll.SWDSetDelay(SWDDevice,0x0), \
                                inspect.stack()[0][3]+ " high delay value 2",\
                                EUD_dll,
                                expectedresult=SUCCESS\
                                )

    return result
    
    
def TestSWDTiming(EUD_dll,SWDDevice):
    result = SUCCESS

############Expect failure##########    
    result |= utils.verboseprint2( EUD_dll.SWDTiming(0, 0xFFFF,0, 0), \
                                    inspect.stack()[0][3]+ " test bad instance  ptr: ",\
                                    EUD_dll,
                                    expectedresult=FAILURE\
                                    )
    
    result |= utils.verboseprint2( EUD_dll.SWDTiming(SWDEUDDevice, 0x1FFFF,0,0), \
                                inspect.stack()[0][3]+ "Test overflow  of retrycount: ",\
                                EUD_dll,
                                expectedresult=FAILURE\
                                )
                                
    result |= utils.verboseprint2( EUD_dll.SWDTiming(SWDEUDDevice, 0xFFFF, 0, 5), \
                                inspect.stack()[0][3]+ "test overflow of wait32 bool ",\
                                EUD_dll,
                                expectedresult=FAILURE\
                                )
                                
    
    result |= utils.verboseprint2( EUD_dll.SWDTiming(SWDEUDDevice, 0xFFFF, 4, 1), \
                                inspect.stack()[0][3]+ "test overflow of turnarounddelay ",\
                                EUD_dll,
                                expectedresult=FAILURE\
                                )
                                
    
    ###########Expect success#################
    result |= utils.verboseprint2( EUD_dll.SWDTiming(SWDEUDDevice, 0xFFFF, 3, 1), \
                                inspect.stack()[0][3]+ "sucess test ",\
                                EUD_dll,
                                expectedresult=SUCCESS\
                                )
    
    return result


def TestSWDJTAG_to_SWD(EUD_dll,SWDDevice):
    result = SUCCESS
    err = EUD_dll.JTAG_to_SWD(SWDDevice)
    #Since Reset done before, JTAG to SWD not yet done.
    if err == SUCCESS:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTAG_to_SWD. JTAG_to_SWD done after peripheral reset done: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(FAILURE,"FAILURE JTAG_to_SWD. Expected success,  instead got: ",err,EUD_dll,verboselevel)

    #Do it again just to be sure.
    err = EUD_dll.JTAG_to_SWD(SWDDevice)
    if err == SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTAG_to_SWD. Assumed that JTAG_to_SWD already ocurred, should have gotten failure. instead got: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTAG_to_SWD. Expected error that JTAG_to_SWD already done. Error:: ",err,EUD_dll,verboselevel)

    #reset peripheral
    err = EUD_dll.SWDPeripheralReset(SWDDevice)
    if err != SUCCESS:
        result=utils.verboseprint(FAILURE,"FAILURE JTAG_to_SWD. Error during SWDResetPeripheral: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS JTAG_to_SWD. Step SWDResetPeriphereal succeeded: ",err,EUD_dll,verboselevel)

    #Now should get success
    err = EUD_dll.JTAG_to_SWD(SWDDevice)
    if err != SUCCESS:
        result=utils.verboseprint(FAILURE,"JTAG_to_SWD. After reset, should have gotten success. Got: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"JTAG_to_SWD. After reset, should have gotten success. Got: ",err,EUD_dll,verboselevel)

    return result

def TestSWDGetJTAGID(EUD_dll,SWDDevice):
    result = SUCCESS

    jtagid=c_int(0)
    result |= utils.verboseprint2( EUD_dll.SWD_GetJTAGID(None,pointer(jtagid)), \
                        inspect.stack()[0][3]+ "null handle test ",\
                        EUD_dll,
                        expectedresult=FAILURE\
                        )
    ##err = EUD_dll.SWD_GetJTAGID(None,pointer(jtagid))
    ##if err == SUCCESS:
    ##    result=utils.verboseprint(FAILURE,"FAILURE SWD_GetJTAGID. Passed null SWDDevice, expected failure. instead got: ",err,EUD_dll,verboselevel)
    ##else:
    ##    result=utils.verboseprint(SUCCESS,"SUCCESS SWD_GetJTAGID. Passed null SWDDevice, expected failure. Error:: ",err,EUD_dll,verboselevel)

    result |= utils.verboseprint2( EUD_dll.SWD_GetJTAGID(SWDDevice,0), \
                        inspect.stack()[0][3]+ "null handle test ",\
                        EUD_dll,
                        expectedresult=FAILURE\
                        )
    ##err = EUD_dll.SWD_GetJTAGID(SWDDevice,0)
    ##if err == SUCCESS:
    ##    result=utils.verboseprint(FAILURE,"FAILURE SWD_GetJTAGID. Passed null pointer for jtagid, should have gotten failure. instead got: ",err,EUD_dll,verboselevel)
    ##else:
    ##    result=utils.verboseprint(SUCCESS,"SUCCESS SWD_GetJTAGID. Passed null pointer for jtagid, expected failure. Error: ",err,EUD_dll,verboselevel)

    ####Should succeed####
    result |= utils.verboseprint2( EUD_dll.SWD_GetJTAGID(SWDDevice,pointer(jtagid)), \
                        inspect.stack()[0][3]+ "null handle test ",\
                        EUD_dll,
                        expectedresult=SUCCESS\
                        )
    ##err = EUD_dll.SWD_GetJTAGID(SWDDevice,pointer(jtagid))
    ##if err != SUCCESS:
    ##    result=utils.verboseprint(FAILURE,"FAILURE SWD_GetJTAGID. Expected success, instead got: ",err,EUD_dll,verboselevel)
    ##elif jtagid is 0x0:
    ##    result=utils.verboseprint(FAILURE,"FAILURE SWD_GetJTAGID. JTAGID passed but got 0x0. Error: ",err,EUD_dll,verboselevel)
    ##else:
    ##    result=utils.verboseprint(SUCCESS,"SUCCESS SWD_GetJTAGID. JTAGID: "+hex(jtagid.value)+". Error: ",err,EUD_dll,verboselevel)


    return result


def TestSWD_di_tms(EUD_dll,SWDDevice):
    result = SUCCESS
    
    ################Expect failures here########################
    result |= utils.verboseprint2( EUD_dll.SWD_di_tms(None,0xFFFF, 0x32), \
                            inspect.stack()[0][3]+ "null handle test ",\
                            EUD_dll,
                            expectedresult=FAILURE\
                            )
    ##err = EUD_dll.SWD_di_tms(None,0xFFFF, 0x32)
    ##if err == SUCCESS:
    ##    result=utils.verboseprint(FAILURE,"SWD_di_tms. JTAG to SWD command: ",err,EUD_dll,verboselevel)
    ##else:
    ##    result=utils.verboseprint(SUCCESS,"SWD_di_tms. JTAG to SWD command: ",err,EUD_dll,verboselevel)
    ##
    result |= utils.verboseprint2( EUD_dll.SWD_di_tms(SWDDevice,0x1FFFF, 0x32), \
                            inspect.stack()[0][3]+ "overflow ditms value ",\
                            EUD_dll,
                            expectedresult=FAILURE\
                            )
    ##err = EUD_dll.SWD_di_tms(SWDDevice,0x1FFFF, 0x32)
    ##if err == SUCCESS:
    ##    result=utils.verboseprint(FAILURE,"SWD_di_tms. JTAG to SWD command: ",err,EUD_dll,verboselevel)
    ##else:
    ##    result=utils.verboseprint(SUCCESS,"SWD_di_tms. JTAG to SWD command: ",err,EUD_dll,verboselevel)
    ##
    result |= utils.verboseprint2( EUD_dll.SWD_di_tms(SWDDevice,0xFFFF, 0x1FFFF), \
                            inspect.stack()[0][3]+ "overflow count value ",\
                            EUD_dll,
                            expectedresult=FAILURE\
                            )
    ##err = EUD_dll.SWD_di_tms(SWDDevice,0xFFFF, 0x1FFFF)
    ##if err == SUCCESS:
    ##    result=utils.verboseprint(FAILURE,"SWD_di_tms. JTAG to SWD command: ",err,EUD_dll,verboselevel)
    ##else:
    ##    result=utils.verboseprint(SUCCESS,"SWD_di_tms. JTAG to SWD command: ",err,EUD_dll,verboselevel)

    return result
    
def resultshandler(result,EUD_dll,expectedresult,functionname,errormessage,successmessage,fatalexit=False):

    if expectedresult is SUCCESS:
        expecteresultstring = "SUCCESS"
    else:
        expecteresultstring = "FAILURE"
        
    if result == expectedresult:
        result=utils.verboseprint(SUCCESS,"SUCCESS "+str(functionname)+". "+successmessage+" Expected "+expectedresultstring,result,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(FAILURE,"FAILURE "+str(functionname)+". "+failuremessage+" Error: ",expectedresultstring,result,EUD_dll,verboselevel)
        
    return result
    
def TestSWDTRSTReset(EUD_dl,SWDDevice):
    result = SUCCESS
    functionname="TestSWDTRSTReset"
    #verify getting SWD JTAGID
    jtagid=c_int(0)
    
    ####Should succeed####
    result=resultshandler(EUD_dll.SWD_GetJTAGID(SWDDevice,pointer(jtagid)),EUD_dll, SUCCESS, functionname,"Retrieving DAPID for first time","Retrieving DAPID for first time")    
    if jtagid is 0x0:
        result=utils.verboseprint(FAILURE,"FAILURE TestSWDTRSTReset. Retrieving DAPID for first time. JTAGID passed but got 0x0. Error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS TestSWDTRSTReset.  Retrieving DAPID for first time. JTAGID: "+hex(jtagid.value)+". Error: ",err,EUD_dll,verboselevel)

        
                           ##*   SWDBitValues are as follows:
    swd_clk          = 0   ##*           bit[0] swd_clk 
    swd_di           = 1   ##*           bit[1] swd_di 
    swd_rctlr_srst_n = 2   ##*           bit[2] swd_rctlr_srst_n 
    swd_gpio_di_oe   = 3   ##*           bit[3] swd_gpio_di_oe
    swd_gpio_srst_n  = 4   ##*           bit[4] swd_gpio_srst_n 
    swd_gpio_trst_n  = 5   ##*           bit[5] swd_gpio_trst_n 
                           ##*           bits[31:6] reserved
                           
                           
    #issue SWD BITBANG with TRST assert/deassert
    value=c_int( (1 << swd_rctlr_srst_n) + (1 << swd_gpio_srst_n)  + (0 << swd_gpio_trst_n) )
    result=resultshandler(EUD_dll.SWDBitBang(SWDDevice,value),EUD_dll, SUCCESS, functionname,"Asserting TRST via SWD Bitbang","Issuing TRST via SWD Bitbang")
    
    value=c_int( (1 << swd_rctlr_srst_n) + (1 << swd_gpio_srst_n)  + (1 << swd_gpio_trst_n) )
    result=resultshandler(EUD_dll.SWDBitBang(SWDDevice,value),EUD_dll, SUCCESS, functionname,"Deasserting TRST via SWD Bitbang","Issuing TRST via SWD Bitbang")
    
    
    
    #verify getting SWD JTAGID
    result=resultshandler(EUD_dll.SWD_GetJTAGID(SWDDevice,pointer(jtagid)),EUD_dll, SUCCESS, functionname,"Retrieving DAPID for first time","Retrieving DAPID for first time")
    if jtagid is 0x0:
        result=utils.verboseprint(FAILURE,"FAILURE TestSWDTRSTReset. Retrieving DAPID for first time. JTAGID passed but got 0x0. Error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS TestSWDTRSTReset.  Retrieving DAPID for first time. JTAGID: "+hex(jtagid.value)+". Error: ",err,EUD_dll,verboselevel)

    #issue SWD BITBANG TRST assert
    value=c_int( (1 << swd_rctlr_srst_n) + (1 << swd_gpio_srst_n)  + (0 << swd_gpio_trst_n) )
    result=resultshandler(EUD_dll.SWDBitBang(SWDDevice,value),EUD_dll, SUCCESS, functionname,"Asserting TRST via SWD Bitbang","Issuing TRST via SWD Bitbang")

    #verify getting SWD JTAGID ->Should fail
    result=resultshandler(EUD_dll.SWD_GetJTAGID(SWDDevice,pointer(jtagid)),EUD_dll, SUCCESS, functionname,"Retrieving DAPID for first time","Retrieving DAPID for first time")
    if jtagid is 0x0:
        result=utils.verboseprint(SUCCESS,"SUCCESS TestSWDTRSTReset.  Should not have gotten DAPID since TRST asserted. JTAGID: "+hex(jtagid.value)+". Error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(FAILURE,"FAILURE TestSWDTRSTReset. Should not have gotten DAPID since TRST asserted. JTAGID passed but got 0x0. Error: ",err,EUD_dll,verboselevel)
        
        

    #issue SWD BITBANG TRST deassert
    value=c_int( (1 << swd_rctlr_srst_n) + (1 << swd_gpio_srst_n)  + (1 << swd_gpio_trst_n) )
    result=resultshandler(EUD_dll.SWDBitBang(SWDDevice,value),EUD_dll, SUCCESS, functionname,"Asserting TRST via SWD Bitbang","Issuing TRST via SWD Bitbang")

    #verify getting SWD JTAGID ->Should succeed
    result=resultshandler(EUD_dll.SWD_GetJTAGID(SWDDevice,pointer(jtagid)),EUD_dll, SUCCESS, functionname,"Retrieving DAPID after TRST deasserted for last time","Retrieving DAPID after TRST deasserted for last time")
    if jtagid is 0x0:
        result=utils.verboseprint(FAILURE,"FAILURE TestSWDTRSTReset. Retrieving DAPID for first time. JTAGID passed but got 0x0. Error: ",err,EUD_dll,verboselevel)
    else:
        result=utils.verboseprint(SUCCESS,"SUCCESS TestSWDTRSTReset.  Retrieving DAPID for first time. JTAGID: "+hex(jtagid.value)+". Error: ",err,EUD_dll,verboselevel)

    return result
    
    
def TestAll(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: TestAll
        ##
        ##  Description:
        ##      Runs all EUD SWD tests
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      Returns SUCCESS (0) or Fail (-1)
        ##
        #################################################################

        '''
    
    try:
        import EUDPythonAPIs
    except:
        print "Error occurred during import of EUDPythonAPIs. Is the file present in directory?"
        return FAILURE
        
    if EUD_dll is None:
        EUD_dll = EUDPythonAPIs.LoadEUDDLL()
        if EUD_dll is None:
            print "Error! EUD_dll could not be loaded. Is it in an accessible directory?"
            return FAILURE
            
    if deviceID is None:
        deviceID = EUDPythonAPIs.SelectEUDDevice(EUD_dll)
        if deviceID is None:
            kill("Error! No EUD Devices detected")
            
    OverallResult=SUCCESS
    
    SWD_EUD_Device = EUDPythonAPIs.EUDInitSWDDevice(EUD_dll=EUD_dll,deviceID=deviceID)
    if SWD_EUD_Device is 0:
        utils.colorprint("EUDInitSWDDevice Test Failed at EUDInitSWDDevice","red")
        return FAILURE
    else:
        utils.colorprint("EUDInitSWDDevice - Initialized successfully via EUDInitSWDDevice","green")
    
    #if SUCCESS != TestSWDTRSTReset(EUD_dll,SWD_EUD_Device):
    #    utils.colorprint("TestEUD_SWDCMD Failed","red")
    #    OverallResult=FAILURE
    
    if SUCCESS != TestEUD_SWDRead(EUD_dll,SWD_EUD_Device):
        utils.colorprint("TestEUD_SWDRead Failed","red")
        OverallResult=FAILURE
        
    if SUCCESS != TestEUD_SWDWrite(EUD_dll,SWD_EUD_Device):
        utils.colorprint("TestEUD_SWDWrite Failed","red")
        OverallResult=FAILURE
        
    if SUCCESS != TestSWDPeripheralReset(EUD_dll,SWD_EUD_Device):
        utils.colorprint("TestSWDPeripheralReset Failed","red")
        OverallResult=FAILURE

    if SUCCESS != TestSWDSetFrequency(EUD_dll,SWD_EUD_Device):
        utils.colorprint("TestSWDSetFrequency Failed","red")
        OverallResult=FAILURE
    
    if SUCCESS != TestSWDSetDelay(EUD_dll,SWD_EUD_Device):
        utils.colorprint("TestSWDSetDelay Failed","red")
        OverallResult=FAILURE
        
    if SUCCESS != TestSWD_di_tms(EUD_dll,SWD_EUD_Device):
        utils.colorprint("TestSWD_di_tms Failed","red")
        OverallResult=FAILURE
        
    if SUCCESS != TestSWDJTAG_to_SWD(EUD_dll,SWD_EUD_Device):
        utils.colorprint("TestSWDJTAG_to_SWD Failed","red")
        OverallResult=FAILURE
        
    if SUCCESS != TestSWDGetJTAGID(EUD_dll,SWD_EUD_Device):
        utils.colorprint("TestSWDJTAG_to_SWD Failed","red")
        OverallResult=FAILURE
    
    ##FIXME - this seems to cause device hang
    ##if SUCCESS != EUD_dll.EUDClosePeripheral(SWD_EUD_Device):
    ##    utils.colorprint("Error - could not close SWD peripheral","red")
    ##    OverallResult=FAILURE
        
    return OverallResult

def parseoptions():
    """
        Parse out given user options
    """
    argparser = OptionParser(version='%prog 1.0')
    argparser.add_option('-e','--eud',      dest='eud',         help="EUD DLL/SO Object to use",default=None)
    argparser.add_option('-d','--deviceid', dest='deviceID',    help="32bit deviceID value. See EUDDisplayAvailableDevices.py",default=None)
    argparser.add_option('-f','--fpgaflag', dest='FPGAFlag',    help="FPGA EUD instance option.",default=None)
    argparser.add_option('-g','--debugflag', dest='debugflag',  help="Debug EUD instance option.",default=None)
    
    #If -h/--help specified, print out description
    for arg in sys.argv[1:]:
        if ('-h' in arg) or ('--help' in arg):
            print EUDSWDAutomationTest.__doc__
            
    (options,args)=argparser.parse_args()
            
    return options

if __name__ == '__main__':
    
    options=parseoptions()
    TestAll(EUD_dll=options.eud,deviceID=options.deviceID,debugflag=options.debugflag,FPGAFlag=options.FPGAFlag)
    
