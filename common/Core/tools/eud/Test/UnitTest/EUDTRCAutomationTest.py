﻿#!/user/bin/env python
'''    
    @file EUDTRCAutomationTest
    @brief Test sequence for EUD Trace peripheral    
    @author JBILLING    
    @bug Trace peripheral not yet functional, validation has not been completed.    
    
    Prerequisites:    
            Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.    
            EUD.dll must be in same directory or in ../../EUD/bin/x64/Debug
    
    Available API's:    
        TestAll    
     
'''
######################################################################################
##
##        @file EUDTRCAutomationTest
##        @brief Test sequence for EUD Trace peripheral
##        @author JBILLING
##        @bug Trace peripheral not yet functional, validation has not been completed.
##
##        Prerequisites:
##                Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
##                EUD.dll must be in same directory or in ../../EUD/bin/x64/Debug
##
##        Available API's:
##            TestAll
##
##                             EDIT HISTORY FOR FILE
##        This section contains comments describing changes made to the module.
##           when       who             what, where, why
##           --------   ---             ---------------------------------------------------------
##           07/03/2017 JBILLING        Redesigned for new trace API interface
##           03/03/2017 JBILLING        Enable and clean up tests now that Trace Periph working on FPGA
##           01/11/2017 JBILLING        Changed Initialization API names
##           10/01/2016 JBILLING        Created
##
######################################################################################
#from future_builtins import *
import os, sys,ctypes, time, struct
from ctypes import *
import EUDTestUtilities as utils
from optparse import OptionParser
import inspect
import shutil
import pdb
nonverbose=0
verbose=1
global verboselevel
verboselevel=verbose
##====---------------------------------------------------====##
##====                Global Variables                   ====##
##====---------------------------------------------------====##

currentdir = os.path.abspath(os.path.dirname(__file__))
os.chdir(currentdir)
##This is where EUDPythonAPIs live
if not os.path.isfile(os.path.abspath(os.path.join(currentdir,"..","..","AutomationSamples","EUDPythonAPIs.py"))):
    kill("Error! Could not find EUDPythonAPIs")
    
sys.path.append(os.path.abspath(os.path.join(currentdir,"..","..","AutomationSamples")))
import EUDPythonAPIs

global DEBUGFLAG
DEBUGFLAG=False
global FPGAFLAG
FPGAFLAG=False

global SUCCESS, FAILURE
SUCCESS=0
FAILURE=-1

global EUDMSMResetHelpString
##====--------------------------------------------------====##
##====                  Python API's                    ====##
##====--------------------------------------------------====##

    
    

def Test_EUD_OpenTrace(EUD_dll,TRC_EUD_Device):
    result=SUCCESS
    
    #Open up trace peripheral, get USB values
    result|=utils.verboseprint2( EUD_dll.EUD_OpenTrace(0),\
                                inspect.stack()[0][3]+" OpenTrace with NULL step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    ##result|=utils.verboseprint2( EUD_dll.EUD_OpenTrace(0xFFFFFFFF),\
    ##                            inspect.stack()[0][3]+" OpenTrace with 0xFFFFFFF step. Got error: ",\
    ##                            EUD_dll,\
    ##                            expectedresult=FAILURE\
    ##                            )                            
    result|=utils.verboseprint2( EUD_dll.EUD_OpenTrace(TRC_EUD_Device),\
                                inspect.stack()[0][3]+" OpenTrace normal step. Got error: ",\
                                EUD_dll,\
                                expectedresult=SUCCESS\
                                )      
                        
                                
    return result 

def Test_EUD_TraceSetChunkSizes(EUD_dll,TRC_EUD_Device):
    result=SUCCESS
    #Set chunk size tests
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetChunkSizes(0,1024*1024,10),\
                                inspect.stack()[0][3]+" step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    
    ##result|=utils.verboseprint2( EUD_dll.EUD_TraceSetChunkSizes(0xFFFFFFFF,1024*1024,10),\
    ##                            inspect.stack()[0][3]+" step. Got error: ",\
    ##                            EUD_dll,\
    ##                            expectedresult=FAILURE\
    ##                            )
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetChunkSizes(TRC_EUD_Device,0,10),\
                                inspect.stack()[0][3]+" bad chunksize (0) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetChunkSizes(TRC_EUD_Device,0xFFFFFFFF,10),\
                                inspect.stack()[0][3]+" bad chunksize (big) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetChunkSizes(TRC_EUD_Device,1024*1024,0),\
                                inspect.stack()[0][3]+" bad maxchunks (0) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetChunkSizes(TRC_EUD_Device,1024*1024,0xFFFF),\
                                inspect.stack()[0][3]+" bad maxchunks (big) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )                            
        
    #
    #   Now set transfer length to 1000, and chunksize to 500. we should get a failure since chunksize cannot be less
    #   than transfer length
    #
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetTransferLength(TRC_EUD_Device,1000),\
                                inspect.stack()[0][3]+" set transfer length to 1000 step. Got error: ",\
                                EUD_dll,\
                                expectedresult=SUCCESS\
                                )
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetChunkSizes(TRC_EUD_Device,500,5),\
                                inspect.stack()[0][3]+" set chunk size < transfer length. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
     
    #
    #   Set trace to 1k chunksize, 5 chunks
    #
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetChunkSizes(TRC_EUD_Device,1024*1024,5),\
                                inspect.stack()[0][3]+" set normal chunk params step. Got error: ",\
                                EUD_dll,\
                                expectedresult=SUCCESS\
                                )
                                
    return result     

def Test_EUD_TraceSetOutputDir(EUD_dll,TRC_EUD_Device):
    result=SUCCESS
    
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetOutputDir(0,r'c:\temp\stuff'),\
                                inspect.stack()[0][3]+" bad param (0) value step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    
    ##result|=utils.verboseprint2( EUD_dll.EUD_TraceSetOutputDir(0xFFFFFFFF,r'c:\temp\stuff'),\
    ##                            inspect.stack()[0][3]+" bad param (big) value step. Got error: ",\
    ##                            EUD_dll,\
    ##                            expectedresult=FAILURE\
    ##                            )
    
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetOutputDir(TRC_EUD_Device,0),\
                                inspect.stack()[0][3]+" bad path (0) value step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )                            
    
    ##result|=utils.verboseprint2( EUD_dll.EUD_TraceSetOutputDir(TRC_EUD_Device,0xf),\
    ##                            inspect.stack()[0][3]+" bad path (0xF) value step. Got error: ",\
    ##                            EUD_dll,\
    ##                            expectedresult=FAILURE\
    ##                            )
    
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetOutputDir(TRC_EUD_Device,'L:\\'),\
                                inspect.stack()[0][3]+" inaccessible path step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    
    
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetOutputDir(TRC_EUD_Device,'C:\\'),\
                                inspect.stack()[0][3]+" No permissions for path access step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )             
    
    
    pathtocheck='c:\\temp\\tracedir12345'
    subdir='traces'
    if os.path.isdir(pathtocheck):
        
        shutil.rmtree(os.path.join(pathtocheck))
        if os.path.isdir(pathtocheck):
            print "Error: Need path to be removed: pathtocheck"
            import pdb; pdb.set_trace()
    
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetOutputDir(TRC_EUD_Device,os.path.join(pathtocheck,subdir)),\
                                inspect.stack()[0][3]+" bad path: parent dir doesn't exist step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )                                               
    
    #
    #   Now for success cases
    #
    if not os.path.isdir(pathtocheck):
        os.mkdir(pathtocheck)
        
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetOutputDir(TRC_EUD_Device,pathtocheck),\
                                inspect.stack()[0][3]+" Good path, already exists step. Got error: ",\
                                EUD_dll,\
                                expectedresult=SUCCESS\
                                )                                               
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetOutputDir(TRC_EUD_Device,os.path.join(pathtocheck,'traces')),\
                                inspect.stack()[0][3]+" Good path, need to make subdirectory step. Got error: ",\
                                EUD_dll,\
                                expectedresult=SUCCESS\
                                )                                               
                                
    return result

def Test_EUD_TraceGetOutputDir(EUD_dll,TRC_EUD_Device):
    result=SUCCESS
    
    outputdir=' '*200
    charsize=c_int(0)
    outputdir_p=c_char_p(outputdir)
    result|=utils.verboseprint2( EUD_dll.EUD_TraceGetOutputDir(0,c_char_p(outputdir),pointer(charsize)),\
                                inspect.stack()[0][3]+" bad handleWrap (0) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    ##result|=utils.verboseprint2( EUD_dll.EUD_TraceGetOutputDir(0xFFFFFFF,outputdir_p,pointer(charsize)),\
    ##                            inspect.stack()[0][3]+" bad handleWrap (0xFF) step. Got error: ",\
    ##                            EUD_dll,\
    ##                            expectedresult=FAILURE\
    ##                            )
    result|=utils.verboseprint2( EUD_dll.EUD_TraceGetOutputDir(TRC_EUD_Device,0,pointer(charsize)),\
                                inspect.stack()[0][3]+" bad parameter step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    result|=utils.verboseprint2( EUD_dll.EUD_TraceGetOutputDir(TRC_EUD_Device,outputdir_p,0),\
                                inspect.stack()[0][3]+" bad parameter step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    #
    #   Now for working case
    #
    result|=utils.verboseprint2( EUD_dll.EUD_TraceGetOutputDir(TRC_EUD_Device,outputdir_p,pointer(charsize)),\
                                inspect.stack()[0][3]+" good parameter step. Got error: ",\
                                EUD_dll,\
                                expectedresult=SUCCESS\
                                )
                                
    #outputdir=outputdir.strip()
    print str(outputdir)
    
    if not os.path.isdir(outputdir):
        print "Error! Could not access returned output directory from EUD_TraceGetOutputDir!: "+str(outputdir)
        result|=FAILURE
        
    return result
    
def Test_EUD_TraceSetCaptureMode(EUD_dll,TRC_EUD_Device):
    result = SUCCESS
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetCaptureMode(0,0),\
                                inspect.stack()[0][3]+" bad handleWrap(0) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    ##result|=utils.verboseprint2( EUD_dll.EUD_TraceSetCaptureMode(0xFFFFFFFF,0),\
    ##                            inspect.stack()[0][3]+" bad handleWrap step. Got error: ",\
    ##                            EUD_dll,\
    ##                            expectedresult=FAILURE\
    ##                            )
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetCaptureMode(TRC_EUD_Device,3),\
                                inspect.stack()[0][3]+" bad parameter step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    #
    #   Success cases
    #
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetCaptureMode(TRC_EUD_Device,1),\
                                inspect.stack()[0][3]+" good parameter (1) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=SUCCESS\
                                )
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetCaptureMode(TRC_EUD_Device,0),\
                                inspect.stack()[0][3]+" good parameter (0) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=SUCCESS\
                                )
    
    return result
    
def Test_EUD_TraceSetTimeoutMS(EUD_dll,TRC_EUD_Device):
    result = SUCCESS
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetTimeoutMS(0,100),\
                                inspect.stack()[0][3]+" bad handleWrap(0) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    #result|=utils.verboseprint2( EUD_dll.EUD_TraceSetTimeoutMS(0xFFFFFFFF,100),\
    #                            inspect.stack()[0][3]+" bad handleWrap (0xFF) step. Got error: ",\
    #                            EUD_dll,\
    #                            expectedresult=FAILURE\
    #                            )
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetTimeoutMS(TRC_EUD_Device,0),\
                                inspect.stack()[0][3]+" bad parameter (0) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetTimeoutMS(TRC_EUD_Device,0xFFFFF),\
                                inspect.stack()[0][3]+" bad parameter (big) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    #
    #   Success cases
    #
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetTimeoutMS(TRC_EUD_Device,100),\
                                inspect.stack()[0][3]+" good parameter (100) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=SUCCESS\
                                )

    return result    
def Test_EUD_TraceSetTransferLength(EUD_dll,TRC_EUD_Device):
    result = SUCCESS
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetTransferLength(0,100),\
                                inspect.stack()[0][3]+" bad handleWrap(0) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    ##result|=utils.verboseprint2( EUD_dll.EUD_TraceSetTransferLength(0xFFFFFFFF,100),\
    ##                            inspect.stack()[0][3]+" bad handleWrap (0xFF) step. Got error: ",\
    ##                            EUD_dll,\
    ##                            expectedresult=FAILURE\
    ##                            )
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetTransferLength(TRC_EUD_Device,0),\
                                inspect.stack()[0][3]+" bad parameter (0) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetTransferLength(TRC_EUD_Device,0xFFFFF),\
                                inspect.stack()[0][3]+" bad parameter (big) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    #
    #   Test that multiples of 128 return failure
    #
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetTransferLength(TRC_EUD_Device,128),\
                                inspect.stack()[0][3]+" bad parameter (128) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetTransferLength(TRC_EUD_Device,128*2),\
                                inspect.stack()[0][3]+" bad parameter (128) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetTransferLength(TRC_EUD_Device,128*10),\
                                inspect.stack()[0][3]+" bad parameter (128) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    #
    #   Success cases
    #
    result|=utils.verboseprint2( EUD_dll.EUD_TraceSetTransferLength(TRC_EUD_Device,1000),\
                                inspect.stack()[0][3]+" good parameter (100) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=SUCCESS\
                                )
    return result

def Test_EUD_CloseTrace(EUD_dll,TRC_EUD_Device):
    result = SUCCESS
    result|=utils.verboseprint2( EUD_dll.EUD_CloseTrace(0),\
                                inspect.stack()[0][3]+" bad handleWrap(0) step. Got error: ",\
                                EUD_dll,\
                                expectedresult=FAILURE\
                                )
    ##result|=utils.verboseprint2( EUD_dll.EUD_CloseTrace(0xFFFFFFFF),\
    ##                            inspect.stack()[0][3]+" bad handleWrap (0xFF) step. Got error: ",\
    ##                            EUD_dll,\
    ##                            expectedresult=FAILURE\
    ##                            )
    #
    #   Success cases
    #
    result|=utils.verboseprint2( EUD_dll.EUD_CloseTrace(TRC_EUD_Device),\
                                inspect.stack()[0][3]+" good parameter step. Got error: ",\
                                EUD_dll,\
                                expectedresult=SUCCESS\
                                )
                                
    #print "Check that EUD Trace device has been de-enumerated from USB"
    #value=raw_input("Has EUD Trace device been de-enumerated from USB? T/F")
    #if value.upper() != "T":
    #    print "Error! Called EUD_CloseTrace but EUD Trace device was not de-enumerated from USB"
    #    result|=FAILURE
    
    return result
    '''
        stress tests:
            circular buffer
                set chunksize to 1kb, machunks to 2
                trans len to 100, set chunksize to 200, machunks to 2
                trans len to 100, set chunksize to 200, machunks to 100
            sequential buffer
                set chunksize to 1kb, machunks to 2
                trans len to 100, set chunksize to 200, machunks to 2
                trans len to 100, set chunksize to 200, machunks to 100
    '''
def Test_EUD_Trace_CircBufStressTests(EUD_dll,TRC_EUD_Device):
    
    import EUDStartTracing
    capturemode=0
    result=SUCCESS
    
    
    translen=1000
    chunksize=1024*2
    maxchunks=4
    timeout_ms=100
    outputdir="C:\\temp\\traces"
    if os.path.isdir(outputdir):
        shutil.rmtree(outputdir)
    
    result = EUDStartTracing.EUD_SetTraceParameters(EUD_dll,TRC_EUD_Device,capturemode,maxchunks,chunksize,translen,timeout_ms,outputdir)
    if SUCCESS != utils.verboseprint2(result,\
        inspect.stack()[0][3]+" - EUD_SetTraceParameters step. Got error: ",\
        EUD_dll,\
        expectedresult=SUCCESS,\
        ):
        return FAILURE
        
    result|=utils.verboseprint2( EUD_dll.EUD_StartTracing(TRC_EUD_Device),\
                                inspect.stack()[0][3]+" first Starttracing step. Got error: ",\
                                EUD_dll,\
                                expectedresult=SUCCESS\
                                )
    
    translen=100
    chunksize=200
    maxchunks=2
    timeout_ms=100
    outputdir="C:\\temp\\traces"
    if os.path.isdir(outputdir):
        shutil.rmtree(outputdir)
    
    
    result = EUDStartTracing.EUD_SetTraceParameters(EUD_dll,TRC_EUD_Device,capturemode,maxchunks,chunksize,translen,timeout_ms,outputdir)
    if SUCCESS != utils.verboseprint2(result,\
        inspect.stack()[0][3]+" - EUD_SetTraceParameters step. Got error: ",\
        EUD_dll,\
        expectedresult=SUCCESS,\
        ):
        return FAILURE
        
    result|=utils.verboseprint2( EUD_dll.EUD_StartTracing(TRC_EUD_Device),\
                                inspect.stack()[0][3]+" second Starttracing step. Got error: ",\
                                EUD_dll,\
                                expectedresult=SUCCESS\
                                )
    
    translen=100
    chunksize=200
    maxchunks=100
    timeout_ms=100
    outputdir="C:\\temp\\traces"
    if os.path.isdir(outputdir):
        shutil.rmtree(outputdir)
    
                                
    result = EUDStartTracing.EUD_SetTraceParameters(EUD_dll,TRC_EUD_Device,capturemode,maxchunks,chunksize,translen,timeout_ms,outputdir)
    if SUCCESS != utils.verboseprint2(result,\
        inspect.stack()[0][3]+" - EUD_SetTraceParameters step. Got error: ",\
        EUD_dll,\
        expectedresult=SUCCESS,\
        ):
        return FAILURE
        
    result|=utils.verboseprint2( EUD_dll.EUD_StartTracing(TRC_EUD_Device),\
                                inspect.stack()[0][3]+" third Starttracing step. Got error: ",\
                                EUD_dll,\
                                expectedresult=SUCCESS\
                                )
                                
    return result
    
def Test_EUD_Trace_SeqBufStressTests(EUD_dll,TRC_EUD_Device):
       
    import EUDStartTracing
    capturemode=1
    result=SUCCESS
    
    translen=1000
    chunksize=1024*2
    maxchunks=2
    timeout_ms=100
    outputdir="C:\\temp\\traces"
    if os.path.isdir(outputdir):
        shutil.rmtree(outputdir)
    
    
    result = EUDStartTracing.EUD_SetTraceParameters(EUD_dll,TRC_EUD_Device,capturemode,maxchunks,chunksize,translen,timeout_ms,outputdir)
    if SUCCESS != utils.verboseprint2(result,\
        inspect.stack()[0][3]+" - EUD_SetTraceParameters step. Got error: ",\
        EUD_dll,\
        expectedresult=SUCCESS,\
        ):
        return FAILURE
        
    result|=utils.verboseprint2( EUD_dll.EUD_StartTracing(TRC_EUD_Device),\
                                inspect.stack()[0][3]+" first Starttracing step. Got error: ",\
                                EUD_dll,\
                                expectedresult=SUCCESS\
                                )
    
    translen=100
    chunksize=200
    maxchunks=2
    timeout_ms=100
    outputdir="C:\\temp\\traces"
    if os.path.isdir(outputdir):
        shutil.rmtree(outputdir)
    
    
    result = EUDStartTracing.EUD_SetTraceParameters(EUD_dll,TRC_EUD_Device,capturemode,maxchunks,chunksize,translen,timeout_ms,outputdir)
    if SUCCESS != utils.verboseprint2(result,\
        inspect.stack()[0][3]+" - EUD_SetTraceParameters step. Got error: ",\
        EUD_dll,\
        expectedresult=SUCCESS,\
        ):
        return FAILURE
        
    result|=utils.verboseprint2( EUD_dll.EUD_StartTracing(TRC_EUD_Device),\
                                inspect.stack()[0][3]+" second Starttracing step. Got error: ",\
                                EUD_dll,\
                                expectedresult=SUCCESS\
                                )
    
    translen=100
    chunksize=200
    maxchunks=100
    timeout_ms=100
    outputdir="C:\\temp\\traces"
    if os.path.isdir(outputdir):
        shutil.rmtree(outputdir)
    
    result = EUDStartTracing.EUD_SetTraceParameters(EUD_dll,TRC_EUD_Device,capturemode,maxchunks,chunksize,translen,timeout_ms,outputdir)
    if SUCCESS != utils.verboseprint2(result,\
        inspect.stack()[0][3]+" - EUD_SetTraceParameters step. Got error: ",\
        EUD_dll,\
        expectedresult=SUCCESS,\
        ):
        return FAILURE
        
        
    result|=utils.verboseprint2( EUD_dll.EUD_StartTracing(TRC_EUD_Device),\
                                inspect.stack()[0][3]+" third Starttracing step. Got error: ",\
                                EUD_dll,\
                                expectedresult=SUCCESS\
                                )
                                
    return result

def TestAll(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: TestAll
        ##
        ##  Description:
        ##      Runs all EUD Trace tests
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      Returns SUCCESS (0) or Fail (-1)
        ##
        #################################################################
    '''
        
    try:
        import EUDPythonAPIs
    except:
        print "Error occurred during import of EUDPythonAPIs. Is the file present in directory?"
        return FAILURE
        
    if EUD_dll is None:
        EUD_dll = EUDPythonAPIs.LoadEUDDLL()
        if EUD_dll is None:
            print "Error! EUD_dll could not be loaded. Is it in an accessible directory?"
            return FAILURE
            
    if deviceID is None:
        deviceID = EUDPythonAPIs.SelectEUDDevice(EUD_dll)
        if deviceID is None:
            kill("Error! No EUD Devices detected")
            
    OverallResult=SUCCESS
    if False:
        TRC_EUD_Device = EUDPythonAPIs.EUDInitTRCDevice(EUD_dll,deviceID=deviceID)
        if TRC_EUD_Device is None:
            utils.colorprint("EUDInitTRCDevice Test Failed","red")
            return FAILURE
    
        if not SUCCESS == Test_EUD_OpenTrace(EUD_dll,TRC_EUD_Device):
            utils.colorprint("Test_EUD_OpenTrace Failed","red")
            OverallResult=FAILURE
    
        if not SUCCESS == Test_EUD_TraceSetChunkSizes(EUD_dll,TRC_EUD_Device):
            utils.colorprint("Test_EUD_TraceSetChunkSizes Failed","red")
            OverallResult=FAILURE
        
        if not SUCCESS == Test_EUD_TraceSetOutputDir(EUD_dll,TRC_EUD_Device):
            utils.colorprint("Test_EUD_TraceSetOutputDir Failed","red")
            OverallResult=FAILURE
    
        #if not SUCCESS == Test_EUD_TraceGetOutputDir(EUD_dll,TRC_EUD_Device):
        #    utils.colorprint("Test_EUD_TraceGetOutputDir Failed","red")
        #    OverallResult=FAILURE
    
        #if not SUCCESS == Test_EUD_TraceSetCaptureMode(EUD_dll,TRC_EUD_Device):
        #    utils.colorprint("Test_EUD_TraceSetCaptureMode Failed","red")
        #    OverallResult=FAILURE
        
        if not SUCCESS == Test_EUD_TraceSetTimeoutMS(EUD_dll,TRC_EUD_Device):
            utils.colorprint("Test_EUD_TraceSetTimeoutMS Failed","red")
            OverallResult=FAILURE    
        
        if not SUCCESS == Test_EUD_TraceSetTransferLength(EUD_dll,TRC_EUD_Device):
            utils.colorprint("Test_EUD_TraceSetTransferLength Failed","red")
            OverallResult=FAILURE
            
        if not SUCCESS == Test_EUD_CloseTrace(EUD_dll,TRC_EUD_Device):
            utils.colorprint("Test_EUD_CloseTrace Failed","red")
            OverallResult=FAILURE
    
    
    #
    #   Init number two, now for stress tests.
    #
    TRC_EUD_Device = EUDPythonAPIs.EUDInitTRCDevice(EUD_dll,deviceID=deviceID)
    if TRC_EUD_Device is None:
        utils.colorprint("EUDInitTRCDevice Second Init Failed","red")
        return FAILURE
        
            

    if not SUCCESS == Test_EUD_Trace_CircBufStressTests(EUD_dll,TRC_EUD_Device):
        utils.colorprint("Test_EUD_Trace_CircBufStressTests Failed","red")
        OverallResult=FAILURE
        
    #if not SUCCESS == Test_EUD_Trace_SeqBufStressTests(EUD_dll,TRC_EUD_Device):
    #    utils.colorprint("Test_EUD_Trace_SeqBufStressTests Failed","red")
    #    OverallResult=FAILURE
    
        
    return OverallResult

def parseoptions():
    """
        Parse out given user options
    """
    argparser = OptionParser(version='%prog 1.0')
    argparser.add_option('-e','--eud',      dest='eud',         help="EUD DLL/SO Object to use",default=None)
    argparser.add_option('-d','--deviceid', dest='deviceID',    help="32bit deviceID value. See EUDDisplayAvailableDevices.py",default=None)
    argparser.add_option('-f','--fpgaflag', dest='FPGAFlag',    help="FPGA EUD instance option.",default=None)
    argparser.add_option('-g','--debugflag', dest='debugflag',  help="Debug EUD instance option.",default=None)
    
    #If -h/--help specified, print out description
    for arg in sys.argv[1:]:
        if ('-h' in arg) or ('--help' in arg):
            print EUDTRCAutomationTest.__doc__
            
    (options,args)=argparser.parse_args()
            
    return options

if __name__ == '__main__':
    
    options=parseoptions()
    TestAll(EUD_dll=options.eud,deviceID=options.deviceID,debugflag=options.debugflag,FPGAFlag=options.FPGAFlag)

    
