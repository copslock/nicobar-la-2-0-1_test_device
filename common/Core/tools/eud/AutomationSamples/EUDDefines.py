﻿#!/user/bin/env python
'''
    @file EUD Defines
    @brief Contains various defines for EUD variables stored
    @author JBILLING
    
    Prerequisites:
            Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
            

    Contains EUDDefines dictionary. Example access:
    import EUDDefines
    myvalue=EUDDefines.EUDDefins['mykey']
    
'''
######################################################################################
##
##        @file EUD Defines
##        @brief Contains various defines for EUD variables stored
##        @author JBILLING
##
##        Prerequisites:
##                Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
##                
##
##        Contains EUDDefines dictionary. Example access:
##        import EUDDefines
##        myvalue=EUDDefines.EUDDefins['mykey']
##
##                             EDIT HISTORY FOR FILE
##        This section contains comments describing changes made to the module.
##           when       who             what, where, why
##           --------   ---             ---------------------------------------------------------
##           10/01/2016 JBILLING        Created
##
######################################################################################
from ctypes import *
import os, sys,ctypes
from optparse import OptionParser

EUD_ERR_CLASS_GENERAL   =0x80000000
EUD_ERR_CLASS_HANDLE    =0x40000000
EUD_ERR_CLASS_USB       =0x20000000
EUD_ERR_CLASS_PERIPH    =0x10000000
EUDDefines={}
EUDDefines.update({
    'EUD_FREQ_120_MHz':0x0,
    'EUD_FREQ_80_MHz':0x1,
    'EUD_FREQ_60_MHz':0x2,
    'EUD_FREQ_40_MHz':0x3,
    'EUD_FREQ_30_MHz':0x4,
    'EUD_FREQ_15_MHz':0x5,
    'EUD_FREQ_7_5_MHz':0x6,
    'EUD_FREQ_3_75_MHz':0x7,
    'EUD_FREQ_1_875_MHz':0x8,
    'EUD_FREQ_0_938_MHz':0x9,
    'EUD_FREQ_0_469_MHz':0xA,
    'EUD_FREQ_0_234_MHz':0xB,
    'EUD_FREQ_0_117_MHz':0xC
    })

EUDDefines.update({
    'SWD_EUD_DELAY_VALUE':18,
    'JTG_EUD_DELAY_VALUE':18,
    })
EUDErrDefines={}
EUDErrDefines.update({
    'EUD_ERR_CTL_ENUMERATION_FAILED':EUD_ERR_CLASS_GENERAL+19,
})

if __name__ == '__main__':
     
    print "##################################################################"
    print "##                                                              ##"
    print "##                EUD Defines                                   ##"
    print "##     Contains various defines for EUD variables stored        ##"
    print "##     in a python dictionary. Access defines by                ##"
    print "##     import EUDDefines                                        ##"
    print "##     value=EUDDefines.EUDDefines['key']                       ##"
    print "##                                                              ##"
    print "##################################################################"
    
    