﻿//====================================================================================================//
//================================ EUD AutomationSamples ReadMe.txt ===========================================//
//====================================================================================================//
//
//  Name:                                                                     
//    AutomationSamples_Readme.txt 
//
//  Description:                                                              
//    Basic info on available automation samples for EUD
//                                                                            
// Copyright (c) 1999, 2011-2019 Qualcomm Technologies, Inc. and/or its subsidiaries. All rights reserved.        
//
//
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who             what, where, why
// --------   ---             ---------------------------------------------------------
// 10/23/2017 JBILLING        Update for multiple devices. EUDPeekPoke added
// 01/12/2017 JBILLING        Various cleanups, documentation done.
// 09/21/2016 JBILLING        Created


////////////////////Notice////////////////////////////
///     All automation scripts have their own      ///
///     help documentation. To view, run the       ///
///     desired python automation script           ///
///     with '-h' or '--help' option.              ///
///     E.G.: 'python EUDReset.py -h'              ///                
////////////////////Notice////////////////////////////

    
Release Notes
    Date: 
        10/23/2017 - Released with deviceID changes and for python 32/64 bit versions.
    Validated with T32 version:
        S.2017.04.000083984
    
    
This document describes the following:
    - Information, Release notes, limitations, fixes of available automation samples
    - Basic instructions for using features available in EUD automation samples directory
    - For T32 based automation samples, version of T32 used.
    
Available scripts
    
    GetEUDStatus
        //Returns:
            //if EUD is connected, mode it is in
            //what's enabled/disabled per ctl register
            //what peripherals are active and in use
    
    EUDDisplayAvailableDevices
        //Displays EUD enabled devices which are connected to the host machine.
        
    EUDPythonAPIs
        //Contains actual python implementations of all subroutines. Python files of these subroutines
        //are simply wrappers to APIs contained in this file.
    
    EUDMSMReset
        //Reset MSM but don't reset EUD
        //Options: -d: Delay in milliseconds between reset asserted and deasserted
    EUDAssertReset
        //Assert SRST (this is the  same as the first portion of MSMReset function)
    EUDDeassertReset
        //De-assert SRST (this is the same as the second portion of MSMReset function)
    
    EUDEnableCharger
        //Enable USB charging of target via EUD
    EUDDisableCharger
        //Disable USB charging of target via EUD
    
    EUDSpoofDetach //actual name of this -> WIll allow system to go to power collapse
        //Useful for allowing system to go to powercollapse    
    EUDSpoofAttach 
        //Connect USB and EUD for PC->MSM communication
        
    EUDPeekPoke
        //Takes T32 script and other options to run on desired device and T32 coreand. 
        //See EUDPeekPoke help for more information
     
    EUDStartTracing
        //Takes various options and starts tracing. See EUDStarTracing help for  more information
    
Other Scripts
    EUDDefines
        //Various defines used in scripts

    
    

        