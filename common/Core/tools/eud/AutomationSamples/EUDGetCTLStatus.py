﻿#!/user/bin/env python
'''
    @file  EUDGetCTLStatus.py
    @brief Polls EUD Control Peripheral for its status register. Returns the value.    
           See also EUDGetCTLStatusString API for readability.    
    @author JBILLING    
    Prerequisites:    
            Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.    
            EUD.dll must be in same directory or in ../EUD/build/x64/Debug    
        
    Available API's:    
      EUDGetCTLStatus      
    
'''
######################################################################################
##
##        @file  EUDGetCTLStatus.py
##        @brief Polls EUD Control Peripheral for its status register. Returns the value.
##               See also EUDGetCTLStatusString API for readability.
##        @author JBILLING
##        Prerequisites:
##                Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
##                EUD.dll must be in same directory or in ../EUD/build/x64/Debug
##                Expected to be run with EUDPythonAPIs  present.
##
##        Available API's:
##            EUDMSMAssertReset
##
##                             EDIT HISTORY FOR FILE
##        This section contains comments describing changes made to the module.
##           when       who             what, where, why
##           --------   ---             ---------------------------------------------------------
##           10/02/2017 JBILLING        Simplified to just call EUDPythonAPIs
##           01/11/2017 JBILLING        Changed Initialization API names
##           10/01/2016 JBILLING        Created
##
######################################################################################
import os, sys,ctypes
from ctypes import *
from optparse import OptionParser

##====---------------------------------------------------====##
##====                Global Variables                   ====##
##====---------------------------------------------------====##
global DEBUGFLAG
DEBUGFLAG=False
global FPGAFLAG
FPGAFLAG=False

global SUCCESS, FAILURE
SUCCESS=0
FAILURE=-1

##====--------------------------------------------------====##
##====                  Python API's                    ====##
##====--------------------------------------------------====##

def EUDGetCTLStatus(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: EUDGetCTLStatus
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Gets CTL status register from EUD API. Returns numeric 
        ##      value of EUD CTL status register.
        ##  
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object, optional.
        ##          DLL object to be used to imported by LoadLibrary.
        ##          If not given, will attempt to load DLL from 
        ##          default paths.
        ##      
        ##  Returns:
        ##      Numeric representation of CTL status register,
        ##      Least significant bit from hardware being bit 0 
        ##      of returned value (little endian).
        ##      See EUD Documentation for more information.
        ##
        #################################################################
    '''
    try:
        import EUDPythonAPIs
    except:
        print "Error! Could not import EUDPythonAPIs. Is the file present?"
        sys.exit(1)
    
    return EUDPythonAPIs.EUDGetCTLStatus(EUD_dll=EUD_dll,deviceID=deviceID,debugflag=debugflag,FPGAFlag=FPGAFlag)
    
def parseoptions():
    """
        Parse out given user options
    """
    argparser = OptionParser(version='%prog 1.0')
    argparser.add_option('-e','--eud',      dest='eud',         help="EUD DLL/SO Object to use",default=None)
    argparser.add_option('-d','--deviceid', dest='deviceID',    help="32bit deviceID value. See EUDDisplayAvailableDevices.py",default=None)
    argparser.add_option('-f','--fpgaflag', dest='FPGAFlag',    help="FPGA EUD instance option.",default=None)
    argparser.add_option('-g','--debugflag', dest='debugflag',  help="Debug EUD instance option.",default=None)
    
    #If -h/--help specified, print out description
    for arg in sys.argv[1:]:
        if ('-h' in arg) or ('--help' in arg):
            print EUDGetCTLStatus.__doc__
            
    (options,args)=argparser.parse_args()
            
    return options
    
def test(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    rvalue=SUCCESS
    rvalue=EUDGetCTLStatus(EUD_dll=EUD_dll,deviceID=deviceID,debugflag=options.debugflag,FPGAFlag=options.FPGAFlag)
    
	#Ctl status register shouldn't populate bits 31:21. Err returns will, however.
    if (rvalue&0xFF000000 != 0) or (rvalue == 0x0):
        print "FAILURE"
    else:
        print "SUCCESS"
    return rvalue
    
if __name__ == '__main__':
    options=parseoptions()
    EUDGetCTLStatus(EUD_dll=options.eud,deviceID=options.deviceID,debugflag=options.debugflag,FPGAFlag=options.FPGAFlag)
    
