﻿#!/user/bin/env python
'''
    @file EUDGetVersion
    @brief Retrieves EUD Version ID (Major, Minor, Spin)
    #author JBILLING

    Prerequisites:
            Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.      
            EUD.dll must be in same directory or in ../EUD/build/x64/Debug      
            Expected to be run with EUDPythonAPIs  present.
        
    Available API's:
        EUDGetVersion
    
'''
######################################################################################
##
##    @file EUDGetVersion
##    @brief Retrieves EUD Version ID (Major, Minor, Spin)
##    #author JBILLING
##
##    Prerequisites:
##            Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.      
##            EUD.dll must be in same directory or in ../EUD/build/x64/Debug      
##            Expected to be run with EUDPythonAPIs  present.
##        
##    Available API's:
##        EUDGetVersion
##
##                             EDIT HISTORY FOR FILE
##        This section contains comments describing changes made to the module.
##           when       who             what, where, why
##           --------   ---             ---------------------------------------------------------
##           10/27/2017 JBILLING        Created
##
######################################################################################
import os, sys,ctypes
from ctypes import *
from optparse import OptionParser
import time
##====---------------------------------------------------====##
##====                Global Variables                   ====##
##====---------------------------------------------------====##
global DEBUGFLAG
DEBUGFLAG=False
global FPGAFLAG
FPGAFLAG=False

global SUCCESS, FAILURE
SUCCESS=0
FAILURE=-1

##====--------------------------------------------------====##
##====                  Python API's                    ====##
##====--------------------------------------------------====##

def EUDGetVersion(EUD_dll=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: EUDGetVersion
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Retrieves EUD Version ID (Major, Minor, Spin)
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      Python string of version: Major_Minor_Spin (e.g. 1_14_3)
        ##      
        #################################################################
    '''
    try:
        import EUDPythonAPIs
    except:
        print "Error! Could not import EUDPythonAPIs. Is the file present?"
        sys.exit(1)
    
    eudversionstring = EUDPythonAPIs.EUDGetVersion(EUD_dll=EUD_dll,debugflag=DEBUGFLAG,FPGAFlag=FPGAFlag)
    
    print "EUD Version: "+str(eudversionstring)
    
    time.sleep(2)
    
    return eudversionstring
    
    
def parseoptions():
    """
        Parse out given user options
    """
    argparser = OptionParser(version='%prog 1.0')
    argparser.add_option('-e','--eud',      dest='eud',         help="EUD DLL/SO Object to use",default=None)
    argparser.add_option('-f','--fpgaflag', dest='FPGAFlag',    help="FPGA EUD instance option.",default=None)
    argparser.add_option('-g','--debugflag', dest='debugflag',  help="Debug EUD instance option.",default=None)
    
    #If -h/--help specified, print out description
    for arg in sys.argv[1:]:
        if ('-h' in arg) or ('--help' in arg):
            print EUDMSMAssertReset.__doc__
            
    (options,args)=argparser.parse_args()
            
    return options
    
def test(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    rvalue=SUCCESS
    rvalue=EUDGetVersion(EUD_dll=EUD_dll,debugflag=options.debugflag,FPGAFlag=options.FPGAFlag)
    if rvalue == 0x0:
        print "SUCCESS"
    else:
        print "FAILURE"
    return rvalue
    
if __name__ == '__main__':
    options=parseoptions()
    EUDGetVersion(EUD_dll=options.eud,debugflag=options.debugflag,FPGAFlag=options.FPGAFlag)
    
