﻿#!/user/bin/env python
'''
    @file EUDAssertReset
    @brief Asserts eud_rctlr_srst_n bit, to hold MSM in reset until EUDMSMDeassertReset is called.
    The Reset Ctlr does not reset EUD when EUD initiates a 
    system reset (USB connection to EUD won't be lost 
    across MSM reset).
    #author JBILLING
    #bug Assert/Deassert reset does not work on FPGA, only on full target.

    Prerequisites:
            Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.      
            EUD.dll must be in same directory or in ../EUD/build/x64/Debug      
            Expected to be run with EUDPythonAPIs  present.
        
    Available API's:
        EUDMSMAssertReset
    
'''
######################################################################################
##
##        @file EUDAssertReset
##        @brief Asserts eud_rctlr_srst_n bit, to hold MSM in reset until EUDMSMDeassertReset is called.
##        The Reset Ctlr does not reset EUD when EUD initiates a 
##        system reset (USB connection to EUD won't be lost 
##        across MSM reset).
##        #author JBILLING
##        #bug Assert/Deassert reset does not work on FPGA, only on full target.
##
##        Prerequisites:
##                Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
##                EUD.dll must be in same directory or in ../EUD/build/x64/Debug
##                Expected to be run with EUDPythonAPIs  present.
##
##        Available API's:
##            EUDMSMAssertReset
##
##                             EDIT HISTORY FOR FILE
##        This section contains comments describing changes made to the module.
##           when       who             what, where, why
##           --------   ---             ---------------------------------------------------------
##           10/02/2017 JBILLING        Simplified to just call EUDPythonAPIs
##           01/11/2017 JBILLING        Changed Initialization API names
##           10/01/2016 JBILLING        Created
##
######################################################################################
import os, sys,ctypes
from ctypes import *
from optparse import OptionParser

##====---------------------------------------------------====##
##====                Global Variables                   ====##
##====---------------------------------------------------====##
global DEBUGFLAG
DEBUGFLAG=False
global FPGAFLAG
FPGAFLAG=False

global SUCCESS, FAILURE
SUCCESS=0
FAILURE=-1

##====--------------------------------------------------====##
##====                  Python API's                    ====##
##====--------------------------------------------------====##

def EUDMSMAssertReset(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: EUDMSMAssertReset
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Asserts eud_rctlr_srst_n bit, to hold MSM in reset until 
        ##      EUDMSMDeassertReset is called.
        ##      The Reset Ctlr does not reset EUD when EUD initiates a 
        ##      system reset (USB connection to EUD won't be lost 
        ##      across MSM reset).
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object, optional.
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      deviceID=(uint32)deviceID
        ##          32 bit device specifier value. 
        ##          Run EUDDisplayAvailableDevices.py to view EUD devices connected 
        ##          to host PC.
        ##  Returns:
        ##      Numeric Error code. 0 for success.
        ##
        #################################################################
    '''
    try:
        import EUDPythonAPIs
    except:
        print "Error! Could not import EUDPythonAPIs. Is the file present?"
        sys.exit(1)
    
    return EUDPythonAPIs.EUDMSMAssertReset(EUD_dll=EUD_dll,deviceID=deviceID,debugflag=debugflag,FPGAFlag=FPGAFlag)
    
    
def parseoptions():
    """
        Parse out given user options
    """
    argparser = OptionParser(version='%prog 1.0')
    argparser.add_option('-e','--eud',      dest='eud',         help="EUD DLL/SO Object to use",default=None)
    argparser.add_option('-d','--deviceid', dest='deviceID',    help="32bit deviceID value. See EUDDisplayAvailableDevices.py",default=None)
    argparser.add_option('-f','--fpgaflag', dest='FPGAFlag',    help="FPGA EUD instance option.",default=None)
    argparser.add_option('-g','--debugflag', dest='debugflag',  help="Debug EUD instance option.",default=None)
    
    #If -h/--help specified, print out description
    for arg in sys.argv[1:]:
        if ('-h' in arg) or ('--help' in arg):
            print EUDMSMAssertReset.__doc__
            
    (options,args)=argparser.parse_args()
            
    return options
    
def test(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    rvalue=SUCCESS
    rvalue=EUDMSMAssertReset(EUD_dll=EUD_dll,deviceID=deviceID,debugflag=options.debugflag,FPGAFlag=options.FPGAFlag)
    if rvalue == 0x0:
        print "SUCCESS"
    else:
        print "FAILURE"
    return rvalue
    
if __name__ == '__main__':
    options=parseoptions()
    EUDMSMAssertReset(EUD_dll=options.eud,deviceID=options.deviceID,debugflag=options.debugflag,FPGAFlag=options.FPGAFlag)
    
