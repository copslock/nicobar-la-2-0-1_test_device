﻿#!/user/bin/env python
'''
    @file EUDStartTracing
    
    @brief Collects traces from EUD Trace Peripheral. Traces are collected in chunks of <chunksize>, up
    to full size of <maxchunks> in a circular buffer mode of capture (once last chunk is written to, first
    chunk is overwritten). Size of each chunk and number of chunks are settable parameters but have defaults 
    defined within EUD software (should be ~1MB chunk sizes and ~10MB max size defaults).
    Trace underflow is common. The EUD.dll/.so will stop capturing when a USB timeout occurs.
    
    If the user runs EUDStartTracing.py by default, the script will attempt to run again forever after an underflow
    
    Additionally, other important parameters can be set. See parameter list.
    
    
    Parameters:
    
        The following parameters can be given:
            -e --eud        EUD DLL/SO instance. If left blank, EUDStartTracing will have EUDPythonAPIs locate one from 
                            <EUDStartTracing_directory>\..\bin\..\
                            
            -d --deviceid   If multiple EUD devices are detected, you can pass this parameter (same hex ID given by 'adb devices' command)
                            and EUDStartTracing will capture trace from that specified device. If not specified, EUDPythonAPIs.SelectEUDDevice
                            will be called and, if multiple devices are present, the user will be prompted.
                            
            -m --metabuild  If a particular metabuild is desired, use this command to change to that directory and use EUD.dll/.so from there.
                            Warning - some older metabuilds may not be backwards compatible with all commands.
            
            -k --maxchunks  This indicates the maximum size (sum of chunks) to be collected. Default is 10 MegaBytes. Max is 1 Gigabyte.
            
            -z --chunksize  This indicates the size of chunks to be saved. Default is 1 MegaByte. Max is 10 MegaBytes.
            
            -d --directory  This is the directory to save trace files at. Default is the OS temp directory. The location will be 
                            printed at the start of gathering traces.
                            
            -t --timeout    This is an EUD peripheral parameter. It indicates a timeout period (time = timeout*125usec).
                            Each time a transfer is completed, the transfer timer is restarted. If the transfer does not complete
                            before the time expires, the Trace Peripheral terminates the transfer by sending either a zero
                            or partial ength packet and resetting the transfer packet counter and transfer timer.
            
            -l --length     This is an EUD peripheral parameter. It indicates the size of transactions from EUD peripheral
                            to EUD host software. This is by default 1000bytes. 
                            <<Note>> - Due to hardware limitations, certain values are not allowed. Specifically, 
                            values that are a multiple of trace peripheral's internal buffer size (128bytes) are not allowed.
                            This may be changed in a future revision.
                            
            -r --retryloop  In case of a trace underflow, EUDStartTracing will attempt to re-initialize and retry trace capture.
                            Set this value to False if you would like EUDStartTracing to simply stop on event of an underflow.
                            If you are seeing frequent underflows, it is recommended to 
                                1. Increase data rate from the device (it could be trace data is not streaming fast enough)
                                2. Use a shorter trace length (-l), so that EUD Trace  peripheral waits less time to fill up 
                                   a packet ,since packets are shorter, before sending it out
                                2. use a longer timeout (-t). 
            
            -o --output     Specify output directory. Default is at C:\temp\EUD_Trace_<timestamp>        
            
            -w --waitflag   Normally, EUDStartTracing will initialize  trace peripheral and wait for user response before  actually 
                            capturing traces. If -w False is specified, then EUDStartTracing will immediately begin capture.
                            
            -f --fpgaflag   Specifies to EUDPythonAPIs.LoadEUDDLL to load EUD FPGA flavor for use on XILINX FPGA platform. Default is silicon.
            
            -g --debugflag  Specifies to EUDPythonAPIs.LoadEUDDLL to load EUD.dll/.so flavor with debug symbols. Default is release  version.
            
    
    @author JBILLING
    @bug Stop trace not functional at this time. EUDStartTracing attempts to collect traces forever until force cancelled. (10/27/2017)
    
    Prerequisites:
            Expected Python version: Python 2.7-2.*, 32 or 64bit. Not compatible with Python3.0 and above.      
            EUD.dll must be in ..\bin\x64\Release or ..\bin\Win32\Release etc. directories.
            Expected to be run with EUDPythonAPIs  present in the same directory.
        
    Available API's:
        EUDStartTracing
        
    
'''
######################################################################################
##
##   @file EUDStartTracing
##    
##    @brief Collects traces from EUD Trace Peripheral. Traces are collected in chunks of <chunksize>, up
##    to full size of <maxchunks> in a circular buffer mode of capture (once last chunk is written to, first
##    chunk is overwritten). Size of each chunk and number of chunks are settable parameters but have defaults 
##    defined within EUD software (should be ~1MB chunk sizes and ~10MB max size defaults).
##    Trace underflow is common. The EUD.dll/.so will stop capturing when a USB timeout occurs.
##    
##    If the user runs EUDStartTracing.py by default, the script will attempt to run again forever after an underflow
##    
##    Additionally, other important parameters can be set. See parameter list.
##    
##    
##    Parameters:
##    
##        The following parameters can be given:
##            -e --eud        EUD DLL/SO instance. If left blank, EUDStartTracing will have EUDPythonAPIs locate one from 
##                            <EUDStartTracing_directory>\..\bin\..\
##                            
##            -d --deviceid   If multiple EUD devices are detected, you can pass this parameter (same hex ID given by 'adb devices' command)
##                            and EUDStartTracing will capture trace from that specified device. If not specified, EUDPythonAPIs.SelectEUDDevice
##                            will be called and, if multiple devices are present, the user will be prompted.
##                            
##            -m --metabuild  If a particular metabuild is desired, use this command to change to that directory and use EUD.dll/.so from there.
##                            Warning - some older metabuilds may not be backwards compatible with all commands.
##            
##            -k --maxchunks  This indicates the maximum size (sum of chunks) to be collected. Default is 10 MegaBytes. Max is 1 Gigabyte.
##            
##            -z --chunksize  This indicates the size of chunks to be saved. Default is 1 MegaByte. Max is 10 MegaBytes.
##            
##            -d --directory  This is the directory to save trace files at. Default is the OS temp directory. The location will be 
##                            printed at the start of gathering traces.
##                            
##            -t --timeout    This is an EUD peripheral parameter. It indicates a timeout period (time = timeout*125usec).
##                            Each time a transfer is completed, the transfer timer is restarted. If the transfer does not complete
##                            before the time expires, the Trace Peripheral terminates the transfer by sending either a zero
##                            or partial ength packet and resetting the transfer packet counter and transfer timer.
##            
##            -l --length     This is an EUD peripheral parameter. It indicates the size of transactions from EUD peripheral
##                            to EUD host software. This is by default 1000bytes. 
##                            <<Note>> - Due to hardware limitations, certain values are not allowed. Specifically, 
##                            values that are a multiple of trace peripheral's internal buffer size (128bytes) are not allowed.
##                            This may be changed in a future revision.
##                            
##            -r --retryloop  In case of a trace underflow, EUDStartTracing will attempt to re-initialize and retry trace capture.
##                            Set this value to False if you would like EUDStartTracing to simply stop on event of an underflow.
##                            If you are seeing frequent underflows, it is recommended to 
##                                1. Increase data rate from the device (it could be trace data is not streaming fast enough)
##                                2. Use a shorter trace length (-l), so that EUD Trace  peripheral waits less time to fill up 
##                                   a packet ,since packets are shorter, before sending it out
##                                2. use a longer timeout (-t). 
##            
##            -o --output     Specify output directory. Default is at C:\temp\EUD_Trace_<timestamp>        
##            
##            -w --waitflag   Normally, EUDStartTracing will initialize  trace peripheral and wait for user response before  actually 
##                            capturing traces. If -w False is specified, then EUDStartTracing will immediately begin capture.
##                            
##            -f --fpgaflag   Specifies to EUDPythonAPIs.LoadEUDDLL to load EUD FPGA flavor for use on XILINX FPGA platform. Default is silicon.
##            
##            -g --debugflag  Specifies to EUDPythonAPIs.LoadEUDDLL to load EUD.dll/.so flavor with debug symbols. Default is release  version.
##            
##    
##      @author JBILLING
##      @bug Stop trace not functional at this time. EUDStartTracing attempts to collect traces forever until force cancelled. (10/27/2017)
##
##    Prerequisites:
##            Expected Python version: Python 2.7-2.*, 32 or 64bit. Not compatible with Python3.0 and above.      
##            EUD.dll must be in ..\bin\x64\Release or ..\bin\Win32\Release etc. directories.
##            Expected to be run with EUDPythonAPIs  present in the same directory.
##        
##    Available API's:
##        EUDStartTracing
##        
##
##                             EDIT HISTORY FOR FILE
##        This section contains comments describing changes made to the module.
##           when       who             what, where, why
##           --------   ---             ---------------------------------------------------------
##           10/27/2017 JBILLING        Support for multiple devices added. Trace loop and additional args added.
##           07/03/2017 JBILLING        Created
##
######################################################################################
import os, sys,ctypes
from ctypes import *
from optparse import OptionParser
import inspect
import time
import sys
try:
    import EUDPythonAPIs
except:
    print "Error occurred during import of EUDPythonAPIs. Is the file present in directory?"
    sys.exit(1)


##====---------------------------------------------------====##
##====                Global Variables                   ====##
##====---------------------------------------------------====##

global EUDDirectories
EUDDirectories=[]
currdir=os.path.dirname(os.path.abspath(__file__))

global DEBUGFLAG
DEBUGFLAG=False
global FPGAFLAG
FPGAFLAG=False

global SUCCESS, FAILURE
SUCCESS=0
FAILURE=-1
global MAXERRSTRINGSIZE
MAXERRSTRINGSIZE=200
global EUDMSMResetHelpString

global DEFAULT_CAPTUREMODE
global DEFAULT_MAXCHUNKS 
global DEFAULT_CHUNKSIZE 
global DEFAULT_TRANSLEN 
global DEFAULT_TIMEOUT
global CIRCULARBUFFERMODE 
global SEQUENTIALBUFFERMODE
CIRCULARBUFFERMODE=0
SEQUENTIALBUFFERMODE=1
DEFAULT_CAPTUREMODE=CIRCULARBUFFERMODE
DEFAULT_MAXCHUNKS=10
DEFAULT_CHUNKSIZE=1024*1024*2

global DEFAULT_TRANSLEN
##Good for high flow usecases (e.g. ETM)
#DEFAULT_TRANSLEN=500
##Good for very high flow usecases (e.g. ETM)
DEFAULT_TRANSLEN=1000
##Good for light flow
#DEFAULT_TRANSLEN=150

#Setting default to near max value, since with low values, once a trace  timeout occurs,
#timeout can reoccur before trace buffer fills to translen, an timeouts reoccur,
#preventing any further traces from being captured.
global DEFAULT_TIMEOUT
DEFAULT_TIMEOUT=1600000

global DEFAULT_OUTPUT
DEFAULT_OUTPUT="use_timestamp"

global DEFAULTOUTPUTBASEDIR
DEFAULTOUTPUTBASEDIR=r'c:\temp'
global DEFAULTOUTPUTDIR
DEFAULTOUTPUTDIR=os.path.join(DEFAULTOUTPUTBASEDIR,"EUDTrace_"+time.strftime("%d-%m-%Y,%H.%M.%S"))
##====--------------------------------------------------====##
##====                  Python API's                    ====##
##====--------------------------------------------------====##                
                
def getoutputdir(outputdirectory=None):
    '''
        Checks that requested directory does not yet exist and can be created. 
        If directory already exists, appends '_#' where '#' is an iterated number.
        If no directory given, creates a timestamped directory.
        Returns created directory.
    '''
    if outputdirectory is None or "use_timestamp" in outputdirectory:
        outputdirectory=DEFAULTOUTPUTDIR
    
    ##Get a new directory; Avoid collisions.
    idx=1
    while os.path.isdir(outputdirectory):
        outputdirectory=outputdirectory+"_"+str(idx)
        idx+=1
        
    if os.path.isdir(outputdirectory):
        EUDPythonAPIs.colorprint("Error! Unexpected file collision when creating trace directory. Please contact support.","red")
        sys.exit(1)
    else:
        if os.path.isdir(os.path.dirname(outputdirectory)):
            try:
                os.makedirs(outputdirectory)
            except:
                EUDPythonAPIs.colorprint("Error! Given output directory not accessible! Path: "+os.path.dirname(outputdirectory),"red")
                sys.exit(1)
        
    if not os.path.isdir(outputdirectory):
        EUDPythonAPIs.colorprint("Error! Was not able to create trace output directory: "+os.path.dirname(outputdirectory),"red")
        sys.exit(1)
    
    return outputdirectory
    
def kill(message):
    if not message is None:
        print message
    sys.exit(1)

def EUD_SetTraceParameters(EUD_dll,TRC_EUD_Device,capturemode,maxchunks,chunksize,translen,timeout_ms,outputdir):
    '''
    #################################################################
    ##
    ##  Function: EUD_SetTraceParameters
    ##  Sets up default trace values for a newly initialized trace device (passed as TRC_EUD_Device)
    ##  Arguments
    ##      EUD_dll - ctype Windows DLL instance of EUD device.
    ##      TRC_EUD_Device - Trace EUD device initialized via EUDPythonAPIs.EUDInitTRCDevice
    ##      Other arguments: Please see above documentation for argument details
    ##  Returns
    ##      0 for success, else failure
    ##
    #################################################################
    
    '''
    result=SUCCESS
    
    #Open up trace peripheral, get USB values
    result|=EUDPythonAPIs.verboseprint2( EUD_dll.EUD_OpenTrace(TRC_EUD_Device),\
                                inspect.stack()[0][3]+" OpenTrace step. Got error: ",\
                                EUD_dll\
                                )
    
    #Set timeout value.
    result|=EUDPythonAPIs.verboseprint2( EUD_dll.EUD_TraceSetTimeoutMS(TRC_EUD_Device,timeout_ms),\
                                inspect.stack()[0][3]+" set timeout_ms step. Got error: ",\
                                EUD_dll\
                                )
    
    #set transaction.
    result|=EUDPythonAPIs.verboseprint2( EUD_dll.EUD_TraceSetTransferLength(TRC_EUD_Device,c_int(translen)),\
                                inspect.stack()[0][3]+" Set transfer length step. Got error: ",\
                                EUD_dll\
                                )
    
    #Set capture mode.
    #result|=EUDPythonAPIs.verboseprint2( EUD_dll.EUD_TraceSetCaptureMode(TRC_EUD_Device,capturemode),\
    #                            inspect.stack()[0][3]+" set capturemode step. Got error: ",\
    #                            EUD_dll\
    #                            )
    
    #Set chunk size and max chunks.
    result|=EUDPythonAPIs.verboseprint2( EUD_dll.EUD_TraceSetChunkSizes(TRC_EUD_Device,c_int(chunksize),c_int(maxchunks)),\
                                inspect.stack()[0][3]+" Set chunksize and maxchunk step. Got error: " ,\
                                EUD_dll\
                                )
    
    #Set output directory.
    result|=EUDPythonAPIs.verboseprint2( EUD_dll.EUD_TraceSetOutputDir(TRC_EUD_Device,c_char_p(outputdir)),\
                                inspect.stack()[0][3]+" set outputdir step. Got error: ",\
                                EUD_dll\
                                )
    
    return result

def EUDStartTracing(EUD_dll         =None,               \
                    deviceID        =None,                \
                    capturemode     =DEFAULT_CAPTUREMODE,\
                    maxchunks       =DEFAULT_MAXCHUNKS,  \
                    chunksize       =DEFAULT_CHUNKSIZE,  \
                    outputdirectory =None,               \
                    translen        =DEFAULT_TRANSLEN,   \
                    timeout         =DEFAULT_TIMEOUT,    \
                    waitonuserflag  =True                \
                    ):
    '''
        #################################################################
        ##
        ##  Function: EUDStartTracing
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Sets up EUD Trace Peripheral and, after user prompt, (if flag is set True),
        ##      starts collecting traces. Please see full documentation above for parameter
        ##      settings.
        ##
        ##  Arguments:
        ##      Please see above for argument documentation
        ##
        ##  Returns:
        ##      Numeric Error code. 0 for success.
        ##
        #################################################################
    '''
    
    OverallResult=SUCCESS
    try:
        import EUDPythonAPIs
    except:
        print "Error occurred during import of EUDPythonAPIs. Is the file present in directory?"
        return FAILURE
    
    if EUD_dll is None:
        EUD_dll = EUDPythonAPIs.LoadEUDDLL()
        if EUD_dll is None:
            return kill("Error! Could not load EUD DLL")
    
    if deviceID is None:
        deviceID = EUDPythonAPIs.SelectEUDDevice(EUD_dll)
        if deviceID is None:
            kill("Error! No EUD Devices detected")
    elif type(deviceID) is str:
        deviceID=long(deviceID,16)
    
    outputdirectory=getoutputdir(outputdirectory=outputdirectory)
    
    TRC_EUD_Device = EUDPythonAPIs.EUDInitTRCDevice(EUD_dll=EUD_dll,deviceID=deviceID)
    if TRC_EUD_Device is None:
        EUDPythonAPIs.colorprint("EUDInitTRCDevice Test Failed","red")
        return FAILURE
    
    #
    #   Set trace parameters.
    #
    if SUCCESS != EUD_SetTraceParameters(   EUD_dll,\
                                            TRC_EUD_Device,\
                                            capturemode, \
                                            maxchunks,\
                                            chunksize,\
                                            translen,\
                                            timeout,\
                                            outputdirectory\
                                            ):
                                        
        EUDPythonAPIs.colorprint(inspect.stack()[0][3]+" - EUD_SetTraceParameters step Failed. Exiting.","red")
        time.sleep(3)
        return FAILURE
    
    #
    #   Print Output Directory (Todo - eventually use EUD_TraceGetOutputDir
    #    
    if outputdirectory is None:
        print "Traces output to c:\\temp\\EUDTrace<timestamp>"
    else:
        print "Traces output to "+str(outputdirectory)

    #
    #   Start QDSS flowing now
    #
    if waitonuserflag is True:
        print "Start QDSS flow. Capture trace is next step."
        input=raw_input("Press a key and ENTER to start tracing")

    print "Starting Trace. Use 'Ctrl-C' to stop tracing"
    #thing=raw_input("Starting Trace. Press any key to  stop tracing")
    
    if SUCCESS != EUDPythonAPIs.verboseprint2( EUD_dll.EUD_StartTracing(TRC_EUD_Device),\
                                inspect.stack()[0][3]+"EUD_StartTrace StartTrace step. Got error: ",\
                                EUD_dll\
                                ):
        return FAILURE
    else:
        return SUCCESS

    
def StopTrace(EUD_dll, TRC_EUD_Device):
    return EUD_dll.EUD_StopTrace(TRC_EUD_Device)
def begintrace(EUD_dll, TRC_EUD_Device):
    return EUD_dll.EUD_StartTracing(TRC_EUD_Device)
    
def parseoptions():
    """
        Parse out given user options
    """
    argparser = OptionParser(version='%prog 1.0')
    argparser.add_option('-e','--eud',      dest='eud',         help="EUD DLL/SO Object to use",default=None)
    argparser.add_option('-d','--deviceid', dest='deviceID',    help="32bit deviceID value. See EUDDisplayAvailableDevices.py",default=None)
    
    argparser.add_option('-k','--maxchunks', dest='maxchunks',     help="maxchunks",default=DEFAULT_MAXCHUNKS)
    argparser.add_option('-z','--chunksize', dest='chunksize',     help="chunksize",default=DEFAULT_CHUNKSIZE)
    argparser.add_option('-t','--timeout', dest='timeout',     help="trace timeout value. recommend  large (> 10000)",default=DEFAULT_TIMEOUT)
    argparser.add_option('-l','--length', dest='translen',     help="trace transfer length size. 50 is for  very slow. 1000  for fast.",default=DEFAULT_TRANSLEN)
    
    argparser.add_option('-r','--retryloop', dest='retryloop',   help="True|False. Option to only attempt one pull of traces from device. Else continual loop option is used.",default=True)
    argparser.add_option('-m','--metabuild', dest='meta',     help="metabuild to use",default=None)
    argparser.add_option('-o','--output', dest='outputdir',     help="trace output directory",default=DEFAULT_OUTPUT)
    argparser.add_option('-w','--waitflag', dest='waitflag',     help="Wait on user flag. If true, dialog will occur before capture of traces",default=True)
    
    argparser.add_option('-f','--fpgaflag', dest='FPGAFlag',    help="FPGA EUD instance option.",default=False)
    argparser.add_option('-g','--debugflag', dest='debugflag',  help="Debug EUD instance option.",default=False)
    
    
    #If -h/--help specified, print out description
    for arg in sys.argv[1:]:
        if ('-h' in arg) or ('--help' in arg):
            print EUDStartTracing.__doc__
            
    (options,args)=argparser.parse_args()
            
    return options
    
def test(EUD_dll=None,deviceID=None):
    rvalue=SUCCESS
    rvalue=EUDStartTracing(EUD_dll=EUD_dll,deviceID=deviceID,debugflag=options.debugflag,FPGAFlag=options.FPGAFlag)
    if rvalue == 0x0:
        print "SUCCESS"
    else:
        print "FAILURE"
    return rvalue
    
def main(options):
    ##Select metabuild
    if options.meta is None:
        use_currdir=True
    else:
        use_currdir = False
    
    if options.eud is None:
        options.eud = EUDPythonAPIs.LoadEUDDLL()
        if options.eud is None:
            return kill("Error! Could not load EUD DLL")
    
    if options.deviceID is None:
        options.deviceID = EUDPythonAPIs.SelectEUDDevice(options.eud)
        if options.deviceID is None:
            kill("Error! No EUD Devices detected")
    
    
    baseoutputdir=getoutputdir(options.outputdir)
    
    if use_currdir is True:
            import EUDStartTracing as trc
    else:
        if os.path.isfile(os.path.join(meta,"common","core","tools","eud","AutomationSamples","EUDStartTracing.py")):
            sys.path.append(os.path.join(meta,"common","core","tools","eud","AutomationSamples"))
            import EUDStartTracing as trc
        else:
            print "Error! could not find EUDStartTracing.py using given meta: "+str(meta)
            sys.exit(1)
    
    if "false" in str(options.retryloop).lower():
        try:
            trc.EUDStartTracing(    EUD_dll=options.eud,\
                                    deviceID=options.deviceID,\
                                    outputdirectory=baseoutputdir,\
                                    timeout=options.timeout,\
                                    maxchunks=options.maxchunks,\
                                    chunksize=options.chunksize,\
                                    translen=options.translen,\
                                    waitonuserflag=options.waitflag\
                                    )
        except KeyboardInterrupt:
                sys.exit(1)
    else:
        ##Loop forever.
        counter=0
        while(True):
            counter+=1
            if counter is 1:
                waitflag = options.waitflag
            else:
                waitflag = False
            try:
                outputdir=os.path.join(baseoutputdir,"trace_"+str(counter))
                trc.EUDStartTracing(EUD_dll=options.eud,\
                                    deviceID=options.deviceID,\
                                    outputdirectory=outputdir,\
                                    timeout=options.timeout,\
                                    maxchunks=options.maxchunks,\
                                    chunksize=options.chunksize,\
                                    translen=options.translen,\
                                    waitonuserflag=waitflag\
                                    )
            except KeyboardInterrupt:
                sys.exit(1)
                
    return rvalue
    
    
if __name__ == '__main__':
    options=parseoptions()
    main(options)
    
