﻿# -*- coding: utf-8 -*-
#!/usr/bin/env python
"""
    @file EUDPeekPoke 
    @brief EUDPeekPoke tool parses input instructions and writes contents of registers to output directory
    #author KCHIMA
    
    Prerequisites:
        Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
        # ADD MORE
        
    Available API's:
        Information subroutines:
            # ADD STUFF
        T32 Launcher subroutines:
           startT32
"""

#====================================================================
#  Created on Thu June 29 11:54:24 2017
#  Name:
#    EUDPeekPoke.py
#
#  Description:
#    Opens T32 and runs provided cmm or cfg script
#
#  Usage
#      'python EUDPeekPoke.py -c <configfile> -o <outputdir> -t <time> \
#                             -w <t32window> -e <endofprogramfile> -d <deviceid> \
#                             -a <apiport> -m <intercomport>
#
# Copyright (c) 2017 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
#
#
#                      EDIT HISTORY FOR FILE
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when               who           what, where, why
# --------           ---           -----------------------------
# 11/17/2017         JBILLING      Additional options (-s,-n) for window closure options
# 10/27/2017         JBILLING      Added multi device support
# 06/29/2017         KCHIMA        Created
#

import os
import sys
import xml
import xml.etree.ElementTree
import datetime
import ctypes
from ctypes import *
import argparse
import unittest

##====---------------------------------------------------====##
##====                Global Variables                   ====##
##====---------------------------------------------------====##
global testMode
testMode = False

global defaultMaxTime #in seconds
defaultMaxTime = 120

global defaultT32window
defaultT32window = 'APPS0'

global WorkingDir
global CurrDir
CurrDir = os.path.abspath(os.path.dirname((__file__)))
os.chdir(CurrDir)
WorkingDir = os.getcwd()

    
global configFilePath
configFilePath = ''
global tempCMMscript

global outputScript
global logFile
global tempscriptCopy
outputScript = None
logFile = None
tempscriptCopy = None
tempCMMscript = None

global outputFileName
global logFileName
global tempCMMscriptName
global tempscriptCopyName
global endOfProgramFilePath
global defaultEOPFilename
outputFileName = "output.txt"
logFileName = "log.txt"
tempCMMscriptName = ""
tempscriptCopyName = "tempscriptCopy.cmm"
endOfProgramFilePath = ""
defaultEOPFilename = "endOfProgram.txt"

global validKeywords
validKeywords = ['DATA.READ','DATA.SET']

global generatedScripts
generatedScripts = []

global TestDir
TestDir=os.path.join(CurrDir,"..","Trace32","TestScripts","common","core","t32","shellscripts")

global T32WindowLauncherPaths
T32WindowLauncherPaths=[
        os.path.join(os.getcwd(),"..","..","..","..","t32","shellscripts"),
        os.path.join(os.getcwd(),"..","..","..","t32","shellscripts"),
        os.path.join(os.getcwd(),"..","..","t32","shellscripts"),
        os.path.join(os.getcwd(),".."),
        TestDir,
    ]
    
global UseTestDir_Flag
if os.path.isdir(TestDir):
    UseTestDir_Flag=True
else:
    UseTestDir_Flag=False

##This flag set in startT32 for automation cases
global TestSetup_Flag
TestSetup_Flag  = False

##
##  Find prependScripts
##
global prependScripts
scriptpaths=[CurrDir]
if os.path.isdir(os.path.join(CurrDir,'EUDPeekPoke')):
    scriptpaths.append(os.path.join(CurrDir,'EUDPeekPoke'))
if os.path.isdir(os.path.join(TestDir,'EUDPeekPoke')):
    scriptpaths.append(os.path.join(TestDir,'EUDPeekPoke'))

prependscriptdir=None
for scriptpath in scriptpaths:
    if 'attachscript.cmm' in os.listdir(scriptpath):
        prependscriptdir=scriptpath
        break
if prependscriptdir is None:
    print "Error! Could not find prerequisite scripts (attachscript.cmm, errorhandlescript.cmm)"
    sys.exit(1)
elif TestDir in prependscriptdir:
    TestSetup_Flag = True
    UseTestDir_Flag=True
    
prependScripts=[os.path.join(prependscriptdir,'attachscript.cmm'), os.path.join(prependscriptdir,'errorhandlescript.cmm')]
    


global console
global color_red
global color_yellow
global color_green
global color_black
global color_blue
global color_intensitfy
global STD_OUTPUT_HANDLE
console = -1
color_red = 12
color_yellow = 14
color_green = 10
color_black = 7
color_blue = 9
color_intensify = 8 #subtract from color_* to remove intensity
STD_OUTPUT_HANDLE = -11

global tab
tab = "    "

global readStatement
global writeStatement2
global writeStatement
global modifyStatement
global assignsymStatement
global printStatement
readStatement       = ''
writeStatement      = 'd.s  {register}  %le  %long  {data}'
modifyStatement     = 'd.s  {register}  %l  (({register})&~({mask}<<{offset}))+({value}<<{offset})'
assignsymStatement  = 'y.create.l     {symbol}     {value}'
printStatement      = 'print  "{value}'

##====---------------------------------------------------====##
##====                End Global Variables               ====##
##====---------------------------------------------------====##
def wait_second(amount=1):
    import time
    try:
        for i in range(0, amount):
            time.sleep(1)
    except KeyboardInterrupt:
        kill()
    
def createREADstatement(arguments):
    """
        Function for converting .cfg read statements to appropriate language.
        Called within processLine() function
        Not currently in use and needs to be defined for future implementation of .cfg
    """
    #read register content
    #add confirmation message
    return

def createWRITEstatement(arguments):
    """
        Function for converting .cfg read statements to appropriate language.
        Called within processLine() function
        Not currently in use and needs to be defined for future implementation of .cfg
    """
    #write to register 
    #do a read to ensure data has been written
    #add confirmation message
    return tab+writeStatement.format(register=arguments[1], data=arguments[2])

def createASSIGNSYMstatement(arguments):
    """
        Function for converting .cfg read statements to appropriate language.
        Called within processLine() function
        Not currently in use and needs to be defined for future implementation of .cfg
    """
    newSymbol = arguments[1]
    return tab+assignsymStatement.format(symbol=newSymbol, value=arguments[2])
    
def createMODIFYstatement(arguments):
    """
        Function for converting .cfg read statements to appropriate language.
        Called within processLine() function
        Not currently in use and needs to be defined for future implementation of .cfg
    """
    return tab+modifyStatement.format(register=arguments[1], mask=arguments[2], offset=arguments[3], value=arguments[4])
    
def createPRINTstatement(arguments):
    """
        Function for converting .cfg read statements to appropriate language.
        Called within processLine() function
        Not currently in use and needs to be defined for future implementation of .cfg
    """
    newLine = ' '.join(arguments[2:])
    return tab+printStatement.format(value=newLine)
    
def createRAWstatement(line):
    """
        Function for converting .cfg read statements to appropriate language.
        Called within processLine() function
        Not currently in use and needs to be defined for future implementation of .cfg
    """
    return tab+line

def processLine(line, lineno):
    '''
        Function for converting .cfg files to appropriate language.
        Not currently in use and needs to be modified for future implementation of .cfg
        The function currently checks for a keyword at the start of the line 
        and the number of arguments following the keyword.
        Note that keywords and required number of arguments are yet to be decided upon.
    '''
    lineno = str(lineno)
    uppercaseLine = line.upper()
    arguments = line.split()
    statement = ""
    
    #ignore line if it is a comment
    if uppercaseLine.startswith(";") or uppercaseLine.startswith("//"):
        return
    
    #check if it is a read statement
    elif uppercaseLine.startswith("READ"):
        if len(arguments) < 2:
            kill("Invalid read instruction at line "+lineno+" >> "+line+"\nExpected syntax: READ {register}")
        statement = createREADstatement(arguments)
        
    #check if it is a write statement
    elif uppercaseLine.startswith("WRITE"):
        if len(arguments) < 3:
            kill("Invalid write instruction at line "+lineno+" >> "+line+"\nExpected syntax: WRITE {register} {data}")
        statement = createWRITEstatement(arguments)
        
    #check if it is an assign statement
    elif uppercaseLine.startswith("ASSIGNSYM"):
        if len(arguments) < 3:
            kill("Invalid write instruction at line "+lineno+" >> "+line+"\nExpected syntax: ASSIGNSYM {symbol} {value}")
        statement = createASSIGNSYMstatement(arguments)
        
    #check if it is a modify statement
    elif uppercaseLine.startswith("MODIFY"):
        if len(arguments) < 6:
            kill("Invalid write instruction at line "+lineno+" >> "+line+"\nExpected syntax: MODIFY {mode} {register/symbol} {} {} {}")
        statement = createMODIFYstatement(arguments)
        
    #check if print statement
    elif uppercaseLine.startswith("PRINT"):
        if len(arguments) < 2:
            kill("Invalid write instruction at line "+lineno+" >> "+line+"\nExpected syntax: PRINT {data}")
        statement = createPRINTstatement(arguments)
        
    else:
        statement = createRAWstatement(line)
        
    writeLine(tempCMMscript, statement)
        
    return

##====---------------------------------------------------====##
##====                Support Functions                  ====##
##====---------------------------------------------------====##
def changeColor(color):
    """
        changes color of terminal print. Works for WINDOWS ONLY. Will cause errors in LINUX
    """
    stdout_handle = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
    windll.kernel32.SetConsoleTextAttribute(stdout_handle, color)
    
def resetColor():
    changeColor(color_black)
  
def kill(msg=None):
    if not msg or testMode:
        sys.exit(1)
    else:
        logError(msg)
        sys.exit(1)

def logSuccess(msg):
    if testMode:
        return
    changeColor(color_green)
    writeLine(console, str(msg))
    resetColor()
    return

def logWarning(msg):
    if testMode:
        return
    changeColor(color_yellow)
    writeLine(console, "Warning! >> " + str(msg))
    resetColor()
    return

def logError(msg=None):
    if not msg or testMode:
        return
    changeColor(color_red)
    writeLine(console, "ERROR! >> " + str(msg))
    resetColor()

def writeLine(file, line):
    """ 
        Writes line to file or console.
    """
    if file == console:
        print line
    elif file is None:
        return
    else:
        if not line.endswith('\n'):
            line = line+'\n'
        file.write(line)
    
    return

def writeHeader(script, scriptDescription=""):
    date = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())
    truncatedSourceFileName = '{:100}'.format(configFilePath)
    header = """//=================== THIS SCRIPT IS AUTOGENERATED ====================
//  Name:                                                                     
//    """+os.path.abspath(script.name)+"""
//
//  Description: 
//    """+scriptDescription+"""                                                             
//                                                                            
//  Copyright (c) 2017 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
//  Confidential and Proprietary        
//
//  WARNING: Autogenerated script. Tweak if you know what you are doing.
//  
//  when                    source                  
//  ----------              --------------------    
//  """+date+"""     """+truncatedSourceFileName+"""
//
//
//

"""
    writeLine(script, header)

def makeNewScript(newScriptPath):
    """
        Makes new script. Deletes script if it already exists before making new script.
        Writes header into script
    """
    if os.path.isfile(newScriptPath):
        os.remove(newScriptPath)
    
    newScript = open(newScriptPath, 'w')
    writeHeader(newScript)
    return newScript

##====---------------------------------------------------====##
##====             End Support Functions                 ====##
##====---------------------------------------------------====##

def createParser():
    """
        Creates parser for getting user options. Returns parser object
    """
    argparser = argparse.ArgumentParser(description='Parse program arguments.')
    
    argparser.add_argument('-c','--config',       dest='config',       help="(Required) config file (e.g. 'sdm845')", default=None)
    argparser.add_argument('-i','--instruction',  dest='instruction',  help="(Optional) single T32 instruction",   default=None)
    argparser.add_argument('-o','--outputdir',    dest='outputdir',    help="(Required) output directory",            default=None,   required=True)
    argparser.add_argument('-w','--t32window',    dest='t32window',    help="(Optional) T32 window (e.g. 'APPS0')",   default=None)
    argparser.add_argument('-t','--maxTime',      dest='maxTime',      help="(Optional) max number of seconds to run the EUDPeekPoke tool",  default=defaultMaxTime, type=int)
    argparser.add_argument('-e','--eopfilepath',  dest='eopFilepath',  help="(Optional) path for file to be generated at end of T32 program",   default=None)
    argparser.add_argument('-d','--deviceid',     dest='deviceid',     help="(Optional) Device ID. If specified, T32 will only attach to device with this ADB serial number.",   default=None)
    argparser.add_argument('-a','--apiport',      dest='apiport',      help="(Optional) API Port number that T32 will be initialized with. Uses default if not specified. Useful to set this in order to launch multiple of the same window to interface with different targets (e.g .device 1 apps0 needs to have a different API  port than device 2 apps0).",   default=None)
    argparser.add_argument('-m','--intercomport', dest='intercomport', help="(Optional) Intercom port number that T32 will be initialized with. Uses default if not specified.",   default=None)
    argparser.add_argument('-s','--closet32',  dest='closet32', help="(Optional) Flag (True or False) indicating whether to close T32 when timeout/eof is finished. Default is True.",   default=True)
    argparser.add_argument('-n','--runinitscripts',  dest='runinitscripts', help="(Optional) Flag (True or False) to indicate whether to run initialization scripts (error handle and attach) prior to called script. Default True",   default=True)
    return argparser

def parseOptions(argparser, args):
    """
        Parses cmd line arguments for running EUDPeekPoke
    """
    global defaultMaxTime
    global endOfProgramFilePath
    global configFilePath
    
    options = argparser.parse_args(args)
    
    if not (options.config or options.instruction):
        argparser.error('No action requested, use -c or -i')
        
    if options.config and options.instruction:
        argparser.error('Multiple actions requested, cannot have -c and -i')
    
    """ get congif/cmm file """
    if options.config:
        if not options.config.endswith('.cfg') and not options.config.endswith('.cmm'):
            kill("Config file must be a .cfg or .cmm script.")
        elif not os.path.isfile(options.config):
            kill("Config file not found.")      
        configFilePath = options.config
    options.config=os.path.abspath(options.config)
    
    """ get output directory """
    changeColor(color_yellow)        
    outputLocation = options.outputdir
    if not os.path.exists(outputLocation):
        outputLocation = os.path.join(WorkingDir,options.outputdir)
        os.makedirs(outputLocation)
    else:
        if options.outputdir in '.':
            options.outputdir = WorkingDir
    resetColor()

    """ get t32 window option """
    if options.t32window is None:
        logWarning("No t32 window provided. Defaulting to APPS0...")
        options.t32window = defaultT32window
    
    """ get max time """
    if options.maxTime < 0:
        logWarning("Max time cannot be negative. Setting max time to default"+defaultMaxTime+"s...")
        options.maxTime = defaultMaxTime
    else:
        logSuccess("MaxTime set to "+str(options.maxTime)+"s")
        

    """ get EOP file option """
    if options.eopFilepath:
        path = os.path.dirname(options.eopFilepath)
        scriptname = os.path.basename(options.eopFilepath)

        if scriptname in '':
            endOfProgramFilePath = os.path.join(WorkingDir,defaultEOPFilename)
            logWarning("Invalid file name. Defaulting to "+endOfProgramFilePath)
        
        elif path in '':
            endOfProgramFilePath = os.path.join(WorkingDir, scriptname)
            
        elif not os.path.exists(path):
            endOfProgramFilePath = os.path.join(WorkingDir, scriptname)
            logWarning("Invalid file path. Defaulting to "+endOfProgramFilePath)
        
        else:
            endOfProgramFilePath = os.path.join(path,scriptname)
    else:
        endOfProgramFilePath = ''
         
    return options

def setupTempCMMscript(outputFile=outputFileName, logFile=logFileName):
    '''
        Function for setting up temporary cmm files translated of .cfg  files
        Not currently approved and needs to be modified for real future implementation of .cfg
        
        The function currently creates writes PRACTICE functions for opening and closing log and output files 
        and methods for logging to output and log files.
    '''
        
    
    setup = '''    
OPEN #1 {output} /Create
OPEN #2 {log} /Create

gosub MAIN
'''

    exitFuncWithEOP = '''
//############# exit function ############
EXIT:
    local &status
    entry %LINE &status
    OPEN #3 {endOfProgramFile} /Create
    WRITE #3 "Program ended. Status: "+"&status"
    CLOSE #1
    CLOSE #2   
    CLOSE #3
    enddo
'''

    exitFuncWithoutEOP = '''
//############# exit function ############
EXIT:
    CLOSE #1
    CLOSE #2   
    enddo
'''

    helperFuncsandMain = '''
    
//########### helper functions ###########
LOGSUCCESS:
    local &statement
    entry %LINE &statement
    WRITE #2 "Success --- "+"&statement"
    RETURN
    
LOGFAIL:
    local &statement
    entry %LINE &statement
    WRITE #2 "Failed --- "+"&statement"
    RETURN

WRITEOUTPUT:
    local &statement
    entry %LINE &statement
    WRITE #1 "Output --- "+"&statement"
    RETURN


//######### test helper functions ########
TEST:
    gosub LOGSUCCESS PASSED LOGSUCCESS TEST
    gosub LOGFAIL PASSED LOGFAIL TEST
    gosub WRITEOUTPUT PASSED WRITEOUTPUT TEST
    RETURN

    
MAIN:
    GLOBALON ERROR GOTO EXIT "Program encountered an error"
'''

    writeLine(tempCMMscript, setup.format(output=outputFile, log=logFile))
    
    if endOfProgramFilePath in '':
        writeLine(tempCMMscript, exitFuncWithoutEOP)
    else:
        writeLine(tempCMMscript, exitFuncWithEOP.format(endOfProgramFile=endOfProgramFilePath))
        
    writeLine(tempCMMscript, helperFuncsandMain)
    return

def startT32(options):
    """     
    - Summary:
        - if input is a script (ie. -c is used and not -i), check if input is not CMM and convert it using processLine function and create a temp CMM script, else run directly
        - open up t32 and send in the raw cmm script if provided, or the translated temp cmm
          (append the eop file path if -e option is enabled)
        - poll for maxtime or eop file path
        - close t32
    """
    
    import fileinput
    
    import tempfile
    tempfile.tempdir = WorkingDir
    
    launchpath=None
    for launcherpath in T32WindowLauncherPaths:
        if  os.path.isdir(launcherpath):
            if  'T32WindowLauncher.py' in os.listdir(launcherpath):
                sys.path.append(launcherpath)
                launchpath=launcherpath
                break
    if launchpath==None:
        kill("Error!  Could not find  directory containing  T32WindowLauncher.py")
    elif TestDir in launchpath:
        UseTestDir_Flag = True
        TestSetup_Flag  = True
    import T32WindowLauncher
    
    t32open = False
    T32dictionary = None
    
    
    
    try:
        if options.config and options.config.endswith('.cfg'):
            '''
               if input is a script and ends with .cfg, create tempscript, make cfg to cmm conversion, 
               open T32 and pass in temp script with endOfProgramFile as an argument.
               Save copy of tempscript
            '''
            outputScriptPath = os.path.join(WorkingDir,options.outputdir,outputFileName)
            logFilePath      = os.path.join(WorkingDir,options.outputdir,logFileName)
            
            global tempCMMscript
            tempCMMscript = tempfile.NamedTemporaryFile(delete=False, suffix='.cmm')
            setupTempCMMscript()
            for line in fileinput.input(configFilePath):
                processLine(line.strip(), fileinput.lineno())
            writeLine(tempCMMscript, "    gosub EXIT")
            tempCMMscript.close() # close temp script before opening up t32
            
            scriptstorun=[]
            if options.runinitscripts is True:
                scriptstorun+=prependScripts
            
            if endOfProgramFilePath in '':
                scriptstorun.append(configFilePath)
            else:
                scriptstorun.append(configFilePath+' '+endOfProgramFilePath)
            
            T32dictionary = T32WindowLauncher.QuickLaunchEUD(options.t32window, scriptstorun, deviceid=options.deviceid,pausetime=10,apiport=options.apiport,intercomport=options.intercomport)
            if T32dictionary:
                t32open = True
        
        elif options.config and options.config.endswith('.cmm'):
            '''
               if input is a script and ends with .cfg, open T32 and pass in cmm script with endofprogramfile as argument
            '''
            scriptstorun=[]
            if options.runinitscripts is True:
                scriptstorun+=prependScripts
            
            if endOfProgramFilePath in '':
                scriptstorun.append(configFilePath)
            else:
                scriptstorun.append(configFilePath+' '+endOfProgramFilePath)
            
            
            T32dictionary = T32WindowLauncher.QuickLaunchEUD(options.t32window, scriptstorun, deviceid=options.deviceid,pausetime=10, apiport=options.apiport,intercomport=options.intercomport)
            if T32dictionary:
                t32open = True
                
        elif options.instruction:
            '''
               if input is not a script, open T32 and pass in instruction with eopFilepath as argument
            '''
            scriptstorun=[]
            if options.runinitscripts is True:
                scriptstorun+=prependScripts
            
            if endOfProgramFilePath in '':
                scriptstorun.append(configFilePath)
            else:
                scriptstorun.append(configFilePath+' '+endOfProgramFilePath)
            
            T32dictionary = T32WindowLauncher.QuickLaunchEUD(options.t32window, scriptstorun, isSingleInstruction=True, pausetime=10, deviceid=options.deviceid,apiport=options.apiport,intercomport=options.intercomport)
            if T32dictionary:
                t32open = True
                
        
        """ 
            poll for maxtime or for eopfile
        """
        count = 0
        while(count < options.maxTime and not os.path.isfile(os.path.abspath(endOfProgramFilePath))):
            wait_second()
            count = count+1

        if os.path.isfile(os.path.abspath(endOfProgramFilePath)):
            logSuccess("EOP reached")
        elif count == options.maxTime:
            logSuccess("Time elapsed")
    
        
    finally:
        """
            try to close t32 if it was opened, delete the temp script if input was .cfg file and make copy of eop and save to ouputdir
        """
        wait_second()
        if t32open:
            if str(options.closet32) in 'True':
                wait_second(5)
                T32WindowLauncher.CloseT32WindowByAPIDict(T32dictionary)
                wait_second(2)
        
        if options.config and options.config.endswith('.cfg'):
            deleteTempScript()
        
        if options.eopFilepath and t32open:
            try:
                from shutil import copyfile
                copyfile(os.path.abspath(endOfProgramFilePath), os.path.abspath(os.path.join(options.outputdir,'peekpokeoutput.txt')))
                logSuccess("Output saved to '"+os.path.join(options.outputdir, "peekpokeoutput.txt"))
                wait_second(2)
            except IOError as io:
                kill("Cannot copy eop to output directory. Ensure file has been closed in T32")
            
    
    return

def deleteTempScript():
    '''
        make a copy of tempscript to ouputdir and delete temp
    '''
    if tempCMMscript:
        print tempscriptCopy
        from shutil import copyfile
        copyfile(tempCMMscript.name, os.path.join(WorkingDir,options.outputdir,tempscriptCopyName))
        wait_second(2)
        os.remove(tempCMMscript.name)
        logSuccess("Temp script deleted. Copy saved to '"+tempscriptCopyName+"'")

def test(module='parser'):
    global testMode
    testMode = True
    
    class ParserTest(unittest.TestCase):
        def setUp(self):
            self.parser = createParser()

        def test_empty_input(self):  #expected: system exit w/ failure
            input = ''
            with self.assertRaises(SystemExit) as se:              
                parsed = parseOptions(self.parser, input.split())
            self.assertNotEqual(se.exception.code, 0)
        
        def test_input_with_only_c(self):  #expected: system exit w/ failure
            input = '-c sampleconfig.cmm'
            with self.assertRaises(SystemExit) as se:              
                parsed = parseOptions(self.parser, input.split())
            self.assertNotEqual(se.exception.code, 0)
            
        def test_input_with_only_o(self):  #expected: system exit w/ failure
            input = r'-o C:\temp'
            with self.assertRaises(SystemExit) as se:              
                parsed = parseOptions(self.parser, input.split())
            self.assertNotEqual(se.exception.code, 0)
            
        def test_input_with_only_t(self): #expected: system exit w/ failure
            input = '-t 60'
            with self.assertRaises(SystemExit) as se:              
                parsed = parseOptions(self.parser, input.split())
            self.assertNotEqual(se.exception.code, 0)
            
        def test_input_with_only_e(self):  #expected: system exit w/ failure
            input = r'-e C:\temp'
            with self.assertRaises(SystemExit) as se:              
                parsed = parseOptions(self.parser, input.split())
            self.assertNotEqual(se.exception.code, 0)
            
        def test_input_with_only_w(self):  #expected: system exit w/ failure
            input = r'-w APPS0'
            with self.assertRaises(SystemExit) as se:              
                parsed = parseOptions(self.parser, input.split())
            self.assertNotEqual(se.exception.code, 0)
            
        def test_input_with_only_h(self):  #expected: prints help summary and exits with 0
            input = r'-h'
            with self.assertRaises(SystemExit) as se:              
                parsed = parseOptions(self.parser, input.split())
            self.assertEqual(se.exception.code, 0)
        
        def test_input_with_correct_c_and_o(self):  #expected: PASS
            input = '-c sampleconfig.cmm -o .'
            parsed = parseOptions(self.parser, input.split())
            self.assertEqual(parsed.config,    'sampleconfig.cmm')
            self.assertEqual(parsed.outputdir, WorkingDir)
            self.assertEqual(parsed.maxTime,   defaultMaxTime)
            self.assertEqual(parsed.eopFilepath, None)
            self.assertEqual(parsed.t32window, defaultT32window)
            
        def test_input_with_noncmm_file(self):  #expected: system exit w/ failure
            input = '-c sampleconfig.txt -o .'
            with self.assertRaises(SystemExit) as se:              
                parsed = parseOptions(self.parser, input.split())
            self.assertNotEqual(se.exception.code, 0)
            
        def test_input_with_all_inputs_correct(self):  #expected: PASS
            input = r'-c sampleconfig.cmm -o C:\temp -t 60 -e C:\temp\eop.txt -w APPS1'
            parsed = parseOptions(self.parser, input.split())
            self.assertEqual(parsed.config,    'sampleconfig.cmm')
            self.assertEqual(parsed.outputdir, r'C:\temp')
            self.assertEqual(parsed.maxTime,   60)
            self.assertEqual(parsed.eopFilepath, r'C:\temp\eop.txt')
            self.assertEqual(parsed.t32window, 'APPS1')
            
    class T32test(unittest.TestCase):
        def setUp(self):
            self.parser = createParser()
            self.testscript = makeNewScript('testscript.cmm')
            writeLine(self.testscript, 'PRINT "peekpoke test: opened t32 successfully"')
            self.testscript.close()

        def test_t32(self):  #expected: system exit normally
            input = '-c testscript.cmm -o . -t 15'
            parsed = parseOptions(self.parser, input.split())
            main(parsed)

        def tearDown(self):
            wait_second(2)
            self.testscript.close()
            os.remove(self.testscript.name)
            

    if module.lower().strip() in 'all': #test all
        suite = unittest.TestLoader().loadTestsFromTestCase(ParserTest)
        unittest.TextTestRunner(verbosity=2).run(suite)
        suite = unittest.TestLoader().loadTestsFromTestCase(T32test)
        unittest.TextTestRunner(verbosity=2).run(suite)
    
    elif module.lower().strip() in 'parser': #test only argument parser
        suite = unittest.TestLoader().loadTestsFromTestCase(ParserTest)
        unittest.TextTestRunner(verbosity=2).run(suite)
    
    elif module.lower().strip() in 't32': #test only t32 startup
        suite = unittest.TestLoader().loadTestsFromTestCase(T32test)
        unittest.TextTestRunner(verbosity=2).run(suite)
    
    return

def main(options):
    """
        - adds prepend script
        - removes eopFilepath if it exists
        - starts t32 and passed in options
    """
    
    #remove eop file
    if os.path.isfile(endOfProgramFilePath):
        os.remove(endOfProgramFilePath)
    
    #try:
    startT32(options)
    #except Exception as e:
    #    kill(e)

if __name__ == '__main__':
    parser = createParser()
    options = parseOptions(parser, sys.argv[1:])
    
    main(options)