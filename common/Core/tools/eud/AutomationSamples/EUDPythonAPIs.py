﻿#!/user/bin/env python
'''
    @file EUDPythonAPIs.py
    @brief Single file with various EUD API's accessible via Python
    @author JBILLING
    @bug Reset, Charger, and attach functionalites only avaialable on full target.
    Prerequisites:
            Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
            EUD.dll must be in same directory or in ../EUD/build/x64/Debug
  
    Available API's:
        EUDGetErrorString
        EUDMSMReset
        EUDSpoofAttach
        EUDSpoofDetach
        EUDEnableCharger
        EUDDisableCharger
        EUDMSMAssertReset
        EUDMSMDeassertReset
        EUDGetCTLStatus
        EUDGetCTLStatusString
        EUDInitTRCDevice
        EUDResetTRCDevice
        EUDInitJTGDevice
        EUDInitSWDDevice
        EUDGetDeviceID
        
'''
######################################################################################
##
##        @file EUDPythonAPIs.py
##        @brief EUD API's accessible via Python
##        @author JBILLING
##        Prerequisites:
##                Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.
##                EUD.dll must be in same directory or in ../EUD/build/x64/Debug
##      
##        Available API's:
##            EUDGetErrorString
##            EUDMSMReset
##            EUDSpoofAttach
##            EUDSpoofDetach
##            EUDEnableCharger
##            EUDDisableCharger
##            EUDMSMAssertReset
##            EUDMSMDeassertReset
##            EUDGetCTLStatus
##            EUDGetCTLStatusString
##            EUDGetDeviceID
##            EUDInitTRCDevice
##            EUDResetTRCDevice
##            EUDInitJTGDevice
##            EUDInitSWDDevice
##            EUDGetDeviceID
##
##                             EDIT HISTORY FOR FILE
##        This section contains comments describing changes made to the module.
##           when       who             what, where, why
##           --------   ---             ---------------------------------------------------------
##           10/19/2017 JBILLING        Multi device support added. All functionality moved to this file, 
##                                      other scripts made to just wrap these subroutines.
##           07/20/2017 JBILLING        EUDGetDeviceID added
##           01/11/2017 JBILLING        Changed Initialization API names
##           10/01/2016 JBILLING        Created
##
######################################################################################
from ctypes import *
import os, sys,ctypes
from optparse import OptionParser
import time
from platform import system
global DEBUGFLAG
DEBUGFLAG=False
global FPGAFLAG
FPGAFLAG=False
global MAXERRSTRINGSIZE
MAXERRSTRINGSIZE=200
global MAXDEVICEARRAYSIZE
MAXDEVICEARRAYSIZE=100
global MAXDEVICESTRINGBUFFERSIZE
MAXDEVICESTRINGBUFFERSIZE=2048
global SUCCESS
SUCCESS=0
global FAILURE
FAILURE=-1
global MAX_ERROR_STRING_SIZE
MAX_ERROR_STRING_SIZE=300
currdir=os.path.dirname(os.path.abspath(__file__))
os_platform = system()


if os_platform == 'Windows':
    EUDDefaultName="EUD.dll"
elif os_platform == 'Linux':
    EUDDefaultName="libeud.so"

EUD_64_Debug_FPGA_Path       = os.path.join(currdir,"..","bin","x64","FPGA_Debug")
EUD_64_Debug_FPGA_Name       = EUDDefaultName          
                                                       
EUD_64_Debug_Silicon_Path    = os.path.join(currdir,"..","bin","x64","Debug")
EUD_64_Debug_Silicon_Name    = EUDDefaultName          
                                                       
EUD_64_Release_FPGA_Path  = os.path.join(currdir,"..","bin","x64","FPGA_Release")
EUD_64_Release_FPGA_Name  = EUDDefaultName        

EUD_64_Release_Silicon_Path  = os.path.join(currdir,"..","bin","x64","Release")
EUD_64_Release_Silicon_Name  = EUDDefaultName           
                                                        
EUD_32_Debug_FPGA_Path       = os.path.join(currdir,"..","bin","Win32","FPGA_Debug")
EUD_32_Debug_FPGA_Name       = EUDDefaultName           
                                                        
EUD_32_Debug_Silicon_Path    = os.path.join(currdir,"..","bin","Win32","Debug")
EUD_32_Debug_Silicon_Name    = EUDDefaultName           
                                                        
EUD_32_Release_Silicon_Path  = os.path.join(currdir,"..","bin","Win32","Release")
EUD_32_Release_Silicon_Name  = EUDDefaultName

EUD_32_Release_FPGA_Path       = os.path.join(currdir,"..","bin","Win32","FPGA_Release")
EUD_32_Release_FPGA_Name       = EUDDefaultName           

def SetupEnvironment():
    '''
        SetupEnvironment: Internal utility to set up Python environment for EUD API's
    '''
    ##====---------------------------------------------------====##
    ##====                Global Variables                   ====##
    ##====---------------------------------------------------====##

    global EUDDirectories
    EUDDirectories=[]
    currdir=os.path.dirname(os.path.abspath(__file__))
    EUDDirectories.append(os.path.join(currdir,"..","EUD","build","x64","debug","EUD.dll"))
    EUDDirectories.append(os.path.join(currdir,"..","EUD","eud.dll"))
    EUDDirectories.append(os.path.join(currdir,"EUD.dll"))
    EUDDirectories.append(os.path.join(currdir,"..","EUD","build","x64","debug","EUD.dll"))
    EUDDirectories.append(os.path.join(currdir,"..","..","EUD","build","x64","release","EUD.dll"))
    EUDDirectories.append(os.path.join(currdir,"..","EUD","build","x64","release","EUD.dll"))

    global SUCCESS, FAILURE
    SUCCESS=0
    FAILURE=-1
    
    EnvSetup=True
    
    return
    
def kill(message=None):
    if not message is None:
        print message
    sys.exit(1)
    
##====--------------------------------------------------====##
##====                  Python API's                    ====##
##====--------------------------------------------------====##
def LoadEUDDLL(debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: LoadEUDDLL
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Loads EUD.dll using ctypes. There are several EUD.dll 
        ##      options to choose from. Depending on parameters given,
        ##      one of the following are chosen:
        ##          ../bin/Win32/Release/EUD.dll       - 32bit, for silicon devices, optimized
        ##          ../bin/Win32/Debug/EUD.dll         - 32bit, for silicon devices, built for debug
        ##          ../bin/Win32/FPGA_Release/EUD.dll  - 32bit, for FPGA devices, optimized
        ##          ../bin/Win32/FPGA_Debug/EUD.dll    - 32bit, FPGA devices, built for debug
        ##
        ##          ../bin/x64/Release/EUD.dll         - 64bit, for silicon devices, optimized
        ##          ../bin/x64/Debug/EUD.dll           - 64bit, for silicon devices, built for debug
        ##          ../bin/x64/FPGA_Release/EUD.dll    - 64bit, for FPGA devices, optimized
        ##          ../bin/x64/FPGA_Debug/EUD.dll      - 64bit, for FPGA devices, built for debug
        ##          
        ##  Arguments:
        ##      debugflag=True/False    
        ##          Optional flag, used to choose debug EUD DLL or not
        ##          Default value is chosen if not specified
        ##      FPGAFlag=True/False
        ##          Optional flag, used to choose FPGA DLL or not
        ##          Default value is chosen if not specified
        ##      
        ##  Returns:
        ##      Returns pointer to instance of EUD object if successful. None if unsuccessful.
        ##
        #################################################################
    '''
    os_platform = system()
    
    is_64bitflag = sys.maxsize > 2**32
    if is_64bitflag is True:
        if debugflag is True:
            if FPGAFlag is True:
                eud_specific_path= EUD_64_Debug_FPGA_Path
                eud_name = EUD_64_Debug_FPGA_Name
            else:
                eud_specific_path= EUD_64_Debug_Silicon_Path
                eud_name = EUD_64_Debug_Silicon_Name
        else:
            if FPGAFlag is True:
                eud_specific_path= EUD_64_Release_FPGA_Path
                eud_name = EUD_64_Release_FPGA_Name
            else:
                eud_specific_path= EUD_64_Release_Silicon_Path
                eud_name = EUD_64_Release_Silicon_Name
    else: 
        if debugflag is True:
            if FPGAFlag is True:
                eud_specific_path= EUD_32_Debug_FPGA_Path
                eud_name = EUD_32_Debug_FPGA_Name
            else:
                eud_specific_path= EUD_32_Debug_Silicon_Path
                eud_name = EUD_32_Debug_Silicon_Name
        else:
            if FPGAFlag is True:
                eud_specific_path= EUD_32_Release_FPGA_Path
                eud_name = EUD_32_Release_FPGA_Name
            else:
                eud_specific_path= EUD_32_Release_Silicon_Path
                eud_name = EUD_32_Release_Silicon_Name
            
    if is_64bitflag is True:
        if os.path.isfile(os.path.join(eud_specific_path,eud_name)):
            if os_platform == 'Windows':
                eud_instance=ctypes.windll.LoadLibrary(os.path.join(eud_specific_path,eud_name))
            elif os_platform == 'Linux':
                eud_instance=ctypes.CDLL(os.path.join(eud_specific_path,eud_name))
        else:
            print "Error! Could not find path to eud library"
            eud_instance = None
    else:
        if os.path.isfile(os.path.join(eud_specific_path,eud_name)):
            eud_instance=CDLL(os.path.join(eud_specific_path,eud_name))
        else:
            print "Error! Could not find path to eud library"
            eud_instance = None

    return eud_instance
    
def SelectEUDDevice(EUD_dll=None,firstavailable=False,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: Selected
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Gets available devices and queries user if needed for selection.
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      SUCCESS or FAILURE depending on result.
        ##
        #################################################################
    '''
    if EUD_dll is None:
        EUD_dll = LoadEUDDLL()
        if EUD_dll is None:
            return FAILURE
    availabledevices = GetListofDevices(EUD_dll)
    if availabledevices is None:
        print "No available EUD Devices present!"
        return None
        
    availabledevices_hexstr=[]
    for device in availabledevices:
        availabledevices_hexstr.append(hex(device)[:-1].upper())
            
    if len(availabledevices_hexstr) is 0:
        print "No available EUD Devices present!"
        return None
    elif len(availabledevices_hexstr) is 1:
        print "Available device: "+str(availabledevices_hexstr)
        
        return availabledevices[0]
        
    elif len(availabledevices_hexstr) > 1:
        
        if firstavailable is True:
            return availabledevices[0]
            
        
        DisplayAvailableDevices(EUD_dll)
        
        selecteddevice = raw_input("Enter the ID from above selection for the device to test on:")
        selecteddevice = selecteddevice.upper()
        
        if not '0X' in selecteddevice:
            selecteddevice = '0X' + selecteddevice
            
        if not selecteddevice in  availabledevices_hexstr:
            selecteddevice = raw_input("Selected device not found. Try again:")
            selecteddevice = selecteddevice.upper()
            if not '0X' in selecteddevice:
                selecteddevice = '0X' + selecteddevice
        
        if not selecteddevice in  availabledevices_hexstr:
            kill("Error! Selected device ("+str(selecteddevice)+") not found in availabledevices: "+str(availabledevices_hexstr))
        
        idx = availabledevices_hexstr.index(selecteddevice)
        
        return availabledevices[idx]
    
def DisplayAvailableDevices(EUD_dll=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: DisplayAvailableDevices
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Retrieves list of EUD devices attached to host PC and 
        ##      returns a python list of strings
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      Python list of strings object of device ID hex values
        ##      
        #################################################################
    '''
    
    if EUD_dll is None:
        EUD_dll = LoadEUDDLL()
        if EUD_dll is None:
            return FAILURE
    
    stringsize=c_int(0)
    charbuffer=' '*MAXDEVICESTRINGBUFFERSIZE
    
    #Call the DLL
    result = EUD_dll.GetAttachedDevicesString(c_char_p(charbuffer),pointer(stringsize))
    
    if result == SUCCESS:
        print "EUD Device List:"
        print str(charbuffer[:stringsize.value])
    else:
        print "Error! Received error message during call to GetAttachedDevicesString:"
        print EUDGetErrorString(result,EUD_dll=EUD_dll)
    
    warningcode = EUD_dll.EUDGetLastError()
    
    if warningcode != SUCCESS:
        import EUDDefines
        warningcode = hex(warningcode + 2**32)
        ctl_enum_warning_code = hex(EUDDefines.EUDErrDefines['EUD_ERR_CTL_ENUMERATION_FAILED'])
        if ctl_enum_warning_code in warningcode:
            print "Warning! Some EUD devices were detected but were non responsive. "
            print "Recommend disabling/enabling them or unplug/replug the device(s)."
        
    return SUCCESS
    
def GetListofDevicesString(EUD_dll=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: GetListofDevicesString
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Retrieves list of EUD devices attached to host PC and 
        ##      returns a python list of strings
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      Python list of strings object of device ID hex values
        ##      
        #################################################################
    '''
    
    if EUD_dll is None:
        EUD_dll = LoadEUDDLL()
        if EUD_dll is None:
            return FAILURE
    
    devicelist = GetListofDevices(EUD_dll)
    stringlist = []
    
    for deviceid in devicelist:
        stringlist.append(hex(deviceid)[:-1]) ##get rid of 'L' at end of hex value
        
    return stringlist
    
def GetListofDevices(EUD_dll=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: GetListofDevices
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Retrieves list of EUD devices attached to host PC and 
        ##      returns a python list.
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      Python list object of device ID hex values
        ##      
        #################################################################
    '''
    if EUD_dll is None:
        EUD_dll = LoadEUDDLL()
        if EUD_dll is None:
            return FAILURE
    
    EUD_dll.GetDeviceIDArray.argtypes = [POINTER(c_uint32),POINTER(c_uint32)]
    EUD_dll.GetDeviceIDArray.restype = c_uint32
    
    devicearray = (c_uint32*MAXDEVICEARRAYSIZE)()
    length_p    = (c_uint32*1)()
    
    result = EUD_dll.GetDeviceIDArray(devicearray, length_p)
    if result != SUCCESS:
        print EUDGetErrorString(result,EUD_dll=EUD_dll)
        return None
    
    deviceIDlist=[]
    for i in range(length_p[0]):
        deviceIDlist.append(devicearray[i])
    
    return deviceIDlist
    
def EUDGetVersion(EUD_dll=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: EUDGetVersion
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Retrieves EUD Version ID (Major, Minor, Spin)
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      Python string of version: Major_Minor_Spin (e.g. 1_14_3)
        ##      
        #################################################################
    '''
    if EUD_dll is None:
        EUD_dll = LoadEUDDLL()
        if EUD_dll is None:
            return FAILURE
    
    major_rev=c_int(0)
    minor_rev=c_int(0)
    spin_rev =c_int(0)
    
    try:
        result = EUD_dll.EUDGetVersion(pointer(major_rev),pointer(minor_rev),pointer(spin_rev))
    except:
        print "EUD Version API not available. EUD is version 1_13_1 or older"
        return "1_13_1"
        
    if result != SUCCESS:
        print EUDGetErrorString(result,EUD_dll=EUD_dll)
        return None
    
    return str(major_rev.value)+"_"+str(minor_rev.value)+"_"+str(spin_rev.value)
    
def EUDGetErrorString(errorcode,EUD_dll=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: EUDGetErrorString
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Retrieves error string from given error code.
        ##
        ##  Arguments:
        ##      errorcode - 
        ##          numeric, required
        ##          EUD error code retrieved by some EUD API
        ##      EUD_dll=<dllobject>
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      Returns an ASCII string description of given EUD error code.
        ##
        #################################################################
    '''
    SetupEnvironment()
    
    SetupEnvironment()
    if EUD_dll is None:
        EUD_dll = LoadEUDDLL()
        if EUD_dll is None:
            return kill("Error! Could not load EUD DLL")
    
    #if deviceID is None:
    #    deviceID = SelectEUDDevice(EUD_dll)
    #    if deviceID is None:
    #        kill("Error! No EUD Devices detected")
            
    ##EUDGetErrorString returns a pointer to string array
    ##Takes a pointer to an int which is the size that we'll use to read the array
    stringsize=c_int(0)
    newerrval=c_int(0)
    charbuffer=' '*MAX_ERROR_STRING_SIZE
    
    #Call the DLL
    newerrval=EUD_dll.EUDGetErrorString(errorcode,c_char_p(charbuffer),pointer(stringsize))
    #Check returned arguments
    if stringsize.value > MAXERRSTRINGSIZE:
        return "EUDGetErrorString Error: EUDGetErrorString returned bad stringsize parameter"

    #If we got an error, then try and find out why.
    if newerrval != 0:
        new_newerrval=EUD_dll.EUDGetErrorString(newerrval,c_char_p(charbuffer),pointer(stringsize))
        #If got an error again, give up
        if new_newerrval != 0:
            return "EUDGetErrorString Error: EUDGetErrorString returned error but could not get errstring. Errcode: "+hex(new_newerrval)
        else:
            return "EUDGetErrorString Error: EUDGetErrorString returned error: "+charbuffer[:stringsize]

    return charbuffer
    
def EUDMSMReset(EUD_dll=None,deviceID=None,delay_ms_value=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: EUDMSMReset
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Initiates reset signal to msm Reset Ctlr (eud_rctlr_srst_n).
        ##      The Reset Ctlr does not reset EUD when EUD initiates a 
        ##      system reset (USB connection to EUD won't be lost across 
        ##      MSM reset).
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object, optional.
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      delay_ms_value=<numeric delay in milliseconds>- 
        ##          numeric value, optional
        ##          Delay value in milliseconds between assert and deassert
        ##          of reset. Passed directly to EUD DLL.
        ##      
        ##  Returns:
        ##      Numeric Error code. 0 for success.
        ##
        #################################################################
    '''
    SetupEnvironment()
    if EUD_dll is None:
        EUD_dll = LoadEUDDLL()
        if EUD_dll is None:
            return kill("Error! Could not load EUD DLL")
    
    if deviceID is None:
        deviceID = SelectEUDDevice(EUD_dll)
        if deviceID is None:
            kill("Error! No EUD Devices detected")
    elif type(deviceID) is str:
        deviceID=long(deviceID,16)
    
    
    #Function contents
    #Default Delay is 1 second.
    default_delay_ms_value = 1000
    
    if delay_ms_value is None:
        delayvalue = default_delay_ms_value
    else:
        delayvalue = delay_ms_value

    errdata = EUD_dll.EUDMSMReset(deviceID,delayvalue)
    if errdata != SUCCESS:
        print "EUDMSMReset(): Failure. Error: "+EUDGetErrorString(errdata,EUD_dll=EUD_dll)
        return errdata

    return SUCCESS

def EUDSpoofAttach(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: EUDSpoofAttach
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Calls EUDSpoofAttach, which updates vbus_attach 
        ##      and sends a vbus_int for chip software to respond 
        ##      and connect USB to EUD/Host PC.
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object, optional.
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      Numeric Error code. 0 for success.
        ##
        #################################################################
    '''
    SetupEnvironment()
    if EUD_dll is None:
        EUD_dll = LoadEUDDLL()
        if EUD_dll is None:
            return kill("Error! Could not load EUD DLL")
    
    if deviceID is None:
        deviceID = SelectEUDDevice(EUD_dll)
        if deviceID is None:
            kill("Error! No EUD Devices detected")
    elif type(deviceID) is str:
        deviceID=long(deviceID,16)
            
    
    errdata = EUD_dll.EUDSpoofAttach(deviceID)
    if errdata != SUCCESS:
        print "EUDSpoofAttach(): Failure. Error: "+EUDGetErrorString(errdata,EUD_dll=EUD_dll)
        return errdata

    return SUCCESS

def EUDSpoofDetach(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: EUDSpoofDetach
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Calls EUDSpoofAttach, which updates vbus_attach 
        ##      and sends a vbus_int for chip software to respond 
        ##      and disconnect USB to EUD/Host PC.
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object, optional.
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      Numeric Error code. 0 for success.
        ##
        #################################################################
    '''
    SetupEnvironment()
    if EUD_dll is None:
        EUD_dll = LoadEUDDLL()
        if EUD_dll is None:
            return kill("Error! Could not load EUD DLL")
    
    if deviceID is None:
        deviceID = SelectEUDDevice(EUD_dll)
        if deviceID is None:
            kill("Error! No EUD Devices detected")
    elif type(deviceID) is str:
        deviceID=long(deviceID,16)
            
    
    errdata = EUD_dll.EUDSpoofDetach(deviceID)
    if errdata != SUCCESS:
        print "EUDSpoofDetach(): Failure. Error: "+EUDGetErrorString(errdata,EUD_dll=EUD_dll)
        return errdata

    return SUCCESS

def EUDEnableCharger(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: EUDEnableCharger
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Calls EUDDisableCharger, which updates chgr_en and sends 
        ##      a chgr_int for chip software to respond to enable 
        ##      charge update.
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object, optional.
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      Numeric Error code. 0 for success.
        ##
        #################################################################
    '''
    SetupEnvironment()
    if EUD_dll is None:
        EUD_dll = LoadEUDDLL()
        if EUD_dll is None:
            return kill("Error! Could not load EUD DLL")
    
    if deviceID is None:
        deviceID = SelectEUDDevice(EUD_dll)
        if deviceID is None:
            kill("Error! No EUD Devices detected")
    elif type(deviceID) is str:
        deviceID=long(deviceID,16)
            
    
    errdata = EUD_dll.EUDEnableCharger(deviceID)
    if errdata != SUCCESS:
        print "EUDEnableCharger(): Failure. Error: "+EUDGetErrorString(errdata,EUD_dll=EUD_dll)
        return errdata

    return SUCCESS

def EUDDisableCharger(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: EUDDisableCharger
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Calls EUDDisableCharger, which updates chgr_en and sends 
        ##      a chgr_int for chip software to respond to disable 
        ##      charge update.
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object, optional.
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      Numeric Error code. 0 for success.
        ##
        #################################################################
    '''
    SetupEnvironment()
    if EUD_dll is None:
        EUD_dll = LoadEUDDLL()
        if EUD_dll is None:
            return kill("Error! Could not load EUD DLL")
    
    if deviceID is None:
        deviceID = SelectEUDDevice(EUD_dll)
        if deviceID is None:
            kill("Error! No EUD Devices detected")
    elif type(deviceID) is str:
        deviceID=long(deviceID,16)
    
    errdata = EUD_dll.EUDDisableCharger(deviceID)
    if errdata != SUCCESS:
        print "EUDDisableCharger(): Failure. Error: "+EUDGetErrorString(errdata,EUD_dll=EUD_dll)
        return errdata

    return SUCCESS

def EUDMSMAssertReset(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: EUDMSMAssertReset
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Asserts eud_rctlr_srst_n bit, to hold MSM in reset until 
        ##      EUDMSMDeassertReset is called.
        ##      The Reset Ctlr does not reset EUD when EUD initiates a 
        ##      system reset (USB connection to EUD won't be lost 
        ##      across MSM reset).
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object, optional.
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      Numeric Error code. 0 for success.
        ##
        #################################################################
    '''
    SetupEnvironment()
    if EUD_dll is None:
        EUD_dll = LoadEUDDLL()
        if EUD_dll is None:
            return kill("Error! Could not load EUD DLL")
    
    if deviceID is None:
        deviceID = SelectEUDDevice(EUD_dll)
        if deviceID is None:
            kill("Error! No EUD Devices detected")
    elif type(deviceID) is str:
        deviceID=long(deviceID,16)
            
    
    #Now swap out global for local
    EUD_DLL_global=EUD_dll
    
    errdata = EUD_dll.EUDMSMAssertReset(deviceID)
    if errdata != SUCCESS:
        print "EUDMSMAssertReset(): Failure. Error: "+EUDGetErrorString(errdata,EUD_dll=EUD_dll)
        return errdata

    return SUCCESS

def EUDMSMDeassertReset(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: EUDMSMDeassertReset
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Deasserts eud_rctlr_srst_n bit, to allow MSM to come 
        ##      out of reset, assuming EUDMSMAssertReset was called prior.
        ##      The Reset Ctlr does not reset EUD when EUD initiates 
        ##      a system reset (USB connection to EUD won't be lost 
        ##      across MSM reset).
        ##  
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object, optional.
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      Numeric Error code. 0 for success.
        ##
        #################################################################
    '''
    SetupEnvironment()
    if EUD_dll is None:
        EUD_dll = LoadEUDDLL()
        if EUD_dll is None:
            return kill("Error! Could not load EUD DLL")
    
    if deviceID is None:
        deviceID = SelectEUDDevice(EUD_dll)
        if deviceID is None:
            kill("Error! No EUD Devices detected")
    elif type(deviceID) is str:
        deviceID=long(deviceID,16)
            
    

    errdata = EUD_dll.EUDMSMDeassertReset(deviceID)
    if errdata != SUCCESS:
        print "EUDMSMDeassertReset(): Failure. Error: "+EUDGetErrorString(errdata,EUD_dll=EUD_dll)
        return errdata

    return SUCCESS

def EUDGetCTLStatus(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: EUDGetCTLStatus
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Gets CTL status register from EUD API. Returns numeric 
        ##      value of EUD CTL status register.
        ##  
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object, optional.
        ##          DLL object to be used to imported by LoadLibrary.
        ##          If not given, will attempt to load DLL from 
        ##          default paths.
        ##      
        ##  Returns:
        ##      Numeric representation of CTL status register,
        ##      Least significant bit from hardware being bit 0 
        ##      of returned value (little endian).
        ##      See EUD Documentation for more information.
        ##
        #################################################################
    '''
    SetupEnvironment()
    if EUD_dll is None:
        EUD_dll = LoadEUDDLL()
        if EUD_dll is None:
            return kill("Error! Could not load EUD DLL")
    
    if deviceID is None:
        deviceID = SelectEUDDevice(EUD_dll)
        if deviceID is None:
            kill("Error! No EUD Devices detected")
    elif type(deviceID) is str:
        deviceID=long(deviceID,16)
            
    errdata=c_int(0)
    ctldata=c_int(0)

    errdata = EUD_dll.EUDGetCTLStatus(deviceID,pointer(ctldata))
    if errdata != SUCCESS:
        print "EUDGetCTLStatus(): Failure. Error: "+EUDGetErrorString(errdata,EUD_dll=EUD_dll)
        return errdata

    return ctldata.value

def EUDGetCTLStatusString(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: EUDGetCTLStatusString
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Gets CTL status register from EUD API. Returns string 
        ##      of EUD CTL status register state.
        ##  
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object, optional.
        ##          DLL object to be used to imported by LoadLibrary.
        ##          If not given, will attempt to load DLL from 
        ##          default paths.
        ##      
        ##  Returns:
        ##      String representation of CTL status register state.
        ##
        #################################################################
    '''
    SetupEnvironment()
    if EUD_dll is None:
        EUD_dll = LoadEUDDLL()
        if EUD_dll is None:
            return kill("Error! Could not load EUD DLL")
    
    if deviceID is None:
        deviceID = SelectEUDDevice(EUD_dll)
        if deviceID is None:
            kill("Error! No EUD Devices detected")
    elif type(deviceID) is str:
        deviceID=long(deviceID,16)
            
            
    stringsize=c_int(0)
    charbuffer=' '*1000
    
    errdata = EUD_dll.EUDGetCTLStatusString(deviceID,c_char_p(charbuffer),pointer(stringsize))
    
    if errdata != SUCCESS:
        print "GetCTLStatusString(): Failure. Error: "+EUDGetErrorString(errdata,EUD_dll=EUD_dll)
        return FAILURE
    if stringsize.value > 1000:
        print "GetCTLStatusString(): Failure - stringsize returned too high. Value: "+str(stringsize.value)
        return FAILURE
        
    return charbuffer[:stringsize.value]
    
def EUDInitTRCDevice(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: EUDInitTRCDevice
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Initializes EUD Trace Peripheral
        ##      This includes the following:
        ##          Enable TRC peripheral
        ##          Set SWD Frequency to default frequency
        ##          Set SWD Delay to default delay
        ##          Get JTAG_ID from target.
        ##              This converts JTAG interface to SWD mode and 
        ##              Performs several SWD transactions to retrieve JTAGID
        ##              From hardware.
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      Numeric Error code. 0 for success.
        ##
        #################################################################
    '''
    SetupEnvironment()
    if EUD_dll is None:
        EUD_dll = LoadEUDDLL()
        if EUD_dll is None:
            return kill("Error! Could not load EUD DLL")
    
    if deviceID is None:
        deviceID = SelectEUDDevice(EUD_dll)
        if deviceID is None:
            kill("Error! No EUD Devices detected")
    elif type(deviceID) is str:
        deviceID=long(deviceID,16)
            
        
    ##EUDGetErrorString returns a pointer to string array
    ##Takes a pointer to an int which is the size that we'll use to read the array
    
    errorcode=c_int(0)
    options=c_int(0)
    trchandler=c_void_p()
    import EUDDefines
    #Call the DLL
    trchandler=EUD_dll.EUDInitializeDeviceTRC(deviceID,options,pointer(errorcode))
    if trchandler is 0:
        print "EUDInitTRCDevice(): Failure. Error: "+EUDGetErrorString(EUD_dll.EUDGetLastError(deviceID),EUD_dll=EUD_dll)
        return None
    
    
    return trchandler


def EUDResetTRCDevice(TRC_EUD_Device,EUD_dll=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: EUDResetTRCDevice
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Resets EUD Trace Peripheral
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      TRC_EUD_Device
        ##          Pointer to initialized trace EUD handler
        ##
        ##  Returns:
        ##      Numeric Error code. 0 for success.
        ##
        #################################################################
    '''
    SetupEnvironment()
    if EUD_dll is None:
        EUD_dll = LoadEUDDLL()
        if EUD_dll is None:
            return kill("Error! Could not load EUD DLL")
    
    #if deviceID is None:
    #    deviceID = SelectEUDDevice(EUD_dll)
    #    if deviceID is None:
    #        kill("Error! No EUD Devices detected")
            
    
    #Call the DLL
    errorcode = EUD_dll.EUD_TraceReset(TRC_EUD_Device)
    
    #Some Failure 
    if errorcode != 0:
        print "EUDInitTRCDevice(): Failure. Error: "+EUDGetErrorString(errorcode,EUD_dll=EUD_dll)
        return errorcode
    
    
    return result


def EUDInitJTGDevice(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: EUDInitJTGDevice
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Initializes EUD Serial Wire Debug peripheral.
        ##      This includes the following:
        ##          Enable SWD peripheral
        ##              Disables JTAG peripheral
        ##              Resets SWD Peripheral
        ##          Set SWD Frequency to default frequency
        ##          Set SWD Delay to default delay
        ##          Get JTAG_ID from target.
        ##              This converts JTAG interface to SWD mode and 
        ##              Performs several SWD transactions to retrieve JTAGID
        ##              From hardware.
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      Numeric Error code. 0 for success.
        ##
        #################################################################
    '''
    SetupEnvironment()
    if EUD_dll is None:
        EUD_dll = LoadEUDDLL()
        if EUD_dll is None:
            return kill("Error! Could not load EUD DLL")
    
    if deviceID is None:
        deviceID = SelectEUDDevice(EUD_dll)
        if deviceID is None:
            kill("Error! No EUD Devices detected")
    elif type(deviceID) is str:
        deviceID=long(deviceID,16)
            
    
        
    ##EUDGetErrorString returns a pointer to string array
    ##Takes a pointer to an int which is the size that we'll use to read the array
    stringsize=c_int(0)
    errorcode=c_int(0)
    options=c_int(0)
    charbuffer=' '*100
    jtghandler=c_void_p()
    import EUDDefines
    #Call the DLL
    
    jtghandler=EUD_dll.EUDInitializeDeviceJTG(deviceID,options,pointer(errorcode))
    if jtghandler is 0:
        print "EUDInitJTGDevice(): Failure. Error: "+EUDGetErrorString(errorcode,EUD_dll=EUD_dll)
        return 0
    

    freq=EUDDefines.EUDDefines['EUD_FREQ_7_5_MHz']
    errorcode = EUD_dll.JTGSetFrequency(jtghandler,freq)
    if not (errorcode is 0):
        print "EUDInitJTGDevice(): Failure. Error: "+EUDGetErrorString(errorcode,EUD_dll=EUD_dll)
        EUD_dll.EUDClosePeripheral(jtghandler)
        return 0
    
    delay=EUDDefines.EUDDefines['JTG_EUD_DELAY_VALUE']
    errorcode = EUD_dll.JTGSetDelay(jtghandler,delay)
    if not (errorcode is 0):
        print "EUDInitJTGDevice(): Failure. Error: "+EUDGetErrorString(errorcode,EUD_dll=EUD_dll)
        EUD_dll.EUDClosePeripheral(jtghandler)
        return 0
    

    jtagID=c_int(0)    
    
    errorcode = EUD_dll.JTAG_GetJTAGID(jtghandler,pointer(jtagID))
    if not (errorcode is 0):
        print "EUDInitJTGDevice(): Failure. Error: "+EUDGetErrorString(errorcode,EUD_dll=EUD_dll)
        EUD_dll.EUDClosePeripheral(jtghandler)
        return 0

    if jtagID.value is 0x0:
        print "EUDInitJTGDevice(): Failure. jtagID is 0x0. Is configuration wrong or device not attached?"
        errorcode = EUD_dll.PERIPH_CLOSE(jtghandler)
        errorcode2 = EUD_dll.EUDClosePeripheral(jtghandler)
        if not (errorcode is 0):
            print "Double error: jtag ID is 0x0, and could not close JTG Peripheral. "
            print "Error1: "+EUDGetErrorString(errorcode,EUD_dll=EUD_dll)
            print "Error2: "+EUDGetErrorString(errorcode2,EUD_dll=EUD_dll)
        return 0
        
    print "JTAGID: "+hex(jtagID.value)
    
    return jtghandler

    
def EUDInitSWDDevice(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: EUDInitSWDDevice
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Initializes EUD Serial Wire Debug peripheral.
        ##      This includes the following:
        ##          Enable SWD peripheral
        ##              Disables JTAG peripheral
        ##              Resets SWD Peripheral
        ##          Set SWD Frequency to default frequency
        ##          Set SWD Delay to default delay
        ##          Get JTAG_ID from target.
        ##              This converts JTAG interface to SWD mode and 
        ##              Performs several SWD transactions to retrieve JTAGID
        ##              From hardware.
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##      
        ##  Returns:
        ##      Numeric Error code. 0 for success.
        ##
        #################################################################
    '''
    SetupEnvironment()
    if EUD_dll is None:
        EUD_dll = LoadEUDDLL()
        if EUD_dll is None:
            return kill("Error! Could not load EUD DLL")
    
    if deviceID is None:
        deviceID = SelectEUDDevice(EUD_dll)
        if deviceID is None:
            kill("Error! No EUD Devices detected")
    elif type(deviceID) is str:
        deviceID=long(deviceID,16)
            
    
        
    ##EUDGetErrorString returns a pointer to string array
    ##Takes a pointer to an int which is the size that we'll use to read the array
    stringsize=c_int(0)
    errorcode=c_int(0)
    options=c_int(1)
    charbuffer=' '*100
    swdhandler=c_void_p()
    import EUDDefines
    #Call the DLL
    
    swdhandler=EUD_dll.EUDInitializeDeviceSWD(deviceID,options,pointer(errorcode))
    if swdhandler is 0:
        print "EUDInitSWDDevice(): Failure. Error: "+EUDGetErrorString(errorcode,EUD_dll=EUD_dll)
        return 0
    
    freq=EUDDefines.EUDDefines['EUD_FREQ_3_75_MHz']
    errorcode = EUD_dll.SWDSetFrequency(swdhandler,freq)
    if not (errorcode is 0):
        print "EUDInitSWDDevice(): Failure. Error: "+EUDGetErrorString(errorcode,EUD_dll=EUD_dll)
        return 0
    
    delay=EUDDefines.EUDDefines['SWD_EUD_DELAY_VALUE']
    errorcode = EUD_dll.SWDSetDelay(swdhandler,delay)
    if not (errorcode is 0):
        print "EUDInitSWDDevice(): Failure. Error: "+EUDGetErrorString(errorcode,EUD_dll=EUD_dll)
        return 0
    
    
    errorcode = EUD_dll.JTAG_to_SWD(swdhandler)
    if not (errorcode is 0):
        if not(errorcode==0x10200005): ##EUD_SWD_ERR_SWD_TO_JTAG_ALREADY_DONE
            print "EUDInitSWDDevice(): Failure. Error: "+EUDGetErrorString(errorcode,EUD_dll=EUD_dll)
            return 0

    jtagID=c_int(0)    
    
    errorcode = EUD_dll.SWD_GetJTAGID(swdhandler,pointer(jtagID))
    if not (errorcode is 0):
        print "EUDInitSWDDevice(): Failure. Error: "+EUDGetErrorString(errorcode,EUD_dll=EUD_dll)
        return 0

    if jtagID.value is 0x0:
        print "EUDInitSWDDevice(): Failure. jtagID is 0x0. Is configuration wrong or device not attached?"
        errorcode2 = EUD_dll.EUDClosePeripheral(swdhandler)
        
        if not (errorcode is 0):
            print "Double error: jtag ID is 0x0, and could not close SWD Peripheral. "
            print "Error1: "+EUDGetErrorString(errorcode,EUD_dll=EUD_dll)
            print "Error2: "+EUDGetErrorString(errorcode2,EUD_dll=EUD_dll)
        
        return 0

    print "JTAGID: "+hex(jtagID.value)
    return swdhandler

def colorprint(string,color):
    '''
    ###################################################################
    ##    colorprint
    ##        Utility API.
    ##        Prints given message string in desired color to windows console
    ##        Available colors: 'red', 'yellow', 'green', 'blue', 0 (default).
    ##    
    ##        
    ##    Arguments:
    ##        string - error string to print
    ##        color - desired color in text. 
    ##            Available colors: 'red', 'yellow', 'green', 'blue', 0 (default).
    ##    Returns:
    ##        None
    ###################################################################

    '''
    if os_platform == 'Windows':
        color_code_black=7
        if str(color) in "red":
            color_code=12
        elif str(color) in "yellow":
            color_code=30
        elif str(color) in "green":
            color_code=26
        elif str(color) in "blue":
            color_code=9
        #default to red
        else:
            color_code=0
            
        #print string
        #return        
        import ctypes
        STD_OUTPUT_HANDLE = -11
        stdout_handle = ctypes.windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
        ctypes.windll.kernel32.SetConsoleTextAttribute(stdout_handle,color_code)
        
        print string
        #restore to black
        #stdout_handle = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
        ctypes.windll.kernel32.SetConsoleTextAttribute(stdout_handle,color_code_black)
        #print " "
    
    elif os_platform == 'Linux':
        color_code_end = '\033[0m'
        
        if str(color) in "red":
            color_code='\33[31m'
        elif str(color) in "yellow":
            color_code='\33[33m'
        elif str(color) in "green":
            color_code='\33[32m'
        elif str(color) in "blue":
            color_code='\33[34m'
        else:
            color_code='\033[0m'
            
        #print the string in the given color and then revert back
        print(color_code + string + color_code_end)
    return

def verboseprint2(errcode,string,EUD_dll,expectedresult=SUCCESS,verbose=1,color=None):
    '''
    ###################################################################
    ##    verboseprint2
    ##    takes a result and returns it, printing output if verbose is set to 1.
    ##    Mainly for test sequences.
    ##    Made to be a more succinct version of verboseprint
    ##
    ##    color can be set, but default is to print red if failure (-1), and green if success(0)
    ##
    ##    Parameters:
    ##        errcode - errcode returned by EUD DLL. 
    ##        result: SUCCESS (0) or FAILURE (-1)
    ##        string: string to print (or not print if verbose == 0)
    ##        
    ##            if verbose is !0 and errcode is !0, prints out err string from EUD DLL
    ##        EUD_dll - EUD DLL
    ##        verbose - 0 == non verbose. 1 == verbose (only failures printed), 2 == more verbose (failures and success printed)
    ##        color= 'red' or 'green'. If not specified, will print 'red' for failure and 'green' for success
    #####################################################################
    
    '''
    SetupEnvironment()
    
    if color is None:
        
        if expectedresult == SUCCESS and errcode == SUCCESS:
            color = 'green'
            string = "SUCCESS: "+str(string)
            rvalue = SUCCESS
        elif expectedresult == FAILURE and errcode != SUCCESS:
            color = 'green'
            string = "SUCCESS: "+str(string)
            rvalue = SUCCESS
        else:
            color = 'red'
            string = "FAILURE: "+str(string)
            rvalue = FAILURE

    if verbose > 0:
        if not (errcode == 0):
            string=str(string+EUDGetErrorString(errcode,EUD_dll=EUD_dll))
        
        if expectedresult is SUCCESS and verbose > 1:
            colorprint(string,color)
        elif expectedresult is FAILURE:
            colorprint(string,color)
        elif expectedresult is SUCCESS and not errcode is SUCCESS:
            colorprint(string,color)
        
    return rvalue
    
def verboseprint(result,string,errcode,EUD_dll,verbose,color=None):
    '''
    ###################################################################
    ##    verboseprint
    ##    takes a result and returns it, printing output if verbose is set to 1.
    ##    Mainly for test sequences.
    ##
    ##    color can be set, but default is to print red if failure (-1), and green if success(0)
    ##
    ##    Parameters:
    ##        result: SUCCESS (0) or FAILURE (-1)
    ##        string: string to print (or not print if verbose == 0)
    ##        errcode - errcode returned by EUD DLL. 
    ##            if verbose is !0 and errcode is !0, prints out err string from EUD DLL
    ##        EUD_dll - EUD DLL
    ##        verbose - 0 == non verbose. 1 == verbose
    ##        color= 'red' or 'green'. If not specified, will print 'red' for failure and 'green' for success
    ###################################################################
    '''
    SetupEnvironment()
    if color is None:
        if result is SUCCESS:
            color = 'green'
        else:
            color = 'red'

    if result is SUCCESS:
        string = "SUCCESS: "+string
    else:
        string = "FAILURE: "+string

    if verbose is 1:
        if not (errcode == 0):
            string=str(string+EUDGetErrorString(errcode,EUD_dll=EUD_dll))
        
        colorprint(string,color)
        return result
    else:
        return result


if __name__ == '__main__':
     
    print "##################################################################"
    print "##                                                              ##"
    print "##                EUD Python APIs                               ##"
    print "##     This file contains various API's to expose EUD           ##"
    print "##     functionality. It is intended that user imports          ##"
    print "##     this library from another python file.                    ##"
    print "##                                                              ##"
    print "##################################################################"
    SetupEnvironment()
    import EUDPythonAPIs
    help(EUDPythonAPIs)

    