﻿#!/user/bin/env python
'''
    @file EUDDisplayAvailableDevices
    @brief Queries for USB EUD devices connected to host PC. Prints them out to screen.
    #author JBILLING

    Prerequisites:
            Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.      
            EUD.dll must be in same directory or in ../EUD/build/...
            Expected to be run with EUDPythonAPIs  present.
        
    Available API's:
        EUDDisplayAvailableDevices
        EUDGetAvailableDevices
    
'''
######################################################################################
##
##        @file EUDDisplayAvailableDevices
##        @brief Queries for USB EUD devices connected to host PC. Prints them out to screen.
##        #author JBILLING
##        
##        Prerequisites:
##                Expected Python version: Python 2.7 or greater. Not compatible with Python3.0 and above.      
##                EUD.dll must be in same directory or in ../EUD/build/...
##                Expected to be run with EUDPythonAPIs  present.
##            
##        Available API's:
##            EUDDisplayAvailableDevices
##            EUDGetAvailableDevices
##
##
##                             EDIT HISTORY FOR FILE
##        This section contains comments describing changes made to the module.
##           when       who             what, where, why
##           --------   ---             ---------------------------------------------------------
##           10/02/2017 JBILLING        Created
##
######################################################################################
import os, sys,ctypes
from ctypes import *
from optparse import OptionParser

##====---------------------------------------------------====##
##====                Global Variables                   ====##
##====---------------------------------------------------====##
global DEBUGFLAG
DEBUGFLAG=False
global FPGAFLAG
FPGAFLAG=False

global SUCCESS, FAILURE
SUCCESS=0
FAILURE=-1

##====--------------------------------------------------====##
##====                  Python API's                    ====##
##====--------------------------------------------------====##

def EUDDisplayAvailableDevices(EUD_dll=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: EUDDisplayAvailableDevices
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Queries for USB EUD devices connected to host PC. Prints them out to screen.
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object, optional.
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##
        ##  Returns:
        ##      Numeric Error code. 0 for success.
        ##
        #################################################################
    '''
    try:
        import EUDPythonAPIs
    except:
        print "Error! Could not import EUDPythonAPIs. Is the file present?"
        sys.exit(1)
    
    return EUDPythonAPIs.DisplayAvailableDevices(EUD_dll=EUD_dll,debugflag=debugflag,FPGAFlag=FPGAFlag)
    
def EUDGetListofDevices(EUD_dll=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    '''
        #################################################################
        ##
        ##  Function: GetListofDevices
        ##
        ##  Description:
        ##      Python->EUD API
        ##      Gets a python list of devices connected to host PC. 
        ##      Types returned are 32bit deviceID's
        ##
        ##  Arguments:
        ##      EUD_dll=<dllobject>
        ##          DLL object, optional.
        ##          DLL object to be used to imported by LoadLibrary
        ##          If not given, will attempt to load DLL from default paths.
        ##
        ##  Returns:
        ##      Python list of 32bit device ID's.
        ##
        #################################################################
    '''
    try:
        import EUDPythonAPIs
    except:
        print "Error! Could not import EUDPythonAPIs. Is the file present?"
        sys.exit(1)
    
    return EUDPythonAPIs.GetListofDevices(EUD_dll)
    
def parseoptions():
    """
        Parse out given user options
    """
    argparser = OptionParser(version='%prog 1.0')
    argparser.add_option('-e','--eud', dest='eud',              help="EUD DLL/SO Object to use",default=None)
    argparser.add_option('-f','--fpgaflag', dest='FPGAFlag',    help="FPGA EUD instance option.",default=None)
    argparser.add_option('-g','--debugflag', dest='debugflag',  help="Debug EUD instance option.",default=None)
    
    #If -h/--help specified, print out description
    for arg in sys.argv[1:]:
        if ('-h' in arg) or ('--help' in arg):
            print EUDMSMAssertReset.__doc__
            
    (options,args)=argparser.parse_args()
            
    return options
    
def test(EUD_dll=None,deviceID=None,debugflag=DEBUGFLAG, FPGAFlag=FPGAFLAG):
    rvalue=SUCCESS
    rvalue=EUDDisplayAvailableDevices(EUD_dll=EUD_dll,debugflag=options.debugflag,FPGAFlag=options.FPGAFlag)
    
    if not type(EUDGetListofDevices(EUD_dll=EUD_dll)) is list:
        rvalue |= 1
        
    if rvalue == 0x0:
        print "SUCCESS"
    else:
        print "FAILURE"
    return rvalue
    
if __name__ == '__main__':
    options=parseoptions()
    EUDDisplayAvailableDevices(EUD_dll=options.eud,debugflag=options.debugflag,FPGAFlag=options.FPGAFlag)
    
