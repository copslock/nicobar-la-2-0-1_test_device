//============================================================================
//  Name:                                                                     
//    std_debug_gui.cmm 
//
//  Description:                                                              
//    Find and display GUI corresponding to user's environment.
//    Return values which std_debug call can consume:
//        &hwversion &debugprofile &extraoptions &debugsequence_callback
//    
//                                                                            
// Copyright (c) 1999, 2011-2019 Qualcomm Technologies, Inc. and/or its subsidiaries. All rights reserved.        
// This file confidentail and  proprietary
//
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who         what, where, why
// --------   ---         ---------------------------------------------------------
// 10/12/2017 JBILLING    Update logic to prevent unnecessary re-openning of GUI
// 09/12/2017 JBILLING    Additional bug fixes and GET_HW_VERSION_GUI added
// 07/31/2017 JBILLING    Bug fixes in execution
// 07/26/2017 JBILLING    Script path updates
// 06/12/2017 JBILLING    New for Hana - only displays dropdown or continues directly to generated debug menu
//
//###################Arguments passed #####################
LOCAL &ArgumentLine
ENTRY %LINE &ArgumentLine
LOCAL &SUBROUTINE &argline
ENTRY &SUBROUTINE %LINE &argline



LOCAL &BaseDebugDir
&BaseDebugDir="&METASCRIPTSDIR/../debug/&CHIPSET"
LOCAL &DebugMenuScript &DebugMenuScriptBase
&DebugMenuScript="debugmenu.cmm"
&DebugMenuScriptBase="_debugmenu.cmm"

GLOBAL &RUMITAG
GLOBAL &KEEP_RUMITAG_PERSISTENT
GLOBAL &RUMITAG_SETLASTRUN_FLAG
IF "&KEEP_RUMITAG_PERSISTENT"!="FALSE"
(
    &KEEP_RUMITAG_PERSISTENT="TRUE"
)

LOCAL &Default_HW_Version
&Default_HW_Version="V1"

LOCAL &persistent_flag_checkbox_default   ///<set to FALSE to keep persistent RUMITAG checkbox un-checked upon first opening DIALOG.
&persistent_flag_checkbox_default="FALSE"

LOCAL &SelectedRUMITag
//#####################Select Subroutine###################

// Input Argument 0 is the name of the utility
&SUBROUTINE=STR.UPR("&SUBROUTINE")

IF !(("&SUBROUTINE"=="MAIN")||("&SUBROUTINE"=="HELP")||("&SUBROUTINE"=="GET_HW_VERSION_GUI"))
(
    &SUBROUTINE="MAIN"
    &argline="&ArgumentLine"
)

// Call the required utility
GOSUB &SUBROUTINE &argline
LOCAL &rvalue
ENTRY %LINE &rvalue
GOSUB EXIT &rvalue


///////////////////////////Main function//////////////////////////////////
MAIN:
    LOCAL &ArgumentLine
    ENTRY %LINE &ArgumentLine


    
    LOCAL &default_hw_version &debugprofile &extraoptions &debugcallback
   IF ("&RUMI"=="TRUE")
    (
        
        IF (("&RUMITAG"=="")||("&RUMITAG_SETLASTRUN_FLAG"=="FALSE"))
        (
            GOSUB GET_HW_VERSION_GUI
            ENTRY &RUMITAG
        )
        IF ("&KEEP_RUMITAG_PERSISTENT"=="FALSE")
        (
            &RUMITAG_SETLASTRUN_FLAG="FALSE"
        )
        GOSUB RUNDEBUGMENU &RUMITAG
        ENTRY &debugprofile &extraoptions %LINE &debugcallback    
    )

    ELSE
    (
        GOSUB RUNDEBUGMENU
        ENTRY &debugprofile &extraoptions %LINE &debugcallback
    )
    

 
    
    //Run callback  to get start node of linked list
    &debugcallback
    LOCAL &start_LL_node
    ENTRY &start_LL_node
    
    IF ("&SelectedRUMITag"=="")
    (
        IF ("&RUMITAG"=="")
        (
            &SelectedRUMITag="&Default_HW_Version"
        )
        ELSE
        (
            &SelectedRUMITag="&RUMITAG"
        )
    )
    
    
    RETURN &SelectedRUMITag &debugprofile &extraoptions &start_LL_node
    
    
GET_HW_VERSION_GUI:
            
            do &BaseDebugDir\available_hw_versions.cmm GETAVAILABLETAGS
            LOCAL &availablerumitags
            ENTRY &availablerumitags

            WINPOS 0. 0. 32. 5. , , , RUMITAGWindow
            DIALOG
            (&
                HEADER "Available RUMI tags for debug"
                
                POS  0. 0. 29. 1.
                TEXT "Please select the RUMI tag which was loaded"
                RUMIPULLDOWN: PULLDOWN "&availablerumitags" "GOTO SET_HW_TAG_ID"
                
                KEEPTAGOPT: CHECKBOX "Keep this RUMI tag for future debug runs" "GOTO SET_PERSISTENT_RUMITAG"
                
                POS 0. 3. 10. 2.
                BUTTON "GO" "CONTINUE"
                POS 12. 3. 10. 2.
                BUTTON "Cancel" "GOTO DIALOGEXIT"
            )
            IF "&persistent_flag_checkbox_default"=="TRUE"
            (
                DIALOG.SET KEEPTAGOPT 
            )
            GOTO SET_PERSISTENT_RUMITAG
RUMITAG_DIALOG_STOP:
            STOP
            
            &RUMITAG_SETLASTRUN_FLAG="TRUE"
            
            LOCAL &selectedtag &debugmenuscript
            &selectedtag=DIALOG.STRING(RUMIPULLDOWN)
            IF DIALOG.BOOLEAN(KEEPTAGOPT)
            (
                &RUMITAG="&selectedtag"
            )
            DIALOG.END

            RETURN &selectedtag
            
            
SET_HW_TAG_ID:
    
    &SelectedRUMITag=DIALOG.STRING(RUMIPULLDOWN)
    
    GOTO RUMITAG_DIALOG_STOP
	
        
SET_PERSISTENT_RUMITAG:
    &RUMITAG=DIALOG.STRING(RUMIPULLDOWN)
    IF DIALOG.BOOLEAN(KEEPTAGOPT)
    (
        &KEEP_RUMITAG_PERSISTENT="TRUE"
    )
    ELSE
    (
        &KEEP_RUMITAG_PERSISTENT="FALSE"
    )
    GOTO RUMITAG_DIALOG_STOP 
    
RUNDEBUGMENU:
    LOCAL &selectedtag
    ENTRY %LINE &selectedtag
    LOCAL &debugprofile &extraoptions &debugcallback
 IF "&RUMI"=="TRUE"
    (
        //&debugmenuscript="&BaseDebugDir"+"/"+"&selectedtag"+"/"+"&DebugMenuScript"
        &debugmenuscript="&BaseDebugDir"+"/"+"&selectedtag"+"/"+"&CHIPSET"+"_"+"&selectedtag"+"&DebugMenuScriptBase"
        IF !OS.FILE("&debugmenuscript")
        (
            do std_fatalexit SYSTEM "Error! Debug menu file not found: &debugmenuscript"
            plist
            pstep
        )
        //Debug menu script will retrieve debug callback based on user's input.
        do &debugmenuscript
        ENTRY &debugprofile %LINE &debugcallback

     )
          ELSE
    (
        do std_debug_gui
    )  
    //Now return options in  teh form that std_debug will consume
    &extraoptions="NULL"
    RETURN &debugprofile &extraoptions &debugcallback
    
    
EXIT:
    LOCAL &rvalue
    ENTRY %LINE &rvalue
    ENDDO &rvalue

DIALOGEXIT:
    &RUMITAG_SETLASTRUN_FLAG="FALSE"
    DIALOG.END
    ENDDO NULL

