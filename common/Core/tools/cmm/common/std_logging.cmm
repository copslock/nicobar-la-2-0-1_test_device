//============================================================================
//  Name:                                                                     
//    std_logging.cmm
//
//  Description:                                                              
//    Logging utility for practice scripts
//
//  Subroutines:
//      INITLOG
//      SETLOGPATH
//      MAIN (writes message to log)
//      ENDLOG
//      HELP
//                                                                            
// Copyright (c) 2016-2018 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
// Confidential and Proprietary
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who           what, where, why
// --------   ---           ---------------------------------------------------------
// 08/17/2017 JBILLING      Created


//################### Arguments passed #####################
LOCAL &ArgumentLine
ENTRY %LINE &ArgumentLine
LOCAL &SUBROUTINE &argline
ENTRY &SUBROUTINE %LINE &argline

//################### General Variables ############################

GLOBAL &STDLOGPATH

LOCAL &DefaultLogPath
IF ("&HOSTOS"=="Windows")
(
    &DefaultLogPath="C:\temp\std_logging_log.log"
)
ELSE
(
    &DefaultLogPath="~~~\std_logging_log.log"
)
//#####################Various env prep####################


//#####################Select Subroutine###################

// Input Argument 0 is the name of the utility
&SUBROUTINE=STR.UPR("&SUBROUTINE")

IF !(("&SUBROUTINE"=="HELP")||("&SUBROUTINE"=="INITLOG")||("&SUBROUTINE"=="SETLOGPATH")||("&SUBROUTINE"=="ENDLOG"))
(
    &SUBROUTINE="MAIN"
    &argline="&ArgumentLine"
)

// Call the required utility
GOSUB &SUBROUTINE &argline
LOCAL &rvalue
ENTRY %LINE &rvalue
GOSUB EXIT &rvalue


////////////////////////////////////////////////////////////
///
///  MAIN
///  @brief Retrieves available debug profiles for specified hardware revision.
///
////////////////////////////////////////////////////////////
MAIN:
    LOCAL &message
    ENTRY %LINE &message
    
    IF ("&STDLOGPATH"=="")
    (
        do std_logging INITLOG &DefaultLogPath
    )
    do std_utils GETTIMESTAMP
    LOCAL &timestamp
    ENTRY %LINE &timestamp
    OPEN #1 &STDLOGPATH /Append
    WRITE #1 "&timestamp &message"
    CLOSE #1
    
    
    RETURN SUCCESS
    
////////////////////////////////////////////////////////////
///
///  INITLOG
///  @brief Sets global var to given value. Writes to log
///
////////////////////////////////////////////////////////////
INITLOG:
    LOCAL &logpath
    ENTRY %LINE &logpath
    IF STRING.SCAN("&logpath"," ",0)!=-1
    (
        do std_fatalexit SYSTEM "Error! std_logging INITLOG: space found in logpath during std_logging"
        PLIST
        PSTEP
    )
    &STDLOGPATH="&logpath"
    do std_utils  GETTIMESTAMP
    LOCAL &timestamp
    ENTRY %LINE &timestamp
    
    IF OS.FILE("&STDLOGPATH")
    (
        os.command del &STDLOGPATH
        wait.500ms
    )
    OPEN #1 &STDLOGPATH /Create
    WRITE #1 "&timestamp Log started."
    CLOSE #1
    
    RETURN SUCCESS
    
////////////////////////////////////////////////////////////
///
///  SETLOGPATH
///  @brief Sets global logpath variable. Useful for intercom slave windows.
///
////////////////////////////////////////////////////////////
SETLOGPATH:
    LOCAL &logpath
    ENTRY %LINE &logpath
    IF STRING.SCAN("&logpath"," ",0)!=-1
    (
        do std_fatalexit SYSTEM "Error! std_logging SETLOGPATH: space found in logpath during std_logging"
        PLIST
        PSTEP
    )
    &STDLOGPATH="&logpath"
    
    RETURN SUCCESS
    
////////////////////////////////////////////////////////////
///
///  FINISHLOG
///  @brief Appends finish line to log file.
///
////////////////////////////////////////////////////////////
FINISHLOG:
    
    do  std_utils GETTIMESTAMP
    ENTRY %LINE &timestamp
    
    do std_logging "=====================Log End.=========================="
    
    RETURN SUCCESS
    
EXIT:
    LOCAL &rvalue
    ENTRY %LINE &rvalue
    ENDDO &rvalue