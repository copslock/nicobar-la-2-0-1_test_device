//============================================================================
//  Name:                                                                     
//    std_debug_fus.cmm 
//
//  Description:                                                              
//    Specialized debug script for fusion targets
//                                                                            
// Copyright (c) 1999, 2011-2019 Qualcomm Technologies, Inc. and/or its subsidiaries. All rights reserved.        
//
//
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who             what, where, why
// --------   ---             ---------------------------------------------------------
// 07/26/2017 JBILLING        Changes for sdx50 added
// 12/16/2016 JBILLING        Added Fusion3 setup for sdx20
// 12/10/2015 JBILLING        Created for 8906/9x55 fusion


//**************************************************
//                  Declarations 
//**************************************************
LOCAL &ArgumentLine
LOCAL &UTILITY &Remainder
LOCAL &targetprocessor
GLOBAL &APQResetCount
GLOBAL &ImageDebugType

//**************************************************
//                  Arguments Passed 
//**************************************************
ENTRY %LINE &ArgumentLine
//ENTRY &UTILITY &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10 &IARG11

ENTRY &UTILITY %LINE &Remainder
//**************************************************
//                  Defaults 
//**************************************************

//**************************************************
//                  Subroutine Checks
//**************************************************
// Name of the utility we are calling
LOCAL &SUBROUTINE
// Any subroutine specific options
// Default exists for each subroutine
LOCAL &OPTION


//**************************************************
//                  Basic Options Check
//**************************************************

&SUBROUTINE="&UTILITY"
IF ("&ArgumentLine"=="")
(
    &SUBROUTINE="POWERUP_ROUTINE"
)
ELSE IF (STRING.UPR("&UTILITY")=="HELP")
(
    &SUBROUTINE="&UTILITY"
)
ELSE
(
    &SUBROUTINE="MAIN"
)


    LOCAL &rvalue
    // This should be created by some top level script. The setupenv for each proc would
    // set this up
     AREA.SELECT
    // Call the required utility
    GOSUB &SUBROUTINE &ArgumentLine
    ENTRY %LINE &rvalue

    GOSUB EXIT &rvalue





//**************************************************
//
// @Function: MAIN
// @Description : sets up remote processor to attach on power up.
//
//**************************************************
MAIN:
        LOCAL &ArgumentLine
        ENTRY %LINE &ArgumentLine
        
        do optextract Img,Lpm,Bkpts,CTI_Enable,alternateelf,extraoption &ArgumentLine
        LOCAL &image &lpm_option &user_defined_bkpts &cti_enable &alternateelf &extraoption
        ENTRY &image &lpm_option &user_defined_bkpts &cti_enable &alternateelf &extraoption //expect 6 returns from optextract
        
        //IF STRING.SCAN("&extraoption","firstrundone",0)!=-1
        //(
        //    GOTO CONTINUEDEBUG
        //)
        
        //ELSE
        //(
        //    &image="mpss-fus"
        //)
        do std_debug_&CHIPSET GETDEBUGDEFAULTS NULL &image &lpm_option &cti_enable &alternateelf &extraoption
        LOCAL &image &targetprocessor &targetprocessorport &bootprocessor &bootprocessorport &peripheral_processor &processortimeoutvalue &debugscript &symbolloadscript &imagebuildroot &entry_bkpt &error_bkpts &lpm_option &sleep_disable_command &cti_enable &multi_elf_option &alternateelf &extraoption
        ENTRY &image &targetprocessor &targetprocessorport &bootprocessor &bootprocessorport &peripheral_processor &processortimeoutvalue &debugscript &symbolloadscript &imagebuildroot &entry_bkpt &error_bkpts &lpm_option &sleep_disable_command &cti_enable &multi_elf_option &alternateelf &extraoption
        
        //IF ("&ImageDebugType"=="apps,mpss")
        //(
         //   do std_debug_&CHIPSET GETDEBUGDEFAULTS NONE apss
         //   ENTRY &none1 &none2 &none3 &none4 &none5 &none6 &none7 &none8 &none9 &none10 &entry_bkpt %LINE &therest
        //)

        do listparser FULLLIST &peripheral_processor &debugscript &symbolloadscript &imagebuildroot &targetprocessor &targetprocessorport
        LOCAL &debugscript_local &debugscript_target &symbolloadscript_local &symbolloadscript_target &imagebuildroot_local &imagebuildroot_target &targetprocessor_local &targetprocessor_target &targetprocessorport_local &targetprocessorport_target
        ENTRY &debugscript_local &debugscript_target &symbolloadscript_local &symbolloadscript_target &imagebuildroot_local &imagebuildroot_target &targetprocessor_local &targetprocessor_target &targetprocessorport_local &targetprocessorport_target
    
        IF ("&FUSION_TYPE"=="FUSION3")
        (
            SYSTEM.POLLING FAST
            do hwio 
            do std_memorymap
            IF (sys.mode()!=0xB)
            (
                sys.down
            )

            GLOBAL &ARGLINE
            &ARGLINE="&ArgumentLine"
            &ImageDebugType="&image"

            b.d /all
            b.s &entry_bkpt /Onchip

            //break at trustzone entry to break early in system
            b.s &SHARED_IMEM_start /Onchip

            //initialize the reset counter
            &APQResetCount=0
            GLOBALON POWERUP do std_debug_fus

            print "reset APQ now !!!"
        )

        IF "&cti_enable"=="true"
        (
            do std_cti_apps SETUPSYNCHALT
        )
        IF ("&FUSION_TYPE"!="FUSION3")
        (
            GO
        )
        //POWER_ROUTINE takes it from here
        END
        
        
        
//This is run from the context of the remote chip's apps processor    
POWERUP_ROUTINE:

    //first reset on APQ
    IF (&APQResetCount==0)
    (
        &APQResetCount=&APQResetCount+1
        GLOBALON POWERUP do std_debug_fus
        SYSTEM.POLLING FAST
        IF ("&FUSION_TYPE"=="FUSION3")
        (
            &extraoption="&extraoption"+",secondpass"
            //GLOBALON POWERUP DEFAULT
        )
        END
    )

    //second reset from APQ
    IF (SYS.MODE()!=0xB)
    (
      sys.m.a
    )
    break
        

    IF ("&FUSION_TYPE"=="FUSION3")
    (
        IF (SYS.MODE()==0xB)
        (
           //refresh the reset counter
           &APQResetCount=0
           
           IF ("&ImageDebugType"=="sbl1")||("&ImageDebugType"=="tz")||("&ImageDebugType"=="aop")
           (
		      IF (r(pc)>&SHARED_IMEM_start)
			  (
			     print "auto attach failed. Run adb cmds to halt/reset APQ and try this script again!" 
			  )
           )
           ELSE
           (
                GO
                WAIT !RUN()

                IF ("&ImageDebugType"=="mpss")||("&ImageDebugType"=="mpss-fus")||("&ImageDebugType"=="mpss,apps")||("&ImageDebugType"=="apps,mpss")||("&ImageDebugType"=="rpm")
                (
                    //set debug cookie to break in RPM/modem
                    do std_debug_&CHIPSET SETDEBUGCOOKIE SET &ImageDebugType
                )
                //if system is running, wait for exec to hit first breakpoint 
                go
                WAIT !RUN()
                //b.d 0x14680000 /o
            )
        )
        ELSE
        (
           //kill script exec if we're waiting for second reset from APQ
           print "something went wrong !!!"
           enddo
        )

        
        
        do optextract Img,Lpm,Bkpts,CTI_Enable,alternateelf,extraoption &ARGLINE
        LOCAL &image &lpm_option &user_defined_bkpts &cti_enable &alternateelf &extraoption
        ENTRY &image &lpm_option &user_defined_bkpts &cti_enable &alternateelf &extraoption //expect 6 returns from optextract
        
        IF ("&FUSION_TYPE"=="FUSION3")
        (
            &extraoption="&extraoption"+",secondpass"
        )
        
        do std_utils SANITIZEQUOTATIONS none &image &lpm_option &user_defined_bkpts &cti_enable &alternateelf
            ENTRY &image &lpm_option &user_defined_bkpts &cti_enable &alternateelf
            
        do std_debug_&CHIPSET GETDEBUGDEFAULTS NULL &image &lpm_option &cti_enable &alternateelf &extraoption
        LOCAL &image &targetprocessor &targetprocessorport &bootprocessor &bootprocessorport &peripheral_processor &processortimeoutvalue &debugscript &symbolloadscript &imagebuildroot &entry_bkpt &error_bkpts &lpm_option &sleep_disable_command &cti_enable &multi_elf_option &alternateelf &extraoption
        ENTRY &image &targetprocessor &targetprocessorport &bootprocessor &bootprocessorport &peripheral_processor &processortimeoutvalue &debugscript &symbolloadscript &imagebuildroot &entry_bkpt &error_bkpts &lpm_option &sleep_disable_command &cti_enable &multi_elf_option &alternateelf &extraoption
        
        do listparser FULLLIST &peripheral_processor &debugscript &symbolloadscript &imagebuildroot &targetprocessor &targetprocessorport
        LOCAL &debugscript_local &debugscript_target &symbolloadscript_local &symbolloadscript_target &imagebuildroot_local &imagebuildroot_target &targetprocessor_local &targetprocessor_target &targetprocessorport_local &targetprocessorport_target
        ENTRY &debugscript_local &debugscript_target &symbolloadscript_local &symbolloadscript_target &imagebuildroot_local &imagebuildroot_target &targetprocessor_local &targetprocessor_target &targetprocessorport_local &targetprocessorport_target
        
CONTINUEDEBUG: //will have to inherit argumentline variables from above for this case.
        
        IF ("&ImageDebugType"=="apps,mpss")||("&ImageDebugType"=="mpss")
        (
            do std_debug_&CHIPSET GETDEBUGDEFAULTS NULL &ImageDebugType &lpm_option &cti_enable &alternateelf &extraoption
            ENTRY &none1 &none2 &none3 &none4 &none5 &none6 &none7 &none8 &none9 &none10 &user_defined_bkpts %LINE &therest
        )
        
        IF (!(("&debugscript_local"=="NULL")||("&debugscript_local"==""))||STRING.SCAN("&extraoption","firstrundone",0)!=-1)&&("&peripheral_processor"=="mproc")
        (       
            do &debugscript_local Img=&image Lpm=&lpm_option Bkpts=&user_defined_bkpts CTI_Enable=&cti_enable alternateelf=&alternateelf extraoption=&extraoption
			&extraoption="&extraoption"+",firstrundone"
            ENTRY %LINE &rvalue
        )
        
        if "&cti_enable"=="true"
        (
            do std_cti masters=&targetprocessor_target slaves==&targetprocessor_local 
        )

        if !(("&debugscript_target"=="NULL")||("&debugscript_target"==""))
        (       
            do &debugscript_target Img=&image Lpm=&lpm_option Bkpts=&user_defined_bkpts CTI_Enable=&cti_enable alternateelf=&alternateelf extraoption=&extraoption
            ENTRY %LINE &rvalue
        )
        
        if (!(("&debugscript_local"=="NULL")||("&debugscript_local"=="")))&&(((STRING.SCAN("&extraoption","prioritizeperiphproc",0)!=-1)&&(STRING.SCAN("&extraoption","firstrundone",0)==-1))||("&peripheral_processor"!="mproc"))
        (       
            do &debugscript_local Img=&image Lpm=&lpm_option Bkpts=&user_defined_bkpts CTI_Enable=&cti_enable alternateelf=&alternateelf extraoption=&extraoption
            ENTRY %LINE &rvalue
        )
        
        
        do std_results std_debug_&FUSION_TYPE &rvalue &ArgumentLine
        //change auto powerup feature
        GLOBALON POWERUP default

        //exit from here and let std_debug take it again.
        ENDDO &rvalue
        
    )    
    ELSE
    (
        sys.m.a
        on error continue
        b.d /all
        b.s sbl1_main_ctl /o
        ON ERROR continue
            G
            G
        ON ERROR
        WAIT !RUN()
        
        
        do optextract Img,Lpm,Bkpts,CTI_Enable,alternateelf,extraoption &ARGLINE
        LOCAL &image &lpm_option &user_defined_bkpts &cti_enable &alternateelf &extraoption
        ENTRY &image &lpm_option &user_defined_bkpts &cti_enable &alternateelf &extraoption //expect 6 returns from optextract
        
        do std_utils SANITIZEQUOTATIONS none &image &lpm_option &user_defined_bkpts &cti_enable &alternateelf
            ENTRY &image &lpm_option &user_defined_bkpts &cti_enable &alternateelf
            
        

        
        
        do std_debug_&CHIPSET GETDEBUGDEFAULTS NULL mpss-fus &lpm_option &user_defined_bkpts &cti_enable &alternateelf &extraoption
        LOCAL &image &targetprocessor &targetprocessorport &bootprocessor &bootprocessorport &peripheral_processor &processortimeoutvalue &debugscript &symbolloadscript &imagebuildroot &entry_bkpt &error_bkpts &lpm_option &sleep_disable_command &cti_enable &multi_elf_option &alternateelf &extraoption
        ENTRY &image &targetprocessor &targetprocessorport &bootprocessor &bootprocessorport &peripheral_processor &processortimeoutvalue &debugscript &symbolloadscript &imagebuildroot &entry_bkpt &error_bkpts &lpm_option &sleep_disable_command &cti_enable &multi_elf_option &alternateelf &extraoption
        
        do listparser FULLLIST &peripheral_processor &debugscript &symbolloadscript &imagebuildroot &targetprocessor &targetprocessorport
        LOCAL &debugscript_local &debugscript_target &symbolloadscript_local &symbolloadscript_target &imagebuildroot_local &imagebuildroot_target &targetprocessor_local &targetprocessor_target &targetprocessorport_local &targetprocessorport_target
        ENTRY &debugscript_local &debugscript_target &symbolloadscript_local &symbolloadscript_target &imagebuildroot_local &imagebuildroot_target &targetprocessor_local &targetprocessor_target &targetprocessorport_local &targetprocessorport_target
            
        do std_debug.cmm SETUPTARGETPROCESSOR apps-fus1 &processortimeoutvalue &targetprocessor_target &MPSS_PORT &bootprocessor
        
        //INTERCOM.EXECUTE &MPSS_PORT SYS.M.A
        b
        do std_intercom_do &MPSS_PORT std_utils BREAKPROC
        
        do std_debug_mpss &ARGLINE
        
        
        IF "&cti_enable"=="true"
        (
            do std_intercom_do &APPS0_MSM_PORT do std_debug_apps TRACEGUI
        )
        
        
        STOP
    )
////////////////////////////////////////////
//
//          FATALEXIT
//
//          Exits all scripts.
//          If logging is enabled, appends failure keyword
//          to passed string and sends that to PRINTRESULTLOG
//
//
//
///////////////////////////////////////////
FATALEXIT:
    LOCAL &string
    ENTRY %LINE &string

    PRINT %ERROR "Loadsim error. Error type: &Result "
    
    
    
        IF ("&LOGSENABLED"=="TRUE")
        (
            //Failure keyword is sometimes passed from lower scripts. 
            //Only append it if it's not already there for cleaner logs.
            IF STRING.SCAN("&string","&FAILUREKEYWORD",0)!=-1
            (
                GOSUB PRINTRESULTLOG &string 
            )
            ELSE
            (
                GOSUB PRINTRESULTLOG &FAILUREKEYWORD " - " &string 
            )
        )

    END

EXIT:

    IF ("&LOGSENABLED"=="TRUE")
    (
        LOCAL &string
        ENTRY %LINE &string
        
        GOSUB PRINTRESULTLOG &string 
    )
    ENDDO

    

