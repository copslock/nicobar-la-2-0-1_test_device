/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
extern "C"{
#include "qsee_log.h"
}
#include "skeleton_cppapp.h" // Common header between CA and TA
#include <stdint.h>

extern "C"{
  void tz_app_init(void);
  void tz_app_shutdown(void);
  void tz_app_cmd_handler(void *cmd, uint32_t cmdlen, void *rsp, uint32_t rsplen);
}

/* tz_app_init() is called on application start. 
   Any initialization required before the TA is ready to handle commands 
   should be placed here. */
void tz_app_init(void)
{
  /* Get the current log mask */
  uint8_t log_mask = qsee_log_get_mask();
  
  /* Enable debug level logs */
  qsee_log_set_mask(log_mask | QSEE_LOG_MSG_DEBUG);
  qsee_log(QSEE_LOG_MSG_DEBUG, "App Start");
}

/* tz_app_init() is called on application shutdown. 
   Any deinitialization required before the TA is unloaded should be placed here. */
void tz_app_shutdown(void)
{
  /* Called on application shutdown. Can be used for any deinitialization required. */
  qsee_log(QSEE_LOG_MSG_DEBUG, "App shutdown");
}

/* tz_app_cmd_handler() is the entry point for a TA to handle a command from 
   a Client Application.*/
void tz_app_cmd_handler(void *cmd, uint32_t cmdlen, void *rsp, uint32_t rsplen)
{
  /* Cast the received command/response to the expected message type */
  struct cmd_req_t *cmd_ptr = (struct cmd_req_t *)cmd;
  struct cmd_rsp_t *rsp_ptr = (struct cmd_rsp_t *)rsp;

  /* Basic error check to ensure command/response are correct message types */
  if (cmdlen < sizeof(struct cmd_req_t) || rsplen < sizeof(struct cmd_rsp_t))
  {
    return;
  }

  qsee_log(QSEE_LOG_MSG_DEBUG, "skeleton cpp TA received cmd_id %d", cmd_ptr->cmd_id);
  
  /* Handle the command based on cmd_id */
  switch (cmd_ptr->cmd_id) {
    case SAMPLE_CMD:
      rsp_ptr->status = SUCCESS;
      break;
    default:
      rsp_ptr->status = FAILURE;
      break;
  }
}
