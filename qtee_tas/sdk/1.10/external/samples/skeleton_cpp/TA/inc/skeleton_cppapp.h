/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
#include <stdint.h>

/* Request cmd_id definitions */
#define SAMPLE_CMD 1

/* Response status definitions */
#define SUCCESS 0
#define FAILURE 1

/** @cond */
// Request structure
struct cmd_req_t {
  uint32_t cmd_id;
};

// Response structure
struct cmd_rsp_t {
  uint32_t status;
};
/** @endcond */
