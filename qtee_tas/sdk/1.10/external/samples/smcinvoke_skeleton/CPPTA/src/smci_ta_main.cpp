/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
#include <cstdint>

#include "CSMCIExample_open.h"
#include "object.h"

extern "C" {
#include "qsee_log.h"
}

extern "C" {
void tz_app_init(void);
void tz_app_shutdown(void);
int32_t app_getAppObject(Object credentials, Object *obj_ptr);
}

/* tz_app_init() is called on application start.
   Any initialization required before the TA is ready to handle commands
   should be placed here. */
void tz_app_init(void)
{
  /* Get the current log mask */
  uint8_t log_mask = qsee_log_get_mask();

  /* Enable debug level logs */
  qsee_log_set_mask(log_mask | QSEE_LOG_MSG_DEBUG);
  qsee_log(QSEE_LOG_MSG_DEBUG, "App Start");
}

/* tz_app_shutdown() is called on application shutdown.
   Any deinitialization required before the TA is unloaded should be placed
   here. */
void tz_app_shutdown(void)
{
  qsee_log(QSEE_LOG_MSG_DEBUG, "App shutdown");
}

/* using app_getAppObject() we return an object for an interface
 * "ISMCIExample", which  we've defined in ISMCExample.idl. The CA gets
 * to this point using the IAppController interface; specifically
 * IAppController_getAppObject(). The CA implementation shows an example
 * of the steps involved in reaching this point. */
int32_t app_getAppObject(Object credentials, Object *obj_ptr)
{
  (void)credentials;
  return CSMCIExample_open(obj_ptr);
}