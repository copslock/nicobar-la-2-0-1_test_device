/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef SMCI_SKELETON_H
#define SMCI_SKELETON_H

#include <cstddef>
#include <cstdint>

extern "C" {
  #include "map_trusted_application.h"
}

int32_t run_smcinvoke_ta_example(ta_image_data *img_data);

#endif /* SMCI_SKELETON_H */
