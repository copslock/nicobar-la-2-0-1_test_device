/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include <climits>
#include <cstdint>
#include <cstdio>
#include <new>

#include "TZCom.h"
#include "alog.h"
#include "ca_paths.h"  // Required for 'TA_PATH'
#include "object.h"
#include "smci_skeleton.h"
#include "stringl.h"

extern "C" {
#include "map_trusted_application.h"
#include "qtee_init.h"  // QTEE off-target environment initialization
}

/* Similar to Android LOG_TAG, define a tag that appears when logging from this
 * application */
static char const LOG_TAG[] = "SMCInvoke_skeleton_CA";

/* This function builds the path to our TA using strlcpy(), using the TA name
 * SMCI_SKELETON_TA_NAME - this is defined in the TA SConscript. */
static char const *create_image_path(void)
{
/* ensure PATH_MAX is defined */
#if !defined(PATH_MAX)
#if defined(MAX_PATH)
#define PATH_MAX MAX_PATH
#else
#define PATH_MAX 260
#endif
#endif

  static const char testTAName[] = SMCI_SKELETON_TA_NAME;
  static char imageName[PATH_MAX];
  size_t index = strlcpy(imageName, TA_PATH, sizeof(imageName));
  index += strlcpy(&imageName[index], testTAName, sizeof(imageName) - index);
  index += strlcpy(&imageName[index], ".so", sizeof(imageName) - index);
  return imageName;
}

int main(void)
{
  int ret = Object_ERROR;         // Return value for qtee_emu calls
  int exampleRet = Object_ERROR;  // Return value for these examples

  ta_image_data img_data;  // A struct containing infomation about the TA

  /* Initialize QTEE Emu. */
  ret = qtee_emu_init();
  if (ret) {
    ALOGE("QTEE Emu initialization failed with %d!", ret);
    return ret;
  }

  ALOGV("QTEE Emu initialized.");

  /* Before we can load our TA, we have to load it into a buffer. */
  char const *imageName = create_image_path();

  /* Here we call map_trusted_application, which maps the TA specified in the path we created
   * into a struct trusted_app_info. */
  ret = map_trusted_application(imageName, &img_data);

  if (ret < 0) {
    ALOGE("Error mapping TA!");
    ret = Object_ERROR;
    goto cleanup;
  }
  /*Run the SMCInvoke TA example, and then deinit the SDK after it returns. */
  exampleRet = run_smcinvoke_ta_example(&img_data);

  if (exampleRet) {
    ALOGE("Errors were encountered during execution: %d!", exampleRet);
  } else {
    ALOGD("CA executed successfully.");
  }

cleanup:
  unmap_trusted_application(&img_data);

  ret = qtee_emu_deinit();
  if (ret) {
    ALOGE("Error occurred during QTEE Emu deinit: %d!", ret);
  }
  return (ret || exampleRet);
}
