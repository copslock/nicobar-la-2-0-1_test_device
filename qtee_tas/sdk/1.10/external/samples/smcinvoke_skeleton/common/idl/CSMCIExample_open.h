/**
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
#pragma once

#include "object.h"
#include <stdint.h>

/* This function creates  a new ISMCIExample object. */
int32_t CSMCIExample_open(Object *obj_out);
