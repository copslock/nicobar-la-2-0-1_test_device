# Copyright (c) 2019 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.

Import('env')
env = env.Clone()

# TA output binary will be called 'smcinvoke_example_ta32' or 'smcinvoke_example_ta64'
target = 'smcinvoke_example_ta' + ['32', '64'][env.Is64BitImage()]

includes = [
  '../../common/idl',
  '../../common/inc',
  '../../TA_SVC/idl',
]

sources = [
  'ta_main.c',
  'CSMCIExampleApp.c',
]

md = {
  'appName':      target,

  # Because we want to use a service that isn't part of the default privilege
  # set (IAdder), we must give this TA the 'Adder' privilege in its metadata.
  # Privilege names use the same name as the interface, omitting the leading 'I'.
  'privileges': ['default', 'Adder']
}

# Use the QTEE SDK Builder for TAs
app = env.SecureAppBuilder(
  sources   = sources,
  includes  = includes,
  metadata  = md,
  # 'image' is the name used for the output .so/mbn file
  image     = target,
)

# Add an alias so that the TA can be built by specifying 'smcinvoke_example_ta' on the command line
env.Alias('smcinvoke_example_ta', app)

# Since this TA depends on the service TA, we add this env.Depends() line.
env.Depends(app, env.Alias('smcinvoke_example_svc_ta'))
# Return the TA binary for higher level SConscripts to reference
Return('app')
