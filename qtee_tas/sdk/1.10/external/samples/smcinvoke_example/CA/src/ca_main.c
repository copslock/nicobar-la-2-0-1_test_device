/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include "IMemRegion.h"
#include "MemObj.h"
#include "TZCom.h"
#include "alog.h"
#include "ca_paths.h"  // Required for 'TA_PATH'
#include "cdefs.h"
#include "map_trusted_application.h"
#include "object.h"
#include "qtee_init.h"  // QTEE off-target environment initialization
#include "smci_example_app.h"
#include "stringl.h"

#include <stdint.h>
#include <stdio.h>

/* Similar to Android LOG_TAG, define a tag that appears when logging from this
 * application. */
static char const LOG_TAG[] = "SMCInvoke_Example_CA";

/* Define PATH_MAX for TAs if not already defined. */
#if !defined(PATH_MAX)
#if defined(MAX_PATH)
#define PATH_MAX MAX_PATH
#else
#define PATH_MAX 260
#endif
#endif

/* This function builds the path to our TA using strlcpy(), using the TA name
 * EXAMPLE_TA_NAME - this is defined in the TA SConscript. */
static const char *create_image_path(const char *path, const char *testTAName)
{
  static char imageName[PATH_MAX];

  size_t index = strlcpy(imageName, path, sizeof(imageName));
  index += strlcpy(&imageName[index], testTAName, sizeof(imageName) - index);
  index += strlcpy(&imageName[index], ".so", sizeof(imageName) - index);

  return imageName;
}

int main(void)
{
  int ret = 0;         // Return value for qtee_emu calls
  int exampleRet = 0;  // Return value for these examples

  Object serviceTAMemObj = Object_NULL;  // We'll use this with IAppLoader_loadFromRegion later

  const char *imageName = NULL;
  ta_image_data img_data[2];

  /* Initialize the QTEE Emu */
  ret = qtee_emu_init();
  if (ret) {
    ALOGE("QTEE Emu initialization failed with error: %d!", ret);
    return ret;
  }

  ALOGD("QTEE Emu initialized.");

  /* Load TAs into memory for loading via IAppLoader.
   * Before we can load a TA, we have to load it into a buffer. We need to load our main TA,
   * and the TA that hosts our IAdder service. */

  ALOGD("Creating Example TA buffer.");
  imageName = create_image_path(TA_PATH, EXAMPLE_TA_NAME);

  /* Here we call map_trusted_application, which maps the TA specified in the path we created into a buffer. */
  ret = map_trusted_application(imageName, &img_data[EXAMPLE_TA]);

  /* Cleanup if mapping the TA failed. */
  if (ret) {
    ALOGE("Error mapping example TA!");
    ret = Object_ERROR;
    goto main_cleanup;
  }

  /* As well as loading TAs directly from a buffer, we can also load them from a memory region. */
  ALOGD("Creating Example Service TA memory object.");
  imageName = create_image_path(SVC_TA_PATH, EXAMPLE_SERVICE_TA_NAME);

  ret = map_trusted_application(imageName, &img_data[EXAMPLE_SERVICE_TA]);

  if (ret) {
    ALOGE("Error mapping example service TA!");
    ret = Object_ERROR;
    goto main_cleanup;
  }

  /* Create a memory object containing the service TA. */
  serviceTAMemObj = MemObj_new(img_data[EXAMPLE_SERVICE_TA].buffer,
                               img_data[EXAMPLE_SERVICE_TA].buffer_size,
                               IMemRegion_PERM_R);

  if (Object_isNull(serviceTAMemObj)) {
    ALOGE("MemObj allocation failed!");
    ret = Object_ERROR_KMEM;
    goto main_cleanup;
  }

  /* Run the SMCInvoke TA example, and then deinit the SDK after it returns.
   * We pass the example TA's buffer and size inside img_data, but we do not need to do the
   * same for the MemObj containing our example service TA. */
  exampleRet = run_smcinvoke_ta_example(&img_data[EXAMPLE_TA], serviceTAMemObj);

  /* Now that we've given the serviceTAMemObj to run_smcinvoke_ta_example, we do not need it
   * anymore and can free it inside the called function. Here we just need to set the reference
   * to null. */
  serviceTAMemObj = Object_NULL;

main_cleanup:

  if (exampleRet) {
    ALOGE("Errors were encountered during execution: %d", exampleRet);
  } else {
    ALOGD("CA executed successfully!");
  }

  ret = qtee_emu_deinit();
  if (ret) {
    ALOGE("Error occurred during QTEE Emu deinit: %d", ret);
  }
  /* Unmap our TAs */

  for (size_t i = 0; i < C_LENGTHOF(img_data); i++) {
      unmap_trusted_application(&img_data[i]);
  }

  return (ret || exampleRet);
}

