/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include "CLoggerExampleCBO_open.h"
#include "ILoggerExampleCBO.h"
#include "ILoggerExampleCBO_invoke.h"
#include "alog.h"
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

static char const LOG_TAG[] = "SMCInvoke_Example_CA";

typedef struct CLoggerExampleCBO {
  int refs;
  int logCount;
} CLoggerExampleCBO;

static int32_t CLoggerExampleCBO_retain(CLoggerExampleCBO *me)
{
  me->refs++;

  /* Log our callback object's reference count here, to make it clearer
   * where it is retained and released when setting it inside our TA
   * object. */
  ALOGD("CLoggerExampleCBO reference count++: %d", me->refs);

  return Object_OK;
}

static int32_t CLoggerExampleCBO_release(CLoggerExampleCBO *me)
{
  me->refs--;
  ALOGD("CLoggerExampleCBO reference count--: %d", me->refs);

  if (me->refs == 0) {
    free(me);
  }

  return Object_OK;
}

static int32_t CLoggerExampleCBO_log(CLoggerExampleCBO *me,
                                     uint8_t severity_val,
                                     const char *in_log_ptr,
                                     size_t in_log_len)
{
  int32_t ret = Object_OK;

  if (in_log_len == 0) {
    ALOGE("Received a log of length 0!");
    ret = ILoggerExampleCBO_ERROR_INPUT_BUFFER_ZERO_LENGTH;
    goto logger_cleanup;
  }

  if (in_log_ptr[in_log_len - 1] != '\0') {
    ALOGE("String was not zero-terminated!");
    ret = ILoggerExampleCBO_ERROR_INPUT_NOT_NULL_TERMINATED;
    goto logger_cleanup;
  }

  switch (severity_val) {
    case ILoggerExampleCBO_LOG_VERBOSE:
      ALOGV("%s", in_log_ptr);
      break;

    case ILoggerExampleCBO_LOG_INFO:
      ALOGI("%s", in_log_ptr);
      break;

    case ILoggerExampleCBO_LOG_DEBUG:
      ALOGD("%s", in_log_ptr);
      break;

    case ILoggerExampleCBO_LOG_WARN:
      ALOGW("%s", in_log_ptr);
      break;

    case ILoggerExampleCBO_LOG_ERROR:
      ALOGE("%s", in_log_ptr);
      break;

    default:
      ALOGE("ILoggerExampleCBO_log was passed an invalid log severity!");
      ret = ILoggerExampleCBO_ERROR_INPUT_INVALID_SEVERITY;
      goto logger_cleanup;
  }

  me->logCount++;

logger_cleanup:
  return ret;
}

static ILoggerExampleCBO_DEFINE_INVOKE(CLoggerExampleCBO_invoke,
                                       CLoggerExampleCBO_,
                                       CLoggerExampleCBO *);

int32_t CLoggerExampleCBO_open(Object *objOut)
{
  CLoggerExampleCBO *me = malloc(sizeof(CLoggerExampleCBO));
  if (!me) {
    ALOGE("Memory allocation for CLoggerExampleCBO failed!");
    return Object_ERROR_KMEM;
  }

  me->refs = 1;
  *objOut = (Object){CLoggerExampleCBO_invoke, me};
  return Object_OK;
}
