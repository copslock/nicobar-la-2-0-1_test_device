/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */


#ifndef _SMCI_EXAMPLE_H_
#define _SMCI_EXAMPLE_H_


#include <stdint.h>
#include <stddef.h>
#include "map_trusted_application.h"
#include "object.h"

/* For indexing our two TAs. */
enum ta_names { EXAMPLE_TA = 0, EXAMPLE_SERVICE_TA = 1 };

int32_t run_smcinvoke_ta_example(ta_image_data *img_data, Object serviceTAMemObj);

#endif // SMCI_EXAMPLE_H