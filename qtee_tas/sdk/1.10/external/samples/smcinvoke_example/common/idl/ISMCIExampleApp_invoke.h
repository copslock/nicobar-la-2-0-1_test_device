/***********************************************************
 Copyright (c) 2019 Qualcomm Technologies, Inc.
 All Rights Reserved.
 Confidential and Proprietary - Qualcomm Technologies, Inc.
************************************************************/
/**
 * Interface to the SMCInvoke Example App functionality.
 */
/** @cond */
#pragma once
// AUTOGENERATED FILE: DO NOT EDIT

#include <stdint.h>
#include "object.h"
#include "ILoggerExampleCBO.h"
#include "ISMCIExampleApp.h"

#define ISMCIExampleApp_DEFINE_INVOKE(func, prefix, type) \
  int32_t func(ObjectCxt h, ObjectOp op, ObjectArg *a, ObjectCounts k) \
  { \
    type me = (type) h; \
    switch (ObjectOp_methodID(op)) { \
      case Object_OP_release: { \
        return prefix##release(me); \
      } \
      case Object_OP_retain: { \
        return prefix##retain(me); \
      } \
      case ISMCIExampleApp_OP_serviceExample: { \
        return prefix##serviceExample(me); \
      } \
      case ISMCIExampleApp_OP_computeHash: { \
        if (k != ObjectCounts_pack(2, 1, 0, 0) || \
          a[1].b.size != 4) { \
          break; \
        } \
        const void *input_ptr = (const void*) a[0].b.ptr; \
        size_t input_len = a[0].b.size / 1; \
        const uint32_t *algorithm_ptr = (const uint32_t*) a[1].b.ptr; \
        void *hash_ptr = (void*) a[2].b.ptr; \
        size_t hash_len = a[2].b.size / 1; \
        int32_t r = prefix##computeHash(me, input_ptr, input_len, *algorithm_ptr, hash_ptr, hash_len, &hash_len); \
        a[2].b.size = hash_len * 1; \
        return r; \
      } \
      case ISMCIExampleApp_OP_setLoggerCBO: { \
        if (k != ObjectCounts_pack(0, 0, 1, 0)) { \
          break; \
        } \
        return prefix##setLoggerCBO(me, a[0].o); \
      } \
      case ISMCIExampleApp_OP_sharedMemoryExample: { \
        if (k != ObjectCounts_pack(0, 0, 1, 0)) { \
          break; \
        } \
        return prefix##sharedMemoryExample(me, a[0].o); \
      } \
    } \
    return Object_ERROR_INVALID; \
  }


