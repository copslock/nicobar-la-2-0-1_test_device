/***********************************************************
 Copyright (c) 2019 Qualcomm Technologies, Inc.
 All Rights Reserved.
 Confidential and Proprietary - Qualcomm Technologies, Inc.
************************************************************/

/**
 * Interface to the SMCInvoke Example App functionality.
 */
/** @cond */

include "ILoggerExampleCBO.idl"

interface ISMCIExampleApp
{
  /** @endcond */

  /**
   * @addtogroup ISMCIExampleApp
   * @{
   */

  /* Hash-related values to use in the ISMCIExampleApp_hashExample caller. */
  const uint32 HASH_SHA256_SIZE = 32;
  const uint32 HASH_SHA256 = 1;

  /* ISMCIExampleApp error code definitions. */
  error ERROR_LOGGER_CBO_NULL_ASSIGN; ///< Attempted to assign a NULL Object reference to the context.
  error ERROR_INPUT_BUFFER_TOO_SMALL; ///< Input buffer from callee is too small.

  /**
    Run the IAdder examples, which showcase how to use a service.

    @return
    Object_OK on success.
  */

  method serviceExample();

  /**
    Compute a secure hash inside the TA using an IHash object.

    @param[in]  plaintext   a plaintext buffer to be hashed
    @param[in]  algorithm   the hashing algorithm to use
    @param[out] hash        the hash of the input buffer

    @return
    Object_OK on success.
  */

  method computeHash(in buffer input, in uint32 algorithm, out buffer hash);

  /**
    Set an ILoggerExampleCBO object in this object's context.

    @param[in] logObj  an ILoggerExampleCBO object
  */

  method setLoggerCBO(in ILoggerExampleCBO logObj);

  /**
    Provide a memory object to the TA. The TA treats this memory object like a
    null-terminated string; printing it and then modifying it.

    @param[in] memObj  a memory object

    @return
    Object_OK on success.
  */

  method sharedMemoryExample(in interface memObj);

  /* @} */ /* end_addtogroup ISMCIExampleApp */
};
