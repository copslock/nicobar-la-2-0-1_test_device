/**
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
#pragma once

#include "object.h"
#include <stdint.h>

/* This function creates a new ISMCIExampleApp object. */

int32_t CSMCIExampleApp_open(Object *obj_out);
