/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include "CAdder.h"
#include "CAdder_open.h"
#include "object.h"
#include "qsee_log.h"
#include "qsee_ta_entry.h"
#include <stdint.h>

int32_t tz_module_open(uint32_t uid, Object cred, Object *objOut)
{
  (void) cred;

  qsee_log(QSEE_LOG_MSG_DEBUG, "SMCI Example Server tz_module_open() uid: %d", uid);

  switch (uid) {
    case CAdder_UID:
      return CAdder_open(objOut);

    default:
      break;
  }

  return Object_ERROR_INVALID;
}

/* tz_app_init() is called on application start.
   Any initialization required before the TA is ready to handle commands
   should be placed here. */
void tz_app_init(void)
{
  /* Get the current log mask */
  uint8_t log_mask = qsee_log_get_mask();

  /* Enable debug level logs */
  qsee_log_set_mask(log_mask | QSEE_LOG_MSG_DEBUG);
  qsee_log(QSEE_LOG_MSG_DEBUG, "App Start");
}

/* tz_app_shutdown() is called on application shutdown.
   Any deinitialization required before the TA is unloaded should be placed
   here. */
void tz_app_shutdown(void)
{
  qsee_log(QSEE_LOG_MSG_DEBUG, "App shutdown");
}

