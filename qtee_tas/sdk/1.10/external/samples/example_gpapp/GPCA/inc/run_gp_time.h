/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef RUN_GP_TIME_H
#define RUN_GP_TIME_H

/* ----------------------------------------------------------------------------
 *   Includes
 * ---------------------------------------------------------------------------- */
#include <stdint.h>

#include "tee_client_api.h"

/* ----------------------------------------------------------------------------
 *   Example time Operation(s)
 * ---------------------------------------------------------------------------- */
TEEC_Result run_tee_wait(void);
TEEC_Result run_tee_wait_cancel(void);

#endif /* RUN_GP_TIME_H */
