/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef RUN_GP_BASIC_H
#define RUN_GP_BASIC_H

/* ----------------------------------------------------------------------------
 *   Includes
 * ---------------------------------------------------------------------------- */
#include "tee_client_api.h"

/* ----------------------------------------------------------------------------
 *   Example basic Operation(s)
 * ---------------------------------------------------------------------------- */

TEEC_Result run_invoke_multiplyHLOSBuffer(void);

#endif /* RUN_GP_BASIC_H */
