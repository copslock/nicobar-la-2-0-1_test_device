/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "stringl.h"
#include "ca_paths.h"
#include "qtee_init.h"
#include "run_gp_basic.h"
#include "run_gp_time.h"
#include "gp_client_imp.h"
#include "tee_client_api.h"

/**
 * Example App UUID this must match to the UUID that TA defined in metadata.
 * You can find the corresponding TA metadata definition in 'GPTA/src/SConscript'.
 */
const TEEC_UUID example_gpapp_ta_uuid = {0x11111111,
                                         0x1111,
                                         0x1111,
                                         {0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11}};

/**
 * This function works in place of the UUID retrieval usually taken from the
 * gptauuid.xml file. It will need to be included in every GP CA.
 */
TEEC_Result getAppFromUUID(const TEEC_UUID *uuid, char *app_name, TEEC_uuid_dep_names *deps)
{
  (void) deps;

  if (memcmp(&example_gpapp_ta_uuid, uuid, sizeof(TEEC_UUID)) == 0) {
    strlcpy(app_name, EXAMPLE_GPAPP_TA_NAME, sizeof(EXAMPLE_GPAPP_TA_NAME));
    return TEEC_SUCCESS;
  } else {
    return TEEC_ERROR_GENERIC;
  }
}

int main(void)
{
  int error = 1;
  TEEC_Result result = TEEC_SUCCESS;
  bool test_failed = false;

  error = qtee_sdk_init();
  if (error) {
    printf("qtee_init() failed\n");
    // If this call fails no point to continue further so return the error
    return error;
  }

  result = run_invoke_multiplyHLOSBuffer();
  if (result != TEEC_SUCCESS) {
    printf("Error running multiply HLOS buffer example, ret = 0x%X.\n", result);
    test_failed = true;
  }

  result = run_tee_wait();
  if (result != TEEC_SUCCESS) {
    printf("Error running tee wait example, ret = 0x%X.\n", result);
    test_failed = true;
  }

  result = run_tee_wait_cancel();
  if (result == TEEC_ERROR_CANCEL) {
    printf("The TEE_Wait request has been cancelled, ret = 0x%X\n", result);
  } else if (result == TEEC_SUCCESS) {  // Regarded as fail as TEEC_ERROR_CANCEL is expected
    printf("Error timed out before it is cancelled, ret = 0x%X\n", result);
    test_failed = true;
  } else {
    printf("Error running tee wait cancel example failed, ret = 0x%X", result);
    test_failed = true;
  }

  if (test_failed) {
    error = 1;
  }

  qtee_sdk_deinit();
  return error;
}
