/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

/* ----------------------------------------------------------------------------
 *   Includes
 * ---------------------------------------------------------------------------- */
#include "common.h"
#include "gp_basic.h"
#include "gp_time.h"
#include "qsee_log.h"
#include "tee_internal_api.h"

/* ----------------------------------------------------------------------------
 *   Trusted Application Entry Points
 * ---------------------------------------------------------------------------- */

/**
 *  Function TA_CreateEntryPoint
 *  Description:
 *    The function TA_CreateEntryPoint is the Trusted Application's constructor,
 *    which the Framework calls when it creates a new instance of the Trusted Application.
 */
TEE_Result TA_EXPORT TA_CreateEntryPoint(void)
{
  qsee_log(QSEE_LOG_MSG_DEBUG, "TA_CreateEntryPoint");

  return TEE_SUCCESS;
}

/**
 *  Function TA_DestroyEntryPoint
 *  Description:
 *    The function TA_DestroyEntryPoint is the Trusted Application's destructor,
 *    which the Framework calls when the instance is being destroyed.
 */
void TA_EXPORT TA_DestroyEntryPoint(void)
{
  qsee_log(QSEE_LOG_MSG_DEBUG, "TA_DestroyEntryPoint");
}

/**
 *  Function TA_OpenSessionEntryPoint
 *  Description:
 *    The Framework calls the function TA_OpenSessionEntryPoint
 *    when a client requests to open a session with the Trusted Application.
 *    The open session request may result in a new Trusted Application instance
 *    being created.
 */
TEE_Result TA_EXPORT TA_OpenSessionEntryPoint(uint32_t nParamTypes,
                                              TEE_Param pParams[4],
                                              void **ppSessionContext)
{
  (void)nParamTypes;
  (void)pParams;
  (void)ppSessionContext;

  qsee_log(QSEE_LOG_MSG_DEBUG, "TA_OpenSessionEntryPoint");

  return TEE_SUCCESS;
}

/**
 *  Function TA_CloseSessionEntryPoint:
 *  Description:
 *    The Framework calls this function to close a client session.
 *    During the call to this function the implementation can use
 *    any session functions.
 */
void TA_EXPORT TA_CloseSessionEntryPoint(void *pSessionContext)
{
  (void)pSessionContext;

  qsee_log(QSEE_LOG_MSG_DEBUG, "TA_CloseSessionEntryPoint");
}

/**
 *  Function TA_InvokeCommandEntryPoint:
 *  Description:
 *    The Framework calls this function when the client invokes a command
 *    within the given session.
 */
TEE_Result TA_EXPORT TA_InvokeCommandEntryPoint(void *pSessionContext,
                                                uint32_t nCommandID,
                                                uint32_t nParamTypes,
                                                TEE_Param pParams[4])
{
  TEE_Result result = TEE_SUCCESS;

  switch (nCommandID) {
    case EXAMPLE_MULTIPLY_HLOS_BUFFER_CMD:
      result = CmdTEEMultiplyHLOSBuffer(pSessionContext, nParamTypes, pParams);
      break;

    case EXAMPLE_TEE_WAIT_CMD:
      result = CmdTEEWait(pSessionContext, nParamTypes, pParams);
      break;

    default:
      qsee_log(QSEE_LOG_MSG_ERROR, "Invalid command ID [0x%X]", nCommandID);
      result = TEE_ERROR_NOT_SUPPORTED;
  }

  return result;
}
