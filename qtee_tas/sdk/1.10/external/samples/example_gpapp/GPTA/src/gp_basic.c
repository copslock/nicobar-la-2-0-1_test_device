/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

/* ----------------------------------------------------------------------------
 *   Includes
 * ---------------------------------------------------------------------------- */
#include <stdint.h>

#include "tee_internal_api.h"
#include "qsee_log.h"
#include "gp_basic.h"

/* ----------------------------------------------------------------------------
 *   Example basic Operation(s)
 * ---------------------------------------------------------------------------- */
TEE_Result CmdTEEMultiplyHLOSBuffer(void *sessionContext,
                                    uint32_t paramTypes,
                                    TEE_Param params[MAX_GP_PARAMS])
{
  (void) sessionContext;

  int paramType = TEE_PARAM_TYPE_NONE;
  int retValue = TEE_SUCCESS;

  do {
    paramType = TEE_PARAM_TYPE_GET(paramTypes, 0);
    if (TEE_PARAM_TYPE_VALUE_INPUT != paramType && TEE_PARAM_TYPE_VALUE_INOUT != paramType) {
      retValue = TEE_ERROR_BAD_PARAMETERS;
      break;
    }

    paramType = TEE_PARAM_TYPE_GET(paramTypes, 1);
    if (TEE_PARAM_TYPE_MEMREF_INOUT != paramType) {
      retValue = TEE_ERROR_BAD_PARAMETERS;
      break;
    }

    if (params[1].memref.size > 0) {
      qsee_log(QSEE_LOG_MSG_DEBUG,
               "Initial value for params[1].memref.buffer = 0x%02x",
               *(uint8_t *)params[1].memref.buffer);

      // Multiply buffer with Value
      for (uint32_t cnt = 0; cnt < params[1].memref.size; cnt++) {
        *((uint8_t *)(params[1].memref.buffer) + cnt) *= params[0].value.a;
      }

      qsee_log(QSEE_LOG_MSG_DEBUG,
               "New value for params[1].memref.buffer = 0x%02x",
               *(uint8_t *)params[1].memref.buffer);
    } else {
      retValue = TEE_ERROR_BAD_PARAMETERS;
    }

  } while (0);

  return retValue;
}
