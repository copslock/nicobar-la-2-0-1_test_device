/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

/* ----------------------------------------------------------------------------
 *   Includes
 * ---------------------------------------------------------------------------- */
#include <stdint.h>

#include "tee_internal_api.h"
#include "qsee_log.h"
#include "gp_time.h"

/* ----------------------------------------------------------------------------
 *   Example time Operation(s)
 * ---------------------------------------------------------------------------- */
TEE_Result CmdTEEWait(void *pSessionContext, uint32_t nParamTypes, TEE_Param pParams[4])
{
  TEE_Result teeResult = TEE_SUCCESS;
  uint32_t waitTimeMs = 0;
  uint32_t cancellable = 0;

  (void)pSessionContext;


  if (TEE_PARAM_TYPE_GET(nParamTypes, 0) != TEE_PARAM_TYPE_VALUE_INPUT &&
      TEE_PARAM_TYPE_GET(nParamTypes, 1) != TEE_PARAM_TYPE_VALUE_INPUT) {
    qsee_log(QSEE_LOG_MSG_ERROR, "CmdTEEWait: invalid parameter types");
    return TEE_ERROR_BAD_PARAMETERS;
  }

  waitTimeMs = pParams[0].value.a;
  cancellable = pParams[1].value.a;

  if(cancellable)
  {
    TEE_UnmaskCancellation();
  }

  teeResult = TEE_Wait(waitTimeMs);

  if ((teeResult != TEE_ERROR_CANCEL) && (teeResult != TEE_SUCCESS)) {
    return TEE_ERROR_GENERIC;
  } else {
    return teeResult;
  }
}
