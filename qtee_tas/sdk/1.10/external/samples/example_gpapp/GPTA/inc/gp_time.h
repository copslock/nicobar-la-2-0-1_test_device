/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
#ifndef GP_TIME_H
#define GP_TIME_H

#include <stdint.h>

#include "tee_internal_api.h"

TEE_Result CmdTEEWait(void *pSessionContext, uint32_t nParamTypes, TEE_Param pParams[MAX_GP_PARAMS]);

#endif  // GP_TIME_H
