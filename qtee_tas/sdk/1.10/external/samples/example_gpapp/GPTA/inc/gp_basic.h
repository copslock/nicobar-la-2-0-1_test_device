/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
#ifndef GP_BASIC_H
#define GP_BASIC_H

/* ----------------------------------------------------------------------------
 *   Includes
 * ---------------------------------------------------------------------------- */
#include <stdint.h>

#include "tee_internal_api.h"

/* ----------------------------------------------------------------------------
 *   Example basic Operation(s)
 * ---------------------------------------------------------------------------- */
TEE_Result CmdTEEMultiplyHLOSBuffer(void *sessionContext,
                                    uint32_t paramTypes,
                                    TEE_Param params[MAX_GP_PARAMS]);

#endif  // GP_BASIC_H
