/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef COMMON_H
#define COMMON_H

// Basic example test CMD ID(s)
#define EXAMPLE_MULTIPLY_HLOS_BUFFER_CMD 0x00000001U

// Time example test CMD ID(s)
#define EXAMPLE_TEE_WAIT_CMD 0x00000011U

#endif /* COMMON_H */
