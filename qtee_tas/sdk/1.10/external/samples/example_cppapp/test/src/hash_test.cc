/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include "gtest/gtest.h"
#include "test_setup.h"

extern "C" {
#include "QSEEComAPI.h"
#include "cdefs.h"
#include "example_app.h"
#include "example_app_methods.h"
#include "hash.h"
#include "hash_init.h"
}

#define SHA1_HASHSIZE 160 / 8
#define SHA256_HASHSIZE 256 / 8
#define SHA384_HASHSIZE 384 / 8
#define SHA512_HASHSIZE 512 / 8

class HashTest : public TestSetup
{
 protected:
  example_app_req req;
  example_app_rsp rsp;

  hash_req hash_req;
  hash_rsp hash_rsp;
};

static const uint8_t sha1_msg[] = "testsha1";

static const uint8_t sha1_expected[SHA1_HASHSIZE] = {29, 238, 75,  26, 255, 178, 73,  188, 116, 194,
                                                     3,  121, 230, 1,  102, 54,  107, 113, 57,  79};

static const uint8_t sha256_msg[] = "testsha256";

static const uint8_t sha256_expected[SHA256_HASHSIZE] = {
  173, 108, 45,  145, 195, 188, 103, 114, 227, 18,  214, 61,  14,  5,   40,  81,
  133, 128, 131, 86,  133, 166, 83,  80,  61,  243, 129, 115, 115, 157, 101, 179};

static const uint8_t sha384_msg[] = "testsha384";

static const uint8_t sha384_expected[SHA384_HASHSIZE] = {
  178, 187, 116, 54,  252, 23, 124, 127, 183, 158, 27, 66,  66,  202, 10,  108,
  77,  21,  13,  183, 199, 27, 21,  79,  116, 119, 44, 195, 19,  233, 3,   132,
  87,  32,  217, 180, 155, 95, 131, 75,  176, 15,  6,  6,   177, 130, 239, 44};

static const uint8_t sha512_msg[] = "testsha512";

static const uint8_t sha512_expected[SHA512_HASHSIZE] = {
  155, 11,  235, 78,  230, 170, 19,  155, 25,  103, 78,  96,  135, 214, 172, 179,
  148, 243, 45,  160, 17,  235, 216, 226, 140, 56,  51,  87,  95,  69,  251, 240,
  50,  155, 103, 213, 156, 189, 254, 222, 80,  167, 190, 88,  65,  80,  113, 102,
  186, 127, 198, 51,  243, 189, 224, 94,  145, 246, 168, 249, 242, 151, 243, 20};

static void run_and_expect_success(struct example_app_req *req,
                                   struct example_app_rsp *rsp,
                                   struct hash_req *hash_req,
                                   struct hash_rsp *hash_rsp,
                                   const uint8_t *expected_result,
                                   uint32_t hash_size,
                                   QSEECom_handle *app_handle)
{
  int error;

  struct hash_rsp *hash_result;

  example_app_req_hash_init(req, hash_req);
  example_app_rsp_hash_init(rsp, hash_rsp);

  error = QSEECom_send_cmd(app_handle, req, sizeof(*req), rsp, sizeof(*rsp));

  ASSERT_EQ(error, QSEECOM_SUCCESS);

  hash_result = static_cast<struct hash_rsp *>(example_app_rsp_get_rsp_struct(rsp));

  EXPECT_NE(hash_result, nullptr);
  EXPECT_EQ(hash_result->status, HASH_SUCCESS);
  EXPECT_EQ(hash_result->hash_size, hash_size);

  bool correct = memcmp(hash_result->hash_result_buffer, expected_result, hash_size) == 0;

  EXPECT_TRUE(correct);
}

TEST_F(HashTest, SHA1)
{
  hash_req_init(&hash_req, QSEE_HASH, HASH_SHA1, sha1_msg, sizeof(sha1_msg) - 1);

  run_and_expect_success(
    &req, &rsp, &hash_req, &hash_rsp, sha1_expected, SHA1_HASHSIZE, app_handle);
}

TEST_F(HashTest, InvokeSHA1)
{
  hash_req_init(&hash_req, INVOKE_HASH, HASH_SHA1, sha1_msg, sizeof(sha1_msg) - 1);

  run_and_expect_success(
    &req, &rsp, &hash_req, &hash_rsp, sha1_expected, SHA1_HASHSIZE, app_handle);
}

TEST_F(HashTest, SHA256)
{
  hash_req_init(&hash_req, QSEE_HASH, HASH_SHA256, sha256_msg, sizeof(sha256_msg) - 1);

  run_and_expect_success(
    &req, &rsp, &hash_req, &hash_rsp, sha256_expected, SHA256_HASHSIZE, app_handle);
}

TEST_F(HashTest, InvokeSHA256)
{
  hash_req_init(&hash_req, INVOKE_HASH, HASH_SHA256, sha256_msg, sizeof(sha256_msg) - 1);

  run_and_expect_success(
    &req, &rsp, &hash_req, &hash_rsp, sha256_expected, SHA256_HASHSIZE, app_handle);
}

TEST_F(HashTest, SHA384)
{
  hash_req_init(&hash_req, QSEE_HASH, HASH_SHA384, sha384_msg, sizeof(sha384_msg) - 1);

  run_and_expect_success(
    &req, &rsp, &hash_req, &hash_rsp, sha384_expected, SHA384_HASHSIZE, app_handle);
}

TEST_F(HashTest, InvokeSHA384)
{
  hash_req_init(&hash_req, INVOKE_HASH, HASH_SHA384, sha384_msg, sizeof(sha384_msg) - 1);

  run_and_expect_success(
    &req, &rsp, &hash_req, &hash_rsp, sha384_expected, SHA384_HASHSIZE, app_handle);
}

TEST_F(HashTest, SHA512)
{
  hash_req_init(&hash_req, QSEE_HASH, HASH_SHA512, sha512_msg, sizeof(sha512_msg) - 1);

  run_and_expect_success(
    &req, &rsp, &hash_req, &hash_rsp, sha512_expected, SHA512_HASHSIZE, app_handle);
}

TEST_F(HashTest, InvokeSHA512)
{
  hash_req_init(&hash_req, INVOKE_HASH, HASH_SHA512, sha512_msg, sizeof(sha512_msg) - 1);

  run_and_expect_success(
    &req, &rsp, &hash_req, &hash_rsp, sha512_expected, SHA512_HASHSIZE, app_handle);
}
