/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include "gtest/gtest.h"
#include "test_setup.h"
#include <stdint.h>

extern "C" {
#include "QSEEComAPI.h"
#include "example_app.h"
#include "example_app_methods.h"
#include "timer.h"
#include "uptime_init.h"
}

class UptimeTest : public TestSetup
{
 protected:
  example_app_req req;
  example_app_rsp rsp;

  uptime_req uptime_req;
  uptime_rsp uptime_rsp;
};

static void run_and_expect_success(struct example_app_req *req,
                                   struct example_app_rsp *rsp,
                                   struct uptime_req *uptime_req,
                                   struct uptime_rsp *uptime_rsp,
                                   QSEECom_handle *app_handle)
{
  int error;

  struct uptime_rsp *uptime_result;

  example_app_req_uptime_init(req, uptime_req);
  example_app_rsp_uptime_init(rsp, uptime_rsp);

  error = QSEECom_send_cmd(app_handle, req, sizeof(*req), rsp, sizeof(*rsp));

  ASSERT_EQ(error, QSEECOM_SUCCESS);

  uptime_result = static_cast<struct uptime_rsp *>(example_app_rsp_get_rsp_struct(rsp));

  EXPECT_NE(uptime_result, nullptr);
  EXPECT_EQ(uptime_result->status, UPTIME_SUCCESS);
}

TEST_F(UptimeTest, GetUptimeMs)
{
  uptime_req_init(&uptime_req, UPTIME_CMD_MS);

  run_and_expect_success(&req, &rsp, &uptime_req, &uptime_rsp, app_handle);
}

TEST_F(UptimeTest, GetUptimeSec)
{
  uptime_req_init(&uptime_req, UPTIME_CMD_S);

  run_and_expect_success(&req, &rsp, &uptime_req, &uptime_rsp, app_handle);
}

TEST_F(UptimeTest, GetUptimeMin)
{
  uptime_req_init(&uptime_req, UPTIME_CMD_MIN);

  run_and_expect_success(&req, &rsp, &uptime_req, &uptime_rsp, app_handle);
}

TEST_F(UptimeTest, InvalidCmd)
{
  int error;

  struct uptime_rsp *uptime_result;

  const uint32_t invalid_cmd = 10;

  uptime_req_init(&uptime_req, invalid_cmd);

  example_app_req_uptime_init(&req, &uptime_req);
  example_app_rsp_uptime_init(&rsp, &uptime_rsp);

  error = QSEECom_send_cmd(app_handle, &req, sizeof(req), &rsp, sizeof(rsp));

  ASSERT_EQ(error, QSEECOM_SUCCESS);

  uptime_result = static_cast<struct uptime_rsp *>(example_app_rsp_get_rsp_struct(&rsp));

  EXPECT_NE(uptime_result, nullptr);
  EXPECT_NE(uptime_result->status, UPTIME_SUCCESS);
}
