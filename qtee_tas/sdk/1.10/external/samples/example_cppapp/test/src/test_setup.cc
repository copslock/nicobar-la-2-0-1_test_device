/*
 * Copyright (c) 2018-2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

extern "C" {
#include "QSEEComAPI.h"
#include "qtee_init.h"
}

#include "ca_paths.h"
#include "test_setup.h"

void TestSetup::SetUp()
{
  app_handle = nullptr;
  shared_buffer_len = 4096;

  int error;

  error = qtee_sdk_init();
  if (error) {
    FAIL();
  }

  error = QSEECom_start_app(&app_handle, TA_PATH, EXAMPLE_APP_TA_NAME, shared_buffer_len);
  if (error) {
    FAIL();
  }
}

void TestSetup::TearDown()
{
  QSEECom_shutdown_app(&app_handle);
  qtee_sdk_deinit();
}
