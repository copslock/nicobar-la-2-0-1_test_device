/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef TEST_SETUP_H
#define TEST_SETUP_H

#include "gtest/gtest.h"

extern "C" {
#include "QSEEComAPI.h"
}

class TestSetup : public ::testing::Test
{
 protected:
  void SetUp();
  void TearDown();

  QSEECom_handle *app_handle;
  uint32_t shared_buffer_len;
};

#endif /* TEST_SETUP_H */
