/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef RUN_UPTIME_H
#define RUN_UPTIME_H

#include "QSEEComAPI.h"

int run_uptime_ms(struct QSEECom_handle *app_handle);

int run_uptime_s(struct QSEECom_handle *app_handle);

int run_uptime_min(struct QSEECom_handle *app_handle);

#endif /* RUN_UPTIME_H */
