/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef RUN_CPPFEATURE_H
#define RUN_CPPFEATURE_H

#include "QSEEComAPI.h"

int run_cppFeature_test(struct QSEECom_handle *app_handle);

#endif /* RUN_UPTIME_H */
