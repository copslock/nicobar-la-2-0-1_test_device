/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef EXAMPLE_LISTENER_H
#define EXAMPLE_LISTENER_H

#define LISTENER_ID             1

/* Register and start the example listener service. */
int setup_listener(void);

/* Shutdown and unregister the example listener service. */
int cleanup_listener(void);

#endif /* EXAMPLE_LISTENER_H */
