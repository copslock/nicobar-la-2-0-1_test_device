/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef RUN_CIPHER_H
#define RUN_CIPHER_H

#include "QSEEComAPI.h"

int run_cipher(struct QSEECom_handle *app_handle);

#endif /* RUN_CIPHER_H */
