/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include "utils.h"

void print_hex(const uint8_t *msg, size_t length)
{
  for (size_t i = 0; i < length; i++) {
    printf("%02x", msg[i]);
  }
  printf("\n");
}
