/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include <stdio.h>

#include "example_app.h"
#include "example_app_methods.h"
#include "run_cppFeatures.h"
#include "cppFeature_init.h"
#include "cppFeature_def.h"

int run_cppFeature_test(struct QSEECom_handle *app_handle)
{
  int error;

  struct example_app_req req;
  struct example_app_rsp rsp;

  struct cppFeature_req cppFeature_req;
  struct cppFeature_rsp cppFeature_rsp;

  struct cppFeature_rsp *cppFeature_result;

  cppFeature_req_init(&cppFeature_req, CPPFEATURE_CMD_S);

  example_app_req_cppFeature_init(&req, &cppFeature_req);
  example_app_rsp_cppFeature_init(&rsp, &cppFeature_rsp);

  error = QSEECom_send_cmd(app_handle, &req, sizeof(req), &rsp, sizeof(rsp));

  if (error == QSEECOM_SUCCESS) {
    cppFeature_result = example_app_rsp_get_rsp_struct(&rsp);
    if (!cppFeature_result) {
      error = CPPFEATURE_FAILURE;
      goto out;
    }

    if (cppFeature_result->status == CPPFEATURE_SUCCESS) {
      printf("cppFeature: %llu s\n", cppFeature_result->cppFeature);
    } else {
      error = CPPFEATURE_FAILURE;
      printf("cppFeature request failed - error: %d\n", cppFeature_result->status);
    }
  }

out:
  return error ? CPPFEATURE_FAILURE : CPPFEATURE_SUCCESS;
}