/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include <stdio.h>

#include "example_app.h"
#include "example_app_methods.h"
#include "run_uptime.h"
#include "timer.h"
#include "uptime_init.h"

int run_uptime_ms(struct QSEECom_handle *app_handle)
{
  int error;

  struct example_app_req req;
  struct example_app_rsp rsp;

  struct uptime_req uptime_req;
  struct uptime_rsp uptime_rsp;

  struct uptime_rsp *uptime_result;

  uptime_req_init(&uptime_req, UPTIME_CMD_MS);

  example_app_req_uptime_init(&req, &uptime_req);
  example_app_rsp_uptime_init(&rsp, &uptime_rsp);

  error = QSEECom_send_cmd(app_handle, &req, sizeof(req), &rsp, sizeof(rsp));

  if (error == QSEECOM_SUCCESS) {
    uptime_result = example_app_rsp_get_rsp_struct(&rsp);
    if (!uptime_result) {
      error = UPTIME_FAILURE;
      goto out;
    }

    if (uptime_result->status == UPTIME_SUCCESS) {
      printf("Uptime: %llu ms\n", uptime_result->uptime);
    } else {
      error = UPTIME_FAILURE;
      printf("Uptime request failed - error: %d\n", uptime_result->status);
    }
  }

out:
  return error ? UPTIME_FAILURE : UPTIME_SUCCESS;
}

int run_uptime_s(struct QSEECom_handle *app_handle)
{
  int error;

  struct example_app_req req;
  struct example_app_rsp rsp;

  struct uptime_req uptime_req;
  struct uptime_rsp uptime_rsp;

  struct uptime_rsp *uptime_result;

  uptime_req_init(&uptime_req, UPTIME_CMD_S);

  example_app_req_uptime_init(&req, &uptime_req);
  example_app_rsp_uptime_init(&rsp, &uptime_rsp);

  error = QSEECom_send_cmd(app_handle, &req, sizeof(req), &rsp, sizeof(rsp));

  if (error == QSEECOM_SUCCESS) {
    uptime_result = example_app_rsp_get_rsp_struct(&rsp);
    if (!uptime_result) {
      error = UPTIME_FAILURE;
      goto out;
    }

    if (uptime_result->status == UPTIME_SUCCESS) {
      printf("Uptime: %llu s\n", uptime_result->uptime);
    } else {
      error = UPTIME_FAILURE;
      printf("Uptime request failed - error: %d\n", uptime_result->status);
    }
  }

out:
  return error ? UPTIME_FAILURE : UPTIME_SUCCESS;
}

int run_uptime_min(struct QSEECom_handle *app_handle)
{
  int error;

  struct example_app_req req;
  struct example_app_rsp rsp;

  struct uptime_req uptime_req;
  struct uptime_rsp uptime_rsp;

  struct uptime_rsp *uptime_result;

  uptime_req_init(&uptime_req, UPTIME_CMD_MIN);

  example_app_req_uptime_init(&req, &uptime_req);
  example_app_rsp_uptime_init(&rsp, &uptime_rsp);

  error = QSEECom_send_cmd(app_handle, &req, sizeof(req), &rsp, sizeof(rsp));

  if (error == QSEECOM_SUCCESS) {
    uptime_result = example_app_rsp_get_rsp_struct(&rsp);
    if (!uptime_result) {
      error = UPTIME_FAILURE;
      goto out;
    }

    if (uptime_result->status == UPTIME_SUCCESS) {
      printf("Uptime: %llu mins\n", uptime_result->uptime);
    } else {
      error = UPTIME_FAILURE;
      printf("Uptime request failed - error: %d\n", uptime_result->status);
    }
  }

out:
  return error ? UPTIME_FAILURE : UPTIME_SUCCESS;
}
