/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include <stdint.h>

#include "cppFeature_init.h"

void cppFeature_req_init(struct cppFeature_req *req, uint32_t cmd_id)
{
  req->cmd_id = cmd_id;
}
