/*
 * Copyright (c) 2018-2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include <stdint.h>

#include "hash.h"
#include "hash_init.h"
#include "stringl.h"

void hash_req_init(struct hash_req *req,
                   enum hash_cmd cmd,
                   enum hash_alg alg,
                   const uint8_t *msg,
                   uint32_t msg_len)
{
  req->cmd = cmd;
  req->alg = alg;

  if (cmd == QSEE_HASH_MODIFIED_CMD) {
    /* QSEECom_send_modified_cmd will set req->msg_ptr later. */
    req->msg_len = msg_len;
  } else {
    if (msg_len > HASH_MAX_MSG_LENGTH) {
      printf("Warning: Message to be hashed exceeds max message length and will be truncated.\n"
             "Message length = %zu\n"
             "Max message length = %zu\n",
             (size_t)msg_len,
             (size_t)HASH_MAX_MSG_LENGTH);
    }
    req->msg_len = memscpy(req->msg_buffer, HASH_MAX_MSG_LENGTH, msg, msg_len);
  }
}
