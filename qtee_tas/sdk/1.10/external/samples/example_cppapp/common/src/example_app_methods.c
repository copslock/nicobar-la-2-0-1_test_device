/*
 * Copyright (c) 2018-2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include "cipher.h"
#include "example_app.h"
#include "example_app_methods.h"
#include "hash.h"
#include "listener.h"
#include "timer.h"

void example_app_req_cipher_init(struct example_app_req *req, struct cipher_req *cipher_req)
{
  req->cmd_id = EXAMPLE_APP_CIPHER;
  req->cmd_req.cipher_req = *cipher_req;
}

void example_app_req_hash_init(struct example_app_req *req, struct hash_req *hash_req)
{
  req->cmd_id = EXAMPLE_APP_HASH;
  req->cmd_req.hash_req = *hash_req;
}

void example_app_req_listener_init(struct example_app_req *req,
                                   struct listener_sendcmd_req *listener_req)
{
  req->cmd_id = EXAMPLE_APP_LISTENER;
  req->cmd_req.listener_req = *listener_req;
}

void example_app_req_uptime_init(struct example_app_req *req, struct uptime_req *uptime_req)
{
  req->cmd_id = EXAMPLE_APP_UPTIME;
  req->cmd_req.uptime_req = *uptime_req;
}

void example_app_req_cppFeature_init(struct example_app_req *req, struct cppFeature_req *cppFeature_req)
{
  req->cmd_id = EXMPLE_APP_CPP_FEATURES;
  req->cmd_req.cppFeature_req = *cppFeature_req;
}

void example_app_rsp_cipher_init(struct example_app_rsp *rsp, struct cipher_rsp *cipher_rsp)
{
  rsp->cmd_id = EXAMPLE_APP_CIPHER;
  rsp->cmd_rsp.cipher_rsp = *cipher_rsp;
  rsp->cmd_rsp.cipher_rsp.status = CIPHER_FAILURE;
}

void example_app_rsp_hash_init(struct example_app_rsp *rsp, struct hash_rsp *hash_rsp)
{
  rsp->cmd_id = EXAMPLE_APP_HASH;
  rsp->cmd_rsp.hash_rsp = *hash_rsp;
  rsp->cmd_rsp.hash_rsp.status = HASH_FAILURE;
}

void example_app_rsp_listener_init(struct example_app_rsp *rsp,
                                   struct listener_sendcmd_rsp *listener_rsp)
{
  rsp->cmd_id = EXAMPLE_APP_LISTENER;
  rsp->cmd_rsp.listener_rsp = *listener_rsp;
  rsp->cmd_rsp.listener_rsp.status = LISTENER_FAILURE;
}

void example_app_rsp_uptime_init(struct example_app_rsp *rsp, struct uptime_rsp *uptime_rsp)
{
  rsp->cmd_id = EXAMPLE_APP_UPTIME;
  rsp->cmd_rsp.uptime_rsp = *uptime_rsp;
  rsp->cmd_rsp.uptime_rsp.status = UPTIME_FAILURE;
}

void example_app_rsp_cppFeature_init(struct example_app_rsp *rsp, struct cppFeature_rsp *cppFeature_rsp)
{
  rsp->cmd_id = EXMPLE_APP_CPP_FEATURES;
  rsp->cmd_rsp.cppFeature_rsp = *cppFeature_rsp;
  rsp->cmd_rsp.cppFeature_rsp.status = CPPFEATURE_FAILURE;
}

void *example_app_rsp_get_rsp_struct(struct example_app_rsp *rsp)
{
  switch (rsp->cmd_id) {
    case EXAMPLE_APP_CIPHER:
      return &rsp->cmd_rsp.cipher_rsp;

    case EXAMPLE_APP_HASH:
      return &rsp->cmd_rsp.hash_rsp;

    case EXAMPLE_APP_LISTENER:
      return &rsp->cmd_rsp.listener_rsp;

    case EXAMPLE_APP_UPTIME:
      return &rsp->cmd_rsp.uptime_rsp;

    case EXMPLE_APP_CPP_FEATURES:
      return &rsp->cmd_rsp.cppFeature_rsp;
  }

  return NULL;
}
