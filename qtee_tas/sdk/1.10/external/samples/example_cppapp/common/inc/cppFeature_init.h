/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef CPPFEATURE_INIT_H
#define CPPFEATURE_INIT_H

#include <stdint.h>
#include "cppFeature_def.h"

void cppFeature_req_init(struct cppFeature_req *req, uint32_t cmd_id);

#endif /* CPPFEATURE_INIT_H */
