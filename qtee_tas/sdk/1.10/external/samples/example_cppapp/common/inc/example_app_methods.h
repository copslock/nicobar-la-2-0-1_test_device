/*
 * Copyright (c) 2018-2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef EXAMPLE_APP_METHODS_H
#define EXAMPLE_APP_METHODS_H

#include "cipher.h"
#include "hash.h"
#include "listener.h"
#include "timer.h"
#include "cppFeature_def.h"

/*
 * The following methods initialize a example_app_req/rsp structure to a
 * specific command.
 *
 * Each method takes a specific request or response structure, which
 * the cmd_req or cmd_rsp members are then assigned to.
 *
 * For example, to initialize a example_app_req struct 'req' to a
 * cipher command request, first init a cipher_req structure with the desired
 * data, then call 'example_app_req_cipher_init(&req, &cipher_req)'. The request
 * structure 'req' can then be sent to the TA.
 */

void example_app_req_cipher_init(struct example_app_req *req, struct cipher_req *cipher_req);

void example_app_req_hash_init(struct example_app_req *req, struct hash_req *hash_req);

void example_app_req_listener_init(struct example_app_req *req,
                                   struct listener_sendcmd_req *listener_req);

void example_app_req_uptime_init(struct example_app_req *req, struct uptime_req *uptime_req);

void example_app_req_cppFeature_init(struct example_app_req *req, struct cppFeature_req *cppFeature_req);

void example_app_rsp_cipher_init(struct example_app_rsp *rsp, struct cipher_rsp *cipher_rsp);

void example_app_rsp_hash_init(struct example_app_rsp *rsp, struct hash_rsp *hash_rsp);

void example_app_rsp_listener_init(struct example_app_rsp *rsp,
                                   struct listener_sendcmd_rsp *listener_rsp);

void example_app_rsp_uptime_init(struct example_app_rsp *rsp, struct uptime_rsp *uptime_rsp);

void example_app_rsp_cppFeature_init(struct example_app_rsp *rsp, struct cppFeature_rsp *cppFeature_rsp);

/* Get the specific response structure from an example_app_rsp struct. */
void *example_app_rsp_get_rsp_struct(struct example_app_rsp *rsp);

#endif /* EXAMPLE_APP_METHODS_H */
