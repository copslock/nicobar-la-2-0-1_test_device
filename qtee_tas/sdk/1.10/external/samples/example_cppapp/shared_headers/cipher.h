/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef CIPHER_H
#define CIPHER_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define CIPHER_SUCCESS 0
#define CIPHER_FAILURE 1

#define CIPHER_MAX_MSG_LENGTH 200
#define CIPHER_MAX_RESULT_SIZE 208

#define CIPHER_BLOCK_SIZE 16

enum cipher_mode { CIPHER_MODE_ENCRYPT, CIPHER_MODE_DECRYPT };

enum cipher_alg { CIPHER_ALG_AES_128, CIPHER_ALG_AES_256 };

/*
 * Request structure.
 * Consists of a mode (encrypt or decrypt),
 * algorithm, message and a padded flag that can be set if it is known
 * that padding is needed. (For encryption the message length will be checked
 * anyway, but this option is helpful for decryption)
 *
 */
struct cipher_req {
  enum cipher_mode mode;
  enum cipher_alg alg;
  uint8_t msg[CIPHER_MAX_MSG_LENGTH];
  uint32_t msg_len;
  bool padded;
};

/*
 * Response structure.
 * Consists of a status, encryption result
 * and the size of the result.
 */
struct cipher_rsp {
  uint32_t status;
  uint8_t result[CIPHER_MAX_RESULT_SIZE];
  uint32_t result_size;
};

#endif /* CIPHER_H */
