/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef CPPFEATURE_DEF_H
#define CPPFEATURE_DEF_H

#include <stdint.h>

#define CPPFEATURE_CMD_S 1

#define CPPFEATURE_SUCCESS 0
#define CPPFEATURE_FAILURE 1

/*
 * Request structure.
 */
struct cppFeature_req {
  uint32_t cmd_id;
};

/*
 * Response structure.
 * Consists of a status and the uptime of the system.
 */
struct cppFeature_rsp {
  uint32_t status;
  unsigned long long cppFeature;
};

#endif /* CPPFEATURE_DEF_H */
