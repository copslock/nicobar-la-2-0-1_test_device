/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef TIMER_H
#define TIMER_H

#include <stdint.h>

#define UPTIME_CMD_MS 1
#define UPTIME_CMD_S 2
#define UPTIME_CMD_MIN 3

#define UPTIME_SUCCESS 0
#define UPTIME_FAILURE 1

/*
 * Request structure.
 */
struct uptime_req {
  uint32_t cmd_id;
};

/*
 * Response structure.
 * Consists of a status and the uptime of the system.
 */
struct uptime_rsp {
  uint32_t status;
  unsigned long long uptime;
};

#endif /* TIMER_H */
