/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef LISTENER_H
#define LISTENER_H

#include <stddef.h>
#include <stdint.h>

#define LISTENER_SUCCESS 0
#define LISTENER_FAILURE 1

enum listener_cmd {
  LISTENER_CMD_1,
  LISTENER_CMD_2,
  LISTENER_CMD_3_MODIFIED_RSP
};

/* Listener request to be sent to the CA from the TA. */
struct listener_req {
  enum listener_cmd cmd_id;
  int32_t arg1;
  int32_t arg2;
} __attribute__((packed));

/* Listener response to be received by the TA from the CA. */
struct listener_rsp {
  int32_t result1;
  int32_t result2;
  uint8_t *shared_buffer;
  size_t shared_buffer_size;
} __attribute__((packed));

/* Request structure to be sent with QSEECom_send_cmd. */
struct listener_sendcmd_req {
  uint32_t listener_id;
};

/* Response structure to be received from QSEECom_send_cmd. */
struct listener_sendcmd_rsp {
  uint32_t status;
};

#endif /* LISTENER_H */
