/*
 * Copyright (c) 2018-2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef HASH_H
#define HASH_H

#include <stddef.h>
#include <stdint.h>

#define HASH_SUCCESS 0
#define HASH_FAILURE 1
#define INVOKE_HASH_FAILURE 2

#define HASH_MAX_MSG_LENGTH 200
#define HASH_MAX_HASH_SIZE 64

enum hash_cmd { QSEE_HASH, QSEE_HASH_MODIFIED_CMD, INVOKE_HASH };

enum hash_alg { HASH_SHA1, HASH_SHA256, HASH_SHA384, HASH_SHA512 };

/*
 * Request structure.
 * Consists of a cmd, hashing algorithm, and a message to be hashed.
 *
 * The request struct also holds a pointer to the hash output which is used by the
 * QSEE_HASH_MODIFIED_CMD command. In the other cases this member is unused.
 *
 * N.B.: These pointers can only point to (simulated or real) ion buffers. QTEE cannot access
 * other CA memory.
 */
struct hash_req {
  enum hash_cmd cmd;
  enum hash_alg alg;
  union {
    uint8_t msg_buffer[HASH_MAX_MSG_LENGTH];
    uint8_t *msg_ptr;
  };
  uint32_t msg_len;
  uint8_t *hash_result_ptr;
};

/*
 * Response structure.
 * Consists of a status and the hash result.
 *
 * hash_result_buffer is unused when running QSEE_HASH_MODIFIED_CMD because the result is stored
 * in the shared buffer pointed to by hash_result_ptr in the request struct.
 */
struct hash_rsp {
  uint32_t status;
  uint8_t hash_result_buffer[HASH_MAX_HASH_SIZE];
  uint32_t hash_size;
};

#endif /* HASH_H */
