# Copyright (c) 2019 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.

import os

# First, call into the QTEE SDK to get an Environment with all the tools
try:
  # Either set $QTEE_SDK in the OS environment or pass
  # QTEE_SDK=/path/to/SDK as a SCons commandline argument
  # In this example, the SCons argument takes priority
  sdk_env_name = 'QTEE_SDK'
  sdk_dir = ARGUMENTS.get(sdk_env_name, os.environ.get(sdk_env_name))

  print 'Initialising QTEE SDK..',
  # Call the QTEE SDK SConscript to get a QTEE SDK SCons Environment
  qtee_sdk_env = SConscript(os.path.join(sdk_dir,'SConstruct'))

except:
  print '\nUnable to initialize QTEE SDK at ${}: {} '.format(sdk_env_name, sdk_dir)

  #re-raise the exception
  raise
  Exit(1)

# Initialize the environment for the target architecture
for arch in ['aarch32', 'aarch64']:
  env = qtee_sdk_env.Clone()

  # Initialize the environment for the target architecture
  env.InitArch(arch)

  out_dir = os.path.join(Dir('.').srcnode().abspath, 'out', env['PROC'])
  env.Replace(OUT_DIR=out_dir)
  env.Replace(LIB_OUT_DIR=out_dir)

  # Call the TA and common SConscripts
  env.SConscript('./TA/src/SConscript', exports='env')
  env.SConscript('./common/src/SConscript', exports='env')

  # Call the CA and test SConscripts if building off-target
  if env.OfftargetTesting():
    env.SConscript('./CA/src/SConscript', exports='env')
    env.SConscript('./test/src/SConscript', exports='env')
