/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef LISTENER_HANDLER_H
#define LISTENER_HANDLER_H

extern "C"{
#include "listener.h"
}

void listener_handle_cmd(const struct listener_sendcmd_req *req,
                         struct listener_sendcmd_rsp *rsp);

#endif /* LISTENER_HANDLER_H */
