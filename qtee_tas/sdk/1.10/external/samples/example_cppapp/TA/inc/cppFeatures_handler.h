/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef CPPFEATURES_HANDLER_H
#define CPPFEATURES_HANDLER_H

#include "cppFeature_def.h"

void cppFeatures_handle_cmd(const struct cppFeature_req *req, struct cppFeature_rsp *rsp);

#endif /* UPTIME_HANDLER_H */