/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef HASH_HANDLER_H
#define HASH_HANDLER_H

extern "C"{
#include "hash.h"
}

void hash_handle_cmd(const struct hash_req *req, struct hash_rsp *rsp);

#endif /* HASH_HANDLER_H */
