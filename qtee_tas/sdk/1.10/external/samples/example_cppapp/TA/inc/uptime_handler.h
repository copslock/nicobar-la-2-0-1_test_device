/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef UPTIME_HANDLER_H
#define UPTIME_HANDLER_H

extern "C"{
#include "timer.h"
}

void uptime_handle_cmd(const struct uptime_req *req, struct uptime_rsp *rsp);

#endif /* UPTIME_HANDLER_H */