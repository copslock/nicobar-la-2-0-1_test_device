/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

extern "C"{
#include "uptime_handler.h"
#include "qsee_log.h"
#include "qsee_timer.h"
}

void uptime_handle_cmd(const struct uptime_req *req, struct uptime_rsp *rsp)
{
  switch (req->cmd_id) {
    case UPTIME_CMD_MS:
      rsp->uptime = qsee_get_uptime();
      break;

    case UPTIME_CMD_S:
      rsp->uptime = (qsee_get_uptime() / 1000);
      break;

    case UPTIME_CMD_MIN:
      rsp->uptime = ((qsee_get_uptime() / 1000) / 60);
      break;

    default:
      qsee_log(QSEE_LOG_MSG_ERROR, "Invalid uptime cmd");
      rsp->status = UPTIME_FAILURE;
      return;
  }

  rsp->status = UPTIME_SUCCESS;
}