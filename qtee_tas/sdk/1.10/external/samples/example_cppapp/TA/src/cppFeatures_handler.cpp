/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include "cppFeatures_handler.h"
#include "cppFeatures.h"
extern "C"{
#include "qsee_log.h"
#include "qsee_timer.h"
}

void cppFeatures_handle_cmd(const struct cppFeature_req *req, struct cppFeature_rsp *rsp)
{
  switch (req->cmd_id) {
    case CPPFEATURE_CMD_S:
      rsp->cppFeature = cppFeatures();
      break;

    default:
      qsee_log(QSEE_LOG_MSG_ERROR, "Invalid uptime cmd");
      rsp->status = CPPFEATURE_FAILURE;
      return;
  }

  rsp->status = CPPFEATURE_SUCCESS;
}