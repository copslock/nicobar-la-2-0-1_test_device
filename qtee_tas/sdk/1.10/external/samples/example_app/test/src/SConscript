# Copyright (c) 2018 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.

import os
Import('env')
env = env.Clone()

includes = [
  '../inc/',
  '../../common/inc',
  '../../shared_headers'
]

sources = [
  'cipher_test.cc',
  'gtest_main.cc',
  'hash_test.cc',
  'test_setup.cc',
  'uptime_test.cc',
]

# If standalone the user should get gtest sources themselves and copy googletest dir into 'test/src'
if env.StandaloneSdk():
  sources.append('googletest/src/gtest-all.cc')
  env.Append(CPPPATH=['./googletest'])
  env.Append(CPPPATH=['./googletest/include'])

else:
  paths = [
    '${BUILD_ROOT}/ssg/bsp/unittests/gtest_master/googlemock/include',
    '${BUILD_ROOT}/ssg/bsp/unittests/gtest_master/googletest/include',
    '${BUILD_ROOT}/ssg/bsp/unittests/gtest_master/googletest',
    '${BUILD_ROOT}/ssg/bsp/unittests/gtest_master/googlemock',
    ]
  env.Append(CPPPATH=paths)
  sources.extend(['${BUILD_ROOT}/ssg/bsp/unittests/gtest_master/googletest/src/gtest-all.cc',
                  '${BUILD_ROOT}/ssg/bsp/unittests/gtest_master/googlemock/src/gmock-all.cc',])


example_app_ta_name = \
  'example_app_ta' + ['32', '64'][env.Is64BitImage()]

env.Append(CPPDEFINES = \
           'EXAMPLE_APP_TA_NAME=\\"{}\\"'.format(example_app_ta_name))

examplelib = env['LIB_OUT_DIR'] + '/example_lib.lib'

app = env.OfftargetClientAppBuilder(
  includes = includes,
  sources = sources,
  image = 'example_app_test',
  user_libs = [examplelib], 
  no_stupid_headers = 'true'
)

env.Alias('example_app_test', app)

env.Depends(app, env.Alias('example_app_ta'))
env.Depends(app, env.Alias('example_lib'))

Return('app')
