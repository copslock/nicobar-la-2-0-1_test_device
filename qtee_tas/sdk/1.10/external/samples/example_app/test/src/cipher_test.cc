/*
 * Copyright (c) 2018 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include "gtest/gtest.h"
#include "test_setup.h"

extern "C" {
#include "QSEEComAPI.h"
#include "cdefs.h"
#include "cipher.h"
#include "cipher_init.h"
#include "example_app.h"
#include "example_app_methods.h"
}

class CipherTest : public TestSetup
{
 protected:
  example_app_req req;
  example_app_rsp rsp;

  cipher_req cipher_req;
  cipher_rsp cipher_rsp;
};

static void run_and_expect_success(struct example_app_req *req,
                                   struct example_app_rsp *rsp,
                                   struct cipher_req *cipher_req,
                                   struct cipher_rsp *cipher_rsp,
                                   const uint8_t *msg,
                                   const uint32_t msg_len,
                                   enum cipher_alg alg,
                                   QSEECom_handle *app_handle)
{
  int error;

  struct cipher_rsp *cipher_result;

  cipher_req_init(cipher_req, CIPHER_MODE_ENCRYPT, alg, msg, msg_len);

  example_app_req_cipher_init(req, cipher_req);
  example_app_rsp_cipher_init(rsp, cipher_rsp);

  /* Encrypt */
  error = QSEECom_send_cmd(app_handle, req, sizeof(*req), rsp, sizeof(*rsp));

  ASSERT_EQ(error, QSEECOM_SUCCESS);

  cipher_result = static_cast<struct cipher_rsp *>(example_app_rsp_get_rsp_struct(rsp));

  EXPECT_NE(cipher_result, nullptr);
  EXPECT_EQ(cipher_result->status, CIPHER_SUCCESS);

  /* Re-init request for decryption using the result as the message. */
  bool msg_was_padded = cipher_req->padded;

  cipher_req_init(
    cipher_req, CIPHER_MODE_DECRYPT, alg, cipher_result->result, cipher_result->result_size);

  cipher_req->padded = msg_was_padded;

  example_app_req_cipher_init(req, cipher_req);
  example_app_rsp_cipher_init(rsp, cipher_rsp);

  /* Decrypt */
  error = QSEECom_send_cmd(app_handle, req, sizeof(*req), rsp, sizeof(*rsp));

  ASSERT_EQ(error, QSEECOM_SUCCESS);

  cipher_result = static_cast<struct cipher_rsp *>(example_app_rsp_get_rsp_struct(rsp));

  EXPECT_NE(cipher_result, nullptr);
  EXPECT_EQ(cipher_result->status, CIPHER_SUCCESS);

  /* Check there is a match */
  EXPECT_EQ(cipher_result->result_size, msg_len);
  bool correct = memcmp(cipher_result->result, msg, msg_len) == 0;
  EXPECT_TRUE(correct);
}

TEST_F(CipherTest, CipherInitNoPad)
{
  const uint8_t msg[] = "testmessageblock";
  const uint32_t msg_len = sizeof(msg) - 1;

  cipher_req_init(&cipher_req, CIPHER_MODE_ENCRYPT, CIPHER_ALG_AES_128, msg, msg_len);

  EXPECT_EQ(cipher_req.msg_len, msg_len);

  bool correct = memcmp(cipher_req.msg, msg, msg_len) == 0;
  EXPECT_TRUE(correct);

  EXPECT_FALSE(cipher_req.padded);
}

TEST_F(CipherTest, CipherInitPad)
{
  const uint8_t msg[] = "testmessage";
  const uint32_t msg_len = sizeof(msg) - 1;

  cipher_req_init(&cipher_req, CIPHER_MODE_ENCRYPT, CIPHER_ALG_AES_128, msg, msg_len);

  EXPECT_EQ(cipher_req.msg_len, msg_len);

  bool correct = memcmp(cipher_req.msg, msg, msg_len) == 0;
  EXPECT_TRUE(correct);

  EXPECT_TRUE(cipher_req.padded);
}

TEST_F(CipherTest, SmallMsgAES128)
{
  const uint8_t msg[] = "testmessage";
  const uint32_t msg_len = sizeof(msg) - 1;

  run_and_expect_success(
    &req, &rsp, &cipher_req, &cipher_rsp, msg, msg_len, CIPHER_ALG_AES_128, app_handle);
}

TEST_F(CipherTest, EncryptBlocksizedMsgAES128)
{
  uint8_t msg[] = "testmessageblock";
  const uint32_t msg_len = sizeof(msg) - 1;

  run_and_expect_success(
    &req, &rsp, &cipher_req, &cipher_rsp, msg, msg_len, CIPHER_ALG_AES_128, app_handle);
}

TEST_F(CipherTest, EncryptLargerMsgAES128)
{
  uint8_t msg[] = "abcdefghijklmnopqrstuvwxyz";
  const uint32_t msg_len = sizeof(msg) - 1;

  run_and_expect_success(
    &req, &rsp, &cipher_req, &cipher_rsp, msg, msg_len, CIPHER_ALG_AES_128, app_handle);
}

TEST_F(CipherTest, EncryptSmallMsgAES256)
{
  uint8_t msg[] = "testmessage";
  const uint32_t msg_len = sizeof(msg) - 1;

  run_and_expect_success(
    &req, &rsp, &cipher_req, &cipher_rsp, msg, msg_len, CIPHER_ALG_AES_256, app_handle);
}

TEST_F(CipherTest, EncryptBlocksizedMsgAES256)
{
  uint8_t msg[] = "testmessageblock";
  const uint32_t msg_len = sizeof(msg) - 1;

  run_and_expect_success(
    &req, &rsp, &cipher_req, &cipher_rsp, msg, msg_len, CIPHER_ALG_AES_256, app_handle);
}

TEST_F(CipherTest, EncryptLargerMsgAES256)
{
  uint8_t msg[] = "abcdefghijklmnopqrstuvwxyz";
  const uint32_t msg_len = sizeof(msg) - 1;

  run_and_expect_success(
    &req, &rsp, &cipher_req, &cipher_rsp, msg, msg_len, CIPHER_ALG_AES_256, app_handle);
}
