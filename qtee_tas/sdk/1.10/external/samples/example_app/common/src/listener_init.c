/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include <stddef.h>
#include <stdint.h>

#include "listener.h"
#include "listener_init.h"

void listener_req_init(struct listener_sendcmd_req *req, uint32_t id)
{
  req->listener_id = id;
}
