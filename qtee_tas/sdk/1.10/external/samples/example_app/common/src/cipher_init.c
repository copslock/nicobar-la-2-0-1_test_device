/*
 * Copyright (c) 2018 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include <stdint.h>

#include "cipher.h"
#include "cipher_init.h"
#include "stringl.h"

static bool cipher_padding_required(uint32_t msg_len)
{
  return (msg_len % CIPHER_BLOCK_SIZE) != 0;
}

void cipher_req_init(struct cipher_req *req,
                     enum cipher_mode mode,
                     enum cipher_alg alg,
                     const uint8_t *msg,
                     uint32_t msg_len)
{
  req->mode = mode;
  req->alg = alg;
  req->msg_len = memscpy(req->msg, CIPHER_MAX_MSG_LENGTH, msg, msg_len);
  req->padded = cipher_padding_required(req->msg_len);
}
