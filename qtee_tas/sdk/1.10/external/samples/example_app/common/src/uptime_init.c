/*
 * Copyright (c) 2018 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include <stdint.h>

#include "timer.h"
#include "uptime_init.h"

void uptime_req_init(struct uptime_req *req, uint32_t cmd_id)
{
  req->cmd_id = cmd_id;
}
