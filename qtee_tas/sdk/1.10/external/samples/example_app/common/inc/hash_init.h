/*
 * Copyright (c) 2018 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef HASH_INIT_H
#define HASH_INIT_H

#include <stdint.h>

#include "hash.h"

void hash_req_init(struct hash_req *req,
                   enum hash_cmd cmd,
                   enum hash_alg alg,
                   const uint8_t *msg,
                   uint32_t msg_len);

#endif /* HASH_INIT_H */
