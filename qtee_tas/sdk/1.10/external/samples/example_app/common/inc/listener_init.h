/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef LISTENER_INIT_H
#define LISTENER_INIT_H

#include <stddef.h>
#include <stdint.h>

#include "listener.h"

void listener_req_init(struct listener_sendcmd_req *req, uint32_t id);

#endif /* LISTENER_INIT_H */
