/*
 * Copyright (c) 2018 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef CIPHER_INIT_H
#define CIPHER_INIT_H

#include <stdint.h>

#include "cipher.h"

void cipher_req_init(struct cipher_req *req,
                     enum cipher_mode mode,
                     enum cipher_alg alg,
                     const uint8_t *msg,
                     uint32_t msg_len);

#endif /* CIPHER_INIT_H */
