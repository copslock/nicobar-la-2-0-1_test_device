/*
 * Copyright (c) 2018 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef UPTIME_INIT_H
#define UPTIME_INIT_H

#include <stdint.h>

#include "timer.h"

void uptime_req_init(struct uptime_req *req, uint32_t cmd_id);

#endif /* UPTIME_INIT_H */
