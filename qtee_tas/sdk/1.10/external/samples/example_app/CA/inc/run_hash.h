/*
 * Copyright (c) 2018-2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef RUN_HASH_H
#define RUN_HASH_H

#include "QSEEComAPI.h"

/* Run hash example using the qsee API. */
int run_qsee_hash(struct QSEECom_handle *app_handle);

/* Run qsee hash example via QSEECom_send_modified_cmd. */
int run_qsee_hash_modified_cmd(struct QSEECom_handle *app_handle);

/* Run hash example using the invoke API. */
int run_invoke_hash(struct QSEECom_handle *app_handle);

#endif /* RUN_HASH_H */
