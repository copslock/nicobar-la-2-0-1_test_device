/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef RUN_LISTENER_H
#define RUN_LISTENER_H

#include "QSEEComAPI.h"

int run_listener(struct QSEECom_handle *app_handle);

#endif /* RUN_LISTENER_H */
