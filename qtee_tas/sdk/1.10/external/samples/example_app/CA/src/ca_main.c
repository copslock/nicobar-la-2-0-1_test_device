/*
 * Copyright (c) 2018-2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "QSEEComAPI.h"
#include "ca_paths.h"
#include "example_app.h"
#include "qtee_init.h"
#include "run_cipher.h"
#include "run_hash.h"
#include "run_listener.h"
#include "run_uptime.h"

int main(void)
{
  struct QSEECom_handle *app_handle = NULL;
  static char const ta_name[] = EXAMPLE_APP_TA_NAME;

  const uint32_t shared_buffer_len = 4096;

  int error = 1;
  bool test_failed = false;

  error = qtee_sdk_init();
  if (error) {
    printf("qtee_init() failed\n");
    return error;
  }

  /* Load the TA */
  error = QSEECom_start_app(&app_handle, TA_PATH, ta_name, shared_buffer_len);

  if (error != QSEECOM_SUCCESS) {
    printf("QSEECom_start_app() failed\n");
    goto out;
  }

  /* Run examples */
  printf("Running cipher example:\n");
  error = run_cipher(app_handle);
  if (error) {
    printf("Error running cipher example.\n");
    test_failed = true;
  }

  printf("\nRunning hash examples:\n");
  error = run_qsee_hash(app_handle);
  if (error) {
    printf("Error running qsee_hash example.\n");
    test_failed = true;
  }

  error = run_qsee_hash_modified_cmd(app_handle);
  if (error) {
    printf("Error running qsee_hash example.\n");
    test_failed = true;
  }

  error = run_invoke_hash(app_handle);
  if (error) {
    printf("Error running invoke_hash example.\n");
    test_failed = true;
  }

  printf("\nRunning listener example:\n");
  error = run_listener(app_handle);
  if (error) {
    printf("Error running listener example.\n");
    test_failed = true;
  }

  printf("\nRunning uptime examples:\n");
  error = run_uptime_ms(app_handle);
  if (error) {
    printf("Error running uptime_ms example.\n");
    test_failed = true;
  }

  error = run_uptime_s(app_handle);
  if (error) {
    printf("Error running uptime_s example.\n");
    test_failed = true;
  }

  error = run_uptime_min(app_handle);
  if (error) {
    printf("Error running uptime_min example.\n");
    test_failed = true;
  }

  if (test_failed) {
    error = 1;
  }

  QSEECom_shutdown_app(&app_handle);

out:
  qtee_sdk_deinit();
  return error;
}
