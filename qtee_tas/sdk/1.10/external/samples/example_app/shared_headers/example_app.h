/*
 * Copyright (c) 2018-2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef EXAMPLE_APP_H
#define EXAMPLE_APP_H

#include "cipher.h"
#include "hash.h"
#include "listener.h"
#include "timer.h"

enum example_app_cmd_id {
  EXAMPLE_APP_CIPHER,
  EXAMPLE_APP_HASH,
  EXAMPLE_APP_LISTENER,
  EXAMPLE_APP_UPTIME
};

union example_app_cmd_req {
  struct cipher_req cipher_req;
  struct hash_req hash_req;
  struct listener_sendcmd_req listener_req;
  struct uptime_req uptime_req;
};

/*
 * Main request structure:
 *
 * Consists of a union that holds request structures that are specific
 * to a certain command.
 *
 * The cmd_id enum is used to identify the command, and therefore the
 * request structure that is required.
 */
struct example_app_req {
  enum example_app_cmd_id cmd_id;
  union example_app_cmd_req cmd_req;
};

union example_app_cmd_rsp {
  struct cipher_rsp cipher_rsp;
  struct hash_rsp hash_rsp;
  struct listener_sendcmd_rsp listener_rsp;
  struct uptime_rsp uptime_rsp;
};

/*
 * Main response structure:
 *
 * Consists of a union that holds response structures that are specific
 * to a certain command.
 *
 * The cmd_id enum is used to identify the command, and therefore the
 * response structure that is required.
 */
struct example_app_rsp {
  enum example_app_cmd_id cmd_id;
  union example_app_cmd_rsp cmd_rsp;
};

#endif /* EXAMPLE_APP_H */
