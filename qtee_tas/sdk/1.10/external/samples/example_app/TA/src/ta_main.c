/*
 * Copyright (c) 2018-2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include <stdint.h>

#include "cipher_handler.h"
#include "example_app.h"
#include "hash_handler.h"
#include "listener_handler.h"
#include "qsee_core.h"
#include "qsee_log.h"
#include "qsee_ta_entry.h"
#include "uptime_handler.h"

void tz_app_init(void)
{
  /* Get the current log mask */
  uint8_t log_mask = qsee_log_get_mask();

  /* Enable debug level logs */
  qsee_log_set_mask(log_mask | QSEE_LOG_MSG_DEBUG);
  qsee_log(QSEE_LOG_MSG_DEBUG, "App Start");
}

void tz_app_shutdown(void)
{
  qsee_log(QSEE_LOG_MSG_DEBUG, "App shutdown");
}

void tz_app_cmd_handler(void *cmd_ptr, uint32_t cmdlen, void *rsp_ptr, uint32_t rsplen)
{
  const struct example_app_req *req = (struct example_app_req *)cmd_ptr;
  struct example_app_rsp *rsp = (struct example_app_rsp *)rsp_ptr;

  if (!cmd_ptr || !rsp_ptr) {
    qsee_log(QSEE_LOG_MSG_ERROR, "Invalid input pointers");
    return;
  }

  if (sizeof(struct example_app_rsp) > rsplen) {
    qsee_log(QSEE_LOG_MSG_ERROR, "Insufficient response length");
    return;
  }

  if (sizeof(struct example_app_req) > cmdlen) {
    qsee_log(QSEE_LOG_MSG_ERROR, "Insufficent command length");
    return;
  }

  if (!(qsee_is_ns_range(cmd_ptr, cmdlen) && qsee_is_ns_range(rsp_ptr, rsplen))) {
    qsee_log(QSEE_LOG_MSG_ERROR, "cmd/rsp buffers are in invalid memory");
    return;
  }

  if (req->cmd_id != rsp->cmd_id) {
    qsee_log(QSEE_LOG_MSG_ERROR, "req/rsp structs have different ids");
    return;
  }

  switch (req->cmd_id) {
    case EXAMPLE_APP_CIPHER:
      cipher_handle_cmd(&req->cmd_req.cipher_req, &rsp->cmd_rsp.cipher_rsp);
      break;

    case EXAMPLE_APP_HASH:
      hash_handle_cmd(&req->cmd_req.hash_req, &rsp->cmd_rsp.hash_rsp);
      break;

    case EXAMPLE_APP_LISTENER:
      listener_handle_cmd(&req->cmd_req.listener_req, &rsp->cmd_rsp.listener_rsp);
      break;

    case EXAMPLE_APP_UPTIME:
      uptime_handle_cmd(&req->cmd_req.uptime_req, &rsp->cmd_rsp.uptime_rsp);
      break;

    default:
      break;
  }
}
