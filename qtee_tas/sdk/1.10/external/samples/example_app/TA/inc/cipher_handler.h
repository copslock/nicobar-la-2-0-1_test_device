/*
 * Copyright (c) 2018 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef CIPHER_HANDLER_H
#define CIPHER_HANDLER_H

#include "cipher.h"

void cipher_handle_cmd(const struct cipher_req *req, struct cipher_rsp *rsp);

#endif /* CIPHER_HANDLER_H */
