#  Copyright (c) 2019 QUALCOMM Technologies Incorporated.
#  All Rights Reserved.
#  Qualcomm Confidential and Proprietary

import sys

def exists(env):
  return True
  
def generate(env):
  
  #this switch allows us to pull .mbn files that were compiled by the standalone SDK and put them in uefitzt
  #for standalone sdk verification purposes
  env.AddCommandlineArgument(env.BoolVariable('UEFITZT_SDK_SHIM','shim to build uefitzt with sdk compiled artifacts',0))
  env.AddCommandlineArgument(env.BoolVariable('OFF_TARGET','OFF_TARGET',0))
  if env['OFF_TARGET'] and  sys.platform.startswith('win'):
    raise ValueError("Cannot do offtarget testing on windows targets!")
