/** @file  ISwFuse.idl */

// Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
// Qualcomm Technologies Proprietary and Confidential.

/** @cond */
interface ISwFuse {
  /** @endcond */

  /**
   * @addtogroup ISwFuse
   * @{
   */

  /** @cond */
  const int32 ID_HLOS_IMG_TAMPER = 0;
  /**< Used during boot to determine
       if the HLOS image has successfully
       been authenticated. */

  const int32 ID_WINSECAPP_LOADED = 1;
  /**< Used by WinSec App to prevent reloading. */

  const int32 ID_UEFISECAPP_LOADED = 2;
  /**< Used by UefiSecApp to prevent reloading. */

  const int32 ID_MDTP_RECOVERY_MS_FUSE = 3;
  /**< Used by MDTP to prevent access to Bootloader APIs. */

  const int32 ID_MDTP_HLOS_MS_FUSE = 4;
  /**< Used by MDTP to prevent access to Bootloader & Recovery APIs. */

  const int32 ID_OEM_FUSE_1 = 5;
  /**< Reserved fuse bit for OEMs. */

  const int32 ID_OEM_FUSE_2 = 6;
  /**< Reserved fuse bit for OEMs. */

  const int32 ID_OEM_FUSE_3 = 7;
  /**< Reserved fuse bit for OEMs. */

  const int32 ID_OEM_FUSE_4 = 8;
  /**< Reserved fuse bit for OEMs. */

  const int32 ID_OEM_FUSE_5 = 9;
  /**< Reserved fuse bit for OEMs. */

  const int32 ID_OEM_FUSE_6 = 10;
  /**< Reserved fuse bit for OEMs. */

  const int32 ID_OEM_FUSE_7 = 11;
  /**< Reserved fuse bit for OEMs. */

  const int32 ID_OEM_FUSE_8 = 12;
  /**< Reserved fuse bit for OEMs. */

  const int32 ID_QCOM_RESERVED_FUSE_1 = 13;
  /**< Reserved fuse bit for QCOM.*/

  const int32 ID_QCOM_RESERVED_FUSE_2 = 14;
  /**< Reserved fuse bit for QCOM.*/

  const int32 ID_QCOM_RESERVED_FUSE_3 = 15;
  /**< Reserved fuse bit for QCOM.*/

  const int32 ID_QCOM_RESERVED_FUSE_4 = 16;
  /**< Reserved fuse bit for QCOM.*/

  const int32 ID_QCOM_RESERVED_FUSE_5 = 17;
  /**< Reserved fuse bit for QCOM.*/

  const int32 ID_QCOM_RESERVED_FUSE_6 = 18;
  /**< Reserved fuse bit for QCOM.*/

  const int32 ID_QCOM_RESERVED_FUSE_7 = 19;
  /**< Reserved fuse bit for QCOM.*/

  const int32 ID_QCOM_RESERVED_FUSE_8 = 20;
  /**< Reserved fuse bit for QCOM.*/

  const int32 ID_TPM_LOADED = 21; 
  /**< Used by TPM App to prevent reloading. */ 

  const int32 ID_HLOS_BL_MILESTONE_FUSE = 22;
  /**< Used to notify the bootloader milestone call.*/

  const int32 ID_HLOS_TAMPER_NOTIFY_FUSE = 23;
  /**< Used to notify TZ that HLOS has been tampered.*/

  const int32 NUM_SW_FUSES = 24;
  /**< Number of supported software fuses.*/

  const int32 STATUS_SECBOOT_ENABLING_CHECK_FAILED = 0;
  /**< secboot enabling check failed */

  const int32 STATUS_SEC_HW_KEY_NOT_PROGRAMMED = 1;
  /**< Sec HW key is not programmed */

  const int32 STATUS_DEBUG_DISABLE_CHECK_FAILED = 2;
  /**< debug disable check failed */

  const int32 STATUS_ANTI_ROLLBACK_CHECK_FAILED = 3;
  /**< Anti-rollback check failed */

  const int32 STATUS_FUSE_CONFIG_CHECK_FAILED = 4;
  /**< fuse config check failed */

  const int32 STATUS_RPMB_PROVISIONED_CHECK_FAILED = 5;
  /**< RPMB provision check failed */

  const int32 STATUS_DEBUG_CHECK_IN_IMAGE_CERTIFICATE_FAILED = 6;
  /**< debug check in image certificate failed */

  const int32 STATUS_RSVD = 7;
  /**< RSVD */

  const int32 STATUS_TZ_SECURE_DEBUG_FUSE_CHECK_FAILED = 8;
  /**< tz secure debug fuse check failed */

  const int32 STATUS_MSS_SECURE_DEBUG_FUSE_CHECK_FAILED = 9;
  /**< mss secure debug fuse check failed */

  const int32 STATUS_CP_SECURE_DEBUG_FUSE_CHECK_FAILED = 10;
  /**< cp secure debug fuse check failed */

  const int32 STATUS_NON_SECURE_SECURE_DEBUG_FUSE_CHECK_FAILED = 11;
  /**< non-secure secure debug fuse check failed */

  /** @endcond */

  /**
   * Blow the SW fuse.
   *
   * @note1   Function is not thread-safe.
   *
   * @param[in] fuse             The SW fuse to blow.
   *
   * @return
   * Object_OK -- success. \n
   * Object_ERROR -- any error occurred.
   */
  method blowSwFuse(in int32 fuse);

  /**
   * Test whether the specified SW fuse is blown.
   *
   * @param[in]  fuse_num       The SW fuse to query.
   * @param[out] is_blown       Whether the given SW fuse is blown.
   *
   * @return
   * Object_OK -- success. \n
   * Object_ERROR -- any error occurred.
   */
  method isSwFuseBlown(in int32 fuse_num, out uint32 is_blown);

  /**
   * Check security status on the device.
   *
   * @param [out] status1   The security status with the following bit field definitions:
   *                        - Bit 0: secboot enabling check failed
   *                        - Bit 1: Sec HW key is not programmed
   *                        - Bit 2: debug disable check failed
   *                        - Bit 3: Anti-rollback check failed
   *                        - Bit 4: fuse config check failed
   *                        - Bit 5: rpmb provisioned check failed
   *                        - Bit 6: debug check in image certificate failed (debug ou field missing from image cert)
   *                        - Bit 7: RSVD
   *                        - Bit 8: tz secure debug fuse check failed
   *                        - Bit 9: mss secure debug fuse check failed
   *                        - Bit 10: cp secure debug fuse check failed
   *                        - Bit 11: non-secure secure debug fuse check failed
   * @param [out] status2   The security status with the following bit field definitions:
   *                        - Bits 0-31: reserved
   *
   * @return
   * Object_OK -- success. \n
   * Object_ERROR -- any error occurred.
   */
  method getSecureState(out uint32 status1, out uint32 status2);
  /** @} */ /* end_addtogroup ISwFuse */
};
