/*------------------------------------------------------------------
 Copyright (c) 2019 Qualcomm Technologies, Inc.
 All Rights Reserved.
 Confidential and Proprietary - Qualcomm Technologies, Inc.
 -------------------------------------------------------------------*/

/** @cond */
interface ITLOCKey {
/** @endcond */

  /**
   * @addtogroup ITLOCKey
   * @{
   */

  /**
    Error code returned by getKeyData method when the key is already generated.
  */  
  error ERROR_KEY_ALREADY_GENERATED;

  /**
    The function gets key data using secure channel API. The Trusted Location (TLOC)
    system running in HLOS shall pass the key data to MDM. In the case of fusion target,
    existing secure channel shared memory based key exchange between TZ-Modem is not
    possible, as there is no shared memory between APQ to MDM. The communication between
    these processors is established by data transfer over PCIe through TLOC in HLOS. As 
    per the design, the expected size of the buffer for the key from the caller is 256 bytes.

    @param[out] keydata  An output buffer populated with key data

    @return
    Object_OK indicates success
    Object_ERROR indicates failure
  */
  method getKeyData(out buffer keydata);

  /**
    The function gets nonce data using secure channel API. The Trusted Location (TLOC)
    system running in HLOS shall pass the nonce data to MDM. To mitigate the anti-reply,
    the nonce will be shared between APQ to MDM for every boot. As per the design, the 
    expected size of the buffer for the nonce from the caller is 128 bytes.

    @param[out] noncedata  An output buffer populated with nonce data

    @return 
    Object_OK indicates success
    Object_ERROR indicates failure
  */
  method getKeyNonce(out buffer noncedata);

  /** @} */ /* end_addtogroup ITLOCKey */
};
