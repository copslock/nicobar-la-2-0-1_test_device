#ifndef TLOC_H_
#define TLOC_H_
/**
@file tloc.h

Modern user authentication techniques, use several methodologies to identify the person who uses the device.
The Trusted Location system is an optional input to the authentication scheme.
It enables QSEE Apps to get secure location from the modem.

This H file provides the API for the Trusted Location System.


When a QSEE App wants to use the Trusted Location System, it linkages with TLOC.
TLOC talks with the Secure Location Service throw an HLOS daemon called TLOCD.

   		    TZ				   HLOS			    Modem

	QSEE App - TLOC ---------- TLOCD -------- Location Service


QSEE Apps can get the location using TLOC_GetTrustedLocation().
This received location is based GPS only and it is the best available location the modem has.
In order to improve the 'age' of the location (increase the accuracy), it is recommended to
warm-up the system before sending this request by calling TLOC_WarmUp().
It is recommended to warm-up the system as soon as possible to increase the probability
of getting a better location.

The Daemon TLOCD is responsible of warming-up the modem periodically (each 'Last Known Location
Freshness Interval' defined in 80-NP516-2 A).
So in case the user can't warm-up the system before calling TLOC_GetTrustedLocation(),
The location would be the location the calculated in the past 'Last Known Location Freshness Interval'.

Another API is TLOC_GetAvailableLocation(). This function returns the best available location based on
any source. Also, the data of this function isn't signed/encrypted.

*/

/*=============================================================================
       Copyright (c) 2015 Qualcomm Technologies Incorporated.
           All Rights Reserved.
       Qualcomm Confidential and Proprietary
=============================================================================*/


/*-------------------------------------------------------------------------
* Include Files
* ----------------------------------------------------------------------*/
#include <com_dtypes.h>
#include <stdbool.h>

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*
 * The TLOC return status.
 */
typedef enum TLOC_Status_
{
	TLOC_SUCCESS			= 0,	//Succeeded to get a location.
	TLOC_SYSTEM_NOT_READY	= 1,	//at least one of the system component is not ready e.g. modem is not up yet.
	TLOC_NO_LOCATION		= 2,	//there is no available location in the modem.
	TLOC_GENERAL_ERROR		= 3,	//any error
	TLOC_BAD_INPUT 			= 4,	//bad input

	TLOC_LAST_STATUS				//must be last

} TLOC_Status;

/*
 * The status of the received location
 * This field is valid only in case TLOC_SUCCESS was returned.
 */
typedef enum TLOC_LocationStatus_
{
	LOCATION_FIX			= 0,
	LOCATION_IN_PROGRESS	= 1,

} TLOC_LocationStatus;

/*
 * The technology used in computing the fix
 */
typedef enum TLOC_TechnologyUsedValues_
{
	QMI_LOC_POS_TECH_MASK_SATELLITE 			   = (0x00000001), //Satellites were used to generate the fix.
	QMI_LOC_POS_TECH_MASK_CELLID    		       = (0x00000002), //Cell towers were used to generate the fix.
	QMI_LOC_POS_TECH_MASK_WIFI 					   = (0x00000004), //Wi-Fi access points were used to generate the fix.
	QMI_LOC_POS_TECH_MASK_SENSORS 				   = (0x00000008), //Sensors were used to generate the fix.
	QMI_LOC_POS_TECH_MASK_REFERENCE_LOCATION 	   = (0x00000010), //Reference Location was used to generate the fix.
	QMI_LOC_POS_TECH_MASK_INJECTED_COARSE_POSITION = (0x00000020), //Coarse position injected into the location engine was used to generate the fix.
	QMI_LOC_POS_TECH_MASK_AFLT 					   = (0x00000040), //AFLT was used to generate the fix.
	QMI_LOC_POS_TECH_MASK_HYBRID 				   = (0x00000080), //GNSS and network-provided measurements were used to generate the fix.

} TLOC_TechnologyUsedValues;

/*
 * The TLOC Location Data that would be sent back to the user.
 */
typedef struct TLOC_LocationData_
{
	// all fields definitions can be found in QMI LOC 2.24; QMI Location Svc Spec - 80-VB816-17 L

	/* Latitude (specified in WGS84 datum).
		Type: Floating point
		Units: Degrees
		Range: -90.0 to 90.0
			- Positive values indicate northern latitude
			- Negative values indicate southern latitude
	*/
	bool Latitude_valid;
	double Latitude;

	/*
	Longitude (specified in WGS84 datum).
		Type: Floating point
		Units: Degrees
		Range: -180.0 to 180.0
			\u2013 Positive values indicate eastern longitude
			- Negative values indicate western longitude
	*/
	bool Longitude_valid;
	double Longitude;

	/*
	Circular horizontal accuracy
		Units: Meters
	*/
	bool Accuracy_valid;
	float Accuracy;

	/*
	Technology used in computing this fix.
		This is a bitmask field.
		Its valid values are defines at TechnologyUsedValues.
	*/
	bool Technology_valid;
	uint32_t Technology;

	/*
	Current UTC timestamp.
		Units: Milliseconds since Jan. 1, 1970
	*/
	bool Time_valid;
	uint64_t Time;

	 /*
	 Altitude with respect to mean sea level.
		Units: Meters
	*/
	bool Altitude_valid;
	float Altitude;

	/*
	Heading.
		Units: Degrees
		Range: 0 to 359.999
	*/
	bool Bearing_valid;
	float Bearing;

	/*
	Horizontal speed.
		Units: Meters/second
	*/
	bool Speed_valid;
	float Speed;

} TLOC_LocationData;


/*-------------------------------------------------------------------------
 * Functions Declarations
 * ----------------------------------------------------------------------*/
/**
 * This function returns the best available location based GPS.
 * It generates a request and sends it to the Secure Location Service.
 * The Secure Location Service returns the best available location.
 * If there isn't any available location, the returned TLOC_Status would be TLOC_NO_LOCATION.
 * In case of success (there is a location), locStatus describes the location -
 * FIX means the location's accuracy is good, IN_PROGRESS means the location's accuracy
 * is not good and the user MAY run this function again to get a better one.
 *
 * @param [out] locData				The location data in case of success.
 * @param [out] locStatus			The location status in case of success.
 *
 * @return TLOC_Status.
*/
TLOC_Status TLOC_GetTrustedLocation(TLOC_LocationData* locData, TLOC_LocationStatus* locStatus);

/**
 * This function returns the best available location (can be from any source).
 * It generates a request and sends it to the Location Service.
 * The Location Service returns the best available location.
 * If there isn't any available location, the returned TLOC_Status would be TLOC_NO_LOCATION.
 * In case of success (there is a location), locStatus describes the location -
 * FIX means the location's accuracy is good, IN_PROGRESS means the location's accuracy
 * is not good and the user MAY run this function again to get a better one.
 *
 * @param [out] locData				The location data in case of success.
 * @param [out] locStatus			The location status in case of success.
 *
 * @return TLOC_Status.
*/
TLOC_Status TLOC_GetAvailableLocation(TLOC_LocationData* locData, TLOC_LocationStatus* locStatus);

/**
 * This function Warms Up the modem (start the GPS and try to find a location fix or to
 * improve the current location).
 * In order to get a location fix as accurate and fresh as possible,
 * the user should call this function AS SOON AS HE CAN.
 * In case TLOC_GetTrustedLocation() request would be sent without calling
 * this function before, the received location might be 30 minutes old although the user
 * is within a GPS range.
 * The calculated location won't be sent back.
 * In order to get it, the user should call TLOC_GetTrustedLocation().
 *
 * @return TLOC_Status.
*/
TLOC_Status TLOC_WarmUp(void);




#endif //TLOC_H_
