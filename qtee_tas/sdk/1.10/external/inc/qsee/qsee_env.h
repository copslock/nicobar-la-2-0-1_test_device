/*=============================================================
  Copyright (c) 2015-2019 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
=============================================================*/

#ifndef __QSEE_ENV_H
#define __QSEE_ENV_H


/**
@addtogroup qtee_env
@{
*/

#include <stdint.h>
#include "object.h"


/** @cond */

/**
  A UID that does not identify any service.
**/
#define QSEE_OPEN_INVALID_ID 0

/** @endcond */

/**
  Request a service from the system.

  On success, *objOut will hold the resulting object reference and
  Object_OK will be returned.  In this case, the caller will hold a
  reference to the object and is responsible for releasing it at some
  point.

  Otherwise, a non-zero value defined in IOpener will be returned. The
  value of objOut will be non-deterministic in case of an error and
  the caller should not access it.

  @param[in]    uid     The uid of the requested service.
  @param[out]   objOut  Requested service instance.

  @return
  Object_OK on success.\n
  IOpener_ERROR_NOT_FOUND if a service matching the ID cannot be found.\n
  IOpener_ERROR_PRIVILEGE if required privileges are not present.

**/
int32_t qsee_open(uint32_t uid, Object *objOut);


/**
  Request a service from the system if it has not already been obtained.

  If the object has already been obtained, it will be returned and
  the object reference will not be retained (the caller does not need to
  call release once for each successful call). Singleton objects are persistent for the lifetime of a TA.

  @param[in]  classID   The uid of the requested service.

  @return
  Requested service instance.\n
  Object_NULL on failure.
**/
Object qsee_open_singleton(uint32_t classID);

/**
  Request the IMemSpace object associated with this TA and retains it. The caller
  is expected to release this object.

  @return
  IMemSpace object associated with the application.
**/
Object qsee_get_space(void);

/** @} */

#endif // __QSEE_ENV_H
