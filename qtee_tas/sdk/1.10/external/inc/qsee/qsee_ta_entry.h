/*===========================================================================
  Copyright (c) 2019 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
===========================================================================*/

#ifndef QSEE_TA_ENTRY
#define QSEE_TA_ENTRY

/**
@addtogroup qtee_ta_entry
@{
*/

#include "object.h"
#include <stdint.h>

/**
  Called by QTEE on application start.

  Any initialization required before the TA is ready to handle commands
  should be placed here.
**/
void tz_app_init(void);

/**
  Called by QTEE on application shutdown.

  Any deinitialization required before the TA is unloaded should be placed here.
**/
void tz_app_shutdown(void);

/**
  Returns an object of an interface the TA implements.

  The CA gets to this point using the IAppController interface;
  specifically IAppController_getAppObject().

  @param[in] credentials An object describing the credentials used to request the
                        object.
  @param[out] obj_ptr    An instance of the requested class.

  @return
  Object_OK if successful.
**/
int32_t app_getAppObject(Object credentials, Object *obj_ptr);

/**
  Returns an object of an interface a TA hosts.

  A TA hosting services would need to implement this function.

  @param[in]  id           The requested class ID.
  @param[in]  credentials  An object describing the credentials used to
                          request the object.
  @param[out] obj_ptr      An instance of the requested class.

  @return
  Object_OK if successful.
**/
int32_t tz_module_open(uint32_t uid, Object credentials, Object *obj_ptr);

/**
  The legacy entry point for a TA to handle a command from a CA.

  Newer TAs wouldn't implement this API, they would implement app_getAppObject().

  @param[in, out]   req         Pointer to the request buffer.
  @param[in]        reqlen      Its length.
  @param[in, out]   rsp         Pointer to the response buffer.
  @param[in]        rsplen      Its length.
**/
void tz_app_cmd_handler(void *req, unsigned int reqlen, void *rsp, unsigned int rsplen);

/** @} */

#endif  // QSEE_TA_ENTRY
