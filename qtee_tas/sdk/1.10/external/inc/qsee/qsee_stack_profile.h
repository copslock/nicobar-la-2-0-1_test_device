#ifndef _QSEE_STACK_PROFILE_H_
#define _QSEE_STACK_PROFILE_H_

/*=============================================================================
              Copyright (c) 2019 QUALCOMM Technologies Incorporated.
                      All Rights Reserved.
              Qualcomm Confidential and Proprietary

=============================================================================*/


#include <stdint.h>
#include <stdbool.h>

/**
 * @addtogroup qtee_stack_profile
 * @{
    API exposed for stack profiling.

    @note1hang TAs should use these APIs only for development purposes. Calls to these APIs
               should not be made in production builds as they have a performance impact.
 */

/**
  Look for stack usage in a function in TA

  @param[in] currSp     stackPtr returned from qsee_prepare_stack_profile.

  @return
  Returns the number of bytes pushed/popped on/off the stack since the calling of qsee_prepare_stack_profile()

  <b>Prerequisite:</b> A call to qsee_prepare_stack_profile() is required before calling qsee_profile_stack_usage().

*/
size_t qsee_profile_stack_usage(uintptr_t currSp);

/**
  Prepare for stack profiling by filling the currently unused stack space of the calling TA with a well defined pattern.

  @return
  Returns current stackPtr

*/
uintptr_t qsee_prepare_stack_profile(void);

/** @} */

#endif //_QSEE_STACK_PROFILE_H_
