#ifndef QSEE_VERSION_H
#define QSEE_VERSION_H

/*============================================================================
Copyright (c) 2012, 2019 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
=========================================================================== */

/*=========================================================================
                              Edit History

when       who     what, where, why
--------   ---     --------------------------------------------------------
08/04/11    rv     Initial Revision

=========================================================================== */

#define QSEE_VERSION_MAJOR 04
#define QSEE_VERSION_MINOR 00

#endif /*QSEE_VERSION_H*/

