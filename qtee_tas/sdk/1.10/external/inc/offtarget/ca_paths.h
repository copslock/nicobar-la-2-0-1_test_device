/********************************************************************
 * Copyright (c) 2018-2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *********************************************************************/

#ifndef CA_PATHS_H
#define CA_PATHS_H

/**
  @brief Constant arrays required to declare the location of commonlib and trusted applications.
  @file ca_paths.h

  @addtogroup client_api_ca_paths
  @{
*/

/**
  This file has to be included in one file of the client application.
**/

#ifdef __cplusplus

extern char const TA_PATH[] = TA_PATH_DEFINE;
extern char const CMNLIB_PATH[] = CMNLIB_PATH_DEFINE;

#else

char const TA_PATH[] = TA_PATH_DEFINE;
char const CMNLIB_PATH[] = CMNLIB_PATH_DEFINE;

#endif /* __cplusplus */

/** @} */

#endif /* CA_PATHS_H */
