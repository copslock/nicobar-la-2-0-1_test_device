/*============================================================
  Copyright (c) 2018-2019 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
==============================================================*/

#ifndef CLIENT_APIS_INC_MAP_TRUSTED_APPLICATION_H_
#define CLIENT_APIS_INC_MAP_TRUSTED_APPLICATION_H_

/**
  @brief Provides helper functions to map a trusted application into
         a read-only buffer.
  @file  map_trusted_application.h

  @addtogroup client_api_map_trusted_application
  @{
*/

#include <stdbool.h>
#include <stddef.h>

typedef struct ta_image_data {
  void *buffer;       ///< Read-only buffer containing the trusted application.
  size_t buffer_size; ///< Size of the buffer.
} ta_image_data;

/**
  Map a trusted application into a read-only buffer.

  @param[in]  appname  The full path to the file.
  @param[out] img_data Pointer to a ta_image_data struct
                       containing the TA buffer and its size.

  @return
  0 on success.\n
  Non-zero if mapping fails.
**/
int map_trusted_application(char const *appname, ta_image_data *img_data);

/**
  Unmap a trusted application referenced by a ta_image_data struct.

  @param[in] img_data Pointer to a ta_image_data struct
                      containing the TA buffer and its size.
**/
void unmap_trusted_application(ta_image_data *img_data);

/** @} */

#endif /* CLIENT_APIS_INC_MAP_TRUSTED_APPLICATION_H_ */
