/********************************************************************
 Copyright (c) 2019 Qualcomm Technologies, Inc.
 All Rights Reserved.
 Confidential and Proprietary - Qualcomm Technologies, Inc.
 *********************************************************************/
#pragma once

/**
  @brief Provides definitions for macros similar to Android's ALOG macros. Colors are based on Android Studio defaults.
  @file  alog.h

  @addtogroup client_api_alog
  @{
*/

#include "ansi_colors.h"
#include <stdio.h>


#define ALOGV(format, x...) printf("%sV/%s( %s#%d): " format "%s\n", ANSI_COLOR_DEFAULT, LOG_TAG, __func__, __LINE__, ##x, ANSI_COLOR_RESET)
#define ALOGI(format, x...) printf("%sI/%s( %s#%d): " format "%s\n", ANSI_COLOR_GREEN, LOG_TAG, __func__, __LINE__, ##x, ANSI_COLOR_RESET)
#define ALOGD(format, x...) printf("%sD/%s( %s#%d): " format "%s\n", ANSI_COLOR_YELLOW, LOG_TAG, __func__, __LINE__, ##x, ANSI_COLOR_RESET)
#define ALOGW(format, x...) printf("%sW/%s( %s#%d): " format "%s\n", ANSI_COLOR_BLUE, LOG_TAG, __func__, __LINE__, ##x, ANSI_COLOR_RESET)
#define ALOGE(format, x...) printf("%sE/%s( %s#%d): " format "%s\n", ANSI_COLOR_RED, LOG_TAG, __func__, __LINE__, ##x, ANSI_COLOR_RESET)

/** @} */
