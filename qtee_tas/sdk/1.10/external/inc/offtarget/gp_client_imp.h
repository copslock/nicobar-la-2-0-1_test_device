/********************************************************************
Copyright (c) 2019 Qualcomm Technologies, Inc.
All Rights Reserved.
Confidential and Proprietary - Qualcomm Technologies, Inc.
*********************************************************************/
#ifndef __GP_CLIENT_IMP_H_
#define __GP_CLIENT_IMP_H_

/**
  @brief APIs to manage dependencies of GP trusted applications.
  @file  gp_client_imp.h

  @addtogroup client_api_gp_client_imp
  @{
*/

#include "tee_client_api.h"
#include <stddef.h>

#define MAX_APP_NAME_SIZE 64       /**< Maximum length of a TAs name. */
#define MAX_UUID_DEP_NAME_COUNT 8  /**< Maximum number of TA dependencies. */

#pragma pack(push, gp_client_imp, 1)
typedef struct {
  char app_name[MAX_UUID_DEP_NAME_COUNT]
               [MAX_APP_NAME_SIZE];  ///< Array containing the names of the dependencies of a GP TA.
  size_t num;                        ///< Number of TA dependencies.
} TEEC_uuid_dep_names;
#pragma pack(pop, gp_client_imp)

/**
  Get the application names of GP trusted applications identified by its UUID.

  A GP client application identifies GP trusted applications by its UUID, whereby
  QTEE identifies and loads them by its name. This API is called from the GP client
  layer to determine the name of the GP trusted application as well as the name of
  its dependencies.

  @note1hang
  An off-target GP client application must implement this function.

  @param[in]  uuid     Pointer to the UUID of the called GP trusted application.
  @param[out] app_name Pointer to the name of the called GP trusted application.
  @param[out] deps     Pointer to a struct containing the names of the dependencies of
                       the GP trusted application.

  @return
  TEEC_SUCCESS on success, \n
  TEEC_GENERIC_ERROR otherwise.
**/
TEEC_Result getAppFromUUID(const TEEC_UUID *uuid, char *app_name, TEEC_uuid_dep_names *deps);

/** @} */

#endif /* __GP_CLIENT_IMP_H_ */
