/********************************************************************
 * Copyright (c) 2018-2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *********************************************************************/

#ifndef OFFTARGET_CLIENT_APIS_INC_QTEE_INIT_H_
#define OFFTARGET_CLIENT_APIS_INC_QTEE_INIT_H_

/**
  @brief APIs to initialize the off-target QTEE emulation environment
  @file  qtee_init.h

  @addtogroup client_api_qtee_init
  @{
*/

#include "object.h"
#include <stdint.h>

/**
  Initialize the QTEE Emulation environment.

  @return
  Object_OK on success \n
  Object_ERROR otherwise.
**/
int32_t qtee_emu_init(void);

/**
  Deinitialize the QTEE Emulation environment.

  @return
  Object_OK on success \n
  Object_ERROR otherwise.
**/
int32_t qtee_emu_deinit(void);

/** @cond **/

/**
  Initialize the QTEE Emulation environment.

  Deprecated: Superseded by qtee_emu_init().

  @return
  Object_OK on success \n
  Object_ERROR otherwise.
**/
int32_t qtee_sdk_init(void);

/**
  Deinitialize the QTEE Emulation environment.

  Deprecated: Superseded by qtee_emu_deinit().

  @return
  Object_OK on success \n
  Object_ERROR otherwise.
**/
int32_t qtee_sdk_deinit(void);

/** @endcond **/

/** @} */

#endif /* OFFTARGET_CLIENT_APIS_INC_QTEE_INIT_H_ */
