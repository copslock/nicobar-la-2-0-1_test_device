/********************************************************************
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *********************************************************************/


#ifndef MEMOBJ_H
#define MEMOBJ_H

#include <stddef.h>
#include <stdint.h>

#include "object.h"

/**
  @brief APIs to create memory objects.
  @file  MemObj.h

  @addtogroup client_api_mem_obj
  @{
*/

/**
  Create a new memory object.

  This API creates a memory region object of a given source buffer.

  The memory object works directly on the source buffer. The supplied
  permissions have to be a subset of the access rights of the source buffer.
  The supplied permissions are only respected by APIs using the memory
  object. Access to the source buffer via its address is not prevented.

  @param[in] addr  Pointer to a buffer representing the memory region.
  @param[in] sizes Size of the buffer.
  @param[in] perms IMemRegion permissions of the memory object.

  @return
  Object representing the memory region on success, \n
  Object_NULL otherwise.
**/
Object MemObj_new(void *addr, size_t size, uint32_t perms);

/**
  Set the permissions of a memory object.

  @param[out] memObj Memory object.
  @param[in]  perms  IMemRegion permissions of the memory object.
**/
void MemObj_setPerms(Object memObj, uint32_t perms);

/**
  Get the permissions of a memory object.

  @param[in] memObj Memory object.

  @return
  A uint32_t representing the IMemRegion permissions.
**/
uint32_t MemObj_getPerms(Object memObj);

/** @} */

#endif /* MEMOBJ_H */
