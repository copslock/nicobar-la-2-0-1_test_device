/********************************************************************
Copyright (c) 2016-2019 Qualcomm Technologies, Inc.
All Rights Reserved.
Confidential and Proprietary - Qualcomm Technologies, Inc.
*********************************************************************/

#ifndef _TZCOM_H_
#define _TZCOM_H_

/**
  @brief APIs to retrieve an IClientEnv object.
  @file  TZCom.h

  @addtogroup client_api_tz_com
  @{
*/


#include "object.h"

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>

/**
  Get root object

  This is used to create a root IClientEnv object.
  It supports default 16 callback threads and 4K callback request buffer.

  @param[out] rootobj root IClientEnv Obj

  @return
  Object_OK on success, \n
  Object_ERROR otherwise.
 */
int TZCom_getRootEnvObject (Object *obj);

/**
  Get root object with configurable callback threads and/or callback buffer size

  This is used by client to create a root IClientEnv Obj when number of callback threads
  and/or callback buffer size are different from default values

  @param[in] cbthread_cnt    cb thread count
  @param[in] cbbuf_len       cb request buffer length
  @param[out] rootobj        root IClientEnv Obj

  @return
  Object_OK on success, \n
  Object_ERROR otherwise.
 */
int TZCom_getRootEnvObjectWithCB (size_t cbthread_cnt, size_t cbbuf_len, Object *obj);

/**
  Client get a new IClientEnv obj

  This interface retrieves the client's credentials and registers the client
  with the TEE. The

  @param[out] obj client's IClientEnv object

  @return
  0 on success, \n
  negative otherwise
 */
int TZCom_getClientEnvObject (Object *obj);

/**
  Wrap a file descriptor into an object

  @param[in] fd    the fd to be wrapped into an obj
  @param[out] obj  fd object that takes ownership of fd
                  i.e.release of obj would close fd.

   @return
   0 on success, \n
   negative otherwise
 */
int TZCom_getFdObject (int fd, Object *obj);

#ifdef __cplusplus
}
#endif

/** @} */

#endif // _TZCOM_H_
