/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef IONSIM_H
#define IONSIM_H

/**
  @brief APIs to create a simulated ION buffer.
  @file  IonSim.h

  @addtogroup client_api_ion_sim
  @{
*/

#include <stddef.h>
#include <stdint.h>

typedef struct {
  int32_t fd;          ///< A simulated file descriptor representing this buffer.
  void *buffer;        ///< Pointer to the simulated buffer.
  size_t buffer_size;  ///< Size of the simulated buffer.
} IonSimHandle;

/**
  Allocate a simulated ion buffer represented by a handle.

  @param[in] handle    Pointer to the handle representing a simulated ion buffer.
  @param[in] size      Size of the allocated buffer.
  @param[in] alignment Alignment of the allocated buffer.

  @return
  0 on success, \n
  negative otherwise.
**/
int IonSim_alloc(IonSimHandle *handle, size_t size, size_t alignment);


/**
  Frees a simulated ion buffer represented by a handle.

  @param[in] handle    Pointer to the handle representing a simulated ion buffer.
**/
void IonSim_free(IonSimHandle *handle);

/** @} */

#endif /* IONSIM_H */
