/* =================================================================================
 *  Copyright (c) 2015 by Qualcomm Technologies, Incorporated.  All Rights
 * Reserved.
 * =================================================================================
 */
#ifndef KM_PLATFORM_UTILS_H_
#define KM_PLATFORM_UTILS_H_

#include "keymaster_defs_version.h"
#include "qsee_log.h"

#include <km_heap.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

/*
 * Pre-processor Definitions
 */
#define ENABLE_LOGGING 1
#define ENABLE_DEBUG 1

/* Make this 1 to enable printing of the auth list in keymaster for debug */
// TODO: Adding commandIDs
#define PRINT_AUTH_LIST 0

#ifdef ENABLE_LOGGING
#define KM_LOG_ERROR(xx_fmt, ...) qsee_log(QSEE_LOG_MSG_ERROR, xx_fmt, ##__VA_ARGS__)
#define KM_LOG_FATAL(xx_fmt, ...) qsee_log(QSEE_LOG_MSG_FATAL, xx_fmt, ##__VA_ARGS__)
#ifdef ENABLE_DEBUG
#define KM_LOG_DEBUG(xx_fmt, ...) qsee_log(QSEE_LOG_MSG_ERROR, xx_fmt, ##__VA_ARGS__)
#else
#define KM_LOG_DEBUG(xx_fmt, ...)
#endif
#else
#define KM_LOG_ERROR(xx_fmt, ...)
#define KM_LOG_FATAL(xx_fmt, ...)
#define KM_LOG_DEBUG(xx_fmt, ...)
#endif

#define UNUSED(x) (void)(x)

#define KM_MAX_OPERATIONS_ALLOWED (16)

#ifndef UINT_MAX
#define UINT_MAX 0xffffffffU
#endif

#define KM_UINT64_MAX 0xffffffffffffffffU
/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

#define DIV_ROUND(x, len) (((x) + (len)-1) / (len))
#define ROUND_UP(x, align) DIV_ROUND(x, align) * (align)

uint64_t htonl(uint64_t t);
uint32_t htonl32(uint32_t t);
uint64_t ntohl(uint64_t t);
uint32_t ntoh32(uint32_t t);

#endif /* KM_PLATFORM_UTILS_H_ */
