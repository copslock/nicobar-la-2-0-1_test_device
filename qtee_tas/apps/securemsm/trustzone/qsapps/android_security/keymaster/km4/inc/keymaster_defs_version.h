/* =================================================================================
 *  Copyright (c) 2017 by Qualcomm Technologies, Incorporated.  All Rights
 * Reserved.
 * =================================================================================
 */

#ifndef KEYMASTER_KM4_INC_KEYMASTER_DEFS_VERSION_H_
#define KEYMASTER_KM4_INC_KEYMASTER_DEFS_VERSION_H_

#include <cdefs.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * Authorization tags each have an associated type.  This enumeration
 * facilitates tagging each with
 * a type, by using the high four bits (of an implied 32-bit unsigned enum
 * value) to specify up to
 * 16 data types.  These values are ORed with tag IDs to generate the final tag
 * ID values.
 */
typedef enum {
    KM_INVALID = 0 << 28, /* Invalid type, used to designate a tag as uninitialized */
    KM_ENUM = 1 << 28,
    KM_ENUM_REP = 2 << 28, /* Repeatable enumeration value. */
    KM_UINT = 3 << 28,
    KM_UINT_REP = 4 << 28, /* Repeatable integer value */
    KM_ULONG = 5 << 28,
    KM_DATE = 6 << 28,
    KM_BOOL = 7 << 28,
    KM_BIGNUM = 8 << 28,
    KM_BYTES = 9 << 28,
    KM_ULONG_REP = 10 << 28, /* Repeatable long value */
} keymaster_tag_type_t;

typedef enum {
    KM_TAG_INVALID = KM_INVALID | 0,

    /*
     * Tags that must be semantically enforced by hardware and software
     * implementations.
     */

    /* Crypto parameters */
    KM_TAG_PURPOSE = KM_ENUM_REP | 1,    /* keymaster_purpose_t. */
    KM_TAG_ALGORITHM = KM_ENUM | 2,      /* keymaster_algorithm_t. */
    KM_TAG_KEY_SIZE = KM_UINT | 3,       /* Key size in bits. */
    KM_TAG_BLOCK_MODE = KM_ENUM_REP | 4, /* keymaster_block_mode_t. */
    KM_TAG_DIGEST = KM_ENUM_REP | 5,     /* keymaster_digest_t. */
    KM_TAG_PADDING = KM_ENUM_REP | 6,    /* keymaster_padding_t. */
    KM_TAG_CALLER_NONCE = KM_BOOL | 7,   /* Allow caller to specify nonce or IV. */
    KM_TAG_MIN_MAC_LENGTH = KM_UINT | 8, /* Minimum length of MAC or AEAD authentication tag in
                                          * bits. */
    KM_TAG_KDF = KM_ENUM_REP | 9,        /* keymaster_kdf_t (keymaster2) */
    KM_TAG_EC_CURVE = KM_ENUM | 10,      /* keymaster_ec_curve_t (keymaster2) */

    /* Algorithm-specific. */
    KM_TAG_RSA_PUBLIC_EXPONENT = KM_ULONG | 200,
    KM_TAG_ECIES_SINGLE_HASH_MODE = KM_BOOL | 201, /* Whether the ephemeral public key is fed into
                                                    * the KDF */
    KM_TAG_INCLUDE_UNIQUE_ID = KM_BOOL | 202,      /* If true, attestation certificates for this key
                                                    * will contain an application-scoped and
                                                    * time-bounded device-unique ID. (keymaster2) */

    /* Other hardware-enforced. */
    KM_TAG_BLOB_USAGE_REQUIREMENTS = KM_ENUM | 301, /* keymaster_key_blob_usage_requirements_t */
    KM_TAG_BOOTLOADER_ONLY = KM_BOOL | 302,         /* Usable only by bootloader */
    KM_TAG_ROLLBACK_RESISTANT = KM_BOOL | 303,      /* Whether key is rollback-resistant. */
    /* HARDWARE_TYPE specifies the type of the secure hardware that is requested
     * for the key
     * generation / import. See the SecurityLevel enum. In the absence of this
     * tag, keystore will
     * use TRUSTED_ENVIRONMENT. If this tag is present and the requested
     * hardware type is not
     * available, Keymaster returns HARDWARE_TYPE_UNAVAILABLE. This tag is not
     * included in
     * attestations, but hardware type will be reflected in the keymaster
     * SecurityLevel of the
     * attestation header. */
    KM_HARDWARE_TYPE = KM_ENUM | 304,

    /**
     * Keys tagged with EARLY_BOOT_ONLY may only be used, or created, during early boot, until
     * IKeymasterDevice::earlyBootEnded() is called.
     */
    KM_EARLY_BOOT_ONLY = KM_BOOL | 305,

    /*
     * Tags that should be semantically enforced by hardware if possible and
     * will otherwise be
     * enforced by software (keystore).
     */

    /* Key validity period */
    KM_TAG_ACTIVE_DATETIME = KM_DATE | 400,             /* Start of validity */
    KM_TAG_ORIGINATION_EXPIRE_DATETIME = KM_DATE | 401, /* Date when new "messages" should no
                                                           longer be created. */
    KM_TAG_USAGE_EXPIRE_DATETIME = KM_DATE | 402,       /* Date when existing "messages" should no
                                                           longer be trusted. */
    KM_TAG_MIN_SECONDS_BETWEEN_OPS = KM_UINT | 403,     /* Minimum elapsed time between
                                                           cryptographic operations with the key. */
    KM_TAG_MAX_USES_PER_BOOT = KM_UINT | 404,           /* Number of times the key can be used per
                                                           boot. */

    /* User authentication */
    KM_TAG_ALL_USERS = KM_BOOL | 500,           /* Reserved for future use -- ignore */
    KM_TAG_USER_ID = KM_UINT | 501,             /* Reserved for future use -- ignore */
    KM_TAG_USER_SECURE_ID = KM_ULONG_REP | 502, /* Secure ID of authorized user or authenticator(s).
                                                   Disallowed if KM_TAG_ALL_USERS or
                                                   KM_TAG_NO_AUTH_REQUIRED is present. */
    KM_TAG_NO_AUTH_REQUIRED = KM_BOOL | 503,    /* If key is usable without authentication. */
    KM_TAG_USER_AUTH_TYPE = KM_ENUM | 504,      /* Bitmask of authenticator types allowed when
                                                 * KM_TAG_USER_SECURE_ID contains a secure user ID,
                                                 * rather than a secure authenticator ID.  Defined in
                                                 * hw_authenticator_type_t in hw_auth_token.h. */
    KM_TAG_AUTH_TIMEOUT = KM_UINT | 505,        /* Required freshness of user authentication for
                                                   private/secret key operations, in seconds.
                                                   Public key operations require no authentication.
                                                   If absent, authentication is required for every
                                                   use.  Authentication state is lost when the
                                                   device is powered off. */
    KM_TAG_ALLOW_WHILE_ON_BODY = KM_BOOL | 506, /* Allow key to be used after authentication timeout
                                                 * if device is still on-body (requires secure
                                                 * on-body sensor. */
    /**
     * TRUSTED_USER_PRESENCE_REQUIRED is an optional feature that specifies that
     *this key may only
     * be used when the user has provided proof of physical presence. Proof of
     *physical presence
     * must be a signal that cannot be provided or spoofed by any attacker who
     *does not have
     * physical control of the device and has not compromised the secure
     *environment. For instance,
     * proof of user identity may be considered proof of presence if it meets
     *the requirements
     * above. However, proof of identity established in one security domain
     *(e.g. TEE) does not
     * constitute proof of presence in another security domain (e.g. StrongBox),
     *and no mechanism
     * analogous to the authentication token is defined for communicating proof
     *of presence across
     * security domains.
     *
     * Some examples:
     *
     *     A hardware button hardwired to a PIN on a StrongBox device in such a
     *way that nothing
     *     other than a button press can trigger the signal constitutes proof of
     *physical presence
     *     for StrongBox keys.
     *
     *     Fingerprint authentication provides proof of presence (and identity)
     *for TEE keys if the
     *     TEE has exclusive control of the fingerprint scanner and performs
     *fingerprint matching.
     *
     *     Password authentication does not provide proof of presence to either
     *TEE or StrongBox,
     *     even if TEE or StrongBox does the password matching, because password
     *input is handled by
     *     the non-secure world, which means an attacker who has compromised
     *Android can spoof
     *     password authentication.
     *
     * Finally, note that no mechanism is defined for delivering proof of
     *presence to Keymaster,
     * except perhaps as implied by an auth token. This means that Keymaster
     *must be able to check
     * proof of presence some other way. Further, the proof of presence must be
     *performed between
     * begin() and the first call to update() or finish(). If the first update()
     *or the finish()
     * call is made without proof of presence, the keymaster method must return
     * ErrorCode:PROOF_OF_PRESENCE_REQUIRED and abort the operation. The caller
     *must delay the
     * update() or finish() call until proof of presence has been provided,
     *which means the caller
     * must also have some mechanism for verifying that the proof has been
     *provided.
     */
    KM_TRUSTED_USER_PRESENCE_REQUIRED = KM_BOOL | 507,
    /* TRUSTED_CONFIRMATION_REQUIRED is only applicable to keys with KeyPurpose
     *SIGN, and specifies
     * that this key may only be used when the user provides confirmation of the
     *data to be
     * signed. Confirmation is proven to keymaster via an approval token. See
     *APPROVAL_TOKEN, as
     * well as the Trusted UI HAL.
     *
     * If an attempt to use a key with this tag does not have a
     *cryptographically valid
     * APPROVAL_TOKEN provided to begin(), or if the data provided to
     *update()/final() does not
     * match the data described in the token, keymaster must return
     *NO_USER_CONFIRMATION. */
    KM_TRUSTED_CONFIRMATION_REQUIRED = KM_BOOL | 508,
    KM_UNLOCKED_DEVICE_REQUIRED = KM_BOOL | 509, /* Require the device screen to be unlocked if
                                                  * the key is used. */

    /* Application access control */
    KM_TAG_ALL_APPLICATIONS = KM_BOOL | 600, /* Specified to indicate key is usable by all
                                              * applications. */
    KM_TAG_APPLICATION_ID = KM_BYTES | 601,  /* Byte string identifying the authorized
                                              * application. */
    KM_TAG_EXPORTABLE = KM_BOOL | 602,       /* If true, private/secret key can be exported, but
                                              * only if all access control requirements for use are
                                              * met. (keymaster2) */

    /*
     * Semantically unenforceable tags, either because they have no specific
     * meaning or because
     * they're informational only.
     */
    KM_TAG_APPLICATION_DATA = KM_BYTES | 700, /* Data provided by authorized application. */
    KM_TAG_CREATION_DATETIME = KM_DATE | 701, /* Key creation time */
    KM_TAG_ORIGIN = KM_ENUM | 702,            /* keymaster_key_origin_t. */

    KM_TAG_ROOT_OF_TRUST = KM_BYTES | 704,         /* Root of trust ID. */
    KM_TAG_OS_VERSION = KM_UINT | 705,             /* Version of system (keymaster2) */
    KM_TAG_OS_PATCHLEVEL = KM_UINT | 706,          /* Patch level of system (keymaster2) */
    KM_TAG_UNIQUE_ID = KM_BYTES | 707,             /* Used to provide unique ID in attestation */
    KM_TAG_ATTESTATION_CHALLENGE = KM_BYTES | 708, /* Used to provide challenge in attestation */
    KM_TAG_ATTESTATION_APPLICATION_ID = KM_BYTES | 709, /* Used to identify the set of possible
                                                         * applications of which one has initiated
                                                         * a key attestation */
    KM_TAG_ATTESTATION_ID_BRAND = KM_BYTES | 710,  /* Used to provide the device's brand name to be
                                                      included in attestation */
    KM_TAG_ATTESTATION_ID_DEVICE = KM_BYTES | 711, /* Used to provide the device's device name to be
                                                      included in attestation */
    KM_TAG_ATTESTATION_ID_PRODUCT = KM_BYTES | 712, /* Used to provide the device's product name to
                                                       be included in attestation */
    KM_TAG_ATTESTATION_ID_SERIAL = KM_BYTES | 713,  /* Used to provide the device's serial number to
                                                       be included in attestation */
    KM_TAG_ATTESTATION_ID_IMEI = KM_BYTES | 714,    /* Used to provide the device's IMEI to be
                                                       included in attestation */
    KM_TAG_ATTESTATION_ID_MEID = KM_BYTES | 715,    /* Used to provide the device's MEID to be
                                                       included in attestation */
    KM_ATTESTATION_ID_MANUFACTURER = KM_BYTES | 716, /* Used to provide the device's manufacturer
                                                          name to be included in attestation */
    KM_ATTESTATION_ID_MODEL = KM_BYTES | 717, /* Used to provide the device's model name to be
                                                   included in attestation */
    /**
     * Patch level of vendor image.  The value is an integer of the form YYYYMM,
     * where YYYY is the
     * four-digit year when the vendor image was released and MM is the
     * two-digit month.  During
     * each boot, the bootloader must provide the patch level of the vendor
     * image to keymaser
     * (mechanism is implemntation-defined).  When keymaster keys are created or
     * updated, the
     * VENDOR_PATCHLEVEL tag must be cryptographically bound to the keys, with
     * the current value as
     * provided by the bootloader.  When keys are used, keymaster must verify
     * that the
     * VENDOR_PATCHLEVEL bound to the key matches the current value.  If they do
     * not match,
     * keymaster must return ErrorCode::KEY_REQUIRES_UPGRADE.  The client must
     * then call upgradeKey.
     */
    KM_VENDOR_PATCHLEVEL = KM_UINT | 718,
    /**
     * Patch level of boot image.  The value is an integer of the form YYYYMM,
     * where YYYY is the
     * four-digit year when the boot image was released and MM is the two-digit
     * month.  During each
     * boot, the bootloader must provide the patch level of the boot image to
     * keymaser (mechanism is
     * implemntation-defined).  When keymaster keys are created or updated, the
     * BOOT_PATCHLEVEL tag
     * must be cryptographically bound to the keys, with the current value as
     * provided by the
     * bootloader.  When keys are used, keymaster must verify that the
     * BOOT_PATCHLEVEL bound to the
     * key matches the current value.  If they do not match, keymaster must
     * return
     * ErrorCode::KEY_REQUIRES_UPGRADE.  The client must then call upgradeKey.
     */
    KM_BOOT_PATCHLEVEL = KM_UINT | 719,

    /**
     * DEVICE_UNIQUE_ATTESTATION is an argument to IKeymasterDevice::attestKey().  It indicates that
     * attestation using a device-unique key is requested, rather than a batch key.  When a
     * device-unique key is used, only the attestation certificate is returned; no additional
     * chained certificates are provided.  It's up to the caller to recognize the device-unique
     * signing key.  Only SecurityLevel::STRONGBOX IKeymasterDevices may support device-unique
     * attestations.  SecurityLevel::TRUSTED_ENVIRONMENT IKeymasterDevices must return
     * ErrorCode::INVALID_ARGUMENT if they receive DEVICE_UNIQUE_ATTESTATION.
     * SecurityLevel::STRONGBOX IKeymasterDevices need not support DEVICE_UNIQUE_ATTESTATION, and
     * return ErrorCode::CANNOT_ATTEST_IDS if they do not support it.
     *
     * IKeymasterDevice implementations that support device-unique attestation MUST add the
     * DEVICE_UNIQUE_ATTESTATION tag to device-unique attestations.
     */
    KM_DEVICE_UNIQUE_ATTESTATION = KM_BOOL | 720,

    /**
     * IDENTITY_CREDENTIAL_KEY is never used by IKeymasterDevice, is not a valid argument to key
     * generation or any operation, is never returned by any method and is never used in a key
     * attestation.  It is used in attestations produced by the IIdentityCredential HAL when that
     * HAL attests to Credential Keys.  IIdentityCredential produces Keymaster-style attestations.
     */
    KM_IDENTITY_CREDENTIAL_KEY = KM_BOOL | 721,

    /**
     * To prevent keys from being compromised if an attacker acquires read access to system / kernel
     * memory, some inline encryption hardware supports protecting storage encryption keys in
     * hardware without software having access to or the ability to set the plaintext keys. Instead,
     * software only sees wrapped version of these keys.
     *
     * STORAGE_KEY is used to denote that a key generated or imported is a key used for storage
     * encryption. Keys of this type can either be generated or imported or secure imported using
     * keymaster. exportKey() can be used to re-wrap storage key with a per-boot ephemeral key
     * wrapped key once the key characteristics are enforced.
     *
     * Keys with this tag cannot be used for any operation within keymaster.
     * ErrorCode::INVALID_OPERATION is returned when a key with Tag::STORAGE_KEY is provided to
     * begin().
     */
    KM_STORAGE_KEY = KM_BOOL | 722,

    /* Tags used only to provide data to or receive data from operations */
    KM_TAG_ASSOCIATED_DATA = KM_BYTES | 1000, /* Used to provide associated data for AEAD modes. */
    KM_TAG_NONCE = KM_BYTES | 1001,           /* Nonce or Initialization Vector */
    KM_TAG_AUTH_TOKEN = KM_BYTES | 1002,      /* Authentication token that proves secure user
                                                 authentication has been performed.  Structure
                                                 defined in hw_auth_token_t in hw_auth_token.h. */
    KM_TAG_MAC_LENGTH = KM_UINT | 1003,       /* MAC or AEAD authentication tag length in
                                               * bits. */

    KM_TAG_RESET_SINCE_ID_ROTATION = KM_BOOL | 1004, /* Whether the device has beeen factory reset
                                                        since the last unique ID rotation.  Used for
                                                        key attestation. */
    /**
     * APPROVAL_TOKEN is used to deliver a cryptographic token proving that the
     * user confirmed a
     * signing request. The content is a 32-byte HMAC value. See the Trusted UI
     * HAL for details of
     * construction.
     */
    KM_CONFIRMATION_TOKEN = KM_BYTES | 1005,

    KM_TAG_USE_SECURE_PROCESSOR = KM_BOOL | 15000,
    KM_TAG_KEYBLOB = KM_BYTES | 16000,
    KM_TAG_AEAD_TAG = KM_BYTES | 16001,

    // Do not use anything in between.
    KM_TAG_NOROT_NOVERSION = KM_BOOL | 16008,

    KM_TAG_FBE_ICE = KM_BOOL | 16201,
    KM_TAG_KEY_TYPE = KM_UINT | 16202,
} keymaster_tag_t;

/**
 * Algorithms that may be provided by keymaster implementations.  Those that
 * must be provided by all
 * implementations are tagged as "required".
 */
typedef enum {
    /* Asymmetric algorithms. */
    KM_ALGORITHM_RSA = 1,
    // KM_ALGORITHM_DSA = 2, -- Removed, do not re-use value 2.
    KM_ALGORITHM_EC = 3,

    /* Block ciphers algorithms */
    KM_ALGORITHM_AES = 32,
    KM_ALGORITHM_TripleDES = 33,

    /* MAC algorithms */
    KM_ALGORITHM_HMAC = 128,
} keymaster_algorithm_t;

/**
 * The origin of a key (or pair), i.e. where it was generated.  Note that
 * KM_TAG_ORIGIN can be found
 * in either the hardware-enforced or software-enforced list for a key,
 * indicating whether the key
 * is hardware or software-based.  Specifically, a key with KM_ORIGIN_GENERATED
 * in the
 * hardware-enforced list is guaranteed never to have existed outide the secure
 * hardware.
 */
typedef enum {
    KM_ORIGIN_GENERATED = 0, /* Generated in keymaster.  Should not exist outside the TEE. */
    KM_ORIGIN_DERIVED = 1,   /* Derived inside keymaster.  Likely exists off-device. */
    KM_ORIGIN_IMPORTED = 2,  /* Imported into keymaster.  Existed as cleartext in Android. */
    KM_ORIGIN_UNKNOWN = 3,   /* Keymaster did not record origin.  This value can
                              * only be seen on
                              * keys in a keymaster0 implementation.  The
                              * keymaster0 adapter uses
                              * this value to document the fact that it is unkown
                              * whether the key
                              * was generated inside or imported into keymaster.
                              */
    KM_SECURELY_IMPORTED = 4 /* Securely imported into keymaster. Was created
                              * elsewhere, and passed
                              * securely through Android to secure hardware. */
} keymaster_key_origin_t;

/**
 * Possible purposes of a key (or pair).
 */
typedef enum {
    KM_PURPOSE_ENCRYPT = 0,    /* Usable with RSA, EC and AES keys. */
    KM_PURPOSE_DECRYPT = 1,    /* Usable with RSA, EC and AES keys. */
    KM_PURPOSE_SIGN = 2,       /* Usable with RSA, EC and HMAC keys. */
    KM_PURPOSE_VERIFY = 3,     /* Usable with RSA, EC and HMAC keys. */
    KM_PURPOSE_DERIVE_KEY = 4, /* Usable with EC keys. */
    KM_PURPOSE_WRAP_KEY = 5,   /* Usable with wrapping keys. */
} keymaster_purpose_t;

typedef enum {
    KM_SECURITY_LEVEL_SOFTWARE = 0,
    KM_SECURITY_LEVEL_TRUSTED_ENVIRONMENT = 1,
    KM_SECURITY_LEVEL_STRONGBOX = 2, /* See IKeymaster::isStrongBox */
} keymaster_security_level_t;

typedef enum {
    KM_ERROR_OK = 0,
    KM_ERROR_ROOT_OF_TRUST_ALREADY_SET = -1,
    KM_ERROR_UNSUPPORTED_PURPOSE = -2,
    KM_ERROR_INCOMPATIBLE_PURPOSE = -3,
    KM_ERROR_UNSUPPORTED_ALGORITHM = -4,
    KM_ERROR_INCOMPATIBLE_ALGORITHM = -5,
    KM_ERROR_UNSUPPORTED_KEY_SIZE = -6,
    KM_ERROR_UNSUPPORTED_BLOCK_MODE = -7,
    KM_ERROR_INCOMPATIBLE_BLOCK_MODE = -8,
    KM_ERROR_UNSUPPORTED_MAC_LENGTH = -9,
    KM_ERROR_UNSUPPORTED_PADDING_MODE = -10,
    KM_ERROR_INCOMPATIBLE_PADDING_MODE = -11,
    KM_ERROR_UNSUPPORTED_DIGEST = -12,
    KM_ERROR_INCOMPATIBLE_DIGEST = -13,
    KM_ERROR_INVALID_EXPIRATION_TIME = -14,
    KM_ERROR_INVALID_USER_ID = -15,
    KM_ERROR_INVALID_AUTHORIZATION_TIMEOUT = -16,
    KM_ERROR_UNSUPPORTED_KEY_FORMAT = -17,
    KM_ERROR_INCOMPATIBLE_KEY_FORMAT = -18,
    KM_ERROR_UNSUPPORTED_KEY_ENCRYPTION_ALGORITHM = -19,   /* For PKCS8 & PKCS12 */
    KM_ERROR_UNSUPPORTED_KEY_VERIFICATION_ALGORITHM = -20, /* For PKCS8 & PKCS12 */
    KM_ERROR_INVALID_INPUT_LENGTH = -21,
    KM_ERROR_KEY_EXPORT_OPTIONS_INVALID = -22,
    KM_ERROR_DELEGATION_NOT_ALLOWED = -23,
    KM_ERROR_KEY_NOT_YET_VALID = -24,
    KM_ERROR_KEY_EXPIRED = -25,
    KM_ERROR_KEY_USER_NOT_AUTHENTICATED = -26,
    KM_ERROR_OUTPUT_PARAMETER_NULL = -27,
    KM_ERROR_INVALID_OPERATION_HANDLE = -28,
    KM_ERROR_INSUFFICIENT_BUFFER_SPACE = -29,
    KM_ERROR_VERIFICATION_FAILED = -30,
    KM_ERROR_TOO_MANY_OPERATIONS = -31,
    KM_ERROR_UNEXPECTED_NULL_POINTER = -32,
    KM_ERROR_INVALID_KEY_BLOB = -33,
    KM_ERROR_IMPORTED_KEY_NOT_ENCRYPTED = -34,
    KM_ERROR_IMPORTED_KEY_DECRYPTION_FAILED = -35,
    KM_ERROR_IMPORTED_KEY_NOT_SIGNED = -36,
    KM_ERROR_IMPORTED_KEY_VERIFICATION_FAILED = -37,
    KM_ERROR_INVALID_ARGUMENT = -38,
    KM_ERROR_UNSUPPORTED_TAG = -39,
    KM_ERROR_INVALID_TAG = -40,
    KM_ERROR_MEMORY_ALLOCATION_FAILED = -41,
    KM_ERROR_IMPORT_PARAMETER_MISMATCH = -44,
    KM_ERROR_SECURE_HW_ACCESS_DENIED = -45,
    KM_ERROR_OPERATION_CANCELLED = -46,
    KM_ERROR_CONCURRENT_ACCESS_CONFLICT = -47,
    KM_ERROR_SECURE_HW_BUSY = -48,
    KM_ERROR_SECURE_HW_COMMUNICATION_FAILED = -49,
    KM_ERROR_UNSUPPORTED_EC_FIELD = -50,
    KM_ERROR_MISSING_NONCE = -51,
    KM_ERROR_INVALID_NONCE = -52,
    KM_ERROR_MISSING_MAC_LENGTH = -53,
    KM_ERROR_KEY_RATE_LIMIT_EXCEEDED = -54,
    KM_ERROR_CALLER_NONCE_PROHIBITED = -55,
    KM_ERROR_KEY_MAX_OPS_EXCEEDED = -56,
    KM_ERROR_INVALID_MAC_LENGTH = -57,
    KM_ERROR_MISSING_MIN_MAC_LENGTH = -58,
    KM_ERROR_UNSUPPORTED_MIN_MAC_LENGTH = -59,
    KM_ERROR_UNSUPPORTED_KDF = -60,
    KM_ERROR_UNSUPPORTED_EC_CURVE = -61,
    KM_ERROR_KEY_REQUIRES_UPGRADE = -62,
    KM_ERROR_ATTESTATION_CHALLENGE_MISSING = -63,
    KM_ERROR_KEYMASTER_NOT_CONFIGURED = -64,
    KM_ERROR_ATTESTATION_APPLICATION_ID_MISSING = -65,
    KM_ERROR_CANNOT_ATTEST_IDS = -66,
    KM_ROLLBACK_RESISTANCE_UNAVAILABLE = -67,
    KM_HARDWARE_TYPE_UNAVAILABLE = -68,
    KM_PROOF_OF_PRESENCE_REQUIRED = -69,
    KM_CONCURRENT_PROOF_OF_PRESENCE_REQUESTED = -70,
    KM_NO_USER_CONFIRMATION = -71,
    KM_DEVICE_LOCKED = -72,
    KM_EARLY_BOOT_ENDED = -73,
    KM_ATTESTATION_KEYS_NOT_PROVISIONED = -74,
    KM_ATTESTATION_IDS_NOT_PROVISIONED = -75,
    KM_INVALID_OPERATION = -76,
    KM_STORAGE_KEY_UNSUPPORTED = -77,

    KM_ERROR_UNIMPLEMENTED = -100,
    KM_ERROR_VERSION_MISMATCH = -101,

    KM_ERROR_UNKNOWN_ERROR = -1000,

    KM_ERROR_SOTER_ERROR = -10000,
    KM_SECURITY_STATE_FAILURE = -10001,
    KM_ERROR_DEVICE_ID_FILE_NOT_FOUND = -10002,
    KM_ERROR_FILE_NOT_FOUND = -10003,
    KM_ERROR_ATTESTATION_KEY_ALREADY_PROVISIONED = -10004,
    KM_ERROR_DEVICE_ID_ALREADY_PROVISIONED = -10005,
    KM_SPU_SHARED_BUF_INITIALIZED = -10006,
    KM_SPU_SHARED_BUF_UNINITIALIZED = -10007,
    KM_SPU_NOT_SUPPORTED = -10008,
    KM_RPMB_NOT_PROVISIONED = -10009,
    KM_ERROR_ATTK_NOT_PROVISIONED = -10010,
    KM_ERROR_ATTK_ALREADY_PROVISIONED = -10011,
    KM_ERROR_SOTER_NOT_ENABLED = -10012,
    KM_ERROR_SHARING_PARAMS_UNINITIALIZED = -10013,
    KM_ERROR_NO_ENTRY_FOUND = -10014,
    KM_ERROR_CANNOT_ATTEST_KEY = -10015,
    KM_ERROR_SB_ARI_UNAVAILABLE = -10016,
    KM_ERROR_SB_ARI_UNAVAILABLE_FDR_REQUIRED = -10017,
    KM_ERROR_DEVICE_ID_REPROVISIONING_NOT_ALLOWED = -10018,
} keymaster_error_t;
#ifdef __cplusplus
}
#endif // __cplusplus

#endif /* KEYMASTER_KM4_INC_KEYMASTER_DEFS_VERSION_H_ */
