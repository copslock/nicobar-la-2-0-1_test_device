/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_HARDWARE_KEYMASTER_DEFS_H
#define ANDROID_HARDWARE_KEYMASTER_DEFS_H

#include <keymaster_defs_version.h>
#include <km_platform_utils.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void km_free(void *ptr);

/**
 * Symmetric block cipher modes provided by keymaster implementations.
 */
typedef enum {
    /* Unauthenticated modes, usable only for encryption/decryption and not
     * generally recommended
     * except for compatibility with existing other protocols. */
    KM_MODE_ECB = 1,
    KM_MODE_CBC = 2,
    KM_MODE_CTR = 3,

    /* Authenticated modes, usable for encryption/decryption and
     * signing/verification.  Recommended
     * over unauthenticated modes for all purposes. */
    KM_MODE_GCM = 32,
} keymaster_block_mode_t;

/**
 * Padding modes that may be applied to plaintext for encryption operations.
 * This list includes
 * padding modes for both symmetric and asymmetric algorithms.  Note that
 * implementations should not
 * provide all possible combinations of algorithm and padding, only the
 * cryptographically-appropriate pairs.
 */
typedef enum {
    KM_PAD_NONE = 1, /* deprecated */
    KM_PAD_RSA_OAEP = 2,
    KM_PAD_RSA_PSS = 3,
    KM_PAD_RSA_PKCS1_1_5_ENCRYPT = 4,
    KM_PAD_RSA_PKCS1_1_5_SIGN = 5,
    KM_PAD_PKCS7 = 64,
} keymaster_padding_t;

/**
 * Digests provided by keymaster implementations.
 */
typedef enum {
    KM_DIGEST_NONE = 0,
    KM_DIGEST_MD5 = 1, /* Optional, may not be implemented in hardware, will be
                        * handled in software
                        * if needed. */
    KM_DIGEST_SHA1 = 2,
    KM_DIGEST_SHA_2_224 = 3,
    KM_DIGEST_SHA_2_256 = 4,
    KM_DIGEST_SHA_2_384 = 5,
    KM_DIGEST_SHA_2_512 = 6,
} keymaster_digest_t;

/*
 * Key derivation functions, mostly used in ECIES.
 */
typedef enum {
    /* Do not apply a key derivation function; use the raw agreed key */
    KM_KDF_NONE = 0,
    /* HKDF defined in RFC 5869 with SHA256 */
    KM_KDF_RFC5869_SHA256 = 1,
    /* KDF1 defined in ISO 18033-2 with SHA1 */
    KM_KDF_ISO18033_2_KDF1_SHA1 = 2,
    /* KDF1 defined in ISO 18033-2 with SHA256 */
    KM_KDF_ISO18033_2_KDF1_SHA256 = 3,
    /* KDF2 defined in ISO 18033-2 with SHA1 */
    KM_KDF_ISO18033_2_KDF2_SHA1 = 4,
    /* KDF2 defined in ISO 18033-2 with SHA256 */
    KM_KDF_ISO18033_2_KDF2_SHA256 = 5,
} keymaster_kdf_t;

/**
 * Supported EC curves, used in ECDSA/ECIES.
 */
typedef enum {
    KM_EC_CURVE_P_224 = 0,
    KM_EC_CURVE_P_256 = 1,
    KM_EC_CURVE_P_384 = 2,
    KM_EC_CURVE_P_521 = 3,
} keymaster_ec_curve_t;

/**
 * Usability requirements of key blobs.  This defines what system functionality
 * must be available
 * for the key to function.  For example, key "blobs" which are actually handles
 * referencing
 * encrypted key material stored in the file system cannot be used until the
 * file system is
 * available, and should have BLOB_REQUIRES_FILE_SYSTEM.  Other requirements
 * entries will be added
 * as needed for implementations.
 */
typedef enum {
    KM_BLOB_STANDALONE = 0,
    KM_BLOB_REQUIRES_FILE_SYSTEM = 1,
} keymaster_key_blob_usage_requirements_t;

typedef struct {
    const uint8_t *data;
    size_t data_length;
} keymaster_blob_t;

typedef struct {
    uint8_t *data;
    size_t data_length;
} keymaster_blob_mod_t;

typedef struct {
    keymaster_tag_t tag;
    union {
        uint32_t enumerated;   /* KM_ENUM and KM_ENUM_REP */
        bool boolean;          /* KM_BOOL */
        uint32_t integer;      /* KM_UINT and KM_UINT_REP */
        uint64_t long_integer; /* KM_ULONG */
        uint64_t date_time;    /* KM_DATE */
        keymaster_blob_t blob; /* KM_BIGNUM and KM_BYTES*/
    } u;
} keymaster_key_param_t;

typedef struct {
    keymaster_key_param_t *params; /* may be NULL if length == 0 */
    size_t length;
} keymaster_key_param_set_t;

/**
 * Parameters that define a key's characteristics, including authorized modes of
 * usage and access
 * control restrictions.  The parameters are divided into two categories, those
 * that are enforced by
 * secure hardware, and those that are not.  For a software-only keymaster
 * implementation the
 * enforced array must NULL.  Hardware implementations must enforce everything
 * in the enforced
 * array.
 */
typedef struct {
    keymaster_key_param_set_t hw_enforced;
    keymaster_key_param_set_t sw_enforced;
} keymaster_key_characteristics_t;

typedef struct {
    const uint8_t *key_material;
    size_t key_material_size;
} keymaster_key_blob_t;

typedef struct {
    keymaster_blob_mod_t *entries;
    size_t entry_count;
} keymaster_cert_chain_t;

typedef struct {
    keymaster_algorithm_t algo;
    keymaster_key_blob_t key;
    keymaster_cert_chain_t cert_chain;
} keybox_install_t;

typedef struct {
    keybox_install_t *entries;
    size_t entry_count;
} km_install_keybox_t;

typedef enum {
    KM_VERIFIED_BOOT_VERIFIED = 0,    /* Full chain of trust extending from the bootloader to
                                       * verified partitions, including the bootloader, boot
                                       * partition, and all verified partitions*/
    KM_VERIFIED_BOOT_SELF_SIGNED = 1, /* The boot partition has been verified using the embedded
                                       * certificate, and the signature is valid. The bootloader
                                       * displays a warning and the fingerprint of the public
                                       * key before allowing the boot process to continue.*/
    KM_VERIFIED_BOOT_UNVERIFIED = 2,  /* The device may be freely modified. Device integrity is left
                                       * to the user to verify out-of-band. The bootloader
                                       * displays a warning to the user before allowing the boot
                                       * process to continue */
    KM_VERIFIED_BOOT_FAILED = 3,      /* The device failed verification. The bootloader displays a
                                       * warning and stops the boot process, so no keymaster
                                       * implementation should ever actually return this value,
                                       * since it should not run.  Included here only for
                                       * completeness. */
} keymaster_verified_boot_t;

/**
 * Formats for key import and export.
 */
typedef enum {
    KM_KEY_FORMAT_X509 = 0,  /* for public key export */
    KM_KEY_FORMAT_PKCS8 = 1, /* for asymmetric key pair import */
    KM_KEY_FORMAT_RAW = 3,   /* for symmetric key import and export*/
} keymaster_key_format_t;

/**
 * The keymaster operation API consists of begin, update, finish and abort. This
 * is the type of the
 * handle used to tie the sequence of calls together.  A 64-bit value is used
 * because it's important
 * that handles not be predictable.  Implementations must use strong random
 * numbers for handle
 * values.
 */
typedef uint64_t keymaster_operation_handle_t;

/* Convenience functions for manipulating keymaster tag types */

static inline keymaster_tag_type_t keymaster_tag_get_type(keymaster_tag_t tag) {
    return (keymaster_tag_type_t)(tag & (0xF << 28));
}

static inline uint32_t keymaster_tag_mask_type(keymaster_tag_t tag) {
    return tag & 0x0FFFFFFF;
}

static inline bool keymaster_tag_type_repeatable(keymaster_tag_type_t type) {
    switch (type) {
    case KM_UINT_REP:
    case KM_ENUM_REP:
        return true;
    default:
        return false;
    }
}

static inline bool keymaster_tag_repeatable(keymaster_tag_t tag) {
    return keymaster_tag_type_repeatable(keymaster_tag_get_type(tag));
}

/* Convenience functions for manipulating keymaster_key_param_t structs */

static inline keymaster_key_param_t keymaster_param_enum(keymaster_tag_t tag, uint32_t value) {
    keymaster_key_param_t param;
    memset(&param, 0, sizeof(param));
    param.tag = tag;
    param.u.enumerated = value;
    return param;
}

static inline keymaster_key_param_t keymaster_param_int(keymaster_tag_t tag, uint32_t value) {
    keymaster_key_param_t param;
    memset(&param, 0, sizeof(param));
    param.tag = tag;
    param.u.integer = value;
    return param;
}

static inline keymaster_key_param_t keymaster_param_long(keymaster_tag_t tag, uint64_t value) {
    // assert(keymaster_tag_get_type(tag) == KM_ULONG);
    keymaster_key_param_t param;
    memset(&param, 0, sizeof(param));
    param.tag = tag;
    param.u.long_integer = value;
    return param;
}

static inline keymaster_key_param_t keymaster_param_blob(keymaster_tag_t tag, const uint8_t *bytes,
                                                         size_t bytes_len) {
    // assert(keymaster_tag_get_type(tag) == KM_BYTES ||
    // keymaster_tag_get_type(tag) == KM_BIGNUM);
    keymaster_key_param_t param;
    memset(&param, 0, sizeof(param));
    param.tag = tag;
    param.u.blob.data = c_const_cast(uint8_t *, bytes);
    param.u.blob.data_length = bytes_len;
    return param;
}

static inline keymaster_key_param_t keymaster_param_bool(keymaster_tag_t tag) {
    // assert(keymaster_tag_get_type(tag) == KM_BOOL);
    keymaster_key_param_t param;
    memset(&param, 0, sizeof(param));
    param.tag = tag;
    param.u.boolean = true;
    return param;
}

static inline keymaster_key_param_t keymaster_param_date(keymaster_tag_t tag, uint64_t value) {
    // assert(keymaster_tag_get_type(tag) == KM_DATE);
    keymaster_key_param_t param;
    memset(&param, 0, sizeof(param));
    param.tag = tag;
    param.u.date_time = value;
    return param;
}

#define KEYMASTER_SIMPLE_COMPARE(a, b) (a < b) ? -1 : ((a > b) ? 1 : 0)
static inline int keymaster_param_compare(const keymaster_key_param_t *a,
                                          const keymaster_key_param_t *b) {
    int retval = KEYMASTER_SIMPLE_COMPARE(a->tag, b->tag);
    if (retval != 0)
        return retval;

    switch (keymaster_tag_get_type(a->tag)) {
    case KM_INVALID:
    case KM_BOOL:
        return 0;
    case KM_ENUM:
    case KM_ENUM_REP:
        return KEYMASTER_SIMPLE_COMPARE(a->u.enumerated, b->u.enumerated);
    case KM_UINT:
    case KM_UINT_REP:
        return KEYMASTER_SIMPLE_COMPARE(a->u.integer, b->u.integer);
    case KM_ULONG:
    case KM_ULONG_REP:
        return KEYMASTER_SIMPLE_COMPARE(a->u.long_integer, b->u.long_integer);
    case KM_DATE:
        return KEYMASTER_SIMPLE_COMPARE(a->u.date_time, b->u.date_time);
    case KM_BIGNUM:
    case KM_BYTES:
        // Handle the empty cases.
        if (a->u.blob.data_length != 0 && b->u.blob.data_length == 0)
            return -1;
        if (a->u.blob.data_length == 0 && b->u.blob.data_length == 0)
            return 0;
        if (a->u.blob.data_length == 0 && b->u.blob.data_length > 0)
            return 1;

        retval = memcmp(a->u.blob.data, b->u.blob.data,
                        a->u.blob.data_length < b->u.blob.data_length ? a->u.blob.data_length
                                                                      : b->u.blob.data_length);
        if (retval != 0)
            return retval;
        else if (a->u.blob.data_length != b->u.blob.data_length) {
            // Equal up to the common length; longer one is larger.
            if (a->u.blob.data_length < b->u.blob.data_length)
                return -1;
            if (a->u.blob.data_length > b->u.blob.data_length)
                return 1;
        };
    }

    return 0;
}

static inline void keymaster_free_cert_chain(keymaster_cert_chain_t *chain) {
    if (chain) {
        for (size_t i = 0; i < chain->entry_count; ++i) {
            km_free((void *)chain->entries[i].data);
            chain->entries[i].data = NULL;
            chain->entries[i].data_length = 0;
        }
        km_free((void *)chain->entries);
        chain->entries = NULL;
        chain->entry_count = 0;
    }
}

//#undef KEYMASTER_SIMPLE_COMPARE
#ifdef __cplusplus
}
#endif // __cplusplus
#endif // ANDROID_HARDWARE_KEYMASTER_DEFS_H
