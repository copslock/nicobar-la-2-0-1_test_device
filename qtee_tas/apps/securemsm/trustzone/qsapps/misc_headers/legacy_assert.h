#pragma once

// ASSERT does not do anything in the TZ source tree
// assert.h has been removed from the standalone SDK, so
// we define and stub ASSERT here
#ifndef ASSERT
#define ASSERT(...)
#endif

// the standard assert.h from musl calls to __assert_fail. This isn't defined by commonlib
// or applib or the TA itself, so for now we just stub this
#ifdef assert
#undef assert
#define assert(...)
#endif
