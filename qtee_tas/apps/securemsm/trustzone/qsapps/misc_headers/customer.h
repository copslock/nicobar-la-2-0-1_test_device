#pragma once

/* The apps component doesn't actually need anything from this file. However, this
   file still get included eventually be many files, so this file is just an empty stub
   so that we don't need to deploy the real version from trustzone_images/build/ms */