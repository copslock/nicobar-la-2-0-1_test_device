// Tell IWYU that the header com_dtypes.h should always be included instead of this one.
// IWYU pragma: private, include "com_dtypes.h"

#ifndef COMDEF_H
#define COMDEF_H

#include "com_dtypes.h"

#ifndef MAX
   #define  MAX( x, y ) ( ((x) > (y)) ? (x) : (y) )
#endif

#ifndef MIN
   #define  MIN( x, y ) ( ((x) < (y)) ? (x) : (y) )
#endif

#ifndef ARR_SIZE
#define  ARR_SIZE( a )  ( sizeof( (a) ) / sizeof( (a[0]) ) )
#endif

#endif  /* COMDEF_H */
