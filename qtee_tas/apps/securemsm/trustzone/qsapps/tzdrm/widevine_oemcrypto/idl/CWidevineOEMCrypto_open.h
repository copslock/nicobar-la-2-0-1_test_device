#ifndef __CWIDEVINEOEMCRYPTO_OPEN_H__
#define __CWIDEVINEOEMCRYPTO_OPEN_H__

#include "object.h"

int32_t i_widevine_oemcrypto_open(Object *obj);

#endif /* __CWIDEVINEOEMCRYPTO_OPEN_H__ */
