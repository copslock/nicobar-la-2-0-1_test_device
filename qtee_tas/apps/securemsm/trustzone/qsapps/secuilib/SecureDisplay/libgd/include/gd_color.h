#ifndef GD_COLOR_H
#define GD_COLOR_H 1

/*===========================================================================
   Copyright (c) 2013 Qualcomm Technologies, Inc.       
   All Rights Reserved.
   Qualcomm Technologies Proprietary and Confidential.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/apps.tz/2.11/securemsm/trustzone/qsapps/secuilib/SecureDisplay/libgd/include/gd_color.h#2 $
  $DateTime: 2020/12/23 16:04:41 $
  $Author: pwbldsvc $

when       who      what, where, why
--------   ---      ------------------------------------
07/02/13   ts       Initial version.

===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

	int gdColorMatch(gdImagePtr im, int col1, int col2, float threshold);

#ifdef __cplusplus
}
#endif

#endif
