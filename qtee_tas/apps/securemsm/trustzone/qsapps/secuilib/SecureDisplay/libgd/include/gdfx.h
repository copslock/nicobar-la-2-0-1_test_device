/*===========================================================================
   Copyright (c) 2013, 2019 Qualcomm Technologies, Inc.
   All Rights Reserved.
   Qualcomm Technologies Proprietary and Confidential.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

when       who      what, where, why
--------   ---      ------------------------------------
07/02/13   ts       Initial version.

===========================================================================*/

#ifndef GDFX_H
#define GDFX_H 1

#ifdef __cplusplus
extern "C" {
#endif

#ifndef SECURE_UI_SUPPORTED

BGD_DECLARE(gdImagePtr) gdImageSquareToCircle(gdImagePtr im, int radius);

BGD_DECLARE(char *) gdImageStringFTCircle(
    gdImagePtr im,
    int cx,
    int cy,
    double radius,
    double textRadius,
    double fillPortion,
    char *font,
    double points,
    char *top,
    char *bottom,
    int fgcolor);

BGD_DECLARE(void) gdImageSharpen (gdImagePtr im, int pct);

#endif

#ifdef __cplusplus
}
#endif

#endif /* GDFX_H */
