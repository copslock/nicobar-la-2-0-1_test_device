#ifndef SECUILIB_LOG_H
#define SECUILIB_LOG_H

#ifdef LE_FLAVOR
#include "TUILog.h"
#else
#include "qsee_log.h"
#endif

#ifndef LE_FLAVOR
#define tui_log(x, y ...) qsee_log(x, y, ...)
#endif

#endif /* SECUILIB_LOG_H */
