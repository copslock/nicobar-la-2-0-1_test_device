#ifndef SECUILIB_HEAP_H
#define SECUILIB_HEAP_H


#ifdef LE_FLAVOR
#include "TUIHeap.h"
#else
#include "qsee_heap.h"
#endif

#ifndef LE_FLAVOR
#define tui_free(x)       qsee_free(x)
#define tui_malloc(x)     qsee_malloc(x)
#define tui_calloc(x, y)  qsee_calloc(x, y)
#define tui_realloc(x, y) qsee_realloc(x, y)
#endif

#endif /* SECUILIB_HEAP_H */
