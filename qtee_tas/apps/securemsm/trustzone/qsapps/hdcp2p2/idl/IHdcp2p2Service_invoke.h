// Copyright (c) 2016 Qualcomm Technologies, Inc.  All Rights Reserved.
// Qualcomm Technologies Proprietary and Confidential.
/**
 * @brief
 * Hdcp2p2 Service provides interface to query info such as topology from
 * hdcp2p2 trusted app.
 *
 */
/** Topology Info **/
#pragma once
// AUTOGENERATED FILE: DO NOT EDIT

#include <stdint.h>
#include "object.h"
#include "IHdcp2p2Service.h"

#define IHdcp2p2Service_DEFINE_INVOKE(func, prefix, type) \
  int32_t func(ObjectCxt h, ObjectOp op, ObjectArg *a, ObjectCounts k) \
  { \
    type me = (type) h; \
    switch (ObjectOp_methodID(op)) { \
      case Object_OP_release: { \
        return prefix##release(me); \
      } \
      case Object_OP_retain: { \
        return prefix##retain(me); \
      } \
      case IHdcp2p2Service_OP_getTopology: { \
        if (k != ObjectCounts_pack(1, 2, 0, 0) || \
          a[0].b.size != 4 || \
          a[2].b.size != 24) { \
          break; \
        } \
        const uint32_t *deviceType_ptr = (const uint32_t*) a[0].b.ptr; \
        uint8_t *receiverIdList_ptr = (uint8_t*) a[1].b.ptr; \
        size_t receiverIdList_len = a[1].b.size / sizeof(uint8_t); \
        IHdcp2p2Service_Topology *topology_ptr = (IHdcp2p2Service_Topology*) a[2].b.ptr; \
        int32_t r = prefix##getTopology(me, *deviceType_ptr, receiverIdList_ptr, receiverIdList_len, &receiverIdList_len, topology_ptr); \
        a[1].b.size = receiverIdList_len * sizeof(uint8_t); \
        return r; \
      } \
    } \
    return Object_ERROR_INVALID; \
  }


