Logging to /local/mnt/workspace/CRMBuilds/BOOT.XF.4.0-00225-NICOBARLAZ-3_20201202_221207/b/boot_images/QcomPkg/SocPkg/NicobarPkg/Bin/LAB/RELEASE/sign/SecImage_log.txt


    SecImage launched as: "/local/mnt/workspace/CRMBuilds/BOOT.XF.4.0-00225-NICOBARLAZ-3_20201202_221207/b/boot_images/QcomPkg/Tools/sectools/sectools.py secimage -i /local/mnt/workspace/CRMBuilds/BOOT.XF.4.0-00225-NICOBARLAZ-3_20201202_221207/b/boot_images/Build/NicobarLAB/Core/RELEASE_CLANG40LINUX/FV/imagefv.elf -g uefifv -c /local/mnt/workspace/CRMBuilds/BOOT.XF.4.0-00225-NICOBARLAZ-3_20201202_221207/b/boot_images/QcomPkg/Tools/sectools/config/integration/secimagev3.xml --cfg_soc_hw_version 0x90010100 --cfg_soc_vers 0x9001 --cfg_in_use_soc_hw_version 1 --cfg_segment_hash_algorithm sha384 -s -o /local/mnt/workspace/CRMBuilds/BOOT.XF.4.0-00225-NICOBARLAZ-3_20201202_221207/b/boot_images/QcomPkg/SocPkg/NicobarPkg/Bin/LAB/RELEASE/sign"

WARNING: OEM ID is set to 0 for sign_id "uefifv"
------------------------------------------------------
Processing 1/1: /local/mnt/workspace/CRMBuilds/BOOT.XF.4.0-00225-NICOBARLAZ-3_20201202_221207/b/boot_images/Build/NicobarLAB/Core/RELEASE_CLANG40LINUX/FV/imagefv.elf

Upscaling MBN header v5 to MBN header v6
Performing OEM sign on image: /local/mnt/workspace/CRMBuilds/BOOT.XF.4.0-00225-NICOBARLAZ-3_20201202_221207/b/boot_images/Build/NicobarLAB/Core/RELEASE_CLANG40LINUX/FV/imagefv.elf
Signed image is stored at /local/mnt/workspace/CRMBuilds/BOOT.XF.4.0-00225-NICOBARLAZ-3_20201202_221207/b/boot_images/QcomPkg/SocPkg/NicobarPkg/Bin/LAB/RELEASE/sign/default/uefifv/imagefv.elf
OEM signed image with RSAPSS
Image /local/mnt/workspace/CRMBuilds/BOOT.XF.4.0-00225-NICOBARLAZ-3_20201202_221207/b/boot_images/QcomPkg/SocPkg/NicobarPkg/Bin/LAB/RELEASE/sign/default/uefifv/imagefv.elf signature is valid
Image /local/mnt/workspace/CRMBuilds/BOOT.XF.4.0-00225-NICOBARLAZ-3_20201202_221207/b/boot_images/QcomPkg/SocPkg/NicobarPkg/Bin/LAB/RELEASE/sign/default/uefifv/imagefv.elf is not encrypted

Base Properties: 
| Integrity Check             | True  |
| Signed                      | True  |
| Encrypted                   | False |
| Size of signature           | 256   |
| Size of one cert            | 2048  |
| Num of certs in cert chain  | 3     |
| Number of root certs        | 1     |
| Maximum number of root certs| 1     |
| Cert chain size             | 6144  |

ELF Properties: 
Header: 
| Magic                      | ELF                           |
| Class                      | ELF32                          |
| Data                       | 2's complement, little endian  |
| Version                    | 1 (Current)                    |
| OS/ABI                     | No extensions or unspecified   |
| ABI Version                | 0                              |
| Type                       | EXEC (Executable file)         |
| Machine                    | Advanced RISC Machines ARM     |
| Version                    | 0x1                            |
| Entry address              | 0x00000000                     |
| Program headers offset     | 0x00000034                     |
| Section headers offset     | 0x00000000                     |
| Flags                      | 0x00000000                     |
| ELF header size            | 52                             |
| Program headers size       | 32                             |
| Number of program headers  | 1                              |
| Section headers size       | 0                              |
| Number of section headers  | 0                              |
| String table section index | 0                              |

Program Headers: 
| Num | Type | Offset | VirtAddr | PhysAddr | FileSize | MemSize | Flags | Align |
|-----|------|--------|----------|----------|----------|---------|-------|-------|
|  1  | LOAD | 0x3000 |   0x0    |   0x0    |  0x2000  |  0x2000 |  RWE  | 0x1000|

Hash Segment Properties: 
| Header Size     | 168B  |
| Hash Algorithm  | sha384|

Header: 
| cert_chain_ptr              | 0x00002088  |
| cert_chain_size             | 0x00001800  |
| cert_chain_size_qti         | 0x00000000  |
| code_size                   | 0x00000090  |
| image_id                    | 0x00000005  |
| image_size                  | 0x00001990  |
| metadata_major_version      | 0x00000000  |
| metadata_major_version_qti  | 0x00000000  |
| metadata_minor_version      | 0x00000000  |
| metadata_minor_version_qti  | 0x00000000  |
| metadata_size               | 0x00000078  |
| metadata_size_qti           | 0x00000000  |
| sig_ptr                     | 0x00002088  |
| sig_size                    | 0x00000100  |
| sig_size_qti                | 0x00000000  |
| version                     | 0x00000006  |
Metadata:
| anti_rollback_version        | 0x00000000  |
| app_id                       | 0x00000000  |
| debug                        | 0x00000001  |
| hw_id                        | 0x00000000  |
| in_use_soc_hw_version        | 0x00000001  |
| model_id                     | 0x00000000  |
| mrc_index                    | 0x00000000  |
| multi_serial_numbers         | 0x00000000  |
| oem_id                       | 0x00000000  |
| oem_id_independent           | 0x00000000  |
| root_revoke_activate_enable  | 0x00000000  |
| rot_en                       | 0x00000000  |
| soc_vers                     | 0x00009001  |
| sw_id                        | 0x00000027  |
| uie_key_switch_enable        | 0x00000000  |
| use_serial_number_in_signing | 0x00000000  |


------------------------------------------------------

SUMMARY:
Following actions were performed: "sign, validate"
Output is saved at: /local/mnt/workspace/CRMBuilds/BOOT.XF.4.0-00225-NICOBARLAZ-3_20201202_221207/b/boot_images/QcomPkg/SocPkg/NicobarPkg/Bin/LAB/RELEASE/sign

| Idx | SignId | Parse | Integrity | Sign | Encrypt |              Validate              |
|     |        |       |           |      |         | Parse | Integrity | Sign | Encrypt |
|-----|--------|-------|-----------|------|---------|-------|-----------|------|---------|
|  1. | uefifv |   T   |     NA    |  T   |    NA   |   T   |     T     |  T   |    NA   |

