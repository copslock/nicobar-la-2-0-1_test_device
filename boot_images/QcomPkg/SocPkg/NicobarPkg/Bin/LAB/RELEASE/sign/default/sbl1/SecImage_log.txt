Upscaling MBN header v5 to MBN header v6
Performing OEM sign on image: /local/mnt/workspace/CRMBuilds/BOOT.XF.4.0-00225-NICOBARLAZ-3_20201202_221207/b/boot_images/Build/NicobarLAB/Loader/RELEASE_CLANG40LINUX/AARCH64/xbl.elf
Signed image is stored at /local/mnt/workspace/CRMBuilds/BOOT.XF.4.0-00225-NICOBARLAZ-3_20201202_221207/b/boot_images/QcomPkg/SocPkg/NicobarPkg/Bin/LAB/RELEASE/sign/default/sbl1/xbl.elf
OEM signed image with RSAPSS
Image /local/mnt/workspace/CRMBuilds/BOOT.XF.4.0-00225-NICOBARLAZ-3_20201202_221207/b/boot_images/QcomPkg/SocPkg/NicobarPkg/Bin/LAB/RELEASE/sign/default/sbl1/xbl.elf signature is valid
Image /local/mnt/workspace/CRMBuilds/BOOT.XF.4.0-00225-NICOBARLAZ-3_20201202_221207/b/boot_images/QcomPkg/SocPkg/NicobarPkg/Bin/LAB/RELEASE/sign/default/sbl1/xbl.elf is not encrypted

Base Properties: 
| Integrity Check             | True  |
| Signed                      | True  |
| Encrypted                   | False |
| Size of signature           | 256   |
| Size of one cert            | 2048  |
| Num of certs in cert chain  | 3     |
| Number of root certs        | 1     |
| Maximum number of root certs| 1     |
| Cert chain size             | 6144  |

ELF Properties: 
Header: 
| Magic                      | ELF                           |
| Class                      | ELF64                          |
| Data                       | 2's complement, little endian  |
| Version                    | 1 (Current)                    |
| OS/ABI                     | No extensions or unspecified   |
| ABI Version                | 0                              |
| Type                       | EXEC (Executable file)         |
| Machine                    | 183                            |
| Version                    | 0x1                            |
| Entry address              | 0x0c221908                     |
| Program headers offset     | 0x00000040                     |
| Section headers offset     | 0x00000000                     |
| Flags                      | 0x00000000                     |
| ELF header size            | 64                             |
| Program headers size       | 56                             |
| Number of program headers  | 15                             |
| Section headers size       | 0                              |
| Number of section headers  | 0                              |
| String table section index | 0                              |

Program Headers: 
| Num | Type | Offset | VirtAddr | PhysAddr | FileSize | MemSize | Flags | Align |
|-----|------|--------|----------|----------|----------|---------|-------|-------|
|  1  | LOAD |0x05e0d0|0x0c119000|0x0c119000| 0x002760 | 0x002760|   RE  | 0x1000|
|  2  | LOAD |0x060830|0x0c11c000|0x0c11c000| 0x001044 | 0x001044|   RW  | 0x1000|
|  3  | LOAD |0x076b00|0x0c11e000|0x0c11e000| 0x000000 | 0x002d84|   RW  | 0x1000|
|  4  | LOAD |0x05e0d0|0x0c215000|0x0c215000| 0x000000 | 0x00a000|   RW  | 0x1000|
|  5  | LOAD |0x003000|0x0c221000|0x0c221000| 0x04e0c4 | 0x04e0c4|   RE  | 0x1000|
|  6  | LOAD |0x0510d0|0x0c274000|0x0c274000| 0x000000 | 0x003000|   RW  | 0x1000|
|  7  | LOAD |0x0510d0|0x0c277000|0x0c277000| 0x00d000 | 0x00d000|   RW  | 0x1000|
|  8  | LOAD |0x05e0d0|0x0c284000|0x0c284000| 0x000000 | 0x010f38|   RW  | 0x1000|
|  9  | LOAD |0x061880|0x0c299000|0x0c299000| 0x015280 | 0x015280|  RWE  | 0x1000|
|  10 | LOAD |0x276b00|0x0c2c0000|0x0c2c0000| 0x017000 | 0x017000|   RE  | 0x1000|
|  11 | LOAD |0x05e0d0|0x45e00000|0x45e00000| 0x000000 | 0x029740|   RW  | 0x1000|
|  12 | LOAD |0x28db00|0x45e35000|0x45e35000| 0x04596f | 0x04596f|   RE  | 0x1000|
|  13 | LOAD |0x2da390|0x45e97000|0x45e97000| 0x000000 | 0x001fe0|   RW  | 0x1000|
|  14 | LOAD |0x2d3470|0x45ea7000|0x45ea7000| 0x006f18 | 0x050a24|   RW  | 0x1000|
|  15 | LOAD |0x076b00|0x5fc00000|0x5fc00000| 0x200000 | 0x200000|  RWE  | 0x1000|

Hash Segment Properties: 
| Header Size     | 168B  |
| Hash Algorithm  | sha384|

Header: 
| cert_chain_ptr              | 0xffffffff  |
| cert_chain_size             | 0x00001800  |
| cert_chain_size_qti         | 0x00000000  |
| code_size                   | 0x00000330  |
| image_id                    | 0x00000005  |
| image_size                  | 0x00001c30  |
| metadata_major_version      | 0x00000000  |
| metadata_major_version_qti  | 0x00000000  |
| metadata_minor_version      | 0x00000000  |
| metadata_minor_version_qti  | 0x00000000  |
| metadata_size               | 0x00000078  |
| metadata_size_qti           | 0x00000000  |
| sig_ptr                     | 0xffffffff  |
| sig_size                    | 0x00000100  |
| sig_size_qti                | 0x00000000  |
| version                     | 0x00000006  |
Metadata:
| anti_rollback_version        | 0x00000000  |
| app_id                       | 0x00000000  |
| debug                        | 0x00000000  |
| hw_id                        | 0x00000000  |
| in_use_soc_hw_version        | 0x00000001  |
| model_id                     | 0x00000000  |
| mrc_index                    | 0x00000000  |
| multi_serial_numbers         | 0x00000000  |
| oem_id                       | 0x00000000  |
| oem_id_independent           | 0x00000000  |
| root_revoke_activate_enable  | 0x00000000  |
| rot_en                       | 0x00000000  |
| soc_vers                     | 0x00009001  |
| sw_id                        | 0x00000000  |
| uie_key_switch_enable        | 0x00000000  |
| use_serial_number_in_signing | 0x00000000  |


